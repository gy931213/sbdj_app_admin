package com.sbdj;

import com.sbdj.core.base.BaseWebConfig;
import com.sbdj.core.constant.Version;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Created by Yly to 2019/11/8 14:51
 * Admin
 */
@SpringBootApplication
public class AdminApplication extends BaseWebConfig {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }

    @Bean
    @Override
    protected Docket api() {
        return super.api("ADMIN", 8080, Version.V1, "com.sbdj.admin");
    }
}
