package com.sbdj.admin.base;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.WebSession;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.GoodsMarkingUtil;
import com.sbdj.core.util.QiNuCloudUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.entity.ConfigQiniu;
import com.sbdj.service.configure.service.IQiNiuService;
import com.sbdj.service.system.entity.SysConfigDtl;
import com.sbdj.service.system.entity.SysFunction;
import com.sbdj.service.system.service.ISysConfigDtlService;
import com.sbdj.service.task.admin.vo.TaskSonBeforeDayVo;
import com.sbdj.service.task.entity.Task;
import com.sbdj.service.task.service.ITaskService;
import com.sbdj.service.task.service.ITaskSonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基本控制器
 * </p>
 *
 * @author Yly
 * @since 2019/11/21 13:04
 */
@Api(tags = "基本控制器")
@RestController
public class BaseController {

    private Logger logger = LoggerFactory.getLogger(BaseController.class);

    @Autowired
    private ISysConfigDtlService iSysConfigDtlService;
    @Autowired
    private ITaskService iTaskService;
    @Autowired
    private IQiNiuService iQiNiuService;
    @Autowired
    private ITaskSonService iTaskSonService;

    @ApiOperation(value = "获取权限")
    @RequestMapping(value = "/menu", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult<Map<String, Object>> loadFunction(HttpServletRequest request) {
        WebSession session = (WebSession) AdminRequestUtil.getSession(request);
        if (StrUtil.isNull(session)) {
            return RespResult.error();
        }
        List<SysFunction> list = AdminRequestUtil.getFunction(request);
        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("name", session.getName());
        map.put("headUrl", session.getHeadUrl());
        return RespResult.success(map);
    }

    @ApiOperation(value = "没有权限的处理")
    @RequestMapping(value = "/unauthorized", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult unauthorized() {
        return RespResult.error("你没有权限", 403 );
    }

    @RequiresPermissions("/index/show")
    @ApiOperation(value = "首页展示")
    @RequestMapping(value = "/index/show", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult show(HttpServletRequest request) {
        //String json = "{\"tss\":[{\"sum\":140,\"status\":\"A\",\"statusName\":\"待操作\",\"createTime\":\"2019-11-22\"},{\"sum\":64,\"status\":\"AU\",\"statusName\":\"待审核\",\"createTime\":\"2019-11-22\"},{\"sum\":3,\"status\":\"F\",\"statusName\":\"待提交\",\"createTime\":\"2019-11-22\"},{\"sum\":45,\"status\":\"NF\",\"statusName\":\"已作废\",\"createTime\":\"2019-11-22\"},{\"sum\":469,\"status\":\"T\",\"statusName\":\"已审核\",\"createTime\":\"2019-11-22\"}],\"tsday\":[{\"sum\":33,\"status\":\"A\",\"statusName\":\"待操作\",\"createTime\":\"2019-11-23\"},{\"sum\":38,\"status\":\"AU\",\"statusName\":\"待审核\",\"createTime\":\"2019-11-23\"},{\"sum\":98,\"status\":\"F\",\"statusName\":\"待提交\",\"createTime\":\"2019-11-23\"},{\"sum\":3,\"status\":\"NF\",\"statusName\":\"已作废\",\"createTime\":\"2019-11-23\"}],\"ts\":[{\"id\":null,\"orgId\":null,\"platformId\":null,\"taskNumber\":null,\"sellerShopId\":null,\"goodsId\":null,\"showPrice\":null,\"prefPrice\":null,\"realPrice\":null,\"packages\":null,\"brokerage\":null,\"serverPrice\":null,\"taskTypeId\":9,\"taskTotal\":329,\"taskResidueTotal\":193,\"createTime\":null,\"releaseTime\":null,\"timingTime\":\"2019-11-23T00:00:00.000+0000\",\"sellerAsk\":null,\"oprId\":null,\"oprTime\":null,\"top\":0,\"status\":null,\"isCard\":null,\"goodsKeywords\":null},{\"id\":null,\"orgId\":null,\"platformId\":null,\"taskNumber\":null,\"sellerShopId\":null,\"goodsId\":null,\"showPrice\":null,\"prefPrice\":null,\"realPrice\":null,\"packages\":null,\"brokerage\":null,\"serverPrice\":null,\"taskTypeId\":10,\"taskTotal\":316,\"taskResidueTotal\":283,\"createTime\":null,\"releaseTime\":null,\"timingTime\":\"2019-11-23T00:00:00.000+0000\",\"sellerAsk\":null,\"oprId\":null,\"oprTime\":null,\"top\":0,\"status\":null,\"isCard\":null,\"goodsKeywords\":null}]}";
        //Gson gson = new GsonBuilder().create();
        //Map<String, Object> map;
        //Type type = new TypeToken<Map<String, Object>>(){}.getType();
        //map = gson.fromJson(json, type);
        Map<String, Object> map = new HashMap<>();
        Long orgId = AdminRequestUtil.orgId(request);
        List<Task> ts = iTaskService.taskSpeedList(orgId);
        List<TaskSonBeforeDayVo> tss = iTaskSonService.findTaskSonIsBeforeDayData(orgId,Boolean.TRUE);
        List<TaskSonBeforeDayVo> tsday = iTaskSonService.findTaskSonIsBeforeDayData(orgId,Boolean.FALSE);
        // 今天发布任务数量
        map.put("ts", ts);
        // 前一天的任务审核情况
        map.put("tss", tss);
        // 当天的任务审核情况
        map.put("tsday",tsday);
        return RespResult.success(map);
    }

    @ApiOperation(value = "获取类型配置信息",notes = "现付单，隔日单" )
    @RequestMapping(value = "/base/code")
    public RespResult findConfigDtlData(@ApiParam(value = "配置编码") String code) {
        List<SysConfigDtl> list = iSysConfigDtlService.findConfigDtl(code);
        return RespResult.success(list);
    }

    @ApiOperation(value = "图片上传")
    @PostMapping(value = "base/upload")
    public RespResult uploadFile(MultipartFile file, HttpServletRequest request) {
        System.out.println(file);
        try {
            InputStream inputStream = file.getInputStream();
            ConfigQiniu qiNiu = iQiNiuService.findOneByOrgId(AdminRequestUtil.orgId(request));
            if (qiNiu == null) {
                return RespResult.error("文件服务配置为空,无法上传!");
            }
            String url = QiNuCloudUtil.uploadFileByInputStream(qiNiu.getZone(), qiNiu.getAccessKey(),
                    qiNiu.getSecretKey(), qiNiu.getBucket(), inputStream);
            if (StrUtil.isNull(url)) {
                return RespResult.error("文件上传失败!");
            }
            return RespResult.success(qiNiu.getHost() + url + qiNiu.getImageView());
        } catch (IOException e) {
            e.printStackTrace();
            return RespResult.error();
        }
    }

    @ApiOperation(value = "上传视频")
    @PostMapping(value = "base/upload/video")
    public RespResult uploadFileVideo(@RequestParam(value = "uploadVideo", required = true) MultipartFile file, HttpServletRequest request) {
        try {
            InputStream inputStream = file.getInputStream();
            ConfigQiniu qiNiu = iQiNiuService.findOneByOrgId(AdminRequestUtil.orgId(request));
            if (qiNiu == null) {
                return RespResult.error("文件服务配置为空，无法上传！");
            }
            String url = QiNuCloudUtil.uploadFileByInputStream(qiNiu.getZone(), qiNiu.getAccessKey(),qiNiu.getSecretKey(), qiNiu.getBucket(), inputStream);
            if (StrUtil.isNull(url)) {
                return RespResult.error("视频上传失败！");
            }
            return RespResult.success(qiNiu.getHost() + url);
        } catch (IOException e) {
            return RespResult.error("视频上传失败！");
        }
    }

    @ApiOperation(value = "编辑文件上传")
    @PostMapping(value = "base//editor/upload")
    public Map<String, Object> upload(@RequestParam(value="files") List<MultipartFile> files, HttpServletRequest request) throws IOException {
        int errno = 0;
        List<String> urls = new ArrayList<>();
        Map<String, Object> map = new HashMap<>(2);
        if (files.size() == 0) {
            errno = 1;
        }
        ConfigQiniu qiNiu =  null;
        if(files.size() > 0) {
            qiNiu = iQiNiuService.findOneByOrgId(AdminRequestUtil.orgId(request));
            if (null == qiNiu) {
                errno = 1;
                map.put("errno", errno);
                map.put("data", urls);
                return map;
            }
        }
        // 遍历files
        for (MultipartFile file : files) {
            String url = QiNuCloudUtil.uploadFileByInputStream(qiNiu.getZone(), qiNiu.getAccessKey(),qiNiu.getSecretKey(), qiNiu.getBucket(), file.getInputStream());
            if(StrUtil.isNull(url)) {
                errno = 1;
            }
            urls.add(qiNiu.getHost() + url + qiNiu.getImageView());
        }
        map.put("errno", errno);
        map.put("data", urls);
        return map;
    }

    @ApiOperation(value = "商品打标", notes = "商品打标")
    @RequestMapping(value = "marking/goods/request")
    public RespResult markingGoods(String shop_id, String keywords, String wws) {
        // 搜索词
        String [] keyArr;
        if(keywords.contains("\n")) {
            keyArr = keywords.split("\n");
        }else {
            keyArr = new String[1];
            keyArr[0] = keywords;
        }

        // 旺旺号
        String[] wwsArr;
        if(wws.contains("\n")) {
            wwsArr = wws.split("\n");
        }else {
            wwsArr = new String[1];
            wwsArr[0]= wws;
        }
        // 判断搜索词和旺旺号数量是不是一致
        if((keyArr.length > 0 && wwsArr.length > 0) && keyArr.length != wwsArr.length) {
            return RespResult.error("搜索词数量，和旺旺号的数量不一致");
        }
        // 响应数据
        Map<String, Object> resp;
        try {
            resp = mark(2L, shop_id, keyArr, wwsArr);
        } catch (Exception e) {
            return RespResult.error(e.getMessage());
        }
        if (StrUtil.isNull(resp)) {return RespResult.error("打标失败");}
        return RespResult.success(resp);
    }

    public Map<String, Object> mark(Long orgId, String tbId, String[] memberNames, String[] keywords) {
        Map<String,Object> map = new HashMap<>();
        Integer retCode100 = 0; // 打标成功
        Integer retCode101 = 0; // 打标异常

        for (int i = 0; i < keywords.length; i++) {
            String kw = keywords[i].trim();
            String ww = memberNames[i].trim();
            try {
                String resp = GoodsMarkingUtil.markingGoods(tbId, kw, ww);
                if (StrUtil.isNotNull(resp)) {
                    JSONObject json = JSONObject.parseObject(resp);
                    boolean flag = json.getBoolean("state");
                    if (flag) {
                        retCode100++;
                        map.put("打标成功", retCode100);
                    } else {
                        retCode101++;
                        map.put(json.getString("msg"), retCode101);
                    }
                }
            } catch (Exception e) {
                retCode101++;
                logger.info("===>打标异常：异常信息【{}】", e.getMessage());
            }
        }

        if (retCode101 > 0) {
            map.put("打标异常：", retCode101);
        }
        logger.info("===>打标结束：当前打标的商品ID：【{}】，响应结果：【{}】",tbId, JSON.toJSON(map));
        return map;
    }
}
