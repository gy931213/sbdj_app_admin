package com.sbdj.admin.base;

import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.constant.AdminType;
import com.sbdj.core.constant.IsSuperType;
import com.sbdj.admin.web.WebSession;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysAdmin;
import com.sbdj.service.system.entity.SysFunction;
import com.sbdj.service.system.service.ISysFunctionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 登录控制器
 * </p>
 *
 * @author Yly
 * @since 2019/11/21 9:32
 */
@Api(tags = "登录控制器")
@RestController
public class LoginController {
    @Autowired
    private ISysFunctionService iSysFunctionService;

    @ApiOperation(value = "后台登录", notes = "采用ajax登录")
    @PostMapping("ajaxLogin")
    public RespResult ajaxLogin(HttpServletRequest request,
                                @ApiParam(value = "手机号码") String mobile,
                                @ApiParam(value = "密码") String pwd) {
        if (StrUtil.isNull(mobile) || StrUtil.isNull(pwd)) {
            return RespResult.error("手机号或密码不能为空");
        }
        try {
            Subject subject = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(mobile, pwd);
            subject.login(token);
            // 授权
            SysAdmin sysAdmin = (SysAdmin) subject.getPrincipal();
            List<SysFunction> list = iSysFunctionService.findFunctionByAdminId(sysAdmin.getId());
            // 判断是否有数据操权限!
            if(StrUtil.isNull(list)) {
                return	RespResult.error("当前登录账号没有授权，请联系管理员！");
            }
            // 保存操作权限
            Set<String> fun = new HashSet<>();
            for (SysFunction function : list) {
                if (StrUtil.isNotNull(function.getUrl())) {
                    fun.add(function.getUrl());
                }
            }
            // 创建当前登录的会话数据!
            WebSession session = new WebSession();
            session.setAuth(Boolean.TRUE);
            session.setIdentify(sysAdmin.getId());
            session.setName(sysAdmin.getName());
            session.setHeadUrl(sysAdmin.getHaedUrl());
            session.setPermissions(fun);
            // 判断是后台操作人员还是商家账号! if : 后台管理员 | else ：商家账号
            if (String.valueOf(AdminType.TYPE_ADMIN).equalsIgnoreCase(String.valueOf(sysAdmin.getType()))){
                // ~判断当前登录人是否是系统超级管理员
                // ~如果是系统超级管理员的话，所属机构id默认为-1,
                // ~如果不是系统管理员的话，所属机构就赋值为当前登录人所属机构id即可
                if(sysAdmin.getIsSuper().equalsIgnoreCase(IsSuperType.IS_SUPER_YES)) {
                    session.setIsSuper(sysAdmin.getIsSuper());
                    session.setOrgId(sysAdmin.getOrgId());
                }else {
                    session.setOrgId(sysAdmin.getOrgId());
                }
            }else{
                session.setSeller(Boolean.TRUE);
                session.setOrgId(sysAdmin.getOrgId());
                // 商家登录，保存商家对应的店铺数据!
                //session.setShopIds(iSellerShopService.findSellerShopIdsByAdminAndOrgId(admin.getId(), admin.getOrgId()));
            }
            AdminRequestUtil.setSession(request, session);
            AdminRequestUtil.setObject(request, list, AdminRequestUtil.FUNCTION_KEY);

            return RespResult.success();
        } catch (IncorrectCredentialsException ice) {
            return RespResult.error("账号或密码错误");
        } catch (Exception e) {
            return RespResult.error(e.getMessage());
        }
    }

    @ApiOperation(value = "注销登录", notes = "注销登录返回登录页面")
    @GetMapping("outlog")
    public RespResult outLogin(HttpServletRequest request) {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        AdminRequestUtil.sessionInvalidate(request);
        return RespResult.success();
    }
}
