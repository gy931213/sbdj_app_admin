package com.sbdj.admin.base;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * <p>
 * 视图控制器
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 15:44
 */
@Api(tags = "视图控制器")
@Controller
public class ViewController {
    // =======================登录模块=======================
    @ApiOperation(value = "登录页面")
    @GetMapping("/login")
    public String login() {return "login";}
    // =======================登录模块=======================

    // =======================首页模块=======================
    @ApiOperation(value = "首页")
    @GetMapping("/")
    public String index() {return "index";}

    @ApiOperation(value = "首页展示")
    @GetMapping("/index")
    public String show() {return "index_v3";}
    // =======================首页模块=======================


    // =======================配置模块=======================
    @ApiOperation(value = "银行卡配置页面")
    @GetMapping("/config/bank")
    public String configBank() {return "config/bank_list";}

    @ApiOperation(value = "平台配置页面")
    @GetMapping("/config/platform")
    public String configPlatform() {return "config/platform_list";}

    @ApiOperation(value = "警告用语页面")
    @GetMapping("/config/warn/message")
    public String configWarnMessage() {return "config/warn_message_list";}

    @ApiOperation(value = "提额配置")
    @GetMapping("/config/quota/setting")
    public String configQuota() {return "config/quota_setting_list";}

    @ApiOperation(value = "七牛配置")
    @GetMapping("/config/qn")
    public String configQiNu() {return "config/qiniu_list";}

    @ApiOperation(value = "阿里云配置")
    @GetMapping("/config/aly")
    public String configALiYun() {return "config/stay_net_time_list";}
    // =======================配置模块=======================

    // =======================财务模块=======================
    @ApiOperation(value = "差价页面")
    @GetMapping("/finance/differ/price")
    public String financeDifferPrice() {return "finance/differ_price_list";}

    @ApiOperation(value = "提现页面")
    @GetMapping("/finance/draw/money/record")
    public String financeDrawMoneyRecord() {return "finance/draw_money_record";}

    @ApiOperation(value = "货款页面")
    @GetMapping("/finance/pay/goods")
    public String financePayGoods() {return "finance/payment_goods_list";}

    @ApiOperation(value = "奖罚页面")
    @GetMapping("/finance/prize/penalty")
    public String financePrizePenalty() {return "finance/prize_penalty_record";}

    @ApiOperation(value = "业务员账单页面")
    @GetMapping("/finance/salesman/bill")
    public String financeSalesmanBill() {return "finance/salesman_bill_list";}

    @ApiOperation(value = "业务员提成页面")
    @GetMapping("/finance/statement")
    public String financeStatement() {return "finance/statement_list";}

    @ApiOperation(value = "银行卡流水页面")
    @GetMapping("/finance/bank/stream")
    public String configBankStream() {return "finance/bank_stream_list";}
    // =======================财务模块=======================

    // =======================信息模块=======================
    @ApiOperation(value = "帮助手册页面")
    @GetMapping("/info/help")
    public String infoHelp() {return "info/help_manual_list";}

    @ApiOperation(value = "系统公告页面")
    @GetMapping("/info/notice")
    public String infoNotice() {return "info/notice_list";}
    // =======================信息模块=======================

    // =======================会员模块=======================
    @ApiOperation(value = "业务员页面")
    @GetMapping("/salesman")
    public String salesman() {return "member/salesman_list";}

    @ApiOperation(value = "号主页面")
    @GetMapping("/salesman/number")
    public String salesmanNumber() {return "member/salesman_number_list";}

    @ApiOperation(value = "业务员等级配置页面")
    @GetMapping("/salesman/level")
    public String salesmanLevel() {return "member/salesman_level_list";}

    @ApiOperation(value = "临时号主页面")
    @GetMapping("/salesman/number/tmp")
    public String salesmanNumberTmp() {return "member/salesman_number_tmp_list";}

    @ApiOperation(value = "业务员额度配置页面")
    @GetMapping("/salesman/royalty")
    public String salesmanRoyalty() {return "member/salesman_royalty_list";}
    // =======================会员模块=======================

    // =======================商家模块=======================
    @ApiOperation(value = "商家页面")
    @GetMapping("/seller")
    public String seller() {return "seller/seller_list";}

    @ApiOperation(value = "店铺页面")
    @GetMapping("/seller/shop")
    public String sellerShop() {return "seller/shops_list";}

    @ApiOperation(value = "商品页面")
    @GetMapping("/seller/shop/goods")
    public String sellerShopGoods() {return "seller/goods_list";}

    @ApiOperation(value = "商品打标")
    @GetMapping("/seller/marking/goods")
    public String markGoods() {return "seller/mark_shop_request";}
    // =======================商家模块=======================

    // =======================系统模块=======================
    @ApiOperation(value = "管理员页面")
    @GetMapping("/sys/admin")
    public String sysAdmin() {
        return "system/admin_list";
    }

    @ApiOperation(value = "管理员资料展示")
    @GetMapping("/sys/admin/info")
    public String sysAdminInfoView() {return "system/admin_info";}

    @ApiOperation(value = "API调用页面")
    @GetMapping("/sys/admin/api")
    public String sysAdminInfo() {
        return "system/sys_admin_api";
    }

    @ApiOperation(value = "系统配置页面")
    @GetMapping("/sys/config")
    public String sysConfig() {
        return "system/config_list";
    }

    @ApiOperation(value = "权限页面")
    @GetMapping("/sys/function")
    public String sysFunction() {
        return "system/function_list";
    }

    @ApiOperation(value = "组织页面")
    @GetMapping("/organization")
    public String organization() {
        return "system/organization_list";
    }

    @ApiOperation(value = "角色页面")
    @GetMapping("/sys/role")
    public String sysRole() {
        return "system/role_list";
    }
    // =======================系统模块=======================

    // =======================任务模块=======================
    @ApiOperation(value = "任务页面")
    @GetMapping("/task")
    public String task() {
        return "task/task_list";
    }

    @ApiOperation(value = "任务审核用语页面")
    @GetMapping("/task/common/sentence")
    public String taskCommonSentence() {
        return "task/task_common_sentence";
    }

    @ApiOperation(value = "任务传图设置页面")
    @GetMapping("/task/picture/type/setting")
    public String taskPictureTypeSetting() {return "task/task_picture_type_setting";}

    @ApiOperation(value = "任务分配页面")
    @GetMapping("/task/setting")
    public String TaskSetting() {
        return "task/task_setting";
    }

    @ApiOperation(value = "申诉任务页面")
    @GetMapping("/task/son/appeal")
    public String taskSonAppeal() {return "task/task_son_appeal_list";}

    @ApiOperation(value = "任务详情页面")
    @GetMapping("/task/son")
    public String taskSon() {
        return "task/task_son_list";
    }

    @ApiOperation(value = "作废任务页面")
    @GetMapping("/task/son/nullify")
    public String taskSonNullify() {return "task/task_son_nullify_list";}

    @ApiOperation(value = "任务模板页面")
    @GetMapping("/task/template")
    public String taskTemplate() {return "task/task_template_list";}

    @ApiOperation(value = "未完成任务页面")
    @GetMapping("/task/undone")
    public String taskSonUndone() {
        return "task/task_undone_list";
    }
    // =======================任务模块=======================
}
