package com.sbdj.admin.configure;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.ExcelUtil;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.admin.query.BankStreamQuery;
import com.sbdj.service.configure.admin.vo.BankStreamVo;
import com.sbdj.service.configure.entity.BankStream;
import com.sbdj.service.configure.entity.ConfigBank;
import com.sbdj.service.configure.service.IBankStreamService;
import com.sbdj.service.configure.service.IConfigBankService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;

/**
 * <p>
 * 银行流水表 前端控制器
 * </p>
 *
 * @author Zc
 * @since 2019-11-08
 */
@Api(tags = "银行流水表控制器")
@RestController
@RequestMapping("/bank/stream")
public class BankStreamController {

    @Autowired
    private IBankStreamService iBankStreamService;

    @Autowired
    private IConfigBankService iConfigBankService;

    @ApiOperation(value = "获取银行流水", notes = "获取银行流水")
    @RequestMapping("list")
    public RespResult<PageData<BankStreamVo>> findBankStreamPage(HttpServletRequest request, BankStreamQuery query, HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<BankStreamVo> page = iBankStreamService.findBankStreamExtPage(query, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "新增银行流水", notes = "新增银行流水")
    @PostMapping("save")
    public RespResult saveBankStream(HttpServletRequest request, BankStream stream) {
        stream.setOrgId(AdminRequestUtil.orgId(request));
        try {
            // 获取银行卡数据!
            ConfigBank bank = iConfigBankService.getById(stream.getBankId());
            if (StrUtil.isNull(bank)) {
                return RespResult.error("银行卡不存在！");
            }
            // 判断流水 if 支出 , else 收入
            if (StrUtil.isNotNull(stream.getDefray()) && stream.getDefray().intValue() > 0) {
				/*if(bank.getBalance().compareTo(stream.getDefray()) == -1) {
					return RespResult.error("当前银行卡余额不足【支出失败】！");
				}*/
                stream.setBalance(bank.getBalance().subtract(stream.getDefray()));
            } else if (StrUtil.isNotNull(stream.getEarning()) && stream.getEarning().intValue() > 0) {
                stream.setBalance(bank.getBalance().add(stream.getEarning()));
            } else {
                return RespResult.error("亲您输对了吗？");
            }
            // 保存银行流水!
            iBankStreamService.saveBankStream(stream);
            // 更新银行卡余额!
            bank.setBalance(stream.getBalance());
            iConfigBankService.updateBank(bank);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "修改银行流水", notes = "修改银行流水")
    @PostMapping("update")
    public RespResult updateBankStream(HttpServletRequest request, Long id, String param, String field) {
        try {
            iBankStreamService.updateBankStream(id, param, AdminRequestUtil.identify(request), field);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success("更新成功");
    }

    @ApiOperation(value = "逻辑删除银行流水", notes = "逻辑删除银行流水")
    @PostMapping("delete")
    public RespResult logicDelete(HttpServletRequest request, Long id) {
        try {
            // 获取银行卡流水!
            BankStream bs = iBankStreamService.getById(id);
            if (StrUtil.isNull(bs)) {
                return RespResult.error("当前流水不存在！");
            }
            // 获取银行卡数据!
            ConfigBank bank = iConfigBankService.getById(bs.getBankId());
            if (StrUtil.isNull(bank)) {
                return RespResult.error("银行卡不存在！");
            }

            BigDecimal balance = null;
            // 判断流水 if 支出 , else 收入
            if (StrUtil.isNotNull(bs.getDefray()) && bs.getDefray().intValue() > 0) {
                // 删除【支出】流水，将支出金额返回对应的银行卡余额中！
                balance = bank.getBalance().add(bs.getDefray());
            } else if (StrUtil.isNotNull(bs.getEarning()) && bs.getEarning().intValue() > 0) {
                // 删除【收入】流水，将收入金额从对应的银行卡余额中减除对应的收入金额！
				/*if(bank.getBalance().compareTo(bs.getEarning()) == -1) {
					return RespResult.error("当前银行卡余额不足，【收入流水】删除失败！");
				}*/
                balance = bank.getBalance().subtract(bs.getEarning());
            } else {
                return RespResult.error("亲您输对了吗？");
            }
            iBankStreamService.logicDeleteBankStream(id, Status.STATUS_DELETE, AdminRequestUtil.identify(request));
            // 更新银行卡余额!
            bank.setBalance(balance);
            iConfigBankService.updateBank(bank);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @RequestMapping("import")
    public RespResult importBankStream(HttpServletRequest request, @RequestParam("file") MultipartFile file, Long bankId) {
        try {
            if(null == bankId || bankId.equals(Number.LONG_ZERO)) {
                return RespResult.error("请选择一项重新导入！");
            }
            List<String[]> result = ExcelUtil.readExcel(file);
            if (result.size() <= 0) {
                return RespResult.error("数据为空！");
            }
            if(!checkIsBankStream(result)) {
                return RespResult.error("不是银行流水数据！");
            }
            iBankStreamService.saveAllBankStream(result,bankId,AdminRequestUtil.orgId(request));
        } catch (Exception e) {
            return RespResult.error(e.getMessage());
        }
        return RespResult.success();
    }

    //method = RequestMethod.GET将数据传递给前端
    @RequestMapping(value="download/excel", method = RequestMethod.GET)
    public void downloadExcel(HttpServletResponse response, HttpServletRequest request)throws IOException {
        // 获取输入流，原始模板位置
        //String  filePath = getClass().getResource("/templates/excel/bankStream.xls" ).getPath();
        //InputStream bis = new BufferedInputStream(new FileInputStream(new File(filePath)));
        InputStream bis = getClass().getClassLoader().getResourceAsStream("templates/excel/bankStream.xls");

        if (StrUtil.isNull(bis)) {
            return;
        }

        // 假如以中文名下载的话，设置下载文件名称
        String filename = "银行流水导入模板.xls";
        // 转码，免得文件名中文乱码
        filename = URLEncoder.encode(filename,"UTF-8");
        //设置文件下载头
        response.addHeader("Content-Disposition", "attachment;filename=" + filename);
        //1.设置文件ContentType类型，这样设置，会自动判断下载文件类型
        response.setContentType("multipart/form-data");
        BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
        int len = 0;
        while((len = bis.read()) != -1){
            out.write(len);
            out.flush();
        }
        out.close();
    }

    /**
     * 检查是否是货款数据
     * @param result 待检查数据
     * @return
     */
    private boolean checkIsBankStream(List<String[]> result) {
        String[] array = result.get(0);
        if(null == array) {
            return false;
        }
        if(array.length >1) {
            return array[0].equals("日期") && array[1].equals("姓名") && array[2].equals("收入") && array[3].equals("支出") && array[4].equals("商家");
        }
        return true;
    }

}
