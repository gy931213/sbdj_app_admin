package com.sbdj.admin.configure;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.configure.admin.query.ConfigBankQuery;
import com.sbdj.service.configure.admin.vo.ConfigBankVo;
import com.sbdj.service.configure.entity.ConfigBank;
import com.sbdj.service.configure.service.IConfigBankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 银行卡配置 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@RestController
@RequestMapping("/config/bank")
public class ConfigBankController {
    @Autowired
    private IConfigBankService iBankService;

    @RequestMapping(value="list")
    public RespResult<PageData<ConfigBankVo>> findBankPageList(HttpServletRequest request, ConfigBankQuery query, HttpReqParam reqParam){
        if(!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<ConfigBankVo> page = iBankService.findBankPageList(query, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @RequestMapping(value="save")
    public RespResult saveBank(HttpServletRequest request, ConfigBank bank) {
        try {
            bank.setCreateId(AdminRequestUtil.identify(request));
            iBankService.saveBank(bank);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @RequestMapping(value="update")
    public RespResult updateBank(ConfigBank bank) {
        try {
            iBankService.updateBank(bank);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @RequestMapping(value="delete")
    public RespResult deleteBank(Long id) {
        try {
            iBankService.logicDeleteBank(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @RequestMapping(value="deletes")
    public RespResult deleteBanks(String ids) {
        try {
            iBankService.logicDeleteBanks(ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @RequestMapping(value="select")
    public RespResult findBankSelect(HttpServletRequest request) {
        List<ConfigBank> list = null;
        if(!AdminRequestUtil.isSuper(request)) {
            list = iBankService.findBankList(AdminRequestUtil.orgId(request));
        }else {
            list = iBankService.findBankList(null);
        }
        return RespResult.success(list);
    }

    @RequestMapping(value="selects")
    public RespResult findBankSelects(HttpServletRequest request,Long orgId) {
        List<ConfigBank> list  = iBankService.findBankList(orgId);
        return RespResult.success(list);
    }
}
