package com.sbdj.admin.configure;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 计算公式配置 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@RestController
@RequestMapping("/config/formula")
public class ConfigFormulaController {

}
