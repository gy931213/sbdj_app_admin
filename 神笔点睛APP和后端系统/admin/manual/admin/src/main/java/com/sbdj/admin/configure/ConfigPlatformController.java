package com.sbdj.admin.configure;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.configure.entity.ConfigPlatform;
import com.sbdj.service.configure.service.IConfigPlatformService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 平台表配置 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Api(tags = "平台控制器")
@RestController
@RequestMapping("/config/platform")
public class ConfigPlatformController {
    @Autowired
    private IConfigPlatformService iConfigPlatformService;

    @ApiOperation(value = "平台列表", notes = "平台列表")
    @RequestMapping(value="list", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult<PageData<ConfigPlatform>> findPlatformPageList(HttpReqParam reqParam) {
        IPage<ConfigPlatform> page = iConfigPlatformService.findPlatformPageList(reqParam.getKeyword(), PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "平台信息", notes = "平台信息")
    @RequestMapping(value="select", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult<List<ConfigPlatform>> findPlatformSelect() {
        List<ConfigPlatform> list= iConfigPlatformService.findPlatformList();
        return RespResult.success(list);
    }

    @ApiOperation(value = "保存平台", notes = "保存平台")
    @PostMapping(value="save")
    public RespResult savePlatform(HttpServletRequest request, ConfigPlatform platform) {
        try {
            platform.setCreateId(AdminRequestUtil.identify(request));
            iConfigPlatformService.savePlatform(platform);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新平台", notes = "更新平台")
    @PostMapping(value="update")
    public RespResult updatePlatform(ConfigPlatform platform) {
        try {
            iConfigPlatformService.updatePlatform(platform);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "删除平台", notes = "删除平台")
    @PostMapping(value="delete")
    public RespResult deletePlatform(@ApiParam(value = "平台id") Long id) {
        try {
            iConfigPlatformService.logicDeletePlatformById(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "批量删除平台", notes = "批量删除平台")
    @PostMapping(value="deletes")
    public RespResult deletesPlatforms(@ApiParam(value = "平台ids") String ids) {
        try {
            iConfigPlatformService.logicDeletePlatformByIds(ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }
}
