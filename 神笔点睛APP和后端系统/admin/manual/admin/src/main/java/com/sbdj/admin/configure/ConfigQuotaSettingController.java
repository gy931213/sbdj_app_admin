package com.sbdj.admin.configure;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.configure.admin.vo.ConfigQuotaSettingVo;
import com.sbdj.service.configure.entity.ConfigQuotaSetting;
import com.sbdj.service.configure.service.IConfigQuotaSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 提现额度配置 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
@RestController
@RequestMapping("/config/quota/setting")
public class ConfigQuotaSettingController {
    @Autowired
    private IConfigQuotaSettingService iQuotaSettingService;

    @PostMapping("list")
    public RespResult<PageData<ConfigQuotaSettingVo>> quotaSettingPageList(HttpServletRequest request, Long orgId, HttpReqParam reqParam) {
        if(!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        IPage<ConfigQuotaSettingVo> page = iQuotaSettingService.findQuotaSettingPage(orgId, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @PostMapping("save")
    public RespResult saveQuotaSetting(ConfigQuotaSetting quotaSetting) {
        try {
            iQuotaSettingService.saveQuotaSetting(quotaSetting);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @PostMapping("update")
    public RespResult updateQuotaSetting(ConfigQuotaSetting quotaSetting) {
        try {
            iQuotaSettingService.updateQuotaSetting(quotaSetting);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @PostMapping("delete")
    public RespResult deleteQuotaSetting(Long id) {
        try {
            iQuotaSettingService.logicDeleteQuotaSetting(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @PostMapping("deletes")
    public RespResult deleteQuotaSettings(String ids) {
        try {
            iQuotaSettingService.logicDeleteQuotaSettingByIds(ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }
}
