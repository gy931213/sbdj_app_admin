package com.sbdj.admin.configure;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.configure.entity.ConfigWarnMessage;
import com.sbdj.service.configure.service.IConfigWarnMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 配置警告信息表 前端控制器
 * </p>
 *
 * @author Zc
 * @since 2019-11-08
 */
@Api(tags = "配置警告控制器")
@RestController
@RequestMapping("/warn/message")
public class ConfigWarnMessageController {

    @Autowired
    private IConfigWarnMessageService iConfigWarnMessageService;

    @ApiOperation(value = "获取全部警告信息配置",notes = "获取全部警告信息配置")
    @PostMapping("list")
    public RespResult<PageData<ConfigWarnMessage>> warnMessageList(HttpServletRequest request, Long orgId, HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        IPage<ConfigWarnMessage> page = PageRequestUtil.buildPageRequest(reqParam);

        page=iConfigWarnMessageService.findWarnMessagePageList(orgId, page);
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "新增警告信息配置",notes = "新增警告信息配置")
    @PostMapping("save")
    public RespResult saveWarnMessage(ConfigWarnMessage warnMessage) {
        try {
            //
            ConfigWarnMessage warnMessage2 = iConfigWarnMessageService.findWarnMessageOne(warnMessage.getOrgId(), warnMessage.getPlatformId());
            if (null != warnMessage2) {
                return RespResult.error("该数据已存在");
            }

            iConfigWarnMessageService.savaWarnMessage(warnMessage);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "修改警告信息配置",notes = "修改警告信息配置")
    @PostMapping("update")
    public RespResult updateWarnMessage(ConfigWarnMessage warnMessage) {
        try {

            ConfigWarnMessage warnMessage2 = iConfigWarnMessageService.findWarnMessageOne(warnMessage.getOrgId(), warnMessage.getPlatformId());
            if (null != warnMessage2 && !warnMessage.getId().equals(warnMessage2.getId()) && warnMessage.getOrgId().equals(warnMessage2.getOrgId()) && warnMessage.getPlatformId().equals(warnMessage2.getPlatformId())) {
                return RespResult.error("该数据已存在");
            }

            iConfigWarnMessageService.updateWarnMessage(warnMessage);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "删除警告信息配置",notes = "删除警告信息配置")
    @PostMapping("delete")
    public RespResult deleteWarnMessage(Long id) {
        try {
            iConfigWarnMessageService.removeById(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "批量删除警告信息配置",notes = "批量删除警告信息配置")
    @PostMapping("deletes")
    public RespResult deleteWarnMessages(String ids) {
        String[] strings = ids.split(",");
        List<String> arrayList = new ArrayList<>();
        for (int i = 0;i<strings.length;i++){
            arrayList.add(i,strings[i]);
        }
        try {
            iConfigWarnMessageService.removeByIds(arrayList);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

}
