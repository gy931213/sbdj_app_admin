package com.sbdj.admin.configure;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.configure.entity.ConfigQiniu;
import com.sbdj.service.configure.service.IQiNiuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 七牛控制器
 * </p>
 *
 * @author Zc
 * @since 2019-11-28
 */
@Api(tags = "七牛控制器")
@RestController
@RequestMapping(value = "/qn")
public class QiNiuController {

    @Autowired
    private IQiNiuService iQiNiuService;

    @ApiOperation(value = "获取全部七牛配置",notes = "获取全部七牛配置")
    @RequestMapping("list")
    public RespResult<PageData<ConfigQiniu>> findQiNiuList(HttpReqParam reqParam) {
        try {
        IPage<ConfigQiniu> page = iQiNiuService.findQiNiuPage(PageRequestUtil.buildPageRequest(reqParam));
         return RespResult.buildPageData(page);
        } catch (Exception e) {
            return RespResult.error();
        }

    }

    @ApiOperation(value = "删除指定七牛配置",notes = "删除指定七牛配置")
    @PostMapping("delete")
    public RespResult deleteQiNiuByid(Long id) {
        try {
            iQiNiuService.deleteQiNiu(id);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "修改指定七牛配置",notes = "修改指定七牛配置")
    @PostMapping("update")
    public RespResult updateQiNiuByid(ConfigQiniu configQiniu) {
        try {
            iQiNiuService.updateQiNiu(configQiniu);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "新增七牛配置",notes = "新增七牛配置")
    @PostMapping("save")
    public RespResult saveNiuByid(ConfigQiniu configQiniu) {
        try {
            iQiNiuService.saveQiNiu(configQiniu);
            return RespResult.success();
        }catch (Exception e) {
            return RespResult.error();
        }
    }

}
