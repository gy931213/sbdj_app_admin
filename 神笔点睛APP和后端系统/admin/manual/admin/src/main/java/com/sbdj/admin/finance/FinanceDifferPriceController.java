package com.sbdj.admin.finance;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.finance.admin.vo.DifferPriceVo;
import com.sbdj.service.finance.admin.query.DifferPriceQuery;
import com.sbdj.service.finance.entity.FinanceDifferPrice;
import com.sbdj.service.finance.service.IFinanceDifferPriceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Api(tags = "子任务差价表控制器")
@RestController
@RequestMapping("/differ/price")
public class FinanceDifferPriceController {
    private static Logger logger = LoggerFactory.getLogger(FinanceDifferPriceController.class);

    @Autowired
    private IFinanceDifferPriceService iFinanceDifferPriceService ;



    @RequestMapping("save")
   @ApiOperation(value = "保存差价",notes = "保存差价")
    public RespResult saveDifferPrice(@ApiParam(value = "差价类对象") FinanceDifferPrice differPrice) {
        try {
            iFinanceDifferPriceService.saveDifferPrice(differPrice);
        } catch (Exception e) {
            logger.error("差价 保存失败");
            return RespResult.error();
        }
        return RespResult.success();
    }

    @RequestMapping("delete")
   @ApiOperation(value = "删除差价信息",notes = "删除差价信息")
    public RespResult deleteDifferPrice(@ApiParam(value = "差价信息ID") Long id) {
        try {
            iFinanceDifferPriceService.deleteDifferPrice(id);
        } catch (Exception e) {
            logger.error("差价 删除失败 id " + id);
            return RespResult.error();
        }
        return RespResult.success();
    }

    @RequestMapping("update")
   @ApiOperation(value = "修改差价信息",notes = "修改差价信息")
    public RespResult updateDifferPrice(@ApiParam(value = "差价类") FinanceDifferPrice differPrice) {
        try {
            iFinanceDifferPriceService.updateDifferPrice(differPrice);
        } catch (Exception e) {
            logger.error("差价 更新失败 id " + differPrice.getId());
            return RespResult.error();
        }
        return RespResult.success();
    }

   // @AuthValidate(AuthCode.DifferPriceAuditor)
    @RequestMapping("auditor")
   @ApiOperation(value = "审核差价",notes = "审核差价")
    public RespResult auditorDifferPrice(@ApiParam(value = "http请求信息")HttpServletRequest request,@ApiParam(value = "ID") Long id
            ,@ApiParam(value = "状态") String status,@ApiParam(value = "") String onlineid) {
        if (StrUtil.isNull(id)) {
            logger.error("所传id为空");
            return RespResult.error();
        }
        if (StrUtil.isNull(status)) {
            logger.error("所传状态为空");
            return RespResult.error();
        }
        try {
            iFinanceDifferPriceService.reviewDifferPrice(id, Status.STATUS_NO, status, AdminRequestUtil.identify(request), onlineid);
        } catch (Exception e) {
            logger.error("差价 审核失败 id " + id);
            return RespResult.error(e.getMessage());
        }
        return RespResult.success();
    }

   // @AuthValidate(AuthCode.DifferPriceAuditors)
    @RequestMapping("auditors")
   @ApiOperation(value = "",notes = "")
    public RespResult auditorDifferPrices(@ApiParam(value = "http请求信息")HttpServletRequest request
            , @ApiParam(value = "批量ID") String ids,@ApiParam(value = "状态") String status) {
        if (StrUtil.isNull(ids)) {
            logger.error("所传id为空");
            return RespResult.error();
        }
        if (StrUtil.isNull(status)) {
            logger.error("所传状态为空");
            return RespResult.error();
        }
        // 获取差价集合
        List<DifferPriceVo> list = iFinanceDifferPriceService.findDifferPriceByIds(ids);
        // 差价集合判空
        if (0 == list.size()) {
            logger.error("所传的id未能检索到数据");
            return RespResult.error();
        }
        // 获取审核人
        Long oprId = AdminRequestUtil.identify(request);
        // 计数
        int count = 0;
        for (DifferPriceVo differPriceExt : list) {
            try {
                iFinanceDifferPriceService.reviewDifferPrice(differPriceExt.getId(), differPriceExt.getStatus(), status, oprId, differPriceExt.getOnlineid());
            } catch (Exception e) {
                logger.error("差价 审核失败 id " + differPriceExt.getId());
                count++;
            }
        }
        return RespResult.success(count);
    }

    @RequestMapping("list")
   @ApiOperation(value = "显示差价信息列表",notes = "显示差价信息列表")
    public RespResult findDifferPrice(@ApiParam(value = "http请求参数") HttpServletRequest request
            ,@ApiParam(value = "差价查询参数类") DifferPriceQuery params,@ApiParam(value = "分页信息") HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            params.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<DifferPriceVo> page = iFinanceDifferPriceService.findDifferPriceByPage(params, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }
}
