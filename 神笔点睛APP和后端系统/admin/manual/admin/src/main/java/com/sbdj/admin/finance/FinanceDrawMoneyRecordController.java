package com.sbdj.admin.finance;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.ExcelUtil;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.RequestUtil;
import com.sbdj.service.finance.admin.query.DrawMoneyRecordQuery;
import com.sbdj.service.finance.admin.vo.DrawMoneyRecordVo;
import com.sbdj.service.finance.service.IFinanceDrawMoneyRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * <p>
 * 财务模块-业务员提现记录 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "业务员提现记录控制器")
@RestController
@RequestMapping("/draw/money/record")
public class FinanceDrawMoneyRecordController {

    @Lazy
    @Autowired
    private IFinanceDrawMoneyRecordService iFinanceDrawMoneyRecordService;

    @ApiOperation(value = "获取业务员提现记录", notes = "获取业务员提现记录")
    @RequestMapping("list")
    public RespResult<PageData<DrawMoneyRecordVo>> findDrawMoneyRecord(HttpServletRequest request, DrawMoneyRecordQuery params, HttpReqParam reqParam) {
            if (!AdminRequestUtil.isSuper(request)) {
                params.setOrgId(AdminRequestUtil.orgId(request));
            }
            IPage<DrawMoneyRecordVo> page = iFinanceDrawMoneyRecordService.findDrawMoneyRecordPage(params, PageRequestUtil.buildPageRequest(reqParam));
            return RespResult.buildPageData(page);

    }

    @RequestMapping("payment")
    public RespResult updateDrawMoneyRecordStatus(HttpServletRequest request, Long id) {
        try {
            iFinanceDrawMoneyRecordService.updateDrawMoneyRecordStatusById(id, AdminRequestUtil.identify(request), Status.STATUS_ALREADY_PAY);
			/*DrawMoneyRecord drawMoneyRecordOne = iDrawMoneyRecordService.findDrawMoneyRecordOne(id);
			Salesman salesmanOne = iSalesmanService.findSalesmanOne(drawMoneyRecordOne.getSalesmanId());
			iSalesmanService.updatesalesmanBrokerage(drawMoneyRecordOne.getSalesmanId(), salesmanOne.getBalance().subtract(drawMoneyRecordOne.getApplyMoney()));*/
            return RespResult.success("更新成功");
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @RequestMapping("balance")
    public RespResult balance(MultipartFile file, HttpServletRequest request) {
        try {
            List<String[]> result = ExcelUtil.readExcel(file);
            if(result.size() <= 0) {
                return RespResult.error("数据为空");
            }
            // ~判断格式是否合格
            if(!checkDrawMoneyRecord(result)) {
                return RespResult.error("不是提现数据");
            }
            // ~
            iFinanceDrawMoneyRecordService.batchCashWithdrawal(AdminRequestUtil.identify(request),result);
        } catch (IOException e) {
            return RespResult.error(e.getMessage());
        }
        return RespResult.success();
    }

    /**
     * 检查是否是提现数据
     * @param result 待检查数据
     * @return
     */
    public boolean checkDrawMoneyRecord(List<String[]> result) {
        String[] array = result.get(0);
        if(null == array) {
            return false;
        }

        if(array.length > 0) {
            return array[0].equals("提现ID");
        }
        return true;
    }

}
