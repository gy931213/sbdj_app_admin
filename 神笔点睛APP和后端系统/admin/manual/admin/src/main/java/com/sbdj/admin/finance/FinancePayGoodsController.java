package com.sbdj.admin.finance;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.ExcelUtil;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.finance.admin.vo.PayGoodsVo;
import com.sbdj.service.finance.admin.query.PayGoodsQuery;
import com.sbdj.service.finance.service.IFinancePayGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;


@Api(tags = "贷款列表控制器")
@RestController
@RequestMapping("/pay/goods")
public class FinancePayGoodsController {
    
    @Autowired
    private IFinancePayGoodsService iFinancePayGoodsService;
    

    @ApiOperation(value = "显示贷款列表",notes = "根据查询条件显示查询列表")
    @RequestMapping("list")
    //@AuthValidate(AuthCode.PayGoodsList)
    public RespResult<PageData<PayGoodsVo>> findPayGoodsPageList(@ApiParam(value = "http请求参数") HttpServletRequest request
            , @ApiParam(value = "贷款列表查询参数") PayGoodsQuery params, @ApiParam(value = "分页参数") HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            params.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<PayGoodsVo> page =iFinancePayGoodsService.findPayGoodsPageList(params, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "贷款支付",notes = "贷款支付")
    @RequestMapping("payment")
    //@AuthValidate(AuthCode.PayGoodsPayment)
    public RespResult updatePayGoodsStatusById(@ApiParam(value = "http请求参数") HttpServletRequest request, @ApiParam(value = "贷款ID") Long id) {
        try {
            iFinancePayGoodsService.updatePayGoodsStatus(id, AdminRequestUtil.identify(request), Status.STATUS_ALREADY_PAY);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "",notes = "")
    @RequestMapping("balance")
    //@AuthValidate(AuthCode.PayGoodsBalance)
    public RespResult balance(MultipartFile file,@ApiParam(value = "http请求参数") HttpServletRequest request) {
        try {
            List<String[]> result = ExcelUtil.readExcel(file);
            if (result.size() <= 0) {
                return RespResult.error("数据为空");
            }
            // ~判断格式是否合格
            if (!checkIsPayGoods(result)) {
                return RespResult.error("不是货款数据");
            }
            // ~
            iFinancePayGoodsService.payGoodsBalance(AdminRequestUtil.identify(request), result);
        } catch (IOException e) {
            return RespResult.error(e.getMessage());
        }
        return RespResult.success();
    }

    /**
     * 检查是否是货款数据
     *
     * @param result 待检查数据
     * @return
     */
    public boolean checkIsPayGoods(List<String[]> result) {
        String[] array = result.get(0);
        if (null == array) {
            return false;
        }

        if (array.length > 1) {
            return array[0].equals("货款ID") && array[1].equals("货款单号");
        } else {
            return array[0].equals("货款ID");
        }
    }
    
    @ApiOperation(value = "删除数据",notes = "物理删除数据")
    @PostMapping("/deletes")
    public RespResult deleteTask(String ids) {
        if (StrUtil.isNull(ids)) {
            return RespResult.error("缺少参数！");
        }
        try {
            iFinancePayGoodsService.deletesPayGoods(ids);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }
}
