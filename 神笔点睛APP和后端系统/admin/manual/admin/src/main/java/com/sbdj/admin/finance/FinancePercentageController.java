package com.sbdj.admin.finance;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.finance.admin.query.PercentageQuery;
import com.sbdj.service.finance.admin.vo.PercentageVo;
import com.sbdj.service.finance.service.IFinancePercentageService;
import com.sbdj.service.member.service.ISalesmanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 业务员提成明细表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */

@Api(tags = "业务员提成明细控制器")
@RestController
@RequestMapping("/statement")
public class FinancePercentageController {

    @Autowired
    private IFinancePercentageService iFinancePercentageService;

    @Autowired
    private ISalesmanService iSalesmanService;


    @ApiOperation(value = "获取业务员提成明细数据", notes = "获取业务员提成明细数据")
    @RequestMapping("list")
    public RespResult<PageData<PercentageVo>> findPercentagePage(HttpServletRequest request, PercentageQuery params, HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            params.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<PercentageVo> page = iFinancePercentageService.findPercentagePage(params, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }


    @ApiOperation(value = "生成提成数据，根据选中的日期生成!", notes = "生成提成数据，根据选中的日期生成!")
    @RequestMapping("generate")
    public RespResult generatePercentage(String date, String jobNumber) {
        try {
            iSalesmanService.computeTaskPercentageToSalesmanBalance(date, jobNumber);
        } catch (Exception e) {
            return RespResult.error(e.getMessage());
        }
        return RespResult.success();
    }
}
