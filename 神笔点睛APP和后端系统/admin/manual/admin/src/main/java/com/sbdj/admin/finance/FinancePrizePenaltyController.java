package com.sbdj.admin.finance;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.finance.admin.vo.PrizePenaltyVo;
import com.sbdj.service.finance.admin.query.PrizePenaltyQuery;
import com.sbdj.service.finance.service.IFinancePrizePenaltyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 财务模块-业务员奖惩表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "业务员奖惩控制器")
@RestController
@RequestMapping("/prize/penalty")
public class FinancePrizePenaltyController {


    @Autowired
    private IFinancePrizePenaltyService iFinancePrizePenaltyService;

    @ApiOperation(value = "显示奖惩列表",notes = "显示奖惩列表")
    @RequestMapping("list")
    public RespResult findPrizePenalty(PrizePenaltyQuery params, HttpReqParam reqParam, HttpServletRequest request) {
        if (!AdminRequestUtil.isSuper(request)) {
            params.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<PrizePenaltyVo> page = iFinancePrizePenaltyService.findPrizePenaltyRecordPage(params, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }
}
