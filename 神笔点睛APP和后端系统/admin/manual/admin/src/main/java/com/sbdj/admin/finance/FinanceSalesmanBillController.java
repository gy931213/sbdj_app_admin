package com.sbdj.admin.finance;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.finance.admin.query.SalesmanBillQuery;
import com.sbdj.service.finance.admin.vo.SalesmanBillVo;
import com.sbdj.service.finance.service.IFinanceSalesmanBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 财务模块-(业务员账单) 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@RestController
@RequestMapping("/finance/salesman/bill")
public class FinanceSalesmanBillController {
    @Autowired
    private IFinanceSalesmanBillService iFinanceSalesmanBillService;

    @GetMapping("/list")
    public RespResult<PageData<SalesmanBillVo>> findSalesmanBillByPage(HttpServletRequest request, SalesmanBillQuery query, HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<SalesmanBillVo> page = iFinanceSalesmanBillService.findSalesmanBillByPage(query, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }
}
