package com.sbdj.admin.finance;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 佣金流水表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@RestController
@RequestMapping("/finance/statement")
public class FinanceStatementController {

}
