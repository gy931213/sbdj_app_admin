package com.sbdj.admin.information;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.information.admin.vo.InfoHelpVo;
import com.sbdj.service.information.entity.InfoHelp;
import com.sbdj.service.information.service.IInfoHelpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 帮助手册 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "帮助手册")
@RestController
@RequestMapping("/info/help")
public class InfoHelpController {
    @Autowired
    private IInfoHelpService iInfoHelpService;

    @ApiOperation(value = "展示帮助手册", notes = "展示帮助手册")
    @GetMapping(value="list")
    public RespResult<PageData<InfoHelpVo>> loadLis(HttpServletRequest request, HttpReqParam reqParam, @ApiParam(value = "机构id") Long orgId){
        if(!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        IPage<InfoHelpVo> page = iInfoHelpService.findHelpPageList(reqParam.getKeyword(),orgId, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "更新帮助手册", notes = "更新帮助手册")
    @PostMapping(value="update")
    public RespResult updateHelp(InfoHelp help) {
        try {
            iInfoHelpService.updateHelp(help);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }


    @ApiOperation(value = "删除帮助手册", notes = "删除帮助手册")
    @PostMapping(value="delete")
    public RespResult updateDelete(@ApiParam(value = "帮助手册id") Long id) {
        try {
            iInfoHelpService.logicDeleteHelp(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "批量删除帮助手册", notes = "批量删除帮助手册")
    @PostMapping(value="deletes")
    public RespResult updateDeletes(@ApiParam(value = "帮助手册ids 1,2,3...") String ids) {
        try {
            iInfoHelpService.logicDeleteHelps(ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新帮助手册状态", notes = "更新帮助手册状态")
    @PostMapping(value="status")
    public RespResult updateHelpStatus(@ApiParam(value = "id") Long id,@ApiParam(value = "状态 {E:启用 D: 删除}") String status) {
        try {
            iInfoHelpService.updateHelpStatus(id, status);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "保存帮助手册", notes = "保存帮助手册")
    @PostMapping(value="save")
    public RespResult saveHelp(HttpServletRequest request,InfoHelp help) {
        try {
            help.setCreateId(AdminRequestUtil.identify(request));
            iInfoHelpService.saveHelp(help);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }
}
