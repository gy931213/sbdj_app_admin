package com.sbdj.admin.information;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.information.admin.vo.InfoNoticeVo;
import com.sbdj.service.information.entity.InfoNotice;
import com.sbdj.service.information.service.IInfoNoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 公告 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "公告")
@RestController
@RequestMapping("/info/notice")
public class InfoNoticeController {
    @Autowired
    private IInfoNoticeService iInfoNoticeService;

    @ApiOperation(value = "展示公告", notes = "展示公告")
    @GetMapping(value="list")
    public RespResult<PageData<InfoNoticeVo>> loadLis(HttpServletRequest request, HttpReqParam reqParam, Long orgId){
        if(!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        IPage<InfoNoticeVo> page = iInfoNoticeService.findNoticePageList(reqParam.getKeyword(),orgId, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "更新公告", notes = "展示公告")
    @PostMapping(value="update")
    public RespResult updateNotice(InfoNotice notice) {
        try {
            iInfoNoticeService.updateNotice(notice);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "展示公告", notes = "展示公告")
    @PostMapping(value="delete")
    public RespResult updateDelete(Long id) {
        try {
            iInfoNoticeService.logicDeleteNotice(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "展示公告", notes = "更新公告")
    @PostMapping(value="deletes")
    public RespResult updateDeletes(String ids) {
        try {
            iInfoNoticeService.logicDeleteNotices(ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新公告状态", notes = "更新公告状态")
    @RequestMapping(value="status")
    public RespResult updateNoticeStatus(Long id,String status) {
        try {
            iInfoNoticeService.updateNoticeStatus(id, status);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "保存公告", notes = "保存公告")
    @PostMapping(value="save")
    public RespResult saveNotice(HttpServletRequest request,InfoNotice notice) {
        try {
            notice.setCreateId(AdminRequestUtil.identify(request));
            iInfoNoticeService.saveNotice(notice);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

}
