package com.sbdj.admin.member;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.PlatformTypeValue;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.*;
import com.sbdj.service.finance.entity.FinancePrizePenalty;
import com.sbdj.service.finance.service.IFinancePrizePenaltyService;
import com.sbdj.service.member.admin.query.TBInfo;
import com.sbdj.service.member.admin.query.TBQuery;
import com.sbdj.service.member.admin.vo.SalesmanImagesVo;
import com.sbdj.service.member.admin.vo.SalesmanVo;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.member.service.ISalesmanImagesService;
import com.sbdj.service.member.service.ISalesmanNumberService;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.system.admin.vo.AreaVo;
import com.sbdj.service.system.service.ISysAdminApiService;
import com.sbdj.service.system.type.AreaType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 业务员表 前端控制器
 * </p>
 *
 * @author ZC
 * @since 2019-11-22
 */
@Api(tags = "业务员控制器")
@RestController
@RequestMapping("/salesman")
public class SalesmanController  {
    private Logger logger = LoggerFactory.getLogger(SalesmanController.class);
    @Autowired
    private ISalesmanService iSalesmanService;
    @Autowired
    private ISalesmanNumberService iSalesmanNumberService;
    @Autowired
    private ISalesmanImagesService iSalesmanImagesService;
    @Autowired
    private IFinancePrizePenaltyService iFinancePrizePenaltyService;
    @Autowired
    private ISysAdminApiService iSysAdminApiService;


    @ApiOperation(value = "获取业务员分页数据", notes = "获取业务员分页数据")
    @PostMapping(value = "list")
    public RespResult<PageData<TBInfo>> findSalesmanPageList(@ApiParam(value = "父级id,根目录默认为-1")HttpServletRequest request,HttpReqParam reqParam, TBQuery params){
        IPage<SalesmanVo> pageParam = PageRequestUtil.buildPageRequest(reqParam);
        pageParam.setSize(reqParam.getLimit());
        //查询全部所需数据
        IPage<TBInfo> page = iSalesmanService.findSalesmanPageList(pageParam,params);

        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "获取上线关系", notes = "获取上线关系")
    @RequestMapping(value = "relation", method = RequestMethod.GET)
    public RespResult<List<SalesmanVo>> findOnlineRelation(Long salesmanId) {
        try {
            List<SalesmanVo> list = iSalesmanService.findOnlineRelation(salesmanId);
            return RespResult.success(list);
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "存储业务员数据", notes = "存储业务员数据")
    @PostMapping(value = "save")
    public RespResult saveSalesman(HttpServletRequest request, TBInfo tbInfo) {
        try {
            // 以防前端更新数据后id有缓存值，后台进行id置空处理
            tbInfo.setId(null);
            // if 判断为空，进行处理
            if (StrUtil.isNull(tbInfo.getMobile())) {
                return RespResult.error("缺少参数！");
            }

            // 去除空格
            removeAllBank(tbInfo);

            //获取用户输入信息判断是否已存在
            Salesman salesman3 = iSalesmanService.findSalesmanByOrgIdAndMobile(2L,tbInfo.getMobile());
            // if 判断不为空，进行处理
            if (StrUtil.isNotNull(tbInfo.getMobile())) {
                if (null != salesman3) {
                    return RespResult.error("业务员已存在!");
                }
            }

            // 检测身份证是否注册过
            if (iSalesmanService.isExistSalesmanByIDCard(tbInfo.getCardNum())) {
                return RespResult.error("该身份证已被注册过!");
            }
            // 检测银行卡是否注册过
            if (iSalesmanService.isExistSalesmanByBank(tbInfo.getBankNum())) {
                return RespResult.error("该银行卡已被注册过!");
            }

            // 检测qq是否注册过
            if (iSalesmanService.isExistSalesmanByQQ(tbInfo.getQq())) {
                return RespResult.error("该QQ已被注册过!");
            }

            // 检测微信号是否注册过
            if (iSalesmanService.isExistSalesmanByWeChat(tbInfo.getWechart())) {
                return RespResult.error("该微信已被注册过!");
            }

            // if 判断有推荐人，进行处理
            if (StrUtil.isNotNull(tbInfo.getSalesmanMobile())) {
                // 获取业务员数据
                //获取信息 判断推荐人是否正常
                salesman3 = iSalesmanService.findSalesmanByOrgIdAndMobile(2L,tbInfo.getSalesmanMobile());
                // if 判断推荐人不存在，进行处理
                if (StrUtil.isNull(salesman3)) {
                    return RespResult.error("推荐人不存在！");
                }
                // if 判断推荐人是禁用状态，进行处理
                if (salesman3.getStatus().equalsIgnoreCase(Status.STATUS_PROHIBIT)) {
                    if (StrUtil.isNotNull(salesman3.getDisableTime())) {
                        return RespResult.error("推荐人需要" + DateUtil.daysBetween(new Date(), salesman3.getDisableTime()) + "天解封");
                    } else {
                        return RespResult.error("推荐人已永久禁用！");
                    }
                }
                // 设置推荐人
                tbInfo.setSalesmanId(salesman3.getId());
            }

            // 判断该淘宝会员名是否存在
            if (StrUtil.isNotNull(tbInfo.getOnlineid())) {
                SalesmanNumber salesmanNumber = iSalesmanNumberService.findSalesmanNumberByPlatformIdOnlineId(PlatformTypeValue.PLATFORM_TYPE_TB.getPlatformId(), tbInfo.getOnlineid());
                if (StrUtil.isNotNull(salesmanNumber)) {
                    return RespResult.error("该淘宝号已经存在!");
                }
            }

            // 记录操作人
            tbInfo.setOprId(AdminRequestUtil.identify(request));

            // 保存业务员信息
            iSalesmanService.saveTBInfo(tbInfo);
            if (tbInfo.getOnlineid() != null) {
                logger.info("请求IP: 【{}】 操作人ID:【{}】 添加了会员信息->【手机:{},名字:{},平台ID:{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), tbInfo.getMobile(), tbInfo.getName(), tbInfo.getOnlineid());
            }

            return RespResult.success();

        } catch(Exception e){
            return RespResult.error();
        }
    }

    private void removeAllBank(TBInfo tbInfo) {
        tbInfo.setSalesmanMobile(StrUtil.removeAllBlank(tbInfo.getSalesmanMobile()));
        tbInfo.setMobile(StrUtil.removeAllBlank(tbInfo.getMobile()));
        tbInfo.setName(StrUtil.removeAllBlank(tbInfo.getName()));
        tbInfo.setOnlineid(StrUtil.removeAllBlank(tbInfo.getOnlineid()));
        tbInfo.setBankNum(StrUtil.removeAllBlank(tbInfo.getBankNum()));
    }

    @ApiOperation(value = "修改业务状态", notes = "修改业务状态")
    @PostMapping(value = "status")
    public RespResult updateSalesmanNumberStatus(HttpServletRequest request, Long id, String status) {
        try {
            iSalesmanService.updateTBInfoStatusById(id, status);
            logger.info("请求IP: 【{}】 操作人ID:【{}】 修改了会员状态->【编号:{},状态:{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), id, status);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "编辑业务员信息", notes = "编辑业务员信息")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public RespResult updateSalesmanData(HttpServletRequest request, TBInfo tbInfo) {
        // if 判断为空，进行处理
        if (StrUtil.isNull(tbInfo) || StrUtil.isNull(tbInfo.getMobile())) {
            return RespResult.error("缺少参数！");
        }

        // 去除空格
        removeAllBank(tbInfo);

        // if 判断不为空，进行处理
        if (StrUtil.isNotNull(tbInfo.getMobile())) {
            Salesman salesman3 = iSalesmanService.findSalesmanByOrgIdAndMobile(2L, tbInfo.getMobile());
            if (StrUtil.isNotNull(salesman3) && !tbInfo.getId().equals(salesman3.getId())) {
                return RespResult.error("业务员已存在!");
            }
            // 检测身份证是否注册过
            if (StrUtil.isNotNull(salesman3) && !salesman3.getCardNum().equalsIgnoreCase(tbInfo.getCardNum())
                    && iSalesmanService.isExistSalesmanByIDCard(tbInfo.getCardNum())) {
                return RespResult.error("该身份证已被注册过!");
            }
            // 检测银行卡是否注册过
            if (StrUtil.isNotNull(salesman3) && !salesman3.getBankNum().equalsIgnoreCase(tbInfo.getCardNum())
                    && iSalesmanService.isExistSalesmanByBank(tbInfo.getBankNum())) {
                return RespResult.error("该银行卡已被注册过!");
            }
        }

        // true为推荐人不禁用，false为推荐人禁用
        boolean bool = true;
        // if 判断有推荐人，进行处理
        if (StrUtil.isNotNull(tbInfo.getSalesmanMobile())) {
            // 获取业务员数据
            Salesman salesman2 = iSalesmanService.findSalesmanByOrgIdAndMobile(2L, tbInfo.getSalesmanMobile());
            // if 判断推荐人不存在，进行处理
            if (StrUtil.isNull(salesman2)) {
                return RespResult.error("推荐人不存在！");
            }
            // if 判断业务员是禁用状态，进行处理
            if (salesman2.getStatus().equalsIgnoreCase(Status.STATUS_PROHIBIT)) {
                //return RespResult.error("推荐人已禁用！");
                bool = false;
            }
            // 设置推荐人
            tbInfo.setSalesmanId(salesman2.getId());
        }

        // 判断该淘宝会员名是否存在
        if (StrUtil.isNotNull(tbInfo.getOnlineid())) {
            QueryWrapper<SalesmanNumber> nqueryWrapper = new QueryWrapper<>();
            nqueryWrapper.eq("platform_id", PlatformTypeValue.PLATFORM_TYPE_TB.getPlatformId());
            nqueryWrapper.eq("onlineid", tbInfo.getOnlineid());

            SalesmanNumber salesmanNumber = iSalesmanNumberService.getOne(nqueryWrapper);
            if (StrUtil.isNotNull(salesmanNumber) && !salesmanNumber.getSalesmanId().equals(tbInfo.getId())) {
                return RespResult.error("该淘宝号已经存在!");
            }
        }
        try {
            iSalesmanService.updateTBInfo(tbInfo);
        } catch (Exception e) {
            return RespResult.error(e.getMessage());
        }
        logger.info("请求IP: 【{}】 操作人ID:【{}】 修改了会员信息->【手机:{},名字:{},平台ID:{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), tbInfo.getMobile(), tbInfo.getName(), tbInfo.getOnlineid());
        return RespResult.success(bool ? "" : "推荐人已禁用！");
    }

    @ApiOperation(value = "淘宝验号", notes = "淘宝验号")
    @RequestMapping("/query")
    public RespDataUtil searchTB(String wangwang) {
        if (StrUtil.isNull(wangwang)) {
            return RespDataUtil.errorData("旺旺号不能为空");
        }
        wangwang = StrUtil.removeAllBlank(wangwang);
        String json = TBOnlineIdCheckUtil.searchOnlineId(wangwang);
        Gson gson = new GsonBuilder().create();
        Map<String, Object> map;
        Type type = new TypeToken<Map<String, Object>>(){}.getType();
        map = gson.fromJson(json, type);
        return RespDataUtil.successData(map);
    }


    @ApiOperation(value = "号主统计请求方法", notes = "号主统计请求方法")
    @RequestMapping("echarts")
    public String echarts() {
        return "member/echarts";
    }

    @ApiOperation(value = "号主统计处理方法", notes = "号主统计处理方法")
    @RequestMapping("echarts.json")
    public RespResult<Map<String, List<AreaVo>>> findEchartsData() {
        Map<String, List<AreaVo>> map = new HashMap<>();
        List<AreaVo> province = iSalesmanNumberService.findSalesmanNumberAreaByAreaCode(AreaType.province);
        map.put("province", province);
        return RespResult.success(map);
    }

    @ApiOperation(value = "标记是否安装查小号", notes = "标记是否安装查小号")
    @PostMapping(value = "search")
    public RespResult updateSalesmanSearch(Long id, Integer isSearch) {
        try {
            iSalesmanService.updateSalesmanSearch(id, isSearch);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "获取业务员对应的证件资源文件", notes = "获取业务员对应的证件资源文件")
    @RequestMapping(value = "src")
    public RespResult findSelesmanSrc(Long id) {
        SalesmanImagesVo salesmanImagesVo = new SalesmanImagesVo();
        //资源类型设置为业务员
        salesmanImagesVo.setSrcType("SM");
        List<SalesmanImagesVo> exts = iSalesmanImagesService.findSalesmanImagesBySrcIdAndSrcType(id, salesmanImagesVo.getSrcType());
        return RespResult.success(exts);
    }

    @ApiOperation(value = "三网检测", notes = "三网检测")
    @RequestMapping("/certification")
    public RespResult certification(HttpServletRequest request, Long id, String name, String mobile, String idCard, String bankNum) {
        if (StrUtil.isNull(id) || StrUtil.isNull(name) || StrUtil.isNull(mobile) || StrUtil.isNull(idCard) || StrUtil.isNull(bankNum)) {
            return
                    RespResult.error("数据缺失");
        }
        mobile = StrUtil.removeAllBlank(mobile);
        name = StrUtil.removeAllBlank(name);
        idCard = StrUtil.removeAllBlank(idCard);
        bankNum = StrUtil.removeAllBlank(bankNum);
        //String mobileOnlineInfo = MobileOnlineUtil.getMobileOnlineInfo(mobile);
        String mobileOnlineInfo = "已停用";
        String idMobileNameInfo = IDMobileNameUtil.getIDMobileNameInfo(idCard, name, mobile);
        //String idMobileNameInfo = IDNameMobileUtil.getIDNameMobileInfo(idCard, name, mobile);
        //String idMobileNameBankInfo = IDMobileNameBankUtil.getIDMobileNameBankInfo(bankNum, idCard, mobile, name);
        // 银行卡三要素
        String idMobileNameBankInfo = IDNameMobileBankUtil.getIDNameMobileBankInfo(idCard, name, "", bankNum);
        Salesman salesman = new Salesman();
        salesman.setId(id);
        salesman.setMobileOnline(mobileOnlineInfo);
        salesman.setIdMobileName(idMobileNameInfo);
        salesman.setIdMobileNameBank(idMobileNameBankInfo);

        iSalesmanService.updateSalesman(salesman);
        Map<String, String> data = new HashMap<>();
        data.put("mobileOnlineInfo", mobileOnlineInfo);
        data.put("idMobileNameInfo", idMobileNameInfo);
        data.put("idMobileNameBankInfo", idMobileNameBankInfo);
        // 记录管理员操作
        iSysAdminApiService.recordAdminInfo(AdminRequestUtil.identify(request));
        logger.info("请求IP: 【{}】 操作人ID:【{}】 验证了会员信息->【id:{},名字:{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), id, name);
        return RespResult.success(data);
    }

    @ApiOperation(value = "子任务列表--禁用号主与上线", notes = "子任务列表--禁用号主与上线")
    @PostMapping("/disable")
    public RespResult disableSalesmanNumber(HttpServletRequest request, String ids) {
        try {
            iSalesmanNumberService.disableSalesmanNumberByIds(ids);
            logger.info("请求IP: 【{}】 操作人ID:【{}】 禁用了号主状态->【编号:{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @PostMapping(value = "review")
    public RespResult reviewSalesmanStatus(HttpServletRequest request, Long id, String startStatus, String endStatus) {
        try {
                iSalesmanService.updateTBInfoStatusById(id, AdminRequestUtil.identify(request), startStatus, endStatus);
            logger.info("请求IP: 【{}】 操作人ID:【{}】 修改了会员状态->【编号:{},状态:{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), id, endStatus);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @PostMapping("prize")
    public RespResult prizePenaltyV1(FinancePrizePenalty prizePenalty, HttpServletRequest request) {
        // 获取当前审核人id
        Long oprId = AdminRequestUtil.identify(request);

        if (StrUtil.isNull(oprId)) {
            return RespResult.error("审核人为空");
        }

        // 判断数据
        if (StrUtil.isNull(prizePenalty)) {
            return RespResult.error("数据为空");
        } else if (StrUtil.isNull(prizePenalty.getSalesmanId())) {
            return RespResult.error("业务员id为空");
        } else if (StrUtil.isNull(prizePenalty.getMoney())) {
            return RespResult.error("奖罚金额为空");
        } else if (StrUtil.isNull(prizePenalty.getReason())) {
            return RespResult.error("奖罚原因为空");
        } else if (StrUtil.isNull(prizePenalty.getStatus())) {
            return RespResult.error("状态为空");
        } else if (StrUtil.isNull(prizePenalty.getType())) {
            return RespResult.error("类型为空");
        } else if (StrUtil.isNull(prizePenalty.getOrgId())) {
            return RespResult.error("所属机构为空");
        }
        // 生成奖罚信息
        try {
            // 设置审核人
            prizePenalty.setAuditorId(oprId);
            iFinancePrizePenaltyService.savePrizePenal(prizePenalty);
            return RespResult.success("处理成功");
        } catch (Exception e) {
            return RespResult.error(StrUtil.isNull(e.getMessage())?"处理失败":e.getMessage());
        }
    }
}
