package com.sbdj.admin.member;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.member.entity.SalesmanLevel;
import com.sbdj.service.member.service.ISalesmanLevelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 业务员等级表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "等级配置控制器")
@RestController
@RequestMapping("/salesman/level")
public class SalesmanLevelController {

    @Autowired
    private ISalesmanLevelService iSalesmanLevelService;

    @ApiOperation(value = "获取等级配置分页数据", notes = "获取等级配置分页数据")
    @PostMapping("list")
    public RespResult<PageData<SalesmanLevel>> salesmanLevelList(HttpReqParam reqParam) {
        try {
            IPage<SalesmanLevel> page = PageRequestUtil.buildPageRequest(reqParam);
            iSalesmanLevelService.findSalesmanLevelPageList(page);
            return RespResult.buildPageData(page);
        }catch (Exception e){
            return  RespResult.error();
        }

    }

    @ApiOperation(value = "保存等级配置分页数据", notes = "保存等级配置分页数据")
    @PostMapping(value = "save")
    public RespResult saveSalesmanLevel(SalesmanLevel salesmanLevel) {
        try {
            iSalesmanLevelService.saveSalesmanLevel(salesmanLevel);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "修改等级配置分页数据", notes = "修改等级配置分页数据")
    @PostMapping(value = "update")
    public RespResult updateSalesmanLevel(SalesmanLevel salesmanLevel) {
        try {
            iSalesmanLevelService.updateSalesmanLevel(salesmanLevel);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }


    @ApiOperation(value = "禁用等级配置",notes = "禁用等级配置")
    @PostMapping(value = "delete")
    public RespResult deleteSalesmanLevel(Long id) {
        try {
            iSalesmanLevelService.loginDeleteSalesmanLevel(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }


    @ApiOperation(value = "查询全部业务员等级配置",notes = "查询全部业务员等级配置")
    @RequestMapping(value = "select")
    public RespResult findSalesmanLevelSelect() {
        List<SalesmanLevel> list = iSalesmanLevelService.findSalesmanLevelSelect();
        return RespResult.success(list);
    }


}
