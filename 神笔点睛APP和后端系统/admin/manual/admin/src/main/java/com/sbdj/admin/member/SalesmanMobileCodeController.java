package com.sbdj.admin.member;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 手机号验证码表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@RestController
@RequestMapping("/salesman/mobile/code")
public class SalesmanMobileCodeController {

}
