package com.sbdj.admin.member;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.admin.query.SalesmanNumberQuery;
import com.sbdj.service.member.admin.vo.SalesmanImagesVo;
import com.sbdj.service.member.admin.vo.SalesmanNumberVo;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.member.entity.SalesmanPhones;
import com.sbdj.service.member.service.ISalesmanImagesService;
import com.sbdj.service.member.service.ISalesmanNumberService;
import com.sbdj.service.member.service.ISalesmanPhonesService;
import com.sbdj.service.member.service.ISalesmanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 号主表 前端控制器
 * </p>
 *
 * @author Zc
 * @since 2019-11-22
 */
@Api(tags = "号主控制器")
@RestController
@RequestMapping("/salesman/number")
public class SalesmanNumberController {

    private static Logger logger = LoggerFactory.getLogger(SalesmanNumberController.class);

    @Autowired
    private ISalesmanNumberService iSalesmanNumberService;

    @Lazy
    @Autowired
    private ISalesmanService iSalesmanService;

    @Autowired
    private ISalesmanImagesService iSalesmanImagesService;

    @Autowired
    private ISalesmanPhonesService iSalesmanPhonesService;

    @ApiOperation(value = "号主获取分页数据", notes = "号主获取分页数据")
    @ResponseBody
    @PostMapping(value = "list")
    public RespResult<PageData<SalesmanNumberVo>> findSalesmanNumberList(HttpServletRequest request, SalesmanNumberQuery params, HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            //为什么改机构
            params.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<SalesmanNumberVo> salesmanNumberIPage = PageRequestUtil.buildPageRequest(reqParam);
        salesmanNumberIPage.setSize(reqParam.getLimit());
        salesmanNumberIPage.setCurrent(reqParam.getPage());
        IPage<SalesmanNumberVo> page = iSalesmanNumberService.findSalesmanNumberPage(params,salesmanNumberIPage);
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "修改号主状态", notes = "修改号主状态")
    @ResponseBody
    @PostMapping(value = "status")
    public RespResult updateSalesmanNumberStatus(HttpServletRequest request, Long id, String status) {
        try {
            iSalesmanNumberService.updateSalesmanNumberStatus(id, status);
            logger.info("请求IP: 【{}】 操作人ID:【{}】 修改了号主状态->【编号:{},状态:{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), id, status);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }


    /**
     *
     * @author ZC
     * @date 2019-11-25 16:17
     * @param request 网页请求
     * @param salesmanNumber 号主信息
     * @param salesMobile 业务员账户
     * @return com.sbdj.admin.web.RespResult
     */
    @ApiOperation(value = "存储新号主", notes = "存储新号主")
    @ResponseBody
    @PostMapping(value = "save")
    public RespResult saveSalesmanNumber(HttpServletRequest request, SalesmanNumber salesmanNumber, String salesMobile) {
        // 去除空格
        salesmanNumber.setCardNum(StrUtil.removeAllBlank(salesmanNumber.getCardNum()));
        salesmanNumber.setMobile(StrUtil.removeAllBlank(salesmanNumber.getMobile()));
        salesMobile = StrUtil.removeAllBlank(salesMobile);
        // ~判断不为空的情况下!
        if (StrUtil.isNotNull(salesMobile)) {
            Salesman sm = iSalesmanService.findSalesmanByMobile(salesMobile);
            if (null == sm) {
                return RespResult.error("业务员账号不存在");
            }
            salesmanNumber.setSalesmanId(sm.getId());
        } else {
            return RespResult.error("业务员账号必填");
        }
        // 号主只能添加一个
        if (salesmanNumber.getSalesmanId() != 1) {
            Integer flag = iSalesmanNumberService.findSalesmanNumberTotalBySalesmanIdAndPlatformId(salesmanNumber.getSalesmanId(), salesmanNumber.getPlatformId());
            if (flag > 0) {
                return RespResult.error("号主只能录一个!");
            }
        }
        //验证手机号
        //String regex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$";
        String phone = salesmanNumber.getMobile();
        //获取1出现在第几个位置若不是第[0]个则说明有错误
        int index = phone.indexOf("1");
        if(salesmanNumber.getMobile().length() != 11){
            return RespResult.error("手机号应为11位数");
        }else if(StrUtil.isNull(phone)||index!=0){
            return RespResult.error("请输入正确手机号");
        }
        // 判断手机号是否重复
        if (iSalesmanNumberService.isExistSalesmanNumberByMobile(phone, salesmanNumber.getPlatformId())) {
            return RespResult.error("该手机号已被注册");
        }
        // 判断身份证件号
        if (iSalesmanNumberService.isExistSalesmanNumberByIDCard(salesmanNumber.getCardNum(), salesmanNumber.getPlatformId())) {
            return RespResult.error("该身份证已被注册");
        }
        // 判断银行卡号

        // 判断新增号主信息是否存在
        if (iSalesmanNumberService.isExistSalesmanNumber(salesmanNumber.getPlatformId(), salesmanNumber.getOnlineid())) {
            return RespResult.error("号主ID已存在");
        }

        try {
            // 昵称除去空格
            salesmanNumber.setOnlineid(StrUtil.removeAllBlank(salesmanNumber.getOnlineid()));
            iSalesmanNumberService.addSalesmanNumber(salesmanNumber);
            logger.info("请求IP: 【{}】 操作人ID:【{}】 添加了号主信息->【编号:{},名字:{},会员:{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), salesmanNumber.getId(), salesmanNumber.getName(), salesmanNumber.getSalesmanId());
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "编辑号主信息", notes = "编辑号主信息")
    @ResponseBody
    @PostMapping(value = "update")
    public RespResult updateSalesmanNumber(HttpServletRequest request, SalesmanNumber salesmanNumber, String salesMobile) {
        // 去除空格
        salesmanNumber.setCardNum(StrUtil.removeAllBlank(salesmanNumber.getCardNum()));
        salesmanNumber.setMobile(StrUtil.removeAllBlank(salesmanNumber.getMobile()));
        salesMobile = StrUtil.removeAllBlank(salesMobile);
        // ~判断不为空的情况下!
        if (StrUtil.isNotNull(salesMobile)) {
            Salesman sm = iSalesmanService.findSalesmanByMobile(salesMobile);
            if (null == sm) {
                return RespResult.error("业务员账号不存在");
            }
            salesmanNumber.setSalesmanId(sm.getId());
        }
        try {
            iSalesmanNumberService.updateSalesmanNumberData(salesmanNumber);
            logger.info("请求IP: 【{}】 操作人ID:【{}】 修改了号主信息->【编号:{},名字:{},会员:{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), salesmanNumber.getId(), salesmanNumber.getName(), salesmanNumber.getSalesmanId());
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }


    @ApiOperation(value = "获取号主资源文件", notes = "获取号主资源文件")
    @ResponseBody
    @RequestMapping(value = "src")
    public RespResult findSalesmanNumberSrc(Long id) {
        try {
            List<SalesmanImagesVo> exts = iSalesmanImagesService.findSalesmanImagesBySrcIdAndSrcType(id, "SN");
            return RespResult.success(exts);
        } catch (Exception e) {
            return RespResult.error();
        }
    }
    @ResponseBody
    @RequestMapping(value = "phones")
    public RespResult<PageData<SalesmanPhones>> findSalesmanPhonesPage(Long id, HttpReqParam reqParam) {
        IPage<SalesmanPhones> page = iSalesmanPhonesService.findSalesmanPhonesPag(id,"SN", PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "子任务列表--禁用号主", notes = "子任务列表--禁用号主")
    @ResponseBody
    @PostMapping("/disable")
    public RespResult disableSalesmanNumber(HttpServletRequest request, String ids) {
        try {
            iSalesmanNumberService.disableSalesmanNumberByIds(ids);
            logger.info("请求IP: 【{}】 操作人ID:【{}】 禁用了号主状态->【编号:{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }


}
