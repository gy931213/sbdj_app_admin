package com.sbdj.admin.member;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.GPSUtil;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.admin.query.SalesmanNumberQuery;
import com.sbdj.service.member.admin.vo.SalesmanNumberTmpVo;
import com.sbdj.service.member.entity.SalesmanNumberPhonesTmp;
import com.sbdj.service.member.entity.SalesmanNumberTmp;
import com.sbdj.service.member.service.ISalesmanNumberPhonesTmpService;
import com.sbdj.service.member.service.ISalesmanNumberTmpService;
import com.sbdj.service.system.entity.SysMapTrajectory;
import com.sbdj.service.system.service.ISysMapTrajectoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * <p>
 * 临时号主表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "临时号主控制器")
@RestController
@RequestMapping("/salesman/number/tmp")
public class SalesmanNumberTmpController {

    private Logger logger = LoggerFactory.getLogger(SalesmanNumberTmpController.class);

    @Autowired
    private ISalesmanNumberTmpService iSalesmanNumberTmpService;
    @Autowired
    private ISalesmanNumberPhonesTmpService iSalesmanNumberPhonesTmpService;
    @Autowired
    private ISysMapTrajectoryService iSysMapTrajectoryService;


    @ApiOperation(value = "获取临时号主分页数据", notes = "获取临时号主分页数据")
    @PostMapping(value = "list")
    public RespResult<PageData<SalesmanNumberTmp>> findSalesmanNumberList(HttpServletRequest request, SalesmanNumberQuery params, HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            params.setOrgId(AdminRequestUtil.orgId(request));
        } else {
            params.setOrgId(AdminRequestUtil.orgId(request));
        }

        if (StrUtil.isNotNull(params.getOrgId()) && ! "1".equals(params.getOrgId().toString()) && !"2".equals(params.getOrgId().toString())) {
            return RespResult.error("您当前没有权限", 403);
        }

        IPage<SalesmanNumberTmp> page =  PageRequestUtil.buildPageRequest(reqParam);
        page = iSalesmanNumberTmpService.findSalesmanNumberPage(params,page);
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "获取临时号主通讯录数据", notes = "获取临时号主通讯录数据")
    @RequestMapping(value = "phones")
    public RespResult<PageData<SalesmanNumberPhonesTmp>> findSalesmanPhonesPage(HttpServletRequest request, Long id, HttpReqParam reqParam) {
        Long orgId;
        if (!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        } else {
            orgId = AdminRequestUtil.orgId(request);
        }

        if (StrUtil.isNotNull(orgId) && (! "1".equals(orgId.toString()) && !"2".equals(orgId.toString()))) {
            return RespResult.error("您当前没有权限", 403);
        }

        IPage<SalesmanNumberPhonesTmp> page = PageRequestUtil.buildPageRequest(reqParam);
        page = iSalesmanNumberPhonesTmpService.findSalesmanNumberPhonesTmpPage(id,page);
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "删除临时号主", notes = "删除临时号主")
    @PostMapping(value = "delete")
    public RespResult deleteSalesmanNumber(HttpServletRequest request, Long id) {
        Long orgId;
        if (!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        } else {
            orgId = AdminRequestUtil.orgId(request);
        }

        if (StrUtil.isNotNull(orgId) && (!orgId.toString().equals("1") && !orgId.toString().equals("2"))) {
            return RespResult.error("您当前没有权限", 403);
        }

        if (StrUtil.isNull(id)) {
            return RespResult.error("临时号主编号不能为空");
        }
        try {
            iSalesmanNumberTmpService.logicDeleteSalesmanNumberTmpById(id);
        } catch (Exception e) {
            return RespResult.error("删除失败");
        }
        return RespResult.success("删除成功");
    }

    @ApiOperation(value = "根据条件获取临时号主定位信息", notes = "根据条件获取临时号主定位信息")
    @PostMapping(value = "position/get")
    public RespResult<List<SalesmanNumberTmpVo>> getPositionById(HttpServletRequest request, Long id) {
        Long orgId;
        if (!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        } else {
            orgId = AdminRequestUtil.orgId(request);
        }

        if (StrUtil.isNotNull(orgId) && (!"1".equals(orgId.toString()) && !"2".equals(orgId.toString()))) {
            return RespResult.error("您当前没有权限", 403);
        }

        if (StrUtil.isNull(id)) {
            return RespResult.error("临时号主ID不能为空");
        }
        List<SalesmanNumberTmpVo> list = iSalesmanNumberTmpService.findSalesmanNumberTmpPositionById(id);
        return RespResult.success(list);
    }

    @ApiOperation(value = "获取全部临时号主定位信息", notes = "获取全部临时号主定位信息")
    @PostMapping(value = "position")
    public RespResult<List<SalesmanNumberTmpVo>> getPositionAll(HttpServletRequest request) {
        Long orgId;
        if (!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        } else {
            orgId = AdminRequestUtil.orgId(request);
        }

        if (StrUtil.isNotNull(orgId) && (!"1".equals(orgId.toString()) && !"2".equals(orgId.toString()))) {
            return RespResult.error("您当前没有权限", 403);
        }

        List<SalesmanNumberTmpVo> list = iSalesmanNumberTmpService.findSalesmanNumberTmpPositionAll();
        return RespResult.success(list);
    }

    @ApiOperation(value = "检测通讯录", notes = "检测通讯录")
    @RequestMapping(value = "check")
    public RespResult checkPhone(String phone){
        if (StrUtil.isNull(phone)) {
            return RespResult.error("参数为空");
        }
        // 开始时间
        long startTime = System.currentTimeMillis();
        // 正则 只提取数字
        Pattern pattern = Pattern.compile("[^0-9]");
        // 初始化Gson
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        // 获取被查询手机的信息
        SalesmanNumberTmp mySnt = iSalesmanNumberTmpService.findSalesmanNumberByPhone(phone);
        // 获取被查询手机的通讯录
        List<SalesmanNumberPhonesTmp> mySnpt = iSalesmanNumberPhonesTmpService.findSalesmanNumberPhonesTmpList(mySnt.getId());
        // 定义被查询手机的通讯录里面已注册查小号的手机
        Set<String> phones = new HashSet<>();
        // 临时数据-号主
        SalesmanNumberTmp sntTmp;
        // 获取手机信息
        String phoneTmp;
        Map<String,String> tmpMap = new HashMap<>();
        for (SalesmanNumberPhonesTmp tmp : mySnpt) {
            // 获取有效手机号码
            phoneTmp = pattern.matcher(tmp.getPhone()).replaceAll("");
            // 排除自己的手机
            if (phone.equalsIgnoreCase(phoneTmp)) {
                continue;
            }
            tmpMap.put(phoneTmp, tmp.getName());
        }
        logger.info("这个{}通讯录实际数量: {}", mySnt.getName(), mySnpt.size());
        // 加入自己
        tmpMap.put(phone, mySnt.getName());
        // 通讯录条数
        int myTotal = tmpMap.size();
        // 迭代查询数据
        for (SalesmanNumberPhonesTmp tmp : mySnpt) {
            // 获取有效手机号码
            phoneTmp = pattern.matcher(tmp.getPhone()).replaceAll("");
            // 排除自己的手机
            if (phone.equalsIgnoreCase(phoneTmp)) {
                continue;
            }
            // 在系统查询记录
            sntTmp = iSalesmanNumberTmpService.findSalesmanNumberByPhone(phoneTmp);
            if (StrUtil.isNotNull(sntTmp)) {
                phones.add(sntTmp.getMobile());
            }
        }
        // 匹配通讯录的数据
        List<SalesmanNumberPhonesTmp> otherSnpt;
        // 数据
        // 定义通讯录匹配模型
        Map<String, Object> contactModel;
        // 使用集合存取数据
        List<Map<String, Object>> dataList = new ArrayList<>();
        // 其他数据条数
        int otherTotal;
        // 我的联系人
        String myContact;
        // 其他联系人
        String otherContact;
        // 保留两位小数
        DecimalFormat df = new DecimalFormat("#0.00");
        // 计数器
        int count;
        // 百分比
        double percentage;
        for (String str : phones) {
            // 获取这个手机所对应的记录
            sntTmp = iSalesmanNumberTmpService.findSalesmanNumberByPhone(str);
            // 获取该手机的通讯录
            otherSnpt = iSalesmanNumberPhonesTmpService.findSalesmanNumberPhonesTmpList(sntTmp.getId());
            if (StrUtil.isNotNull(otherSnpt)) {
                Map<String,String> tmpsMap = new HashMap<>();
                for (SalesmanNumberPhonesTmp tmps : otherSnpt) {
                    otherContact = pattern.matcher(tmps.getPhone()).replaceAll("");
                    tmpsMap.put(otherContact, tmps.getName());
                }
                logger.info("这个{}通讯录实际数量: {}", sntTmp.getName(), tmpsMap.size());
                tmpsMap.put(sntTmp.getMobile(), sntTmp.getName());
                otherTotal = tmpsMap.size();
                // 排除没有通讯录记录的数据
                if (otherTotal < 1) {
                    continue;
                }
                contactModel = new HashMap<>();
                contactModel.put("myPhone", phone);
                contactModel.put("myName", mySnt.getName());
                contactModel.put("otherPhone", str);
                contactModel.put("otherName", sntTmp.getName());
                contactModel.put("myTotal", myTotal);
                contactModel.put("otherTotal", otherTotal);
                count = 0;
                // 数据匹配
                for (String other : tmpsMap.keySet()) {
                    for (String my : tmpMap.keySet()) {
                        if (other.equalsIgnoreCase(my)) {
                            count++;
                        }
                    }
                }
                // 只需要重复的
                if (count < 1) {
                    continue;
                }
                contactModel.put("sameData", count);
                percentage = (count * 1.0) / (myTotal * 1.0);
                contactModel.put("percentage", df.format(percentage));
                dataList.add(contactModel);
            }
        }
        // JSON序列化
        logger.info(gson.toJson(dataList));
        // 结束时间
        long endTime = System.currentTimeMillis();
        logger.info("通讯录检测 总共耗时: " + (endTime - startTime) + "ms");
        return RespResult.success(dataList);
    }

    @ApiOperation(value = "深度检测通讯录", notes = "深度检测通讯录")
    @RequestMapping(value = "depth/check")
    public RespResult depthCheckPhone(String phone) {
        // 开始时间
        long startTime = System.currentTimeMillis();
        // 正则 只提取数字
        Pattern pattern = Pattern.compile("[^0-9]");
        // 初始化Gson
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        // 获取被查询手机的信息
        SalesmanNumberTmp mySnt = iSalesmanNumberTmpService.findSalesmanNumberByPhone(phone);
        // 获取被查询手机的通讯录
        List<SalesmanNumberPhonesTmp> mySnpt = iSalesmanNumberPhonesTmpService.list(new QueryWrapper<SalesmanNumberPhonesTmp>()
                .eq("target_id", mySnt.getId()).select("phone", "name"));

        Map<String, String> myContacts = new HashMap<>();
        String phoneTmp;
        for (SalesmanNumberPhonesTmp spt : mySnpt) {
            if (StrUtil.isNull(spt.getPhone())) {
                continue;
            }
            phoneTmp = pattern.matcher(spt.getPhone()).replaceAll("");
            if (phone.equalsIgnoreCase(phoneTmp)) {
                continue;
            }
            myContacts.put(phoneTmp, spt.getName());
        }
        myContacts.put(phone, mySnt.getName());
        // 统计数量
        int myContactsSize = myContacts.size() - 1;

        Map<Long, Map<String, String>> ext1 = new HashMap<>();
        Map<String, String> ext2;
        List<SalesmanNumberPhonesTmp> list = iSalesmanNumberPhonesTmpService.list(new QueryWrapper<SalesmanNumberPhonesTmp>().select("phone", "name", "target_id"));

        // 排除重复数据
        for (SalesmanNumberPhonesTmp snpt : list) {
            if (StrUtil.isNull(snpt.getPhone())) {
                continue;
            }
            if (ext1.containsKey(snpt.getTargetId())) {
                ext2 = ext1.get(snpt.getTargetId());
            } else {
                ext2 = new HashMap<>();
            }
            phoneTmp = pattern.matcher(snpt.getPhone()).replaceAll("");
            ext2.put(phoneTmp, snpt.getName());
            ext1.put(snpt.getTargetId(), ext2);
        }

        // 重复计数
        int count;
        // 重复率
        double percentage;
        // 临时号主
        SalesmanNumberTmp snt;
        // 保存数据
        Map<String, Object> map;
        // 保留两位小数
        DecimalFormat df = new DecimalFormat("#0.00");
        List<Map<String, Object>> data = new ArrayList<>();
        // 匹配通讯录
        for (Map.Entry<Long, Map<String, String>> entry : ext1.entrySet()) {
            count = 0;
            for (String tmp : myContacts.keySet()) {
                for (String key : entry.getValue().keySet()) {
                    if (tmp.equalsIgnoreCase(key)) {
                        count++;
                    }
                }
            }
            if (count > 0) {
                percentage = (double) count / (double) myContactsSize;
                if (percentage < 0.01) {
                    continue;
                }
                snt = iSalesmanNumberTmpService.getById(entry.getKey());
                map = new HashMap<>();
                //map.put("myPhone", phone);
                //map.put("myName", mySnt.getName());
                map.put("otherName", snt.getName());
                map.put("otherPhone", snt.getMobile());
                map.put("percentage", df.format(percentage));
                map.put("myTotal", myContactsSize);
                map.put("otherTotal", entry.getValue().size());
                map.put("sameData", count);
                data.add(map);
            }
        }
        // JSON序列化
        logger.info(gson.toJson(data));
        // 结束时间
        long endTime = System.currentTimeMillis();
        logger.info("通讯录检测 总共耗时: " + (endTime - startTime) + "ms");

        return RespResult.success(data);
    }

    @ApiOperation(value = "检测距离", notes = "检测距离")
    @RequestMapping("position/check")
    public RespResult checkPosition(String phone, Double distance) {
        // 开始时间
        long startTime = System.currentTimeMillis();
        // 获取临时号主信息
        SalesmanNumberTmp snt = iSalesmanNumberTmpService.findSalesmanNumberByPhone(phone);
        // 获取该临时号主的最近一次定位
        SysMapTrajectory smt = iSysMapTrajectoryService.getOne(new QueryWrapper<SysMapTrajectory>()
                .eq("target_id", snt.getId()).orderByDesc("create_time").last("limit 1"));
        // 获取所有的定位信息
        QueryWrapper<SysMapTrajectory> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("target_id","longitude","latitude","ip")
                .notExists("select 1 from sys_map_trajectory smt where target_id=smt.target_id and smt.create_time < create_time");
        List<SysMapTrajectory> list = iSysMapTrajectoryService.list(queryWrapper);
        Map<Long, Map<String, Object>> data = new HashMap<>();
        Map<String, Object> info;
        double distanceTmp;
        // 临时号主信息
        SalesmanNumberTmp sntTmp;
        // 获取符合要求的数据
        for (SysMapTrajectory tmp : list) {
            info = new HashMap<>();
            distanceTmp = GPSUtil.getDistance(Double.parseDouble(smt.getLongitude()), Double.parseDouble(smt.getLatitude()),
                    Double.parseDouble(tmp.getLongitude()), Double.parseDouble(tmp.getLatitude()));
            if (distanceTmp > distance) {
                continue;
            }
            sntTmp = iSalesmanNumberTmpService.getById(tmp.getTargetId());
            info.put("distance", distanceTmp);
            info.put("position", tmp.getLongitude() + "," + tmp.getLatitude());
            info.put("targetId", tmp.getTargetId());
            info.put("ip", tmp.getIp());
            info.put("name", sntTmp.getName());
            info.put("phone", sntTmp.getMobile());
            data.put(tmp.getTargetId(), info);
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        logger.info(gson.toJson(data));
        // 结束时间
        long endTime = System.currentTimeMillis();
        logger.info("位置检测 总共耗时: " + (endTime - startTime) + "ms");
        return RespResult.success(data);
    }

}
