package com.sbdj.admin.member;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 业务员\业务员号主\通讯录表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Controller
@RequestMapping("/salesman/phones")
public class SalesmanPhonesController {

}
