package com.sbdj.admin.member;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 临时号主\通讯录表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "临时号主通讯录控制器")
@Controller
@RequestMapping("/salesman/phones/tmp")
public class SalesmanPhonesTmpController {


}
