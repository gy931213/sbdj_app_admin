package com.sbdj.admin.member;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.member.admin.vo.SalesmanRoyaltyVo;
import com.sbdj.service.member.entity.SalesmanRoyalty;
import com.sbdj.service.member.service.ISalesmanRoyaltyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 业务员额度增长配置表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "额度增长配置")
@RestController
@RequestMapping("/salesman/royalty")
public class SalesmanRoyaltyController {

    @Autowired
    private ISalesmanRoyaltyService iSalesmanRoyaltyService;

    @ApiOperation(value = "获取额度增长配置分页数据" ,notes = "获取额度增长配置分页数据")
    @PostMapping("list")
    public RespResult<PageData<SalesmanRoyaltyVo>> salesmanRoyaltyList(HttpServletRequest request, Long orgId, HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        IPage<SalesmanRoyaltyVo> page =  iSalesmanRoyaltyService.findSalesmanRoyaltyPageList(orgId,PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "禁用额度增长配置" ,notes = "禁用额度增长配置")
    @PostMapping("delete")
    public RespResult deleteSalesmanRoyalty(Long id) {
        try {
            iSalesmanRoyaltyService.logicDeleteSalesmanRoyalty(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "修改额度增长配置" ,notes = "修改额度增长配置")
    @PostMapping("update")
    public RespResult updateSalesmanRoyalty(SalesmanRoyalty royalty) {
        try {
            SalesmanRoyalty royalty2 = iSalesmanRoyaltyService.findSalesmanRoyaltyByOrgId(royalty.getOrgId());
            if (null != royalty2 && !royalty.getId().equals(royalty2.getId()) && royalty.getOrgId().equals(royalty2.getOrgId())) {
                return RespResult.error("配置已存在");
            }
            iSalesmanRoyaltyService.updateSalesmanRoyalty(royalty);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "保存额度增长配置" ,notes = "保存额度增长配置")
    @PostMapping("save")
    public RespResult saveSalesmanRoyalty(SalesmanRoyalty royalty) {
        try {
            SalesmanRoyalty royalty2 = iSalesmanRoyaltyService.findSalesmanRoyaltyByOrgId(royalty.getOrgId());
            if (null != royalty2 && royalty.getOrgId().equals(royalty2.getOrgId())) {
                return RespResult.error("配置已存在");
            }
            iSalesmanRoyaltyService.saveSalesmanRoyalty(royalty);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

}
