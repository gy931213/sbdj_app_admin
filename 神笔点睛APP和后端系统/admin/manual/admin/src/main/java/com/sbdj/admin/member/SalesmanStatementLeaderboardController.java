package com.sbdj.admin.member;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 业务员佣金排行榜 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Controller
@RequestMapping("/salesman/statement/leaderboard")
public class SalesmanStatementLeaderboardController {

}
