package com.sbdj.admin.seller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.constant.AdminType;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.admin.query.AdminQuery;
import com.sbdj.service.system.admin.vo.AdminVo;
import com.sbdj.service.system.entity.SysAdmin;
import com.sbdj.service.system.service.ISysAdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 商家控制器
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 19:23
 */
@Api(tags = "商家管理")
@RequestMapping("/seller")
@RestController
public class SellerController {
    @Autowired
    private ISysAdminService iSysAdminService;

    @ApiOperation(value = "商家分页展示", notes = "商家分页展示")
    @RequestMapping(value="list")
    public RespResult<PageData<AdminVo>> loadList(HttpServletRequest request, AdminQuery query, HttpReqParam reqParam) {
        query.setType(AdminType.TYPE_SELLER);
        if(!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<AdminVo> page = iSysAdminService.findAdminPageList(query, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "添加商家", notes = "添加商家")
    @PostMapping(value="save")
    public RespResult saveAdmin(HttpServletRequest request, SysAdmin admin, String bankids) {
        try {
            SysAdmin admin2= iSysAdminService.findSysAdminByMobile(admin.getMobile());
            if(admin2!=null) {
                return RespResult.error("账号已存在！");
            }

            // 判断是否选择了银行卡!
			/*if(StrKit.isNull(bankids)) {
				return RespResult.error("请选择对应的银行卡！");
			}*/

			// 默认密码
            admin.setPwd("123456");
            admin.setOprId(AdminRequestUtil.identify(request));
            admin.setCreaterId(AdminRequestUtil.identify(request));
            admin.setWbKey(StrUtil.getRandomString(18));
            admin.setType(AdminType.TYPE_SELLER);
            iSysAdminService.saveAdmin(admin);

			/*// 数据非空判断~
			if(null!=admin && !StrKit.isNull(admin.getRoleIds())) {
				// 保存人员对应的角色~
				iAdminRoleService.saveAdminRole(admin.getId(), admin.getRoleIds());
			}*/

			/*// 保存对应的银行卡!
			iAdminBankService.saveAdminBank(bankids, admin.getId());*/
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "逻辑删除商家", notes = "逻辑删除商家")
    @PostMapping(value="delete")
    public RespResult deleteAdmin(HttpServletRequest request,@ApiParam(value = "商家id") Long id) {
        try {
            iSysAdminService.logicDeleteAdmin(id, AdminRequestUtil.identify(request));
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "批量逻辑删除商家", notes = "批量逻辑删除商家")
    @PostMapping(value="deletes")
    public RespResult deleteAdmin(HttpServletRequest request,@ApiParam(value = "商家ids 1,2,3...") String ids) {
        try {
            iSysAdminService.logicDeleteAdmins(ids, AdminRequestUtil.identify(request));
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新商家", notes = "更新商家")
    @PostMapping(value="update")
    public RespResult updateAdmin(HttpServletRequest request,SysAdmin admin,String bankids) {
        try {
            SysAdmin admin2= iSysAdminService.findSysAdminByMobile(admin.getMobile());
            if(admin2!=null && !admin2.getId().equals(admin.getId())) {
                return RespResult.error("账号已存在！");
            }

            // 判断是否选择了银行卡!
			/*if(StrKit.isNull(bankids)) {
				return RespResult.error("请选择对应的银行卡！");
			}*/

            admin.setOprId(AdminRequestUtil.identify(request));
            iSysAdminService.updateAdmin(admin);

            // 数据非空判断~
			/*if(null!=admin && !StrKit.isNull(admin.getRoleIds())) {
				// 保存人员对应的角色~
				iAdminRoleService.saveAdminRole(admin.getId(), admin.getRoleIds());
			}*/

            // 保存对应的银行卡!
            /*iAdminBankService.saveAdminBank(bankids, admin.getId());*/
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "商家列表", notes = "商家列表")
    @RequestMapping(value="select", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult<List<SysAdmin>> findAdminSelect(HttpServletRequest request){
        Long orgId = null;
        if(!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        List<SysAdmin> list =iSysAdminService.findAdminByTypeAndOrgId(orgId);
        return RespResult.success(list);
    }



    @ApiOperation(value = "禁用商家登录账号", notes = "禁用商家登录账号")
    @PostMapping(value="prohibit")
    public RespResult adminProhibit(HttpServletRequest request,@ApiParam(value = "商家id") Long id) {
        try {
            iSysAdminService.updateStatus(id, Status.STATUS_LOCK, AdminRequestUtil.identify(request));
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "启用商家登录账号", notes = "启用商家登录账号")
    @PostMapping(value="enabling")
    public RespResult adminEnabling(HttpServletRequest request, @ApiParam(value = "商家id") Long id) {
        try {
            iSysAdminService.updateStatus(id, Status.STATUS_ACTIVITY, AdminRequestUtil.identify(request));
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }
}
