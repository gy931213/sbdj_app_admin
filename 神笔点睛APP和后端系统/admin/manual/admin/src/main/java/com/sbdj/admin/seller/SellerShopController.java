package com.sbdj.admin.seller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.seller.admin.query.SellerShopQuery;
import com.sbdj.service.seller.admin.vo.SellerShopVo;
import com.sbdj.service.seller.entity.SellerShop;
import com.sbdj.service.seller.service.ISellerShopService;
import com.sbdj.service.system.entity.SysAdmin;
import com.sbdj.service.system.service.ISysAdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 商家/卖方（店铺） 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "店铺控制器")
@RestController
@RequestMapping("/seller/shop")
public class SellerShopController {
    private Logger logger = LoggerFactory.getLogger(SellerShopController.class);
    @Autowired
    private ISellerShopService iSellerShopService;
    @Autowired
    private ISysAdminService iSysAdminService;

    @ApiOperation(value = "展示店铺列表", notes = "展示店铺列表")
    @GetMapping(value="list")
    public RespResult<PageData<SellerShopVo>> findSellerShopPage(HttpServletRequest request, HttpReqParam reqParam, SellerShopQuery query){
        if(!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<SellerShopVo> page = iSellerShopService.findSellerShopPageList(query, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "保存店铺", notes = "保存店铺")
    @PostMapping(value="save")
    public RespResult saveSellerShop(HttpServletRequest request, SellerShop sellerShop) {
        try {
            // 判断是否为空！
            if(StrUtil.isNull(sellerShop.getShopName())) {
                return RespResult.error("店铺名称为空！");
            }
            // 店铺名称进行空格处理！
            sellerShop.setShopName(StrUtil.removeAllBlank(sellerShop.getShopName()));

            // 判断该店铺数据是否存在！
            SellerShop shop =iSellerShopService.findSellerShopByPlatformIdAndShopName(sellerShop.getPlatformId(), sellerShop.getShopName(),AdminRequestUtil.orgId(request));
            if(null!=shop) {
                return RespResult.error("店铺名称已存在");
            }

            SysAdmin admin = iSysAdminService.getById(sellerShop.getAdminId());
            if(null==admin) {
                logger.info("保存数据失败，失败原因，根据商家id获取数据为null");
                return RespResult.error("保存失败");
            }
            Long identify = AdminRequestUtil.identify(request);
            sellerShop.setAdminId(sellerShop.getAdminId());
            sellerShop.setCreateId(identify);
            sellerShop.setOrgId(admin.getOrgId());
            iSellerShopService.saveSellerShop(sellerShop);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新店铺", notes = "更新店铺")
    @PostMapping(value="update")
    public RespResult updateSellerShop(HttpServletRequest request,SellerShop sellerShop) {
        try {
            Long orgId = AdminRequestUtil.orgId(request);
            // 获取店铺数据
            SellerShop shop =iSellerShopService.findSellerShopByPlatformIdAndShopName(sellerShop.getPlatformId(), sellerShop.getShopName(),orgId);
            if(null!=shop && !shop.getId().equals(sellerShop.getId()) && shop.getOrgId().equals(orgId)) {
                return RespResult.error("店铺名称已存在");
            }
            sellerShop.setOprId(AdminRequestUtil.identify(request));
            iSellerShopService.updateSellerShop(sellerShop);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }


    @ApiOperation(value = "删除店铺", notes = "删除店铺")
    @PostMapping(value="delete")
    public RespResult deleteSellerShop(HttpServletRequest request,Long id) {
        try {
            iSellerShopService.logicDeleteSellerShop(id,AdminRequestUtil.identify(request));
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "批量删除店铺", notes = "批量删除店铺")
    @PostMapping(value="deletes")
    public RespResult deleteSellerShops(HttpServletRequest request,String ids) {
        try {
            iSellerShopService.logicDeleteSellerShops(ids,AdminRequestUtil.identify(request));
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "店铺列表", notes = "店铺列表")
    @GetMapping(value="select")
    public RespResult findSellerShopSelect(HttpServletRequest request) {
        Long orgId = null;
        if(!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        List<SellerShop> list = iSellerShopService.findSellerShopByOrgId(orgId);
        return RespResult.success(list);
    }
}
