package com.sbdj.admin.seller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.JsoupUtil;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.seller.admin.query.SellerShopGoodsQuery;
import com.sbdj.service.seller.admin.vo.SellerShopGoodsVo;
import com.sbdj.service.seller.entity.SellerShopGoods;
import com.sbdj.service.seller.service.ISellerShopGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 商家/卖方（店铺商品） 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "商品控制器")
@RestController
@RequestMapping("/seller/shop/goods")
public class SellerShopGoodsController {
    @Autowired
    private ISellerShopGoodsService iSellerShopGoodsService;

    @ApiOperation(value = "分页展示商品", notes = "分页展示商品")
    @RequestMapping("list")
    public RespResult<PageData<SellerShopGoodsVo>> sellerShopGoodsList(HttpServletRequest request, HttpReqParam reqParam, SellerShopGoodsQuery query) {
        if(!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<SellerShopGoodsVo> page = iSellerShopGoodsService.findSellerShopGoodsList(query, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "保存商品", notes = "保存商品")
    @PostMapping("save")
    public RespResult saveSellerShopGoods(HttpServletRequest request, SellerShopGoods shopGoods) {
        try {
            shopGoods.setCreateId(AdminRequestUtil.identify(request));
			/*SellerShopGoods goods = iSellerShopGoodsService.findSellerShopGoodsByTitle(shopGoods.getShopId(), shopGoods.getTitle());
			if(null!=goods) {
				return RespResult.error("商品已存在");
			}*/
            iSellerShopGoodsService.saveSellerShopGoods(shopGoods);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }

    }

    @ApiOperation(value = "更新商品", notes = "更新商品")
    @PostMapping("update")
    public RespResult updateSellerShopGoods(SellerShopGoods shopGoods, HttpServletRequest request) {
        try {
            shopGoods.setOprId(AdminRequestUtil.identify(request));
            iSellerShopGoodsService.updateSellerShopGoods(shopGoods);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "删除商品", notes = "删除商品")
    @PostMapping("delete")
    public RespResult deleteSellerShopGoods(HttpServletRequest request,Long id) {
        try {
            iSellerShopGoodsService.logicDeleteSellerShopGoods(id, AdminRequestUtil.identify(request));
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "是否开启商品打标", notes = "是否开启商品打标")
    @PostMapping("update/marking")
    public RespResult updateSellerShopGoods(Long id, Integer isCard) {
        try {
            iSellerShopGoodsService.updateSellerShopGoodsByIsCard(id, isCard);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "获取商品信息", notes = "获取商品信息")
    @GetMapping(value = "/info")
    public RespResult getShopGoods(String link) {
        try {
            if(!StrUtil.indexOf(link, "http://") && !StrUtil.indexOf(link, "https://")) {
                return RespResult.error("请求链接不正确");
            }
            JsoupUtil.RespElement re = JsoupUtil.analysisShopGoods(link);
            Map<String, Object> map = new HashMap<>();
            map.put("shop", re);
            map.put("resp", JSON.toJSON(re));
            return RespResult.success(map);
        } catch (Exception e) {
            return RespResult.error(e.getMessage());
        }
    }
}
