package com.sbdj.admin.shiro;

import com.sbdj.core.util.MD5Util;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysAdmin;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * <p>
 * 自定义验证
 * </p>
 *
 * @author Yly
 * @since 2019/11/21 9:27
 */
public class MyCredentalsMatcher extends SimpleCredentialsMatcher {
    /**
     * 重写父类的密码比较的方法
     * 比较的算法：
     *    1.接收用户在密码框输入的明文
     *    2.使用Md5Hash算法进行加密
     *    3.获取数据库中的加密的密码进行比较
     * @author Yly
     * @date 2019/11/21 09:31
     * @param token 代表用户在界面上输入的用户名和密码
     * @param info 它内部会包含数据库中的密码（当前用户加密后的密码）
     * @return boolean 如果返回true代表密码验证成功，如果返回false代表密码比对失败，失败后程序就会出现异常
     */
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        // 1.向下转型 获取口令信息
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        // 2.获取用户信息
        SysAdmin sysAdmin = null;
        PrincipalCollection principals = info.getPrincipals();
        Object obj = principals.getPrimaryPrincipal();
        if (obj instanceof SysAdmin) {
            sysAdmin = (SysAdmin) obj;
        }
        if (StrUtil.isNull(sysAdmin)) {
            return false;
        }
        // 3.获取密码
        String credentials = (String) info.getCredentials();
        // 4.使用MD5算法进行加密
        String pwd = MD5Util.str2MD5(new String(usernamePasswordToken.getPassword()) + sysAdmin.getWbKey());
        return credentials.equals(pwd);
    }
}
