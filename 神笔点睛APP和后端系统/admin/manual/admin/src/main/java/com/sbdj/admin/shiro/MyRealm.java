package com.sbdj.admin.shiro;

import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.WebSession;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysAdmin;
import com.sbdj.service.system.service.ISysAdminService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 自定义认证口令
 * </p>
 *
 * @author Yly
 * @since 2019/11/21 9:27
 */
public class MyRealm extends AuthorizingRealm {
    @Autowired
    private ISysAdminService iSysAdminService;
    @Autowired
    private HttpServletRequest request;


    /**
     * 授权方法
     * @author Yly
     * @date 2019/11/21 9:29
     * @param principalCollection 用户信息
     * @return org.apache.shiro.authz.AuthorizationInfo
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        WebSession session = (WebSession) AdminRequestUtil.getSession(request);
        info.addStringPermissions(session.permissions());
        return info;
    }

    /**
     * 认证方法
     * @author Yly
     * @date 2019/11/21 9:30
     * @param authenticationToken 认证信息
     * @return org.apache.shiro.authc.AuthenticationInfo
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 1.向下转型获取口令信息
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
        // 2.获取手机号
        String phone = usernamePasswordToken.getUsername();
        // 根据账号获取登录人信息
        SysAdmin admin = iSysAdminService.findSysAdminByMobile(phone);
        if(null == admin) {
            throw new UnknownAccountException("当前登录账号不存在");
        }
        if (StrUtil.isNull(admin.getStatus())) {
            throw new AccountException("当前登录帐号异常，请联系管理员！");
        }
        if(admin.getStatus().equals(Status.STATUS_DELETE)) {
            throw new LockedAccountException("当前登录帐号已冻结，请联系管理员！");
        }
        if(admin.getStatus().equals(Status.STATUS_LOCK)) {
            throw new DisabledAccountException("当前登录帐号已禁用，请联系管理员！");
        }
        if(StrUtil.isNull(admin.getWbKey())) {
            throw new CredentialsException("凭证异常");
        }
        return new SimpleAuthenticationInfo(admin, admin.getPwd(), getName());
    }

    @PostConstruct
    public void initCustomCredentialsMatcher() {
        //该句作用是重写shiro的密码验证，让shiro用我自己的验证
        setCredentialsMatcher(new MyCredentalsMatcher());
    }
}
