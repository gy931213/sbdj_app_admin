package com.sbdj.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.system.admin.query.AdminApiQuery;
import com.sbdj.service.system.admin.vo.AdminApiVo;
import com.sbdj.service.system.service.ISysAdminApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 管理员记录 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "管理员记录")
@RequestMapping("/sys/admin/api")
@RestController
public class SysAdminApiController {
    @Autowired
    private ISysAdminApiService iSysAdminApiService;

    @ApiOperation(value = "管理员记录列表", tags = "管理员记录列表")
    @GetMapping("/list")
    public RespResult<PageData<AdminApiVo>> list(AdminApiQuery query, HttpReqParam reqParam) {
        IPage<AdminApiVo> page = iSysAdminApiService.findAdminApiPageByParams(query, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }
}
