package com.sbdj.admin.system;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 管理员[商家]-对应的银行开表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@RestController
@RequestMapping("/sys/admin/bank")
public class SysAdminBankController {

}
