package com.sbdj.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.constant.AdminType;
import com.sbdj.admin.web.WebSession;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.admin.query.AdminQuery;
import com.sbdj.service.system.admin.vo.AdminInfoVo;
import com.sbdj.service.system.admin.vo.AdminVo;
import com.sbdj.service.system.entity.SysAdmin;
import com.sbdj.service.system.service.ISysAdminRoleService;
import com.sbdj.service.system.service.ISysAdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 管理员表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Api(tags = "管理员控制器")
@RestController
@RequestMapping("/sys/admin")
public class SysAdminController {
    @Autowired
    private ISysAdminService iSysAdminService;
    @Autowired
    private ISysAdminRoleService iSysAdminRoleService;

    @ApiOperation(value = "管理员列表", notes = "管理员列表")
    @RequestMapping(value="list", method = {RequestMethod.GET, RequestMethod.POST})
    public RespResult<PageData<AdminVo>> loadList(HttpServletRequest request, AdminQuery query, HttpReqParam reqParam) {
        query.setType(AdminType.TYPE_ADMIN);
        if(!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<AdminVo> page = iSysAdminService.findAdminPageList(query, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "保存管理员", notes = "保存管理员")
    @PostMapping(value="save")
    public RespResult saveAdmin(HttpServletRequest request, SysAdmin admin, @ApiParam(value = "角色ids 1,2,3...") String roleIds) {
        try {
            // ~判断所属机构中是否存在相同的账号数据信息
            SysAdmin obj = iSysAdminService.findSysAdminByMobile(admin.getMobile());
            if(StrUtil.isNotNull(obj)) {
                return RespResult.error("账号已存在！");
            }
            admin.setCreaterId(AdminRequestUtil.identify(request));
            admin.setWbKey(StrUtil.getRandomString(18));
            admin.setType(AdminType.TYPE_ADMIN);
            // 默认非系统管理员
            admin.setIsSuper("0");
            iSysAdminService.saveAdmin(admin);

            // 数据非空判断~
            if(StrUtil.isNotNull(admin.getId()) && StrUtil.isNotNull(roleIds)) {
                // 保存人员对应的角色~
                iSysAdminRoleService.saveAdminRole(admin.getId(), roleIds);
            }
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "逻辑删除管理员", notes = "逻辑删除管理员")
    @PostMapping(value="delete")
    public RespResult deleteAdmin(HttpServletRequest request,@ApiParam(value = "管理员id") Long id) {
        try {
            iSysAdminService.logicDeleteAdmin(id, AdminRequestUtil.identify(request));
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "批量逻辑删除管理员", notes = "批量逻辑删除管理员")
    @PostMapping(value="deletes")
    public RespResult deleteAdmin(HttpServletRequest request,@ApiParam(value = "管理员ids 1,2,3...") String ids) {
        try {
            iSysAdminService.logicDeleteAdmins(ids, AdminRequestUtil.identify(request));
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新管理员", notes = "更新管理员")
    @PostMapping(value="update")
    public RespResult updateAdmin(HttpServletRequest request,SysAdmin admin,@ApiParam("角色ids 1,2,3...") String roleIds) {
        try {
            // ~判断所属机构中是否存在相同的账号数据信息
            SysAdmin obj = iSysAdminService.findSysAdminByMobile(admin.getMobile());
            if(null!=obj && !obj.getId().equals(admin.getId()) && obj.getMobile().equals(admin.getMobile())) {
                return RespResult.error("账号已存在！");
            }
            // 默认非系统管理员
            admin.setIsSuper("0");
            // 操作人
            admin.setOprId(AdminRequestUtil.identify(request));
            // 管理员拥有的角色ids
            admin.setRoleIds(roleIds);
            iSysAdminService.updateAdmin(admin);

            // 数据非空判断~
            if(StrUtil.isNotNull(admin.getId()) && StrUtil.isNotNull(roleIds)) {
                // 保存人员对应的角色~
                iSysAdminRoleService.saveAdminRole(admin.getId(), roleIds);
            }
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "修改管理员的状态", notes = "修改管理员的状态")
    @PostMapping(value="status")
    public RespResult updateAdminStatusByAdminId(HttpServletRequest request,@ApiParam(value = "管理员id") Long id,
                                                 @ApiParam(value = "状态 {E:启用 D:禁用}") String status) {
        if(null==id || null==status || status.isEmpty()) {
            return RespResult.error();
        }
        try {
            iSysAdminService.updateStatus(id, status, AdminRequestUtil.identify(request));
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "获取管理员信息", notes = "获取管理员信息")
    @RequestMapping("/info/get")
    public RespResult<AdminInfoVo> adminInfo(HttpServletRequest request) {
        AdminInfoVo aiv = iSysAdminService.findAdminInfoVoByAdminId(AdminRequestUtil.identify(request));
        return RespResult.success(aiv);
    }

    @ApiOperation(value = "更新管理员头像", notes = "更新管理员头像")
    @PostMapping("/info/update")
    public RespResult updateAdminInfoHeadUrl(HttpServletRequest request,@ApiParam(value = "管理员id") Long id,
                                             @ApiParam(value = "头像url") String headUrl) {
        try {
            iSysAdminService.updateAdminHeadUrlById(id, headUrl);
        } catch (Exception e) {
            return RespResult.error();
        }
        // 更新当前登录人头像
        WebSession ws = (WebSession) AdminRequestUtil.getSession(request);
        ws.setHeadUrl(headUrl);
        AdminRequestUtil.setSession(request, ws);
        return RespResult.success();
    }

    @ApiOperation(value = "获取管理员列表", notes = "获取管理员列表")
    @RequestMapping(value="select", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult<List<SysAdmin>> findAdminSelect(HttpServletRequest request) {
        List<SysAdmin> list;
        if (AdminRequestUtil.isSuper(request)) {
            list = iSysAdminService.findAdminSelect(null);
        }else {
            list = iSysAdminService.findAdminSelect(AdminRequestUtil.orgId(request));
        }
        return RespResult.success(list);
    }
}
