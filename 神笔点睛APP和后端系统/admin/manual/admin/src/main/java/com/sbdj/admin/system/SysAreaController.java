package com.sbdj.admin.system;


import com.sbdj.admin.web.RespResult;
import com.sbdj.core.constant.Number;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysArea;
import com.sbdj.service.system.service.ISysAreaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 省市区表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Api(tags = "省市区控制器")
@RestController
@RequestMapping("/sys/area")
public class SysAreaController {
    @Autowired
    private ISysAreaService iSysAreaService;

    @ApiOperation(value = "获取欧省市区数据", notes = "获取省市区数据")
    @RequestMapping(value = "select", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult<List<SysArea>> findAreaData(@ApiParam(value = "父级id,根目录默认为-1") String parentId) {
        /*if (StrUtil.isNull(parentId)) {
            return RespResult.error();
        }*/
        if (StrUtil.isNotNull(parentId) && "null".equalsIgnoreCase(parentId)) {
            return RespResult.error();
        }
        List<SysArea> list = iSysAreaService.findAreaByParentId(StrUtil.isNull(parentId)? Number.LONG_NEGATIVE:Long.parseLong(parentId));
        return RespResult.success(list);
    }
}
