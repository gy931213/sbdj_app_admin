package com.sbdj.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.system.entity.SysConfig;
import com.sbdj.service.system.entity.SysConfigDtl;
import com.sbdj.service.system.service.ISysConfigDtlService;
import com.sbdj.service.system.service.ISysConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 系统配置表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Api(tags = "系统配置")
@RestController
@RequestMapping("/sys/config")
public class SysConfigController {
    @Autowired
    private ISysConfigService iSysConfigService;
    @Autowired
    private ISysConfigDtlService iSysConfigDtlService;

    @ApiOperation(value = "系统配置列表", notes = "系统配置列表")
    @GetMapping(value = "list")
    public RespResult<List<SysConfig>> findList() {
        List<SysConfig> list = iSysConfigService.findSysConfigList();
        return RespResult.success(list);
    }

    @ApiOperation(value = "添加系统配置", notes = "添加系统配置")
    @PostMapping(value = "add")
    public RespResult addConfig(SysConfig config) {
        try {
            iSysConfigService.saveConfig(config);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新系统配置", notes = "更新系统配置")
    @PostMapping(value = "update")
    public RespResult updateConfig(SysConfig config) {
        try {
            iSysConfigService.updateConfig(config);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "删除系统配置", notes = "删除系统配置")
    @PostMapping(value = "delete")
    public RespResult deleteConfig(@ApiParam(value = "系统配置id") Long id) {
        try {
            iSysConfigService.logicDeleteConfig(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "批量删除系统配置", notes = "批量删除系统配置")
    @PostMapping(value = "deletes")
    public RespResult deleteConfig(@ApiParam(value = "系统配置ids") String ids) {
        try {
            iSysConfigService.logicDeleteConfigs(ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "展示详细系统配置", notes = "展示详细系统配置")
    @GetMapping(value = "dtl/list")
    public RespResult<PageData<SysConfigDtl>> findDtlList(HttpReqParam reqParam,@ApiParam(value = "系统配置id") Long id) {
        IPage<SysConfigDtl> list = iSysConfigDtlService.findConfigDtlList(id, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(list);
    }

    @ApiOperation(value = "添加详细系统配置", notes = "添加详细系统配置")
    @PostMapping(value = "dtl/add")
    public RespResult addConfigDtl(SysConfigDtl configDtl) {
        try {
            List<SysConfigDtl> configDtlList = iSysConfigDtlService.findConfigDtl(configDtl.getConfigId());
            if (null != configDtlList) {
                for (SysConfigDtl sysConfigDtl : configDtlList) {
                    if (sysConfigDtl.getSort().equals(configDtl.getSort())) {
                        //新增
                        if (configDtl.getId() == null) {
                            return RespResult.error("请输入正确的排序号");
                        } else if (!configDtl.getId().equals(sysConfigDtl.getId())) {
                            return RespResult.error("请输入正确的排序号");
                        }
                    }
                }
            }
            configDtl.setConfigId(configDtl.getConfigId());
            iSysConfigDtlService.saveConfigDtl(configDtl);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新详细系统配置", notes = "更新详细系统配置")
    @PostMapping(value = "dtl/update")
    public RespResult updateConfigDtl(SysConfigDtl configDtl) {
        try {
            List<SysConfigDtl> configDtlList = iSysConfigDtlService.findConfigDtl(configDtl.getConfigId());
            if (null != configDtlList) {
                for (SysConfigDtl sysConfigDtl : configDtlList) {
                    if (sysConfigDtl.getSort().equals(configDtl.getSort())) {
                        //新增
                        if (configDtl.getId() == null) {
                            return RespResult.error("请输入正确的排序号");
                        } else if (!configDtl.getId().equals(sysConfigDtl.getId())) {
                            return RespResult.error("请输入正确的排序号");
                        }
                    }
                }
            }
            configDtl.setConfigId(configDtl.getConfigId());
            iSysConfigDtlService.updateConfigDtl(configDtl);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "删除详细系统配置", notes = "删除详细系统配置")
    @GetMapping(value = "dtl/delete")
    public RespResult deleteConfigDtl(@ApiParam(value = "详细系统配置id") Long id) {
        try {
            iSysConfigDtlService.logicDeleteConfigDtl(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "批量删除详细系统配置", notes = "批量删除详细系统配置")
    @GetMapping(value = "dtl/deletes")
    public RespResult deleteConfigDtls(@ApiParam(value = "详细系统配置ids 1,2,3...") String ids) {
        try {
            iSysConfigDtlService.logicDeleteConfigDtls(ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }
}
