package com.sbdj.admin.system;


import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.service.system.entity.SysFunction;
import com.sbdj.service.system.service.ISysFunctionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 功能表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Api(tags = "功能(权限)控制器")
@RestController
@RequestMapping("/sys/function")
public class SysFunctionController {
    @Autowired
    private ISysFunctionService iSysFunctionService;

    @ApiOperation(value = "展示所有功能菜单", notes = "展示所有功能菜单")
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public RespResult<List<SysFunction>> findFunctionPageList() {
        List<SysFunction> list = iSysFunctionService.findFunctionPageList();
        return RespResult.success(list);
    }

    @ApiOperation(value = "已授权的功能菜单", notes = "已授权的功能菜单")
    @PostMapping(value = "authorize")
    public RespResult<List<SysFunction>> findFunctionList(HttpServletRequest request) {
        List<SysFunction> data;
        if (AdminRequestUtil.isSuper(request)) {
            data = iSysFunctionService.findFunctionPageList();
        } else {
            data = iSysFunctionService.findFunctionList(AdminRequestUtil.orgId(request));
        }
        return RespResult.success(data);
    }

    @ApiOperation(value = "保存功能菜单", notes = "保存功能菜单")
    @PostMapping(value="save")
    public RespResult saveFunction(SysFunction function) {
        try {
            iSysFunctionService.saveFunction(function);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新功能菜单", notes = "更新功能菜单")
    @PostMapping(value="update")
    public RespResult updateFunction(SysFunction function) {
        try {
            iSysFunctionService.updateFunction(function);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "显示功能菜单", notes = "显示功能菜单")
    @RequestMapping(value="byroleid", method = {RequestMethod.GET, RequestMethod.POST})
    public RespResult<List<SysFunction>> findFunctionByRoleId(@ApiParam(value = "角色ID") Long roleId){
        List<Long> funcIds = iSysFunctionService.getRoleFunctionByRoleId(roleId);
        if(funcIds.isEmpty()) {
            return RespResult.success();
        }
        List<SysFunction> list = iSysFunctionService.findFunctionByIds(funcIds);
        return RespResult.success(list);
    }

    @ApiOperation(value = "回显功能菜单", notes = "回显功能菜单")
    @PostMapping(value="echo")
    public RespResult<List<SysFunction>> echoFunctionByOrgId(@ApiParam(value = "角色ID") Long orgId){
        List<SysFunction> list = iSysFunctionService.findFunctionList(orgId);
        return RespResult.success(list);
    }

    @ApiOperation(value = "删除权限", notes = "删除权限")
    @PostMapping(value = "/delete")
    public RespResult deleteFunction(@ApiParam(value = "权限id") Long id) {
        try {
            iSysFunctionService.removeById(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "批量删除权限", notes = "批量删除权限")
    @PostMapping(value = "/deletes")
    public RespResult deleteFunction(@ApiParam(value = "权限ids") String ids) {
        try {
            iSysFunctionService.deleteFunctionByIds(ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }
}
