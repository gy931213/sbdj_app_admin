package com.sbdj.admin.system;


import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.service.system.entity.SysOrganization;
import com.sbdj.service.system.entity.SysRole;
import com.sbdj.service.system.service.ISysOrgFunctionService;
import com.sbdj.service.system.service.ISysOrganizationService;
import com.sbdj.service.system.service.ISysRoleFunctionService;
import com.sbdj.service.system.service.ISysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 组织表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Api(tags = "组织架构控制器")
@RestController
@RequestMapping("/sys/organization")
public class SysOrganizationController {
    @Autowired
    private ISysOrganizationService iSysOrganizationService;
    @Autowired
    private ISysOrgFunctionService iSysOrgFunctionService;
    @Autowired
    private ISysRoleService iSysRoleService;
    @Autowired
    private ISysRoleFunctionService iSysRoleFunctionService;


    @ApiOperation(value = "组织列表", notes = "展示组织")
    @RequestMapping(value="list", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult<List<SysOrganization>> loadOrganization() {
        List<SysOrganization> list = iSysOrganizationService.findOrganizationList();
        return RespResult.success(list);
    }

    @ApiOperation(value = "保存组织", notes = "保存组织")
    @PostMapping(value="save")
    public RespResult saveOrganization(HttpServletRequest request, SysOrganization organization) {
        try {
            organization.setCreateId(AdminRequestUtil.identify(request));
            iSysOrganizationService.saveOrganization(organization);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新组织", notes = "更新组织")
    @RequestMapping(value="update", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult updateOrganization(HttpServletRequest request,SysOrganization organization) {
        try {
            organization.setOprId(AdminRequestUtil.identify(request));
            iSysOrganizationService.updateOrganization(organization);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "删除组织", notes = "删除组织")
    @RequestMapping(value="delete", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult deleteOrganization(HttpServletRequest request,@ApiParam(value = "组织ids 1,2,3...") String ids) {
        try {
            iSysOrganizationService.deleteOrganization(ids,AdminRequestUtil.identify(request));
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "组织选项", notes = "组织选项")
    @RequestMapping(value="select", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult<List<SysOrganization>> findOrganizationSelect(HttpServletRequest request){
        List<SysOrganization> list;
        if(!AdminRequestUtil.isSuper(request)) {
            list = iSysOrganizationService.findOrganizationSelect(AdminRequestUtil.orgId(request));
        }else {
            list = iSysOrganizationService.findOrganizationSelect();
        }
        return RespResult.success(list);
    }

    @ApiOperation(value = "组织授权", notes = "组织授权")
    @PostMapping(value = "authorize")
    public RespResult authorizeOrganization(HttpServletRequest request,@ApiParam(value = "组织id") Long orgId,@ApiParam(value = "权限ids 1,2,3...") String funcIds) {
        try {
            iSysOrgFunctionService.saveOrgFunction(orgId,AdminRequestUtil.identify(request),funcIds);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "组织初始化", notes = "组织初始化")
    @PostMapping(value = "init")
    public RespResult initRole(@ApiParam(value = "组织id") Long orgId,@ApiParam(value = "权限ids 1,2,3...") String funcIds) {
        SysRole role = new SysRole();
        role.setName("管理员");
        role.setOrgId(orgId);
        role.setCode("GLY");
        role.setDescription("管理员");
        try {
            iSysRoleService.saveRole(role);
            iSysRoleFunctionService.saveRoleFunction(role.getId(),funcIds);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }





    @ApiOperation(value = "查询组织结构数据",notes = "查询组织结构数据")
    @RequestMapping(value = "/org/select")
    public RespResult<List<SysOrganization>> findOrganizationSelsect(@ApiParam(value = "http请求信息") HttpServletRequest request) {
        List<SysOrganization> list = null;

        if (!AdminRequestUtil.isSuper(request)) {
            list = iSysOrganizationService.findOrganizationSelect(AdminRequestUtil.orgId(request));
        } else {
            list = iSysOrganizationService.findOrganizationSelect();
        }
        return RespResult.success(list);
    }

}
