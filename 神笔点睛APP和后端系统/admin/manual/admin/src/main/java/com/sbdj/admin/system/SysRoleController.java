package com.sbdj.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.system.admin.vo.SysRoleVo;
import com.sbdj.service.system.entity.SysAdminRole;
import com.sbdj.service.system.entity.SysRole;
import com.sbdj.service.system.service.ISysAdminRoleService;
import com.sbdj.service.system.service.ISysRoleFunctionService;
import com.sbdj.service.system.service.ISysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 角色表
 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Api(tags = "角色控制器")
@RestController
@RequestMapping("/sys/role")
public class SysRoleController {
    @Autowired
    private ISysRoleService iSysRoleService;
    @Autowired
    private ISysRoleFunctionService iSysRoleFunctionService;
    @Autowired
    private ISysAdminRoleService iSysAdminRoleService;

    @ApiOperation(value = "保存角色", notes = "保存角色")
    @PostMapping(value="save")
    public RespResult saveRole(SysRole role) {
        try {
            iSysRoleService.saveRole(role);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新角色", notes = "更新角色")
    @PostMapping(value="update")
    public RespResult updateRole(SysRole role) {
        try {
            iSysRoleService.updateRole(role);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "逻辑删除角色", notes = "逻辑删除角色")
    @PostMapping(value="delete")
    public RespResult deleteRole(@ApiParam(value = "角色id") Long id) {
        try {
            iSysRoleService.logicDeleteRole(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "批量逻辑删除", notes = "批量逻辑删除")
    @PostMapping(value="deletes")
    public RespResult deleteRole(@ApiParam(value = "角色ids 1,2,3..") String ids) {
        try {
            iSysRoleService.logicDeleteRoles(ids);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "分页展示角色数据", notes = "分页展示角色数据")
    @RequestMapping(value="list", method = {RequestMethod.GET,RequestMethod.POST})
    public RespResult<PageData<SysRoleVo>> rolePageList(HttpServletRequest request, HttpReqParam reqParam) {
        if(!AdminRequestUtil.isSuper(request)) {
            reqParam.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<SysRoleVo> page = iSysRoleService.findRolePage(reqParam.getKeyword(), reqParam.getOrgId(), PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "保存角色对应的权限", notes = "保存角色对应的权限")
    @PostMapping(value="save/rf")
    public RespResult saveRoleFunctionData(@ApiParam(value = "角色id") Long roleId, @ApiParam(value = "权限ids 1,2,3...") String funcIds) {
        if(roleId==null || funcIds.isEmpty()) {
            return RespResult.error();
        }
        try {
            // 先处理历史数据，物理删除数据!
            iSysRoleFunctionService.deleteRoleFunctionByRoleIds(roleId.toString());

            // 保存角色数据权限数据!
            iSysRoleFunctionService.saveRoleFunction(roleId, funcIds);
            return RespResult.success();
        } catch (Exception e) {
            e.printStackTrace();
            return RespResult.error();
        }
    }

    @ApiOperation(value = "通过组织id获取角色列表", notes = "通过组织id获取角色列表")
    @RequestMapping(value="select",method=RequestMethod.GET)
    public RespResult<List<SysRole>> findRoleSelect(@ApiParam(value = "组织id") Long orgId) {
        List<SysRole> list = iSysRoleService.findRoleSelect(orgId);
        return RespResult.success(list);
    }

    @ApiOperation(value = "通过管理员id获取角色列表", notes = "通过管理员id获取角色列表")
    @PostMapping(value="byadminid")
    public RespResult<List<SysAdminRole>> findRoleByAdmin(@ApiParam(value = "管理员id") Long adminId) {
        List<SysAdminRole> list = iSysAdminRoleService.findAdminRoleByAdminId(adminId);
        return RespResult.success(list);
    }
}
