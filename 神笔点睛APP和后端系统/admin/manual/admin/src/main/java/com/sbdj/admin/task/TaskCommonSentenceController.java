package com.sbdj.admin.task;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.task.admin.query.TaskCommonSentenceQuery;
import com.sbdj.service.task.admin.vo.TaskCommonSentenceVo;
import com.sbdj.service.task.entity.TaskCommonSentence;
import com.sbdj.service.task.service.ITaskCommonSentenceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 任务审核常用的语句 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Api(tags = "审核常用语句控制器")
@RestController
@RequestMapping("/task/common/sentence")
public class TaskCommonSentenceController {


    @Autowired
    private ITaskCommonSentenceService iTaskCommonSentenceService;
    
    @ApiOperation(value = "查询所有的数据",notes = "查询所有的数据")
    @RequestMapping("/show")
    public RespResult show() {
        List<TaskCommonSentence> taskCommonSentenceList = iTaskCommonSentenceService.findTaskCommonSentenceList();
        return RespResult.success(taskCommonSentenceList);
    }


    @ApiOperation(value = "常用语句列表分页查询数据",notes = "根据机构编号与平台编号分页查询数据")
    @RequestMapping("/list")
    public  RespResult<PageData<TaskCommonSentenceVo>> list(HttpServletRequest request, TaskCommonSentenceQuery query, HttpReqParam param) {
        if (!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<TaskCommonSentenceVo> page = iTaskCommonSentenceService.findTaskCommonSentenceListByOrgIdAndPlatformIdPageList(query, PageRequestUtil.buildPageRequest(param));
        return  RespResult.buildPageData(page);
    }

    @ApiOperation(value = "通过机构号、平台编号与类型查询数据",notes = "通过机构号、平台编号与类型查询数据")
    @RequestMapping("/select")
    public RespResult select(HttpServletRequest request, Long platformId, String type) {
        Long orgId = null;
        if (!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        List<TaskCommonSentence> taskCommonSentenceList = iTaskCommonSentenceService.findTaskCommonSentenceListByOrgIdAndPlatformIdAndType(orgId, platformId, type);
        if (null == taskCommonSentenceList) {
            return RespResult.error("没有数据");
        }
        return RespResult.success(taskCommonSentenceList);
    }

    @ApiOperation(value = "保存数据",notes = "保存数据")
    @RequestMapping("/save")
    public RespResult save(TaskCommonSentence taskCommonSentence) {
        if (null == taskCommonSentence) {
            return RespResult.error("数据为空");
        }
        try {
            iTaskCommonSentenceService.saveaskCommonSentence(taskCommonSentence);
        } catch (Exception e) {
            return RespResult.error("数据保存失败");
        }
        return RespResult.success();
    }


    @ApiOperation(value = "修改数据",notes = "修改数据")
    @RequestMapping("/update")
    public RespResult update(TaskCommonSentence taskCommonSentence) {
        if (null == taskCommonSentence) {
            return RespResult.error("数据为空");
        }
        try {
            iTaskCommonSentenceService.updateTaskCommonSentence(taskCommonSentence);
        } catch (Exception e) {
            e.printStackTrace();
            return RespResult.error("更新失败");
        }
        return RespResult.success();
    }


    @ApiOperation(value = "删除数据",notes = "逻辑删除")
    @RequestMapping("/delete")
    public RespResult delete(Long id) {
        try {
            iTaskCommonSentenceService.deleteTaskCommonSentence(id);
        } catch (Exception e) {
            return RespResult.error("删除失败");
        }
        return RespResult.success();
    }

    @ApiOperation(value = "更新状态",notes = "更新状态")
    @RequestMapping("/status")
    public RespResult status(Long id, String status) {
        try {
            iTaskCommonSentenceService.updateTaskCommonSentenceStatus(id, status);
        } catch (Exception e) {
            return RespResult.error("禁用失败");
        }
        return RespResult.success();
    }
}
