package com.sbdj.admin.task;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.base.TaskRedis;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.task.admin.query.TaskQuery;
import com.sbdj.service.task.admin.vo.TaskVo;
import com.sbdj.service.task.entity.Task;
import com.sbdj.service.task.entity.TaskKeyword;
import com.sbdj.service.task.service.ITaskKeywordService;
import com.sbdj.service.task.service.ITaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Api(tags = "任务主表控制器")
@RestController
@RequestMapping("/task")
public class TaskController {

    private Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private ITaskService iTaskService;
    @Autowired
    private ITaskKeywordService iTaskKeywordService;



    @ApiOperation(value = "展示所有任务信息", notes = "根据条件查询任务信息")
    @RequestMapping(value = "list")
    public RespResult<PageData<TaskVo>> finaTaskPageList(@ApiParam(value = "Http请求") HttpServletRequest request
            , @ApiParam(value = "任务查询条件")TaskQuery query,@ApiParam(value = "分页信息") HttpReqParam reqParam){
        if(!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<TaskVo> page = iTaskService.findTaskPageList(query, PageRequestUtil.buildPageRequest(reqParam));
        return  RespResult.buildPageData(page);
    }

    @ApiOperation(value = "查看任务详细信息", notes = "展示所有任务信息")
    @RequestMapping(value = "findkeyword")
    public RespResult findTaskKeywordDataByTaskIdAndStatus(@ApiParam(value = "任务ID") Long taskId
            ,@ApiParam(value = "任务状态") String status) {
        List<TaskKeyword> list = iTaskKeywordService.findAllByTaskIdAndStatus(taskId, status);
        return RespResult.success(list);
    }

    @ApiOperation(value = "更新状态：终止，运行", notes = "更新状态：终止，运行")
    @PostMapping("status")
    public RespResult updateTaskStatusByTaskId(@ApiParam(value = "Http请求") HttpServletRequest request
            , @ApiParam(value = "任务ID")Long taskId,@ApiParam(value = "任务状态") String status,@ApiParam(value = "状态：YES,NO") String type) {
        try {
            // 当前操作的任务数据
            Task task =	iTaskService.findTaskOne(taskId);
            if(StrUtil.isNull(task)) {
                return RespResult.error("该任务不存在！");
            }
            // 当前登录人id（操作人id）
            Long oprId = AdminRequestUtil.identify(request);

            // if “R” 运行
            if(status.equalsIgnoreCase(Status.STATUS_RELEASED)) {
                // if 【YES|NO】
                if(StrUtil.isNotNull(type) && type.equalsIgnoreCase(Status.STATUS_YES)) {
                    // 运行，改变定时发布时间，定时发布时间为当前时间 ! （“改变定时发布时间”）
                    iTaskService.updateStatusRun(taskId, AdminRequestUtil.identify(request), status);
                }else {
                    // 运行任务不改变定时发布时间 ! （“不改变定时发布时间”）
                    iTaskService.updateTaskStatusRunTimingTime(taskId, oprId, status);
                }

                // Redis_缓存任务可领数量。

                if(!TaskRedis.setTaskTotalCacheKey(task.getId() ,task.getTaskResidueTotal())){
                    logger.error("===>【任务更新，并且缓存“任务可领数量”失败！要缓存的ID：{}，任务编号为：{}】",task.getId(),task.getTaskNumber());
                }

                // Redis_缓存任务数据。
                //if(!RedisKit.set(TaskRedis.getTaskDataCachKey(task.getId()),task)){
                //	logger.error("===>【任务更新，并且缓存“任务数据”失败！要缓存的ID:{}，任务编号为：{}】",task.getId(),task.getTaskNumber());
                //}

                // else if “N” 终止
            }else if(status.equalsIgnoreCase(Status.STATUS_STOP)){
                // 终止任务
                iTaskService.updateStatusStop(taskId, oprId, status);

                // 删除——Redis_缓存任务可领数量。
                TaskRedis.remove(task.getId());
                // 删除——Redis_缓存任务数据。
                //RedisKit.del(TaskRedis.getTaskDataCachKey(task.getId()));
            }
        } catch (Exception e) {
            logger.error("===>[发布|终止]任务异常，异常原因：{}",e.getMessage());
            return RespResult.error();
        }
        return RespResult.success();
    }


    @ApiOperation(value = "逻辑删除任务数据", notes = "根据任务ID逻辑删除任务数据")
    @PostMapping("/deletes")
  //  @AuthValidate(AuthCode.TaskDeletes)
    public RespResult deleteTask(@ApiParam(value = "Http请求") HttpServletRequest request, @ApiParam(value = "批量任务IDs") String ids) {
        if(StrUtil.isNull(ids)) {
            return RespResult.error("缺少参数！");
        }
        List<Long> list = StrUtil.getIdsByLong(ids);
        if(StrUtil.isNull(list)) {
            return RespResult.error("缺少参数！");
        }
        try {
            for (Long id : list) {
                iTaskService.logiceDeleteTask(id, AdminRequestUtil.identify(request));
            }
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }


    @ApiOperation(value = "置顶", notes = "根据任务ID置顶")
    @PostMapping("settop")
  //  @AuthValidate(AuthCode.TaskSetTop)
    public RespResult setTaskTop(@ApiParam(value = "Http请求") HttpServletRequest request, @ApiParam(value = "任务ID") Long taskId) {
        try {
            iTaskService.setTaskTopByTaskId(taskId, AdminRequestUtil.identify(request));
        }catch (BaseException e) {
            System.out.println(e.getMessage());
            return RespResult.error(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return RespResult.success();
    }


    @ApiOperation(value = "取消置顶", notes = "根据任务ID取消置顶")
    @PostMapping("canceltop")
  //  @AuthValidate(AuthCode.TaskCancelTop)
    public RespResult cancelTaskTop(@ApiParam(value = "Http请求") HttpServletRequest request, @ApiParam(value = "任务ID") Long taskId) {
        try {
            iTaskService.cancelTaskTopByTaskId(taskId, AdminRequestUtil.identify(request));
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "获取任务数据",notes = "根据店铺ID获取对应的任务数据")
    @RequestMapping("findTaskBySellerShopId")
    RespResult findTaskBySellerShopId(Long sellerShopId, Long goodsId) {
        Task task = iTaskService.findTaskBySellerShopId(sellerShopId, goodsId);
        return RespResult.success(task);
    }


    @ApiOperation(value = "保存任务数据",notes = "保存任务数据")
    @PostMapping(value = "save")
   // @AuthValidate(AuthCode.TaskSave)
    public RespResult saveTask(@ApiParam(value = "任务对象") Task task,@ApiParam(value = "关键字") String taskKeyword) {
        try {
            // 判断搜索词或者任务信息为空
            if (StrUtil.isNull(taskKeyword) || StrUtil.isNull(task)) {
                return RespResult.error("保存失败！");
            }
            // 保存任务和任务搜索词信息
            iTaskService.saveTaskAndKeyword(task, taskKeyword);
        } catch (Exception e) {
            return RespResult.error("保存失败");
        }
        return RespResult.success();
    }


    @ApiOperation(value = "更新任务和任务搜索词信息",notes = "更新任务和任务搜索词信息")
    @PostMapping(value = "update")
   // @AuthValidate(AuthCode.TaskUpdate)
    public RespResult updateTask(Task task, String taskKeyword, String time) {
        try {
            // 判断搜索词或者任务信息为空
            if (StrUtil.isNull(taskKeyword) || StrUtil.isNull(task)) {
                return RespResult.error("更新失败");
            }
            // 更新任务和任务搜索词信息
            iTaskService.upadteTaskAndKeyword(task, taskKeyword, time);
        } catch (Exception e) {
            logger.error("===>[更新|发布]任务异常，异常原因：{}", e.getMessage());
            return RespResult.error("更新失败");
        }
        return RespResult.success();
    }
}


