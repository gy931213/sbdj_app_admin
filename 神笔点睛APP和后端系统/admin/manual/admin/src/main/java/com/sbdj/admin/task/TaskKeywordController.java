package com.sbdj.admin.task;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 任务关键词表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Controller
@RequestMapping("/task/keyword")
public class TaskKeywordController {

}
