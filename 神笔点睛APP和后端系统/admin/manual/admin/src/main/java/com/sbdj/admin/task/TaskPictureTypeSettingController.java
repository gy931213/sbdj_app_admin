package com.sbdj.admin.task;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.task.entity.TaskPictureTypeSetting;
import com.sbdj.service.task.service.ITaskPictureTypeSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Api(tags = "任务图片控制器")
@RestController
@RequestMapping("/task/picture/type/setting")
public class TaskPictureTypeSettingController {
    @Autowired
    private ITaskPictureTypeSettingService iTaskPictureTypeSettingService;

    @ApiOperation(value = "保存任务图片",notes = "保存任务图片类型")
    @RequestMapping("save")
    public RespResult saveTaskPictureTypeSetting(HttpServletRequest request, @ApiParam(value = "任务图片类型对象") TaskPictureTypeSetting taskPictureTypeSetting) {
        try {
            taskPictureTypeSetting.setOrgId(AdminRequestUtil.orgId(request));
            iTaskPictureTypeSettingService.saveTaskPictureTypeSetting(taskPictureTypeSetting);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "逻辑删除任务图片",notes = "逻辑删除任务图片")
    @RequestMapping("delete")
    public RespResult deleteTaskPictureTypeSetting(@ApiParam(value = "任务图片类型ID") Long id) {
        try {
            //iTaskPictureTypeSettingService.deleteTaskPictureTypeSetting(id);
            iTaskPictureTypeSettingService.deleteTaskPictureTypeSetting(id, Status.STATUS_DELETE);
        } catch (Exception e) {
            e.printStackTrace();
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "修改任务图片状态",notes = "禁用，启用")
    @RequestMapping("status")
    public RespResult disableOrEnableTaskPictureTypeSetting(@ApiParam(value = "任务图片类型ID") Long id,@ApiParam(value = "任务图片状态") String status) {
        try {

            iTaskPictureTypeSettingService.disableTaskPictureTypeSetting(id, status);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "修改任务图片类型设置",notes = "修改任务图片类型设置")
    @RequestMapping("update")
    public RespResult updateTaskPictureTypeSetting(HttpServletRequest request, @ApiParam(value = "图片类型设置类") TaskPictureTypeSetting taskPictureTypeSetting) {
        try {
            taskPictureTypeSetting.setOrgId(AdminRequestUtil.orgId(request));
            iTaskPictureTypeSettingService.updateTaskPictureTypeSetting(taskPictureTypeSetting);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }


    @ApiOperation(value = "显示图片类型设置",notes = "显示图片类型设置")
    @RequestMapping("list")
    public RespResult<PageData<TaskPictureTypeSetting>> findTaskPictureTypeSettingByPage(@ApiParam(value = "http请求信息") HttpServletRequest request
            ,@ApiParam(value = "") String status,@ApiParam(value = "")HttpReqParam reqParam) {
        Long orgId = null;
        if (!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        IPage<TaskPictureTypeSetting> page = iTaskPictureTypeSettingService.findTaskPictureTypeSettingPageList(orgId, status, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(page);
    }

    @ApiOperation(value = "查找任务图片类型设置",notes = "查找任务图片类型设置")
    @RequestMapping("select")
    public RespResult findTaskPictureTypeSettingSelect(@ApiParam(value = "http请求信息") HttpServletRequest request) {
        Long orgId = null;
        if (!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        List<TaskPictureTypeSetting> list = iTaskPictureTypeSettingService.findTaskPictureTypeSettingSelect(orgId);
        return RespResult.success(list);
    }
}
