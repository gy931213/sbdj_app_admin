package com.sbdj.admin.task;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.task.entity.TaskSetting;
import com.sbdj.service.task.service.ITaskSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@Api(tags = "任务分配配置控制器")
@RestController
@RequestMapping("/task/setting")
public class TaskSettingController {

    @Autowired
    private ITaskSettingService iTaskSettingService;



    @ApiOperation(value = "显示任务分配配置",notes = "根据条件任务分配配置")
    @PostMapping(value = "list")
    //@AuthValidate(AuthCode.TaskSettingList)
    public RespResult<PageData<TaskSetting>> TaskSettingList(@ApiParam(value = "http请求信息") HttpServletRequest request, @ApiParam(value = "组织ID") Long orgId, @ApiParam(value = "分页数据") HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            orgId = AdminRequestUtil.orgId(request);
        }
        IPage<TaskSetting> page = iTaskSettingService.findTaskSettingPageList(orgId, PageRequestUtil.buildPageRequest(reqParam));
        return  RespResult.buildPageData(page);
    }


    @ApiOperation(value = "保存任务分配配置",notes = "保存任务分配配置")
    @PostMapping(value = "save")
   // @AuthValidate(AuthCode.TaskSettingSave)
    public RespResult saveTaskSetting(@ApiParam(value = "任务分配配置对象") TaskSetting setting) {
        try {
            // ~
            TaskSetting setting2 = iTaskSettingService.findTaskSettingOne(setting.getOrgId(), setting.getSalesmanLevelId());
            if (null != setting2) {
                return RespResult.error("等级配置已存在");
            }
            iTaskSettingService.saveTaskSetting(setting);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }

    @ApiOperation(value = "更新任务分配配置",notes = "更新任务分配配置")
    @PostMapping(value = "update")
   // @AuthValidate(AuthCode.TaskSettingUpdate)
    public RespResult updateTaskSetting(@ApiParam(value = "任务分配配置对象") TaskSetting setting) {
        try {
            // ~
            TaskSetting setting2 = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(setting.getOrgId(), setting.getSalesmanLevelId());
            if (null != setting2 && !setting.getId().equals(setting2.getId()) && setting2.getSalesmanLevelId().equals(setting.getSalesmanLevelId()) && setting.getOrgId().equals(setting2.getOrgId())) {
                return RespResult.error("等级配置已存在");
            }
            iTaskSettingService.updateTaskSetting(setting);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }


    @ApiOperation(value = "逻辑删除任务分配配置数据",notes = "逻辑删除任务分配配置数据")
    @PostMapping(value = "delete")
    //@AuthValidate(AuthCode.TaskSettingDelete)
    public RespResult deleteTaskSetting(@ApiParam(value = "") Long id) {
        try {
            iTaskSettingService.logicDeleteTaskSetting(id);
            return RespResult.success();
        } catch (Exception e) {
            return RespResult.error();
        }
    }
}
