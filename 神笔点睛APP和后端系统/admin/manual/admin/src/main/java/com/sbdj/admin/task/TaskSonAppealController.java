package com.sbdj.admin.task;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonAppealVo;
import com.sbdj.service.task.entity.TaskSonAppeal;
import com.sbdj.service.task.service.ITaskSonAppealService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@Api(tags = "子任务申诉控制器")
@RestController
@RequestMapping("/task/son/appeal")
public class TaskSonAppealController {
    @Autowired
    private ITaskSonAppealService iTaskSonAppealService;




    @ApiOperation(value = "显示申诉列表数据",notes = "根据条件显示申诉列表数据")
    @RequestMapping("list")
    public RespResult<PageData<TaskSonAppealVo>> listTaskSonAppeal(@ApiParam(value = "http请求数据") HttpServletRequest request
            , @ApiParam(value = "子任务申诉查询条件") TaskSonQuery query, @ApiParam(value = "分页数据") HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<TaskSonAppealVo> page = iTaskSonAppealService.findTaskSonAppealByPage(query, PageRequestUtil.buildPageRequest(reqParam));
        return  RespResult.buildPageData(page);

    }



    @ApiOperation(value = "保存申诉数据",notes = "保存申诉数据")
    @RequestMapping("save")
    public RespResult saveTaskSonAppeal(@ApiParam(value = "子任务申诉对象") TaskSonAppeal taskSonAppeal) {
        try {
            iTaskSonAppealService.saveTaskSonAppeal(taskSonAppeal);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }


    @ApiOperation(value = "修改申诉数据",notes = "修改申诉数据")
    @RequestMapping("update")
    public RespResult updateTaskSonAppeal(@ApiParam(value = "http请求数据") HttpServletRequest request,@ApiParam(value = "子任务申诉对象") TaskSonAppeal taskSonAppeal) {
        taskSonAppeal.setAuditorId(AdminRequestUtil.identify(request));
        try {
            iTaskSonAppealService.updateTaskSonAppeal(taskSonAppeal);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "逻辑删除申述数据",notes = "逻辑删除申述数据")
    @RequestMapping("delete")
    public RespResult deleteTaskSonAppeal(@ApiParam(value = "http请求数据") HttpServletRequest request,@ApiParam(value = "子任务申诉表ID") Long id) {
        Long oprId = AdminRequestUtil.identify(request);
        try {
            iTaskSonAppealService.logicDeleteTaskSonAppealById(id, oprId);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }


    @ApiOperation(value = "审核申诉数据",notes = "审核申诉数据")
    @RequestMapping("review")
    public RespResult reviewTaskSonAppeal(@ApiParam(value = "http请求数据") HttpServletRequest request,@ApiParam(value = "") Long id,@ApiParam(value = "子任务ID") Long taskSonId
            ,@ApiParam(value = "开始状态") String beginStatus,@ApiParam(value = "结束状态") String endStatus,@ApiParam(value = "内容") String account) {
        // 审核人
        Long oprId = AdminRequestUtil.identify(request);
        try {
            if (Status.STATUS_YES_AUDITED.equals(endStatus)) {
                iTaskSonAppealService.reviewTaskSonAppeal(id, taskSonId, beginStatus, endStatus, account, oprId);
            } else {
                iTaskSonAppealService.reviewTaskSonAppeal(id, beginStatus, endStatus, account, oprId);
            }
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }
}
