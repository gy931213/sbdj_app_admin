package com.sbdj.admin.task;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.RedisUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.finance.entity.FinanceDifferPrice;
import com.sbdj.service.finance.service.IFinanceDifferPriceService;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonImagesVo;
import com.sbdj.service.task.admin.vo.TaskSonVo;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.entity.TaskSonNotPass;
import com.sbdj.service.task.service.ITaskSonImagesService;
import com.sbdj.service.task.service.ITaskSonNotPassService;
import com.sbdj.service.task.service.ITaskSonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;


@Api(tags = "子任务控制器")
@RestController
@RequestMapping("/task/son")
public class TaskSonController {

    private static Logger logger = LoggerFactory.getLogger(TaskSonController.class);

    @Autowired
    private ITaskSonService iTaskSonService;
    @Autowired
    private ITaskSonImagesService iTaskSonImagesService;
    @Autowired
    private ITaskSonNotPassService iTaskSonNotPassService;
    @Autowired
    private IFinanceDifferPriceService iFinanceDifferPriceService;

    @ApiOperation(value = "获取子任务列表数据",notes = "根据条件获取")
    @RequestMapping("list")
    // @AuthValidate(AuthCode.TaskSonList)
    public  RespResult<PageData<TaskSonVo>> finaTaskPageList(HttpServletRequest request, TaskSonQuery sonQuery, HttpReqParam reqParam){
        if(!AdminRequestUtil.isSuper(request)) {
            sonQuery.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<TaskSonVo> page = iTaskSonService.findTaskSonNumPageList(sonQuery, PageRequestUtil.buildPageRequest(reqParam));
        return  RespResult.buildPageData(page);
    }

    @ApiOperation(value = "批量审核",notes = "根据ID批量审核")
    @PostMapping("/force/auditors")
    public RespResult forceAuditorTaskSonByIds(@ApiParam(value = "http请求信息") HttpServletRequest request,@ApiParam(value = "批量子任务IDs") String ids) {
        try {
            iTaskSonService.forceAuditorTaskSonByTaskSonIds(ids, AdminRequestUtil.identify(request));
            logger.info("请求IP:【{}】 操作人：【{}】 审核任务 子任务编号：【{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), ids);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @ApiOperation(value = "批量做废",notes = "批量做废")
    @PostMapping("invalid")
 //   @AuthValidate(AuthCode.TaskSonAuditors)
    public RespResult invalidTaskSonByIds(@ApiParam(value = "http请求信息")HttpServletRequest request,@ApiParam(value = "批量子任务IDs") String ids) {
        if (StrUtil.isNull(ids)) {
            return RespResult.error("数据不能为空");
        }
        try {
            iTaskSonService.invalidTaskSon(ids);
            logger.info("请求IP：【{}】 操作人：【{}】 作废任务：【{}】", request.getRemoteHost(), AdminRequestUtil.identify(request), ids);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }


    @ApiOperation(value = "截图",notes = "根据子任务id获取任务对应的图片集合数据")
    @PostMapping(value = "find/imgs")
    //@AuthValidate(AuthCode.TaskSonImgs)
    public RespResult finaTaskSonImagesByTaskSonId(@ApiParam(value = "子任务ID") Long taskSonId) {
        List<TaskSonImagesVo> list = iTaskSonImagesService.findTaskSonImagesList(taskSonId);
        return RespResult.success(list);
    }

    @ApiOperation(value = "截图(差价截图)",notes = "显示截图信息")
    @RequestMapping("get")
    public RespResult getTaskSonById(@ApiParam(value = "子任务编号") Long id) {
        // 数据排空
        if (StrUtil.isNull(id)) {
            return RespResult.error("id不能为空");
        }
        // 获取相关数据
        TaskSonVo taskSonExt = iTaskSonService.getTaskSonById(id);
        return RespResult.success(taskSonExt);
    }

    @PostMapping("auditor")
    public RespResult auditorTaskSonById(HttpServletRequest request,Long id,String endStatus,String account, String ids) {
        try {
            if(StrUtil.isNull(RedisUtil.get("UNIQUE_"+id))) {
                RedisUtil.set("UNIQUE_"+id, id, 10);
            }else {
                return RespResult.error("已被审核，请勿重复审核");
            }
            TaskSon taskSonOne = iTaskSonService.findTaskSonOne(id);
            // 第一次审核
            iTaskSonService.auditorTaskSonByTaskSonId(id, AdminRequestUtil.identify(request),endStatus,account , taskSonOne);
			/*// 恢复对应业务员的信用额度
			if(endStatus.equalsIgnoreCase(Status.STATUS_YES_AUDITED) || endStatus.equalsIgnoreCase(Status.STATUS_AUDITED_FAIL)) {
				// 查询是否垫付，查询的结果为空则不是垫付
				TaskSon payAdvance = iTaskSonService.findPayAdvance(id);
				if(payAdvance == null){
					TaskSon taskSon = iTaskSonService.findTaskSonOne(id);
					iSalesmanService.updateSalesmanCreditById(taskSon.getSalesmanId(), taskSon.getRealPrice(), false);
				}
			}*/

            // 保存不通过任务数据
            saveTaskSonNotPass(endStatus, taskSonOne, ids, account);

            // 记录操作人修改
            logger.info("子任务已审核! 子任务编号:{} 原因:{} 操作人:{}", id, account, AdminRequestUtil.identify(request));
        } catch (Exception e) {
            return RespResult.error("审核失败");
        }
        return RespResult.success();
    }

    @PostMapping("retrial")
    public RespResult retrialTaskSonById(HttpServletRequest request,Long id,String endStatus,String account, String ids) {
        try {
            if(StrUtil.isNull(RedisUtil.get("UNIQUE_"+id))) {
                RedisUtil.set("UNIQUE_"+id, id, 10);
            }else {
                return RespResult.error("已被重审，请勿重复重审");
            }
            // 查询是否已审核
            TaskSon taskSonOne = iTaskSonService.findTaskSonOne(id);
            String statusOne = taskSonOne.getStatus();
            // 重审的状态只有，审核失败，审核通过，状态的更换，失败 > 通过 ，失败 >  不通过， 通过 > 失败，通过 > 不通过
            if (statusOne.equalsIgnoreCase(Status.STATUS_AUDITED_FAIL) || statusOne.equalsIgnoreCase(Status.STATUS_YES_AUDITED)) {
                // \(^o^)/~ 子任务重审，无须恢复信用额度
                iTaskSonService.auditorTaskSonAgainByTaskSonId(id, AdminRequestUtil.identify(request), statusOne, endStatus,account, taskSonOne);
            }
            // 保存设置
            saveTaskSonNotPass(endStatus, taskSonOne, ids, account);
            // 记录操作人修改
            logger.info("子任务已重审! 子任务编号:{} 原因:{} 操作人:{}", id, account, AdminRequestUtil.identify(request));
        } catch (Exception e) {
            return RespResult.error("重审失败");
        }
        return RespResult.success();
    }

    /*@PostMapping("auditors")
    public RespResult auditorTaskSonByIds(HttpServletRequest request,String ids) {
        try {
            iTaskSonService.auditorTaskSonByTaskSonIds(ids, AdminRequestUtil.identify(request));
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }*/

    @PostMapping(value="timeout")
    public RespResult updateTaskSonIsTimeOut(HttpServletRequest request,Long id,String isTimeOut) {
        try {
            iTaskSonService.updateTaskSonIsTimeOut(id, AdminRequestUtil.identify(request), isTimeOut);
            // 记录操作人修改
            logger.info("子任务更改超时状态! 子任务编号:{} 操作人:{}", id, AdminRequestUtil.identify(request));
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @PostMapping("update/payment")
    public  RespResult updateTaskSonPayment(HttpServletRequest request, Long id, BigDecimal payment) {
        if (StrUtil.isNull(id)) {
            return RespResult.error("子任务编号不能为空");
        } else if (StrUtil.isNull(payment)) {
            return RespResult.error("实付价格不能为空");
        } else if (payment.compareTo(BigDecimal.ZERO) <= 0) {
            return RespResult.error("实付价格不能低于或等于0");
        }
        // 获取差价
        FinanceDifferPrice differPrice = iFinanceDifferPriceService.findDifferPriceByTaskSonId(id);
        if (StrUtil.isNotNull(differPrice) && Status.STATUS_YES.equals(differPrice.getStatus())) {
            return RespResult.error("差价已处理,无法修改");
        }
        try {
            iTaskSonService.updateTaskSonPaymentById(id,payment);
            // 记录当时操作人修改
            logger.info("子任务实付价已改! 子任务编号:{} 实付价:{} 操作人编号:{}", id, payment, AdminRequestUtil.identify(request));
            // 获取子任务
            TaskSon taskSon = iTaskSonService.findTaskSonOne(id);
            // 差价处理 (更新) 非审核状态不会处理差价
            iTaskSonService.saveDifferPrice(taskSon.getRealPrice(), taskSon.getPayment(), taskSon, taskSon.getStatus());
        } catch (Exception e) {
            return RespResult.error("更新实付款失败");
        }
        return RespResult.success();
    }

    @PostMapping("update/ordernum")
    public RespResult updateOrderNum(HttpServletRequest request,Long taskSonId,Long platformId,String orderNum) {
        if(StrUtil.isNull(orderNum)) {
            return RespResult.error("订单号不能为空！");
        }
        TaskSon ts = iTaskSonService.findTaskSonByOrderNum(orderNum);
        if(null !=ts && ts.getPlatformId().equals(platformId) && !ts.getId().equals(taskSonId)) {
            return RespResult.error("订单号已存在！");
        }
        try {
            iTaskSonService.updateOrderNumByTaskSonId(taskSonId, platformId, orderNum);
            // 记录操作人修改
            logger.info("子任务订单号已改! 子任务编号:{} 平台编号:{} 订单号:{} 操作人:{}", taskSonId, platformId, orderNum, AdminRequestUtil.identify(request));
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    // 保存子任务不通过相关设置
    private void saveTaskSonNotPass(String endStatus, TaskSon taskSonOne, String ids, String account) {
        // 不通过图片设置
        if (endStatus.equals(Status.STATUS_NOT_PASS)) {
            // 逻辑删除历史数据
            iTaskSonNotPassService.logicDeleteTaskSonNotPass(taskSonOne.getId());
            // 保存最新的数据
            TaskSonNotPass taskSonNotPass = new TaskSonNotPass();
            taskSonNotPass.setOrgId(taskSonOne.getOrgId());
            taskSonNotPass.setTaskSonId(taskSonOne.getId());
            taskSonNotPass.setImages(ids);
            taskSonNotPass.setAccount(account);
            iTaskSonNotPassService.saveTaskSonNotPass(taskSonNotPass);
        }
    }

}
