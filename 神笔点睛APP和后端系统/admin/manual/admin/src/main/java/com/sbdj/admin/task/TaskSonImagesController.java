package com.sbdj.admin.task;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 子任务所属图片集合表 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@RestController
@RequestMapping("/task/son/images")
public class TaskSonImagesController {

}
