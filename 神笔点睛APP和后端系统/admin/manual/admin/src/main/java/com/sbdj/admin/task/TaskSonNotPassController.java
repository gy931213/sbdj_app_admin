package com.sbdj.admin.task;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 子任务未通过

 前端控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@RestController
@RequestMapping("/task/son/not/pass")
public class TaskSonNotPassController {

}
