package com.sbdj.admin.task;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonNullifyVo;
import com.sbdj.service.task.service.ITaskSonNullifyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@Api(tags = "子任务作废控制器")
@RestController
@RequestMapping("/task/son/nullify")
public class TaskSonNullifyController {

    @Autowired
    private ITaskSonNullifyService iTaskSonNullifyService;



    @ApiOperation(value = "显示作废任务列表",notes = "按条件显示作废任务列表")
    @RequestMapping("list")
    public RespResult<PageData<TaskSonNullifyVo>> listTaskSonAppeal(@ApiParam(value = "Http请求信息") HttpServletRequest request
            ,@ApiParam(value = "子任务查询类")  TaskSonQuery query,@ApiParam(value = "分页数据")  HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<TaskSonNullifyVo> page = iTaskSonNullifyService.findTaskSonNullifyByPage(query, PageRequestUtil.buildPageRequest(reqParam));
        return  RespResult.buildPageData(page);
    }


    @ApiOperation(value = "审核",notes = "审核")
    @RequestMapping("review")
    public RespResult reviewTaskSonAppeal(@ApiParam(value = "http请求信息") HttpServletRequest request,@ApiParam(value = "作废子任务ID")  Long id
            ,@ApiParam(value = "开始状态")  String beginStatus,@ApiParam(value = "结束状态")  String endStatus,@ApiParam(value = "审批原因") String account) {
        // 审核人
        Long oprId =AdminRequestUtil.identify(request);
        try {
            if (endStatus.equalsIgnoreCase("H")) {
                iTaskSonNullifyService.reviewTaskSonNullify(id, beginStatus, Status.STATUS_YES_AUDITED, account, oprId);
            } else {
                iTaskSonNullifyService.reviewTaskSonNullifyNotRecovering(id, beginStatus, endStatus, account, oprId);
            }
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }
}
