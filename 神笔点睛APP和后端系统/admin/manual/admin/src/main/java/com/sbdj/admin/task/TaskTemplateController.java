package com.sbdj.admin.task;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.task.admin.query.TaskQuery;
import com.sbdj.service.task.admin.vo.TaskVo;
import com.sbdj.service.task.entity.Task;
import com.sbdj.service.task.entity.TaskKeyword;
import com.sbdj.service.task.service.ITaskKeywordService;
import com.sbdj.service.task.service.ITaskService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;



@Api(tags = "任务模板控制器")
@RequestMapping("/task/template")
@RestController
public class TaskTemplateController {
    private final static Logger logger = LoggerFactory.getLogger(TaskTemplateController.class);

    @Autowired
    private ITaskService iTaskService;
    @Autowired
    private ITaskKeywordService iTaskKeywordService;




    @ApiOperation(value = "显示任务模板",notes = "根据查询条件显示模板数据")
    @RequestMapping("list")
    public RespResult<PageData<TaskVo>> finaTaskPageList(@ApiParam(value = "Http请求信息") HttpServletRequest request
            ,@ApiParam(value = "任务模板查询条件") TaskQuery query,@ApiParam(value = "分页数据") HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<TaskVo> page = iTaskService.findTaskTemplatePageList(query, PageRequestUtil.buildPageRequest(reqParam));
        return  RespResult.buildPageData(page);
    }

    @ApiOperation(value = "保存模板数据",notes = "保存模板数据")
    @PostMapping(value = "save")
    public RespResult saveTask(@ApiParam(value = "任务对象") Task task,@ApiParam(value = "关键字") String taskKeyword) {
        try {
            // 判断搜索词或者任务信息为空
            if (StrUtil.isNull(taskKeyword) || StrUtil.isNull(task)) {
                return RespResult.error("保存失败！");
            }
            // 保存任务和任务搜索词信息
            iTaskService.saveTaskTemplate(task, taskKeyword);
        } catch (Exception e) {
            return RespResult.error("保存失败");
        }
        return RespResult.success();
    }

    @ApiOperation(value = "更新模板",notes = "更新模板")
    @PostMapping(value = "update")
    public RespResult updateTask(@ApiParam(value = "任务对象") Task task,@ApiParam(value = "关键字") String taskKeyword,@ApiParam(value = "更新时间") String time) {
        try {
            // 判断搜索词或者任务信息为空
            if (StrUtil.isNull(taskKeyword) || StrUtil.isNull(task)) {
                return RespResult.error("更新失败");
            }
            // 更新任务和任务搜索词信息
            iTaskService.updateTaskTemplate(task, taskKeyword, time);
        } catch (Exception e) {
            logger.error("===>[更新]任务模板异常，异常原因：{}", e.getMessage());
            return RespResult.error("更新失败");
        }
        return RespResult.success();
    }


    @ApiOperation(value = "根据任务编号以及状态值来获取词条",notes = "根据任务编号以及状态值来获取词条")
    @RequestMapping("/find/keyword")
    public RespResult findTaskKeywordDataByTaskIdAndStatus(@ApiParam(value = "") Long taskId) {
        List<TaskKeyword> list = iTaskKeywordService.findAllByTaskIdAndStatus(taskId, Status.STATUS_TEMPLATE);
        return RespResult.success(list);
    }


}
