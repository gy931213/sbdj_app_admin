package com.sbdj.admin.task;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.admin.web.AdminRequestUtil;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.admin.web.RespResult;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.task.admin.query.TaskQuery;
import com.sbdj.service.task.admin.vo.TaskVo;
import com.sbdj.service.task.entity.Task;
import com.sbdj.service.task.entity.TaskKeyword;
import com.sbdj.service.task.service.ITaskKeywordService;
import com.sbdj.service.task.service.ITaskService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Api(tags = "未完成任务控制器")
@RequestMapping("/task/undone")
@RestController
public class TaskUndoneController {

    private final static Logger logger = LoggerFactory.getLogger(TaskUndoneController.class);

    @Autowired
    private ITaskService iTaskService;
    @Autowired
    private ITaskKeywordService iTaskKeywordService;




    @ApiOperation(value = "显示未完成任务信息",notes = "按查询条件显示未完成任务信息")
    @RequestMapping("list")
    public RespResult<PageData<TaskVo>> finaTaskPageList(@ApiParam(value = "Http请求信息") HttpServletRequest request,@ApiParam(value = "任务查询条件") TaskQuery query,@ApiParam(value = "分页信息") HttpReqParam reqParam) {
        if (!AdminRequestUtil.isSuper(request)) {
            query.setOrgId(AdminRequestUtil.orgId(request));
        }
        IPage<TaskVo> page = iTaskService.findTaskUndonePageList(query, PageRequestUtil.buildPageRequest(reqParam));
        return  RespResult.buildPageData(page);
    }

    @ApiOperation(value = "显示任务关键字数据",notes = "通过ID查找任务关键字数据")
    @RequestMapping("findkeyword")
    public RespResult findTaskKeywordDataByTaskIdAndStatus(@ApiParam(value = "任务ID") Long taskId) {
        List<TaskKeyword> list = iTaskKeywordService.findAllByTaskIdAndStatus(taskId, null);
        return RespResult.success(list);
    }


    @ApiOperation(value = "更新任务和任务搜索词信息",notes = "更新任务和任务搜索词信息")
    @PostMapping(value = "update")
    public RespResult updateTask(@ApiParam(value = "更新的任务对象") Task task,@ApiParam(value = "关键字") String taskKeyword,@ApiParam(value = "定时更新时间") String time) {
        try {
            // 判断搜索词或者任务信息为空
            if (StrUtil.isNull(taskKeyword) || StrUtil.isNull(task)) {
                return RespResult.error("更新失败");
            }
            // 更新任务和任务搜索词信息
            iTaskService.updateTaskUndone(task, taskKeyword, time);
        } catch (Exception e) {
            logger.error("===>[更新]任务异常，异常原因：{}", e.getMessage());
            return RespResult.error("更新失败");
        }
        return RespResult.success();
    }

}
