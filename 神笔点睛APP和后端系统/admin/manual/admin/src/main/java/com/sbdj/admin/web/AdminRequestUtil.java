package com.sbdj.admin.web;

import com.sbdj.core.base.ISession;
import com.sbdj.core.constant.Number;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysFunction;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 请求工具类
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 22:22
 */
public class AdminRequestUtil {
	private AdminRequestUtil() {
	}

	public static final String WEB_I_SESSION_KEY = "WEB_SESSION_KEY";
	
	public static final String FUNCTION_KEY = "function";
	
	/**
	 * 系统超级管理员 【是】
	 */
	public  static final String IS_SUPER_YES = "1";
	
	/**
	 * 系统超级管理员 【否】
	 */
	public  static final String IS_SUPER_NO = "0";

	
	/**
	 * 设置web回话中的Session
	 * 
	 * @param request
	 * @return
	 */
	public static void setSession(HttpServletRequest request, ISession session) {
		request.getSession().setAttribute(WEB_I_SESSION_KEY, session);
	}

	/**
	 * 获取web回话中的Session
	 * 
	 * @param request
	 * @return
	 */
	public static ISession getSession(HttpServletRequest request) {
		return (ISession) request.getSession().getAttribute(WEB_I_SESSION_KEY);
	}

	/**
	 * sessionInvalidate:注销web回话中的Session <br/>
	 *
	 * @author 黄天良
	 * @param request
	 * @since JDK 1.7
	 */
	public static void sessionInvalidate(HttpServletRequest request) {
		request.getSession().invalidate();
	}

	/**
	 * isAuth:判断用户是否登录
	 * 
	 * @author 黄天良
	 * @param request
	 * @return
	 */
	public static boolean isAuth(HttpServletRequest request) {
		ISession session = AdminRequestUtil.getSession(request);
		if (null!=session && session.auth()) {
			return session.auth();
		} else {
			return Boolean.FALSE;
		}
	}

	/**
	 * setObject:设置对象数据 <br/>
	 *
	 * @author 黄天良
	 * @param request
	 * @param object  保存的对象
	 * @param key     保存的key
	 * @since JDK 1.7
	 */
	public static void setObject(HttpServletRequest request, Object object, String key) {
		request.getSession().setAttribute(key, object);
	}

	/**
	 * getObject:获取对象数据 <br/>
	 *
	 * @author 黄天良
	 * @param request
	 * @param key     获取对象数据key
	 * @return
	 * @since JDK 1.7
	 */
	public static <T> Object getObject(HttpServletRequest request, String key) {
		return request.getSession().getAttribute(key);
	}

	/**
	 * 获取功能集合
	 * @author Yly
	 * @date 2019/11/21 13:23
	 * @param request 请求会话
	 * @return java.util.List<com.sbdj.service.system.entity.SysFunction> 功能集合
	 */
	public static <T> List<SysFunction> getFunction(HttpServletRequest request) {
		Object obj = getObject(request, FUNCTION_KEY);
		List<SysFunction> list = new ArrayList<>();
		if (obj instanceof List<?>) {
			for (Object o : (List<?>) obj) {
				list.add((SysFunction) o);
			}
		}
		return list;
	}

	/**
	 * identify: 获取用户标识identify<br/>
	 *
	 * @date 2018年8月15日
	 * @param request 请求对象
	 * @return
	 * @since JDK 1.8
	 */
	public static Long identify(HttpServletRequest request) {
		WebSession session = (WebSession) AdminRequestUtil.getSession(request);
		return session == null ? Number.LONG_ZERO : session.identify();
	}
	
	/**
	 * 获取当前登录人所属机构
	 * @param request
	 * @return
	 */
	public static Long orgId(HttpServletRequest request) {
		WebSession session = (WebSession) AdminRequestUtil.getSession(request);
		return session == null ? Number.LONG_ZERO : session.getOrgId();
	}

	/**
	 * 判断是否是系统超级管理员
	 * @param request
	 * @return
	 */
	public static boolean isSuper(HttpServletRequest request) {
		WebSession session = (WebSession) AdminRequestUtil.getSession(request);
		if(null == session) {
				return false;
		}
		if(null != session.getIsSuper() && !session.getIsSuper().equalsIgnoreCase(IS_SUPER_YES)) {
			return false;
		}
		return true;
	}

	/**
	 * 判断是否是商家 ，[是：true | 否：false ]
	 * @param request
	 * @return
	 */
	public static  boolean isSeller(HttpServletRequest request){
		WebSession session = (WebSession) AdminRequestUtil.getSession(request);
		if(null == session) {
			return false;
		}
		if (session.isSeller()){
			return true;
		}
		return false;
	}
	
	/**
	 * @Title: getShopIds  
	 * @Description: 获取当前商家所属的店铺数据
	 * @param request
	 * @return  
	 * @return String 返回类型
	 */
	public static String getShopIds(HttpServletRequest request) {
		WebSession session = (WebSession) AdminRequestUtil.getSession(request);
		if(null == session) {
			return null;
		}
		if (StrUtil.isNull(session.getShopIds())){
			return null;
		}
		return session.getShopIds();
	}
}
