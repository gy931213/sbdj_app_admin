package com.sbdj.admin.web;

import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 全局异常拦截
 * </p>
 *
 * @author Yly
 * @since 2019/11/21 16:46
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(UnauthorizedException.class)
    public RespResult exceptionHandle(UnauthorizedException ue) {
        return RespResult.error("你没有权限访问", 403);
    }
}
