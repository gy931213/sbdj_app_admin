package com.sbdj.admin.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.gson.Gson;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.ResultCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 * 返回请求后的结果
 * </p>
 *
 * @author Yly
 * @since 2019-11-25
 */
@ApiModel(value = "返回请求后的结果")
public class RespResult<T> {

	@ApiModelProperty(value = "返回的消息")
	private String message;
	@ApiModelProperty(value = "返回的代码")
	private Integer retCode;

	@ApiModelProperty(value = "正常返回数据")
	private T data;

	public RespResult() { }

	public RespResult(String message, int retCode) {
		this.message = message;
		this.retCode = retCode;
	}

	/**
	 * buildPageData:pageData数据的结果. <br/>
	 *
	 * @param page 当前分页数据
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> RespResult<PageData<T>> buildPageData(IPage<T> page) {
		PageData<T> pd = new PageData<>();
		pd.setPage(page.getCurrent());
		pd.setLimit(page.getSize());
		pd.setTotal(page.getTotal());
		pd.setList(page.getRecords());
		return new RespResult<>(pd);
	}

	private RespResult(T data) {
		this.retCode = ResultCode.RESULT_SUCCESS_CODE;
		this.message = "success";
		this.data = data;
	}

	private RespResult(String message) {
		this.retCode = ResultCode.RESULT_SUCCESS_CODE;
		this.message = message;
	}

	/**
	 * success:成功时候的调用 . <br/>
	 *
	 * @param data 返回成功请求的数据结果
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> RespResult<T> success(T data) {
		return new RespResult<T>(data);
	}

	/**
	 * success:成功时候的调用 . <br/>
	 * 
	 * @param message 返回成功消息
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> RespResult<T> success(String message) {
		return new RespResult<T>(message);
	}

	/**
	 * success:成功，参数可传可不传 . <br/>
	 *
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> RespResult<T> success() {
		return success("success");
	}

	/**
	 * error: 失败时候的调用 . <br/>
	 *
	 * @param message 提示消息
	 * @param retCode 代码
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> RespResult<T> error(String message, int retCode) {
		return new RespResult<T>(message, retCode);
	}

	/**
	 * error: 失败时候的调用 . <br/>
	 *
	 * @author 黄天良
	 * @param message 返回请求的错误信息
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> RespResult<T> error(String message) {
		return new RespResult<T>(message, ResultCode.RESULT_ERROR_CODE);
	}

	/**
	 * error: 失败时候的调用
	 * 
	 * @return
	 */
	public static <T> RespResult<T> error() {
		return error("error");
	}

	/**
	 * respOutput: <br/>
	 *
	 * @param request  请求对象
	 * @param response 响应对象
	 * @param codeMsg  消息对象
	 * @since JDK 1.8
	 */
	public static void respOutput(HttpServletRequest request, HttpServletResponse response, String codeMsg) {
		// 判断是不是ajax请求或者是不是APP请求
		if (isAjax(request) || isApp(request)) {
			try {
				Gson gson = new Gson();
				response.setContentType("application/json;charset=UTF-8");
				response.getWriter().write(gson.toJson(codeMsg));
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					response.getWriter().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * respOutput: <br/>
	 *
	 * @param request    请求对象
	 * @param response   响应对象
	 * @param respResult 消息对象
	 * @since JDK 1.8
	 */
	public static void respOutput(HttpServletRequest request, HttpServletResponse response, RespResult respResult) {
		// 判断是不是ajax请求或者是不是APP请求
		if (isAjax(request) || isApp(request)) {
			try {
				Gson gson = new Gson();
				response.setContentType("application/json;charset=UTF-8");
				response.resetBuffer();
				response.getWriter().println(gson.toJson(respResult));
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					response.getWriter().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * isAjax: 判断是否ajax请求<br/>
	 *
	 * @param request
	 * @return
	 * @since JDK 1.8
	 */
	private static boolean isAjax(HttpServletRequest request) {
		return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
	}

	/**
	 * isApp: 判断是否是APP<br/>
	 *
	 * @param request
	 * @return
	 * @since JDK 1.8
	 */
	private static boolean isApp(HttpServletRequest request) {
		return "app".equals(request.getHeader("client"));
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getRetCode() {
		return retCode;
	}

	public void setRetCode(Integer retCode) {
		this.retCode = retCode;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
