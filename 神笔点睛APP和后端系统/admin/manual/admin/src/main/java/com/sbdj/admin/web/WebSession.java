package com.sbdj.admin.web;

import com.sbdj.core.base.ISession;
import com.sbdj.core.constant.Number;

import java.util.List;
import java.util.Set;

public class WebSession implements ISession {

	private static final long serialVersionUID = 1L;
	/** 是否登陆 **/
	private boolean auth = Boolean.FALSE;
	/** 用户标识 **/
	private Long identify = Number.LONG_ZERO;
	/** 当前登录人名称 **/
	private String name;
	/**所属机构**/
	private Long orgId = -1L;
	/** 所属角色**/
	private List<String> roles;
	/**数据操作权限**/
	private Set<String> permissions;
	/**是否为超级管理员，否：0/是：1**/
	private String isSuper = "0";
	/**是否是商家[是：true | 否：false]**/
	private boolean isSeller = Boolean.FALSE;
	/**店铺数据ids，只有商家账号存在店铺数据**/
	private String shopIds;
	/**头像url**/
	private String headUrl;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean auth() {
		return this.auth;
	}

	@Override
	public Long identify() {
		return this.identify;
	}

    @Override
    public Long orgId() {
        return this.orgId;
    }

    @Override
    public Set<String> permissions() {
        return this.permissions;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public Long getIdentify() {
        return identify;
    }

    public void setIdentify(Long identify) {
        this.identify = identify;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<String> permissions) {
        this.permissions = permissions;
    }

    public String getIsSuper() {
        return isSuper;
    }

    public void setIsSuper(String isSuper) {
        this.isSuper = isSuper;
    }

    public boolean isSeller() {
        return isSeller;
    }

    public void setSeller(boolean seller) {
        isSeller = seller;
    }

    public String getShopIds() {
        return shopIds;
    }

    public void setShopIds(String shopIds) {
        this.shopIds = shopIds;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }
}
