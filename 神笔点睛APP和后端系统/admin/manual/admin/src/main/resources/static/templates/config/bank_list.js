
layui.use(['table','form','upload'], function(){
    var table = layui.table;
    var form  = layui.form;
    var $=layui.jquery;
    var upload = layui.upload;

    //操作类型[add|update],默认为add
    var oprType = 'add';
    var layer_ = '';
    var bankId = 0;

    table =  $.extend(table, {config: {checkName: 'checked'}});
    table.render({
        elem: '#table-data',
        url: '/config/bank/list',
        toolbar: '#toolbar',
        title: '银行卡数据表',
        id: 'table_list',
        cols: [[
            {type:'radio',align: 'center'},
            {type:'numbers',title:'序号',align: 'center',width:'5%'},
            {field:'orgName', title:'所属机构'},
            {field:'name', title:'开户行'},
            {field:'detail', title:'支行名称'},
            {field:'number', title:'卡号'},
            {field:'userName', title:'开户人姓名'},
            {field:'balance', title:'余额'},
            {field:'adminName', title:'负责人',hide:true},
            {field:'createTime', title:'创建时间', align: 'center'},
            {fixed: 'right', title:'操作', toolbar: '#operation',  width:180}
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        parseData: function(res){
        	return shenbi.returnParse(res);
        },
        page: true,
        done: function(res, curr, count){
            // 导入流水
            var $showLoad = '';
            var uploadInst = upload.render({
                elem: '#importStream', 		  //绑定元素
                url: '/bank/stream/import',   //导入流水接口
                type: "file",
                accept: 'file',
                before:function(resp){
                    //关键代码
                    this.data={bankId:$("input[name='bankid']").val()};
                    // 文件提交上传前的回调
                    $showLoad = shenbi.showLoad("导入中...");
                },
                done: function(res){
                    shenbi.closeLoad($showLoad);
                    //上传完毕回调
                    if(res.retCode === 0){
                        layer.msg("流水导入成功",{icon:1,time:3000},function(){
                            //window.location.reload();
                            // ~刷新数据
                            //table_reload();
                        });
                    }else{
                        layer.msg(res.message,{icon:5,time:3000},function(){
                            //window.location.reload();
                            // ~刷新数据
                            //table_reload();
                        });
                    }
                },error: function(res){
                    //请求异常回调
                    layer.msg("流水文件导入异常",{icon:5,time:3000},function(){
                        //window.location.reload()
                    });
                }
            });
        }
    });

    //监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        //layer.msg(JSON.stringify(data.field));
        table.reload('table_list',{
        	page: {
               curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });
        return false;
    });

    // 刷新列表
    function table_reload(){
        table.reload('table_list');
    }

    //渲染select选项
    kits.select({
        elem:'#sbwl_org_select_1',
        url:'/sys/organization/select',
        value:'id',
        name:'name',
        form:form
    });

    //头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'sbwl_dels':
                var data = checkStatus.data;
                var ids = [];
                $.each(data,function(index,item){
                    ids.push(item.id);
                });

                if(ids.length<=0){
                    layer.msg("至少选择一项进行操作");
                    return false
                }

                layer.msg('确定要批量删除吗？', {
                    btn: ['确定', '取消'],
                    yes: function(index, layero){
                        var data = obj.data;
                        shenbi.ajax({
                            type:'post',
                            url:'/config/bank/deletes',
                            sendMsg:'删除中...',
                            data:{ids:ids.join()},
                            success:function(resp){
                                if(resp.retCode === 0){
                                    layer.msg("已删除");
                                }else{
                                    layer.msg("删除失败");
                                }
                                table_reload();
                            }
                        });
                    }
                });

                break;
            case 'sbwl_create':
                $("input[name=id]").val("");

                //渲染select选项（机构）
                kits.select({
                    elem:'.sbwl_org_select',
                    url:'/sys/organization/select',
                    value:'id',
                    name:'name',
                    form:form
                });

                //渲染select选项
                kits.select({
                    elem:'.sbwl_obj_select',
                    url:'/sys/admin/select',
                    value:"id",
                    name:'anotherName',
                    form:form
                });

                // 新增页面
                showEditOrAdd('新增','#sbwl_tmpl','add');
                break;
            case 'download':
                // 下载导入模板
                window.location.href = "/bank/stream/download/excel";
                break;
        };
    });

    //监听行工具事件
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        console.log(obj)
        if(obj.event === 'sbwl-set'){

        } else if(obj.event === 'sbwl-edit'){
            // 设置表单数据!
            form.val("sbwl_from", {
                "id":data.id,
                "name":data.name,
                "detail":data.detail,
                "userName":data.userName,
                "number":data.number
            });

            // 渲染机构的 select选项
            kits.select({
                elem:'.sbwl_org_select',
                url:'/sys/organization/select',
                value:'id',
                name:'name',
                select:data.orgId,
                form:form
            });

            var adminId = data.adminId === undefined ? 0 : data.adminId;

            //渲染select选项
            kits.select({
                elem:'.sbwl_obj_select',
                url:'/sys/admin/select',
                value:"id",
                name:'anotherName',
                select: adminId,
                form:form
            });

            // 打开编辑页面!
            showEditOrAdd('编辑','#sbwl_tmpl','update');
        }else if(obj.event === 'sbwl-del'){
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    var data = obj.data;
                    shenbi.ajax({
                        type:'post',
                        url:'/config/bank/delete',
                        sendMsg:'删除中...',
                        data:{id:data.id},
                        success:function(resp){
                            if(resp.retCode==0){
                                layer.msg("已删除");
                            }else{
                                layer.msg("删除失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else{
            layer.msg("没有相关操作");
        }
    });

    //打开编辑页面或者是新增数据页面
    function showEditOrAdd(title,doc,opr){
        layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['650px', '410px'],
            content: $(doc),
            cancel: function(){
                $('#sbwl_from_id')[0].reset();
            }
        });
        // 设置操作类型!
        oprType = opr;
    }

    //from表单验证规则
    form.verify({
        orgId: function(value){
            if(value==0){
                return "请选择对应组织";
            }
        }
    });

    //from表单监听提交
    form.on('submit(sbwl_submit)', function(data){
        // 提交后台数据!
        shenbi.ajax({
            post:'post',
            url:oprType=='add'?'/config/bank/save':'/config/bank/update',
            data:data.field,
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode==0){
                    layer.close(layer_);
                    if(oprType=="add"){
                        layer.msg("成功");
                    }else{
                        layer.msg("已更新");
                    }
                    // 刷新数据表
                    table_reload();
                    // 重置表单
                    $('#sbwl_from_id')[0].reset();
                }
            }
        });
        return false
    });

    table.on('radio(table-filter)', function(obj){
        $("input[name='bankid']").val("");
        /*  console.log(obj.checked); //当前是否选中状态
          console.log(obj.data); //选中行的相关数据
          console.log(obj.type); //如果触发的是全选，则为：all，如果触发的是单选，则为：one*/
        if(obj.checked){
            $("input[name='bankid']").val(obj.data.id);
        }
    });

});