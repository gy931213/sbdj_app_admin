
// 
layui.use(['table','form'], function(){
  var table = layui.table;
  var form  = layui.form;
  //操作类型[add|update],默认为add
  var oprType = 'add';
  
   // 表格
  table.render({
	id:'task-table-list',
    elem: '#table-data',
	url:'/mark/goods/set/list',
	toolbar: '#toolbar',
	title: '打标商品配置',
	method:'POST',
	cols: [[
	  {type:'checkbox',sort:true,align: 'center'},
	  {type:'numbers',title:'序号',align: 'center',width:'5%'},
      {field:'orgName', title:'所属机构', align: 'center',hide:true},
      {field:'url', title:'请求地址', align: 'center'},
      {field:'uid', title:'数字账号', align: 'center'},
	  {field:'token', title:'请求密码'},
	  {field:'money', title:'扣费金额'},
      {field:'status', title:'状态', width:100,align: 'center', event:'status', templet:function(rse){
        if(rse.status === 'E'){
            return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #60c560;">启用</a>';
        }else if(rse.status === 'P'){
            return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #FFB800;">禁用</a>';
        }
      }},
	  {field:'createTime', title:'创建时间',align: 'center',hide:true},
	  {fixed: 'right', title:'操作', width:180,align: 'center',templet:function(rse){
		  var _html = '';
			  _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';
			  _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-del" style="background-color: #d9534f;"><i class="layui-icon">&#xe640;</i> 删除</a>';
		  return _html;
	  }}
    ]],
    limit:30,
    limits:[30,50,100,200,500,1000],
    parseData: function(res){ 
        return shenbi.returnParse(res);
    },
	page: true
  });
  
	  //监听搜索按钮
	  form.on('submit(sbwl_search)', function(data){
	    table.reload('task-table-list',{
	    	page: {
	            curr: 1 //重新从第 1 页开始
	         },
	         where:data.field
	    });
	    return false;
	  });
	  
	  // ~刷新列表
	  function table_reload(){
		  table.reload('task-table-list');
	  }
	  
	  // ~头工具栏事件
	  table.on('toolbar(table-filter)', function(obj){
	    var checkStatus = table.checkStatus(obj.config.id);
	    switch(obj.event){
	      case 'sbwl-create':
	    	  	// 重置表单
				$('#sbwl_task_picture_type_setting_id')[0].reset();
				$("input[name=id]").val("");

				// ~渲染新增窗口
              showCreateTaskPictureTypeSetting('<i class="layui-icon">&#xe61f;</i> 新增','#sbwl_task_picture_type_setting_tmpl','add');
	      break;
	    };
	  });
  
	// ~监听数据列表中的操作相关事件~
	table.on('tool(table-filter)', function(obj){
		var data = obj.data;
		if(obj.event === 'sbwl-del'){
			// ~删除
			layer.msg('确定要删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/mark/goods/set/delete',
	    				data:{id:data.id},
	    				sendMsg:'删除中...',
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除");
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					// ~刷新数据
	    					table_reload();
	    				}
	    			 });
	    	     }
			 });
		}else if(obj.event === 'sbwl-edit'){
			// ~编辑
			
			 // ~表单赋值
			 form.val("sbwl_task_picture_type_setting_from", {
				  "id":data.id,
				  "url":data.url,
				  "uid":data.uid,
				  "token":data.token,
				  "money":data.money,
				  "status":data.status
			  });
			
			// ~渲染编辑页面
            showCreateTaskPictureTypeSetting('<i class="layui-icon">&#xe642;</i> 编辑','#sbwl_task_picture_type_setting_tmpl','update');
		}else if(obj.event === 'status'){
		    var title = '';
		    var status = '';
		    var id = data.id;
		    if (data.status === 'E') {
               title = '确定要禁用吗？';
               status = 'P';
            } else if (data.status === 'P') {
                title = '确定要启用吗？';
                status = 'E';
            }
            layer.msg(title, {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/mark/goods/set/status',
                        data:{id:id,status:status},
                        sendMsg:'更新中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已更新");
                            }else{
                                layer.msg("更新失败");
                            }
                            // ~刷新数据
                            table_reload();
                        }
                    });
                }
            });
        }else{
			 layer.msg("没有具体操作事件");
		}
	});
	
  	//~添加任务配置的弹出层~
	var task_layer;
	function showCreateTaskPictureTypeSetting(title,doc,opr){
		task_layer = layer.open({
			type: 1,
		    title: title,
		    skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['700px', '400px'],
			content: $(doc),
			cancel: function(){
				// 重置表单
				$('#sbwl_task_picture_type_setting_id')[0].reset();
			}
		});
		//layer.full(task_layer);
		oprType = opr;
	}
  
	// ~from表单监听提交
	form.on('submit(sbwl_task_picture_type_setting_submit)', function(data){
	   // 提交后台数据!
		shenbi.ajax({
			type:'post',
			url:oprType=='add'?'/mark/goods/set/save':'/mark/goods/set/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.close(task_layer);
					layer.msg("成功"); 
					// 重置表单
					$('#sbwl_task_picture_type_setting_id')[0].reset();
				}else{
					// 错误提示
					layer.msg(resp.message); 
				}
				// 刷新数据表
				table_reload();
			}
		});
		return false
	  });
});