
layui.use(['table','form'], function(){
  var table = layui.table;
  var form  = layui.form;
  var $=layui.jquery;
  
  //操作类型[add|update],默认为add
  var oprType = 'add';
  var layer_ = ''; 
  
  table.render({
    elem: '#table-data',
	url: '/mirror/list',
	toolbar: '#toolbar',
	title: '照耀镜数据表',
	id: 'table_list',
	cols: [[
	  {type:'checkbox',sort:true,align: 'center'},
	  {type:'numbers',title:'序号',align: 'center',width:'5%'},
	  {field:'cookie', title:'cookie'},
	  {field:'hostOne', title:'第一步'},
	  {field:'hostOneParams', title:'第一步参数'},
	  {field:'hostTwo', title:'第二步 '},
	  {field:'hostTwoParams', title:'第二步参数'},
	  {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
	  }},
	  {fixed: 'right', title:'操作', toolbar: '#operation', width:180,align: 'center'}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return {
          "code": res.retCode, 		//解析接口状态
          "msg": res.data.message, 	//解析提示文本
          "count": res.data.total, 	//解析数据长度
          "data": res.data.list 	//解析数据列表
        }
    },
	page: true
  });
  
  //监听搜索按钮
  form.on('submit(sbwl_search)', function(data){
    //layer.msg(JSON.stringify(data.field));
    table.reload('table_list',{
    	page: {
            curr: 1 //重新从第 1 页开始
         },
         where:data.field
    });
    return false;
  });
  
  // 刷新列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
    	case 'sbwl_dels':
		    var data = checkStatus.data;
		    var ids = [];
		    $.each(data,function(index,item){
			  ids.push(item.id);
		    });
		    
		    if(ids.length<=0){
		    	layer.msg("至少选择一项进行操作"); 
		    	return false
		    }
		    
  	    	layer.msg('确定要批量删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/mirror/deletes',
	    				sendMsg:'删除中...',
	    				data:{ids:ids.join()},
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
  	    	
    		break;
	    case 'sbwl_create':
	    	 $("input[name=id]").val("");
	    	 
		  	// 新增页面
			showEditOrAdd('新增','#sbwl_tmpl','add');
	    break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'sbwl-set'){
		     
	    } else if(obj.event === 'sbwl-edit'){
	    	 // 设置表单数据!
	   	    form.val("sbwl_from", {
	   	      "id":data.id,
	   	      "cookie":data.cookie,	
			  "hostOne":data.hostOne,
			  "hostOneParams":data.hostOneParams,
			  "hostTwo":data.hostTwo,
			  "hostTwoParams":data.hostTwoParams
			});
	   	    
	    	// 打开编辑页面!
			showEditOrAdd('编辑','#sbwl_tmpl','update');
	    }else if(obj.event === 'sbwl-del'){
	    	layer.msg('确定要删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/mirror/delete',
	    				sendMsg:'删除中...',
	    				data:{id:data.id},
	    				success:function(resp){
	    					if(resp.retCode==0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
	    }else{
	    	layer.msg("没有相关操作");
	    }
  	});
  
  	//打开编辑页面或者是新增数据页面
  	function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
	        type: 1,
	        title: title,
	        skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['800px', '450px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		oprType = opr;
  	}

  	
	  //from表单监听提交
	  form.on('submit(sbwl_submit)', function(data){
	   // 提交后台数据!
		shenbi.ajax({
			post:'post',
			url:oprType=='add'?'/mirror/save':'/mirror/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode==0){
					layer.close(layer_);
					if(oprType=="add"){
						layer.msg("成功"); 
					}else{
						layer.msg("已更新"); 
					}
					// 刷新数据表
					table_reload();
					// 重置表单
					$('#sbwl_from_id')[0].reset();
				}
			}
		});
		return false
	  });
});