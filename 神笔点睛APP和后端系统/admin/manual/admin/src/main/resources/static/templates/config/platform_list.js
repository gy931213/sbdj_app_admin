
layui.use(['table','form','upload'], function(){
	var upload = layui.upload;
	var table = layui.table;
	var form  = layui.form;
	var $=layui.jquery;

	//操作类型[add|update],默认为add
	var oprType = 'add';
	var layer_ = '';

	table.render({
		elem: '#table-data',
		url: '/config/platform/list',
		toolbar: '#toolbar',
		title: '平台数据表',
		id: 'table_list',
		cols: [[
			{type:'checkbox',sort:true,align: 'center'},
			{type:'numbers',title:'序号',align: 'center',width:'5%'},
			{field:'name', title:'平台名称',align: 'center'},
			{field:'url',height:150, width:200,title:'图片链接',align: 'center'},
			{field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
					return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
				}},
			{fixed: 'right', title:'操作', toolbar: '#operation', width:180,align: 'center'}
		]],
		limit:50,
		limits:[50,100,200,500,1000],
		parseData: function(res){
			return {
				"code": res.retCode, 		//解析接口状态
				"msg": res.data.message, 	//解析提示文本
				"count": res.data.total, 	//解析数据长度
				"data": res.data.list 	//解析数据列表
			}
		},
		page: true
	});

	//监听搜索按钮
	form.on('submit(sbwl_search)', function(data){
		//layer.msg(JSON.stringify(data.field));
		table.reload('table_list',{
			page: {
				curr: 1 //重新从第 1 页开始
			},
			where:data.field
		});
		return false;
	});

	//~监听输入事件
	$('input[name="name"]').on('input',function(i,item){
		var val = $(this).val();
		var char =pinyin.getCamelChars(val);
		$('input[name="code"]').val(char);
	});


	// 刷新列表
	function table_reload(){
		table.reload('table_list');
	}

	//头工具栏事件
	table.on('toolbar(table-filter)', function(obj){
		var checkStatus = table.checkStatus(obj.config.id);
		switch(obj.event){
			case 'sbwl_dels':
				var data = checkStatus.data;
				var ids = [];
				$.each(data,function(index,item){
					ids.push(item.id);
				});

				if(ids.length<=0){
					layer.msg("至少选择一项进行操作");
					return false
				}

				layer.msg('确定要批量删除吗？', {
					btn: ['确定', '取消'],
					yes: function(index, layero){
						var data = obj.data;
						shenbi.ajax({
							type:'post',
							url:'/config/platform/deletes',
							data:{ids:ids.join()},
							sendMsg:'删除中...',
							success:function(resp){
								if(resp.retCode === 0){
									layer.msg("已删除");
								}else{
									layer.msg("删除失败");
								}
								table_reload();
							}
						});
					}
				});

				break;
			case 'sbwl_create':
				$("input[name=id]").val("");
				$("#sbwl-img ").attr('src',"");
				// 新增页面
				showEditOrAdd('新增','#sbwl_tmpl','add');
				break;
		};
	});

	//监听行工具事件
	table.on('tool(table-filter)', function(obj){
		var data = obj.data;
		console.log(obj)
		if(obj.event === 'sbwl-set'){

		} else if(obj.event === 'sbwl-edit'){
			// 设置表单数据!
			form.val("sbwl_from", {
				"id":data.id,
				"name":data.name,
				"url":data.url,
				"code":data.code,
				"sort": data.sort
			});

			// 图片
			$("#sbwl-img ").attr('src',data.url);

			// 打开编辑页面!
			showEditOrAdd('编辑','#sbwl_tmpl','update');
		}else if(obj.event === 'sbwl-del'){
			layer.msg('确定要删除吗？', {
				btn: ['确定', '取消'],
				yes: function(index, layero){
					var data = obj.data;
					shenbi.ajax({
						type:'post',
						url:'/config/platform/delete',
						data:{id:data.id},
						sendMsg:'删除中...',
						success:function(resp){
							if(resp.retCode === 0){
								layer.msg("已删除");
							}else{
								layer.msg("删除失败");
							}
							table_reload();
						}
					});
				}
			});
		}else{
			layer.msg("没有相关操作");
		}
	});

	//打开编辑页面或者是新增数据页面
	function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
			type: 1,
			title: title,
			skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['650px', '410px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		oprType = opr;
	}

	//from表单监听提交
	form.on('submit(sbwl_submit)', function(data){
		// 提交后台数据!
		shenbi.ajax({
			type:'post',
			url:oprType==='add'?'/config/platform/save':'/config/platform/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.close(layer_);
					if(oprType==="add"){
						layer.msg("成功");
					}else{
						layer.msg("已更新");
					}
					// 刷新数据表
					table_reload();
					// 重置表单
					$('#sbwl_from_id')[0].reset();
				}
			}
		});
		return false
	});

	// 图片上传
	var $showLoad = '';
	var uploadInst = upload.render({
		elem: '#test1', //绑定元素
		url: '/base/upload', //上传接口
		accept:'images',
		acceptMime:'image/*',
		before:function(){
			// 文件提交上传前的回调
			$showLoad = shenbi.showLoad("上传中...");
		},
		done: function(res){
			shenbi.closeLoad($showLoad);
			//上传完毕回调
			if(res.retCode==0 && res.message!=""){
				console.log(res.message);
				$("input[name='url']").val(res.message);
				$("#sbwl-img").attr('src',res.message)
			}
		},error: function(res){
			//请求异常回调
			console.log(res)
		}
	});
});