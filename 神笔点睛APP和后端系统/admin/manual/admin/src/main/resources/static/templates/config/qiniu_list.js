
layui.use(['table','form'], function(){
  var table = layui.table;
  var form  = layui.form;
  var $=layui.jquery;
  
  //操作类型[add|update],默认为add
  var oprType = 'add';
  var layer_ = ''; 
  
  table.render({
    elem: '#table-data',
	url: '/qn/list',
	toolbar: '#toolbar',
	title: '七牛数据表',
	id: 'table_list',
	cols: [[
	  {type:'checkbox',sort:true,align: 'center'},
	  {type:'numbers',title:'序号',align: 'center',width:'5%'},
	  {field:'orgName', title:'所属机构'},
	  {field:'zone', title:'服务器所属区域',templet:function(rse){
		  if(rse.zone === 0){
			  return "华东";
		  }else if(rse.zone === 1){
			  return "华北";
		  }else if(rse.zone === 2){
			  return "华南";
		  }else if(rse.zone === 3){
			  return "北美";
		  }else if(rse.zone === 4){
			  return "东南亚";
		  }
	  }},
	  {field:'accessKey', title:'AccessKey'},
	  {field:'secretKey', title:'SecretKey'},
	  {field:'bucket', title:'存储空间名 '},
	  {field:'host', title:'服务器地址'},
	  {field:'imageView', title:'图片参数'},
	  {fixed: 'right', title:'操作', toolbar: '#operation', width:180,align: 'center'}
    ]],
    limits:[30,50,70,80],
    parseData: function(res){ 
        return {
          "code": res.retCode, 		//解析接口状态
          "msg": res.data.message, 	//解析提示文本
          "count": res.data.total, 	//解析数据长度
          "data": res.data.list 	//解析数据列表
        }
    },
	page: true
  });
  
  //监听搜索按钮
  form.on('submit(sbwl_search)', function(data){
    //layer.msg(JSON.stringify(data.field));
    table.reload('table_list',{
    	page: {
            curr: 1 //重新从第 1 页开始
         },
         where:data.field
    });
    return false;
  });
  
  // 刷新列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  	//渲染select选项 
	kits.select({
		elem:'#sbwl_org_select_1',
		url:'/sys/organization/select',
		value:'id',
		name:'name',
		form:form
	});
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
    	case 'sbwl_dels':
		    var data = checkStatus.data;
		    var ids = [];
		    $.each(data,function(index,item){
			  ids.push(item.id);
		    });
		    
		    if(ids.length<=0){
		    	layer.msg("至少选择一项进行操作"); 
		    	return false
		    }
		    
  	    	layer.msg('确定要批量删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/config/bank/deletes',
	    				sendMsg:'删除中...',
	    				data:{ids:ids.join()},
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
  	    	
    		break;
	    case 'sbwl_create':
	    	 $("input[name=id]").val("");
	    	 
	    	 //渲染select选项（机构） 
	    	 kits.select({
	    		 elem:'.sbwl_org_select',
	    		 url:'/sys/organization/select',
	    		 value:'id',
	    		 name:'name',
	    		 form:form
	    	 });
	    	 
		  	// 新增页面
			showEditOrAdd('新增','#sbwl_tmpl','add');
	    break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
	    var data = obj.data;
	    console.log(obj)
	    if(obj.event === 'sbwl-set'){
		     
	    } else if(obj.event === 'sbwl-edit'){
	    	 // 设置表单数据!
	   	    form.val("sbwl_from", {
	   	      "id":data.id,
	   	      "zone":data.zone,	
			  "accessKey":data.accessKey,
			  "secretKey":data.secretKey,
			  "imageView":data.imageView,
			  "host":data.host,
			  "bucket":data.bucket
			});
	   	    
	   	    // 渲染机构的 select选项 
	    	kits.select({
	    		 elem:'.sbwl_org_select',
	    		 url:'/sys/organization/select',
	    		 value:'id',
	    		 name:'name',
	    		 select:data.orgId,
	    		 form:form
	    	 });
	   	    
	    	// 打开编辑页面!
			showEditOrAdd('编辑','#sbwl_tmpl','update');
	    }else if(obj.event === 'sbwl-del'){
	    	layer.msg('确定要删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/qn/delete',
	    				sendMsg:'删除中...',
	    				data:{id:data.id},
	    				success:function(resp){
	    					if(resp.retCode==0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
	    }else{
	    	layer.msg("没有相关操作");
	    }
  	});
  
  	//打开编辑页面或者是新增数据页面
  	function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
	        type: 1,
	        title: title,
	        skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['650px', '510px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		oprType = opr;
  	}

	    //from表单验证规则
		form.verify({
			orgId: function(value){
				if(value==0){
					return "请选择对应组织";
				}
			}
		 });
  	
	  //from表单监听提交
	  form.on('submit(sbwl_submit)', function(data){
	   // 提交后台数据!
		shenbi.ajax({
			post:'post',
			url:oprType=='add'?'/qn/save':'/qn/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode==0){
					layer.close(layer_);
					if(oprType=="add"){
						layer.msg("成功"); 
					}else{
						layer.msg("已更新"); 
					}
					// 刷新数据表
					table_reload();
					// 重置表单
					$('#sbwl_from_id')[0].reset();
				}
			}
		});
		return false
	  });
});