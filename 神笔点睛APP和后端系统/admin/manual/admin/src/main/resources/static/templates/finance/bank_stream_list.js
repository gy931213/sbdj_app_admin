
layui.use(['table','form','laydate'], function(){
  var table = layui.table;
  var laydate = layui.laydate;
  var form  = layui.form;
  
	//开始日期
	laydate.render({
		elem: '#beginDate',
		value: new Date(),
	});
	
	//结束日期
	laydate.render({
		elem: '#endDate',
		value: new Date(),
	});
	
	// 流水日期
	laydate.render({
		elem: '#streamDate',
		value: new Date()
	});
  
  table.render({
	id:'table_list',
    elem: '#table-data',
	url:'/bank/stream/list',
	toolbar: '#toolbar',
	title: '银行流水数据表',
	cols: [[
	  {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
        {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
	  {field:'orgName', title:'所属机构', align: 'center',hide:true},
	  {field:'streamDate', title:'流水日期', align: 'center',hide:true},
	  {field:'bankName', title:'所属银行',align: 'center'},
	  {field:'userName', title:'用户名称', align: 'center',edit: 'text'},
	  {field:'defray', title:'支出', align: 'center'},
	  {field:'earning', title:'收入', align: 'center'},
	  {field:'balance', title:'余额', align: 'center'},
	  {field:'isSeller', title:'商家', align: 'center',templet:function(resp){
		  if(resp.isSeller === 1){
			  return '<a class="layui-btn layui-btn-xs layui-btn-radius layui-btn-norma">是</a>';
		  }else{
			  return '<a class="layui-btn layui-btn-xs layui-btn-radius layui-btn-normal">否</a>';
		  }
	  }},
	  {field:'streamType', title:'流水类型', align: 'center',templet:function(resp){
		  if(resp.streamType === 1){
			  return '<a class="layui-btn layui-btn-xs layui-btn-radius layui-btn-norma">银行流水</a>';
		  }else{
			  return '<a class="layui-btn layui-btn-xs layui-btn-radius layui-btn-normal">其他流水</a>';
		  }
	  }},
	  {field:'remark', title:'备注',edit: 'text'},
	  {field:'createTime', title:'创建时间', align: 'center',hide:true},
	  {fixed: 'right', title:'操作', width:150,templet:function(resp){
              return '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="stream-delete"><i class="layui-icon">&#xe640;</i> 删除</a>';
		  /*if(resp.streamType == 0){
			  return '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="stream-delete"><i class="layui-icon">&#xe640;</i> 删除</a>';
		  }else{
			  return '<a class="layui-btn layui-btn-xs layui-btn-disabled"><i class="layui-icon">&#xe640;</i> 删除</a>';
		  }*/
	  }}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return shenbi.returnParse(res);
    },
	page: true
  });
  
  
  //监听搜索按钮
  form.on('submit(sbwl_search)', function(data){
    table.reload('table_list',{
    	page: {
            curr: 1 //重新从第 1 页开始
         },
         where:data.field
    });
    return false;
  });
  
  // 刷新列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  // 注：edit是固定事件名，test是table原始容器的属性 lay-filter="对应的值"
  // 监听数据列表中的值更新
  table.on('edit(table-filter)', function(obj){ 
	  /*console.log(obj.value); //得到修改后的值
	  console.log(obj.field); //当前编辑的字段名
	  console.log(obj.data); //所在行的所有相关数据  
	  */
	  // 更新数据
	  shenbi.ajax({
		type:'post',
		url:'/bank/stream/update',
		data:{
			id:obj.data.id,
			param:obj.value,
			field:obj.field
		},
		sendMsg:'更新中...',
		success:function(resp){
			if(resp.retCode === 0){
				layer.msg("已更新");
			}else{
				layer.msg("更新失败");
			}
			// ~刷新数据
			table_reload();
		}
	  });
  });
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'add-stream':
       //var data = checkStatus.data;
        //layer.alert(JSON.stringify(data));
    	  
    	  // 所属银行
 	   	 kits.select({
 	   		 elem:'.sbwl_bank_select',
 	   		 url:'/config/bank/select',
 	   		 value:'id',
 	   		 name:'name',
 	   		 form:form
 	   	 });
    	  
    	  showStream("新增","#stream");
      break;
      case 'isAll':
        layer.msg(checkStatus.isAll ? '全选': '未全选');
      break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    if(obj.event === 'stream-delete'){
    	 // 删除
		 layer.msg('确定要删除吗？', {
	   	    btn: ['确定', '取消'],
	   	    yes: function(index, layero){
	   	    	shenbi.ajax({
	   				type:'post',
	   				url:'/bank/stream/delete',
	   				data:{
	   					id:data.id
	   				},
	   				sendMsg:'删除中...',
	   				success:function(resp){
	   					if(resp.retCode === 0){
	   						layer.msg("已删除");
	   					}else{
	   						layer.msg(resp.message == "" ? "删除失败" : resp.message);
	   					}
	   					// ~刷新数据
	   					table_reload();
	   				}
	   			 });
	   	     }
		 });
    } else{
		layer.msg("没有相关操作");
    }
  });
  
  var stream = '';
  function showStream(title, doc){
  	$('#stream-form')[0].reset();
  		stream = layer.open({
          type: 1,
          title: title,
          skin: 'layui-layer-molv',
          shadeClose: false,
          area: ['620px', '540px'],
          content: $(doc),
          cancel: function(){
              $('#stream-form')[0].reset();
          }
      });
  }
  
  form.verify({
	  bankId: function(value){
		if(value==0){
			return "请选择所属银行";
		}
	  }
  });
  
  
  var intdefault = 0;
  // 支出
  $("input[name='defray']").on("input",function(e){
    //获取input输入的值
    console.log($(this).val());
    $("input[name='earning']").val(intdefault.toFixed(2));
  });
  
  // 收入
  $("input[name='earning']").on("input",function(e){
    //获取input输入的值
    console.log($(this).val());
    $("input[name='defray']").val(intdefault.toFixed(2));
  });
  
  
  form.on('submit(sbwl_stream_submit)', function(resp){
	  var data = resp.field;
	  if(data.isSeller == undefined){
		 data['isSeller'] = 0;
	  }else if(undefined != data.isSeller && data.isSeller == 'on' ){
		 data['isSeller'] = 1;
	  }else{
		  data['isSeller'] = 0;
	  }
	  data.streamDate =new Date(data.streamDate);
	  
	  // 提交后台数据!
	  shenbi.ajax({
		type:'post',
		url:'/bank/stream/save',
		data:data,
		sendMsg:'提交中...',
		success:function(resp){
			if(resp.retCode === 0){
				layer.msg("提交成功"); 
				// 刷新数据表
				table_reload();
				// 重置表单
				$('#stream-form')[0].reset();
				layer.close(stream);
			}else{
				// ~异常信息
				layer.msg(resp.message);
			}
		}
	 });
	return false
  });
  
  
});