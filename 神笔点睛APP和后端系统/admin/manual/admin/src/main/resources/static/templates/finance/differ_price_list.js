
layui.use(['table','form','laydate'], function(){
    var table = layui.table;
    var laydate = layui.laydate;
    var form  = layui.form;

    //开始日期
    laydate.render({
        elem: '#beginDate',
        type: 'datetime'
    });
    //结束日期
    laydate.render({
        elem: '#endDate',
        type: 'datetime'
    });

    // ~渲染任务类型
    kits.select({
        elem:'#sbwl-taskType-select',
        url:'/base/code',
        data:{code:'TASK_TYPE'},
        value:'id',
        name:'name',
        form:form,
    });

    // ~渲染select选项 （机构）
/*    kits.select({
        elem:'#sbwl_org_select',
        url:'/sys/organization/select',
        value:'id',
        name:'name',
        form:form
    });*/

    // 快链字段设置样式
    var key = ['salesmanName','onlineid','shopName'];

    table.render({
        id:'table_list',
        elem: '#table-data',
        url:'/differ/price/list',
        title: '差价数据表',
        toolbar: '#toolbar',
        height: 'full-20',
        where: {status:'NO'},
        totalRow: true,
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left', totalRowText: '合计'},
            {field:'orgName', title:'所属机构',align: 'center',hide:true},
            {field:'jobNumber', title:'工号', align: 'center',hide:true},
            {field:'salesmanName', title:'姓名', align: 'center',event:'search-salesmanName'},
            {field:'shopName', title:'所属店铺', align: 'center',event:'search-shopName'},
            {field:'title', title:'商品标题', align: 'center',hide:true},
            {field:'link', title:'商品链接', align: 'center',templet:function(rse){
                    return '<a href="'+rse.link+'" target="_blank">'+rse.link+'</a>';
                }},
            {field:'taskTypeCode', title:'任务类型', align: 'center',templet:function(rse){
                    switch (rse.taskTypeCode) {
                        case 'XFD':
                            return '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #058c14;">现付单</a>';
                        case 'GRD':
                            return '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #9a5400;">隔日单</a>';
                        case 'LLD':
                            return '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #c307c5;">浏览单</a>';
                        default:
                            return '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #d90000;">未知</a>';
                    }
                }},
            {field:'onlineid', title:'号主ID', align: 'center',event:'search-onlineid'},
            {field:'realPrice', title:'应付金额', align: 'center', totalRow: true},
            {field:'payment', title:'实付金额', align: 'center', totalRow: true},
            {field:'differPrice', title:'差价金额 ', align: 'center', totalRow: true},
            {field:'status', title:'是否审核', align: 'center',hide:true,templet:function(rse){
                    switch (rse.status) {
                        case 'NO':
                            return '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #d9534f;">否</a>';
                        case 'YES':
                            return '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #60c560;">是</a>';
                        default:
                            return '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #d9534f;">作废</a>';
                    }
                }},
            {field:'createTime', title:'领取时间', align: 'center',templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {field:'oprName', title:'审核人 ', align: 'center',hide:true},
            {field:'oprTime', title:'审核时间', align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.oprTime);
                }},
            {fixed: 'right', title:'操作',align:'center', width:'11%',templet:function (rse) {
                    var _html = '';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-check-img"><i class="layui-icon">&#xe615;</i> 截图</a>';
                    if (rse.status === 'NO') {
                        _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-auditing"><i class="layui-icon">&#xe605;</i> 审核</a>';
                    } else if (rse.status === 'YES') {
                        _html+='<a class="layui-btn layui-btn-sm" style="background-color: #999;"><i class="layui-icon">&#xe605;</i> 审核</a>';
                    } else if (rse.status === 'D') {
                        _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-del"><i class="layui-icon">&#xe640;</i> 删除</a>';
                    }
                    return _html;
                }}
        ]],
        limit:50,
        limits:[10,30,50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true
    });

    //表头样式
    tableThStyle(key);

    //监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });

        // 表头样式
        tableThStyle(key);
        return false;
    });

    // 刷新列表
    function table_reload(){
        table.reload('table_list');
        //表头样式
        tableThStyle(key);
    }

    // ~头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'getCheckData':
                var data = checkStatus.data;
                layer.alert(JSON.stringify(data));
                break;
            case 'getCheckLength':
                var data = checkStatus.data;
                layer.msg('选中了：'+ data.length + ' 个');
                break;
            case 'isAll':
                layer.msg(checkStatus.isAll ? '全选': '未全选');
                break;
            case 'review':
                // 批量选中要对单的任务数据!
                var data = checkStatus.data;
                if(data.length < 2){
                    layer.msg("必须选中2项进行操作！");
                    return false;
                }
                // 获取所选中的数据
                var ids = [];
                $.each(data,function(i,item){
                    ids.push(item.id);
                });
                if(ids.length < 2){
                    layer.msg("必须选中2项进行操作！");
                    return false;
                }
                var win = layer.open({
                    title:'批量审核'
                    ,btn: ['通过', '拒绝', '取消']
                    ,btn1: function(index, layero){
                        //按钮【按钮一】的回调
                        shenbi.ajax({
                            type:'post',
                            url:'/differ/price/auditors',
                            data:{ids:ids.join(),status:'YES'},
                            sendMsg:'审核中...',
                            success:function(resp){
                                if(resp.retCode === 0) {
                                    var data = resp.data;
                                    if (data > 0) {
                                        var html = '<ul class="layer_notice" style="display: block;background: #279a3fc4;">';
                                        html += ' <li>&nbsp;<a style="color: #efeaea"> 一共有 '+data+' 条数据,业务员余额不足,无法处理...</a>&nbsp;</li>';
                                        html += '</ul>';
                                        layer.open({
                                            type: 1,
                                            shade: false,
                                            title: false, //不显示标题
                                            content: html,
                                            cancel: function () {
                                                //layer.msg('捕获就是从页面已经存在的元素上，包裹layer的结构', {time: 5000, icon:6});
                                            }
                                        });
                                    } else {
                                        layer.msg("已审核");
                                    }
                                } else{
                                    layer.msg("审核失败");
                                }
                                table_reload();
                                layer.close(win);
                            }
                        });
                        return false;
                    }
                    ,btn2: function(index, layero){
                        //按钮【按钮二】的回调
                        shenbi.ajax({
                            type:'post',
                            url:'/differ/price/auditors',
                            data:{ids:ids.join(),status:'D'},
                            sendMsg:'审核中...',
                            success:function(resp){
                                if(resp.retCode === 0){
                                    layer.msg("已审核");
                                }else{
                                    layer.msg("审核失败");
                                }
                                table_reload();
                                layer.close(win);
                            }
                        });
                        return false;
                    }
                    ,btn3: function(index, layero){
                        //按钮【按钮三】的回调
                    }
                    ,cancel: function(){
                        //右上角关闭回调
                    }
                });
                /*s*/
                break;
        };
    });
    //监听行工具事件
    table.on('tool(table-filter)', function(obj) {
        var data = obj.data;
        if (obj.event === 'sbwl-set') {

        } else if (obj.event === 'sbwl-edit') {
            // 设置表单数据!
            /*form.val("sbwl_from", {
                "id":data.id,
                "name":data.name,
                "mobile":data.mobile,
                "gender":data.gender,
                "referrer":data.referrer
            });*/
        } else if (obj.event === 'sbwl-del') {
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function (index, layero) {
                    var data = obj.data;
                    shenbi.ajax({
                        type: 'post',
                        url: '/differ/price/delete',
                        data: {id: data.id},
                        sendMsg: '删除中...',
                        success: function (resp) {
                            if (resp.retCode === 0) {
                                layer.msg("已删除");
                            } else {
                                layer.msg("删除失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        } else if (obj.event === 'search-salesmanName') {
            // 模拟鼠标双击
            if (!dblclick()) {
                return false;
            }
            // 业务员名称
            table.reload('table_list', {
                where: {'salesmanName': data.salesmanName}
            });
            $("input[name='salesmanName']").val(data.salesmanName);
        } else if (obj.event === 'search-onlineid') {
            // 模拟鼠标双击
            if (!dblclick()) {
                return false;
            }
            // 号主昵称
            table.reload('table_list', {
                where: {'onlineid': data.onlineid}
            });
            $("input[name='onlineid']").val(data.onlineid);
        } else if (obj.event === 'search-shopName') {
            // 模拟鼠标双击
            if (!dblclick()) {
                return false;
            }
            // 店铺名称
            table.reload('table_list', {
                where: {'shopName': data.shopName}
            });
            $("input[name='shopName']").val(data.shopName);
        } else if (obj.event === 'sbwl-auditing') {
            // ~子任务审核
            $("#differ_price_id").val(data.id);
            $("#differ_price_onlineid").val(data.onlineid);
            // ~打开审核任务窗口
            showAuditor("审核", "#sbwl_auditor_tmpl");
        } else if (obj.event ===  'sbwl-check-img') {
            var data = obj.data;
            shenbi.ajax({
                type: 'post',
                url: '/task/son/get',
                data: {id: data.taskSonId},
                success: function (res) {
                    var resp = res.data;
                    if (null !== resp) {
                        // ~表单赋值
                        form.val("sbwl_task_son_img_from", {
                            "id":resp.id,
                            "imei":resp.imei,
                            "shopName":resp.shopName,
                            "sellerNick":resp.sellerNick,
                            "keyword":resp.keyword,
                            "onlineid":resp.onlineid,
                            "payment":resp.payment
                        });

                        // 渲染商品主图
                        if(null == resp.imgLink || "" == resp.imgLink){
                            $("#shopGoodsImg").attr('src','/img/why.jpg');
                        }else{
                            $("#shopGoodsImg").attr('src',resp.imgLink);
                        }
                        // ~查看任务截图
                        // ~获取业务员相关的证件图片~
                        shenbi.ajax({
                            type:'post',
                            url:'/task/son/find/imgs',
                            data:{taskSonId:resp.id},
                            success:function(respa){
                                if(respa.retCode === 0){
                                    var data = respa.data;
                                    var _html = '</br>';
                                    for (var i = 0; i < data.length; i++) {
                                        _html+='<div class="layui-form-item task-div-son-img-li">';
                                        _html+='	<div class="sbwl-task-son-img">';
                                        _html+='		<img id="sbwl-img" alt="" src="'+data[i].link+'" style="width: 400px">';
                                        _html+='		<div class="scr"><b>'+data[i].typeName+'</b></div>';
                                        _html+='	</div>';
                                        _html+='</div>';
                                    }
                                    // ~渲染数据~
                                    $("#sbwl_taskSon_tmpl_img").html(_html);
                                    // ~渲染数据~
                                    showTaskSonImgSrc('<i class="layui-icon">&#xe64a;</i> 任务截图','#sbwl_taskSon_tmpl');
                                }
                            }
                        });
                    }
                }
            });
        }else{
            layer.msg("没有相关操作");
        }
        // 表头样式
        tableThStyle(key);
    });

    // 更新实付价格的操作!
    form.on('submit(payment_submit)', function(data){
        shenbi.ajax({
            type:'post',
            url: '/task/son/update/payment',
            data:data.field,
            sendMsg: '提交中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.msg("提交成功");
                }else{
                    layer.msg(resp.message==='error'?"提交失败":resp.message);
                }
                // ~刷新数据
                table_reload();
            }
        });
        return false
    });

    // ~审核任务窗口
    var _layer;
    function showAuditor(title,doc){
        _layer= layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            area: ['300px', '180px'],
            content: $(doc),
            cancel: function(){
                $("#sbwl_auditor_id")[0].reset();
                $(".js-hidden").addClass("sbwl-hidden");
            }
        });
    }

    //~查看子任务截图的弹出层~
    function showTaskSonImgSrc(title,doc) {
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function () {

            }
        });
    };

    // ~审核任务from表单监听提交
    form.on('submit(sbwl_auditor_submit)', function(data){

        shenbi.ajax({
            type:'post',
            url: '/differ/price/auditor',
            data:data.field,
            sendMsg:  '审核中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.msg("已审核");
                }else{
                    layer.msg(resp.message==="error"?"审核失败":resp.message);
                }
                // ~刷新数据
                table_reload();
                $("#sbwl_auditor_id")[0].reset();
                $(".js-hidden").addClass("sbwl-hidden");
                layer.close(_layer);
            }
        });
        return false
    });
});