
layui.use(['table','form','laydate','upload'], function(){
  var table = layui.table;
  var laydate = layui.laydate;
  var form  = layui.form;
  var upload = layui.upload;
  
	//开始日期
	laydate.render({
		elem: '#beginDate',
		type: 'datetime'
	});
	//结束日期
	laydate.render({
		elem: '#endDate',
		type: 'datetime'
	});
  
  table.render({
	id:'table_list',
    elem: '#table-data',
	url:'/draw/money/record/list',
	toolbar: '#toolbar',
	title: '提现数据表',
	cols: [[
	  {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
	  {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
	  {field:'id', title:'提现ID'},
	  {field:'orgName', title:'所属机构', align: 'center',hide:true},
	  {field:'jobNumber', title:'工号',align: 'center'},
	  {field:'salesmanName', title:'业务员姓名', align: 'center'},
	  {field:'bankNum', title:'银行卡号', align: 'center'},
	  {field:'bankName', title:'开户行', align: 'center'},
	  {field:'bankad', title:'支行', align: 'center'},
	  {field:'mobile', title:'业务员电话', align: 'center'},
	  {field:'provinceName', title:'省', align: 'center',hide:true},
	  {field:'cityName', title:'市', align: 'center',hide:true},
	  {field:'countyName', title:'区/镇', align: 'center',hide:true},
	  {field:'balance', title:'总余额', align: 'center'},
	  {field:'applyMoney', title:'申请金额', align: 'center'},
	  {field:'auditorName', title:'审核人',align: 'center',hide:true},
	  {field:'createTime', title:'申请时间', align: 'center',templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
	  }},
	  {field:'auditorTime', title:'支付时间', align: 'center',hide:true,templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.auditorTime);
	  }},
	  {field:'payMoney', title:'支付金额', align: 'center',hide:true},
	  {fixed: 'right', title:'操作', width:150,align: 'center',templet:function(rse){
		  var _html = '';
		  if(rse.status === 'N'){
			  _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-payment"><i class="layui-icon">&#xe605;</i> 支付</a>';  
		  }else if(rse.status === 'Y'){
			  _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" style="background-color: #60c560;"> 已支付</a>';  
		  }else{
			  
		  }
		  return _html;
	  }}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return shenbi.returnParse(res);
    },
	page: true
  });
  
  
  //监听搜索按钮
  form.on('submit(sbwl_search)', function(data){
    table.reload('table_list',{
    	page: {
            curr: 1 //重新从第 1 页开始
         },
         where:data.field
    });
    return false;
  });
  
  // 刷新列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'getCheckData':
        var data = checkStatus.data;
        layer.alert(JSON.stringify(data));
      break;
      case 'getCheckLength':
        var data = checkStatus.data;
        layer.msg('选中了：'+ data.length + ' 个');
      break;
      case 'isAll':
        layer.msg(checkStatus.isAll ? '全选': '未全选');
      break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    if(obj.event === 'sbwl-payment'){
    	 // ~支付
		 layer.msg('确定要支付吗？', {
	   	    btn: ['确定', '取消'],
	   	    yes: function(index, layero){
	   	    	shenbi.ajax({
	   				type:'post',
	   				url:'/draw/money/record/payment',
	   				data:{
	   					id:data.id
	   				},
	   				sendMsg:'支付中...',
	   				success:function(resp){
	   					if(resp.retCode === 0){
	   						layer.msg("已支付");
	   					}else{
	   						layer.msg("支付失败");
	   					}
	   					// ~刷新数据
	   					table_reload();
	   				}
	   			 });
	   	     }
		 });
    } else if(obj.event === 'edit'){
      
    }
  });
  
//对账上传excel
  var $showLoad = '';
  var uploadInst = upload.render({
	elem: '#uploaderInput', 	//绑定元素
	url: '/draw/money/record/balance',  //上传接口
	type: "file",
	accept: 'file',
	before:function(){
		// 文件提交上传前的回调
		$showLoad = shenbi.showLoad("对账中...");
	},
	done: function(res){
		shenbi.closeLoad($showLoad);
		//上传完毕回调
		if(res.retCode === 0){
			layer.msg("已对账",{icon:1,time:3000},function(){
				//window.location.reload();
				// ~刷新数据
				table_reload();
			}); 
		}else{
			layer.msg(res.message,{icon:5,time:3000},function(){
				//window.location.reload();
				// ~刷新数据
				table_reload();
			}); 
		}
	},error: function(res){
	  //请求异常回调
		layer.msg("上传文件异常",{icon:5,time:3000},function(){
			//window.location.reload()
		}); 
	}
  });
  
  
});