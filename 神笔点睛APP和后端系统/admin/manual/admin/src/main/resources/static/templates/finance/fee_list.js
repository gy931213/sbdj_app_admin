
layui.use(['table','laydate'], function(){
  var table = layui.table;
  var laydate = layui.laydate;
	
	//开始日期
	laydate.render({
		elem: '#beginDate'
	});
	//结束日期
	laydate.render({
		elem: '#endDate'
	});
  
  table.render({
    elem: '#table-data',
	url:'/test/table/demo1.json',
	toolbar: '#toolbar',
	title: '提成数据表',
	cols: [[
      {type: 'checkbox', fixed: 'left'},
	  {field:'id', title:'序号', width:80, fixed: 'left', unresize: true,align: 'center'},
	  {field:'username', title:'业务员账号',align: 'center'},
	  {field:'username', title:'业务员姓名', align: 'center'},
	  {field:'username', title:'业务员银行卡', align: 'center'},
	  {field:'username', title:'业务员电话', align: 'center'},
	  {field:'username', title:'所在城市', align: 'center'},
	  {field:'username', title:'下级业务员编号', align: 'center'},
		{field:'username', title:'下级业务员姓名', align: 'center'},
		{field:'username', title:'子任务编号', align: 'center'},
		{field:'username', title:'店铺名称', align: 'center'},
		{field:'username', title:'商品名称', align: 'center'},
	  {field:'username', title:'提成', align: 'center'}
    ]],
	page: true
  });
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'getCheckData':
        var data = checkStatus.data;
        layer.alert(JSON.stringify(data));
      break;
      case 'getCheckLength':
        var data = checkStatus.data;
        layer.msg('选中了：'+ data.length + ' 个');
      break;
      case 'isAll':
        layer.msg(checkStatus.isAll ? '全选': '未全选');
      break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    //console.log(obj)
    if(obj.event === 'del'){
      layer.confirm('真的删除行么', function(index){
        obj.del();
        layer.close(index);
      });
    } else if(obj.event === 'edit'){
      layer.prompt({
        formType: 2
        ,value: data.email
      }, function(value, index){
        obj.update({
          email: value
        });
        layer.close(index);
      });
    }
  });
  
});