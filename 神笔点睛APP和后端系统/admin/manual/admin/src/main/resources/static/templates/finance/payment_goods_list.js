
layui.use(['table','form','laydate','upload'], function(){
    var table = layui.table;
    var laydate = layui.laydate;
    var form  = layui.form;
    var upload = layui.upload;

    //开始日期
    laydate.render({
        elem: '#beginDate',
        type: 'datetime'
    });
    //结束日期
    laydate.render({
        elem: '#endDate',
        type: 'datetime'
    });

    // 状态
    var statuss = [
        {'value':'A','name':'待操作'},
        {'value':'F','name':'待提交'},
        {'value':'T','name':'已审核'},
        {'value':'AU','name':'已传图'},
        {'value':'AN','name':'不通过'},
        {'value':'AF','name':'已失败'},
        {'value':'NF','name':'已作废'},
        {'value':'AP','name':'申诉中'}
    ];

    shenbi.select({
        elem:'#taskStatus',
        data:statuss,
        form:form
    });

    // 快链字段设置样式
    var key = ['salesmanName'];

    table.render({
        id:'table_list',
        elem: '#table-data',
        url:'/pay/goods/list',
        toolbar: '#toolbar',
        where:{status:'N'},
        title: '货款数据表',
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
            {field:'id', title:'货款ID',hide:true},
            {field:'applyNumber', title:'货款单号'},
            {field:'orgName', title:'所属机构',align: 'center',hide:true},
            {field:'jobNumber', title:'工号',align: 'center',hide:true},
            {field:'salesmanNumberId', title:'号主编号',align: 'center',hide:true},
            {field:'salesmanName', title:'姓名', align: 'center',event:'search-salesmanName'},
            {field:'sellerShopName', title:'所属店铺', align: 'center',hide:true},
            {field:'onlineid', title:'号主ID', align: 'center'},
            {field:'mobile', title:'电话', align: 'center',hide:true},
            {field:'bankNum', title:'银行卡号', align: 'center'},
            {field:'bankName', title:'开户行名称 ', align: 'center'},
            {field:'bankad', title:'支行名称', align: 'center'},
            {field:'province', title:'省', align: 'center',hide:true},
            {field:'city', title:'市', align: 'center',hide:true},
            {field:'district', title:'区/镇', align: 'center',hide:true},
            {field:'applyMoney', title:'货款金额', align: 'center'},
            {field:'createTime', title:'申请时间', align: 'center',templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {field:'payTime', title:'支付时间', align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.payTime);
                }},
            {field:'payMoney', title:'支付金额', align: 'center',hide:true},
            {field:'taskStatusName', title:'任务状态', align: 'center', templet:function(rse){
      		  if(rse.taskStatus === 'AU'){
    			  return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #009688;">待审核</a>';
    		  }else if(rse.taskStatus === 'T'){
    			  return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #60c560;">已审核</a>';
    		  }else if(rse.taskStatus === 'AN'){
    			  return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #d9534f;">不通过</a>';
    		  }else if(rse.taskStatus === 'AF'){
    			  return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #d9534f;">已失败</a>';
    		  }else if(rse.taskStatus === 'F'){
    			  return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #FFB800;">待提交</a>';
    		  }else if(rse.taskStatus === 'A'){
    			  return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #FFB800;">待操作</a>';
    		  } else if(rse.taskStatus === 'AP') {
                  return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #1ff0ff;">申诉中</a>';
              } else if(rse.taskStatus === 'NF') {
                  return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #ff37a6;">已作废</a>';
              }
    	  }},
            {field:'taskTypeName', title:'任务类型', align: 'center'},
            {field:'typeName', title:'货款类型', align: 'center',templet:function(rse){
                    if(rse.type === 'DF'){
                        return '<a class="layui-btn layui-btn-sm" style="width: 70px;background-color: #f0ad4e;">垫付</a>';
                    }
                    if(rse.type === 'SQ'){
                        return '<a class="layui-btn layui-btn-sm" style="width: 70px;background-color: #5bc0de;">申请</a>';
                    }
                }},
            {field:'comment', title:'备注/说明 ', align: 'center',hide:true},
            {field:'auditorName', title:'审核人 ', align: 'center',hide:true},
            {fixed: 'right', title:'操作',width:150,align: 'center',templet:function(rse){
                    var _html = '';
                    if(rse.status === 'N'){
                        _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-payment"><i class="layui-icon">&#xe605;</i> 支付</a>';
                    }else if(rse.status === 'Y'){
                        _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" style="background-color: #60c560;"> 已支付</a>';
                    }else{

                    }
                    return _html;
                }}
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true
    });

    //表头样式
    tableThStyle(key);

    //监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });

        // 表头样式
        tableThStyle(key);
        return false;
    });

    //监听指定开关
    form.on('switch(switchHistory)', function(data){
        if(this.checked){
            $('input[name="isHistory"]').val('YES');
            layer.tips('温馨提示：开启历史数据查询！', data.othis);
        }else{
            $('input[name="isHistory"]').val('NO');
            layer.tips('温馨提示：查询前 5 天的数据！', data.othis);
        }
    });

    // 刷新列表
    function table_reload(){
        table.reload('table_list');
        //表头样式
        tableThStyle(key);
    }

    // ~渲染任务类型
    kits.select({
        elem:'#sbwl-taskType-select',
        url:'/base/code',
        data:{code:'TASK_TYPE'},
        value:'id',
        name:'name',
        form:form,
    });

    // ~头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'dels':
                // 要进行删除操作的ids
                var ids = [];
                // 获取当前选中数据!
                var data = checkStatus.data;
                // 判断是否选中一项!
                if(data.length <= 0 ){
                    return  layer.msg('必须选中一项进行删除操作！');
                }
                // 循环遍历数据!
                for (var i = 0; i < data.length; i++) {
                    ids.push(data[i].id);
                }
                layer.msg('确定要批量删除吗？', {
                    btn: ['确定', '取消'],
                    yes: function(index, layero){
                        shenbi.ajax({
                            type:'post',
                            url:'/pay/goods/deletes',
                            data:{ids:ids.join()},
                            sendMsg:'删除中...',
                            traditional: true,
                            success:function(resp){
                                if(resp.retCode === 0){
                                    layer.msg("已删除");
                                }else{
                                    layer.msg("删除失败");
                                }
                                table_reload();
                            }
                        });
                    }
                });
                break;
        };
    });

//监听行工具事件
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        if(obj.event === 'sbwl-payment'){
            // ~支付
            layer.msg('确定要支付吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/pay/goods/payment',
                        data:{
                            id:data.id
                        },
                        sendMsg:'支付中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已支付");
                            }else{
                                layer.msg("支付失败");
                            }
                            // ~刷新数据
                            table_reload();
                        }
                    });
                }
            });
        } else if(obj.event === 'search-salesmanName'){
            // 模拟鼠标双击
            if(!dblclick()){
                return false;
            }
            // 店铺名称
            table.reload('table_list',{
                where:{'name':data.salesmanName}
            });
            $("input[name='name']").val(data.salesmanName);
        }

        // 表头样式
        tableThStyle(key);
    });


    // 对账上传excel
    var $showLoad = '';
    var uploadInst = upload.render({
        elem: '#uploaderInput', 	//绑定元素
        url: '/pay/goods/balance',  //上传接口
        type: "file",
        accept: 'file',
        before:function(){
            // 文件提交上传前的回调
            $showLoad = shenbi.showLoad("对账中...");
        },
        done: function(res){
            shenbi.closeLoad($showLoad);
            //上传完毕回调
            if(res.retCode === 0){
                layer.msg("已对账",{icon:1,time:3000},function(){
                    //window.location.reload();
                    // ~刷新数据
                    table_reload();
                });
            }else{
                layer.msg(res.message,{icon:5,time:3000},function(){
                    //window.location.reload();
                    // ~刷新数据
                    table_reload();
                });
            }
        },error: function(res){
            //请求异常回调
            layer.msg("上传文件异常",{icon:5,time:3000},function(){
                //window.location.reload()
            });
        }
    });

});