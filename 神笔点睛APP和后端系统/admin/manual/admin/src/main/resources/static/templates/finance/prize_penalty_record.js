
layui.use(['table','form','laydate'], function(){
  var table = layui.table;
  var laydate = layui.laydate;
  var form  = layui.form;

	//开始日期
	laydate.render({
		elem: '#beginDate',
		type: 'datetime'
	});
	//结束日期
	laydate.render({
		elem: '#endDate',
		type: 'datetime'
	});
  
  table.render({
	id:'table_list',
    elem: '#table-data',
	url:'/prize/penalty/list',
	toolbar: '#toolbar',
	title: '奖罚数据表',
	cols: [[
	  {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
	  {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
	  {field:'orgName', title:'所属机构', align: 'center',hide:true},
	  {field:'jobNumber', title:'工号',align: 'center'},
	  {field:'salesmanName', title:'业务员姓名', align: 'center'},
	  {field:'mobile', title:'业务员电话', align: 'center'},
      {field:'reason', title:'原因', align: 'center'},
      {field:'money', title:'金额', align: 'center'},
      {field:'type', title:'类型', align: 'center', templet:function(rse){
          var tmp = "";
          if (rse.type === 'PR') {
              tmp = '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #60c560;">奖励</a>';
          } else if (rse.type === 'PE') {
              tmp = '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #d9534f;">惩罚</a>';
          }
          return tmp;
      }},
	  {field:'auditorName', title:'审核人',align: 'center',hide:true},
	  {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
	  }}/*,
	  {fixed: 'right', title:'操作', width:150,align: 'center',templet:function(rse){
		  var _html = '';
		  if(rse.status === 'N'){
			  _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-payment"><i class="layui-icon">&#xe605;</i> 支付</a>';  
		  }else if(rse.status === 'Y'){
			  _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" style="background-color: #60c560;"> 已支付</a>';  
		  }else{
			  
		  }
		  return _html;
	  }}*/
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return {
          "code": res.retCode, 		//解析接口状态
          "msg": res.data.message, 	//解析提示文本
          "count": res.data.total, 	//解析数据长度
          "data": res.data.list 	//解析数据列表
        }
    },
	page: true
  });
  
  
  //监听搜索按钮
  form.on('submit(sbwl_search)', function(data){
    table.reload('table_list',{
    	page: {
            curr: 1 //重新从第 1 页开始
         },
         where:data.field
    });
    return false;
  });
  
  // 刷新列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'getCheckData':
        var data = checkStatus.data;
        layer.alert(JSON.stringify(data));
      break;
      case 'getCheckLength':
        var data = checkStatus.data;
        layer.msg('选中了：'+ data.length + ' 个');
      break;
      case 'isAll':
        layer.msg(checkStatus.isAll ? '全选': '未全选');
      break;
    };
  });

  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    if(obj.event === 'sbwl-payment'){
    	 // ~支付
		 layer.msg('确定要支付吗？', {
	   	    btn: ['确定', '取消'],
	   	    yes: function(index, layero){
	   	    	shenbi.ajax({
	   				type:'post',
	   				url:'/draw/money/record/payment',
	   				data:{
	   					id:data.id
	   				},
	   				sendMsg:'支付中...',
	   				success:function(resp){
	   					if(resp.retCode === 0){
	   						layer.msg("已支付");
	   					}else{
	   						layer.msg("支付失败");
	   					}
	   					// ~刷新数据
	   					table_reload();
	   				}
	   			 });
	   	     }
		 });
    } else if(obj.event === 'edit'){

    }
  });
});