
layui.use(['table','laydate'], function(){
  var table = layui.table;
  var laydate = layui.laydate;
	
	//开始日期
	laydate.render({
		elem: '#beginDate'
	});
	//结束日期
	laydate.render({
		elem: '#endDate'
	});
  
  table.render({
    elem: '#table-data',
	url:'/test/table/demo1.json',
	toolbar: '#toolbar',
	title: '微信红包数据表',
	cols: [[
	  {type: 'checkbox', fixed: 'left'},
	  {field:'id', title:'序号', width:80, fixed: 'left', unresize: true,align: 'center'},
	  {field:'username', title:'淘宝账号',align: 'center'},
	  {field:'username', title:'号主姓名', align: 'center'},
	  {field:'username', title:'领取人微信昵称', align: 'center'},
	  {field:'username', title:'淘宝订单号', align: 'center'},
	  {field:'username', title:'店铺名称', align: 'center'},
	  {field:'username', title:'发送红包金额', align: 'center'},
	  {field:'username', title:'审核时间', align: 'center'},
	  {field:'username', title:'创建时间', align: 'center'},
	  {field:'username', title:'红包支付时间', align: 'center'},
	  {field:'username', title:'状态', align: 'center'},
	  {field:'username', title:'审核管理员', align: 'center'},
	  {field:'username', title:'上传图片', align: 'center'},
	  {fixed: 'right', title:'操作', toolbar: '#operation', width:150,align: 'center'}
    ]],
	page: true
  });
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'getCheckData':
        var data = checkStatus.data;
        layer.alert(JSON.stringify(data));
      break;
      case 'getCheckLength':
        var data = checkStatus.data;
        layer.msg('选中了：'+ data.length + ' 个');
      break;
      case 'isAll':
        layer.msg(checkStatus.isAll ? '全选': '未全选');
      break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    //console.log(obj)
    if(obj.event === 'del'){
      layer.confirm('真的删除行么', function(index){
        obj.del();
        layer.close(index);
      });
    } else if(obj.event === 'edit'){
      layer.prompt({
        formType: 2
        ,value: data.email
      }, function(value, index){
        obj.update({
          email: value
        });
        layer.close(index);
      });
    }
  });
  
});