
layui.use(['table','form','laydate'], function(){
  var table = layui.table;
  var laydate = layui.laydate;
  var form  = layui.form;
  var element = layui.element;

	//开始日期
	laydate.render({
		elem: '#beginDate',
	});
	
	laydate.render({
		elem: '#statementDate',
	});
	
  table.render({
	id:'table_list',
    elem: '#table-data',
	url:'/statement/list',
	toolbar: '#toolbar',
	title: '提成数据',
	cols: [[
	  {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
	  {field:'orgName', title:'所属机构',align: 'center',hide:true},
	  {field:'createDate', title:'日期',align: 'center'},
	  {field:'jobNumber', title:'业务员工号',align: 'center',hide:true},
	  {field:'name', title:'业务员姓名', align: 'center'},
	  {field:'percentage', title:'提成金额', align: 'center'},
      {field:'balanceOld', title:'前一天账户余额', align: 'center'},
      {field:'balanceNew', title:'提成后账户余额', align: 'center'},
      {field:'balance', title:'账户余额', align: 'center',hide:true},
    ]],
    limit:30,
    limits:[30,60,200,500,1000],
    parseData: function(res){ 
        return {
          "code": res.retCode, 		//解析接口状态
          "msg": res.data.message, 	//解析提示文本
          "count": res.data.total, 	//解析数据长度
          "data": res.data.list 	//解析数据列表
        }
    },
	page: true
  });
  
  //监听搜索按钮
  form.on('submit(sbwl_search)', function(data){
    table.reload('table_list',{
    	page: {
            curr: 1 //重新从第 1 页开始
         },
         where:data.field
    });
    return false;
  });
  
  // 刷新列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'showStatement':
    	  showStatement("生成提成数据"," #showStatement");
      break;
      case 'getCheckLength':
        var data = checkStatus.data;
        layer.msg('选中了：'+ data.length + ' 个');
      break;
      case 'isAll':
        layer.msg(checkStatus.isAll ? '全选': '未全选');
      break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    if(obj.event === 'sbwl-payment'){
    	
    } else if(obj.event === 'edit'){

    }
  });
  
  // 重新生成业务员提成数据的提示框!
  var statement = '';
  function showStatement(title, doc){
  	$('#statement-form')[0].reset();
  	statement = layer.open({
          type: 1,
          title: title,
          skin: 'layui-layer-molv',
          shadeClose: false,
          area: ['637px', '259px'],
          content: $(doc),
          cancel: function(){
        	 $('#statement-form')[0].reset();
          }
      });
  }
  
  form.on('submit(sbwl_stream_submit)', function(resp){
	  //alert(JSON.stringify(resp.field))
      shenbi.ajax({
          type:'post',
          url:'/statement/generate',
          data:{
              date:resp.field.date,
              jobNumber:resp.field.jobNumber
          },
          sendMsg:'处理中...',
          success:function(resp){
              if(resp.retCode === 0){
                  layer.msg("已处理");
                  layer.close(statement);
              }else{
                  layer.msg(resp.message);
              }
              // ~刷新数据
              table_reload();
          }
      });
	  return false;
  });
  
});