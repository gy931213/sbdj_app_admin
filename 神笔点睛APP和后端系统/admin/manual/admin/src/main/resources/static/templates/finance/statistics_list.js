
layui.use(['table','laydate'], function(){
  var table = layui.table;
  var laydate = layui.laydate;
	
	//开始日期
	laydate.render({
		elem: '#beginDate'
	});
	//结束日期
	laydate.render({
		elem: '#endDate'
	});
  
  table.render({
    elem: '#table-data',
	url:'/test/table/demo1.json',
	toolbar: '#toolbar',
	title: '财务统计数据表',
	cols: [[
      {type: 'checkbox', fixed: 'left'},
	  {field:'id', title:'序号', width:80, fixed: 'left', unresize: true,align: 'center'},
	  {field:'username', title:'时间',align: 'center'},
	  {field:'username', title:'店名', align: 'center'},
	  {field:'username', title:'联系人', align: 'center'},
	  {field:'username', title:'实付价格', align: 'center'},
	  {field:'username', title:'费用', align: 'center'},
	  {field:'username', title:'任务量', align: 'center'},
	  {field:'username', title:'应收金额', align: 'center'},
		{field:'username', title:'实收金额', align: 'center'},
		{field:'username', title:'差额', align: 'center'},
		{field:'username', title:'收款人', align: 'center'},
	  {fixed: 'right', title:'操作', toolbar: '#operation', width:150,align: 'center'}
    ]],
	page: true
  });
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'getCheckData':
        var data = checkStatus.data;
        layer.alert(JSON.stringify(data));
      break;
      case 'getCheckLength':
        var data = checkStatus.data;
        layer.msg('选中了：'+ data.length + ' 个');
      break;
      case 'isAll':
        layer.msg(checkStatus.isAll ? '全选': '未全选');
      break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    //console.log(obj)
    if(obj.event === 'del'){
      layer.confirm('真的删除行么', function(index){
        obj.del();
        layer.close(index);
      });
    } else if(obj.event === 'edit'){
      layer.prompt({
        formType: 2
        ,value: data.email
      }, function(value, index){
        obj.update({
          email: value
        });
        layer.close(index);
      });
    }
  });
  
});