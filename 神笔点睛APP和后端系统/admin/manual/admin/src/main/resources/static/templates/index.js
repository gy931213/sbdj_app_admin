
	// ~获取当前登录页所属的菜单
	shenbi.ajax({
		type:'post',
		url:'/menu',
		success:function(resp){
			if(resp!=""){
				if(resp.retCode==0){
					loadMenu(resp.data.list);
					loadAdminInfo(resp.data);
				}
			}
		}
	});
	
	// 渲染个人信息
	function loadAdminInfo(data){
		if(data.name!=""){
			$("#adminName").html("  "+data.name);
		}
		var $headUrl = "img/profile_small.jpg";
		if(data.headUrl != "" && data.headUrl != null){
			$headUrl = data.headUrl;
		}
		$("#headUrl").attr("src",$headUrl);
	}
	
	function loadMenu(array){
		// 第一级
		var _html1 = '';
		for (var i = 0; i < array.length; i++) {
			if(array[i].pid==-1 && array[i].type==1){
				_html1+='<li>';
				_html1+='	<a>';
				_html1+='		<i class="'+array[i].icon+'"></i>';
				_html1+='		<span class="nav-label">'+array[i].name+'</span>';
				_html1+='		<span class="fa arrow"></span>';
				_html1+='	</a>';
				
				_html1+='<ul class="nav nav-second-level collapse">';
				// 第二级
				for (var j = 0; j < array.length; j++) {
					if(array[j].pid==array[i].id && array[j].type==2){
						_html1+='	<li>';
						_html1+='		<a class="J_menuItem" href="'+array[j].url+'" data-index="'+array[j].id+'">'+array[j].name+'</a>';
						_html1+='	</li>';
					}
				}
				_html1+='</ul>';
				_html1+='</li>';
			}
		}
		
		//$("#side-menu").html(_html1);
		//$("#sbwl_nav_header").after("");
		$("#sbwl_nav_header").after(_html1);
		$('#side-menu').metisMenu(); // ul.nav#side-menu
        //$(window.parent.document).find("#side-menu").metisMenu();
		$.getScript("/js/contabs.min.js");
	}
