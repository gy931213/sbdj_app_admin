
layui.use(['table','form','upload'], function(){
    var table = layui.table;
    var table = layui.table;
    var form  = layui.form;
    var upload = layui.upload;
    var $=layui.jquery;
    // 默认图片显示
    var defaultImg = '/img/jiahao03.jpg';

    table.render({
        elem: '#table-data',
        url:'/info/help/list',
        toolbar: '#toolbar',
        title: '帮助手册数据表',
        id:'table_list',
        cols: [[
            {type:'checkbox',sort:true,align: 'center'},
            {type:'numbers',title:'序号',align: 'center',width:'5%'},
            {field:'orgName', title:'所属组织',align: 'center'},
            {field:'title', title:'标题',align: 'center'},
            {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            /*  {field:'status', title:'状态', align: 'center'},*/
            {fixed: 'right', title:'操作', toolbar: '#operation', width:200,align: 'center'}
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        parseData: function(res){
            return {
                "code": res.retCode, 		//解析接口状态
                "msg": res.data.message, 	//解析提示文本
                "count": res.data.total, 	//解析数据长度
                "data": res.data.list 	//解析数据列表
            }
        },
        page: true
    });

    //监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });
        return false;
    });

    // 刷新列表
    function table_reload(){
        table.reload('table_list');
    }

    //~渲染select选项 （机构）
    kits.select({
        elem:'#sbwl_org_select',
        url:'/sys/organization/select',
        value:'id',
        name:'name',
        form:form
    });

//头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'sbwl_dels':
                var data = checkStatus.data;
                var ids = [];
                $.each(data,function(index,item){
                    ids.push(item.id);
                });

                if(ids.length<=0){
                    layer.msg("至少选择一项进行操作");
                    return false
                }

                layer.msg('确定要批量删除吗？', {
                    btn: ['确定', '取消'],
                    yes: function(index, layero){
                        var data = obj.data;
                        shenbi.ajax({
                            type:'post',
                            url:'/info/help/deletes',
                            data:{ids:ids.join()},
                            sendMsg:'删除中...',
                            success:function(resp){
                                if(resp.retCode === 0){
                                    layer.msg("已删除");
                                }else{
                                    layer.msg("删除失败");
                                }
                                table_reload();
                            }
                        });
                    }
                });

                break;
            case 'sbwl_create':
                //置空
                $("input[name=id]").val("");
                //$(".summernote").code('')
                editor.txt.clear();

                //~渲染select选项 （机构）
                kits.select({
                    elem:'.sbwl-org-select',
                    url:'/sys/organization/select',
                    value:'id',
                    name:'name',
                    form:form
                });
                // 默认图片
                $("#test1").attr("src", defaultImg);
                // 清空封面
                $("#coverImage").val('');
                uploadInst.config.elem.next()[0].value = '';

                // 新增页面
                showEditOrAdd('新增','#sbwl_tmpl','add');
                break;
        };
    });

    //监听行工具事件
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        if(obj.event === 'sbwl-del'){
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    var data = obj.data;
                    shenbi.ajax({
                        type:'post',
                        url:'/info/help/delete',
                        data:{id:data.id},
                        sendMsg:'删除中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已删除");
                            }else{
                                layer.msg("删除失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        } else if(obj.event === 'sbwl-edit'){
            var data = obj.data;
            // 设置表单数据!
            form.val("sbwl_from", {
                "id":data.id,
                "title":data.title,
                "type":data.type,
                "coverImage":data.coverImage
            });

            if ('TXT' === data.type) {
                // 清空封面
                $("#test1").attr("src", defaultImg);
                $("#coverImage").val('');
                // 清空视频资源
                $("#resource").val('');
                $($("#data-video")).remove();
                $("#b").addClass('sbwl-hidden');
            } else if ('IMG' === data.type) {
                if (undefined === data.coverImage || '' === data.coverImage) {
                    $("#test1").attr("src", defaultImg);
                } else {
                    $("#test1").attr("src", data.coverImage);
                }
                $("#resource").val('');
                $($("#data-video")).remove();
                $("#b").removeClass('sbwl-hidden');
            } else if ('VID' === data.type) {
                if (undefined === data.coverImage || '' === data.coverImage) {
                    $("#test1").attr("src", defaultImg);
                } else {
                    $("#test1").attr("src", data.coverImage);
                }
                if('VID' === data.type && data.resource !== ""){
                    $("#resource").val(data.resource);
                }
                $("#b").removeClass('sbwl-hidden');
            }

            // ~渲染select选项 （机构）
            kits.select({
                elem:'.sbwl-org-select',
                url:'/sys/organization/select',
                value:'id',
                name:'name',
                form:form,
                select:data.orgId
            });

            // 富文本赋值
            /*$(".summernote").code(data.content)*/
            editor.txt.html(data.content);

            // 更新页面
            showEditOrAdd('编辑','#sbwl_tmpl','update');
        }else {
            layer.msg("没有相关操作");
        }
    });

    //打开编辑页面或者是新增数据页面
    function showEditOrAdd(title,doc,opr){
        layer_ = layer.open({
            type: 1,
            title: title,
            //maxmin: true,
            skin: 'layui-layer-molv',
            content: $(doc),
            cancel: function(){
                // 置空
                $('#sbwl_from_id')[0].reset();
                // $(".summernote").code('')
                editor.txt.clear();
            }
        });
        // 设置操作类型!
        oprType = opr;
        layer.full(layer_);
    }

    //from表单验证规则
    form.verify({
        orgId: function(value){
            if(value=='0'){
                return "请选择对应组织";
            }
        }
    });

    //from表单监听提交
    form.on('submit(sbwl_submit)', function(data){
        // 获取富文本中的code数据内容
        /*var sHTML = $('.summernote').code();
        data.field.content=sHTML;*/
        // 获取富文本中的code数据内容
        var sHTML = editor.txt.html();
        data.field.content=sHTML;
        if(sHTML.indexOf("data-video") === -1){
            data.field.resource = '';
        }
        // 提交后台数据!
        shenbi.ajax({
            type:'post',
            url:oprType=='add'?'/info/help/save':'/info/help/update',
            data:data.field,
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.close(layer_);
                    if(oprType=="add"){
                        layer.msg("成功");
                    }else{
                        layer.msg("已更新");
                    }
                    // 刷新数据表
                    table_reload();
                    // 重置表单
                    $('#sbwl_from_id')[0].reset();
                    // 重置富文本
                    // $('.summernote').summernote('reset');
                    //清空编辑器内容
                    editor.txt.clear()
                }
            }
        });
        return false
    });

    // 加载富文本框架
    /*	$(".summernote").summernote({
            lang:"zh-CN",
             height: 400,
        });*/

    form.on('select(type)', function(data){
        if ('TXT' === data.value) {
            // 清空封面
            $("#test1").attr("src", defaultImg);
            $("#coverImage").val('');
            // 清空视频资源
            $("#resource").val('');
            $($("#data-video")).remove();
            $("#b").addClass('sbwl-hidden');
        } else if ('IMG' === data.value) {
            $("#resource").val('');
            $($("#data-video")).remove();
            $("#b").removeClass('sbwl-hidden');
        } else if ('VID' === data.value) {
            $("#b").removeClass('sbwl-hidden');
        }
    });

    //普通图片上传
    var uploadInst = upload.render({
        elem: '#test1'
        ,url: '/base/upload/'
        ,before: function(obj){
            //预读本地文件示例，不支持ie8
            obj.preview(function(index, file, result){
                $('#test1').attr('src', result); //图片链接（base64）
            });
        }
        ,done: function(res){
            //如果上传失败
            if(res.code > 0){
                return layer.msg('上传失败');
            }
            //上传成功
            $("#coverImage").val(res.message);
            uploadInst.config.elem.next()[0].value = '';
        }
        ,error: function(){
            //演示失败状态，并实现重传
            var demoText = $('#demoText');
            demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
            demoText.find('.demo-reload').on('click', function(){
                uploadInst.upload();
            });
        }
    });
    uploadInst.config.elem.next()[0].value = '';

    var $showLoad = null;
    var E = window.wangEditor
    editor = new E('#editor')
    editor.customConfig.menus = [
        'head', // 标题
        'bold', // 粗体
        'fontSize', // 字号
        'fontName', // 字体
        'italic', // 斜体
        'underline', // 下划线
        'strikeThrough', // 删除线
        'foreColor', // 文字颜色
        'backColor', // 背景颜色
        'link', // 插入链接
        'list', // 列表
        'justify', // 对齐方式
        'quote', // 引用
        'emoticon', // 表情
        'image', // 插入图片
        'table', // 表格
        'video', // 插入视频
        'code', // 插入代码
        'undo', // 撤销
        'redo' // 重复
    ];
    // 隐藏“网络图片”tab
    editor.customConfig.showLinkImg = false
    // 关闭粘贴内容中的样式
    editor.customConfig.pasteFilterStyle = false
    // 忽略粘贴内容中的图片
    editor.customConfig.pasteIgnoreImg = false
    // 将图片大小限制为 3M
    editor.customConfig.uploadImgMaxSize = 3 * 1024 * 1024
    // 限制一次最多上传 1 张图片
    editor.customConfig.uploadImgMaxLength = 1
    // 将 timeout 时间改为 3s
    editor.customConfig.uploadImgTimeout = 6000;
    editor.customConfig.zIndex = 100
    editor.customConfig.uploadImgServer = '/base/editor/upload'
    editor.customConfig.uploadFileName = 'files'
    editor.customConfig.uploadImgHooks = {
        before : function(xhr, editor, files) {
            $showLoad = shenbi.showLoad("上传中...");
        },
        success : function(xhr, editor, result) {
            layer.msg("图片上传成功");
        },
        fail : function(xhr, editor, result) {
            layer.msg("图片插入错误");
        },
        error : function(xhr, editor) {
            layer.msg("图片上传出错");
        },
        timeout : function(xhr, editor) {
            layer.msg("图片上传超时");
        },
        customInsert : function(insertImg, result, editor) {
            shenbi.closeLoad($showLoad);
            var url = result.data;//获取后台返回的url
            // 自带的图片插入函数
            insertImg(url);
        }
    };
    editor.create();

    var $showLoad = null;
    var $showLoad = upload.render({
        elem: '#uploaderVideo',
        url: '/base/upload/video',
        accept: 'video',
        field:'uploadVideo',
        size:52000,
        before:function(){
            $showLoad = shenbi.showLoad("上传中...");
        },
        done: function(res){
            shenbi.closeLoad($showLoad);
            layer.msg("视频上传成功！");
            if(res.retCode === 0 && res.message!=""){
                var html = '<video id="data-video" src="'+res.message+'" width="320" height="240" controls="controls"></video> <p><br></p>';
                editor.txt.append(html);
                $("#resource").val(res.message);
            }
        },error: function(res){
            console.log(res)
            layer.msg("视频上传失败！");
        }
    });

});