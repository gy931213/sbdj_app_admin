// login.js 登录页面
layui.use([ 'form' ], function() {
	var form = layui.form;
	// 添加一个回车按钮
	document.onkeydown = function(e) { // 回车提交表单
		var theEvent = window.event || e;
		var code = theEvent.keyCode || theEvent.which;
		if (code == 13) {
			$(".logBut").click(); // #chk_match 是你 提交按钮的ID
		}
	}

	// from表单验证规则
	form.verify({
		mobile : function(value) {
			/*
			 * if(value.length < 5){ return ''; }
			 */
		},
		pwd : [ /(.+){6,12}$/, '密码必须6到12位' ],
	});

	// from表单监听提交
	form.on('submit(sbwl_login_submit)', function(data) {
		shenbi.ajax({
			method : 'POST',
			url : 'ajaxLogin',
			data : data.field,
			sendMsg : '登录中...',
			success : function(resp) {
				if (resp.retCode == 0) {
					layer.msg("登录成功，正在跳转", {
						time : 500
					}, function() {
						window.location.href = "/"
					});
				} else {
					layer.msg(resp.message == "" ? "登录失败!" : resp.message, {
						time : 1000
					});
				}
			}
		});
		return false;
	});
});
