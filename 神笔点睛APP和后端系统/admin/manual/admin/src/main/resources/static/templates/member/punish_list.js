
// 
layui.use('table', function(){
  var table = layui.table;
  
  table.render({
    elem: '#table-data',
	url:'/test/table/demo1.json',
	toolbar: '#toolbar',
	title: '处罚数据表',
	cols: [[
      {type: 'checkbox', fixed: 'left'},
	  {field:'id', title:'序号', width:80, fixed: 'left', unresize: true,align: 'center'},
	  {field:'username', title:'业务员名称',align: 'center'},
	  {field:'username', title:'违规时间', align: 'center'},
	  {field:'username', title:'违规订单号',align: 'center'},
	  {field:'username', title:'违规说明',align: 'center'},
	  {field:'username', title:'罚款金额', align: 'center'},
	  {field:'username', title:'操作管理员', align: 'center'},
    ]],
	page: true
  });
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'getCheckData':
        var data = checkStatus.data;
        layer.alert(JSON.stringify(data));
      break;
      case 'getCheckLength':
        var data = checkStatus.data;
        layer.msg('选中了：'+ data.length + ' 个');
      break;
      case 'isAll':
        layer.msg(checkStatus.isAll ? '全选': '未全选');
      break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    //console.log(obj)
    if(obj.event === 'del'){
      layer.confirm('真的删除行么', function(index){
        obj.del();
        layer.close(index);
      });
    } else if(obj.event === 'edit'){
      layer.prompt({
        formType: 2
        ,value: data.email
      }, function(value, index){
        obj.update({
          email: value
        });
        layer.close(index);
      });
    }
  });
  
});