
// 
layui.use(['table','form','upload'], function(){
  var table = layui.table;
  var form  = layui.form;
  
  // ~操作类型[add|update],默认为add
  var oprType = 'add';
  var layer_ = ''; 
  
  // ~渲染业务员列表数据~
  table.render({
	id:'table_list',
	method:'post',
    elem: '#table-data',
	url:'/salesman/level/list',
	toolbar: '#toolbar',
	title: '业务员等级配置',
	cols: [[
	  {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
	  {type:'numbers',title:'序号',width:'5%',fixed: 'left'},	  
	  {field:'name', title:'等级名称'},
	  {field:'code', title:'等级代码',hide:true},
	  {field:'sort', title:'排序号',hide:true},
	  {field:'createTime', title:'创建时间',templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
	  }},
	  {fixed: 'right', title:'操作', width:200,align: 'center',templet:function(res){
		  var _html = '';
		  _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';
		  _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-del" style="background-color: #d9534f;"><i class="layui-icon">&#xe640;</i> 删除</a>';
		  return _html;
	  }}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return shenbi.returnParse(res);
    },
	page: true
  });
  
  // ~刷新数据列表
  function table_reload(){
	  table.reload('table_list');
  }
  
	// ~监听搜索按钮
   form.on('submit(sbwl_search)', function(data){
     table.reload('table_list',{
    	 page: {
             curr: 1 //重新从第 1 页开始
          },
          where:data.field
     });
     return false;
   });
  
   // ~监听输入事件
   $('input[name="name"]').on('input',function(i,item){
	   var val = $(this).val();
	   var char =pinyin.getCamelChars(val);
	   $('input[name="code"]').val("LEVEL_"+char);
   });
   
  // ~工具栏事件~
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      // ~新增等级
      case 'sbwl-add-salesman-level':
    	// ~新增页面
		showEditOrAdd('新增业务员等级','#sbwl_tmpl','add');
      break;
      // ~删除等级
      case 'sbwl-del-salesman-level':
    	    var data = checkStatus.data;
            layer.msg('选中了：'+ data.length + ' 个');
      break;
    };
  });
  
  // ~监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    if(obj.event === 'sbwl-del'){
    	 // ~删除操作~
    	layer.msg('确定要删除吗？', {
    	    btn: ['确定', '取消'],
    	    yes: function(index, layero){
    	    	shenbi.ajax({
    				type:'post',
    				url:'/salesman/level/delete',
    				data:{id:data.id},
    				sendMsg:'删除中...',
    				success:function(resp){
    					if(resp.retCode === 0){
    						layer.msg("已删除"); 
    					}else{
    						layer.msg("删除失败");
    					}
    					table_reload();
    				}
    			});
    	    }
    	});
    }else if(obj.event === 'sbwl-edit'){
    	// 编辑数据
    	
    	// ~表单赋值
		 form.val("sbwl_from", {
			  "id":data.id,
			  "code":data.code,
			  "name":data.name,
			  "sort":data.sort
		  });
    	
    	// ~编辑页面
		showEditOrAdd('编辑业务员等级','#sbwl_tmpl','update');
    }
  });
  
	// ~打开编辑页面或者是新增数据页面
	function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
	    type: 1,
		    title: title,
		    skin: 'layui-layer-molv',
			//shadeClose: false,
		    //maxmin: true,
			area: ['650px', '270px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		oprType = opr;
	}
	
	 //from表单监听提交
	 form.on('submit(sbwl_submit)', function(data){
		  // 提交后台数据!
		  shenbi.ajax({
			type:'post',
			url:oprType=='add'?'/salesman/level/save':'/salesman/level/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.close(layer_);
					if(oprType=="add"){
						layer.msg("成功"); 
					}else{
						layer.msg("已更新"); 
					}
					// 刷新数据表
					table_reload();
					// 重置表单
					$('#sbwl_from_id')[0].reset();
				}else{
					// ~异常信息
					layer.msg(resp.message);
				}
			}
		 });
		return false
	 });
});