
// 
layui.use(['table','form','upload','laydate'], function(){
    var upload = layui.upload;
    var table = layui.table;
    var form  = layui.form;
    var laydate = layui.laydate;

    // ~操作类型[add|update],默认为add
    var oprType = 'add';
    var layer_ = '';
    // ~证件信息
    var zjxx = [];

    // 设置表头快链的样式
    var key = ['salesmanName','mobile','name'];

    // ~渲染业务员列表数据~
    table.render({
        id:'table_list',
        method:'post',
        elem: '#table-data',
        url:'/salesman/list',
        toolbar: '#toolbar',
        title: '业务员列表',
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
           /* {field:'orgName', title:'所属机构', align: 'center',hide:true},*/
            {field:'salesmanName', title:'邀请人', align: 'center',event:'search-salesman-name',templet:function(rse){
                    return kits.isNotEmpty(rse.salesmanName);
                }},
            {field:'jobNumber', title:'工号', align: 'center',hide:true},
            {field:'mobile', title:'登录账号', align: 'center',event:'search-mobile'},
            {field:'name', title:'姓名', align: 'center',event:'search-name'},
            {field:'platformName', title:'所属平台', align: 'center'},
            {field:'onlineid', title:'平台ID',align: 'center',event:'search-onlineid'},
            /*{field:'mobile', title:'电话', align: 'center'},*/
            {field:'provinceName', title:'所在地区', align: 'center',hide:true,templet:function(rse){
                    return  rse.provinceName+" "+rse.cityName+" "+rse.countyName;
                }},
            {field:'taskTotal', title:'完成单数', align: 'center'},
            {field:'provinceName', title:'省', align: 'center',hide:true},
            {field:'cityName', title:'市', align: 'center',hide:true},
            {field:'countyName', title:'区', align: 'center',hide:true},
            {field:'cardNum', title:'身份证号', align: 'center'},
            {field:'bankNum', title:'银行卡号', align: 'center'},
            {field:'firstMission', title:'首次任务',width:90,templet:function(rse){
                    if (rse.firstMission === 0) {
                        return  '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #60c560;">是</a>';
                    } else {
                        return  '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #d9534f;">否</a>';
                    }
                }},
            {field:'wechart', title:'微信号', align: 'center',hide:true},
            {field:'qq', title:'QQ号', align: 'center',hide:true},
            {field:'remainderXFD', title:'可接现付单',width:100,hide:true, align: 'center'},
            {field:'remainderGRD', title:'可接隔日单',width:100,hide:true, align: 'center'},
            /*{field:'balance', title:'账号余额', align: 'center',hide:true},*/
            {field:'isSearch', title:'查小号?', align: 'center',hide:true,templet:function (rse) {
                    if (rse.isSearch === 0) {
                        return  '<a class="layui-btn layui-btn-sm" lay-event="search" style="width:60px;background-color: #d9534f;">否</a>';
                    } else if (rse.isSearch === 1) {
                        return  '<a class="layui-btn layui-btn-sm" lay-event="search" style="width:60px;background-color: #60c560;">是</a>';
                    }
                }},
            /*{field:'salesmanLevelName', title:'所属等级', align: 'center',hide:true},*/
            /*{field:'totalCredit', title:'封顶额度', align: 'center',hide:true},*/
            /*{field:'surplusCredit', title:'可用额度', align: 'center',hide:true},*/
            /*{field:'bank', title:'所属银行', align: 'center',hide:true},*/
            {field:'oprName', title:'操作人',align:'center',event:'search-oprName',hide:true},
            {field:'lastLoginTime', title:'登录时间', align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.lastLoginTime);
                }},
            {field:'loginTotal', title:'登录次数', align: 'center',hide:true},
            {field:'createTime', title:'创建时间', align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {field:'disableTime', title:'禁用时间', align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.disableTime);
                }},
           /* {field:'status', title:'提成', align: 'center',hide:true,templet:function(rse){
                    return '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="view-royalty">查看提成</a>';
                }},*/
            {field:'status', title:'业务状态',width:90,align: 'center',templet:function(rse){
                    if(rse.status==="E"){
                        return '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="sbwl-prohibit">'+kits.returnStatus(rse.status)+'</a>';
                    }else if(rse.status==="P"){
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled" lay-event="sbwl-enable">'+kits.returnStatus(rse.status)+'</a>';
                    }else if(rse.status==="EP"){
                        return '<a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="sbwl-unreviewed">'+kits.returnStatus(rse.status)+'</a>';
                    }else{
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled">'+kits.returnStatus(rse.status)+'</a>';
                    }
                }},
            {field:'pStatus', title:'号主状态',width:90,align: 'center',templet:function(rse){
                    if(rse.pStatus==="E"){
                        return '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="sn-prohibit">'+kits.returnStatus(rse.pStatus)+'</a>';
                    }else if(rse.pStatus==="P"){
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled" lay-event="sn-enable">'+kits.returnStatus(rse.pStatus)+'</a>';
                    }else{
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled">'+kits.returnStatus(rse.pStatus)+'</a>';
                    }
                }},
            {fixed: 'right', title:'操作', width:261,align: 'center',templet:function(res){
                    var _html = '';
                    /* if(res.status=="E"){
                         _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-prohibit"><i class="layui-icon">&#x1006;</i> 禁用</a>';
                     }else if(res.status=="P"){
                         _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-enable"><i class="layui-icon">&#xe605;</i> 启用</a>';
                     }*/
                    //_html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                    /* _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="verification"><i class="layui-icon">&#xe615;</i> 验证</a>';*/
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-checking"><i class="layui-icon">&#xe615;</i> 认证</a>';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-relation-list"><i class="layui-icon">&#xe615;</i> 关系</a>';
                    /*_html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-mail-list"><i class="layui-icon">&#xe615;</i> 通讯录</a>';*/
                    /*_html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';*/
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbdj-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                    /*_html+='<a class="layui-btn layui-btn-sm layui-btn-warm" lay-event="sbwl-prize"><i class="layui-icon">&#xe65e;</i> 奖罚</a>';*/
                    return _html;
                }}
        ]],
        limit:10,
        limits:[10,20,30,50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true
    });

    // 表头样式
    tableThStyle(key);

    // ~刷新数据列表
    function table_reload(){
        table.reload('table_list');
        // 表头样式
        tableThStyle(key);
    }

    // ~渲染select选项 （机构）
    kits.select({
        elem:'#sbwl_org_select',
        url:'/sys/organization/select',
        value:'id',
        name:'name',
        form:form
    });

    //开始日期
    laydate.render({
        elem: '#beginDate',
        type: 'datetime'
    });
    //结束日期
    laydate.render({
        elem: '#endDate',
        type: 'datetime'
    });

    //渲染select选项 (平台)
    kits.select({
        elem:'.sbwl_selece',
        url:'/config/platform/select',
        value:'id',
        name:'name',
        form:form
    });

    // ~渲染select选项（业务员等级）
    kits.select({
        elem:'#sbwl_level_select',
        url:'/salesman/level/select',
        value:'id',
        name:'name',
        form:form,
        option:'<option value="0">请选择等级</option>',
    });

    // ~监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });

        // 表头样式
        tableThStyle(key);
        return false;
    });

    // ~工具栏事件~
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            // ~新增业务员
            case 'sbwl-add-salesman':

                // ~渲染select选项（机构）
                kits.select({
                    elem:'.sbwl_org_select',
                    url:'/sys/organization/select',
                    value:'id',
                    name:'name',
                    form:form
                });

                // ~渲染select选项（省）
                kits.select({
                    elem:'.sbwl_prov_select',
                    url:'/sys/area/select',
                    data:{parentId:-1},
                    value:'areaId',
                    name:'areaName',
                    form:form,
                    option:'<option value="0">请选择省</option>'
                });

                // ~渲染select选项（业务员等级）
                kits.select({
                    elem:'.sbwl_level_select',
                    url:'/salesman/level/select',
                    value:'id',
                    name:'name',
                    form:form,
                    option:'<option value="0">请选择等级</option>'
                });

                // 所属银行
                kits.select({
                    elem:'.sbwl_bank_select',
                    url:'/config/bank/select',
                    value:'id',
                    name:'name',
                    form:form
                });

                // ~恢复密码的限制
                $("input[name='pwd']").attr("lay-verify","required|pwd");
                // 重置id
                $("input[name='id']").val("");


                // ~新增页面
                showEditOrAdd('新增业务员','#sbwl_tmpl','add');
                break;
            case 'sbdj-add-tb':
                // 重置表单
                $('#sbdj_from_id')[0].reset();
                // ~渲染select选项（省）
                kits.select({
                    elem:'.sbwl_prov_select',
                    url:'/sys/area/select',
                    data:{parentId:-1},
                    value:'areaId',
                    name:'areaName',
                    form:form,
                    option:'<option value="0">请选择省</option>'
                });

                // 重置id
                $("input[name='id']").val("");
                // ~新增页面
                showEditOrAdd('新增淘宝刷手','#sbdj_tmpl','add');
                break;
            case 'sbdj-search-tb':
                // 重置表单
                $('#sbdj_search_tb_form')[0].reset();
                showSearch('查询淘宝刷手', '#sbdj_search_tb_tmpl', 'search');
                break;
            // ~删除业务员
            case 'sbwl-del-salesman':
                var data = checkStatus.data;
                layer.msg('选中了：'+ data.length + ' 个');
                break;
        };
    });

    // ~监听select选中的事件（省）
    form.on('select(sbwl_prov_select)', function(data){
        // ~渲染select选项（市）
        kits.select({
            elem:'.sbwl_city_select',
            url:'/sys/area/select',
            data:{parentId:data.value},
            value:'areaId',
            name:'areaName',
            form:form,
            option:'<option value="0">请选择市</option>'
        });

        // ~重置县区的数据选项~
        $(".sbwl_county_select").html('<option value="0">请选择县/区</option>');
    });

    // ~监听select选中的事件（市）
    form.on('select(sbwl_city_select)', function(data){
        // ~渲染select选项（县/区）
        kits.select({
            elem:'.sbwl_county_select',
            url:'/sys/area/select',
            data:{parentId:data.value},
            value:'areaId',
            name:'areaName',
            form:form,
            option:'<option value="0">请选择县/区</option>'
        });
    });

    // ~from表单数据验证
    form.verify({
        orgId: function(value){
            if(value==='0'){
                return "请选择对应组织";
            }
        },
        provinceId:function(value){
            if(value==='0'){
                return "请选择省";
            }
        },
        cityId:function(value){
            if(value==='0'){
                return "请选择市";
            }
        },
        countyId:function(value){
            if(value==='0'){
                return "请选择（县\\区）";
            }
        },
        salesmanLevelId:function(value){
            if(value === '0'){
                return "请选择对应等级";
            }
        },
        bankNum:function (value) {
            // 银行卡正则
            var reg = /^[1-9]\d{3}((\s\d{4}){3}\s\d{3}|(\s\d{4}){3})$/;
            if (!reg.test(value)) {
                return "请输入正确的银行卡号";
            }
        },
        newMobile:function (value) {
            // 提示警告
            if (value === '1') {
                layer.alert('注意该业务是一级业务员!', {icon: 7});
            }
            return '请输入推荐人';
        },
        salesmanMobile:function (value) {
            // 提示警告
            if (value === '1') {
                layer.alert('注意该业务是一级业务员!', {icon: 7});
            }
            return '请输入推荐人';
        },
        onlineid: function (value) {
            if (value === '') {
                return '请输入淘宝ID';
            }
        }
    });


    // ~监听行工具事件
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        //console.log(obj)
        if(obj.event === 'sbwl-prohibit'){
            // ~禁用操作~
            layer.msg('确定要禁用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/salesman/status',
                        data:{
                            id:data.id,
                            status:"P"
                        },sendMsg:'禁用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已禁用");
                            }else{
                                layer.msg("禁用失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-enable'){
            // ~启用操作~
            layer.msg('确定要启用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/salesman/status',
                        data:{
                            id:data.id,
                            status:"E"
                        },sendMsg:'启用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已启用");
                            }else{
                                layer.msg("启用失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        } else if(obj.event === 'sn-prohibit'){
            // ~禁用操作~
            layer.msg('确定要禁用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/salesman/number/status',
                        data:{
                            id:data.salesmanNumberId,
                            status:"P"
                        },sendMsg:'禁用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已禁用");
                            }else{
                                layer.msg("禁用失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sn-enable'){
            // ~启用操作~
            layer.msg('确定要启用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/salesman/number/status',
                        data:{
                            id:data.salesmanNumberId,
                            status:"E"
                        },sendMsg:'启用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已启用");
                            }else{
                                layer.msg("启用失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-unreviewed'){
            // ~表单赋值
            form.val("sbwl_unreviewed_from", {
                "id":data.id
            });
            showSalesmanUnreviewed("更新业务员", "#sbwl_unreviewed_tmpl")
        }else if(obj.event === 'search-oprName'){
            // 模拟鼠标双击
            if(!dblclick()){
                return false;
            }
            // 审核人
            table.reload('table_list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'oprName':data.oprName}
            });
            $("input[name='oprName']").val(data.oprName);
        }else if(obj.event === 'search'){
            var isSearch = 0;
            if (data.isSearch === 1) {
                isSearch = 0;
            } else {
                isSearch = 1;
            }
            // ~操作~
            layer.msg('安装过查小号吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/salesman/search',
                        data:{
                            id:data.id,
                            isSearch:isSearch
                        },sendMsg:'标记中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已标记");
                            }else{
                                layer.msg("标记失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-checking'){
            // ~表单赋值
            form.val("sbwl_sal_from", {
                "id":data.id,
                "idCard":data.cardNum,
                "bankNum":data.bankNum,
                "mobile":data.mobile,
                "name":data.name,
                "idMobileName":data.idMobileName,
                "idMobileNameBank":data.idMobileNameBank,
                "lengthTime":data.mobileOnline,
                "updateTime":kits.dateFtt("yyyy-MM-dd hh:mm:ss",data.updateTime)
            });

            // ~查看证件照片
            // ~获取业务员相关的证件图片~
            shenbi.ajax({
                type:'post',
                url:'/salesman/src',
                data:{id:data.id},
                success:function(resp){
                    if(resp.retCode==0){
                        var data = resp.data;
                        var _html = '</br>';
                        for (var i = 0; i < data.length; i++) {
                            _html+='<div class="layui-form-item">';
                            _html+='	<div class="layui-input-block">';
                            _html+='		<img id="sbwl-img" alt="" src="'+data[i].link+'" style="width: 400px">';
                            _html+='		<div class="scr"><b>'+data[i].typeName+'</b></div>';
                            _html+='	</div>';
                            _html+='</div>';
                        }
                        // ~渲染数据~
                        $("#sbwl-src-list").html(_html);
                    }
                }
            });

            // ~渲染数据~
            showSalesmanSrc('<i class="layui-icon">&#xe64a;</i> 证照图片','#sbwl_src_tmpl');
        }else if(obj.event === 'sbwl-prize') {
            // ~表单赋值
            form.val("sbwl_prize_from", {
                "salesmanId":data.id,
                "status": data.status,
                "orgId": data.orgId
            });
            // ~渲染数据~
            showSalesmanPrize('<i class="layui-icon">&#xe65e;</i> 奖罚','#sbwl_prize_tmpl')
        }else if(obj.event === 'sbwl-edit'){

            // ~渲染select选项（机构）
            kits.select({
                elem:'.sbwl_org_select',
                url:'/sys/organization/select',
                value:'id',
                name:'name',
                form:form,
                select:data.orgId
            });

            var bankId = data.bankId === undefined ? 0 : data.bankId;

            // 所属银行
            kits.select({
                elem:'.sbwl_bank_select',
                url:'/config/bank/select',
                value:"id",
                name:'name',
                select: bankId,
                form:form
            });

            // ~表单赋值
            form.val("sbwl_from", {
                "id":data.id,
                "name":data.name,
                "mobile":data.mobile,
                "pwd":data.pwd,
                "newMobile":data.salesmanMobile,
                "wechart":data.wechart,
                "qq":data.qq,
                "bankNum":data.bankNum,
                "bankName":data.bankName,
                "bankad":data.bankad,
                "cardNum":data.cardNum,
                "platformId":data.platformId,
            });

            // ~渲染select选项（省）
            kits.select({
                elem:'.sbwl_prov_select',
                url:'/sys/area/select',
                data:{parentId:-1},
                value:'areaId',
                name:'areaName',
                form:form,
                option:'<option value="0">请选择省</option>',
                select:data.provinceId
            });

            // ~渲染select选项（市）
            kits.select({
                elem:'.sbwl_city_select',
                url:'/sys/area/select',
                data:{parentId:data.provinceId},
                value:'areaId',
                name:'areaName',
                form:form,
                option:'<option value="0">请选择市</option>',
                select:data.cityId
            });

            // ~渲染select选项（县/区）
            kits.select({
                elem:'.sbwl_county_select',
                url:'/sys/area/select',
                data:{parentId:data.cityId},
                value:'areaId',
                name:'areaName',
                form:form,
                option:'<option value="0">请选择县/区</option>',
                select:data.countyId
            });

            // ~渲染select选项（业务员等级）
            kits.select({
                elem:'.sbwl_level_select',
                url:'/salesman/level/select',
                value:'id',
                name:'name',
                form:form,
                option:'<option value="0">请选择等级</option>',
                select:data.salesmanLevelId
            });

            // ~去除密码的限制
            $("input[name='pwd']").attr("lay-verify","");

            // ~禁用数据
            //$("input[name='newMobile']").attr("disabled",true).addClass("input-check");

            // ~编辑数据
            showEditOrAdd('编辑业务员','#sbwl_tmpl','update');

        }else if(obj.event === 'sbdj-edit'){
            // 重置表单
            $('#sbdj_from_id')[0].reset();
            // ~表单赋值
            form.val("sbdj_from", {
                "id":data.id,
                "name":data.name,
                "mobile":data.mobile,
                "salesmanMobile":data.salesmanMobile,
                "wechart":data.wechart,
                "qq":data.qq,
                "bankNum":data.bankNum,
                "bankName":data.bankName,
                "bankad":data.bankad,
                "cardNum":data.cardNum,
                "onlineid":data.onlineid,
                "platformId":data.platformId,
            });

            // ~渲染select选项（省）
            kits.select({
                elem:'.sbwl_prov_select',
                url:'/sys/area/select',
                data:{parentId:-1},
                value:'areaId',
                name:'areaName',
                form:form,
                option:'<option value="0">请选择省</option>',
                select:data.provinceId
            });

            // ~渲染select选项（市）
            kits.select({
                elem:'.sbwl_city_select',
                url:'/sys/area/select',
                data:{parentId:data.provinceId},
                value:'areaId',
                name:'areaName',
                form:form,
                option:'<option value="0">请选择市</option>',
                select:data.cityId
            });

            // ~渲染select选项（县/区）
            kits.select({
                elem:'.sbwl_county_select',
                url:'/sys/area/select',
                data:{parentId:data.cityId},
                value:'areaId',
                name:'areaName',
                form:form,
                option:'<option value="0">请选择县/区</option>',
                select:data.countyId
            });

            // ~编辑数据
            showEditOrAdd('编辑淘宝刷手','#sbdj_tmpl','update');

        }else if(obj.event === 'sbwl-mail-list'){
            // ~查看通讯录操作~
            if(data=="" || data.id=="") return false;
            console.log(data)
            tableSalesmanPhones(data.id);
            showSalesmanPhones('<i class="layui-icon">&#xe613;</i> 通讯录','#sbwl_phones_tmpl');

        }else if(obj.event === 'sbwl-relation-list'){
            // ~查看关系操作~
            if(data==="" || data.id==="") return false;
            tableSalesmanOnline(data.id);
            showSalesmanOnline('<i class="layui-icon">&#xe613;</i> 关系表','#sbwl_online_tmpl');
        }else if(obj.event === 'search-salesman-name'){
            if(!dblclick()){
                return false;
            }
            // ~邀请人
            $("input[name='salesmanName']").val(data.salesmanName);
            table.reload('table_list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'salesmanName':data.salesmanName}
            });
        }else if(obj.event === 'search-mobile'){
            if(!dblclick()){
                return false;
            }
            // ~登录账号
            $("input[name='mobile']").val(data.mobile);
            table.reload('table_list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'mobile':data.mobile}
            });
        }else if(obj.event === 'search-name'){
            if(!dblclick()){
                return false;
            }
            // ~姓名
            $("input[name='name']").val(data.name);
            table.reload('table_list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'name':data.name}
            });
        }else if(obj.event === 'view-royalty'){
            // 单个业务员的全部提成数据!
            table.render({
                id:'salesman-statement-list',
                elem: '#salesman-statement',
                url:'/statement/bysaleid?salesmanId='+data.id,
                //toolbar: '#toolbar',
                title: '提成数据',
                cols: [[
                    {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
                    {field:'date', title:'日期', align: 'center'},
                    {field:'count', title:'金额', align: 'center'},
                    {field:'name', title:'姓名', align: 'center'}
                ]],
                limit:30,
                limits:[30,60,200,500,1000],
                parseData: function(res){
                    return {
                        "code": res.retCode, 		//解析接口状态
                        "msg": res.data.message, 	//解析提示文本
                        "count": res.data.total, 	//解析数据长度
                        "data": res.data.list 	//解析数据列表
                    }
                },
                page: true
            });
            viewRoyaltySalesman("业务员提成数据","#viewRoyaltyTmpl");
        }

        // 表头样式
        tableThStyle(key);
    });

    // ~监听行工具事件
    table.on('tool(table_online_filter)', function(obj){
        var data = obj.data;
        //console.log(obj)
        if(obj.event === 'sbwl-prohibit'){
            // ~禁用操作~
            layer.msg('确定要禁用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/salesman/status',
                        data:{
                            id:data.id,
                            status:"P"
                        },sendMsg:'禁用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已禁用");
                            }else{
                                layer.msg("禁用失败");
                            }
                            table.reload('table_online_list');
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-enable'){
            // ~启用操作~
            layer.msg('确定要启用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/salesman/status',
                        data:{
                            id:data.id,
                            status:"E"
                        },sendMsg:'启用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已启用");
                            }else{
                                layer.msg("启用失败");
                            }
                            table.reload('table_online_list');
                        }
                    });
                }
            });
        }
    });

    //from验证业务员信息表单监听提交
    form.on('submit(sbwl_verification_submit)', function(data){
        var  item = data.field;
        if(null == item || item == ""){
            layer.msg("参数缺失验证失败");
            return false;
        }

        /*if(item.idCard == "" || item.idCard == null){
            layer.msg("身份证号不能为空");
            return false;
        }*/

        if(item.mobile === "" || item.mobile === null){
            layer.msg("电话号码不能为空");
            return false;
        }

        shenbi.ajax({
            type:'post',
            url:'/salesman/mobile/online',
            data:{
                id: item.id,
                mobile: item.mobile
            },
            sendMsg:'验证中...',
            success:function(resp){
                // ~表单赋值
                form.val("sbwl_sal_from", {
                    "lengthTime":resp.message
                });
            }
        });

        return false
    })

    //from验证业务员奖罚表单监听提交
    form.on('submit(sbwl_prize_submit)', function(data){
        var  item = data.field;
        if(null == item || item == ""){
            layer.msg("参数缺失验证失败");
            return false;
        }

        if(item.type == "" || item.type == null){
            layer.msg("请选择类型");
            return false;
        }

        if(item.money == "" || item.money == null){
            layer.msg("金额不能为空");
            return false;
        }

        if(item.reason == "" || item.reason == null){
            layer.msg("原因不能为空");
            return false;
        }

        shenbi.ajax({
            type:'post',
            url:'/salesman/prize',
            data:data.field,
            sendMsg:'提交中...',
            success:function(resp){
                layer.msg(resp.message);
                if(resp.retCode === 0) {
                    // 刷新数据表
                    table_reload();
                    layer.close(layer_prize);
                }
            }
        });
        return false
    });

    var layer_unreviewed;
    // ~查看业务员的奖罚弹出层~
    function showSalesmanUnreviewed(title, doc){
        $('#sbwl_unreviewed_id')[0].reset();
        layer_unreviewed = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '370px'],
            content: $(doc),
            cancel: function(){
                $('#sbwl_unreviewed_id')[0].reset();
            }
        });
    }

    //from验证业务员奖罚表单监听提交
    form.on('submit(sbwl_unreviewed_submit)', function(data){
        var  item = data.field;
        if(null === item || item === ""){
            layer.msg("参数缺失验证失败");
            return false;
        }

        if(item.endStatus === null || item.endStatus === ""){
            layer.msg("请选择类型");
            return false;
        }
        // ~启用操作~
        shenbi.ajax({
            type:'post',
            url:'/salesman/review',
            data:{
                id:item.id,
                startStatus:item.startStatus,
                endStatus:item.endStatus
            },sendMsg:'启用中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.msg("已启用");
                }else{
                    layer.msg("启用失败");
                }
                table_reload();
                layer.close(layer_unreviewed);
            }
        });
        return false
    });


    // ~打开编辑页面或者是新增数据页面
    function showSearch(title,doc,opr){
        layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            maxmin: true,
            area: ['1050px', '730px'],
            content: $(doc),
            cancel: function(){
                $('#sbdj_search_tb_form')[0].reset();
            }
        });
        // 设置操作类型!
        oprType = opr;
        layer.full(layer_);
    }

    $("#search-tb").click(function () {
        $('#sbdj_search_tb_form')[0].reset();
        $("#renZheng").html(' ');
        var wangwang = $("#wangwang").val();
        if (wangwang === '') {
            layer.open({
                title: '提示'
                ,content: '淘宝号不能为空'
            });
            return;
        }
        shenbi.ajax({
            type:'post',
            url:'/salesman/query',
            data:{
                wangwang: wangwang
            },sendMsg:'查询中...',
            success:function(resp){
                var data = resp.data.data;
                if (data !== null && resp.data.code === "0") {
                    // ~表单赋值
                    form.val("sbdj_search_tb_form", {
                        "buyerGoodNum":data.buyerGoodNum,
                        "gender":data.gender,
                        "weekCount":data.weekCount,
                        "countBefore":data.countBefore,
                        "jiangNum":data.jiangNum,
                        "fox":data.fox,
                        "renZheng":data.renZheng,
                        "yunBlack":data.yunBlack,
                        "wwcreatedStr":data.wwcreatedStr,
                        "sentRate":data.sentRate,
                        "weekCreditAverage":data.weekCreditAverage,
                        "sellerTotalNum":data.sellerTotalNum,
                        "buyHistory":JSON.stringify(data.purchaseRecords)
                    });
                    $("#renZheng").html(data.renZheng)
                } else {
                    //layer.msg("信息查询失败");
                    if (resp.code === -1) {
                        layer.open({
                            title: '提示'
                            ,content: resp.msg
                        });
                    } else {
                        layer.open({
                            title: '提示'
                            ,content: resp.data.msg
                        });
                    }
                }
            }
        });
    });

    // ~打开编辑页面或者是新增数据页面
    function showEditOrAdd(title,doc,opr){
        layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            maxmin: true,
            area: ['1050px', '730px'],
            content: $(doc),
            cancel: function(){
                $('#sbwl_from_id')[0].reset();
            }
        });
        // 设置操作类型!
        oprType = opr;
        layer.full(layer_);
    }

    // ~查看业务员的证照图片的弹出层~
    function showSalesmanSrc(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){
                //$('#sbwl_from_id')[0].reset();
            }
        });
    }

    var layer_prize;
    // ~查看业务员的奖罚弹出层~
    function showSalesmanPrize(title, doc){
        $('#sbwl_prize_id')[0].reset();
        layer_prize = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '370px'],
            content: $(doc),
            cancel: function(){
                $('#sbwl_prize_id')[0].reset();
            }
        });
    }

    //from表单监听提交
    form.on('submit(sbdj_submit)', function(data){
        var data = data.field;
        // 提示
        if (data.salesmanMobile === '1') {
            //layer.alert('注意该业务是一级业务员!', {icon: 7});
            layer.open({
                content: '请核对该业务无推荐人,如有推荐人请输入推荐人手机号码!',
                icon: 7,
                yes: function(index, layero){
                    //do something
                    layer.close(index); //如果设定了yes回调，需进行手工关闭
                    // 提交后台数据!
                    shenbi.ajax({
                        type:'post',
                        url:oprType==='add'?'/salesman/save':'/salesman/update',
                        data:data,
                        sendMsg:'提交中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.close(layer_);
                                if(oprType==="add"){
                                    layer.msg("成功");
                                }else{
                                    layer.msg(resp.message === "" ? "已更新" : resp.message);
                                }
                                // 刷新数据表
                                table_reload();
                                // 重置表单
                                $('#sbwl_from_id')[0].reset();
                            }else{
                                // ~异常信息
                                layer.msg(resp.message);
                            }
                        }
                    });
                }
            });
        } else {
            // 提交后台数据!
            shenbi.ajax({
                type:'post',
                url:oprType=='add'?'/salesman/save':'/salesman/update',
                data:data,
                sendMsg:'提交中...',
                success:function(resp){
                    if(resp.retCode === 0){
                        layer.close(layer_);
                        if(oprType=="add"){
                            layer.msg("成功");
                        }else{
                            layer.msg(resp.message == "" ? "已更新" : resp.message);
                        }
                        // 刷新数据表
                        table_reload();
                        // 重置表单
                        $('#sbwl_from_id')[0].reset();
                    }else{
                        // ~异常信息
                        layer.msg(resp.message);
                    }
                }
            });
        }
        return false
    });

    /*//from表单监听提交
    form.on('submit(sbwl_submit)', function(data){
        var data = data.field;
        // 提示
        if (data.newMobile === '1') {
            //layer.alert('注意该业务是一级业务员!', {icon: 7});
            layer.open({
                content: '请核对该业务无推荐人,如有推荐人请输入推荐人手机号码!',
                icon: 7,
                yes: function(index, layero){
                    //do something
                    layer.close(index); //如果设定了yes回调，需进行手工关闭
                    // 提交后台数据!
                    shenbi.ajax({
                        type:'post',
                        url:oprType=='add'?'/salesman/save':'/salesman/update',
                        data:data,
                        sendMsg:'提交中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.close(layer_);
                                if(oprType=="add"){
                                    layer.msg("成功");
                                }else{
                                    layer.msg(resp.message == "" ? "已更新" : resp.message);
                                }
                                // 刷新数据表
                                table_reload();
                                // 重置表单
                                $('#sbwl_from_id')[0].reset();
                            }else{
                                // ~异常信息
                                layer.msg(resp.message);
                            }
                        }
                    });
                }
            });
        } else {
            // 提交后台数据!
            shenbi.ajax({
                type:'post',
                url:oprType=='add'?'/salesman/save':'/salesman/update',
                data:data,
                sendMsg:'提交中...',
                success:function(resp){
                    if(resp.retCode === 0){
                        layer.close(layer_);
                        if(oprType=="add"){
                            layer.msg("成功");
                        }else{
                            layer.msg(resp.message == "" ? "已更新" : resp.message);
                        }
                        // 刷新数据表
                        table_reload();
                        // 重置表单
                        $('#sbwl_from_id')[0].reset();
                    }else{
                        // ~异常信息
                        layer.msg(resp.message);
                    }
                }
            });
        }
        return false
    });*/

    // ~图片上传
    var $showLoad = '';
    var uploadInst = upload.render({
        elem: '#test1', //绑定元素
        url: '/base/upload', //上传接口
        accept:'images',
        acceptMime:'image/*',
        before:function(){
            // 文件提交上传前的回调
            $showLoad = shenbi.showLoad("上传中...");
        },
        done: function(res){
            shenbi.closeLoad($showLoad);
            //上传完毕回调
            if(res.retCode==0 && res.message!=""){
                var id = $("select[name='zjlxId']").val();
                var map = {};
                console.log(res.message);
                $("input[name='cardImg']").val(res.message);
                $("#sbwl-img").attr('src',res.message)
                map.link = res.message;
                map.typeId = id;
                zjxx.push(map);
                map = {};
            }
        },error: function(res){
            //请求异常回调
            console.log(res)
        }
    });

    //~查看业务员号主的通讯录的弹出层~
    function showSalesmanPhones(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
        layer.full(layer_);
    }

    // 查看业务员提成数据
    function viewRoyaltySalesman(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
        layer.full(layer_);
    }

    // ~业务员号主的通讯录列表数据~
    function tableSalesmanPhones(id){
        // ~数据渲染
        table.render({
            id:'table_phones_list',
            elem: '#table_phones_data',
            url:'/salesman/phones?id='+id,
            //toolbar: '#toolbar',
            title: '业务员通讯录数据表',
            cols: [[
                {type:'checkbox',sort:true,align: 'center'},
                {type:'numbers',align: 'center'},
                {field:'name', title:'姓名',align: 'center'},
                {field:'phone', title:'电话', align: 'center'},
                {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
                        return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                    }}
            ]],
            parseData: function(res){
                return {
                    "code": res.retCode, 		//解析接口状态
                    "msg": res.data.message, 	//解析提示文本
                    "count": res.data.total, 	//解析数据长度
                    "data": res.data.list 	//解析数据列表
                }
            },
            page: true
        });
    }

    function showSalesmanOnline(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['1050px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
        layer.full(layer_);
    }

    function tableSalesmanOnline(id){
        // ~数据渲染
        table.render({
            id:'table_online_list',
            elem: '#table_online_data',
            url:'/salesman/relation?salesmanId='+id,
            //toolbar: '#toolbar',
            title: '业务员上级数据表',
            cols: [[
                {type:'checkbox',sort:true,align: 'center'},
                {type:'numbers',align: 'center'},
                {field:'name', title:'姓名',align: 'center'},
                {field:'mobile', title:'电话', align: 'center'},
                {field:'salesmanName', title:'上级姓名',align: 'center'},
                {field:'salesmanMobile', title:'上级电话', align: 'center'},
                {field:'status', title:'状态',align: 'center',templet:function(rse){
                        if(rse.status==="E"){
                            return '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="sbwl-prohibit">'+kits.returnStatus(rse.status)+'</a>';
                        }else if(rse.status==="P"){
                            return '<a class="layui-btn layui-btn-xs layui-btn-disabled" lay-event="sbwl-enable">'+kits.returnStatus(rse.status)+'</a>';
                        }else{
                            return '<a class="layui-btn layui-btn-xs layui-btn-disabled">'+kits.returnStatus(rse.status)+'</a>';
                        }
                    }},
            ]],
            parseData: function(res){
                return {
                    "code": res.retCode, 		//解析接口状态
                    "msg": res.message, 	//解析提示文本
                    "count": res.data.length, 	//解析数据长度
                    "data": res.data 	//解析数据列表
                }
            },
            page: false
        });
    }

    //三网手机在网时长
    function lengthTimeFunct(data){
        if(data === '1'){
            return "在网时长是 0~3 个月";
        }else if(data === '2'){
            return "在网时长是 3~6 个月";
        }else if(data === '3'){
            return "在网时长是 6~12 个月";
        }else if(data === '4'){
            return "在网时长是 12~24 月";
        }else if(data === '5'){
            return "在网时长是 24 个月以上";
        }else if(data === '6'){
            return "系统无记录";
        }
    }

    // 时长验证
    $("#onlineCertification").click(function () {
       var id = $("#id").val();
       var mobile = $("#mobile").val();
        shenbi.ajax({
            type:'post',
            url:'/salesman/mobile/online',
            data:{
                id: id,
                mobile: mobile
            },
            sendMsg:'验证中...',
            success:function(resp){
                $("#lengthTime").val(resp.message);
            }
        });
       table_reload();
    });

    // 手机验证
    $("#mobileCertification").click(function () {
        var id = $("#id").val();
        var name = $("#name").val();
        var mobile = $("#mobile").val();
        var idCard = $("#idCard").val();
        shenbi.ajax({
            type:'post',
            url:'/salesman/mobile/certification',
            data:{
                id: id,
                mobile: mobile,
                name: name,
                idCard: idCard
            },
            sendMsg:'验证中...',
            success:function(resp){
                // ~表单赋值
                $("#idMobileName").val(resp.message);
            }
        });
        table_reload();
    });

    // 银行验证
    $("#bankCertification").click(function () {
        var id = $("#id").val();
        var name = $("#name").val();
        var mobile = $("#mobile").val();
        var idCard = $("#idCard").val();
        var bankNum = $("#bankNum").val();
        shenbi.ajax({
            type:'post',
            url:'/salesman/bank/certification',
            data:{
                id: id,
                mobile: mobile,
                name: name,
                idCard: idCard,
                bankNum: bankNum
            },
            sendMsg:'验证中...',
            success:function(resp){
                $("#idMobileNameBank").val(resp.message);
            }
        });
        table_reload();
    });

    // 信息认证
    $("#certification").click(function () {
        $(this).attr("disabled", "disabled");
        var id = $("#id").val();
        var name = $("#name").val();
        var mobile = $("#mobile").val();
        var idCard = $("#idCard").val();
        var bankNum = $("#bankNum").val();
        shenbi.ajax({
            type:'post',
            url:'/salesman/certification',
            data:{
                id: id,
                mobile: mobile,
                name: name,
                idCard: idCard,
                bankNum: bankNum
            },
            sendMsg:'验证中...',
            success:function(resp){
                $("#idMobileNameBank").val(resp.data.idMobileNameBankInfo);
                $("#idMobileName").val(resp.data.idMobileNameInfo);
                $("#lengthTime").val(resp.data.mobileOnlineInfo);
                table_reload();
                $("#certification").removeAttrs("disabled");
            }
        });
    });
});