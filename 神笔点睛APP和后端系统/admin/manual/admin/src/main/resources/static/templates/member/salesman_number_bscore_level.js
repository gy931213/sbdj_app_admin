//
layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;

    // ~操作类型[add|update],默认为add
    var oprType = 'add';
    var layer_ = '';

    // ~渲染业务员列表数据~
    table.render({
        id: 'table_list',
        method: 'post',
        elem: '#table-data',
        url: '/salesman/number/level',
        toolbar: '#toolbar',
        title: '号主信用配置',
        cols: [[
        	{type:'checkbox',sort:true,align: 'center'},
      	  {type:'numbers',title:'序号',align: 'center',width:'5%'},
            {field: 'id', title: '信用等级id', hide: true},
            {field: 'levelName', title: '信用等级名称'},
            {field: 'level', title: '信用等级'},
            {field: 'weekAvg', title: '周平均'},
            {field: 'beginBscore', title: '开始的信用分'},
            {field: 'endBscore', title: '结束的信用分'},
            {field: 'status', title: '状态', hide: true},
            {
                field: 'createTime', title: '创建时间', templet: function (rse) {
                    return kits.dateFtt("yyyy-MM-dd hh:mm:ss", rse.createTime);
                }, hide: true
            },
            {field: 'sort', title: '排序'},
            {fixed: 'right', title: '操作', toolbar: '#operation', width: 200, align: 'center'}
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        parseData: function (res) {
            return shenbi.returnParse(res);
        },
        page: true
    });

    // ~刷新数据列表
    function table_reload() {
        table.reload('table_list');
    }

    // ~工具栏事件~
    table.on('toolbar(table-filter)', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        switch (obj.event) {
            // ~新增等级
            case 'sbwl-add-salesman-level':
                // ~新增页面
                showEditOrAdd('新增业务员等级', '#sbwl_tmpl', 'add');
                break;
            // ~删除等级
            case 'sbwl-del-salesman-level':
                var data = checkStatus.data;
                var ids = [];
                $.each(data, function (index, item) {
                    ids.push(item.id);
                });

                if (ids.length <= 0) {
                    layer.msg("至少选择一项进行操作");
                    return false
                }

                layer.msg('确定要批量删除吗？', {
                    btn: ['确定', '取消'],
                    yes: function (index, layero) {
                        var data = obj.data;
                        shenbi.ajax({
                            type: 'get',
                            url: '/salesman/number/level/deletes',
                            data: {ids: ids.join()},
                            sendMsg:'删除中...',
                            success: function (resp) {
                                if (resp.retCode == 0) {
                                    layer.msg("已删除");
                                    table_reload();
                                } else {
                                    layer.msg("删除失败");
                                    table_reload();
                                }
                            }
                        });
                    }
                });
                break;
        }
        ;
    });

    // ~监听行工具事件
    table.on('tool(table-filter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'sbwl-del') {
            // ~删除操作~
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function (index, layero) {
                    shenbi.ajax({
                        type: 'get',
                        url: '/salesman/number/level/delete',
                        data: {id: data.id},
                        sendMsg:'删除中...',
                        success: function (resp) {
                            if (resp.retCode === 0) {
                                layer.msg("已删除");
                            } else {
                                layer.msg("删除失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        } else if (obj.event === 'sbwl-edit') {
            // 编辑数据

            // ~表单赋值
            form.val("sbwl_from", {
                "id": data.id,
                "levelName": data.levelName,
                "level": data.level,
                "weekAvg" : data.weekAvg,
                "beginBscore" : data.beginBscore,
                "endBscore" : data.endBscore,
                "sort" : data.sort
            });

            // ~编辑页面
            showEditOrAdd('编辑业务员等级', '#sbwl_tmpl', 'update');
        }
    });

    // ~打开编辑页面或者是新增数据页面
    function showEditOrAdd(title, doc, opr) {
        layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            //shadeClose: false,
            //maxmin: true,
            area: ['40%', '60%'],
            content: $(doc),
            cancel: function () {
                $('#sbwl_from_id')[0].reset();
            }
        });
        // 设置操作类型!
        oprType = opr;
    }


    //自定义验证规则
    form.verify({
        levelName: [
            /^[\S]{2,5}$/
            , '请输入2到5位等级名称'
        ]
        , level:function (val) {
            var reg = /^[1-9]{1,4}$/;
            if(!(reg.test(val))){
                return "请输入正确的等级";
            }
        }
        , weekAvg:function (val) {
            //decimal正则
            var reg = /^[0-9]\d{0,2}(\.\d{0,2})?$/
            var reg1 = /^([0-9]{1,4})$/;
            if(!(reg.test(val) || reg1.test(val))){
                return "请输入正确格式的周平均数";
            }
        }
        , beginBscore: [
            /^\d{1,12}$/
            , '请输入正确格式的信用分'
        ]
        , endBscore: [
            /^\d{1,12}$/
            , '请输入正确格式的信用分'
        ]
        , sort: [
            /^\d{1,4}$/
            , '请输入正确格式的排序号'
        ]
    });

    //from表单监听提交
    form.on('submit(sbwl_submit)', function (data) {
        // 提交后台数据!
        shenbi.ajax({
            type: 'post',
            url: '/salesman/number/level/save',
            data: data.field,
            sendMsg:'提交中...',
            success: function (resp) {
                if (resp.retCode == 0) {
                    layer.close(layer_);
                    if (oprType == "add") {
                        layer.msg("成功");
                    } else {
                        layer.msg("已更新");
                    }
                    // 刷新数据表
                    table_reload();
                    // 重置表单
                    $('#sbwl_from_id')[0].reset();
                } else {
                    // ~异常信息
                    layer.msg(resp.message);
                }
            }
        });
        return false
    });
    
});