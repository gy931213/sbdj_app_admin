//
layui.use(['table','laydate', 'form'], function () {
    var table = layui.table,
        form = layui.form,
        laydate = layui.laydate;

    //开始日期
    laydate.render({
        elem: '#beginDate',
        type: 'datetime'
    });
    //结束日期
    laydate.render({
        elem: '#endDate',
        type: 'datetime'
    });

    // 渲染业务员列表数据~
    table.render({
        id: 'table_list',
        method: 'post',
        elem: '#table-data',
        url: '/sn/cash/return/record/list',
        toolbar: '#toolbar',
        title: '号主返现记录列表',
        where:{returnStatus:'fail'},
        cols: [[
        	{type:'checkbox',sort:true,align: 'center'},
      	  	{type:'numbers',title:'序号',align: 'center',width:'5%'},
            {field: 'orgName', title: '所属机构',hide: true},
            {field: 'platformName', title: '所属平台'},
            {field: 'sellerShopName', title: '所属店铺'},
            {field: 'taskSonNumber', title: '所属子任务',hide: true},
            {field: 'taskCreateTime', title: '任务领取时间',hide: true,templet: function (rse) {
                    return kits.dateFtt("yyyy-MM-dd hh:mm:ss", rse.taskCreateTime);
            }},
            {field: 'salesmanName', title: '所属业务员'},
            {field: 'oprName', title: '操作人',hide: true},
            {field: 'oprTime', title: '操作时间',hide: true,templet: function (rse) {
                    return kits.dateFtt("yyyy-MM-dd hh:mm:ss", rse.oprTime);
            }},
            {field: 'orderNum', title: '订单号'},
            {field: 'onlineid', title: '会员号ID'},
            {field: 'returnStatus',width: 150, title: '返现状态', align: 'center',templet: function (rse) {
            	if(rse.returnStatus === 'success'){
            		 return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #60c560;">成功</a>';
            	}else{
            		 return '<a class="layui-btn layui-btn-sm" lay-event="return-to-cash" style="width:55px;background-color: #d9534f;">失败</a>';
            	}
            }},
            {field: 'reason', title: '失败原因',hide: true},
            {field: 'realPrice', title: '实付金额',hide: true},
            {
                field: 'createTime', title: '创建时间', templet: function (rse) {
                    return kits.dateFtt("yyyy-MM-dd hh:mm:ss", rse.createTime);
                }, hide: true
            },
            {fixed: 'right', title:'操作', width:100,templet:function(rse){
                    var _html = '';
                    if (rse.resources === undefined || rse.resources === null || rse.resources === '') {
                        _html+='<a class="layui-btn layui-btn-sm layui-btn-danger"><i class="layui-icon">&#xe615;</i> 截图</a>';
                    } else {
                        _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-check-img"><i class="layui-icon">&#xe615;</i> 截图</a>';
                    }
                    return _html;
                }}
           /* {fixed: 'right', title: '操作', toolbar: '#operation', width: 200, align: 'center'}*/
        ]],
        limit:30,
        limits:[20,30,50,100,200,500,1000],
        parseData: function (res) {
            return shenbi.returnParse(res);
        },
        page: true
    });

    // ~渲染select选项 （机构）
	/*kits.select({
		elem:'#sbwl_org_select',
		url:'/sys/organization/select',
		value:'id',
		name:'name',
		form:form
	});*/
	
	 //渲染select选项 (平台)
	 kits.select({
		 elem:'.sbwl_selece',
		 url:'/config/platform/select',
		 value:'id',
		 name:'name',
		 form:form
	 });
    
    // 刷新数据列表
    function table_reload() {
        table.reload('table_list');
    }
    
    // 监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
      table.reload('table_list',{
    	  page: {
              curr: 1 //重新从第 1 页开始
           },
           where:data.field
      });
      return false;
    });

    // 工具栏事件
    table.on('toolbar(table-filter)', function(obj){
      var checkStatus = table.checkStatus(obj.config.id);
      switch(obj.event){
        case 'allReturn':
          var data = checkStatus.data;
     	  // 获取所选中的数据
      	  var ids = [];
          if(data.length < 2){
      		  layer.msg("必须选中二项进行操作！");  
      		  return false;
      	  }
          // 遍历选中的值
          $.each(data,function(i,item){
      		  ids.push(item.id);
      	  });
      	  if(ids.length < 2){
      		  layer.msg("必须选中2项进行操作！");  
      		  return false;
      	  }
      	  
      	  // 批量返现
    	 layer.msg('确定批量返现吗？', {
    	    btn: ['确定', '取消'],
    	    yes: function(index, layero){
    	    	var html = '<ul class="layer_notice" style="display: block;background: #279a3fc4;">';
		    	shenbi.ajax({
					type:'POST',
					url:'/sn/cash/return/record/cashbacks',
					data:{ids:ids.join()},
					sendMsg:'返现提交中...',
					success:function(resp){
						if(resp.retCode === 0){
    						var data = resp.data;
    						html += ' <li>&nbsp;<a style="color: #efeaea"> 1. '+data.success+'</a></li>';
    						html += ' <li>&nbsp;<a style="color: #efeaea"> 2. '+data.error_msg+'</a></li>';
    						html += '</ul>';
    						layer.open({
							  type: 1,
							  shade: false,
							  title: false,
							  content: html, 
							  cancel: function(){
								  
							  }
							});
    					}else{
    						layer.msg("返现失败");
    					}
						// 刷新数据
						table_reload();
					}
				});
    	    }
    	  });
        break;
      }});
    
    
    
    // 监听 lay-event 相关触发事件!
    table.on('tool(table-filter)', function(obj) {
        var data = obj.data;
        if (obj.event === 'return-to-cash') {
            layer.msg('确定要返现吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    // 请求后台返现接口
                    shenbi.ajax({
                        type:'post',
                        url:'/sn/cash/return/record/cashback',
                        data:{
                            id:data.id,
                        },
                        sendMsg:'返现中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已返现");
                                // 刷新数据
                                table_reload();
                            }else{
                                layer.msg(resp.message == "" ? "返现失败" : resp.message);
                            }
                        }
                    });
                }
            });
        } else if(obj.event === 'sbwl-check-img'){
            if (data.resources === undefined || data.resources === null || data.resources === '') {return;}
            var imgs = data.resources.split(',');
            // ~查看任务截图
            // ~获取业务员相关的证件图片~
            var _html = '</br>';
            shenbi.ajax({
                type:'post',
                url:'/sn/cash/return/record/resources',
                data:{taskSonId:data.taskSonId},
                success:function(resp){
                    if(resp.retCode === 0){
                        var data = resp.data;
                        for (var i = 0; i < data.length; i++) {
                            _html+='<div class="layui-form-item task-div-son-img-li">';
                            _html+='	<div class="sbwl-task-son-img">';
                            _html+='		<img id="sbwl-img" alt="" src="'+data[i].link+'" style="width: 400px">';
                            _html+='		<div class="scr"><b>子任务相关</b></div>';
                            _html+='	</div>';
                            _html+='</div>';
                        }
                        for (var i = 0; i < imgs.length; i++) {
                            _html+='<div class="layui-form-item task-div-son-img-li">';
                            _html+='	<div class="sbwl-task-son-img">';
                            _html+='		<img id="sbwl-img" alt="" src="'+imgs[i]+'" style="width: 400px">';
                            _html+='		<div class="scr"><b>返现相关</b></div>';
                            _html+='	</div>';
                            _html+='</div>';
                        }
                        // ~渲染数据~
                        $("#sbwl_taskSon_tmpl_img").html(_html);
                        // ~渲染数据~
                        showTaskSonImgSrc('<i class="layui-icon">&#xe64a;</i> 任务截图','#sbwl_taskSon_tmpl');
                    }
                }
            });
        }
    });

    //~查看子任务截图的弹出层~
    function showTaskSonImgSrc(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
    }
    
});