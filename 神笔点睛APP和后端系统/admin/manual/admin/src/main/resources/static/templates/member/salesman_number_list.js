
// 
layui.use(['table','form','upload'], function(){
    var upload = layui.upload;
    var table = layui.table;
    var form  = layui.form;

    // ~操作类型[add|update],默认为add
    var $oprType = 'add';
    var $layer = '';
    var $pageIndex = '';

    // 快链字段设置样式
    var key = ['salesmanName','onlineid','name','mobile','cardNum'];

    // ~
    table.render({
        id:'table_list',
        method:'post',
        elem: '#table-data',
        url:'/salesman/number/list',
        toolbar: '#toolbar',
        title: '号主数据表',
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
            {field:'platformName', title:'所属平台', align: 'center'},
            {field:'salesmanName', title:'所属业务员', align: 'center',event:'search-salesman-name'},
            {field:'onlineid', title:'号主ID',align: 'center',event:'search-onlineid'},
            {field:'name', title:'姓名',align: 'center',event:'search-name'},
            {field:'mobile', title:'电话',align: 'center',event:'search-mobile'},
            {field:'cardNum', title:'身份证号',align: 'center',event:'search-cardNum'},
            {field:'provinceName', title:'所在地区', align: 'center',templet:function(rse){
                    return  rse.provinceName+" "+rse.cityName+" "+rse.countyName;
                }},
            {field:'provinceName', title:'省', align: 'center',hide:true},
            {field:'cityName', title:'市', align: 'center',hide:true},
            {field:'countyName', title:'区', align: 'center',hide:true},
            {field:'imei', title:'串号', align: 'center',hide:true},
            {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {field:'status', title:'状态', width:80,align: 'center',templet: function(rse){
                    if(rse.status=="E"){
                        return '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="sbwl-prohibit">'+kits.returnStatus(rse.status)+'</a>';
                    }else if(rse.status=="P"){
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled" lay-event="sbwl-enable">'+kits.returnStatus(rse.status)+'</a>';
                    }else{
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled">'+kits.returnStatus(rse.status)+'</a>';
                    }
                }},
            {fixed: 'right', title:'操作',width:280,align: 'center',templet:function(res){
                    var _html = '';
                    /* if(res.status=="E"){
                         _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-prohibit"><i class="layui-icon">&#x1006;</i> 禁用</a>';
                     }else if(res.status=="P"){
                         _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-enable"><i class="layui-icon">&#xe605;</i> 启用</a>';
                     }*/
                    // _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-checking"><i class="layui-icon">&#xe615;</i> 证件</a>';
                    // _html+='<a class="layui-btn layui-btn-sm layui-btn-warm" lay-event="sbwl-mail-list"><i class="layui-icon">&#xe615;</i> 通讯录</a>';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-warm" lay-event="sbwl-marking-list"><i class="layui-icon">&#xe615;</i> 全网</a>';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                    return _html;
                }}
        ]],
        limit:10,
        limits:[10,20,30,50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true
    });

    // 表头样式
    tableThStyle(key);

    //渲染select选项 (平台)
    kits.select({
        elem:'.sbwl_selece',
        url:'/config/platform/select',
        value:'id',
        name:'name',
        form:form
    });

    // ~刷新数据列表
    function table_reload(){
        table.reload('table_list');
        // 表头样式
        tableThStyle(key);
    }
    // ~渲染select选项（省）
    kits.select({
        elem:'.sbwl_prov_select',
        url:'/sys/area/select',
        data:{parentId:-1},
        value:'areaId',
        name:'areaName',
        form:form,
        option:'<option value="0">请选择省</option>'
    });

    // ~监听select选中的事件（省）
    form.on('select(sbwl_prov_select)', function(data){
        // ~渲染select选项（市）
        kits.select({
            elem:'.sbwl_city_select',
            url:'/sys/area/select',
            data:{parentId:data.value},
            value:'areaId',
            name:'areaName',
            form:form,
            option:'<option value="0">请选择市</option>'
        });
        // ~重置县区的数据选项~
        $(".sbwl_county_select").html('<option value="0">请选择县/区</option>');
    });

    // ~监听select选中的事件（市）
    form.on('select(sbwl_city_select)', function(data){
        // ~渲染select选项（县/区）
        kits.select({
            elem:'.sbwl_county_select',
            url:'/sys/area/select',
            data:{parentId:data.value},
            value:'areaId',
            name:'areaName',
            form:form,
            option:'<option value="0">请选择县/区</option>'
        });
    });

    // ~监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });

        // 表头样式
        tableThStyle(key);
        return false;
    });

    //头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'province':
                showprovince("省数量统计","#sbwl_province_tmpl");
                break;
            case 'getCheckLength':
                var data = checkStatus.data;
                layer.msg('选中了：'+ data.length + ' 个');
                break;
            case 'isAll':
                layer.msg(checkStatus.isAll ? '全选': '未全选');
                break;
            case 'add':
                // ~所属平台
                kits.select({
                    elem:'#sbwl-selece',
                    url:'/config/platform/select',
                    value:'id',
                    name:'name',
                    form:form,
                    option:'<option value="0">请选择平台</option>',
                });
                // ~省市区
                // ~渲染select选项（省）
                kits.select({
                    elem:'.sbwl_prov_select',
                    url:'/sys/area/select',
                    value:'areaId',
                    name:'areaName',
                    form:form,
                    option:'<option value="0">请选择省</option>'
                });

                $("input[name='salesMobile']").attr("placeholder","输入所属业务员登录账号");

                // ~编辑数据
                showEditOrAdd('添加号主信息','#sbwl_tmpl','add');
                break;
        };
    });

    // ~from表单验证规则
    form.verify({
        platformId: function(value){
            if(value===0){
                return "请选择对应平台";
            }
        },
        provinceId: function(value){
            if(value===0 || value === ''){
                return "请选择对应省份";
            }
        },
        cityId: function(value){
            if(value===0 || value === ''){
                return "请选择对应城市";
            }
        },
        countyId: function(value){
            if(value===0 || value === ''){
                return "请选择对应区/镇";
            }
        },
        name: function (value) {
            if(value===''){
                return "请输入姓名";
            }
        },
        mobile: function (value) {
            if (value==='') {
                return '请输入手机号';
            }
        },
        onlineid: function (value) {
            if (value==='') {
                return '请输入串号';
            }
        }
    });

    // ~监听数据列表中的操作触发事件~
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        if(obj.event === 'sbwl-prohibit'){
            // ~禁用操作~
            layer.msg('确定要禁用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/salesman/number/status',
                        data:{
                            id:data.id,
                            status:"P"
                        },
                        sendMsg:'禁用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已禁用");
                            }else{
                                layer.msg("禁用失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-enable'){
            // ~启用操作~
            layer.msg('确定要启用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/salesman/number/status',
                        data:{
                            id:data.id,
                            status:"E"
                        },
                        sendMsg:'启用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已启用");
                            }else{
                                layer.msg("启用失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-edit'){
            // ~编辑操作~

            // ~所属平台
            kits.select({
                elem:'#sbwl-selece',
                url:'/config/platform/select',
                value:'id',
                name:'name',
                form:form,
                select:data.platformId,
                option:'<option value="0">请选择平台</option>',
            });
            // ~省市区
            // ~渲染select选项（省）
            kits.select({
                elem:'.sbwl_prov_select',
                url:'/sys/area/select',
                data:{parentId:-1},
                value:'areaId',
                name:'areaName',
                form:form,
                option:'<option value="0">请选择省</option>',
                select:data.provinceId
            });

            // ~渲染select选项（市）
            kits.select({
                elem:'.sbwl_city_select',
                url:'/sys/area/select',
                data:{parentId:data.provinceId},
                value:'areaId',
                name:'areaName',
                form:form,
                option:'<option value="0">请选择市</option>',
                select:data.cityId
            });

            // ~渲染select选项（县/区）
            kits.select({
                elem:'.sbwl_county_select',
                url:'/sys/area/select',
                data:{parentId:data.cityId},
                value:'areaId',
                name:'areaName',
                form:form,
                option:'<option value="0">请选择县/区</option>',
                select:data.countyId
            });

            // ~表单赋值
            form.val("sbwl_from", {
                "id":data.id,
                "name":data.name,
                "mobile":data.mobile,
                "onlineid":data.onlineid,
                "cardNum":data.cardNum,
                "imei":data.imei,
            });

            // ~所属业务员
            $("input[name='salesMobile']").attr("placeholder","所属业务员：【"+data.salesmanName+"】，切换所属业务员，输入所属业务员登录账号");

            // ~编辑数据
            showEditOrAdd('编辑号主信息','#sbwl_tmpl','update');
        }else if(obj.event === 'sbwl-checking'){
            // ~查看证照操作~

            // ~获取业务员相关的证件图片~
            shenbi.ajax({
                type:'post',
                url:'/salesman/number/src',
                data:{id:data.id},
                success:function(resp){
                    if(resp.retCode === 0){
                        var data = resp.data;
                        var _html = '</br>';
                        for (var i = 0; i < data.length; i++) {
                            _html+='<div class="layui-form-item">';
                            _html+='	<div class="layui-input-block">';
                            _html+='		<img id="sbwl-img" alt="" src="'+data[i].link+'" style="width: 400px">';
                            _html+='		<div class="scr"><b>'+data[i].typeName+'</b></div>';
                            _html+='	</div>';
                            _html+='</div>';
                        }
                        // ~渲染数据~
                        $("#sbwl_src_tmpl").html(_html);
                    }
                }
            });

            // ~渲染数据~
            showSalesmanNumberSrc('<i class="layui-icon">&#xe64a;</i> 证照图片','#sbwl_src_tmpl');
        }else if(obj.event === 'sbwl-mail-list'){
            // ~查看通讯录操作~
            if(data=="" || data.id=="") return false;
            tableSalesmanNumberPhones(data.id);
            showSalesmanNumberPhones('<i class="layui-icon">&#xe613;</i> 通讯录','#sbwl_phones_tmpl');

        }else if(obj.event === 'search-salesman-name'){
            if(!dblclick()){
                return false;
            }
            // ~所属业务员
            $("input[name='salesmanName']").val(data.salesmanName);
            table.reload('table_list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'salesmanName':data.salesmanName}
            });
        }else if(obj.event === 'search-name'){
            if(!dblclick()){
                return false;
            }
            // ~号主名称
            $("input[name='name']").val(data.name);
            table.reload('table_list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'name':data.name}
            });
        }else if(obj.event === 'search-onlineid'){
            if(!dblclick()){
                return false;
            }
            // ~号主id
            $("input[name='onlineid']").val(data.onlineid);
            table.reload('table_list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'onlineid':data.onlineid}
            });
        }else if(obj.event === 'search-mobile'){
            if(!dblclick()){
                return false;
            }
            // ~号主手机号
            $("input[name='mobile']").val(data.mobile);
            table.reload('table_list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'mobile':data.mobile}
            });
        } else if(obj.event === 'sbwl-marking-list') {
            // ~表单赋值
            form.val("sbwl_marking_from", {
                "onlineid":data.onlineid,
                "name": data.name
            });
            showSalesmanNumberMarking('<i class="layui-icon">&#xe615;</i> 全网打标','#sbwl_marking_tmpl')
        }/*else if(obj.event === 'search-cardNum'){
    	// ~身份证号
    	$("input[name='cardNum']").val(data.cardNum);
    	table.reload('table_list',{
	        where:{'cardNum':data.cardNum}
	    });
	    return false;
    }*/ else{
            // ~没有相关操作~
        }
        // 表头样式
        tableThStyle(key);
    });

    //~查看业务员号主的证照图片的弹出层~
    function showSalesmanNumberSrc(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
    }

    //~查看业务员号主的通讯录的弹出层~
    function showSalesmanNumberPhones(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
        layer.full(layer_);
    }

    // ~打开编辑页面或者是新增数据页面
    function showEditOrAdd(title,doc,opr){
        $layer = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            maxmin: true,
            area: ['1050px', '730px'],
            content: $(doc),
            cancel: function(){
                $('#sbwl_from_id')[0].reset();
            }
        });
        // 设置操作类型!
        $oprType = opr;
        layer.full($layer);
    }

    //from表单监听提交
    form.on('submit(sbwl_submit)', function(data){
        // 提交后台数据!
        shenbi.ajax({
            type:'post',
            url:$oprType=='add'?'/salesman/number/save':'/salesman/number/update',
            data:data.field,
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.close($layer);
                    if($oprType=="add"){
                        layer.msg("成功");
                    }else{
                        layer.msg("已更新");
                    }
                    // 刷新数据表
                    table_reload();
                    // 重置表单
                    $('#sbwl_from_id')[0].reset();
                }else{
                    // ~异常信息
                    layer.msg(resp.message);
                }
            }
        });
        return false
    });

    var layer_marking;
    // ~查看业务员号主的全网打标弹出层~
    function showSalesmanNumberMarking(title, doc){
        $('#sbwl_marking_id')[0].reset();
        layer_marking = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['580px', '233px'],
            content: $(doc),
            cancel: function(){
                $('#sbwl_marking_id')[0].reset();
            }
        });
    }

    //from验证业务员号主全网打标表单监听提交
    form.on('submit(sbwl_marking_submit)', function(data){
        var  item = data.field;

        if(item.type == "" || item.type == null){
            layer.msg("请选择类型");
            return false;
        }

        //alert(JSON.stringify(item))

        shenbi.ajax({
            type:'get',
            url:'/marking/upload',
            data: {
                onlineid: item.onlineid,
                type: item.type
            },
            sendMsg:'提交中...',
            success:function(resp){
                layer.msg(resp.message);
                if(resp.retCode === 0) {
                    // 刷新数据表
                    table_reload();
                    layer.close(layer_marking);
                }
            }
        });
        return false
    });

    // ~业务员号主的通讯录列表数据~
    function tableSalesmanNumberPhones(id){
        // ~数据渲染
        table.render({
            id:'table_phones_list',
            elem: '#table_phones_data',
            url:'/salesman/number/phones?id='+id,
            //toolbar: '#toolbar',
            title: '号主通讯录数据表',
            cols: [[
                {type:'checkbox',sort:true,align: 'center'},
                {type:'numbers',align: 'center'},
                {field:'name', title:'姓名',align: 'center'},
                {field:'phone', title:'电话', align: 'center'},
                {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
                        return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                    }}
            ]],
            parseData: function(res){
                return {
                    "code": res.retCode, 		//解析接口状态
                    "msg": res.data.message, 	//解析提示文本
                    "count": res.data.total, 	//解析数据长度
                    "data": res.data.list 	//解析数据列表
                }
            },
            page: true
        });
    }

    // 统计省数据（号主分布）
    function showprovince(title,doc){
        var $province = echarts.init(document.getElementById('province'));

        var $layer = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            maxmin: true,
            area: ['1050px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
        //layer.full($layer);

        $.ajax({
            url: '/salesman/echarts.json',
            type: 'get',
            success: function (resp) {
                if(resp.retCode === 0){
                    province(resp.data.province,$province);
                }
            },
            error: function (data) {
                layer.msg("服务器繁忙，请稍后重试...",{time:4000});
            }
        });
    }

    function province(data,$province){
        var legendData = [];
        var seriesData = data;
        var selected = [];
        for(var i = 0; i < data.length; i++){
            legendData.push(data[i].name);
            //selected.push(data[i].name +":"+"true");
        }

        option = {
            title : {
                text: '省数量统计',
                subtext: '',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                type: 'scroll',
                orient: 'vertical',
                right: 0,
                top: 20,
                bottom: 20,
                data: legendData,

                selected: selected
            },
            series : [
                {
                    name: '省',
                    type: 'pie',
                    radius : '50%',
                    center: ['40%', '50%'],
                    data: seriesData,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };

        $province.setOption(option);
    }

});