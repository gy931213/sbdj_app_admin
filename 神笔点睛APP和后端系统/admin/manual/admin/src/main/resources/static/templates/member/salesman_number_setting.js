
// 
layui.use(['table','form','upload'], function(){
  var upload = layui.upload;
  var table = layui.table;
  var form  = layui.form;
  
  // ~操作类型[add|update],默认为add
  var oprType = 'add';
  var layer_ = ''; 
  // ~证件信息
  var zjxx = [];
  
  // ~渲染业务员列表数据~
  table.render({
	id:'table_list',
	method:'post',
    elem: '#table-data',
	url:'/salesman/number/setting/list',
	toolbar: '#toolbar',
	title: '查号规则配置',
	where:{orgId:0},
	cols: [[
		{type:'checkbox',sort:true,align: 'center'},
		  {type:'numbers',title:'序号',align: 'center',width:'5%'},	  
      {field:'orgName', title:'所属机构', align: 'center'},
	  {field:'month', title:'注册时间', align: 'center',templet:function(rse){
		 return "大于"+rse.month+"个月";
	  }},
	  {field:'verify', title:'实名认证', align: 'center',templet:function(rse){
		  if(rse.verify === '0'){
			 return "否";
		  }else{
			  return "是"; 
		  }
	  }},
	  {field:'bscore', title:'买家信用不得小于', align: 'center',templet:function(rse){
		  return  rse.bscore+" 分";
	  }},
	  {field:'sellerQueryTotal', title:'商家查询不得大于', align: 'center',templet:function(rse){
		  return  rse.sellerQueryTotal+" 次";
	  }},
	  {field:'xhPd', title:'跑单', align: 'center',hide:true},
	  {field:'xhQz', title:'敲诈', align: 'center',hide:true},
	  {field:'xhPz', title:'骗子', align: 'center',hide:true},
	  {field:'xhDj', title:'打假', align: 'center',hide:true},
	  {field:'xhCp', title:'差评', align: 'center',hide:true},
	  {field:'xhTk', title:'淘客', align: 'center',hide:true},
	  {field:'xhJq', title:'降权', align: 'center',hide:true},
	  {field:'xhHmd', title:'云黑名单', align: 'center',hide:true},
	  {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
	  }},
	  {fixed: 'right', title:'操作', width:180,  toolbar: '#operation',align: 'center'}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return shenbi.returnParse(res);
    },
	page: true
  });
  
  // ~刷新数据列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  // ~渲染select选项 （机构）
	kits.select({
		elem:'#sbwl_org_select',
		url:'/sys/organization/select',
		value:'id',
		name:'name',
		form:form
	});
	
	// ~监听搜索按钮
   form.on('submit(sbwl_search)', function(data){
     table.reload('table_list',{
    	 page: {
             curr: 1 //重新从第 1 页开始
          },
          where:data.field
     });
     return false;
   });
  
  // ~工具栏事件~
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      // ~新增业务员
      case 'sbwl-add-salesman':
    	 // ~渲染select选项（机构） 
    	 kits.select({
    		 elem:'.sbwl_org_select',
    		 url:'/sys/organization/select',
    		 value:'id',
    		 name:'name',
    		 form:form
    	 });
    	// ~新增页面
		showEditOrAdd('新增查号规则','#sbwl_tmpl','add');
      break;
      // ~删除业务员
      case 'sbwl-del-salesman':
    	    var data = checkStatus.data;
            layer.msg('选中了：'+ data.length + ' 个');
      break;
    };
  });
	
	// ~from表单数据验证
	form.verify({
		orgId: function(value){
			if(value==0){
				return "请选择所属机构";
			}
		}
	});
  
  // ~监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    if(obj.event === 'sbwl-edit'){
   	 // 设置表单数据!
   	    form.val("sbwl_from", {
   	      "id":data.id,
   	      "month":data.month,
		  "bscore":data.bscore,
		  "verify":data.verify,
		  "sellerQueryTotal":data.sellerQueryTotal,
		  "xhPd":data.xhPd,
		  "xhQz":data.xhQz,
		  "xhPz":data.xhPz,
		  "xhDj":data.xhDj,
		  "xhCp":data.xhCp,
		  "xhTk":data.xhTk,
		  "xhJq":data.xhJq,
		  "xhHmd":data.xhHmd
		});
    	// $("input[name=verify][value="+data.verify+"]").attr("checked",true);
         //form.render(); //更新全部  
   	    
   	    // 渲染机构的 select选项 
    	 kits.select({
    		 elem:'.sbwl_org_select',
    		 url:'/sys/organization/select',
    		 value:'id',
    		 name:'name',
    		 select:data.orgId,
    		 form:form
    	 });
    	 
    	// ~新增页面
 		showEditOrAdd('编辑查号规则','#sbwl_tmpl','update');
    }else if(obj.event === 'sbwl-del'){
    	layer.msg('确定要删除吗？', {
    	    btn: ['确定', '取消'],
    	    yes: function(index, layero){
    	    	var data = obj.data;
    	    	shenbi.ajax({
    				type:'post',
    				url:'/salesman/number/setting/delete',
    				data:{id:data.id},
    				sendMsg:'删除中...',
    				success:function(resp){
    					if(resp.retCode === 0){
    						layer.msg("已删除"); 
    					}else{
    						layer.msg("删除失败");
    					}
    					table_reload();
    				}
    			});
    	    }
    	});
    }
  });
  
	// ~打开编辑页面或者是新增数据页面
	function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
	    type: 1,
		    title: title,
		    skin: 'layui-layer-molv',
			shadeClose: false,
		    //maxmin: true,
			area: ['850px', '780px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		oprType = opr;
	}
	
	 //from表单监听提交
	 form.on('submit(sbwl_submit)', function(data){
		// 提交后台数据!
		shenbi.ajax({
			type:'post',
			url:oprType=='add'?'/salesman/number/setting/save':'/salesman/number/setting/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.close(layer_);
					if(oprType=="add"){
						layer.msg("成功"); 
					}else{
						layer.msg("已更新"); 
					}
					// 刷新数据表
					table_reload();
					// 重置表单
					$('#sbwl_from_id')[0].reset();
				}else{
					// ~异常信息
					layer.msg(resp.message);
				}
			}
		 });
		return false
	 });
  
});