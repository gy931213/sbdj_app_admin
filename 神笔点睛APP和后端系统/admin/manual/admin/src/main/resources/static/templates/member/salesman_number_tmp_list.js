layui.use(['table','form','upload'], function(){
    var table = layui.table;
    var form  = layui.form;

    // 快链字段设置样式
    var key = ['name','mobile'];

    // ~
    table.render({
        id:'table_list',
        method:'post',
        elem: '#table-data',
        url:'/salesman/number/tmp/list',
        toolbar: '#toolbar',
        title: '临时号主数据表',
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
            {field:'name', title:'姓名',align: 'center',event:'search-name'},
            {field:'mobile', title:'电话',align: 'center',event:'search-mobile'},
            {field:'imei', title:'串号', align: 'center', hide: true},
            {field:'requestCount', title:'查询次数', align: 'center', hide: true},
            {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {field:'updateTime', title:'更新时间', align: 'center', hide: true, templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.updateTime);
                }},
            {fixed: 'right', title:'操作',width:480,align: 'center',templet:function(res){
                    var _html = '';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-position"><i class="layui-icon">&#xe670;</i> 定位</a>';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-mail-list"><i class="layui-icon">&#xe615;</i> 通讯录</a>';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-contact-list"><i class="layui-icon">&#xe615;</i> 检测</a>';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-contact-depth-list"><i class="layui-icon">&#xe615;</i> 深度检测</a>';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-position-list"><i class="layui-icon">&#xe615;</i> 位置检测</a>';
                    /*_html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-del"><i class="layui-icon">&#xe640;</i> 删除</a>';*/
                    return _html;
                }}
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true
    });

    // 表头样式
    tableThStyle(key);

    // ~刷新数据列表
    function table_reload(){
        table.reload('table_list');
        // 表头样式
        tableThStyle(key);
    }

    // ~监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });

        // 表头样式
        tableThStyle(key);
        return false;
    });

    //头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'getCheckData':
                var data = checkStatus.data;
                layer.alert(JSON.stringify(data));
                break;
            case 'getCheckLength':
                var data = checkStatus.data;
                layer.msg('选中了：'+ data.length + ' 个');
                break;
            case 'isAll':
                layer.msg(checkStatus.isAll ? '全选': '未全选');
                break;
            case 'position':
                shenbi.ajax({
                    type:'post',
                    url:'/salesman/number/tmp/position',
                    data:{},
                    sendMsg:'定位中...',
                    success:function(resp){
                        showSalesmanNumberPosition('<i class="layui-icon">&#xe670;</i> 定位','#sbwl_position_tmpl');
                        var map = new AMap.Map('container', {
                            resizeEnable: true, //是否监控地图容器尺寸变化
                            zoom: 5
                        });
                        var markerList = [];
                        resp.data.forEach(function (item,index) {
                            // 创建一个 Marker 实例：
                            var marker = new AMap.Marker({
                                position: item.longLati.split(','), // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
                                title: item.name + ' ' + item.mobile + ' ' + item.createTime
                            });
                            markerList.push(marker);
                        });

                        // 将创建的点标记添加到已有的地图实例：
                        map.add(markerList);
                    }
                });
                break;
        };
    });

    // ~监听数据列表中的操作触发事件~
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        if(obj.event === 'sbwl-del'){
            // ~删除操作~
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/salesman/number/tmp/delete',
                        data:{id:data.id},
                        sendMsg:'删除中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已删除");
                            }else{
                                layer.msg("删除失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-mail-list'){
            // ~查看通讯录操作~
            if(data==="" || data.id==="") return false;
            tableSalesmanNumberPhones(data.id);
            showSalesmanNumberPhones('<i class="layui-icon">&#xe613;</i> 通讯录','#sbwl_phones_tmpl');

        }else if(obj.event === 'sbwl-contact-list'){
            // ~查看通讯录操作~
            if(data==="" || data.mobile==="") return false;
            tableSalesmanNumberPhonesCheck(data.mobile);
            showSalesmanNumberPhonesCheck('<i class="layui-icon">&#xe613;</i> 通讯录检测','#sbwl_phones_check_tmpl');
        } else if(obj.event === 'sbwl-contact-depth-list') {
            // ~查看通讯录操作~
            if(data==="" || data.mobile==="") return false;
            tableSalesmanNumberPhonesDepthCheck(data.mobile)
            showSalesmanNumberPhonesCheck('<i class="layui-icon">&#xe613;</i> 通讯录深度检测','#sbwl_phones_check_tmpl');
        } else if(obj.event === 'sbwl-position-list'){
            // ~查看距离操作~
            if(data==="" || data.mobile==="") return false;
            layer.prompt({
                value: '1',
                title: '请输入检测距离(km)'
            }, function(value, index, elem){
                // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除，
                if(value === "" || value ==null || isNaN(value)){
                    layer.msg('请输入数字');
                    return false;
                }
                layer.close(index);
                tableSalesmanNumberPositionCheck(data.mobile, value);
                showSalesmanNumberPhonesCheck('<i class="layui-icon">&#xe613;</i> 距离检测','#sbwl_phones_check_tmpl');
            });
        } else if(obj.event === 'search-name'){
            if(!dblclick()){
                return false;
            }
            // ~号主名称
            $("input[name='name']").val(data.name);
            table.reload('table_list',{
                where:{'name':data.name}
            });
        }else if(obj.event === 'search-mobile'){
            if(!dblclick()){
                return false;
            }
            // ~号主手机号
            $("input[name='mobile']").val(data.mobile);
            table.reload('table_list',{
                where:{'mobile':data.mobile}
            });
        } else if(obj.event === 'sbwl-position') {
            shenbi.ajax({
                type:'post',
                url:'/salesman/number/tmp/position/get',
                data:{id:data.id},
                sendMsg:'定位中...',
                success:function(resp){
                    showSalesmanNumberPosition('<i class="layui-icon">&#xe670;</i> 定位','#sbwl_position_tmpl');
                    var map = new AMap.Map('container', {
                        resizeEnable: true, //是否监控地图容器尺寸变化
                        zoom: 5
                    });
                    var markerList = [];
                    resp.data.forEach(function (item,index) {
                        // 创建一个 Marker 实例：
                        var marker = new AMap.Marker({
                            position: item.longLati.split(','), // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
                            title: item.name + ' ' + item.mobile + ' ' + item.createTime
                        });
                        markerList.push(marker);
                    });

                    // 将创建的点标记添加到已有的地图实例：
                    map.add(markerList);
                }
            });

        } else{
            // ~没有相关操作~
        }
        // 表头样式
        tableThStyle(key);
    });

    //~查看业务员号主的通讯录的弹出层~
    function showSalesmanNumberPhonesCheck(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
        layer.full(layer_);
    }

    // ~业务员号主的通讯录列表数据~
    function tableSalesmanNumberPhonesCheck(phone){
        // ~数据渲染
        table.render({
            id:'table_phones_check_list',
            elem: '#table_phones_check_data',
            url:'/salesman/number/tmp/check?phone='+phone,
            //toolbar: '#toolbar',
            title: '号主通讯录数据表',
            done: function (res, curr, count) {
                $("table").css("width", "100%");
            },
            cellMinWidth: 80,
            cols: [[
                {type:'checkbox',sort:true,align: 'center'},
                {type:'numbers',align: 'center'},
                {field:'otherName', title:'姓名',align: 'center'},
                {field:'otherPhone', title:'电话', align: 'center'},
                {field:'otherTotal', title:'联系人', align: 'center'},
                {field:'myTotal', title:'被查的联系人', align: 'center'},
                {field:'sameData', title:'相同数量', align: 'center'},
                {field:'percentage', title:'相似度', align: 'center'}
            ]],
            parseData: function(res){
                return {
                    "code": res.retCode, 		//解析接口状态
                    "msg": res.data.message, 	//解析提示文本
                    "count": res.data.total, 	//解析数据长度
                    "data": res.data 	//解析数据列表
                }
            },
            page: false
        });
    }

    // ~业务员号主的通讯录列表数据~
    function tableSalesmanNumberPhonesDepthCheck(phone){
        // ~数据渲染
        table.render({
            id:'table_phones_check_list',
            elem: '#table_phones_check_data',
            url:'/salesman/number/tmp/depth/check?phone='+phone,
            //toolbar: '#toolbar',
            title: '号主通讯录数据表',
            done: function (res, curr, count) {
                $("table").css("width", "100%");
            },
            cellMinWidth: 80,
            cols: [[
                {type:'checkbox',sort:true,align: 'center'},
                {type:'numbers',align: 'center'},
                {field:'otherName', title:'姓名',align: 'center'},
                {field:'otherPhone', title:'电话', align: 'center'},
                {field:'otherTotal', title:'联系人', align: 'center'},
                {field:'myTotal', title:'被查的联系人', align: 'center'},
                {field:'sameData', title:'相同数量', align: 'center'},
                {field:'percentage', title:'相似度', align: 'center'}
            ]],
            parseData: function(res){
                return {
                    "code": res.retCode, 		//解析接口状态
                    "msg": res.data.message, 	//解析提示文本
                    "count": res.data.total, 	//解析数据长度
                    "data": res.data 	//解析数据列表
                }
            },
            page: false
        });
    }

    // ~业务员号主的距离列表数据~
    function tableSalesmanNumberPositionCheck(phone, distance){
        // ~数据渲染
        table.render({
            id:'table_phones_check_list',
            elem: '#table_phones_check_data',
            url:'/salesman/number/tmp/position/check?phone='+phone+'&distance='+distance,
            //toolbar: '#toolbar',
            title: '号主距离数据表',
            done: function (res, curr, count) {
                $("table").css("width", "100%");
            },
            //cellMinWidth: 80,
            cols: [[
                //{type:'checkbox',sort:true,align: 'center'},
                //{type:'numbers',align: 'center'},
                {field:'name', title:'姓名',align: 'center'},
                {field:'phone', title:'电话', align: 'center'},
                {field:'distance', title:'距离(km)', align: 'center'},
                {field:'ip', title:'ip', align: 'center'},
                {field:'position', title:'经纬度', align: 'center'},
            ]],
            parseData: function(res){
                return {
                    "code": res.retCode, 		//解析接口状态
                    "msg": res.data.message, 	//解析提示文本
                    "count": res.data.total, 	//解析数据长度
                    "data": res.data 	//解析数据列表
                }
            },
            page: false
        });
    }

    //~查看业务员号主的通讯录的弹出层~
    function showSalesmanNumberPhones(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
        layer.full(layer_);
    }

    // ~业务员号主的通讯录列表数据~
    function tableSalesmanNumberPhones(id){
        // ~数据渲染
        table.render({
            id:'table_phones_list',
            elem: '#table_phones_data',
            url:'/salesman/number/tmp/phones?id='+id,
            //toolbar: '#toolbar',
            title: '号主通讯录数据表',
            done: function (res, curr, count) {
                $("table").css("width", "100%");
            },
            cellMinWidth: 80,
            cols: [[
                {type:'checkbox',sort:true,align: 'center'},
                {type:'numbers',align: 'center'},
                {field:'name', title:'姓名',align: 'center'},
                {field:'phone', title:'电话', align: 'center'},
                {field:'imei', title:'串号', align: 'center'},
                {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
                        return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                    }}
            ]],
            parseData: function(res){
                return {
                    "code": res.retCode, 		//解析接口状态
                    "msg": res.data.message, 	//解析提示文本
                    "count": res.data.total, 	//解析数据长度
                    "data": res.data.list 	//解析数据列表
                }
            },
            page: true
        });
    }

    //~查看临时号主定位的弹出层~
    function showSalesmanNumberPosition(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
        layer.full(layer_);
    }
});