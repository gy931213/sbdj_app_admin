
// 
layui.use(['table','form'], function(){
  var table = layui.table;
  var form  = layui.form;
  
  // ~操作类型[add|update],默认为add
  var oprType = 'add';
  var layer_ = ''; 
  
  // ~渲染业务员额度增长数据~
  table.render({
	id:'table_list',
	method:'post',
    elem: '#table-data',
	url:'/salesman/royalty/list',
	toolbar: '#toolbar',
	title: '业务员额度增长配置',
	cols: [[
		{type:'checkbox',sort:true,align: 'center'},
		  {type:'numbers',title:'序号',align: 'center',width:'5%'},	  
	  {field:'orgName', title:'所属机构'},
	  {field:'payment', title:'现付单'},
	  {field:'nextDay', title:'隔日单'},
	  {field:'browse', title:'浏览单'},
	  {field:'percentage', title:'提成'},
	  {field:'createTime', title:'创建时间',templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
	  }},
	  {fixed: 'right', title:'操作', width:200,align: 'center',templet:function(res){
		  var _html = '';
		  _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';
		  _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-del" style="background-color: #d9534f;"><i class="layui-icon">&#xe640;</i> 删除</a>';
		  return _html;
	  }}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return shenbi.returnParse(res);
    },
	page: true
  });
  
  // ~刷新数据列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  	//~渲染select选项 （所属机构）
	kits.select({
		elem:'#sbwl_org_select',
		url:'/sys/organization/select',
		value:'id',
		name:'name',
		form:form
	});
  
	// ~监听搜索按钮
   form.on('submit(sbwl_search)', function(data){
     table.reload('table_list',{
    	 page: {
             curr: 1 //重新从第 1 页开始
          },
          where:data.field
     });
     return false;
   });
  
  // ~工具栏事件~
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      // ~新增等级
      case 'sbwl-add-salesman-poyalty':
    	// ~渲染select选项 （机构）
		kits.select({
			elem:'.sbwl_org_select',
			url:'/sys/organization/select',
			value:'id',
			name:'name',
			form:form
		});
    	// ~新增页面
		showEditOrAdd('新增额度增长配置','#sbwl_tmpl','add');
      break;
      // ~删除等级
      case 'sbwl-del-salesman-poyalty':
    	    var data = checkStatus.data;
            layer.msg('选中了：'+ data.length + ' 个');
      break;
    };
  });
  
  // ~监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    if(obj.event === 'sbwl-del'){
    	 // ~删除操作~
    	layer.msg('确定要删除吗？', {
    	    btn: ['确定', '取消'],
    	    yes: function(index, layero){
    	    	shenbi.ajax({
    				type:'post',
    				url:'/salesman/royalty/delete',
    				data:{id:data.id},
    				sendMsg:'删除中...',
    				success:function(resp){
    					if(resp.retCode === 0){
    						layer.msg("已删除"); 
    					}else{
    						layer.msg("删除失败");
    					}
    					table_reload();
    				}
    			});
    	    }
    	});
    }else if(obj.event === 'sbwl-edit'){
    	// 编辑数据
    	
    	// ~表单赋值
		 form.val("sbwl_from", {
			  "id":data.id,
			  "payment":data.payment,
			  "nextDay":data.nextDay,
			  "browse":data.browse,
			  "percentage":data.percentage
		  });
		 
		 // ~渲染select选项 （机构）
		 kits.select({
			elem:'.sbwl_org_select',
			url:'/sys/organization/select',
			value:'id',
			name:'name',
			form:form,
			select:data.orgId,
		 });
    	
    	// ~编辑页面
		showEditOrAdd('编辑额度增长配置','#sbwl_tmpl','update');
    }
  });
  
	// ~打开编辑页面或者是新增数据页面
	function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
	    type: 1,
		    title: title,
		    skin: 'layui-layer-molv',
			//shadeClose: false,
		    //maxmin: true,
			area: ['650px', '470px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		oprType = opr;
	}
	
	
	// ~from表单数据验证
	form.verify({
		orgId:function(value){
			if(value === '0'){
				return "请选择所属机构";
			}
		}
	});
	
	 //from表单监听提交
	 form.on('submit(sbwl_submit)', function(data){
		  // 提交后台数据!
		  shenbi.ajax({
			type:'post',
			url:oprType=='add'?'/salesman/royalty/save':'/salesman/royalty/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.close(layer_);
					if(oprType=="add"){
						layer.msg("成功"); 
					}else{
						layer.msg("已更新"); 
					}
					// 刷新数据表
					table_reload();
					// 重置表单
					$('#sbwl_from_id')[0].reset();
				}else{
					// ~异常信息
					layer.msg(resp.message);
				}
			}
		 });
		return false
	 });
});