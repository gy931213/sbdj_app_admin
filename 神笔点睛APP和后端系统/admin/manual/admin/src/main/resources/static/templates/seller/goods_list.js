
layui.use(['table','form','upload'], function(){
    var upload = layui.upload;
    var table = layui.table;
    var form  = layui.form;
    var $=layui.jquery;
    
    // 统一初始化
    loadCheckAllClick(form);
    loadOnehecked(form);

    //操作类型[add|update],默认为add
    var oprType = 'add';
    var layer_ = '';

    // 快链字段设置样式
    var key = ['shopName','category'];

    // 表格数据渲染
    table.render({
        elem: '#table-data',
        url: '/seller/shop/goods/list',
        toolbar: '#toolbar',
        title: '商家店铺数据表',
        id: 'table_list',
        cols: [[
            {type:'checkbox',sort:true,align: 'center'},
            {type:'numbers',title:'序号',align: 'center',width:'5%'},
            {field:'platformName', title:'所属平台', align: 'center',width:'8%'},
            {field:'shopName', title:'所属店铺',align: 'center',width:'10%',event:'search-shopName'},
            {field:'title', title:'商品标题', align: 'center',templet:function(rse){
               return '<a href="'+rse.link+'" target="_blank">'+rse.title+'</a>';
            }},
            {field:'link', title:'商品链接', align: 'center'},
            /*{field:'flag', title:'旗帜', align: 'center',width:100,templet:function(rse){
    			return flagFunc(rse.flag);
    		}},*/
    		{field:'remark', title:'备注',width:100, align: 'center'},
            {field:'category', title:'商品类目',width:100, align: 'center',event:'search-category'},
            {field:'weight', title:'商品重量',width:100, align: 'center', hide:true},
            {field: 'isCard', title:'商品魔搜', width:98, align: 'center', templet:function (rse) {
                if (rse.isCard == 0) {
                    return '<input type="checkbox" data-id="'+rse.id+'" lay-skin="switch" id="isCard"  lay-filter="isCard" lay-text="开|关">';
                } else if (rse.isCard == 1) {
                    return '<input type="checkbox" data-id="'+rse.id+'" lay-skin="switch" id="isCard" checked lay-filter="isCard" lay-text="开|关">';
                } else {
                    return "<em>异常</em>"
                }
            }},
            /*{field:'isCoupon', title:'优惠卷', hide:true,width:80, align: 'center',templet:function(rse){
              if(rse.isCoupon === 1){
      			  return '<a class="layui-btn layui-btn-xs layui-btn-normal">有</a>';  
      		  }else if(rse.isCoupon === 0){
      			  return '<a class="layui-btn layui-btn-xs layui-btn-disabled">无</a>';
      		  }else{
      			  return '<a class="layui-btn layui-btn-xs layui-btn-disabled">未知</a>'; 
      		  }
            }},*/
            {field:'couponLink', title:'优惠卷链接',hide:true},
            {field:'createTime', title:'创建时间', hide:true,align: 'center',templet:function(rse){
                return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
            }},
            {fixed: 'right', title:'操作', toolbar: '#operation', width:280,align: 'center'}
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true
    });

   // 旗帜颜色
   function flagFunc(flag){
  	  //0(灰色), 1(红色), 2(黄色), 3(绿色), 4(蓝色), 5(粉红色)
  	  switch (flag) {
  	case 0:
  		return flag+"(灰色)";
  	case 1:
  		return  flag+"(红色)";
  	case 2:
  		return  flag+"(黄色)";
  	case 3:
  		return  flag+"(绿色)";
  	case 4:
  		return  flag+"(蓝色)";
  	case 5:
  		return  flag+"(粉红色)";
  	default:
  		if(null == flag || "" == flag){
  			return  "";	
  		}
  	  return  flag;
  	}
  }

    // 设置表头有快联的样式
    tableThStyle(key);

    // 开启或者关闭商品打标开关
    form.on('switch(isCard)', function(data){
        var id = $(this).data('id');
        var isCard = data.elem.checked?1:0;
        // 更新打标状态
        shenbi.ajax({
            type:'post',
            url:'/seller/shop/goods/update/marking',
            data:{id:id,isCard:isCard},
            sendMsg:'更新中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.msg(isCard === 0 ? "关闭打标" : "开启打标");
                    table_reload()
                }else{
                    layer.msg("失败");
                }
            }
        });
    });

    // 监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });
        // 设置表头有快联的样式
        tableThStyle(key);
        return false;
    });

    // 监听指定开关【商品快搜】
    form.on('switch(switchIsCard)', function(data){
        if(this.checked){
            $('input[name="isCard"]').val(1);
            layer.tips('温馨提示：开启商品打标！', data.othis);
        }else{
            $('input[name="isCard"]').val(0);
            layer.tips('温馨提示：关闭商品打标！', data.othis);
        }
    });
    
    // 监听指定开关【优惠卷】
    form.on('switch(switchIsCoupon)', function(data){
        if(this.checked){
            $('input[name="isCoupon"]').val(1);
            layer.tips('温馨提示 ：该商品有优惠卷！', data.othis);
            
            // 添加属性
	        $('input[name="couponLink"]').attr("lay-verify","required|isCouponLink");
	        $(".coupon-item-li").removeClass("hidden");
        }else{
            $('input[name="isCoupon"]').val(0);
            layer.tips('温馨提示：该商品没有优惠卷！', data.othis);
            
            // 删除属性
	        $('input[name="couponLink"]').removeAttr("lay-verify");    
	        $('.coupon-item-li').addClass("hidden");
        }
    });

    // 渲染select选项 (店铺)
    kits.select({
        elem:'#sbwl_selece_id',
        url:'/seller/shop/select',
        value:'id',
        name:'shopName',
        form:form
    });

    // 渲染select选项 (平台)
    kits.select({
        elem:'.selece_platform',
        url:'/config/platform/select',
        value:'id',
        name:'name',
        form:form
    });

    // 刷新列表
    function table_reload(){
        table.reload('table_list');
        // 设置表头有快联的样式
        tableThStyle(key);
    }

    // 重置
    form.on('submit(sbwl-reset)',function(){
        // 设置表头有快联的样式
        tableThStyle(key);
    })

    // 头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'sbwl_dels':
                var data = checkStatus.data;
                var ids = [];
                $.each(data,function(index,item){
                    ids.push(item.id);
                });

                if(ids.length<=0){
                    layer.msg("至少选择一项进行操作");
                    return false
                }

                layer.msg('确定要批量删除吗？', {
                    btn: ['确定', '取消'],
                    yes: function(index, layero){
                        var data = obj.data;
                        shenbi.ajax({
                            type:'post',
                            url:'/seller/shop/goods/deletes',
                            data:{ids:ids.join()},
                            sendMsg:'删除中...',
                            success:function(resp){
                                if(resp.retCode === 0){
                                    layer.msg("已删除");
                                }else{
                                    layer.msg("删除失败");
                                }
                                table_reload();
                            }
                        });
                    }
                });
                break;
            case 'sbwl_create':
                $("input[name=id]").val("");

                //渲染select选项 (店铺)
                kits.select({
                    elem:'.sbwl_selece',
                    url:'/seller/shop/select',
                    value:'id',
                    name:'shopName',
                    form:form,
                    option:'<option value="0"></option>'
                });
                
            	$("#couponLink_img").attr('src',"");
            	$("#sbwl-img").attr('src',"");
                // 数据、显示、隐藏
                $('.coupon-item-li').addClass("hidden");
                $("input[name='couponLink']").attr("lay-verify","")
                
                // 新增页面
                showEditOrAdd('新增商品','#sbwl_tmpl','add');
                break;
        };
    });
    
    // 监听行工具事件
    var $isCard = 0; // 是否进行打标
    var $is
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        // ~新增商品任务~
        if(obj.event === 'sbwl-add-task'){
        	
            // 是否进行打标
            $isCard = data.isCard;
            
            // 加载任务数据
            loadTaskData(form,data);
            
            // 商品主图
            $("#sbwl-img-url").attr('src',data.imgLink);
            // 优惠卷图片
            if(data.isCoupon === 1){
            	$("#couponLink").attr('src',data.couponLink);
            	$(".couponLink-img").removeClass("hidden");
            }else{
            	$("#couponLink").attr('src',"");
            	$(".couponLink-img").addClass("hidden");
            }
            
            // ~渲染任务类型
            kits.select({
                elem:'#sbwl-taskType-select',
                url:'/base/code',
                data:{code:'TASK_TYPE'},
                value:'id',
                name:'name',
                form:form,
                option:'<option value="0">请选择任务类型</option>'
            });

            // ~清除旧的数据
            $(".item-task-li").remove();

            // ~渲染新增商品任务窗口~
            showCreateTask('<i class="layui-icon">&#xe61f;</i> 任务','#sbwl_task_tmpl');
        }else if(obj.event === 'sbwl-set'){


        } else if(obj.event === 'sbwl-edit'){
        	// 清除img
        	$("#couponLink_img").attr('src',"");
        	$("#sbwl-img").attr('src',"");
        	
            // 设置表单数据!
            form.val("sbwl_from", {
                "id":data.id,
                "title":data.title,
                "link":data.link,
                "isCard":data.isCard,
                "isCoupon":data.isCoupon,
                "imgLink":data.imgLink,
                "couponLink":data.couponLink,
                "flag":data.flag,
                "remark":data.remark,
                "weight":data.weight,
                "keywords":data.keywords,
                "category":data.category
            });
            
           /* // 商品快搜
            $('input[name="isCard"]').val(data.isCard);
            // 优惠卷
            $('input[name="isCoupon"]').val(data.isCoupon);    */
            
            // if 是否快搜
            if(data.isCard === 1){
            	$("#switchIsCard").prop("checked",true);
            }else{
            	$("#switchIsCard").prop("checked",false);
            }
            
            // if 判断是否有优惠卷
            if(data.isCoupon === 1){
            	$("#switchIsCoupon").prop("checked",true);
            	$(".coupon-item-li").removeClass("hidden");
            	$("#couponLink_img").attr('src',data.couponLink);
            	$("input[name='couponLink']").attr("lay-verify","required|couponLink")
            }else{
            	$("#switchIsCoupon").prop("checked",false);
            	$('.coupon-item-li').addClass("hidden");
            	$("input[name='couponLink']").attr("lay-verify","")
            }
            
            // 重新渲染form
            form.render();

            // 渲染图片
            $("#sbwl-img").attr('src',data.imgLink);
            
            // 渲染select选项 （店铺）
            kits.select({
                elem:'.sbwl_selece',
                url:'/seller/shop/select',
                value:'id',
                name:'shopName',
                select:data.shopId,
                form:form
            });

            // 打开编辑页面!
            showEditOrAdd('编辑','#sbwl_tmpl','update');
        }else if(obj.event === 'sbwl-del') {
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function (index, layero) {
                    var data = obj.data;
                    shenbi.ajax({
                        type: 'post',
                        url: '/seller/shop/goods/delete',
                        data: {id: data.id},
                        sendMsg: '删除中...',
                        success: function (resp) {
                            if (resp.retCode === 0) {
                                layer.msg("已删除");
                            } else {
                                layer.msg("删除失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        } else if(obj.event === 'search-shopName'){
            if(!dblclick()){
                return false;
            }
            // 店铺名称
            $("input[name='shopName']").val(data.shopName);
            table.reload('table_list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'shopName':data.shopName}
            });
        } else if(obj.event === 'search-category'){
            if(!dblclick()){
                return false;
            }
            // 店铺名称
            $("input[name='category']").val(data.category);
            table.reload('table_list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'category':data.category}
            });
        }else{
            layer.msg("没有相关操作");
        }
        // 设置表头有快联的样式
        tableThStyle(key);
    });

    //打开编辑页面或者是新增数据页面
    function showEditOrAdd(title,doc,opr){
        layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['650px', '700px'],
            content: $(doc),
            cancel: function(){
                $('#sbwl_from_id')[0].reset();
            }
        });
        // 设置操作类型!
        oprType = opr;
    }

    //~添加商品任务的弹出层~
    var task_layer;
    function showCreateTask(title,doc){
        task_layer = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){
                // ~清除旧的数据
                $(".item-task-li").remove();
            }
        });
        layer.full(task_layer);
    }
    //from表单验证规则
    form.verify({
        shopId: function(value){
            if(value === '0'){
                return "请选择所属店铺";
            }
        },
        teskTypeId: function(value){
            if(value === '0'){
                return "请选择任务类型";
            }
        }
    });

    // ~新增商品from表单监听提交
    form.on('submit(sbwl_submit)', function(data){
        // 提交后台数据!
        shenbi.ajax({
            type:'post',
            url:oprType=='add'?'/seller/shop/goods/save':'/seller/shop/goods/update',
            data:data.field,
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.close(layer_);
                    if(oprType=="add"){
                        layer.msg("成功");
                    }else{
                        layer.msg("已更新");
                    }
                    // 刷新数据表
                    table_reload();
                    // 重置表单
                    $('#sbwl_from_id')[0].reset();
                }else{
                    // 错误提示
                    layer.msg(resp.message);
                }
            }
        });
        return false
    });

    // 图片上传
    var $showLoad = '';
    var uploadInst = upload.render({
        elem: '#uploaderInput', //绑定元素
        url: '/base/upload', //上传接口
        accept:'images',
        acceptMime:'image/*',
        before:function(){
            // 文件提交上传前的回调
            $showLoad = shenbi.showLoad("上传中...");
        },
        done: function(res){
            shenbi.closeLoad($showLoad);
            //上传完毕回调
            if(res.retCode === 0 && res.message!=""){
                $("input[name='imgLink']").val(res.message);
                $("#sbwl-img").attr('src',res.message)
            }
        },error: function(res){
            //请求异常回调
            console.log(res)
        }
    });
    
    // 商品优惠卷上传
    var $couponShow = '';
    var coupon = upload.render({
        elem: '#uploaderCoupon',
        url: '/base/upload',
        accept:'images',
        acceptMime:'image/*',
        before:function(){
        	$couponShow = shenbi.showLoad("上传中...");
        },
        done: function(res){
            shenbi.closeLoad($couponShow);
            if(res.retCode === 0 && res.message!=""){
                $("input[name='couponLink']").val(res.message);
                $("#couponLink_img").attr('src',res.message)
            }
        },error: function(res){
            console.log(res)
        }
    });
    
    // =======================================任务相关 start==================================
    // 全局变量 子任务编号
    var $taskId = 0;
    /**
     * 加载任务信息数据
     * @param form 表单对象
     * @param data 数据集
     * @return 
     */
    function loadTaskData(form,data){
    	// 设置任务表单数据
        form.val("sbwl_task_from", {
            "platformId":data.platformId,
            "platformName":data.platformName,
            "title":data.title,
            "link":data.link,
            "imgLink":data.imgLink,
            "goodsId":data.id,
            "sellerShopId":data.shopId,
            "orgId":data.orgId,
            "isCard":data.isCard,
            "goodsKeywords":data.keywords
        });
        
        // 请求服务器，获取历史的任务数据记录
        shenbi.ajax({
            type:'post',
            url:'/task/findTaskBySellerShopId',
            data:{
                sellerShopId:data.shopId,
                goodsId:data.id
            },
            sendMsg:'获取信息中...',
            success:function(resp){
            	// 去除checkbox选中状态	
            	clearCheckbox();
            	
            	// 数据null处理（if 处理数据为空）
            	//if(resp.data == null){
                    // if 判断是否开启，全选的选项
                    if($isCard === 0){
                    	$(".select-item").addClass("hidden");
                    	$(".select-all-item").addClass("hidden");
                    }else{
                    	$(".select-item").removeClass("hidden");
                    	$(".select-all-item").removeClass("hidden");
                    	// 选中全选的【是】
                    	$(".select-all-yes").prop("checked", true);
                    	
                    	// 选中第一个搜索词为：【是】
                    	$(".select-item-index0 .select-yes").prop("checked", true);
                    }
                    // 重新渲染form
                    form.render('checkbox');
            	//}
            	
                // 设置表单数据!
                form.val("sbwl_task_from", {
                    "showPrice": resp.data.showPrice,
                    "prefPrice":resp.data.prefPrice,
                    "realPrice":resp.data.realPrice,
                    "packages":resp.data.packages,
                    "sellerAsk":resp.data.sellerAsk,
                });

                clearKeyword();
                $taskId = resp.data.id;
            }
        });
    };

    $("#getKeyword").click(function () {
        // 清空数据
        clearKeyword();
        // 获取搜索词的数据（根据任务id获取对应的搜索词数据）
        shenbi.ajax({
            type:'post',
            url:'/task/findkeyword',
            data:{
                taskId: $taskId
                //status: resp.data.status
            },
            sendMsg:'填充信息中...',
            success:function(respa){
                $.each(respa.data , function (i,item) {
                    if (i > 0) {
                        var _html = '';
                        _html+='<div class="layui-input-block sbwl-task-keyword item-task-li">';
                        _html+='	<div class="layui-inline" style="width: 286px">';
                        _html+='		<input type="text" class="layui-input task-key" name="keyword" value="'+item.keyword+'" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
                        _html+='	</div>';
                        _html+='	<div class="layui-inline" style="width: 200px">';
                        _html+='		<input type="text" class="layui-input task-number" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
                        _html+='	</div>';
                        _html+='	<div class="layui-inline ss-btn sbwl-click-del"  style="width: 30px">';
                        _html+='		<a class="layui-btn layui-btn-danger"><i class="layui-icon">&#x1006;</i></a>';
                        _html+='	</div>';
                        // 加载选中的Checkbox
                        _html+= loadCheckedCheckboxHtml($isCard,item.isCard,false);
                        _html+='</div>';
                    }else{
                        // 赋值第一个搜索词!
                        $("input[name='keyword']:eq(0)").val(item.keyword);

                        // if 判断是否开启，全选的选项
                        if($isCard === 0){
                            $(".select-item").addClass("hidden");
                            $(".select-all-item").addClass("hidden");
                        }else{
                            $(".select-item").removeClass("hidden");
                            $(".select-all-item").removeClass("hidden");
                            // 选中全选的【是】
                            //$(".select-all-yes").prop("checked", true);

                            // 选中第一个搜索词为：【是】
                            //$(".select-item-index0 .select-yes").prop("checked", true);
                            if(item.isCard === 0){
                                $(".select-item-index0 .select-no").prop("checked", true);
                            }else if(item.isCard === 1){
                                $(".select-item-index0 .select-yes").prop("checked", true);
                            }else if(item.isCard === 2){
                                $(".select-item-index0 .select-auto").prop("checked", true);
                            }
                        }
                    }
                    // ~渲染搜索词~
                    $("#sbwl-list-div").after(_html);
                });
                // 进行渲染
                form.render();
            }
        });
    });

    $("#clearKeyword").click(function () {
        clearKeyword();
    });

    // 清理任务关键词
    function clearKeyword() {
        $(".item-task-li").remove();
        // 赋值第一个搜索词!
        $("input[name='keyword']:eq(0)").val('');
        // 赋值第一个数量!
        $("input[name='number']:eq(0)").val('');
        // ~渲染总任务数~
        $("input[name='taskTotal']").val(0);
    }

    // 文件上传
    var uploadInst = upload.render({
        elem: '#upload'//绑定元素
        ,accept: 'file'
        ,auto: false
        ,exts: 'xls|xlsx'
        ,choose: function(res){
            var load = layer.load(2,{ // 此处1没有意义，随便写个东西
                icon: 0, // 0~2 ,0比较好看
                shade: [0.5,'black'], // 黑色透明度0.5背景
                time: 3*1000
            });
            res.preview(function(index, file, result){
                xlsxUtils.import(file, function (w) {
                    var rs = xlsxUtils.getSheetByIndex(0);
                    var keys = ['关键词', '数量', 'keyword', 'number'];
                    rs.forEach(function (result, index) {
                        if (result[keys[0]] !== undefined) {
                            keywordInit(index, result[keys[0]], result[keys[1]], $isCard);
                        } else if (result[keys[2]] !== undefined) {
                            keywordInit(index, result[keys[2]], result[keys[3]], $isCard);
                        }
                    });
                    // ~获取到搜索词的任务数~
                    eachTaskNumberFunc();
                });
            });
            //清空 input file 值，以免删除后出现同名文件不可选
            uploadInst.config.elem.next()[0].value = '';
            //layer.close(load);
        }
    });

    function keywordInit(index, keyword, number, isCard) {
        if (index > 0) {
            var _html = '';
            _html+='<div class="layui-input-block sbwl-task-keyword item-task-li">';
            _html+='	<div class="layui-inline" style="width: 286px">';
            _html+='		<input type="text" class="layui-input task-key" name="keyword" value="'+keyword+'" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
            _html+='	</div>';
            _html+='	<div class="layui-inline" style="width: 200px">';
            _html+='		<input type="text" class="layui-input task-number" name="number" value="'+number+'" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
            _html+='	</div>';
            _html+='	<div class="layui-inline ss-btn sbwl-click-del"  style="width: 30px">';
            _html+='		<a class="layui-btn layui-btn-danger"><i class="layui-icon">&#x1006;</i></a>';
            _html+='	</div>';
            // 加载选中的Checkbox
            _html+= loadCheckedCheckboxHtml($isCard,isCard,false);
            _html+='</div>';
        }else{
            // 赋值第一个搜索词!
            $("input[name='keyword']:eq(0)").val(keyword);
            $("input[name='number']:eq(0)").val(number);

            // if 判断是否开启，全选的选项
            if($isCard == 0){
                $(".select-item").addClass("hidden");
                $(".select-all-item").addClass("hidden");
            }else{
                $(".select-item").removeClass("hidden");
                $(".select-all-item").removeClass("hidden");
                // 选中全选的【是】
                //$(".select-all-yes").prop("checked", true);

                // 选中第一个搜索词为：【是】
                //$(".select-item-index0 .select-yes").prop("checked", true);
                if(isCard === 0){
                    $(".select-item-index0 .select-no").prop("checked", true);
                    $(".select-item-index0 .select-yes").prop("checked", false);
                    $(".select-item-index0 .select-auto").prop("checked", false);
                }else if(isCard === 1){
                    $(".select-item-index0 .select-no").prop("checked", false);
                    $(".select-item-index0 .select-yes").prop("checked", true);
                    $(".select-item-index0 .select-auto").prop("checked", false);
                }else if(isCard === 2){
                    $(".select-item-index0 .select-no").prop("checked", false);
                    $(".select-item-index0 .select-yes").prop("checked", false);
                    $(".select-item-index0 .select-auto").prop("checked", true);
                }
            }
        }
        // ~渲染搜索词~
        $("#sbwl-list-div").after(_html);
        // 进行渲染
        form.render();
    }

    /**
     * 清理checkbox选中(第一个)
     */
    function clearCheckbox(){
		var item = $(".sbwl-task-keyword .select-item-index0").find("input[type='checkbox']");
		item.each(function () {
            if ($(this).prop("checked")) {
                $(this).prop("checked", false);
            } 
        })
        form.render('checkbox');
    }

    // ~添加关键词搜索输入框~
    $("#js-click-btn").on('click',function(){
        var _html = '';
        _html+='<div class="layui-input-block sbwl-task-keyword item-task-li">';
        _html+='	<div class="layui-inline" style="width: 286px">';
        _html+='		<input type="text" class="layui-input task-key" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
        _html+='	</div>';
        _html+='	<div class="layui-inline" style="width: 200px">';
        _html+='		<input type="text" class="layui-input task-number" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
        _html+='	</div>';
        _html+='	<div class="layui-inline ss-btn sbwl-click-del"  style="width: 30px">';
        _html+='		<a class="layui-btn layui-btn-danger"><i class="layui-icon">&#x1006;</i></a>';
        _html+='	</div>';
        // 判断是否要进行打标
        _html+= loadOneCheckedCheckboxHtml($isCard);
        _html+='</div>';

        // ~渲染搜索词~
        $("#sbwl-list-div").after(_html);
        // 重新渲染form
        form.render();
    });

    // ~删除当前点击的搜索词
    $(document).on('click','.sbwl-click-del',function(){
        // ~删除
        $(this).parent().remove();
        // ~获取到搜索词的任务数~
        eachTaskNumberFunc();
    });

    // ~监听任务数输入的事件~
    $(document).bind('input','.task-number',function(){
        // ~遍历搜索词的任务数~
        eachTaskNumberFunc();
    });

    // ~遍历搜索词的任务数的函数~
    function eachTaskNumberFunc(){
        // ~获取到搜索词的任务数~
        var taskTotal = 0; // ~总任务数
        $("input[name='number']").each(function(i,item){
            if(item.value!="" && item.value!=null){
                taskTotal+=parseInt(item.value);
            }
        });
        // ~渲染总任务数~
        $("input[name='taskTotal']").val(taskTotal);
    }
   
    // ~新增商品任务form表单~
    form.on('submit(sbwl_task_submit)', function(data){
        var data = data.field;
        // 获取到是否打标标记，[{0：否},{1：是}] 
        var isCard = $isCard;
        if(isCard == 1 && !isCheckeds()){
        	return false;
        }
        // 设置是否打标
        //data.isCard = isCard;
        console.log(JSON.stringify(data))
        // 搜索词
        data.taskKeyword = loadTaskKeyword(isCard);
        
        // ~提交后台数据
        shenbi.ajax({
            type:'post',
            url:'/task/save',
            data:data,
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode==0){
                    layer.msg("成功");
                    layer.close(task_layer)
                    // 重置表单
                    $('#sbwl_task_id')[0].reset();
                }else{
                    // 错误提示
                    layer.msg(resp.message);
                    layer.close(task_layer)
                }
            }
        });
        return false
    });


    // ~新增商品任务form表单~
    form.on('submit(sbwl_task_template_submit)', function(data){
        var data = data.field;
        // 获取到是否打标标记，[{0：否},{1：是}]
        var isCard = $isCard;
        if(isCard == 1 && !isCheckeds()){
            return false;
        }
        // 设置是否打标
        //data.isCard = isCard;
        console.log(JSON.stringify(data));
        // 搜索词
        data.taskKeyword = loadTaskKeyword(isCard);

        // ~提交后台数据
        shenbi.ajax({
            type:'post',
            url:'/task/template/save',
            data:data,
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode===0){
                    layer.msg("成功");
                    layer.close(task_layer)
                    // 重置表单
                    $('#sbwl_task_id')[0].reset();
                }else{
                    // 错误提示
                    layer.msg(resp.message);
                    layer.close(task_layer)
                }
            }
        });
        return false
    });

    // 监听输入商品ID
    /*$("#shopId").blur(function () {
       getShopLink();
    });*/

    $("#shopId").on('input', function () {
        getShopLink();
    });

    form.on('select(shopType)', function () {
        getShopLink();
        var link = $("#link").val();
        // ~提交后台数据
        shenbi.ajax({
            type:'get',
            url:'/seller/shop/goods/info',
            data:{link:link},
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode===0){
                    if (resp.data.shop === null) {
                        // 表单赋值
                        form.val("sbwl_from", {
                            "imgLink": '',
                            "title": ''
                        });
                        return;
                    }
                    // 表单赋值
                    form.val("sbwl_from", {
                        "imgLink": resp.data.shop.link,
                        "title": resp.data.shop.title
                    });
                    $("#sbwl-img").attr("src", resp.data.shop.link);
                }else{
                    // 错误提示
                    layer.msg(resp.message);
                    form.val("sbwl_from", {
                        "imgLink": '',
                        "title": ''
                    });
                    $("#sbwl-img").attr("src", '');
                }
            }
        });
    });

    function getShopLink() {
        var type = $("#shopType").val();
        if (type === 'NO') {return;}
        var value = $("#shopId").val();
        if (value === undefined || value === null || /^\s*$/.test(value)) {
            $("#link").val('');
            return;
        }
        var result = '';
        switch (type) {
            case 'TB':
                result = 'https://item.taobao.com/item.htm?id='+value;
                break;
            case 'TM':
                result = 'https://detail.tmall.com/item.htm?id='+value;
                break;
            case 'JD':
                result = 'https://item.jd.com/'+value+'.html';
                break;
        }
        $("#link").val(result);
    }
});