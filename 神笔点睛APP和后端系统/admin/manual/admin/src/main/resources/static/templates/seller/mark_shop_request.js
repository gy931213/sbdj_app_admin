layui.use(['form', 'layedit', 'laydate'], function(){
    var form = layui.form,
        layer = layui.layer;

    //监听提交
    form.on('submit(lay-submit)', function(data){
        ajaxRequst(data);
        return false;
    });

    // ajax
    function ajaxRequst(data){
        console.log(data)
        var $shop_id = $("input[name='shop_id']").val();
        var $keyword = $("input[name='keywords']").val();
        var $wws = $("input[name='wws']").val();

        // 加载层~
        var layerMsg = layer.load(2,{ // 此处1没有意义，随便写个东西
            icon: 0, // 0~2 ,0比较好看
            shade: [0.5,'black'] // 黑色透明度0.5背景
        });

        $.post('/marking/goods/request',data.field,function(resp){
            layer.close(layerMsg);
            var $resp;
            if(resp.retCode > 0){
                $resp = resp.message;
            }else{
                $resp = JSON.stringify(resp.data);
            }
            //表单初始赋值
            form.val('example', {
                "respTextarea":$resp
            })
        })
    }
});