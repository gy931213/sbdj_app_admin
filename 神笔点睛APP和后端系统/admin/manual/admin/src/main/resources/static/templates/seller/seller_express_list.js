layui.use(['table','laydate','form','upload'], function(){
    var table = layui.table;
    var laydate = layui.laydate;
    var form  = layui.form;
    var upload = layui.upload;

    // 状态
    function getStatus(key){
        switch (key) {
            case "0":
                //0：未填充
                return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #c58000;">未填充</a>';
                break;
            case "1":
                //1：未打单
                return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #c50b1d;">未打单</a>';
                break;
            case "2":
                //2：已打单
                return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #60c560;">已打单</a>';
                break;
            default:
                return "默认";
                break;
        }
    }

    // 订单状态
    function orderStatus(key){
        switch (key) {
            case 0:
                //0：待付款
                return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #d9534f;">待付款</a>';
                break;
            case 1:
                //1：待发货
                return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #008ac5;">待发货</a>';
                break;
            case 2:
                //2：待收货
                return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #c58600;">待收货</a>';
                break;
            case 3:
                //3：已完成
                return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #60c560;">已完成</a>';
                break;
            case -1:
                //-1：已关闭
                return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #d9534f;">已关闭</a>';
                break;
            default:
                return "默认";
                break;
        }
    }

    //渲染select选项
    kits.select({
        elem:'#sbwl_org_select_1',
        url:'/sys/organization/select',
        value:'id',
        name:'name',
        form:form
    });

    //渲染select选项
    kits.select({
        elem:'#sbwl_expresscom_select',
        url:'/seller/express/expresscom',
        value:'expresscom',
        name:'expresscom',
        form:form
    });


    //开始日期
    laydate.render({
        elem: '#beginDate',
        type: 'datetime'
    });
    //结束日期
    laydate.render({
        elem: '#endDate',
        type: 'datetime'
    });

    // 表格
    table.render({
        id:'task-table-list',
        elem:'#table-data',
        url:'/seller/express/list',
        toolbar: '#toolbar',
        title: '菜鸟面单',
        where: {status:"0"},
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
            {field:'expresssn', title:'订单编号'},
            {field:'receiverName', title:'收件人'},
            {field:'lll', title:'固话'},
            {field:'receiverMobile', title:'手机'},
            {field:'address', title:'地址'},
            {field:'title', title:'发货信息'},
            {field:'sellerName', title:'发件人'},
            {field:'sellerMobile', title:'发件人电话'},
            {field:'sellerAddress', title:'发件人地址'},
            {field:'weight', title:'备注', align: 'center'},
            {field:'lll', title:'代收金额', align: 'center'},
            {field:'lll', title:'保价金额', align: 'center'},
            {field:'lll', title:'业务类型', align: 'center'},
            {field:'remark', title:'商家备注', align: 'center',hide:true},
            {field:'expresscom', title:'物流公司',hide:true},
            {field:'orgName', title:'机构名',hide:true},
            {field:'shopName', title:'店铺名称',hide:true},
            {field:'orderNum', title:'平台单号',hide:true},
            {field:'auditorName', title:'操作人',hide:true},
            {field:'orderStatus', title:'订单状态', width:100,align: 'center',hide:true, templet:function(rse){
                    return orderStatus(rse.orderStatus);
                }},
            {field:'createTime', title:'领取时间',align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {field:'deliveryTime', title:'传送时间',align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.deliveryTime);
                }},
            {field:'status', title:'发货状态', width:100,align: 'center', templet:function(rse){
                    return getStatus(rse.status);
                }}
        ]],
        limit:30,
        limits:[10,30,50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true
    });

    // ~监听搜索按钮
    form.on('submit(sbwl-seller-express-search)', function(data){
        table.reload('task-table-list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });

        return false;
    });

    // 重置
    form.on('submit(sbwl-reset)',function(){
        shenbi.select({
            elem:'#taskStatus',
            data:statuss,
            form:form,
        });
    })

    // ~刷新列表
    function table_reload(){
        table.reload('task-table-list');
    }

    // ~头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'getCheckData':
                var data = checkStatus.data;
                layer.alert(JSON.stringify(data));
                break;
            case 'getCheckLength':
                var data = checkStatus.data;
                layer.msg('选中了：'+ data.length + ' 个');
                break;
            case 'isAll':
                layer.msg(checkStatus.isAll ? '全选': '未全选');
                break;
            case 'orders':
                // 批量选中要对单的任务数据!
                var data = checkStatus.data;
                if(data.length < 1){
                    layer.msg("必须选中1项进行操作！");
                    return false;
                }
                // 获取所选中的数据
                var ids = [];
                $.each(data,function(i,item){
                    ids.push(item.id);
                });
                // 批量拉取
                layer.msg('确定批量拉取吗？', {
                    btn: ['确定', '取消'],
                    yes: function(index, layero){
                        shenbi.ajax({
                            type:'POST',
                            url:'/seller/express/update',
                            data:{ids:ids.join()},
                            sendMsg:'拉取中...',
                            success:function(resp){
                                if(resp.retCode === 0){
                                    layer.msg("拉取完成",{time:5000});
                                    table_reload();
                                }else{
                                    layer.msg("拉取失败");
                                }
                                // ~刷新数据
                                table_reload();
                            }
                        });
                    }
                });
                break;
            case 'check':
                break;
        };
    });

    // 订单上传
    var $showLoad = '';
    var uploadInst = upload.render({
        elem: '#uploaderInput', 	//绑定元素
        url: '/seller/express/callback',  //上传接口
        type: "file",
        accept: 'file',
        before:function(){
            // 文件提交上传前的回调
            $showLoad = shenbi.showLoad("标记中...");
        },
        done: function(res){
            shenbi.closeLoad($showLoad);
            //上传完毕回调
            if(res.retCode === 0){
                layer.msg("已标记",{icon:1,time:3000},function(){
                    //window.location.reload();
                    // ~刷新数据
                    table_reload();
                });
            }else{
                layer.msg(res.message,{icon:5,time:3000},function(){
                    //window.location.reload();
                    // ~刷新数据
                    table_reload();
                });
            }
        },error: function(res){
            //请求异常回调
            layer.msg("上传文件异常",{icon:5,time:3000},function(){
                //window.location.reload()
            });
        }
    });

});