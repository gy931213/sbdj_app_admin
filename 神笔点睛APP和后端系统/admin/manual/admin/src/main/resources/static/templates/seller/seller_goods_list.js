
layui.use(['table','form','upload'], function(){
	var upload = layui.upload;
	var table = layui.table;
	var form  = layui.form;
	var $=layui.jquery;
  
	//操作类型[add|update],默认为add
	var oprType = 'add';
	var layer_ = ''; 
  
  // 表格数据渲染
  table.render({
    elem: '#table-data',
	url: '/goods/seller/goods/list',
	toolbar: '#toolbar',
	title: '商家店铺数据表',
	id: 'table_list',
	cols: [[
		{type:'checkbox',sort:true,align: 'center'},
		  {type:'numbers',title:'序号',align: 'center',width:'5%'},
		{field:'platformName', title:'所属平台',align: 'center'},
		{field:'shopName', title:'所属店铺',align: 'center'},
		{field:'title', title:'商品标题', align: 'center',templet:function(rse){
			return '<a href="'+rse.link+'" target="_blank">'+rse.title+'</a>';
		}},
		{field:'link', title:'商品链接', align: 'center',templet:function(rse){
			return '<a href="'+rse.link+'" target="_blank">'+rse.link+'</a>';
		}},
		{field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
			  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
		}},
		{fixed: 'right', title:'操作', toolbar: '#operation', width:280,align: 'center'}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return shenbi.returnParse(res);
    },
	page: true
  });
  
  //监听搜索按钮
  form.on('submit(sbwl_search)', function(data){
    table.reload('table_list',{
    	page: {
            curr: 1 //重新从第 1 页开始
         },
         where:data.field
    });
    return false;
  });

  
  //刷新列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
    	case 'sbwl_dels':
		    var data = checkStatus.data;
		    var ids = [];
		    $.each(data,function(index,item){
			  ids.push(item.id);
		    });
		    
		    if(ids.length<=0){
		    	layer.msg("至少选择一项进行操作"); 
		    	return false
		    }
		    
  	    	layer.msg('确定要批量删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/goods/deletes',
	    				data:{ids:ids.join()},
	    				sendMsg:'删除中...',
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
    		break;
	    case 'sbwl_create':
	    	 $("input[name=id]").val("");
	    	 
	    	 //渲染select选项 (店铺)
	    	 kits.select({
	    		 elem:'.sbwl_selece',
	    		 url:'/seller/shop/selectv',
	    		 value:'id',
	    		 name:'shopName',
	    		 form:form
	    	 });
	    	 
		  	// 新增页面
			showEditOrAdd('新增商品','#sbwl_tmpl','add');
	    break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
	    var data = obj.data;
	    // ~新增商品任务~
	    if(obj.event === 'sbwl-add-task'){
	    	 // ~设置表单数据!
	   	    form.val("sbwl_task_from", {
	   	      "platformId":data.platformId,
	   	      "platformName":data.platformName,
			  "title":data.title,
			  "link":data.link,
			  "imgLink":data.imgLink,
			  "goodsId":data.id,
			  "sellerShopId":data.shopId,
			  "orgId":data.orgId,
			});

            shenbi.ajax({
                type:'post',
                url:'/task/findTaskBySellerShopId',
                data:{
                	sellerShopId:data.shopId,
                	goodsId:data.id
                },
                sendMsg:'获取信息中...',
                success:function(resp){
                    // ~设置表单数据!
                    form.val("sbwl_task_from", {
                        "showPrice": resp.data.showPrice,
                        "prefPrice":resp.data.prefPrice,
                        "realPrice":resp.data.realPrice,
                        "packages":resp.data.packages,
                        "sellerAsk":resp.data.sellerAsk,
                    });

                    shenbi.ajax({
                        type:'post',
                        url:'/task/findkeyword',
                        data:{
                            taskId: resp.data.id
                            //status: resp.data.status
                        },
                        sendMsg:'填充信息中...',
                        success:function(respa){
                            $.each(respa.data , function (i,item) {
                                if (i > 0) {
                                    var _html = '';
                                    _html+='<div class="layui-input-block sbwl-task-keyword item-task-li">';
                                    _html+='	<div class="layui-inline" style="width: 286px">';
                                    _html+='		<input type="text" class="layui-input task-key" name="keyword" value="'+item.keyword+'" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
                                    _html+='	</div>';
                                    _html+='	<div class="layui-inline" style="width: 200px">';
                                    _html+='		<input type="text" class="layui-input task-number" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
                                    _html+='	</div>';
                                    _html+='	<div class="layui-inline ss-btn sbwl-click-del"  style="width: 30px">';
                                    _html+='		<a class="layui-btn layui-btn-danger"><i class="layui-icon">&#x1006;</i></a>';
                                    _html+='	</div>';
                                    _html+='</div>';
                                }else{
                               	 	$("input[name='keyword']:eq(0)").val(item.keyword);
                                }
                                // ~渲染搜索词~
                                $("#sbwl-list-div").after(_html);
                            })
                        }
                    });
                }
            });

	   	    $("#sbwl-img-url").attr('src',data.imgLink);
	   	     // ~渲染任务类型
		   	 kits.select({
				 elem:'#sbwl-taskType-select',
				 url:'/base/code',
				 data:{code:'TASK_TYPE'},
				 value:'id',
	    		 name:'name',
	    		 form:form,
	    		 option:'<option value="0">请选择任务类型</option>'
			 });

	   	 	// ~清除旧的数据
		   	$(".item-task-li").remove();

	    	// ~渲染新增商品任务窗口~
	    	showCreateTask('<i class="layui-icon">&#xe61f;</i> 任务','#sbwl_task_tmpl');
	    }else if(obj.event === 'sbwl-set'){
		     
	    } else if(obj.event === 'sbwl-edit'){
	    	 // 设置表单数据!
	   	    form.val("sbwl_from", {
	   	      "id":data.id,
	   	      "title":data.title,	
			  "link":data.link,
			  "imgLink":data.imgLink
			});
	   	    
	   	    // 渲染图片
	   	    $("#sbwl-img").attr('src',data.imgLink)
	   	    
	   	     // 渲染select选项 （店铺）
	    	 kits.select({
	    		 elem:'.sbwl_selece',
	    		 url:'/seller/shop/selectv',
	    		 value:'id',
	    		 name:'shopName',
	    		 select:data.shopId,
	    		 form:form
	    	 });
	    	 
	    	 // 打开编辑页面!
			showEditOrAdd('编辑','#sbwl_tmpl','update');
	    }else if(obj.event === 'sbwl-del'){
	    	layer.msg('确定要删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/goods/delete',
	    				data:{id:data.id},
	    				sendMsg:'删除中...',
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
	    }else{
	    	layer.msg("没有相关操作");
	    }
  	});
  
  	//打开编辑页面或者是新增数据页面
  	function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
	        type: 1,
	        title: title,
	        skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['650px', '500px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		oprType = opr;
  	}
  	
   //~添加商品任务的弹出层~
  	var task_layer;
	function showCreateTask(title,doc){
		task_layer = layer.open({
			type: 1,
		    title: title,
		    skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['620px', '730px'],
			content: $(doc),
			cancel: function(){
				 // ~清除旧的数据
			   	$(".item-task-li").remove();
			}
		});
		layer.full(task_layer);
	}
	  	//from表单验证规则
		form.verify({
			shopId: function(value){
				if(value === '0'){
					return "请选择所属店铺";
				}
			},
			teskTypeId: function(value){
				if(value === '0'){
					return "请选择任务类型";
				}
			}
		});

	  // ~新增商品from表单监听提交
	  form.on('submit(sbwl_submit)', function(data){
	   // 提交后台数据!
		shenbi.ajax({
			type:'post',
			url:oprType=='add'?'/goods/save':'/goods/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.close(layer_);
					if(oprType=="add"){
						layer.msg("成功"); 
					}else{
						layer.msg("已更新"); 
					}
					// 刷新数据表
					table_reload();
					// 重置表单
					$('#sbwl_from_id')[0].reset();
				}else{
					// 错误提示
					layer.msg(resp.message); 
				}
			}
		});
		return false
	  });
	  
	  // 图片上传
	  var $showLoad = '';
	  var uploadInst = upload.render({
		elem: '#uploaderInput', //绑定元素
		url: '/base/upload', //上传接口
		accept:'images',
		acceptMime:'image/*',
		before:function(){
			// 文件提交上传前的回调
			$showLoad = shenbi.showLoad("上传中...");
		},
		done: function(res){
			shenbi.closeLoad($showLoad);
			//上传完毕回调
			if(res.retCode === 0 && res.message!=""){
				console.log(res.message);
				$("input[name='imgLink']").val(res.message);
				$("#sbwl-img").attr('src',res.message)
			}
		},error: function(res){
		  //请求异常回调
			console.log(res)
		}
	  });
	  
	  // ~添加关键词搜索输入框~
	  $("#js-click-btn").on('click',function(){
		  var _html = '';
		  _html+='<div class="layui-input-block sbwl-task-keyword item-task-li">';
		  _html+='	<div class="layui-inline" style="width: 286px">';
		  _html+='		<input type="text" class="layui-input task-key" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
		  _html+='	</div>';
		  _html+='	<div class="layui-inline" style="width: 200px">';
		  _html+='		<input type="text" class="layui-input task-number" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
		  _html+='	</div>';
		  _html+='	<div class="layui-inline ss-btn sbwl-click-del"  style="width: 30px">';
		  _html+='		<a class="layui-btn layui-btn-danger"><i class="layui-icon">&#x1006;</i></a>';
		  _html+='	</div>';
		  _html+='</div>';
		  
		  // ~渲染搜索词~
		  $("#sbwl-list-div").after(_html);
	  });
	  
	  // ~删除当前点击的搜索词
	  $(document).on('click','.sbwl-click-del',function(){
		  // ~删除
		  $(this).parent().remove();
		  // ~获取到搜索词的任务数~
		  eachTaskNumberFunc();
	  });
	  
	  // ~监听任务数输入的事件~
	  $(document).bind('input','.task-number',function(){
		  // ~遍历搜索词的任务数~
		  eachTaskNumberFunc();
	  });
	  
	  // ~遍历搜索词的任务数的函数~
	  function eachTaskNumberFunc(){
		  // ~获取到搜索词的任务数~
		  var taskTotal = 0; // ~总任务数
		  $("input[name='number']").each(function(i,item){
		    if(item.value!="" && item.value!=null){
		    	taskTotal+=parseInt(item.value);	
		    }
		  });
		  // ~渲染总任务数~
		  $("input[name='taskTotal']").val(taskTotal);
	  }
	  
	  // ~遍历搜索词和搜索词的任务数的函数~
	  function eachTaskKeyword(){
		  var array = [];
		   // ~
		  $(".sbwl-task-keyword").each(function(i,item){
			 var map = {};
			 map.keyword = $(this).find('input[name="keyword"]').val();
			 map.number =$(this).find('input[name="number"]').val();
			 array.push(map);
			 map = {};
		  });
		  return JSON.stringify(array);
	  }
	  
	  // ~新增商品任务form表单~
	  form.on('submit(sbwl_task_submit)', function(data){
	  	var data = data.field;
	  		data.taskKeyword = eachTaskKeyword();
	    // ~提交后台数据
		 shenbi.ajax({
			type:'post',
			url:'/task/save',
			data:data,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode==0){
					layer.msg("成功"); 
					layer.close(task_layer)
					// 重置表单
					$('#sbwl_task_id')[0].reset();
				}else{
					// 错误提示
					layer.msg(resp.message); 
					layer.close(task_layer)
				}
			}
		});
		return false
	  });
	  
});