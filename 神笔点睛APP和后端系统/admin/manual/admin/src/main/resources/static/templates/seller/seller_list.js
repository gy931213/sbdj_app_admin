
layui.use(['table','form'], function(){
    var formSelects = layui.formSelects;
    var table = layui.table;
    var form  = layui.form;
    var $=layui.jquery;

    //操作类型[add|update],默认为add
    var oprType = 'add';
    var layer_ = '';

    table.render({
        elem: '#table-data',
        url:'/seller/list',
        toolbar: '#toolbar',
        title: '商家数据表',
        id:'table_list',
        cols: [[
            {type:'checkbox',sort:true,align: 'center'},
            {type:'numbers',title:'序号',align: 'center',width:'5%'},
            /*{field:'id', title:'序号', width:80, fixed: 'left', unresize: true,align: 'center'},*/
            {field:'orgName', title:'所属组织'},
            /*{field:'bankName', title:'所属银行卡'},*/
            {field:'referrer', title:'推荐人', hide:true},
            {field:'name', title:'用户名称'},
            {field:'mobile', title:'用户账号'},
            {field:'gender', title:'性别',templet:function(rse){
                    if(rse.gender==0){
                        return "男";
                    }else if(rse.gender==1){
                        return "女";
                    }else{
                        return "保密";
                    }
                }},
            {field:'createTime', title:'创建时间', align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.lastLoginTime);
                }},
            {field:'lastLoginTime', title:'最后登录时间',hide:true,align: 'center',templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.lastLoginTime);
                }},
            {field:'ip', title:'IP地址', hide:true,align: 'center'},
            {field:'status', title:'状态', align: 'center',templet:function(rse){
                    if(rse.status === 'A'){
                        return '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="sbwl-prohibit">已启用</a>';
                    }else{
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled" lay-event="sbwl-enable">已禁用</a>';
                    }
                }},
            {fixed: 'right', title:'操作', toolbar: '#operation', width:180,align: 'center'}
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true
    });

    //监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        //layer.msg(JSON.stringify(data.field));
        table.reload('table_list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });
        return false;
    });

    //渲染select选项
    kits.select({
        elem:'#sbwl_org_select_1',
        url:'/sys/organization/select',
        value:'id',
        name:'name',
        form:form
    });

    // 监听select选中的事件~
    form.on('select(sbwl_org_select)', function(data){
        // 角色
        kits.selects({
            elem:'selectId',
            url:'/sys/role/select',
            data:{orgId:data.value},
            value:'id',
            name:'name',
            select:formSelects
        });

        // 银行卡
        kits.selects({
            elem:'selects_bank_id',
            url:'/config/bank/selects',
            data:{orgId:data.value},
            value:'id',
            name:'name',
            select:formSelects
        });
    });

    // 刷新列表
    function table_reload(){
        table.reload('table_list');
    }

    //头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'sbwl_dels':
                var data = checkStatus.data;
                var ids = [];
                $.each(data,function(index,item){
                    ids.push(item.id);
                });

                if(ids.length<=0){
                    layer.msg("至少选择一项进行操作");
                    return false
                }

                layer.msg('确定要批量删除吗？', {
                    btn: ['确定', '取消'],
                    yes: function(index, layero){
                        var data = obj.data;
                        shenbi.ajax({
                            type:'post',
                            url:'/sys/admin/deletes',
                            data:{ids:ids.join()},
                            sendMsg:'删除中...',
                            success:function(resp){
                                if(resp.retCode === 0){
                                    layer.msg("已删除");
                                }else{
                                    layer.msg("删除失败");
                                }
                                table_reload();
                            }
                        });
                    }
                });

                break;
            case 'sbwl_create':
                $("input[name=id]").val("");
                $("input[name=pwd]").attr("readOnly",false);
                $('#sbwl_from_id')[0].reset();
                //渲染select选项
                kits.select({
                    elem:'.sbwl_org_select',
                    url:'/sys/organization/select',
                    value:'id',
                    name:'name',
                    form:form
                });

                $("input[name=id]").val("");
                $("input[name=pwd]").attr("lay-verify","required|pwd");
                $("input[name=pwd]").attr("placeholder","请输入新密码");

                // 新增页面
                showEditOrAdd('新增商家','#sbwl_tmpl','add');
                break;
        };
    });

    //监听行工具事件
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        console.log(obj)
        if(obj.event === 'sbwl-set'){

        } else if(obj.event === 'sbwl-edit'){
            // 设置表单数据!
            form.val("sbwl_from", {
                "id":data.id,
                "name":data.name,
                "mobile":data.mobile,
                "gender":data.gender,
                "referrer":data.referrer
            });

            // 渲染select选项
            kits.select({
                elem:'.sbwl_org_select',
                url:'/sys/organization/select',
                value:'id',
                name:'name',
                select:data.orgId,
                form:form
            });

            // 渲染机构对应的角色数据
            kits.selects({
                elem:'selectId',
                url:'/sys/role/select',
                data:{orgId:data.orgId},
                value:'id',
                name:'name',
                select:formSelects,
                selectVal:data.roleIds
            });

            // 银行卡
            kits.selects({
                elem:'selects_bank_id',
                url:'/config/bank/selects',
                data:{orgId:data.orgId},
                value:'id',
                name:'name',
                select:formSelects,
                selectVal:data.bankIds
            });

            //setSelects(data.id);
            $("input[name=pwd]").val("");
            $("input[name=pwd]").attr("lay-verify","");
            $("input[name=pwd]").attr("placeholder","更新密码，请输入新密码即可，默认为空，填入则为更新旧密码");

            // 打开编辑页面!
            showEditOrAdd('编辑','#sbwl_tmpl','update');
        }else if(obj.event === 'sbwl-del'){
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    var data = obj.data;
                    shenbi.ajax({
                        type:'post',
                        url:'/seller/delete',
                        data:{id:data.id},
                        sendMsg:'删除中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已删除");
                            }else{
                                layer.msg("删除失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else  if(obj.event === 'sbwl-prohibit'){
            // ~禁用操作~
            layer.msg('确定要禁用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/seller/prohibit',
                        data:{
                            id:data.id
                        },sendMsg:'禁用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已禁用");
                            }else{
                                layer.msg("禁用失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-enable'){
            // ~启用操作~
            layer.msg('确定要启用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/seller/enabling',
                        data:{
                            id:data.id
                        },sendMsg:'启用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已启用");
                            }else{
                                layer.msg("启用失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else{
            layer.msg("没有相关操作");
        }
    });

    //打开编辑页面或者是新增数据页面
    function showEditOrAdd(title,doc,opr){
        layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['650px', '490px'],
            content: $(doc),
            cancel: function(){
                $('#sbwl_from_id')[0].reset();
            }
        });
        // 设置操作类型!
        oprType = opr;
    }

    //from表单验证规则
    form.verify({
        orgId: function(value){
            if(value==0){
                return "请选择对应组织";
            }
        }
    });

    //from表单监听提交
    form.on('submit(sbwl_submit)', function(data){
        // 获取角色设置的数据
        var roleId = formSelects.value('selectId', 'valStr');
        var bankId = formSelects.value('selects_bank_id', 'valStr');
        var param = data.field;
        param.roleIds = roleId;
        param.bankids = bankId;

        // 提交后台数据!
        shenbi.ajax({
            type:'post',
            url:oprType==='add'?'/seller/save':'/seller/update',
            data:param,
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.close(layer_);
                    if(oprType==="add"){
                        layer.msg("成功");
                    }else{
                        layer.msg("已更新");
                    }
                    // 刷新数据表
                    table_reload();
                    // 重置表单
                    $('#sbwl_from_id')[0].reset();
                }else{
                    layer.msg(resp.message);
                    return false
                }
            }
        });
        return false
    });
});