
layui.use(['table','form','upload'], function(){
  var table = layui.table;
  var form  = layui.form;
  var $=layui.jquery;
  var upload = layui.upload;
  
  //操作类型[add|update],默认为add
  var oprType = 'add';
  var layer_ = ''; 
  
  // 表格数据渲染
  table.render({
    elem: '#table-data',
	url: '/shop/seller/shop/list',
	toolbar: '#toolbar',
	title: '商家店铺数据表',
	id: 'table_list',
	cols: [[
		{type:'checkbox',sort:true,align: 'center'},
		{type:'numbers',title:'序号',align: 'center',width:'5%',hide:true},
		{field:'id',title:'ID'},
		{field:'platformName', title:'所属平台',align: 'center'},
		{field:'shopCode', title:'店铺编码', align: 'center'},
		{field:'shopName', title:'店铺名称', align: 'center'},
		{field:'userNaem', title:'发件人', align: 'center'},
		{field:'userPhone', title:'发件人电话', align: 'center'},
		{field:'sellerNick', title:'卖家昵称', align: 'center'},
		{field:'uniacid', title:'对单ID', align: 'center'},
		{field:'flag', title:'旗帜', align: 'center',templet:function(rse){
			return flagFunc(rse.flag);
		}},
		{field:'address', title:'发件地址', hide:true},
		{field:'remark', title:'备注', align: 'center'},
		{field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
			  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
		}},
		{fixed: 'right', title:'操作', toolbar: '#operation', width:250,align: 'center'}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return shenbi.returnParse(res);
    },
	page: true
  });
  
  // 旗帜颜色
  function flagFunc(flag){
	  //0(灰色), 1(红色), 2(黄色), 3(绿色), 4(蓝色), 5(粉红色)
	  switch (flag) {
	case 0:
		return flag+"(灰色)";
	case 1:
		return  flag+"(红色)";
	case 2:
		return  flag+"(黄色)";
	case 3:
		return  flag+"(绿色)";
	case 4:
		return  flag+"(蓝色)";
	case 5:
		return  flag+"(粉红色)";
	default:
		if(null == flag || "" == flag){
			return  "";	
		}
	  return  flag;
	}
  }
  
  //监听搜索按钮
  form.on('submit(sbwl_search)', function(data){
    table.reload('table_list',{
    	page: {
            curr: 1 //重新从第 1 页开始
         },
         where:data.field
    });
    return false;
  });
  
  	 //渲染select选项 (平台)
	 kits.select({
		 elem:'.sbwl_selece',
		 url:'/config/platform/select',
		 value:'id',
		 name:'name',
		 form:form
	 });
	 
  //刷新列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
    	case 'sbwl_dels':
		    var data = checkStatus.data;
		    var ids = [];
		    $.each(data,function(index,item){
			  ids.push(item.id);
		    });
		    
		    if(ids.length<=0){
		    	layer.msg("至少选择一项进行操作"); 
		    	return false
		    }
		    
  	    	layer.msg('确定要批量删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/seller/shop/deletes',
	    				data:{ids:ids.join()},
	    				sendMsg:'删除中...',
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
    		break;
	    case 'sbwl_create':
	    	 $("input[name=id]").val("");
	    	 
	    	 //渲染select选项 (平台)
	    	 kits.select({
	    		 elem:'.sbwl_selece',
	    		 url:'/config/platform/select',
	    		 value:'id',
	    		 name:'name',
	    		 form:form
	    	 });
	    	 
	    	//渲染select选项 (商家选项)
    		kits.select({
    			elem:'.sbwl_seller_selece',
    			url:'/seller/select',
    			value:'id',
    			name:'name',
    			form:form
    		});
	    	 
		  	// 新增页面
			showEditOrAdd('新增','#sbwl_tmpl','add');
	    break;
    };
  });
  
//监听行工具事件
  table.on('tool(table-filter)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'sbwl-set'){
		     
	    } else if(obj.event === 'sbwl-edit'){
	    	 // 设置表单数据!
	   	    form.val("sbwl_from", {
	   	      "id":data.id,
	   	      "shopName":data.shopName,	
			  "shopCode":data.shopCode,
			  "platformId":data.platformId,
			  "userPhone":data.userPhone,
			  "userNaem":data.userNaem,
			  "sellerNick":data.sellerNick,
			  "uniacid":data.uniacid,
			  "flag":data.flag,
			  "remark":data.remark,
			  "address":data.address
			});
	   	    
	   	     // 渲染select选项 
	    	 kits.select({
	    		 elem:'#sbwl_selece',
	    		 url:'/config/platform/select',
	    		 value:'id',
	    		 name:'name',
	    		 select:data.platformId,
	    		 form:form
	    	 });
	    	 
	    	 // 打开编辑页面!
			showEditOrAdd('编辑','#sbwl_tmpl','update');
	    }else if(obj.event === 'sbwl-del'){
	    	layer.msg('确定要删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/shop/seller/shop/delete',
	    				data:{id:data.id},
	    				sendMsg:'删除中...',
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
	    }else if(obj.event === 'import_win'){
	    	 $("input[name='phone']").val(data.userPhone);
	    	 $("input[name='name']").val(data.userNaem);
	    	 $("input[name='shopName']").val(data.shopName+"-"+findDate());
	    	 $("#js-address").text(data.address);
	    	showImportWin("导入","#import-win");
	    } else{
	    	layer.msg("没有相关操作");
	    }
  	});
  
  function findDate(){
      var myDate = new Date();
		//获取当前年
      var year=myDate.getFullYear();
		//获取当前月
      var month=myDate.getMonth()+1;
		//获取当前日
      var date=myDate.getDate();
      return year+"/"+month+"/"+date;
  }
  	//打开要导入的win窗口
	var $uploadLayer;
	function showImportWin(title,doc){
		$uploadLayer = layer.open({
	        type: 1,
	        title: title,
	        skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['650px', '550px'],
			content: $(doc),
			cancel: function(){
				cleanUp();
			}
		});
	};
	
	// 清除数据，导入数的参数
  	function cleanUp(){
  		 $("input[name='phone']").val("");
    	 $("input[name='name']").val("");
    	 $("input[name='shopName']").val("");
    	 $("input[name='url']").val("");
    	 $("input[name='addressdz']").val("");
  	}
  
  	//打开编辑页面或者是新增数据页面
  	function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
	        type: 1,
	        title: title,
	        skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['650px', '550px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		oprType = opr;
  	}
  	
  	//from表单验证规则
	form.verify({
		adminId: function(value){
			if(value == '0'){
				return "请选择对应的商家";
			}
		},
		platformId: function(value){
			if(value == '0'){
				return "请选择对应的平台";
			}
		}
	});

  //from表单监听提交
  form.on('submit(sbwl_submit)', function(data){
	 var $data = data.field;
	 if($data.flag == 0){
		 $data.flag = 5;
	 }
   // 提交后台数据!
	shenbi.ajax({
		type:'post',
		url:oprType=='add'?'/shop/seller/shop/save':'/shop/seller/shop/update',
		data:$data,
		sendMsg:'提交中...',
		success:function(resp){
			if(resp.retCode === 0){
				layer.close(layer_);
				if(oprType=="add"){
					layer.msg("成功"); 
				}else{
					layer.msg("已更新"); 
				}
				// 刷新数据表
				table_reload();
				// 重置表单
				$('#sbwl_from_id')[0].reset();
			}else{
				// 错误提示
				layer.msg(resp.message); 
			}
		}
	});
	return false
  });
  
	// 上传过滤文件
  var $showLoad;
  upload.render({
		elem: '#btn-import',   //绑定元素
		url: '/shop/filtering',  //上传接口
		type: "file",
		accept: 'file',
		auto: false,
		bindAction:'#btnUploader',
		choose: function(obj){
			//将每次选择的文件追加到文件队列
			obj.preview(function(index,file , result){
				$('input[name="url"]').val(file.name);
			});
		},
		before:function(obj){
			//关键代码
			this.data={
				'address' : $("#js-address").val(),
				'name' : $("#js-name").val(),
				'phone' : $("#js-phone").val(),
				//'express' : $("#js-express").val(),
				//'remark' : $("#js-remark").val(),
			};
			// 文件提交上传前的回调
			$showLoad = shenbi.showLoad("数据导入中，请稍等");
		},
		done: function(res){
			var fileName = $("#js-shopName").val();
			shenbi.closeLoad($showLoad);
			cleanUp();
			layer.close($uploadLayer);
			//上传完毕回调
			if(res.retCode === 0){
				layer.msg("导入成功！",{icon:1,time:2000},function(){
					window.location.href = "/shop/export?fileName="+fileName;
				}); 
			}else{
				layer.msg(res.message,{icon:5,time:3000},function(){}); 
			}
		},error: function(res){
		  //请求异常回调
			layer.msg("上传文件异常！",{icon:5,time:3000},function(){}); 
		}
	});
  
});