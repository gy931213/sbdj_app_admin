layui.use(['upload','form'], function(){
	var form = layui.form,
    	upload = layui.upload;
	
	shenbi.ajax({
		type:'get',
		url:'/sys/admin/info/get',
		success:function(resp){
			if(resp.retCode === 0){
				loadForm(resp.data);
			}else{
				layer.msg("加载失败！");
			}
		}
	});
	
	// 加载form
	function loadForm(data){
	  form.val("admin_form", {
		  id:data.id,
		  orgName:data.orgName,
		  name:data.name,
		  mobile:data.mobile,
		  gender:data.gender,
		  lastLoginTime:data.lastLoginTime,
		  headUrl:data.headUrl
	  });
	}
	
	 // 图片上传
	  var $showLoad = '';
	  var uploadInst = upload.render({
		elem: '#uploaderInput', //绑定元素
		url: '/base/upload', //上传接口
		accept:'images',
		acceptMime:'image/*',
		before:function(){
			// 文件提交上传前的回调
			$showLoad = shenbi.showLoad("上传中...");
		},
		done: function(res){
			shenbi.closeLoad($showLoad);
			//上传完毕回调
			if(res.retCode === 0 && res.message!=""){
				 // 赋值
				 form.val("admin_form", {
					  headUrl:res.message
				 });
			}
		},error: function(res){
		  //请求异常回调
			console.log(res)
		}
	  });
	  
	  // ~新增商品任务form表单~
	  form.on('submit(setmyinfo)', function(data){
	     // 提交后台数据
		 shenbi.ajax({
			type:'post',
			url:'/sys/admin/info/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode==0){
					layer.msg("成功"); 
				}else{
					layer.msg(resp.message); 
				}
			}
		});
		return false
	  });
	
});