
/*layui.config({
	base: '/js/plugins/layui/lay/modules/'
}).extend({
	formSelects: 'formSelects-v4'
});*/
layui.use(['table','form'], function(){
	var formSelects = layui.formSelects;
	var table = layui.table;
	var form  = layui.form;
	var $=layui.jquery;
  
  //操作类型[add|update],默认为add
  var oprType = 'add';
  var layer_ = ''; 
  
  table.render({
    elem: '#table-data',
	url:'/sys/admin/list',
	toolbar: '#toolbar',
	title: '管理员数据表',
	id:'table_list',
	cols: [[
		{type:'checkbox',sort:true,align: 'center'},
		  {type:'numbers',title:'序号',align: 'center',width:'5%'},
	  {field:'orgName', title:'所属组织',align: 'center'},
	  {field:'name', title:'用户名称',align: 'center'},
        {field:'anotherName', title:'别名/代号',align: 'center'},
	  {field:'mobile', title:'用户账号',align: 'center'},
	  {field:'gender', title:'性别', align: 'center',templet:function(rse){
		if(rse.gender==0){
			//return "男";&#xe662;
			return '<i class="layui-icon">&#xe662;</i>';
		}else if(rse.gender==1){
			//return "女";
			return '<i class="layui-icon">&#xe661;</i>';
		}else{
			//return "保密";
			return '<i class="layui-icon">&#xe664;</i>';
		}
	  }},
	  {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.lastLoginTime);
	  }},
	  {field:'lastLoginTime', title:'最后登录时间', align: 'center',templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.lastLoginTime);
	  }},
	  {field:'ip', title:'IP地址', align: 'center'},
	  {field:'status', title:'状态', align: 'center',templet:function(rse){
		  return kits.returnStatus(rse.status);
	  }},
	  {fixed: 'right', title:'操作', toolbar: '#operation', width:180,align: 'center'}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return {
          "code": res.retCode, 		//解析接口状态
          "msg":  res.message, 	    //解析提示文本
          "count": res.data.total, 	//解析数据长度
          "data": res.data.list 	//解析数据列表
        }
    },
	page: true
  });
  
  //监听搜索按钮
  form.on('submit(sbwl_search)', function(data){
    table.reload('table_list',{
    	page: {
            curr: 1 //重新从第 1 页开始
         },
         where:data.field
    });
    return false;
  });
  
	//渲染select选项 
	kits.select({
		elem:'#sbwl_org_select_1',
		url:'/sys/organization/select',
		value:'id',
		name:'name',
		form:form
	});
  
  // 刷新列表
  function table_reload(){
	  table.reload('table_list');
  }
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
    	case 'sbwl_dels':
		    var data = checkStatus.data;
		    var ids = [];
		    $.each(data,function(index,item){
			  ids.push(item.id);
		    });
		    
		    if(ids.length<=0){
		    	layer.msg("至少选择一项进行操作"); 
		    	return false
		    }
		    
  	    	layer.msg('确定要批量删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/sys/admin/deletes',
	    				data:{ids:ids.join()},
	    				sendMsg:'删除中...',
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
    		break;
	    case 'sbwl_create':
	    	 $("input[name=id]").val("");
	    	 $("input[name=pwd]").attr("readOnly",false);
	    	 
	    	 //渲染select选项（机构） 
	    	 kits.select({
	    		 elem:'.sbwl_org_select',
	    		 url:'/sys/organization/select',
	    		 value:'id',
	    		 name:'name',
	    		 form:form
	    	 });
	    	 
		  	// 新增页面
			showEditOrAdd('新增','#sbwl_tmpl','add');
	    break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'sbwl-set'){
		     
	    } else if(obj.event === 'sbwl-edit'){
	    	 // 设置表单数据!
	   	    form.val("sbwl_from", {
	   	      "id":data.id,
	   	      "name":data.name,	
			  "mobile":data.mobile,
			  "gender":data.gender,
			  "isSuper":data.isSuper,
                "anotherName":data.anotherName
			});
	   	    
	   	    // 渲染机构的 select选项 
	    	 kits.select({
	    		 elem:'.sbwl_org_select',
	    		 url:'/sys/organization/select',
	    		 value:'id',
	    		 name:'name',
	    		 select:data.orgId,
	    		 form:form
	    	 });
	    	 
	    	 // 渲染机构对应的角色数据
	    	 kits.selects({
				 elem:'selectId',
				 url:'/sys/role/select',
				 data:{orgId:data.orgId},
				 value:'id',
				 name:'name',
				 select:formSelects,
				 selectVal:data.roleIds
			});
	    	 
	    	 // 渲染角色的select选项
	    	 //setSelects(data.id);
	    	$("input[name=pwd]").val("");
	    	$("input[name=pwd]").attr("lay-verify","");
	   	    $("input[name=pwd]").attr("placeholder","更新密码，请输入新密码即可，默认为空，填入则为更新旧密码");
	   	    
	    	// 打开编辑页面!
			showEditOrAdd('编辑','#sbwl_tmpl','update');
	    }else if(obj.event === 'sbwl-del'){
	    	layer.msg('确定要删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/sys/admin/delete',
	    				data:{id:data.id},
	    				sendMsg:'删除中...',
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
	    }else{
	    	layer.msg("没有相关操作");
	    }
  	});
  
  	//打开编辑页面或者是新增数据页面
  	function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
	        type: 1,
	        title: title,
	        skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['650px', '500px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		oprType = opr;
  	}
  	
  /**!
   * 设置角色的select选项选中函数~
   */
  function setSelects(adminId){
	  shenbi.ajax({
		type:'post',
		url:'/sys/role/byadminid',
		data:{adminId:adminId},
		success:function(resp){
			if(resp.retCode === 0){
				var item = resp.data;
				var val = [];
				for (var i = 0; i < item.length; i++) {
					val.push(item[i].roleId);
				}
				//formSelects.value("selectId", val);
				return val;
			}
		}
	 });
  }
  	
  	//from表单验证规则
	form.verify({
		orgId: function(value){
			if(value== '0'){
				return "请选择对应组织";
			}
		},
		roleId:function(value){
			if(value== '0'){
				return "请选择角色";
			}
		}
	});

	// 监听select选中的事件~
	form.on('select(sbwl_org_select)', function(data){
		kits.selects({
			 elem:'selectId',
			 url:'/sys/role/select',
			 data:{orgId:data.value},
			 value:'id',
			 name:'name',
			 select:formSelects
		});
		console.log(data)
	});
	
	  //from表单监听提交
	  form.on('submit(sbwl_submit)', function(data){
		  // 获取角色设置的数据
		  var roleId = formSelects.value('selectId', 'valStr');
		  var data = data.field;
		  	  data.roleIds = roleId;
		  
		   // 提交后台数据!
		  shenbi.ajax({
				type:'post',
				url:oprType=='add'?'/sys/admin/save':'/sys/admin/update',
				data:data,
				sendMsg:'提交中...',
				success:function(resp){
					if(resp.retCode === 0){
						layer.close(layer_);
						if(oprType=="add"){
							layer.msg("成功"); 
						}else{
							layer.msg("已更新"); 
						}
						// 刷新数据表
						table_reload();
						// 重置表单
						$('#sbwl_from_id')[0].reset();
					}else{
						layer.msg(resp.message); 
					}
				}
			});
		return false
	  });
});