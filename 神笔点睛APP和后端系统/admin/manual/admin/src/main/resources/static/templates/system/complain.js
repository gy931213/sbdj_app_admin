
layui.use(['table','form','laydate'], function(){
    var table = layui.table;
    var laydate = layui.laydate;
    var form  = layui.form;

    //开始日期
    laydate.render({
        elem: '#startDate',
        type: 'datetime'
    });
    //结束日期
    laydate.render({
        elem: '#endDate',
        type: 'datetime'
    });

    // ~渲染select选项 （机构）
    kits.select({
        elem:'#sbwl_org_select',
        url:'/sys/organization/select',
        value:'id',
        name:'name',
        form:form
    });

    table.render({
        id:'table_list',
        elem: '#table-data',
        url:'/complain/list',
        toolbar: '#toolbar',
        title: '投诉数据表',
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
            {field:'orgName', title:'所属机构', align: 'center',hide:false},
            {field:'complainName', title:'投诉人名称', align: 'center',hide:false},
            {field:'defendantName', title:'被投诉人名称', align: 'center',hide:false},
            {field:'speed', title:'响应速度', align: 'center',hide:true, templet:function(rse){
                    var tmp = "";
                    if (rse.speed === 1) {
                        tmp = "一星";
                    } else if (rse.speed === 2) {
                        tmp = "二星";
                    } else if (rse.speed === 3) {
                        tmp = "三星";
                    } else if (rse.speed === 4) {
                        tmp = "四星";
                    } else if (rse.speed === 5) {
                        tmp = "五星";
                    }
                    return tmp;
                }},
            {field:'efficiency', title:'完成效率', align: 'center',hide:true, templet:function(rse){
                    var tmp = "";
                    if (rse.efficiency === 1) {
                        tmp = "一星";
                    } else if (rse.efficiency === 2) {
                        tmp = "二星";
                    } else if (rse.efficiency === 3) {
                        tmp = "三星";
                    } else if (rse.efficiency === 4) {
                        tmp = "四星";
                    } else if (rse.efficiency === 5) {
                        tmp = "五星";
                    }
                    return tmp;
                }},
            {field:'attitude', title:'服务态度', align: 'center',hide:true, templet:function(rse){
                    var tmp = "";
                    if (rse.attitude === 1) {
                        tmp = "一星";
                    } else if (rse.attitude === 2) {
                        tmp = "二星";
                    } else if (rse.attitude === 3) {
                        tmp = "三星";
                    } else if (rse.attitude === 4) {
                        tmp = "四星";
                    } else if (rse.attitude === 5) {
                        tmp = "五星";
                    }
                    return tmp;
                }},
            {field:'type', title:'投诉类型', align: 'center', templet:function(rse){
                    var tmp = "";
                    if (rse.type === 0) {
                        tmp = "业务员";
                    } else if (rse.type === 1) {
                        tmp = "商家";
                    }
                    return tmp;
                }},
            {field:'praiseType', title:'评价类型', align: 'center', templet:function(rse){
                    var tmp = "";
                    if (rse.praiseType === 1) {
                        tmp = "好评";
                    } else if (rse.praiseType === 2) {
                        tmp = "中评";
                    } else if (rse.praiseType === 3) {
                        tmp = "差评";
                    }
                    return tmp;
                }},
            {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {fixed: 'right', title:'操作', toolbar: '#operation', width:180,align: 'center'}
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        parseData: function(res){
            return {
                "code": res.retCode, 		//解析接口状态
                "msg": res.data.message, 	//解析提示文本
                "count": res.data.total, 	//解析数据长度
                "data": res.data.list 	//解析数据列表
            }
        },
        page: true
    });


    //监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
        	page: {
                curr: 1 //重新从第 1 页开始
             },
             where:data.field
        });
        return false;
    });

    //监听搜索按钮
    form.on('submit(sbwl_mon)', function(data){
        var $province = echarts.init(document.getElementById('province'));
        $.ajax({
            url: '/complain/total',
            data: data.field,
            type: 'get',
            success: function (resp) {
                if(resp.retCode === 0){
                    province(resp.data.province,$province);
                }
            },
            error: function (data) {
                layer.msg("服务器繁忙，请稍后重试...",{time:4000});
            }
        });
        return false;
    });

    // 刷新列表
    function table_reload(){
        table.reload('table_list');
    }

    //头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'getCheckData':
                var data = checkStatus.data;
                layer.alert(JSON.stringify(data));
                break;
            case 'getCheckLength':
                var data = checkStatus.data;
                layer.msg('选中了：'+ data.length + ' 个');
                break;
            case 'isAll':
                layer.msg(checkStatus.isAll ? '全选': '未全选');
                break;
            case 'statistics':
                //开始月份
                laydate.render({
                    elem: '#startMonth',
                    type: 'month'
                });
                //结束月份
                laydate.render({
                    elem: '#endMonth',
                    type: 'month'
                });
                var $province = echarts.init(document.getElementById('province'));
                var date = new Date();
                var month = date.getFullYear() + '-' + (date.getMonth() >= 9 ? (date.getMonth()+1) : '0' + date.getMonth());
                $.ajax({
                    url: '/complain/total',
                    type: 'get',
                    data: {
                        startMonth: month,
                        endMonth: month
                    },
                    success: function (resp) {
                        if(resp.retCode === 0){
                            province(resp.data.province,$province);
                        }
                    },
                    error: function (data) {
                        layer.msg("服务器繁忙，请稍后重试...",{time:4000});
                    }
                });
                showprovince("月度总结","#sbwl_province_tmpl");
                break;
        };
    });

    //监听行工具事件
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        if(obj.event === 'sbwl-del'){
            // ~支付
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/complain/delete',
                        data:{
                            id:data.id
                        },
                        sendMsg:'支付中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已删除");
                            }else{
                                layer.msg("删除失败");
                            }
                            // ~刷新数据
                            table_reload();
                        }
                    });
                }
            });
        } else if(obj.event === 'edit'){

        }
    });

    // 统计数据
    function showprovince(title,doc){
        $('#province-form')[0].reset();
        $layer = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            maxmin: true,
            area: ['1050px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
        layer.full($layer);
    }

    function province(data,$province){
        var name = [];
        var speed = [];
        var attitude = [];
        var efficiency = [];
        for(var i = 0; i < data.length; i++){
            name.push(data[i].name);
            attitude.push(data[i].attitude);
            speed.push(data[i].speed);
            efficiency.push(data[i].efficiency);
        }

        var colors = ['#5793f3', '#d14a61', '#675bba'];

        option = {
            color: colors,

            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross'
                }
            },
            grid: {
                right: '20%'
            },
            toolbox: {
                feature: {
                    dataView: {show: true, readOnly: false},
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            legend: {
                data:['完成效率','服务态度','响应速度']
            },
            xAxis: [
                {
                    type: 'category',
                    axisTick: {
                        alignWithLabel: true
                    },
                    data: name
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: '完成效率',
                    min: 0,
                    position: 'right',
                    axisLine: {
                        lineStyle: {
                            color: colors[0]
                        }
                    },
                    axisLabel: {
                        formatter: '{value} 星'
                    }
                },
                {
                    type: 'value',
                    name: '服务态度',
                    min: 0,
                    position: 'right',
                    offset: 80,
                    axisLine: {
                        lineStyle: {
                            color: colors[1]
                        }
                    },
                    axisLabel: {
                        formatter: '{value} 星'
                    }
                },
                {
                    type: 'value',
                    name: '响应速度',
                    min: 0,
                    position: 'left',
                    axisLine: {
                        lineStyle: {
                            color: colors[2]
                        }
                    },
                    axisLabel: {
                        formatter: '{value} 星'
                    }
                }
            ],
            series: [
                {
                    name:'完成效率',
                    type:'bar',
                    data: efficiency
                },
                {
                    name:'服务态度',
                    type:'bar',
                    yAxisIndex: 1,
                    data: attitude
                },
                {
                    name:'响应速度',
                    type:'bar',
                    yAxisIndex: 2,
                    data: speed
                }
            ]
        };

        $province.setOption(option);
    }
});