/*layui.config({
	base: '/js/plugins/layui/lay/modules/'
}).extend({
	formSelects: 'formSelects-v4'
});*/
layui.use(['table', 'form'], function () {
    var formSelects = layui.formSelects;
    var table = layui.table;
    var form = layui.form;
    var $ = layui.jquery;

    //操作类型[add|update],默认为add
    var oprType = 'add';
    var layer_ = '';

    table.render({
        elem: '#table-data',
        url: '/sys/config/list',
        toolbar: '#toolbar',
        title: '管理员数据表',
        id: 'table_list',
        cols: [[
        	{type:'checkbox',sort:true,align: 'center'},
      	  {type:'numbers',title:'序号',align: 'center',width:'5%'},
            {field: 'id', title: '配置名称id', align: 'center', hide: true},
            {field: 'name', title: '配置名称', align: 'center'},
            {field: 'code', title: '配置代码', align: 'center'},
            {
                field: 'createTime', title: '创建时间', align: 'center', templet: function (rse) {
                    return kits.dateFtt("yyyy-MM-dd hh:mm:ss", rse.createTime);
                }
            },
            {
                field: 'status', title: '状态', align: 'center', templet: function (rse) {
                    return kits.returnStatus(rse.status);
                }
            },
            {fixed: 'right', title: '操作', toolbar: '#operation', width: 270, align: 'center'}
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        parseData: function (res) {
            return {
                "code": res.retCode, 		//解析接口状态
                "msg": res.message, 	//解析提示文本
                "count": res.data.total, 	//解析数据长度
                "data": res.data 	//解析数据列表
            }
        },
        page: true
    });

    // 刷新列表
    function table_reload() {
        table.reload('table_list');
        table.reload('table_list_dtl')
    }

    //头工具栏事件
    table.on('toolbar(table-filter)', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        switch (obj.event) {
            case 'sbwl_dels':
                var data = checkStatus.data;
                var ids = [];
                $.each(data, function (index, item) {
                    ids.push(item.id);
                });

                if (ids.length <= 0) {
                    layer.msg("至少选择一项进行操作");
                    return false
                }

                layer.msg('确定要批量删除吗？', {
                    btn: ['确定', '取消'],
                    yes: function (index, layero) {
                        var data = obj.data;
                        shenbi.ajax({
                            type: 'post',
                            url: '/sys/config/deletes',
                            data: {ids: ids.join()},
                            success: function (resp) {
                                if (resp.retCode === 0) {
                                    layer.msg("已删除");
                                    table_reload();
                                } else {
                                    layer.msg("删除失败");
                                    table_reload();
                                }
                            }
                        });
                    }
                });
                break;
            case 'sbwl_create':
                $("input[name=id]").val("");
                $("input[name=pwd]").attr("readOnly", false);
                // 新增页面
                showEditOrAdd('新增', '#sbwl_tmpl', 'add');
                break;
        }
    });

    //头工具栏事件
    table.on('toolbar(table-filter-dtl)', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        switch (obj.event) {
            case 'sbwl_dtl_create':
                // 详情新增页面
                showEditOrAdd('新增', '#sbwl_tmpl_dtl', 'add');
                break;
            case 'sbwl_dtl_dels':
                var data = checkStatus.data;
                var ids = [];
                $.each(data, function (index, item) {
                    ids.push(item.id);
                });

                if (ids.length <= 0) {
                    layer.msg("至少选择一项进行操作");
                    return false
                }

                layer.msg('确定要批量删除吗？', {
                    btn: ['确定', '取消'],
                    yes: function (index, layero) {
                        var data = obj.data;
                        shenbi.ajax({
                            type: 'post',
                            url: '/sys/config/dtl/deletes',
                            data: {ids: ids.join()},
                            success: function (resp) {
                                if (resp.retCode === 0) {
                                    layer.msg("已删除");
                                    table_reload();
                                } else {
                                    layer.msg("删除失败");
                                    table_reload();
                                }
                            }
                        });
                    }
                });
                break;
        }
    });

    //监听行工具事件
    table.on('tool(table-filter)', function (obj) {
        var data = obj.data;
        console.log(obj)
        if (obj.event === 'sbwl-set') {

        } else if (obj.event === 'sbwl-edit') {
            // 设置回显表单数据!
            form.val("sbwl_from", {
                "id": data.id,
                "name": data.name,
                "code": data.code
            });

            // 打开编辑页面!
            showEditOrAdd('编辑', '#sbwl_tmpl', 'update');
        } else if (obj.event === 'sbwl-del') {
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function (index, layero) {
                    var data = obj.data;
                    shenbi.ajax({
                        type: 'post',
                        url: '/sys/config/delete',
                        data: {id: data.id},
                        success: function (resp) {
                            if (resp.retCode === 0) {
                                layer.msg("已删除");
                                table_reload();
                            } else {
                                layer.msg("删除失败");
                                table_reload();
                            }
                        }
                    });
                }
            });
        } else if (obj.event === 'sbwl-mail-list') {
            // ~查看系统配置详情操作~
            if (data === "" || data.id === "") return false;
            //渲染table数据
            tableConfigDtl(data.id);
            //打开页面
            var layer_ = layer.open({
                type: 1,
                title: '<i class="layui-icon">&#xe613;</i> 配置详情',
                skin: 'layui-layer-molv',
                shadeClose: false,
                area: ['100%', '730px'],
                content: $("#sbwl_phones_tmpl"),
                cancel: function () {

                }
            });
            layer.full(layer_);
        } else {
            layer.msg("没有相关操作");
        }
    });

    //详情页——监听行工具事件
    table.on('tool(table-filter-dtl)', function (obj) {
        var data = obj.data;
        console.log(obj)
        if (obj.event === 'sbwl-set') {

        } else if (obj.event === 'sbwl_edit') {
            // 设置详情回显表单数据!
            form.val("sbwl_from_dtl", {
                "id": data.id,
                "name": data.name,
                "sort": data.sort,
                "code": data.code
            });

            // 打开详情编辑页面!
            showEditOrAdd('编辑', '#sbwl_tmpl_dtl', 'update');
        } else if (obj.event === 'sbwl_del') {
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function (index, layero) {
                    var data = obj.data;
                    shenbi.ajax({
                        type: 'post',
                        url: '/config/dtl/delete',
                        data: {id: data.id},
                        success: function (resp) {
                            if (resp.retCode === 0) {
                                layer.msg("已删除");
                                table_reload();
                            } else {
                                layer.msg("删除失败");
                                table_reload();
                            }
                        }
                    });
                }
            });
        } else {
            layer.msg("没有相关操作");
        }
    });

    // ~系统配置详情数据~
    function tableConfigDtl(id) {
        // ~数据渲染
        table.render({
            id: 'table_list_dtl',
            elem: '#table_configDtl_data',
            url: '/sys/config/dtl/list?id=' + id,
            toolbar: '#dtlToolbar',
            title: '系统配置详情表',
            cols: [[
                {type: 'checkbox', sort: true, align: 'center'},
                {type: 'numbers', align: 'center'},
                {field: 'id', title: '配置id', align: 'center', hide: true},
                {field: 'name', title: '配置名称', align: 'center'},
                {field: 'code', title: '编码', align: 'center'},
                {
                    field: 'createTime', title: '创建时间', align: 'center', templet: function (rse) {
                        return kits.dateFtt("yyyy-MM-dd hh:mm:ss", rse.createTime);
                    }
                },
                {
                    field: 'status', title: '状态', align: 'center', templet: function (rse) {
                        return kits.returnStatus(rse.status);
                    }
                },
                {field: 'sort', title: '排序号', align: 'center'},
                {fixed: 'right', title: '操作', toolbar: '#dltOperation', width: 180, align: 'center'}
            ]],
            parseData: function (res) {
                return {
                    "code": res.retCode, 		//解析接口状态
                    "msg": res.data.message, 	//解析提示文本
                    "count": res.data.total, 	//解析数据长度
                    "data": res.data.list 	//解析数据列表
                }
            },
            page: true
        });
        // 当前父级id
        $("input[name='configId']").val(id);
    }

    // ~监听提交code输入事件
    $('input[name="name"]').on('input', function (i, item) {
        var val = $(this).val();
        var char = pinyin.getCamelChars(val);
        $('input[name="code"]').val("TYPE_" + char);
    });

    $("#sbwl_from_id_dtl input[name='name']").on('input',function(){
        var val = $(this).val();
        var char = pinyin.getCamelChars(val);
        $("#codedtl").val(char);
    });

    //打开编辑页面或者是新增数据页面
    function showEditOrAdd(title, doc, opr) {
        layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['650px', '500px'],
            content: $(doc),
            cancel: function () {
                $('#sbwl_from_id')[0].reset();
                $('#sbwl_from_id_dtl')[0].reset();
            }
        });
        // 设置操作类型!
        oprType = opr;
    }

    //from表单监听提交
    form.on('submit(sbwl_submit)', function (data) {
        // 获取角色设置的数据
        var data = data.field;

        // 提交后台数据!
        shenbi.ajax({
            type: 'post',
            url: oprType === 'add' ? '/sys/config/add' : '/sys/config/update',
            data: data,
            success: function (resp) {
                if (resp.retCode === 0) {
                    layer.close(layer_);
                    if (oprType === "add") {
                        layer.msg("成功");
                    } else {
                        layer.msg("已更新");
                    }
                    // 刷新数据表
                    table_reload();
                    // 重置表单
                    $('#sbwl_from_id')[0].reset();
                } else {
                    layer.msg(data.message);
                }
            }
        });
        return false
    });
    //详情from表单监听提交
    form.on('submit(sbwl_dtl_submit)', function (data) {
        var data = data.field;
        // 提交后台数据!
        shenbi.ajax({
            type: 'post',
            url: oprType === 'add' ? '/sys/config/dtl/add' : '/sys/config/dtl/update',
            data: data,
            success: function (resp) {
                if (resp.retCode === 0) {
                    layer.close(layer_);
                    if (oprType === "add") {
                        layer.msg("成功");
                    } else {
                        layer.msg("已更新");
                    }
                    // 刷新数据表
                    table_reload();
                    // 重置表单
                    $('#sbwl_from_id_dtl')[0].reset();
                } else {
                    layer.msg(resp.message);
                }
            }
        });
        return false
    });
});