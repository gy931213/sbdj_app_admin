var editObj=null,ptable=null,treeGrid=null,tableId='table-data',layer=null,form=null,$=null;
//操作类型[add|update],默认为add
var oprType = 'add';
layui.config({
	base: '/js/plugins/layui/lay/modules/'
}).extend({
	treeGrid:'treeGrid'
}).use(['jquery','treeGrid','layer','form'], function(){
	    $=layui.jquery;
		treeGrid = layui.treeGrid;//很重要
		layer=layui.layer;
		form = layui.form;

	// 数据渲染
	ptable=treeGrid.render({
		id:tableId,
		elem: '#table-data',
		url:'/sys/function/list',
		cellMinWidth: 100,
		idField:'id',//必須字段
		treeId:'id',//树形id字段名称
		treeUpId:'pid',//树形父id字段名称
		treeShowName:'name',//以树形式显示的字段
		heightRemove:[".dHead",10],//不计算的高度,表格设定的是固定高度，此项不生效
		height:'100%',
		isFilter:false,
		iconOpen:true,//是否显示图标【默认显示】
		isOpenDefault:true,//节点默认是展开还是折叠【默认展开】
		loading:true,
		method:'post',
		isPage:false,
		toolbar: '#toolbar',
		cols: [
			[{type:'numbers',align: 'center'},
				{type:'checkbox',sort:true,align: 'center'},
				{field:'icon', width:80, title: '图标',templet:function(rse){
						return '<i class="'+rse.icon+'"></i>';
					},align: 'center'},
				{field:'name',  title: '功能名称'},
				{field:'code',  title: '功能代码'},
				{field:'url',  title: '请求url'},
				{field:'authority',  title: '标识',align: 'center'},
				{field:'type',  title: '类型',align: 'center',templet:function(rse){
						if(rse.type==='1'){
							return '<a class="layui-btn layui-btn-xs layui-btn-normal layui-btn-radius" data-val="'+rse.type+'">目录</a>';
						}else if(rse.type==='2'){
							return'<a class="layui-btn layui-btn-xs layui-btn-warm layui-btn-radius" data-val="'+rse.type+'">菜单</a>';
						}else if(rse.type==='3'){
							return '<a class="layui-btn layui-btn-xs layui-btn-danger layui-btn-radius" data-val="'+rse.type+'">按钮</a>';
						}else{
							return '<a class="layui-btn layui-btn-xs layui-btn-normal layui-btn-radius">未知</a>';
						}
					}},
				{field:'path',  title: '路径',align: 'center'},
				{field:'pid', title: 'pid',align: 'center'},
				{field:'sort', title: 'sort',align: 'center',templet:function(rse){
						return '<span class="layui-badge layui-bg-gray">'+rse.sort+'</span>';
					}},
				{width:200,title: '操作', align:'center',
					templet: function(d){
						var html='';
						if(d.pid ===-1 && d.path === "|" && d.type==='1'){
							html+='<a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="sbwl-add-menu">新增菜单</a>';
						}
						if(d.pid !==-1 && d.type==='2'){
							html+='<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="sbwl-add-btn">新增按钮</a>';
						}
						html+='<a class="layui-btn  layui-btn-normal layui-btn-xs" lay-event="sbwl-edit">编辑</a>';
						html+='<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="sbwl-del">删除</a>';
						return html;
					}
				}
			]],
		parseData:function (res) {//数据加载后回调
			if(res.retCode === 403){
				return {
					"code": res.retCode, 		//解析接口状态
					"msg": res.message 	    //解析提示文本
				}
			} else {
				return {
					"code": res.retCode, 		//解析接口状态
					"msg": res.data.message, 	//解析提示文本
					"count": res.data.total, 	//解析数据长度
					"data": res.data 			//解析数据列表
				}
			}
		},
		onClickRow:function (index, o) {
			// console.log(index,o,"单击！");
		},
		onDblClickRow:function (index, o) {
			// console.log(index,o,"双击");
		}
	});

	// 刷新数据列表
	function table_reload(){
		//treeGrid.reload('tree_grid_data');
		query();
	}

	//~监听输入事件
	$('input[name="name"]').on('input',function(i,item){
		var val = $(this).val();
		var char =pinyin.getCamelChars(val);
		$('input[name="code"]').val('FUNC_'+char);
	});

	//头工具栏事件
	/*treeGrid.on('toolbar('+tableId+')', function(obj){

	});*/

	//监听数据列表中的操作触发事件
	treeGrid.on('tool(table-filter)', function(obj){
		var data = obj.data;
		// 新增菜单
		if(obj.event === 'sbwl-add-menu'){
			// 设置表单数据!
			form.val("sbwl_from", {
				"path":data.path+data.id+"|",
				"pid":data.id,
				"type":"2"
			});
			// form 处理
			form_set(2);
			// 打开编辑页面!
			showEditOrAdd('新增菜单','#sbwl_tmpl','add');

		}else if(obj.event === 'sbwl-add-btn'){
			// 新增按钮
			// 设置表单数据!
			form.val("sbwl_from", {
				"path":data.path+data.id+"|",
				"pid":data.id,
				"type":"3",
			});
			// form 处理
			form_set(3);
			// 打开编辑页面!
			showEditOrAdd('新增按钮','#sbwl_tmpl','add');
		}else if(obj.event === 'sbwl-edit'){
			// 设置表单数据!
			form.val("sbwl_from", {
				"id":data.id,
				"name":data.name,
				"code":data.code,
				"url":data.url,
				"type":data.type,
				"icon":data.icon,
				"path":data.path,
				"authority":data.authority,
				"pid":data.pid,
				"sort":data.sort
			});
			// form 处理
			form_set(data.type);
			// 打开编辑页面!
			showEditOrAdd('编辑','#sbwl_tmpl','update');
		}else if(obj.event === 'sbwl-del'){
			layer.msg('确定要删除吗？', {
				btn: ['确定', '取消'],
				yes: function(index, layero){
					var data = obj.data;
					shenbi.ajax({
						type:'post',
						url:'/sys/function/delete',
						data:{id:data.id},
						sendMsg:'删除中...',
						success:function(resp){
							if(resp.retCode === 0){
								layer.msg("已删除");
							}else{
								layer.msg("删除失败");
							}
							//table_reload();
							query();
						}
					});
				}
			});
		}else{
			layer.msg("没有相关操作");
		}
	});

	//from表单监听提交
	form.on('submit(sbwl_submit)', function(data){
		// 提交后台数据!
		shenbi.ajax({
			type:'post',
			url:oprType==='add'?'/sys/function/save':'/sys/function/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.close(layer_);
					if(oprType==="add"){
						layer.msg("成功");
					}else{
						layer.msg("已更新");
					}
					// 刷新数据表
					//table_reload();
					query();
					// 重置表单
					$('#sbwl_from_id')[0].reset();
				}
			}
		});
		return false
	});

	// form 设置
	function form_set(type){
		switch (type) {
			case 1:
				$("#sbwl-func-url").css({display: 'none'});
				$("#sbwl-func-authority").css({display: 'none'});
				$("#sbwl-func-url").find("input").attr("lay-verify","");
				$("#sbwl-func-authority").find("input").attr("lay-verify","authority");
				break;
			case 2:
				$("#sbwl-func-authority").css({display: 'none'});
				$("#sbwl-func-icon").css({display: 'none'});
				$("#sbwl-func-path").css({display: 'block'});
				$("#sbwl-func-url").css({display: 'block'});
				$("#sbwl-func-path").find("input").attr("lay-verify","required|path");
				$("#sbwl-func-url").find("input").attr("lay-verify","required");
				break
			case 3:
				$("#sbwl-func-icon").css({display: 'none'});
				$("#sbwl-func-url").css({display: 'block'});
				$("#sbwl-func-path").css({display: 'block'});
				$("#sbwl-func-authority").css({display: 'block'});
				$("#sbwl-func-path").find("input").attr("lay-verify","required|path");
				$("#sbwl-func-url").find("input").attr("lay-verify","required");
				$("#sbwl-func-authority").find("input").attr("lay-verify","required|authority");
				break
		}
	}

});

var i=1000000;
function del(obj) {
	var checkStatus = treeGrid.checkStatus(tableId)
		,data = checkStatus.data;
	// 要进行删除操作的ids
	var ids = [];
	// 判断是否选中一项!
	if(data.length <= 0 ){
		return  layer.msg('必须选中一项进行删除操作！');
	}
	// 循环遍历数据!
	for (var i = 0; i < data.length; i++) {
		ids.push(data[i].id);
	}
	layer.confirm("你确定删除数据吗？如果存在下级节点则一并删除，此操作不能撤销！", {icon: 3, title:'提示'},
		function(index){//确定回调
			//obj.del();
			shenbi.ajax({
				type:'post',
				url:'/sys/function/deletes',
				data:{ids:ids.join()},
				sendMsg:'删除中...',
				success:function(resp){
					if(resp.retCode === 0){
						layer.msg("已删除");
						query();
					}else{
						layer.msg("删除失败");
					}
					//table_reload();
				}
			});
			query();
			layer.close(index);
		},function (index) {//取消回调
			query();
			layer.close(index);
		}
	);
}
//添加
function add() {
	// 打开新增目录页面
	showEditOrAdd('新增目录','#sbwl_tmpl','add');
	/*var pdata=pObj?pObj.data:null;
    var param={};
    param.name='水果'+Math.random();
    param.id=++i;
    param.pId=pdata?pdata.id:null;
    treeGrid.addRow(tableId,pdata?pdata[treeGrid.config.indexName]+1:0,param);*/
}

function print() {
	console.log(treeGrid.cache[tableId]);
	var loadIndex=layer.msg("对象已打印，按F12，在控制台查看！", {
		time:3000
		,offset: 'auto'//顶部
		,shade: 0
	});
}
function openorclose() {
	var map=treeGrid.getDataMap(tableId);
	var o= map['102'];
	treeGrid.treeNodeOpen(tableId,o,!o[treeGrid.config.cols.isOpen]);
}
function openAll() {
	var treedata=treeGrid.getDataTreeList(tableId);
	treeGrid.treeOpenAll(tableId,!treedata[0][treeGrid.config.cols.isOpen]);
}
function getCheckData() {
	var checkStatus = treeGrid.checkStatus(tableId)
		,data = checkStatus.data;
	layer.alert(JSON.stringify(data));
}
function radioStatus() {
	var data = treeGrid.radioStatus(tableId)
	layer.alert(JSON.stringify(data));
}
function getCheckLength() {
	var checkStatus = treeGrid.checkStatus(tableId)
		,data = checkStatus.data;
	layer.msg('选中了：'+ data.length + ' 个');
}
function reload() {
	treeGrid.reload(tableId,{

	});
}
function query() {
	treeGrid.query(tableId,{
		where:{

		}
	});
}
function test() {
	console.log(treeGrid.cache[tableId],treeGrid.getClass(tableId));

}
//打开编辑页面或者是新增数据页面
function showEditOrAdd(title,doc,opr){
	layer_ = layer.open({
		type: 1,
		title: title,
		skin: 'layui-layer-molv',
		shadeClose: false,
		area: ['650px', '650px'],
		content: $(doc),
		cancel: function(){
			$('#sbwl_from_id')[0].reset();
		}
	});
	// 设置操作类型!
	oprType = opr;
}