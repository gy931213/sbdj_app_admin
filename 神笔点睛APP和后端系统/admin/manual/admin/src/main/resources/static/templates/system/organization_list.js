

var editObj=null,ptable=null,treeGrid=null,tableId='table-data',layer=null,$=null;
layui.config({
    base: '/js/plugins/layui/lay/modules/'
}).extend({
    treeGrid:'treeGrid'
}).use(['jquery','treeGrid','layer','form'], function(){
        $=layui.jquery;
        treeGrid = layui.treeGrid;//很重要
        layer=layui.layer;
        form = layui.form;

    //操作类型[add|update],默认为add
    var oprType = 'add';
    var _xtree = null;
    var $orgId = null;
    // 数据授权弹出层
    var authorLayer = null;

    // 渲染功能树列表数据~!
    function loadTree(array){
        var tree = [];
        var map = {};
        var cd_data = [];
        var cd_map = {};
        var an_data = [];
        var an_map = {};

        // 获取目录 [{type:1}]
        for (var i = 0; i < array.length; i++) {
            if(array[i].pid===-1 && array[i].type==='1'){
                map.title = array[i].name;
                map.value = array[i].id;

                // 获取菜单 [{type:2}]
                for (var j = 0; j < array.length; j++) {
                    if(array[j].pid===array[i].id && array[j].type==='2'){
                        cd_map.title = array[j].name;
                        cd_map.value = array[j].path+array[j].id;

                        // 获取按钮 [{type:3}]
                        for (var k = 0; k < array.length; k++) {
                            if(array[k].pid===array[j].id && array[k].type==='3'){
                                an_map.title = array[k].name;
                                an_map.value = array[k].path+array[k].id;
                                an_map.data = [];
                            }

                            // 判断按钮是否为空!
                            if(JSON.stringify(an_map)!='{}'){
                                an_data.push(an_map);
                                an_map = {};
                            }
                        }
                        cd_map.data = an_data;
                        an_data = [];
                    }

                    // 判断菜单是否为空!
                    if(JSON.stringify(cd_map)!='{}'){
                        cd_data.push(cd_map);
                        cd_map = {};
                    }
                }

                // 将菜单赋值给根节点!
                map.data = cd_data;
                cd_data = [];
            }

            // 判断目录是否为空!
            if(JSON.stringify(map)!='{}'){
                tree.push(map);
                map = {};
            }
        }
        _xtree = new layuiXtree({
            elem: 'sbwl-xtree',
            form: form,
            data: tree,
            isopen: false,
            ckall: true,
        });
    };

    // 数据权限回显选中
    function layuiXtreeCheckbox(array){
        if(array.length <= 0){
            return;
        }
        // 在渲染数据之前删除历史数据
        //$("#sbwl-xtree .layui-form-checkbox").removeClass("layui-form-checked");
        //~目录
        for (var i = 0; i < array.length; i++) {
            if(array[i].type==='1'){
                var _this = $("#sbwl-xtree").find("input[value='"+array[i].id+"']");
                _this.attr("checked", true);
                var _next = _this.next();
                _next.addClass("layui-form-checked");
            }
        }
        //~菜单
        for (var j = 0; j < array.length; j++) {
            if(array[j].type==='2'){
                var _this = $("#sbwl-xtree").find("input[value='"+array[j].path+array[j].id+"']");
                _this.attr("checked", true);
                var _next = _this.next();
                _next.addClass("layui-form-checked");
            }
        }
        //~按钮
        for (var k = 0; k < array.length; k++) {
            if(array[k].type==='3'){
                var _this = $("#sbwl-xtree").find("input[value='"+array[k].path+array[k].id+"']");
                _this.attr("checked", true);
                var _next = _this.next();
                _next.addClass("layui-form-checked");
            }
        }
    }

    // 保存机构对应得权限数据
    form.on('submit(sbwl_submit_author)', function(data){
        var $ids = '';
        var oCks = _xtree.GetChecked();
        for (var i = 0; i < oCks.length; i++) {
            var val = oCks[i].value;
            if(val.indexOf("|")!=-1){
                var str = val.split("|");
                $ids+= str.join();
            }else{
                $ids+= $ids ==="" ? val : ","+val;
            }
        }

        if($ids === "" || $ids === null){
            layer.msg("请选择权限");
            return false;
        }

        // 权限数据
        var $func = kits.cancelRepeat($ids.split(","));
        shenbi.ajax({
            type:'post',
            url:'/sys/organization/authorize',
            data:{
                orgId:$orgId,
                funcIds:$func
            },
            success:function(resp){
                layer.close(authorLayer);
                if(resp.retCode === 0){
                    layer.msg("成功");
                }else{
                    layer.msg("失败");
                }
            }
        });
        console.log($func)
        return false;
    });

    // 数据渲染
    ptable=treeGrid.render({
        id:tableId,
        elem: '#table-data',
        url:'/sys/organization/list',
        cellMinWidth: 100,
        idField:'id',//必須字段
        treeId:'id',//树形id字段名称
        treeUpId:'pid',//树形父id字段名称
        treeShowName:'name',//以树形式显示的字段
        heightRemove:[".dHead",10],//不计算的高度,表格设定的是固定高度，此项不生效
        height:'100%',
        isFilter:false,
        iconOpen:true,//是否显示图标【默认显示】
        isOpenDefault:false,//节点默认是展开还是折叠【默认展开】
        loading:true,
        method:'post',
        isPage:false,
        cols: [
            [{type:'numbers',align: 'center'},
                {type:'checkbox',sort:true,align: 'center'},
                {field:'name',  title: '组织名称',align: 'center'},
                {field:'code',  title: '组织代码',align: 'center'},
                {field:'path',  title: '组织等级',align: 'center'},
                {field:'count',  title: '打标次数',align: 'center'},
                {field:'pid',  title: 'pid',align: 'center'},
                {field:'uniacid',  title: '公众号ID',align: 'center'},
                {field:'sort', title: 'sort',align: 'center',templet:function(rse){
                        return '<span class="layui-badge layui-bg-gray">'+rse.sort+'</span>';
                    }},
                {width:250,title: '操作', align:'center',
                    templet: function(rse){
                        var html='';
                        if(rse.path==="|" && rse.pid===-1){
                            html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-add"><i class="layui-icon layui-icon-add-circle-fine"></i>  新增</a>';
                            html+='<a class="layui-btn layui-btn-normal layui-btn-sm" lay-event="sbwl-edit"><i class="layui-icon layui-icon-edit"></i> 编辑</a>';
                        }else{
                            html+='<a class="layui-btn layui-btn-normal layui-btn-sm" lay-event="sbwl-edit"><i class="layui-icon layui-icon-edit"></i> 编辑</a>';
                            html+='<a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="sbwl-del"><i class="layui-icon layui-icon-delete"></i> 删除</a>';
                            html+='<a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="sbwl-author"><i class="layui-icon layui-icon-auz"></i> 授权</a>';
                        }
                        return html;
                    }
                }
            ]],
        //toolbar: '#toolbar',
        parseData:function (res) {//数据加载后回调
            return {
                "code": res.retCode, 		//解析接口状态
                "msg": res.data.message, 	//解析提示文本
                "count": res.data.total, 	//解析数据长度
                "data": res.data 			//解析数据列表
            }
        },
        onClickRow:function (index, o) {
            console.log(index,o,"单击！");
        },
        onDblClickRow:function (index, o) {
        }
    });

    // 刷新数据列表
    function table_reload(){
        treeGrid.reload('tree_grid_data');
    }

    // ~监听输入事件
    $('input[name="name"]').on('input',function(i,item){
        var val = $(this).val();
        var char =pinyin.getCamelChars(val);
        $('input[name="code"]').val("ORG_"+char);
    });

    //监听数据列表中的操作触发事件
    treeGrid.on('tool(table-filter)', function(obj){
        var data = obj.data;
        console.log(obj)

        if(obj.event === 'sbwl-add'){
            $("input[name=id]").val("");
            $("input[name=pid]").val(data.id);
            $("input[name=path]").val("|"+data.id+"|");

            //渲染select选项
            kits.select({
                elem:'#sbwl_role_select',
                url:'/sys/role/select',
                data:{"orgId":data.path==="|"?-1:data.id},
                value:'id',
                name:'name',
                form:form
            });

            // 打开编辑页面!
            showEditOrAdd('新增','#sbwl_tmpl','add');
        }else if(obj.event === 'sbwl-edit'){
            // 设置表单数据!
            form.val("sbwl_from", {
                "id":data.id,
                "name":data.name,
                "code":data.code,
                "path":data.path,
                "pid":data.pid,
                "sort":data.sort,
                "uniacid":data.uniacid,
                "count":data.count
            });

            //渲染select选项
            kits.select({
                elem:'#sbwl_role_select',
                url:'/sys/role/select',
                data:{"orgId":data.id},
                value:'id',
                name:'name',
                select:data.roleId,
                form:form
            });

            // 打开编辑页面!
            showEditOrAdd('编辑','#sbwl_tmpl','update');
        }else if(obj.event === 'sbwl-del'){
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    var data = obj.data;
                    if(data.path==="|" || data.pid===-1){
                        layer.msg("根节点不能删除");
                        return false
                    }
                    shenbi.ajax({
                        type:'post',
                        url:'/sys/organization/delete',
                        data:{ids:data.id},
                        sendMsg:'删除中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已删除");
                            }else{
                                layer.msg("删除失败");
                            }
                            query();
                        }
                    });
                }
            });
            // 授权
        }else if(obj.event === 'sbwl-author'){

            // 获取功能数据
            shenbi.ajax({
                type:'post',
                url:'/sys/function/list',
                success:function(resp){
                    loadTree(resp.data);
                }
            });

            // 保存所属机构id
            $orgId = data.id;
            showAuthor("数据授权","#sbwl_tmpl_author");

            // 获取回显数据
            shenbi.ajax({
                type:'post',
                url:'/sys/function/echo',
                data:{
                    orgId:$orgId
                },
                success:function(resp){
                    layuiXtreeCheckbox(resp.data);
                }
            });

        } else{
            layer.msg("没有相关操作");
        }

        // 数据授权
        function showAuthor(title,doc){
            authorLayer = layer.open({
                type: 1,
                title: title,
                skin: 'layui-layer-molv',
                shadeClose: false,
                area: ['650px', '500px'],
                content: $(doc),
                cancel: function(){

                }
            });
        }

        //打开编辑页面或者是新增数据页面
        function showEditOrAdd(title,doc,opr){
            layer_ = layer.open({
                type: 1,
                title: title,
                skin: 'layui-layer-molv',
                shadeClose: false,
                area: ['650px', '460px'],
                content: $(doc),
                cancel: function(){
                    $('#sbwl_from_id')[0].reset();
                }
            });
            // 设置操作类型!
            oprType = opr;
        }

        //from表单验证规则
        form.verify({
            sort: function(value){
                if(value===''){
                    return "请输入排序号";
                }
            }
        });
        //from表单监听提交
        form.on('submit(sbwl_submit)', function(data){
            // 提交后台数据!
            shenbi.ajax({
                type:'post',
                url:oprType==='add'?'/sys/organization/save':'/sys/organization/update',
                data:data.field,
                sendMsg:'提交中...',
                success:function(resp){
                    if(resp.retCode === 0){
                        layer.close(layer_);
                        if(oprType==="add"){
                            layer.msg("成功");
                        }else{
                            layer.msg("已更新");
                        }
                        // 刷新数据表
                        query();
                        // 重置表单
                        $('#sbwl_from_id')[0].reset();
                    }
                }
            });
            return false
        });
    });
});



function del(obj) {
    layer.confirm("你确定删除数据吗？如果存在下级节点则一并删除，此操作不能撤销！", {icon: 3, title:'提示'},
        function(index){//确定回调
            obj.del();
            layer.close(index);
        },function (index) {//取消回调
            layer.close(index);
        }
    );
}


var i=1000000;
//添加
function add(pObj) {
    var pdata=pObj?pObj.data:null;
    var param={};
    param.name='水果'+Math.random();
    param.id=++i;
    param.pId=pdata?pdata.id:null;
    treeGrid.addRow(tableId,pdata?pdata[treeGrid.config.indexName]+1:0,param);
}

function print() {
    console.log(treeGrid.cache[tableId]);
    var loadIndex=layer.msg("对象已打印，按F12，在控制台查看！", {
        time:3000
        ,offset: 'auto'//顶部
        ,shade: 0
    });
}

function openorclose() {
    var map=treeGrid.getDataMap(tableId);
    var o= map['102'];
    treeGrid.treeNodeOpen(tableId,o,!o[treeGrid.config.cols.isOpen]);
}


function openAll() {
    var treedata=treeGrid.getDataTreeList(tableId);
    treeGrid.treeOpenAll(tableId,!treedata[0][treeGrid.config.cols.isOpen]);
}

function getCheckData() {
    var checkStatus = treeGrid.checkStatus(tableId)
        ,data = checkStatus.data;
    layer.alert(JSON.stringify(data));
}
function radioStatus() {
    var data = treeGrid.radioStatus(tableId)
    layer.alert(JSON.stringify(data));
}
function getCheckLength() {
    var checkStatus = treeGrid.checkStatus(tableId)
        ,data = checkStatus.data;
    layer.msg('选中了：'+ data.length + ' 个');
}

function reload() {
    treeGrid.reload(tableId,{
        page:{
            curr:1
        }
    });
}
function query() {
    treeGrid.query(tableId,{
        where:{

        }
    });
}

function test() {
    console.log(treeGrid.cache[tableId],treeGrid.getClass(tableId));

}