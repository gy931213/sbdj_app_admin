
 layui.use(['form'], function () {
	var form = layui.form;
	var _xtree ='';
	// 获取功能数据
    shenbi.ajax({
		type:'post',
		url:'/sys/function/list',
		success:function(resp){
			loadTree(resp.data);
		}
	});
    
    // 渲染功能树列表数据~!
    function loadTree(array){
    	 var tree = [];
    	 var map = {};
    	 var cd_data = [];
    	 var cd_map = {};
    	 var an_data = [];
    	 var an_map = {};
    	 
    	 // 获取目录 [{type:1}]
    	 for (var i = 0; i < array.length; i++) {
    		 if(array[i].pid==-1 && array[i].type==1){
    			 map.title = array[i].name;
    			 map.value = array[i].id;
    			 
    			 // 获取菜单 [{type:2}]
    			 for (var j = 0; j < array.length; j++) {
					if(array[j].pid==array[i].id && array[j].type==2){
						cd_map.title = array[j].name;
						cd_map.value = array[j].path+array[j].id;
						
						// 获取按钮 [{type:3}]
						for (var k = 0; k < array.length; k++) {
							if(array[k].pid==array[j].id && array[k].type==3){
								an_map.title = array[k].name;
								an_map.value = array[k].path+array[k].id; 
								an_map.data = [];
							}
							
							 // 判断按钮是否为空! 
							 if(JSON.stringify(an_map)!='{}'){
								 an_data.push(an_map);
								 an_map = {};
							 }
						}
						cd_map.data = an_data;
						an_data = [];
					}
					
					 // 判断菜单是否为空! 
					 if(JSON.stringify(cd_map)!='{}'){
						 cd_data.push(cd_map);
						 cd_map = {};
					 }
				 }
    			
    			 // 将菜单赋值给根节点!
    			 map.data = cd_data;
    			 cd_data = [];
    		 }
    		 
    		// 判断目录是否为空! 
    		 if(JSON.stringify(map)!='{}'){
	    		 tree.push(map);
	    		 map = {};
    		 }
 		}
    	 
    	 _xtree = new layuiXtree({
	        elem: 'sbwl-xtree',
	        form: form,
	        data: tree,
	        isopen: false,
	        ckall: true,
	    });
    }
	    
    
    
  /*//获取全部[选中的][末级节点]原checkbox DOM对象，返回Array
    document.getElementById('btn1').onclick = function () {

        var oCks = xtree1.GetChecked(); //这是方法
        var ids = '';
        for (var i = 0; i < oCks.length; i++) {
        	var val = oCks[i].value;
        	if(val.indexOf("|")!=-1){
        		var str = val.split("|");
        		ids+= str.join();
        	}else{
        		ids+=","+val;
        	}
        	str+=oCks[i].value;
            //console.log(oCks[i].value);
        }
        console.log(ids);
        console.log(kits.cancelRepeat(ids.split(",")));
    }*/

   /* //获取全部原checkbox DOM对象，返回Array
    document.getElementById('btn2').onclick = function () {

        var oCks = xtree1.GetAllCheckBox(); //这是方法

        for (var i = 0; i < oCks.length; i++) {
            console.log(oCks[i].value);
        }
    }*/

    /*//更新数据，重新渲染
    document.getElementById('btn3').onclick = function () {
    	xtree1.render();
    }
*/
   /* //通过value值找父级DOM对象，顶级节点与错误值均返回null
    document.getElementById('btn4').onclick = function () {

        var oCks = xtree1.GetParent(document.getElementById('txt1').value);  //这是方法

        if (oCks != null) { //如果返回的对象不为null，则获取到父节点了，如果为null，说明这个值对应的节点是一级节点或是值错误
            console.log(oCks.value);
        }
        else {
            console.log('无父级节点或value值错误');
        }

    }*/
    
   
	    
	/* var xtree3 = new layuiXtree({
	       elem: 'sbwl-xtree',                  //必填
	       form: form,                    		//必填
	       data: json, 							//必填
	       isopen: false,  						//加载完毕后的展开状态，默认值：true
	       ckall: true,    						//启用全选功能，默认值：false
	       ckallback: function () { 			//全选框状态改变后执行的回调函数  
	    	 
	       },icon: {        					//三种图标样式，更改几个都可以，用的是layui的图标
	           open: "图标代号"       			//节点打开的图标
	           close: "图标代号"    				//节点关闭的图标
	           end: "图标代号"      				//末尾节点的图标
	       }, color: {       					//三种图标颜色，独立配色，更改几个都可以
	           open: "#EE9A00",        			//节点图标打开的颜色
	           close: "#EEC591",     			//节点图标关闭的颜色
	           end: "#828282",       			//末级节点图标的颜色
	       },click: function (data) {  			//节点选中状态改变事件监听，全选框有自己的监听事件
	           console.log(data.elem); 			//得到checkbox原始DOM对象
	           console.log(data.elem.checked); 	//开关是否开启，true或者false
	           console.log(data.value); 		//开关value值，也可以通过data.elem.value得到
	           console.log(data.othis); 		//得到美化后的DOM对象
	       }
	});*/
 });
 
	// 根据角色id（roleId）获取角色对应的权限数据！ 
	function getRoleFunctionByRoleId(roleId){
		// 获取功能数据
	    shenbi.ajax({
			type:'post',
			url:'/sys/function/byroleid',
			data:{roleId:roleId},
			success:function(resp){
				var data = eval('(' + resp + ')');
				console(data.data);
			}
		});
	}
 
 