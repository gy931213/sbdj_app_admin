
layui.use(['table','jquery','form'], function(){
  var table = layui.table;
  var form  = layui.form;
  var $=layui.jquery;
  var _xtree ='';
  
  //操作类型[add|update],默认为add
  var oprType = 'add';
  var layer_ = ''; 
  // 角色id
  var roleId = '';
  
  // 表格渲染数据
  table.render({
    elem: '#table-data',
	url:'/sys/role/list',
	toolbar: '#toolbar',
	title: '角色数据表',
	id:'table_list',
	cols: [[
		{type:'checkbox',sort:true,align: 'center'},
		  {type:'numbers',title:'序号',align: 'center',width:'5%'},
	  {field:'orgName', title:'所属组织',align: 'center'},
	  {field:'name', title:'角色名称',align: 'center'},
	  {field:'code', title:'角色代码',align: 'center'},
	  {field:'description', title:'角色描述', align: 'center'},
	/*  {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
	  }},*/
	  {fixed: 'right', title:'操作', toolbar: '#operation', width:300,align: 'center'}
    ]],
    limit:30,
    limits:[30,50,70,80],
    parseData: function(res){ 
    	//console.log(res)
        return {
          "code": res.retCode, 		//解析接口状态
          "msg": res.data.message, 	//解析提示文本
          "count": res.data.total, 	//解析数据长度
          "data": res.data.list 	//解析数据列表
        }
    },
	page: true
  });
  
  //监听搜索按钮
  form.on('submit(sbwl_search)', function(data){
    //layer.msg(JSON.stringify(data.field));
    table.reload('table_list',{
    	page: {
            curr: 1 //重新从第 1 页开始
         },
         where:data.field
    });
    return false;
  });
  
  //监听保存按钮
  form.on('submit(sbwl_save)', function(data){
	  var _ids = '';
	  var oCks = _xtree.GetChecked();
      for (var i = 0; i < oCks.length; i++) {
    	  var val = oCks[i].value;
    	  if(val.indexOf("|")!=-1){
    		  var str = val.split("|");
    		  _ids+= str.join();
    	  }else{
    		  _ids+=_ids==""?val:","+val;
    	  }
      }
      
      if(roleId==null || roleId==""){
	    	layer.msg("请选择对应的角色"); 
	    	return false
	  }
      if(_ids==null || _ids==""){
	    	layer.msg("请勾选对应的权限"); 
	    	return false
	  }
     // 功能权限id
     var _funcIds = kits.cancelRepeat(_ids.split(","));
     shenbi.ajax({
			type:'post',
			url:'/sys/role/save/rf',
			data:{
				roleId:roleId,
				funcIds:_funcIds
			},
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.msg("成功"); 
				}else{
					layer.msg("失败");
				}
			}
		});
    return false;
  });
  
  // 刷新列表
  function table_reload(){
	  table.reload('table_list');
  }
  
    //渲染select选项 
	kits.select({
		elem:'#sbwl_org_select_1',
		url:'/sys/organization/select',
		value:'id',
		name:'name',
		form:form
	});
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
		case 'sbwl_dels':
		    var data = checkStatus.data;
		    var ids = [];
		    $.each(data,function(index,item){
			  ids.push(item.id);
		    });
		    if(ids.length<=0){
		    	layer.msg("至少选择一项进行操作"); 
		    	return false
		    }
  	    	layer.msg('确定要批量删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/sys/role/deletes',
	    				data:{ids:ids.join()},
	    				sendMsg:'删除中...',
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
    		break;
      case 'sbwl_create':
    	  	$('#sbwl_from_id')[0].reset();
    	  	 $("input[name=id]").val("");
    	  	 
    	  	//渲染select选项 
	    	 kits.select({
	    		 elem:'.sbwl_org_select',
	    		 url:'/sys/organization/select',
	    		 value:'id',
	    		 name:'name',
	    		 form:form
	    	 });
    	  	 
    	  	// 新增页面
			showEditOrAdd('新增角色','#sbwl_tmpl','add');
        break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
	    var data = obj.data;
	    //console.log(obj)
	    if(obj.event === 'sbwl-set'){
	    	roleId = data.id;
	    	 //标注选中样式
		    obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
		    if(roleId==null || roleId===""){
		    	return false
		    }
		    // 根据角色id获取角色对应的权限数据的函数！
		    getRoleFunctionByRoleId(roleId);
	    } else if(obj.event === 'sbwl-edit'){
	    	 // 设置表单数据!
	   	    form.val("sbwl_from", {
	   	      "id":data.id,
	   	      "name":data.name,	
	   	      "code":data.code,
			  "description":data.description
			});
	   	    
	   	    // 渲染select选项 
	    	 kits.select({
	    		 elem:'.sbwl_org_select',
	    		 url:'/sys/organization/select',
	    		 value:'id',
	    		 name:'name',
	    		 select:data.orgId,
	    		 form:form
	    	 });
	   	    
	    	// 打开编辑页面!
			showEditOrAdd('编辑角色','#sbwl_tmpl','update');
	    }else if(obj.event === 'sbwl-del'){
	    	layer.msg('确定要删除吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	var data = obj.data;
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/sys/role/delete',
	    				data:{id:data.id},
	    				sendMsg:'删除中...',
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已删除"); 
	    					}else{
	    						layer.msg("删除失败");
	    					}
	    					table_reload();
	    				}
	    			});
	    	    }
	    	});
	    }else{
	    	layer.msg("没有相关操作");
	    }
  	});
  
	  //打开编辑页面或者是新增数据页面
	  function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
	        type: 1,
	        title: title,
	        skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['650px', '420px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		oprType = opr;
	 }
	  
		//from表单验证规则
		form.verify({
			orgId: function(value){
				if(value=='0'){
					return "请选择对应组织";
				}
			}
		});
		
		// 监听input值改变事件~
		$('input[name="name"]').bind('input propertychange', function() {  
		    $('input[name="code"]').val(pinyin.getCamelChars($(this).val()));  
		});

	  //from表单监听提交
	  form.on('submit(sbwl_submit)', function(data){
	   // 提交后台数据!
		shenbi.ajax({
			type:'post',
			url:oprType==='add'?'/sys/role/save':'/sys/role/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.close(layer_);
					if(oprType==="add"){
						layer.msg("成功"); 
					}else{
						layer.msg("已更新"); 
					}
					table_reload();
					// 重置表单
					$('#sbwl_from_id')[0].reset();
				}
			}
		});
	    return false;
	  });
	  
  	// 获取功能树数据~
    shenbi.ajax({
		type:'post',
		url:'/sys/function/authorize',
		success:function(resp){
			loadTree(resp.data);
		}
	});
	  
    // 渲染功能树列表数据~!
    function loadTree(array){
    	 var tree = [];
    	 var map = {};
    	 var cd_data = [];
    	 var cd_map = {};
    	 var an_data = [];
    	 var an_map = {};
    	 
    	 // 获取目录 [{type:1}]
    	 for (var i = 0; i < array.length; i++) {
    		 if(array[i].pid==-1 && array[i].type==1){
    			 map.title = array[i].name;
    			 map.value = array[i].id;
    			 
    			 // 获取菜单 [{type:2}]
    			 for (var j = 0; j < array.length; j++) {
					if(array[j].pid==array[i].id && array[j].type==2){
						cd_map.title = array[j].name;
						cd_map.value = array[j].path+array[j].id;
						
						// 获取按钮 [{type:3}]
						for (var k = 0; k < array.length; k++) {
							if(array[k].pid==array[j].id && array[k].type==3){
								an_map.title = array[k].name;
								an_map.value = array[k].path+array[k].id; 
								an_map.data = [];
							}
							
							 // 判断按钮是否为空! 
							 if(JSON.stringify(an_map)!='{}'){
								 an_data.push(an_map);
								 an_map = {};
							 }
						}
						cd_map.data = an_data;
						an_data = [];
					}
					
					 // 判断菜单是否为空! 
					 if(JSON.stringify(cd_map)!='{}'){
						 cd_data.push(cd_map);
						 cd_map = {};
					 }
				 }
    			
    			 // 将菜单赋值给根节点!
    			 map.data = cd_data;
    			 cd_data = [];
    		 }
    		 
    		// 判断目录是否为空! 
    		 if(JSON.stringify(map)!='{}'){
	    		 tree.push(map);
	    		 map = {};
    		 }
 		}
    	 
    	 _xtree = new layuiXtree({
	        elem: 'sbwl-xtree',
	        form: form,
	        data: tree
	    });
    }  
	  
   // 根据角色id（roleId）获取角色对应的权限数据！ 
	function getRoleFunctionByRoleId(roleId){
		// 获取功能数据
	    shenbi.ajax({
			type:'get',
			url:'/sys/function/byroleid',
			data:{roleId:roleId},
			success:function(resp){
				// 重新渲染功能树数据
				_xtree.render();
				if(resp!=""){
					layuiXtreeCheckbox(resp.data);
				}
			}
		});
	}
		
	function layuiXtreeCheckbox(array){
		// 在渲染数据之前删除历史数据
		//$("#sbwl-xtree .layui-form-checkbox").removeClass("layui-form-checked");
		//~目录
		for (var i = 0; i < array.length; i++) {
			if(array[i].type==1){
				var _this = $("#sbwl-xtree").find("input[value='"+array[i].id+"']");
					_this.attr("checked", true);
				var _next = _this.next();
					_next.addClass("layui-form-checked");
			}
		}
		//~菜单
		for (var j = 0; j < array.length; j++) {
			if(array[j].type==2){
				var _this = $("#sbwl-xtree").find("input[value='"+array[j].path+array[j].id+"']");
					_this.attr("checked", true);
				var _next = _this.next();
					_next.addClass("layui-form-checked");
			}
		}
		//~按钮
		for (var k = 0; k < array.length; k++) {
			if(array[k].type==3){
				var _this = $("#sbwl-xtree").find("input[value='"+array[k].path+array[k].id+"']");
					_this.attr("checked", true);
				var _next = _this.next();
					_next.addClass("layui-form-checked");
			}
		}
	}
		
});