layui.use(['table','form','laydate'], function(){
    var table = layui.table;
    var form  = layui.form;
    var laydate = layui.laydate;
    var $=layui.jquery;

    //开始日期
    laydate.render({
        elem: '#beginDate',
        type: 'date'
    });
    //结束日期
    laydate.render({
        elem: '#endDate',
        type: 'date'
    });

    table.render({
        elem: '#table-data',
        url:'/sys/admin/api/list',
        toolbar: '#toolbar',
        title: '管理员数据表',
        id:'table_list',
        totalRow: true,
        cols: [[
            {type:'checkbox',sort:true,align: 'center'},
            {type:'numbers',title:'序号',align: 'center',width:'5%', totalRowText: '合计'},
            {field:'name', title:'用户名称',align: 'center'},
            {field:'count', title:'调用次数',align: 'center',totalRow:true},
            {field:'createTime', title:'调用时间', align: 'center',templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd",rse.createTime);
                }},
        ]],
        limit:50,
        limits:[10,20,30,50,100,200,500,1000],
        parseData: function(res){
            return {
                "code": res.retCode, 		//解析接口状态
                "msg":  res.message, 	    //解析提示文本
                "count": res.data.total, 	//解析数据长度
                "data": res.data.list 	//解析数据列表
            }
        },
        page: true
    });

    //监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });
        return false;
    });

    //渲染select选项
    kits.select({
        elem:'#sbwl_org_select_1',
        url:'/sys/organization/select',
        value:'id',
        name:'name',
        form:form
    });
});