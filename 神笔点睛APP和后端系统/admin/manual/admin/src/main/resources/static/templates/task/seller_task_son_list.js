
// 
layui.use(['table','laydate','form'], function(){
  var table = layui.table;
  var laydate = layui.laydate;
  var form  = layui.form;
  // 状态
  var statuss = [
	  {'value':'A','name':'待操作'},
	  {'value':'F','name':'待提交'},
	  {'value':'T','name':'已审核'},
	  {'value':'AU','name':'已传图'},
	  {'value':'AN','name':'不通过'},
	  {'value':'AF','name':'失败'}
  ];
  
  // 是否重审 [是：true|否：false]
  var isRetrial = true;
  
  // 订单状态
  function orderStatus(key){
	  switch (key) {
	case 0:
		  //0：待付款
		return "待付款";
		break;
	case 1:
		  //1：待发货
		return "待发货";
		break;
	case 2:
		  //2：待收货
		return "待收货";
		break;
	case 3:
		  //3：已完成 
		return "已完成";
		break;
	case -1:
		  //-1：已关闭
		return "已关闭";
		break;
	default:
		return "默认";
		break;
	}
  }
  
   // 快链字段设置样式
   var key = ['orderNum','shopName','onlineid'];
  
  shenbi.select({
	  elem:'#taskStatus',
	  data:statuss,
	  form:form,
  });
  
	//开始日期
	laydate.render({
		elem: '#beginDate',
		type: 'datetime'
	});
	//结束日期
	laydate.render({
		elem: '#endDate',
		type: 'datetime'
	});
	
   // 表格
  table.render({
	id:'task-table-list',
    elem:'#table-data',
	url:'/seller/task/son/list',
	toolbar: '#toolbar',
	title: '子任务数据表',
	cols: [[
	  {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
	  {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
	  {field:'taskNumber', title:'任务单号',hide:true},
	  {field:'taskSonNumber', title:'子任务单号', hide:true},
	  {field:'taskTypeName', title:'任务类型',hide:true},
	  {field:'orderNum', title:'订单号',event:'search-orderNum',edit: 'text'},
	  {field:'platformName', title:'所属平台',width:90,align: 'center'},
	  {field:'shopName', title:'店铺名称',event:'search-shopName'},
	  {field:'title', title:'商品标题',hide:true},
	  {field:'keyword', title:'搜索词'},
	  {field:'onlineid', title:'号主ID',event:'search-onlineid'},
	  {field:'buyerNick', title:'买家昵称'},
	  {field:'realPrice', title:'应付金额', width:100,align: 'center'},
	  {field:'payment', title:'实付金额', width:100,align: 'center'},
	  {field:'orderStatus', title:'订单状态', width:100,align: 'center',templet:function(rse){
		  return orderStatus(rse.orderStatus);
	  }},
	  {field:'createTime', title:'领取时间',align: 'center',hide:true,templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
	  }},
	  {fixed: 'right', title:'操作', width:250,templet:function(rse){
		  var _html = '';
		  if((rse.status != 'A' && rse.status !='F')){
			  if(rse.orderMatch === 'YES'){
				  _html+='<a class="layui-btn layui-btn-sm" style="background-color: #999;" lay-event="sbwl-auditing-order"><i class="layui-icon">&#xe605;</i> 对单</a>';  
			  } else{
				  _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-auditing-order"><i class="layui-icon">&#xe605;</i> 对单</a>';
			  }
		  }
	   return _html;
	  }}
    ]],
    limit:30,
    limits:[30,50,100,200,500,1000],
    parseData: function(res){ 
        return shenbi.returnParse(res);
    },
	page: true
  });
  	
  	 // 设置表头有快联的样式
  	 tableThStyle(key);
  
	  // ~监听搜索按钮
	  form.on('submit(sbwl-taskSon-search)', function(data){
	    table.reload('task-table-list',{
	    	page: {
	            curr: 1 //重新从第 1 页开始
	         },
	         where:data.field
	    });
	    
	      // 监听指定开关
		  form.on('switch(switchHistory)', function(data){
			if(this.checked){
				$('input[name="isHistory"]').val('YES');
				layer.tips('温馨提示：开启历史数据查询！', data.othis);
			}else{
				$('input[name="isHistory"]').val('NO');
				layer.tips('温馨提示：查询前 5 天的数据！', data.othis);
			}
		  });
	    
	     // 设置表头有快联的样式
	  	 tableThStyle(key);
	    return false;
	  });
	  
	  // 重置
	  form.on('submit(sbwl-reset)',function(){
		  shenbi.select({
			  elem:'#taskStatus',
			  data:statuss,
			  form:form,
		  });
		// 设置表头有快联的样式
		 tableThStyle(key);
	  })
  
  	 //渲染select选项 (平台)
	 kits.select({
		 elem:'#sbwl-platform-select',
		 url:'/config/platform/select',
		 value:'id',
		 name:'name',
		 form:form
	 });
  
	 // ~渲染任务类型
   	 kits.select({
		 elem:'#sbwl-taskType-select',
		 url:'/base/code',
		 data:{code:'TASK_TYPE'},
		 value:'id',
		 name:'name',
		 form:form,
	 });
  // ~刷新列表
  function table_reload(){
	  table.reload('task-table-list');
	   // 设置表头有快联的样式
	   tableThStyle(key);
  }
  
  //注：edit是固定事件名，test是table原始容器的属性 lay-filter="对应的值"
  // 监听数据列表中的值更新
  table.on('edit(table-filter)', function(obj){
	    shenbi.ajax({
			type:'post',
			url:'/task/son/update/ordernum',
			data:{
				taskSonId : obj.data.id, 			// 子任务id
				platformId : obj.data.platformId,   // 平台id
				orderNum : obj.value			    // 要更新订单号
			},
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.msg("已更新！");
				}else{
					layer.msg(resp.message == "" ? "更新失败！" : resp.message);
				}
				// ~刷新数据
				table_reload();
			}
		 });
  });  
  
  
  
  
  // ~头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'sbwl-auditing-select':
    	  // ~审核所选任务
    	  var data = checkStatus.data;
    	  if(data.length <= 0){
    		  layer.msg("必须选中一项进行操作");  
    		  return false;
    	  }
    	  // ~获取所选中的数据
    	  var ids = [];
    	  $.each(data,function(i,item){
    		  ids.push(item.id);
    	  });
    	  if(ids.length <= 0){
    		  layer.msg("必须选中一项进行操作");  
    		  return false;
    	  }
    	  // ~批量审核任务
    	  layer.msg('确定批量审核吗？', {
    	    btn: ['确定', '取消'],
    	    yes: function(index, layero){
    	    	shenbi.ajax({
    				type:'POST',
    				url:'/task/son/auditors',
    				data:{ids:ids.join()},
    				sendMsg:'审核中...',
    				success:function(resp){
    					if(resp.retCode === 0){
    						layer.msg("已审核");
    					}else{
    						layer.msg("审核失败");
    					}
    					// ~刷新数据
    					table_reload();
    				}
    			 });
    	     }
		 });
      break;
      case 'orders':
    	  // 批量选中要对单的任务数据! 
    	  var data = checkStatus.data;
    	  if(data.length < 2){
    		  layer.msg("必须选中2项进行操作！");  
    		  return false;
    	  }
    	  // 获取所选中的数据
    	  var ids = [];
    	  $.each(data,function(i,item){
    		  ids.push(item.id);
    	  });
    	  if(ids.length < 2){
    		  layer.msg("必须选中2项进行操作！");  
    		  return false;
    	  }
    	  // 批量对单
    	  layer.msg('确定批量对单吗？', {
    	    btn: ['确定', '取消'],
    	    yes: function(index, layero){
    	    	var html = '<ul class="layer_notice" style="display: block;background: #279a3fc4;">';
    	    	shenbi.ajax({
    				type:'POST',
    				url:'/task/son/matchan/orders',
    				data:{taskSonId:ids.join()},
    				sendMsg:'对单中...',
    				success:function(resp){
    					if(resp.retCode === 0){
    						var data = resp.data;
    						html += ' <li>&nbsp;<a style="color: #efeaea"> 1. '+data.success+'</a></li>';
    						html += ' <li>&nbsp;<a style="color: #efeaea"> 2. '+data.notUniacid+'</a></li>';
    						html += ' <li>&nbsp;<a style="color: #efeaea"> 3. '+data.failOrder+'</a></li>';
    						html += '</ul>';
    						layer.open({
							  type: 1,
							  shade: false,
							  title: false, //不显示标题
							  content: html, 
							  cancel: function(){
							  }
							});
    					}else{
    						layer.msg("对单失败");
    					}
    					// ~刷新数据
    					table_reload();
    				}
    			 });
    	     }
		 });
      break;
    };
  });
  
	// ~监听数据列表中的操作相关事件~
	table.on('tool(table-filter)', function(obj){
		var data = obj.data;
		if(obj.event === 'sbwl-auditing'){
			// ~子任务审核
			$("#task_id").val(data.id);
			$("#status").val(data.status);
			// ~打开审核任务窗口
			showAuditorTask("审核","#sbwl_auditorTask_tmpl");
			// 标志为【审核】
			isRetrial = false;

            //渲染select选项 (平台)
            kits.select({
                elem:'#sbwl_reason_select',
                url:'/task/common/sentence/select',
                data:{platformId:data.platformId},
                value:'content',
                name:'content',
                form:form
            });
		}else if(obj.event === 'sbwl-check-img'){
			
			// ~表单赋值
			form.val("sbwl_task_son_img_from", {
				"imei":data.imei,
				"shopName":data.shopName,
				"keyword":data.keyword,
				"realPrice":data.realPrice,
				"onlineid":data.onlineid
			});
				// ~查看任务截图
			 	// ~获取业务员相关的证件图片~
		        shenbi.ajax({
			  		type:'post',
			  		url:'/task/son/find/imgs',
			  		data:{taskSonId:data.id},
			  		success:function(resp){
			  			if(resp.retCode === 0){
			  				var data = resp.data;
			  				var _html = '</br>';
			  				for (var i = 0; i < data.length; i++) {
			  					_html+='<div class="layui-form-item task-div-son-img-li">';
			  					_html+='	<div class="sbwl-task-son-img">';
			  					_html+='		<img id="sbwl-img" alt="" src="'+data[i].link+'" style="width: 400px">';
			  					_html+='		<div class="scr"><b>'+data[i].typeName+'</b></div>';
			  					_html+='	</div>';
			  					_html+='</div>';
			  				}
			  				// ~渲染数据~
			  				$("#sbwl_taskSon_tmpl_img").html(_html);
			  			    // ~渲染数据~   
			  			    showTaskSonImgSrc('<i class="layui-icon">&#xe64a;</i> 任务截图','#sbwl_taskSon_tmpl');
			  			}
			  		}
			  	});   
		}else if(obj.event === 'sbwl-back-auditing'){
			//layer.msg("反审核");
			//~重审核操作
			// ~子任务审核
			$("#task_id").val(data.id);
			$("#status").val(data.status);
			// ~打开审核任务窗口
			showAuditorTask("重审","#sbwl_auditorTask_tmpl");
			// 标志为【重审】
			isRetrial = true;

            //渲染select选项 (平台)
            kits.select({
                elem:'#sbwl_reason_select',
                url:'/task/common/sentence/select',
                data:{platformId:data.platformId},
                value:'content',
                name:'content',
                form:form
            });
		}else if(obj.event === 'is-time-out'){
			// 将任务超时更新为不超时
			 layer.msg('确定要更新为【否】吗？', {
	    	    btn: ['确定', '取消'],
	    	    yes: function(index, layero){
	    	    	shenbi.ajax({
	    				type:'post',
	    				url:'/task/son/timeout',
	    				data:{
	    					id:data.id,
	    					isTimeOut:'NO'
	    				},
	    				sendMsg:'提交中...',
	    				success:function(resp){
	    					if(resp.retCode === 0){
	    						layer.msg("已更新");
	    					}else{
	    						layer.msg("更新失败");
	    					}
	    					// ~刷新数据
	    					table_reload();
	    				}
	    			 });
	    	     }
			 });
		} else if(obj.event === 'sbwl-auditing-order'){
			// ~对单操作
			 shenbi.ajax({
			  		type:'post',
			  		url:'/task/son/matchan/orde',
			  		data:{taskSonId:data.id},
			  		sendMsg:'对单中...',
			  		success:function(resp){
			  			if(resp.retCode === 0){
			  				layer.msg("已对单"); 
			  			}else{
			  				layer.msg("对单失败");
			  			}
			  			// ~刷新数据
    					table_reload();
			  		}
			  });
			 
		// ~超链接查询	 
		}else if(obj.event === 'search-shopName'){
			// 模拟鼠标双击
			if(!dblclick()){
	    		return false;
	    	}
			// 店铺名称
			table.reload('task-table-list',{
			      where:{'shopName':data.shopName}
			 });
			$("input[name='shopName']").val(data.shopName);
		}else if(obj.event === 'search-orderNum'){
			// 模拟鼠标双击
			if(!dblclick()){
	    		return false;
	    	}
			// 店铺名称 
			table.reload('task-table-list',{
			      where:{'orderNum':data.orderNum}
			});
			$("input[name='orderNum']").val(data.orderNum);
		}else if(obj.event === 'search-salesmanName'){
			// 模拟鼠标双击
			if(!dblclick()){
	    		return false;
	    	}
			// 业务员名称
			table.reload('task-table-list',{
			      where:{'salesmanName':data.salesmanName}
			});
			$("input[name='salesmanName']").val(data.salesmanName);
		}else if(obj.event === 'search-onlineid'){
			// 模拟鼠标双击
			if(!dblclick()){
	    		return false;
	    	}
			// 号主id 
			table.reload('task-table-list',{
			      where:{'onlineid':data.onlineid}
			});
			$("input[name='onlineid']").val(data.onlineid);
		}else if(obj.event === 'search-auditorName'){
			// 模拟鼠标双击
			if(!dblclick()){
	    		return false;
	    	}
			// 审核人 
			table.reload('task-table-list',{
			      where:{'auditorName':data.auditorName}
			 });
			$("input[name='auditorName']").val(data.auditorName);
		}else if(obj.event === 'search-status'){
			// 模拟鼠标双击
			if(!dblclick()){
	    		return false;
	    	}
			// 标注选中
			 shenbi.select({
				  elem:'#taskStatus',
				  data:statuss,
				  form:form,
				  select:data.status
			 });
			// 状态
			table.reload('task-table-list',{
			      where:{'status':data.status}
			 });
		}else{
			layer.msg("没有具体操作事件");
		}
		
	  	 // 设置表头有快联的样式
	  	 tableThStyle(key);
	});
	
	// ~根据任务id获取任务相关的搜索词数据
	function getTaskKeywordDataByTaskId(el,taskId){
		shenbi.ajax({
			type:'post',
			url:'/task/findkeyword',
			data:{taskId:taskId},
			success:function(resp){
				if(resp.retCode ===0){
					// ~查看
					if(el==='check'){
						// ~渲染搜索词数据
						renderingTaskKeywordHtmlCheck(resp.data);	
					}
					// ~编辑
					if(el==='edit'){
						// ~渲染搜索词数据
						renderingTaskKeywordHtmlEdit(resp.data);
					}
				}else{
					layer.msg("load失败");
				}
			}
		});
	}
	// ~渲染任务搜索词数据的函数（查看）
	function renderingTaskKeywordHtmlCheck(data){
		$("#sbwl-list-div-check").html("");
		$(".sbwl-task-keyword-li").remove();
		for (var i = 0; i < data.length; i++) {
			var _html = '';
			if(i===0){
				_html+='	<div class="layui-inline" style="width: 286px">';
				_html+='		<input type="text" class="layui-input input-check" value="'+data[i].keyword+'" disabled="disabled" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
				_html+='	</div>';
				_html+='	<div class="layui-inline" style="width: 200px">';
				_html+='		<input type="text" class="layui-input input-check" value="'+data[i].number+'" disabled="disabled" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
				_html+='	</div>';
				// ~渲染搜索词第[0]~
				$("#sbwl-list-div-check").html(_html);
			}else{
				_html+='<div class="layui-input-block sbwl-task-keyword-li sbwl-task-keyword">';
				_html+='	<div class="layui-inline" style="width: 286px">';
				_html+='		<input type="text" class="layui-input task-key input-check" value="'+data[i].keyword+'" disabled="disabled" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
				_html+='	</div>';
				_html+='	<div class="layui-inline" style="width: 200px">';
				_html+='		<input type="text" class="layui-input task-number input-check" value="'+data[i].number+'" disabled="disabled" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
				_html+='	</div>';
				_html+='</div>';
				// ~渲染搜索词~
				$("#sbwl-list-div-check").after(_html);
			}
		}
	}
	
	// ~渲染任务搜索词数据的函数（编辑）
	function renderingTaskKeywordHtmlEdit(data){
		$("#sbwl-list-div-edit").html("");
		$(".sbwl-task-keyword-li").remove();
		for (var i = 0; i < data.length; i++) {
			var _html = '';
			if(i===0){
				_html+='	<div class="layui-inline" style="width: 286px">';
				_html+='		<input type="text" class="layui-input task-key" value="'+data[i].keyword+'" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
				_html+='	</div>';
				_html+='	<div class="layui-inline" style="width: 200px">';
				_html+='		<input type="text" class="layui-input task-number" value="'+data[i].number+'" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
				_html+='	</div>';
				_html+='	<div class="layui-inline ss-btn" id="js-click-btn" style="width: 30px">';
				_html+='		<a class="layui-btn"><i class="layui-icon">&#xe608;</i></a>';
				_html+='	</div>';
				// ~渲染搜索词第[0]~
				$("#sbwl-list-div-edit").html(_html);
			}else{
				_html+='<div class="layui-input-block sbwl-task-keyword-li sbwl-task-keyword">';
				_html+='	<div class="layui-inline" style="width: 286px">';
				_html+='		<input type="text" class="layui-input task-key" value="'+data[i].keyword+'" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
				_html+='	</div>';
				_html+='	<div class="layui-inline" style="width: 200px">';
				_html+='		<input type="text" class="layui-input task-number" value="'+data[i].number+'" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
				_html+='	</div>';
				_html+='	<div class="layui-inline ss-btn sbwl-click-del"  style="width: 30px">';
				_html+='		<a class="layui-btn layui-btn-danger"><i class="layui-icon">&#x1006;</i></a>';
				_html+='	</div>';
				_html+='</div>';
				// ~渲染搜索词~
				$("#sbwl-list-div-edit").after(_html);
			}
		}
	}
	 // ~添加关键词搜索输入框~
	  $(document).on('click','#js-click-btn',function(){
		  var _html = '';
		  _html+='<div class="layui-input-block sbwl-task-keyword">';
		  _html+='	<div class="layui-inline" style="width: 286px">';
		  _html+='		<input type="text" class="layui-input task-key" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
		  _html+='	</div>';
		  _html+='	<div class="layui-inline" style="width: 200px">';
		  _html+='		<input type="text" class="layui-input task-number" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
		  _html+='	</div>';
		  _html+='	<div class="layui-inline ss-btn sbwl-click-del"  style="width: 30px">';
		  _html+='		<a class="layui-btn layui-btn-danger"><i class="layui-icon">&#x1006;</i></a>';
		  _html+='	</div>';
		  _html+='</div>';
		  
		  // ~渲染搜索词~
		  $("#sbwl-list-div-edit").after(_html);
	  });
	
	 // ~删除当前点击的搜索词
	  $(document).on('click','.sbwl-click-del',function(){
		  // ~删除
		  $(this).parent().remove();
		  // ~获取到搜索词的任务数~
		  eachTaskNumberFunc();
	  });
	  
	  // ~监听任务数输入的事件~
	  $(document).bind('input','.task-number',function(){
		  // ~遍历搜索词的任务数~
		  eachTaskNumberFunc();
	  });
	  
	  // ~遍历搜索词的任务数的函数~
	  function eachTaskNumberFunc(){
		  // ~获取到搜索词的任务数~
		  var taskTotal = 0; // ~总任务数
		  $("input[name='number']").each(function(i,item){
		    console.log(item.value); 
		    if(item.value!="" && item.value!=null){
		    	taskTotal+=parseInt(item.value);	
		    }
		  });
		  // ~渲染总任务数~
		  $("input[name='taskTotal']").val(taskTotal);
	  }
	  
	  // ~遍历搜索词和搜索词的任务数的函数~
	  function eachTaskKeyword(){
		  var array = [];
		   // ~
		  $(".sbwl-task-keyword").each(function(i,item){
			 var map = {};
			 map.keyword = $(this).find('input[name="keyword"]').val();
			 map.number =$(this).find('input[name="number"]').val();
			 array.push(map);
			 map = {};
		  });
		  return JSON.stringify(array);
	  }
  
  	//~添加商品任务的弹出层~
	var task_layer;
	function showCreateTask(title,doc){
		task_layer = layer.open({
			type: 1,
		    title: title,
		    skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['620px', '730px'],
			content: $(doc),
			cancel: function(){
				
			}
		});
		layer.full(task_layer);
	}
	
	// ~from表单验证规则
	form.verify({
		taskTypeId: function(value){
			if(value==0){
				return "请选择对应任务类型";
			}
		}
	});
  
	// ~from表单监听提交
	form.on('submit(sbwl_task_submit)', function(data){
		var data = data.field;
  			data.taskKeyword = eachTaskKeyword();
	   // 提交后台数据!
		shenbi.ajax({
			type:'post',
			url:'/task/update',
			data:data,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.msg("已更新"); 
					// 重置表单
					$('#sbwl_editTask_id')[0].reset();
				}else{
					// 错误提示
					layer.msg(resp.message); 
				}
				// 刷新数据表
				table_reload();
				layer.close(task_layer);
			}
		});
		return false
	  });
	
	//~查看子任务截图的弹出层~
	function showTaskSonImgSrc(title,doc){
		var layer_ = layer.open({
			type: 1,
		    title: title,
		    skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['620px', '730px'],
			content: $(doc),
			cancel: function(){
				
			}
		});
	}
	
 	// ~审核任务窗口
	var _layer
	function showAuditorTask(title,doc){
		_layer= layer.open({
	    type: 1,
		    title: title,
		    skin: 'layui-layer-molv',
			area: ['638px', '345px'],
			content: $(doc),
			cancel: function(){
	            $("#sbwl_auditorTask_id")[0].reset();
                $(".js-hidden").addClass("sbwl-hidden");
			}
		});
	}
	
	// ~审核任务from表单监听提交
	form.on('submit(sbwl_auditorTask_submit)', function(data){

		if(null!=data && data.field.endStatus!="T" && data.field.account == ""){
            layer.msg('审批原因不能为空!', {icon: 0});
			return false;
		}

    	shenbi.ajax({
			type:'post',
			url: isRetrial == true ? '/task/son/retrial' : '/task/son/auditor',
			data:data.field,
			sendMsg: isRetrial == true ? '重审中...' : '审核中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.msg("已审核");
				}else{
					layer.msg("审核失败");
				}
				// ~刷新数据
				table_reload();
                $("#sbwl_auditorTask_id")[0].reset();
                $(".js-hidden").addClass("sbwl-hidden");
				layer.close(_layer);
			}
		});
		return false
	  });

    form.on('radio(btn)', function(data){
        if (data.value == 'T') {
            $(".js-hidden").addClass("sbwl-hidden");
        } else {
            $(".js-hidden").removeClass("sbwl-hidden");
        }
    });

    form.on('select(reason)', function(data){
        $("#account").val(data.value == 0 ? "" : data.value);
    });

});