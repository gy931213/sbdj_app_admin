
/**
 * 根据参数拼接选中的Checkbox所需的html 2019/05/03
 * @param isCard      是否进行打标：[{0 ：否}，{1 ：是}]
 * @param isCardIndex 打标类型：[{0 ：否},{1 ：是},{2 ：自动}]
 * @param isDisabled  是否禁用：[{false ： 否},{true ： 是}]
 * @returns
 */
function loadCheckedCheckboxHtml(isCard,isCardIndex,isDisabled){
	var _html = '';
	var disabled =  isDisabled ? "disabled" : "";
	if(isCard == 1){
        _html+='	<div class="layui-inline select-item" style="width: 300px;height: 40px">';
        if(isCardIndex === 0){
    	   _html+='		<input type="checkbox" class="select-no" value="0" title="否" checked '+disabled+'>';
           _html+='		<input type="checkbox" class="select-yes" value="1" title="是" '+disabled+'>';
           _html+='		<input type="checkbox" class="select-auto" value="2" title="自动" '+disabled+'>';
        }else if(isCardIndex === 1){
    	   _html+='		<input type="checkbox" class="select-no" value="0" title="否" '+disabled+'>';
           _html+='		<input type="checkbox" class="select-yes" value="1" title="是" checked '+disabled+'>';
           _html+='		<input type="checkbox" class="select-auto" value="2" title="自动" '+disabled+'>';
        }else if(isCardIndex === 2){
    	   _html+='		<input type="checkbox" class="select-no" value="0" title="否" '+disabled+'>';
           _html+='		<input type="checkbox" class="select-yes" value="1" title="是" '+disabled+'>';
           _html+='		<input type="checkbox" class="select-auto" value="2" title="自动" checked '+disabled+'>';
        }
        _html+='	</div>';
    }
	return _html;
};

/**
 * 根据参数拼接一个Checkbox的html 2019/05/03
 * @param isCard      是否进行打标：[{0 ：否}，{1 ：是}]
 * @returns
 */
function loadOneCheckedCheckboxHtml(isCard){
	var _html = '';
	if(isCard == 1){
        _html+='	<div class="layui-inline select-item" style="width: 300px;height: 40px">';
	    _html+='		<input type="checkbox" class="select-no" value="0" title="否">';
        _html+='		<input type="checkbox" class="select-yes" value="1" title="是" checked>';
        _html+='		<input type="checkbox" class="select-auto" value="2" title="自动">';
        _html+='	</div>';
    }
	return _html;
};

/**
 * 全选【否\是\自动】的checkbox选项.
 * @param  form 表单对象.
 * @returns
 */
function loadCheckAllClick(form){
	// 全选【否】
    form.on('checkbox(select-all-no)', function (data) {
        var a = data.elem.checked;
        if (a == true) {
            $(".select-no").prop("checked", true);
        } else {
            $(".select-no").prop("checked", false);
        }
        
        // 取消其他全选的选项【是&自动】
        $(".select-all-yes").prop("checked", false);
        $(".select-yes").prop("checked", false);
        $(".select-all-auto").prop("checked", false);
        $(".select-auto").prop("checked", false);
        // 重新渲染form
        form.render('checkbox');
    });
	
	//全选【是】
    form.on('checkbox(select-all-yes)', function (data) {
        var a = data.elem.checked;
        if (a == true) {
            $(".select-yes").prop("checked", true);
        } else {
            $(".select-yes").prop("checked", false);
        }
        
        // 取消其他全选的选项【否&自动】
        $(".select-all-no").prop("checked", false);
        $(".select-no").prop("checked", false);
        $(".select-all-auto").prop("checked", false);
        $(".select-auto").prop("checked", false);
        
        // 重新渲染form
        form.render('checkbox');
    });

    //全选【自动】
    form.on('checkbox(select-all-auto)', function (data) {
        var a = data.elem.checked;
        if (a == true) {
            $(".select-auto").prop("checked", true);
        } else {
            $(".select-auto").prop("checked", false);
        }
        
        // 取消其他全选的选项【否&是】
        $(".select-all-no").prop("checked", false);
        $(".select-no").prop("checked", false);
        $(".select-all-yes").prop("checked", false);
        $(".select-yes").prop("checked", false);
        
        // 重新渲染form
        form.render('checkbox');
    });
};

/**
 * checkbox 单选（三项中只能选中其中一项，防止用户重复的选中多项进行操作）.
 * @param  form 表单对象.
 * @returns
 */
function loadOnehecked(form){
	$(document).on("click",".select-item .layui-unselect",function() {
		var data = $(this).parent().children("input");
		$.each(data,function(i,val){
			if(val.checked){
				$(val).prop("checked", false);
			}
		})
		$(this).prev().prop("checked", true);
		 // 重新渲染form
	    form.render('checkbox');
	});
};

/**
 * 判断是否选中其中一项Checkbox
 * @returns
 */
function isCheckeds(){
	var item = $(".sbwl-task-keyword .select-item");
	var flag = true; // 标记是否选中一项
	var index = 0;   // 记录第几行
	for (var i = 0; i < item.length; i++) {
		// 获取是否选中一项
		var bool = $(item[i]).children(" input[type='checkbox']").is(':checked');
		if(!bool){
			flag = false;
			index = i;
			break;
		}
	}
	if(!flag){
		layer.msg("警告：第【"+(index+1)+"】行，必须选择一项！");
	}
	return flag;
};

/**
 * 获取任务搜索词的方法
 * @param isCard  是否进行打标：[{0 ：否}，{1 ：是}]
 * @returns
 */
function loadTaskKeyword(isCard){
	var array = [];
    $(".sbwl-task-keyword").each(function(i,item){
        var map = {};
        // 搜索词
        map.keyword = $(this).find('input[name="keyword"]').val();
        // 搜索词数量
        map.number =$(this).find('input[name="number"]').val();
        // 获取到是否打标标记，[{0：否},{1：是}] 
        if(isCard === 0){
         // 标记为 0
         map.isCard = isCard;	
        }else{
        	// 获取checkbox 选中的值！// [{0 ：否},{1 ：是},{2 ：自动}]
        	var checked = $(item).children(".select-item").children("input:checkbox[type='checkbox']:checked");
        	map.isCard = checked.val();
        }
        array.push(map);
        map = {};
    });
    return JSON.stringify(array);
};

/**
 * 根据打标标志，显示或者隐藏相关的元素. <br/>
 * @param isCard 打标标记 [{0：否},{1：是}]
 * @returns
 */
function selectAllItemByIsCard(isCard){
	if(isCard === 0){
		$(".select-all-item").addClass("hidden");
	}else{
		$(".select-all-item").removeClass("hidden");
	}
}

