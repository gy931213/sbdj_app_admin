
layui.use(['table','form'], function(){
    var table = layui.table;
    var form  = layui.form;
    var formSelects = layui.formSelects;

    // ~操作类型[add|update],默认为add
    var oprType = 'add';
    var layer_ = '';

    // ~渲染任务审核常用语句数据~
    table.render({
        id:'table_list',
        method:'post',
        elem: '#table-data',
        url:'/task/common/sentence/list',
        toolbar: '#toolbar',
        title: '任务审核常用语句列表',
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
            {field:'orgName', title:'所属机构', align: 'center'},
            {field:'platformName', title:'所属平台', align: 'center'},
            {field:'content', title:'原因', align: 'center'},
            {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {field:'status', title:'状态',align: 'center',templet:function(rse){
                    if(rse.status=="A"){
                        return '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="sbwl-prohibit">启用</a>';
                    }else if(rse.status=="D"){
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled" lay-event="sbwl-enable">禁用</a>';
                    }
                }},
            {field:'type', title:'类型',  templet:function(rse){
                    if(rse.type === 'AN'){
                        return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #60c560;">不通过</a>';
                    }else if(rse.type === 'AF'){
                        return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #d9534f;">已失败</a>';
                    }
                }},
            {fixed: 'right', title:'操作', width:180,align: 'center',templet:function(res){
                    var _html = '';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-warm" lay-event="sbwl-del"><i class="layui-icon">&#xe640;</i> 删除</a>';
                    return _html;
                }}
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true
    });

    // ~刷新数据列表
    function table_reload(){
        table.reload('table_list');
    }

    // ~渲染select选项 （机构）
    kits.select({
        elem:'#sbwl_org_select',
        url:'/sys/organization/select',
        value:'id',
        name:'name',
        form:form
    });

    //渲染select选项 (平台)
    kits.select({
        elem:'#sbwl-platform-select',
        url:'/config/platform/select',
        value:'id',
        name:'name',
        form:form
    });


    // ~监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
        	page: {
                curr: 1 //重新从第 1 页开始
             },
             where:data.field
        });
        return false;
    });

    // ~工具栏事件~
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            // ~新增原因
            case 'sbwl-add-task-common-sentence':
                var data = checkStatus.data;

                // ~渲染select选项（机构）
                kits.select({
                    elem:'#sbwl_org_selecta',
                    url:'/sys/organization/select',
                    value:'id',
                    name:'name',
                    form:form
                });

                //渲染select选项 (平台)
                kits.select({
                    elem:'#sbwl-platform-selecta',
                    url:'/config/platform/select',
                    value:'id',
                    name:'name',
                    form:form
                });

                // 渲染机构对应的角色数据
                kits.selects({
                    elem:'selectId',
                    url:'/task/picture/type/setting/select',
                    data:{},
                    value:'id',
                    name:'name',
                    select:formSelects,
                    selectVal:data.tptsId
                });

                // ~新增页面
                showEditOrAdd('新增原因','#sbwl_tmpl','add');
                break;
            // ~删除业务员
            case 'sbwl-del-task-common-sentence':
                var data = checkStatus.data;
                layer.msg('选中了：'+ data.length + ' 个');
                break;
        };
    });


// ~from表单数据验证
    form.verify({
        orgId: function(value){
            if(value==0){
                return "请选择对应组织";
            }
        },
        platformId:function(value){
            if(value==0){
                return "请选择平台编号";
            }
        },
        status: function (value) {
            if (value=='') {
                return "请选择状态"
            }
        },
        content: function (value) {
            if (value=='') {
                return "请输入原因"
            }
        },
        type: function (value) {
            console.log(value);
            if (value==='') {
                return '请选择类型'
            }
        }
    });

// ~监听行工具事件
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        if(obj.event === 'sbwl-prohibit'){
            // ~禁用操作~
            layer.msg('确定要禁用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/task/common/sentence/status',
                        data:{
                            id:data.id,
                            status:"D"
                        },sendMsg:'禁用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已禁用");
                            }else{
                                layer.msg("禁用失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-enable'){
            // ~启用操作~
            layer.msg('确定要启用吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/task/common/sentence/status',
                        data:{
                            id:data.id,
                            status:"A"
                        },sendMsg:'启用中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已启用");
                            }else{
                                layer.msg("启用失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-edit'){

            // ~渲染select选项（机构）
            kits.select({
                elem:'#sbwl_org_selecta',
                url:'/sys/organization/select',
                value:'id',
                name:'name',
                form:form,
                select:data.orgId
            });

            //渲染select选项 (平台)
            kits.select({
                elem:'#sbwl-platform-selecta',
                url:'/config/platform/select',
                value:'id',
                name:'name',
                form:form,
                select:data.platformId
            });

            // 渲染机构对应的角色数据
            kits.selects({
                elem:'selectId',
                url:'/task/picture/type/setting/select',
                data:{},
                value:'id',
                name:'name',
                select:formSelects,
                selectVal:data.tptsId
            });

            var values = data.tptsId === null ? '' : data.tptsId.split(",");
            formSelects.value("selectId",formSelects.value("selectId","val"),false);
            formSelects.value("selectId",values,true);

            //$("#status").val(data.status);
            // ~表单赋值
            form.val("sbwl_from", {
                "id":data.id,
                "content":data.content,
                "status": data.status,
                "type": data.type
            });

            // ~编辑数据
            showEditOrAdd('编辑原因','#sbwl_tmpl','update');

        } else if (obj.event === 'sbwl-del') {
            // ~启用操作~
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/task/common/sentence/delete',
                        data:{
                            id:data.id
                        },sendMsg:'删除中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已删除");
                            }else{
                                layer.msg("删除失败");
                            }
                            table_reload();
                        }
                    });
                }
            });
        }
    });

// ~打开编辑页面或者是新增数据页面
    function showEditOrAdd(title,doc,opr){
        layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '482px'],
            content: $(doc),
            cancel: function(){
                $('#sbwl_from_id')[0].reset();
            }
        });
        // 设置操作类型!
        oprType = opr;
        //layer.full(layer_);
    }

//from表单监听提交
    form.on('submit(sbwl_submit)', function(data){
        var data = data.field;
        // 提交后台数据!
        shenbi.ajax({
            type:'post',
            url:oprType=='add'?'/task/common/sentence/save':'/task/common/sentence/update',
            data:data,
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.close(layer_);
                    if(oprType=="add"){
                        layer.msg("成功");
                    }else{
                        layer.msg("已更新");
                    }
                    // 刷新数据表
                    table_reload();
                    // 重置表单
                    $('#sbwl_from_id')[0].reset();
                }else{
                    // ~异常信息
                    layer.msg(resp.message);
                }
            }
        });
        return false
    });
});