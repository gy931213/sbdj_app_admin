
// 
layui.use(['table','laydate','form'], function(){
    var table = layui.table;
    var laydate = layui.laydate;
    var form  = layui.form;

    // 统一初始化
    loadCheckAllClick(form);
    loadOnehecked(form);

    //开始日期
    laydate.render({
        elem: '#beginDate',
        type: 'datetime'
    });
    //结束日期
    laydate.render({
        elem: '#endDate',
        type: 'datetime'
    });

    //定时发布时间
    laydate.render({
        elem: '#timingTime',
        type: 'datetime'
    });

    // 表格
    table.render({
        id:'task-table-list',
        elem: '#table-data',
        url:'/task/list',
        toolbar: '#toolbar',
        title: '任务数据表',
        where:{status:'R'},
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
            {field:'taskNumber', title:'任务编号', align: 'center',hide:true},
            {field:'taskTypeName', title:'任务类型',align: 'center',width:87,templet:function (res) {
                    if (res.taskTypeName === '现付单') {
                        return '<a class="layui-btn layui-btn-sm" style="background-color: #60c560;">'+res.taskTypeName+'</a>';
                    } else if (res.taskTypeName === '隔日单') {
                        return '<a class="layui-btn layui-btn-sm" style="background-color: #d9534f;">'+res.taskTypeName+'</a>';
                    }
                }},
            {field:'timingTime', title:'定时发布',width:160},
            {field:'platformName', title:'所属平台',align: 'center',hide:true},
            {field:'shopName', title:'店铺名称', align: 'center'},
            {field:'userName', title:'商家名称', align: 'center',width:90},
            {field:'showPrice', title:'显示价', align: 'center',width:80},
            {field:'prefPrice', title:'优惠劵', align: 'center',width:80},
            {field:'realPrice', title:'实付价', align: 'center',width:80},
            {field:'brokerage', title:'佣金', align: 'center',hide:true},
            {field:'serverPrice', title:'服务费', align: 'center',hide:true},
            {field:'category', title:'商品类目', align: 'center',hide:true},
            {field:'taskTotal', title:'发布数', width:80, align: 'center',templet:function(rse){
                    return '<a class="layui-btn layui-btn-sm" style="width: 50px;background-color: #60c560;">'+rse.taskTotal+'</a>';
                }},
            {field:'tesidueNum', title:'剩余数', width:80, align: 'center',templet:function(rse){
                    return '<a class="layui-btn layui-btn-sm" style="width:50px;background-color: #d9534f;">'+rse.tesidueNum+'</a>';
                }},
            {field:'stayOprNum', title:'待操作', width:80, align: 'center',templet:function(rse){
                    return '<a class="layui-btn layui-btn-sm" style="width: 50px;background-color: #FFB800;">'+rse.stayOprNum+'</a>';
                }},
            {field:'notAuditedNum', title:'待审核', width:80, align: 'center',templet:function(rse){
                    return '<a class="layui-btn layui-btn-sm" style="width: 50px;background-color: #f0ad4e;">'+rse.notAuditedNum+'</a>';
                }},
            {field:'yesAuditedNum', title:'完成数', width:80, align: 'center',templet:function(rse){
                    return '<a class="layui-btn layui-btn-sm" style="width: 50px;background-color: #5bc0de;">'+rse.yesAuditedNum+'</a>';
                }},
            {field:'createTime', title:'创建时间',hide:true},
            {field:'status', title:'状态',align: 'center',width:80,templet:function(rse){
                    if(rse.status==='C'){
                        return '<a class="layui-btn layui-btn-xs layui-btn-normal">'+kits.returnStatus(rse.status)+'</a>';
                    }else if(rse.status==='R'){
                        return '<a class="layui-btn layui-btn-xs layui-btn-normal" style="background-color: #60c560;">'+kits.returnStatus(rse.status)+'</a>';
                    }else if(rse.status=="N"){
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled">'+kits.returnStatus(rse.status)+'</a>';
                    }else{
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled">'+kits.returnStatus(rse.status)+'</a>';
                    }
                }},
            {field:'isCard', title:'快搜',hide:true,templet:function(rse){
                    if(rse.isCard === 1){
                        return '<a class="layui-btn layui-btn-xs layui-btn-normal" style="background-color: #60c560;">是</a>';
                    }else if(rse.isCard === 0){
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled">否</a>';
                    }else{
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled">未知</a>';
                    }
                }},
            {fixed: 'right', title:'操作',width:270,templet:function(rse){
                    var _html = '';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-check"><i class="layui-icon">&#xe615;</i> 查看</a>';
                    if(rse.status==='C'){
                        _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                        _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-del" style="background-color: #d9534f;"><i class="layui-icon">&#xe640;</i> 删除</a>';
                    }
                    if(rse.status==='R'){
                        _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-stop"><i class="layui-icon">&#x1006;</i> 终止</a>';
                        if(rse.top == 0){
                            _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-set-top"><i class="layui-icon">&#xe604;</i> 置顶</a>';
                        }else{
                            _html+='<a class="layui-btn layui-btn-sm layui-btn-warm" lay-event="sbwl-cancel-top"><i class="layui-icon">&#xe604;</i> 已置顶</a>';
                        }
                    }
                    if(rse.status==='N'){
                        _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-start"><i class="layui-icon">&#xe605;</i> 运行</a>';
                    }
                    return _html;
                }}
        ]],
        page: true,
        limit:10,
        limits:[10,30,50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        }
    });

    //监听搜索按钮
    form.on('submit(sbwl-task-search)', function(data){
        table.reload('task-table-list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });
        return false;
    });

    // 监听指定开关
    form.on('switch(switchHistory)', function(data){
        if(this.checked){
            $('input[name="isHistory"]').val('YES');
            layer.tips('温馨提示：开启历史数据查询！', data.othis);
        }else{
            $('input[name="isHistory"]').val('NO');
            layer.tips('温馨提示：查询前 5 天的数据！', data.othis);
        }
    });

    //渲染select选项 (平台)
    kits.select({
        elem:'#sbwl-platform-select',
        url:'/config/platform/select',
        value:'id',
        name:'name',
        form:form
    });

    // ~渲染任务类型
    kits.select({
        elem:'#sbwl-taskType-select',
        url:'/base/code',
        data:{code:'TASK_TYPE'},
        value:'id',
        name:'name',
        form:form,
    });
    // ~刷新列表
    function table_reload(){
        table.reload('task-table-list');
    }

    // ~头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'dels':
                // 要进行删除操作的ids
                var ids = [];
                // 获取当前选中数据!
                var data = checkStatus.data;
                // 判断是否选中一项!
                if(data.length <= 0 ){
                    return  layer.msg('必须选中一项进行删除操作！');
                }
                // 循环遍历数据!
                for (var i = 0; i < data.length; i++) {
                    ids.push(data[i].id);
                }

                //layer.alert(JSON.stringify(data));
                layer.alert(JSON.stringify(ids));

                layer.msg('确定要批量删除吗？', {
                    btn: ['确定', '取消'],
                    yes: function(index, layero){
                        shenbi.ajax({
                            type:'post',
                            url:'/task/deletes',
                            data:{ids:ids.join()},
                            sendMsg:'删除中...',
                            traditional: true,
                            success:function(resp){
                                if(resp.retCode === 0){
                                    layer.msg("已删除");
                                }else{
                                    layer.msg("删除失败");
                                }
                                table_reload();
                            }
                        });
                    }
                });

                break;
            case 'getCheckLength':
                var data = checkStatus.data;
                layer.msg('选中了：'+ data.length + ' 个');
                break;
            case 'isAll':
                layer.msg(checkStatus.isAll ? '全选': '未全选');
                break;
        };
    });

    // 监听数据列表中的操作相关事件
    var $isCard = 0; // 标志是否要打标[{0：否},{1：是}]
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;   // 数据集
        $isCard = data.isCard; // 赋值
        // ~查看
        if(obj.event === 'sbwl-check'){
            // ~任务查看
            // ~表单赋值
            form.val("sbwl_checkTask_from", {
                "id":data.id,
                "taskNumber":data.taskNumber,
                "showPrice":data.showPrice,
                "prefPrice":data.prefPrice,
                "realPrice":data.realPrice,
                "taskTotal":data.taskTotal,
                "sellerAsk":data.sellerAsk,
                "platformId":data.platformId,
                "platformName":data.platformName,
                "title":data.title,
                "link":data.link,
                "imgLink":data.imgLink,
                "taskTypeName":data.taskTypeName,
                "packages":data.packages,
                "goodsKeywords":data.goodsKeywords,
                "viewtime": kits.dateFtt("yyyy-MM-dd hh:mm:ss",data.timingTime)
            });
            $("#sbwl-img-url-check").attr('src',data.imgLink);

            // ~获取任务对应的搜索词
            getTaskKeywordDataByTaskId("check",data.id);

            // 显示或者隐藏（是全选，自动全选）
            // selectAllItemByIsCard($isCard);

            // ~渲染新增商品任务窗口~
            showCreateTask('<i class="layui-icon">&#xe61f;</i> 查看','#sbwl_checkTask_tmpl');
        }else if(obj.event === 'sbwl-edit'){
            // ~编辑任务

            // 加载佣金的计算公式放置隐藏域
            /*$("#formula").val("");
            shenbi.ajax({
                type:'get',
                url:'/task/formula',
                success:function(resp){
                    console.log(resp)
                    if(resp.retCode === 0 && resp.data != null){
                        $("#formula").val(resp.data.formula);
                    }
                }
            });*/

            // ~表单赋值
            form.val("sbwl_editTask_from", {
                "id":data.id,
                "isCard":data.isCard,
                "taskNumber":data.taskNumber,
                "showPrice":data.showPrice,
                "prefPrice":data.prefPrice,
                "realPrice":data.realPrice,
                "taskTotal":data.taskTotal,
                "sellerAsk":data.sellerAsk,
                "platformId":data.platformId,
                "platformName":data.platformName,
                "title":data.title,
                "link":data.link,
                "imgLink":data.imgLink,
                // "brokerage":data.brokerage,
                // "serverPrice":data.serverPrice,
                "packages":data.packages,
                "goodsKeywords":data.goodsKeywords
            });
            $("#sbwl-img-url-edit").attr('src',data.imgLink);

            // ~渲染任务类型
            kits.select({
                elem:'#sbwl-taskType-select-edit',
                url:'/base/code',
                data:{code:'TASK_TYPE'},
                value:'id',
                name:'name',
                form:form,
                select:data.taskTypeId,
                option:'<option value="0">请选择任务类型</option>'
            });

            // ~获取任务对应的搜索词
            getTaskKeywordDataByTaskId("edit",data.id);

            // 显示或者隐藏（是全选，自动全选）
            selectAllItemByIsCard($isCard);

            // ~渲染新增商品任务窗口~
            showCreateTask('<i class="layui-icon">&#xe61f;</i> 编辑','#sbwl_editTask_tmpl');
        }else if(obj.event === 'sbwl-stop'){
            // ~任务终止
            layer.msg('确定要终止任务吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        url:'/task/status',
                        type:'post',
                        data:{
                            taskId:data.id,
                            status:"N"
                        },
                        sendMsg:'终止中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已终止");
                            }else{
                                layer.msg("终止失败");
                            }
                            // ~刷新数据
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-start'){
            // ~任务运行
            /* layer.msg('确定要运行任务吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        url:'/task/status',
                        type:'post',
                        data:{
                            taskId:data.id,
                            status:"R"
                        },
                        sendMsg:'运行中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已运行");
                            }else{
                                layer.msg("运行失败");
                            }
                            // ~刷新数据
                            table_reload();
                        }
                    });
                 }
             });*/

            layer.confirm('确定要运行任务吗？', {
                btn: ['定时发布', '发布', '取消'],
                btn4: function(index, layero){
                    //按钮【按钮三】的回调
                    layer.close(index);
                    console.log("3");
                }
            }, function(index, layero){
                layer.close(index);
                //按钮【按钮一】的回调
                console.log("1");
                layer.msg('确定要定时发布吗？', {
                    btn: ['确定', '取消'],
                    yes: function(index, layero){
                        shenbi.ajax({
                            url:'/task/status',
                            type:'post',
                            data:{
                                taskId:data.id,
                                status:'R',
                                type:'NO',
                            },
                            sendMsg:'提交中 ...',
                            success:function(resp){
                                if(resp.retCode === 0){
                                    layer.msg("定时发布成功！");
                                }else{
                                    layer.msg("定时发布失败！");
                                }
                                // ~刷新数据
                                table_reload();
                            }
                        });
                    }
                });
            }, function(index){
                layer.close(index);
                //按钮【按钮二】的回调
                console.log("2");
                layer.msg('确定要发布吗？', {
                    btn: ['确定', '取消'],
                    yes: function(index, layero){
                        shenbi.ajax({
                            url:'/task/status',
                            type:'post',
                            data:{
                                taskId:data.id,
                                status:"R",
                                type:'YES',
                            },
                            sendMsg:'提交中 ...',
                            success:function(resp){
                                if(resp.retCode === 0){
                                    layer.msg("发布成功！");
                                }else{
                                    layer.msg("发布失败！");
                                }
                                // ~刷新数据
                                table_reload();
                            }
                        });
                    }
                });
            });

        }else if(obj.event === 'sbwl-del'){
            // ~删除任务，任务
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/task/deletes',
                        data:{
                            ids:data.id,
                        },sendMsg:'删除中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已删除");
                            }else{
                                layer.msg("删除失败");
                            }
                            // ~刷新数据
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-set-top'){
            // ~任务置顶
            layer.msg('确定要置顶吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/task/settop',
                        data:{
                            taskId:data.id,
                        },sendMsg:'置顶中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已置顶");
                            }else{
                                layer.msg(resp.message);
                            }
                            // ~刷新数据
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-cancel-top'){
            // ~任务置顶
            layer.msg('确定要取消置顶吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/task/canceltop',
                        data:{
                            taskId:data.id,
                        },sendMsg:'取消置顶中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("取消已置顶");
                            }else{
                                layer.msg("取消置顶失败");
                            }
                            // ~刷新数据
                            table_reload();
                        }
                    });
                }
            });
        } else{
            layer.msg("没有具体操作事件");
        }
    });

    // ~根据任务id获取任务相关的搜索词数据
    function getTaskKeywordDataByTaskId(el,taskId){
        shenbi.ajax({
            type:'post',
            url:'/task/findkeyword',
            data:{taskId:taskId},
            success:function(resp){
                if(resp.retCode==0){
                    // ~查看
                    if(el==='check'){
                        // ~渲染搜索词数据
                        renderingTaskKeywordHtmlCheck(resp.data);
                    }
                    // ~编辑
                    if(el==='edit'){
                        // ~渲染搜索词数据
                        renderingTaskKeywordHtmlEdit(resp.data);
                    }
                }else{
                    layer.msg("load失败");
                }
            }
        });
    }
    // ~渲染任务搜索词数据的函数（查看）
    function renderingTaskKeywordHtmlCheck(data){
        $("#sbwl-list-div-check").html("");
        $(".sbwl-task-keyword-li").remove();
        for (var i = 0; i < data.length; i++) {
            var _html = '';
            if(i===0){
                _html+='	<div class="layui-inline" style="width: 286px">';
                _html+='		<input type="text" class="layui-input input-check" value="'+data[i].keyword+'" disabled="disabled" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
                _html+='	</div>';
                _html+='	<div class="layui-inline" style="width: 200px">';
                _html+='		<input type="text" class="layui-input input-check" value="'+data[i].number+'" disabled="disabled" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
                _html+='	</div>';
                // 加载选中的Checkbox
                _html+= loadCheckedCheckboxHtml($isCard,data[i].isCard,true);
                // ~渲染搜索词第[0]~
                $("#sbwl-list-div-check").html(_html);
            }else{
                _html+='<div class="layui-input-block sbwl-task-keyword-li">';
                _html+='	<div class="layui-inline" style="width: 286px">';
                _html+='		<input type="text" class="layui-input task-key input-check" value="'+data[i].keyword+'" disabled="disabled" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
                _html+='	</div>';
                _html+='	<div class="layui-inline" style="width: 200px">';
                _html+='		<input type="text" class="layui-input task-number input-check" value="'+data[i].number+'" disabled="disabled" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
                _html+='	</div>';
                // 加载选中的Checkbox
                _html+= loadCheckedCheckboxHtml($isCard,data[i].isCard,true);
                _html+='</div>';
                // ~渲染搜索词~
                $("#sbwl-list-div-check").after(_html);
            }
        }

        // 进行渲染
        form.render('checkbox');
    }

    // ~渲染任务搜索词数据的函数（编辑）
    function renderingTaskKeywordHtmlEdit(data){
        $("#sbwl-list-div-edit").html("");
        $(".sbwl-task-keyword-li").remove();
        for (var i = 0; i < data.length; i++) {
            var _html = '';
            if(i===0){
                _html+='	<div class="layui-inline" style="width: 286px">';
                _html+='		<input type="text" class="layui-input task-key" value="'+data[i].keyword+'" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
                _html+='	</div>';
                _html+='	<div class="layui-inline" style="width: 200px">';
                _html+='		<input type="text" class="layui-input task-number" value="'+data[i].number+'" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
                _html+='	</div>';
                _html+='	<div class="layui-inline ss-btn" id="js-click-btn" style="width: 30px">';
                _html+='		<a class="layui-btn"><i class="layui-icon">&#xe608;</i></a>';
                _html+='	</div>';
                // 加载选中的Checkbox
                _html+= loadCheckedCheckboxHtml($isCard,data[i].isCard,false);
                // ~渲染搜索词第[0]~
                $("#sbwl-list-div-edit").html(_html);
            }else{
                _html+='<div class="layui-input-block sbwl-task-keyword-li sbwl-task-keyword">';
                _html+='	<div class="layui-inline" style="width: 286px">';
                _html+='		<input type="text" class="layui-input task-key" value="'+data[i].keyword+'" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
                _html+='	</div>';
                _html+='	<div class="layui-inline" style="width: 200px">';
                _html+='		<input type="text" class="layui-input task-number" value="'+data[i].number+'" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
                _html+='	</div>';
                _html+='	<div class="layui-inline ss-btn sbwl-click-del"  style="width: 30px">';
                _html+='		<a class="layui-btn layui-btn-danger"><i class="layui-icon">&#x1006;</i></a>';
                _html+='	</div>';
                // 加载选中的Checkbox
                _html+= loadCheckedCheckboxHtml($isCard,data[i].isCard,false);
                _html+='</div>';
                // ~渲染搜索词~
                $("#sbwl-list-div-edit").after(_html);
            }
        }
        // 进行渲染
        form.render('checkbox');
    };

    // ~添加关键词搜索输入框~
    $(document).on('click','#js-click-btn',function(){
        var _html = '';
        _html+='<div class="layui-input-block sbwl-task-keyword-li sbwl-task-keyword">';
        _html+='	<div class="layui-inline" style="width: 286px">';
        _html+='		<input type="text" class="layui-input task-key" name="keyword" lay-verType="tips" lay-verify="required" placeholder="请输入搜索词">';
        _html+='	</div>';
        _html+='	<div class="layui-inline" style="width: 200px">';
        _html+='		<input type="text" class="layui-input task-number" name="number" lay-verType="tips" lay-verify="required|number" placeholder="请输入任务数">';
        _html+='	</div>';
        _html+='	<div class="layui-inline ss-btn sbwl-click-del"  style="width: 30px">';
        _html+='		<a class="layui-btn layui-btn-danger"><i class="layui-icon">&#x1006;</i></a>';
        _html+='	</div>';
        _html+= loadOneCheckedCheckboxHtml($isCard);
        _html+='</div>';

        // ~渲染搜索词~
        $("#sbwl-list-div-edit").after(_html);
        // 进行渲染
        form.render('checkbox');
    });

    // ~删除当前点击的搜索词
    $(document).on('click','.sbwl-click-del',function(){
        // ~删除
        $(this).parent().remove();
        // ~获取到搜索词的任务数~
        eachTaskNumberFunc();
    });

    // ~监听任务数输入的事件~
    $(document).bind('input','.task-number',function(){
        // ~遍历搜索词的任务数~
        eachTaskNumberFunc();
    });

    // ~遍历搜索词的任务数的函数~
    function eachTaskNumberFunc(){
        // ~获取到搜索词的任务数~
        var taskTotal = 0; // ~总任务数
        $("input[name='number']").each(function(i,item){
            console.log(item.value);
            if(item.value!="" && item.value!=null){
                taskTotal+=parseInt(item.value);
            }
        });
        // ~渲染总任务数~
        $("input[name='taskTotal']").val(taskTotal);
    }

    // ~遍历搜索词和搜索词的任务数的函数~
    function eachTaskKeyword(){
        var array = [];
        // ~遍历搜索词数据
        $(".sbwl-task-keyword").each(function(i,item){
            var map = {};
            map.keyword = $(this).find('input[name="keyword"]').val();
            map.number =$(this).find('input[name="number"]').val();
            array.push(map);
            map = {};
        });
        return JSON.stringify(array);
    }

    //~添加商品任务的弹出层~
    var task_layer;
    function showCreateTask(title,doc){
        task_layer = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
        layer.full(task_layer);
    }

    // ~from表单验证规则
    form.verify({
        taskTypeId: function(value){
            if(value==0){
                return "请选择对应任务类型";
            }
        }
    });

    // ~from表单监听提交
    form.on('submit(sbwl_task_submit)', function(data){
        var data = data.field;
        // 判断数据是否正常
        if(!isCheckeds()){
            return false;
        }

        data.taskKeyword = loadTaskKeyword($isCard); // 任务搜索词
        data.status = $(this).attr("type");   // 状态类型
        data.time = $("input[name='time']").val();// 发布时间

        // 提交后台数据!
        shenbi.ajax({
            type:'post',
            url:'/task/update',
            data:data,
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode === 0){
                    if(data.status === 'R'){
                        layer.msg("已发布");
                    }else{
                        layer.msg("已更新");
                    }
                    // 重置表单
                    $('#sbwl_editTask_id')[0].reset();
                }else{
                    // 错误提示
                    layer.msg(resp.message);
                }
                // 刷新数据表
                table_reload();
                layer.close(task_layer);
            }
        });
        return false
    });

    // 计算佣金的函数
    /*$("#realPriceFormula").on('input',function(){
        var formula = $("#formula").val();
        var x = parseInt($(this).val()==""?0:$(this).val());
        if(x !="" && (x >0 && x < 100)){
            x = 100;
        }
        if(formula == ""){
            return false
        }
        var $eval = eval(formula);
        $eval = x == 0 ? 0 : $eval;
        $("#brokerageFormula").val($eval.toFixed(2));
    });*/

});