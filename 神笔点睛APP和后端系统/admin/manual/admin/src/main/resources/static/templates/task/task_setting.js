
// 
layui.use(['table','form'], function(){
    var table = layui.table;
    var form  = layui.form;
    //操作类型[add|update],默认为add
    var oprType = 'add';

    // 表格
    table.render({
        id:'task-table-list',
        elem: '#table-data',
        url:'/task/setting/list',
        toolbar: '#toolbar',
        title: '任务配置数据表',
        method:'POST',
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
            {field:'orgName', title:'所属机构', align: 'center',hide:true},
            {field:'salesmanLevelName', title:'所属等级', align: 'center'},
            {field:'credit', title:'信用额度', align: 'center'},
            {field:'allotTotal', title:'一个业务员同一天只能分配号主'},
            {field:'inviteTotal', title:'一个业务员同一天邀请下线个数'},
            {field:'taskTotal', title:'号主同一天能接任务数'},
            {field:'sameBigDays', title:'设置号主近n天'},
            {field:'notBigDays', title:'号主近n天任务数不能超过'},
            {field:'sameShopBigDays', title:'同店铺大于多少天佣金减半'},
            {field:'sameShopSmallDays', title:'同店铺小于多少天不可接任务'},
            {field:'onlineIntervalDay', title:'上级间隔日期'},
            {field:'onlineOfflineIntervalDay', title:'同级间隔日期'},
            {field:'offlineIntervalDay', title:'下级间隔日期'},
            {field:'categoryIntervalDay', title:'类目间隔日期'},
            {field:'pddIntervalDay', title:'拼多多间隔日期'},
            {field:'createTime', title:'创建时间',align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {fixed: 'right', title:'操作', width:180,align: 'center',templet:function(rse){
                    var _html = '';
                    _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-edit"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                    _html+='<a class="layui-btn layui-btn-sm" lay-event="sbwl-del" style="background-color: #d9534f;"><i class="layui-icon">&#xe640;</i> 删除</a>';
                    return _html;
                }}
        ]],
        limit:30,
        limits:[30,50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true
    });

    //监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('task-table-list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });
        return false;
    });

    // ~刷新列表
    function table_reload(){
        table.reload('task-table-list');
    }

    // ~渲染select选项 （机构）
    kits.select({
        elem:'#sbwl_org_select_',
        url:'/sys/organization/select',
        value:'id',
        name:'name',
        form:form,
    });

    // ~头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'sbwl-create':
                // 重置表单
                $('#sbwl_task_setting_id')[0].reset();
                $("input[name=id]").val("");

                // ~渲染select选项 （机构）
                kits.select({
                    elem:'#sbwl-org-select',
                    url:'/sys/organization/select',
                    value:'id',
                    name:'name',
                    form:form
                });

                // ~渲染select选项 （等级）
                kits.select({
                    elem:'#sbwl-level-select',
                    url:'/salesman/level/select',
                    value:'id',
                    name:'name',
                    form:form
                });
                // ~渲染新增窗口
                showCreateTaskSetting('<i class="layui-icon">&#xe61f;</i> 新增','#sbwl_task_setting_tmpl','add');
                break;
        };
    });

    // ~监听数据列表中的操作相关事件~
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        if(obj.event === 'sbwl-del'){
            // ~删除
            layer.msg('确定要删除吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/task/setting/delete',
                        data:{id:data.id},
                        sendMsg:'删除中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已删除");
                            }else{
                                layer.msg("删除失败");
                            }
                            // ~刷新数据
                            table_reload();
                        }
                    });
                }
            });
        }else if(obj.event === 'sbwl-edit'){
            // ~编辑
            // ~渲染select选项 （机构）
            kits.select({
                elem:'#sbwl-org-select',
                url:'/sys/organization/select',
                value:'id',
                name:'name',
                form:form,
                select:data.orgId
            });

            // ~渲染select选项 （等级）
            kits.select({
                elem:'#sbwl-level-select',
                url:'/salesman/level/select',
                value:'id',
                name:'name',
                form:form,
                select:data.salesmanLevelId
            });

            // ~表单赋值
            form.val("sbwl_task_setting_from", {
                "id":data.id,
                "sameShopSmallDays":data.sameShopSmallDays,
                "sameShopBigDays":data.sameShopBigDays,
                "notBigDays":data.notBigDays,
                "taskTotal":data.taskTotal,
                "allotTotal":data.allotTotal,
                "inviteTotal":data.inviteTotal,
                "credit":data.credit,
                "sameBigDays":data.sameBigDays,
                "onlineIntervalDay":data.onlineIntervalDay,
                "onlineOfflineIntervalDay":data.onlineOfflineIntervalDay,
                "offlineIntervalDay":data.offlineIntervalDay,
                "memberFlag":data.memberFlag,
                "offlineFlag":data.offlineFlag,
                "onlineFlag":data.onlineFlag,
                "onlineOfflineFlag":data.onlineOfflineFlag,
                "autoGetOrderFlag":data.autoGetOrderFlag,
                "showOrderFlag":data.showOrderFlag,
                "uploadOrderNumFlag":data.uploadOrderNumFlag,
                "categoryFlag":data.categoryFlag,
                "categoryIntervalDay":data.categoryIntervalDay,
                "pddIntervalDay":data.pddIntervalDay
            });

            // ~渲染编辑页面
            showCreateTaskSetting('<i class="layui-icon">&#xe642;</i> 编辑','#sbwl_task_setting_tmpl','update');
        }else{
            layer.msg("没有具体操作事件");
        }
    });

    //~添加任务配置的弹出层~
    var task_layer;
    function showCreateTaskSetting(title,doc,opr){
        task_layer = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['700px', '730px'],
            content: $(doc),
            cancel: function(){
                // 重置表单
                $('#sbwl_task_setting_id')[0].reset();
            }
        });
        //layer.full(task_layer);
        oprType = opr;
    }

    // ~from表单验证规则
    form.verify({
        orgId: function(value){
            if(value===0){
                return "请选择对应机构";
            }
        },
        salesmanLevelId:function(value){
            if(value===0){
                return "请选择对应等级";
            }
        }
    });

    // ~from表单监听提交
    form.on('submit(sbwl_taskSetting_submit)', function(data){
        // 提交后台数据!
        shenbi.ajax({
            type:'post',
            url:oprType=='add'?'/task/setting/save':'/task/setting/update',
            data:data.field,
            sendMsg:'提交中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.close(task_layer);
                    layer.msg("成功");
                    // 重置表单
                    $('#sbwl_task_setting_id')[0].reset();
                }else{
                    // 错误提示
                    layer.msg(resp.message);
                }
                // 刷新数据表
                table_reload();
            }
        });
        return false
    });
});