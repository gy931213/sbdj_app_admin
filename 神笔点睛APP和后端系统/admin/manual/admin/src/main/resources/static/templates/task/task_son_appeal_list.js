
// 
layui.use(['table','laydate','form'], function(){
    var table = layui.table;
    var laydate = layui.laydate;
    var form  = layui.form;
    // 状态
    var statuss = [
        {'value':'T','name':'已审核'},
        {'value':'W','name':'待审核'},
        {'value':'N','name':'不通过'},
    ];


    // 快链字段设置样式
    var key = ['orderNum','salesmanName','onlineid','auditorName','shopName'];

    //开始日期
    laydate.render({
        elem: '#beginDate',
        type: 'datetime'
    });
    //结束日期
    laydate.render({
        elem: '#endDate',
        type: 'datetime'
    });

    // 表格
    table.render({
        id:'task-table-list',
        elem:'#table-data',
        url:'/task/son/appeal/list',
        toolbar: '#toolbar',
        title: '申请表',
        cols: [[
            {type:'checkbox',sort:true,align: 'center',fixed: 'left'},
            {type:'numbers',title:'序号',align: 'center',width:'5%',fixed: 'left'},
            {field:'taskSonNumber', title:'子任务单号'},
            {field:'shopName', title:'店铺名称',event:'search-shopName'},
            {field:'orderNum', title:'订单号',event:'search-orderNum'},
            {field:'salesmanName', title:'业务员名称',event:'search-salesmanName'},
            {field:'salesmanNumberName', title:'号主名称',hide:true},
            {field:'onlineid', title:'号主ID',event:'search-onlineid'},
            {field:'reasons', title:'原因'},
            {field:'realPrice', title:'应付金额'},
            {field:'account', title:'审核原因',hide:true},
            {field:'auditorName', title:'审核人',align:'center',event:'search-auditorName',hide:true},
            {field:'isTimeOut', title:'超时',width:100,align: 'center',templet:function(rse){
                    if(rse.isTimeOut === 'YES'){
                        return '<a class="layui-btn layui-btn-sm" lay-event="is-time-out" style="width:60px;background-color: #d9534f;">是</a>';
                    }
                    if(rse.isTimeOut === 'NO'){
                        return '<a class="layui-btn layui-btn-sm" style="width:60px;background-color: #60c560;">否</a>';
                    }
                }},
            {field:'createTime', title:'领取时间',align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {field:'auditorTime', title:'审核时间',align: 'center',hide:true,templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.updateTime);
                }},
            {field:'status', title:'状态', width:100,align: 'center', event:'search-status', templet:function(rse){
                    if(rse.status === 'W'){
                        return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #009688;">未审核</a>';
                    }else if(rse.status === 'T'){
                        return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #60c560;">已审核</a>';
                    }else if(rse.status === 'N'){
                        return '<a class="layui-btn layui-btn-sm" style="width:55px;background-color: #d9534f;">不通过</a>';
                    }
                }},
            {fixed: 'right', title:'操作', width:250,templet:function(rse){
                    var _html = '';
                    _html+='<a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="sbwl-check-img"><i class="layui-icon">&#xe615;</i> 截图</a>';
                    if(rse.status === 'W'){
                        _html+='<a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="sbwl-auditing"><i class="layui-icon">&#xe605;</i> 审核</a>';
                    }
                    if (rse.status === 'N') {
                        _html+='<a class="layui-btn layui-btn-sm" style="background-color: #d9534f;" lay-event="sbwl-auditing" ><i class="layui-icon">&#xe605;</i> 不通过';
                    } else if (rse.status === 'T'){
                        _html+='<a class="layui-btn layui-btn-sm" style="background-color: #999;"><i class="layui-icon">&#xe605;</i> 审核';
                    }
                    return _html;
                }}
        ]],
        limit:10,
        limits:[10,30,50,100,200,500,1000],
        parseData: function(res){
            return shenbi.returnParse(res);
        },
        page: true,
    });

    // 设置表头有快联的样式
    tableThStyle(key);

    // ~监听搜索按钮
    form.on('submit(sbwl-taskSon-search)', function(data){
        table.reload('task-table-list',{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where:data.field
        });

        // 设置表头有快联的样式
        tableThStyle(key);
        return false;
    });

    // 重置
    form.on('submit(sbwl-reset)',function(){
        shenbi.select({
            elem:'#taskStatus',
            data:statuss,
            form:form,
        });
        // 设置表头有快联的样式
        tableThStyle(key);
    })


    // ~刷新列表
    function table_reload(){
        table.reload('task-table-list');
        // 设置表头有快联的样式
        tableThStyle(key);
    }

    // ~头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'getCheckData':
                var data = checkStatus.data;
                layer.alert(JSON.stringify(data));
                break;
            case 'getCheckLength':
                var data = checkStatus.data;
                layer.msg('选中了：'+ data.length + ' 个');
                break;
            case 'isAll':
                layer.msg(checkStatus.isAll ? '全选': '未全选');
                break;
        };
    });

    // ~监听数据列表中的操作相关事件~
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        if(obj.event === 'sbwl-auditing'){
            // ~子任务审核
            $("#id").val(data.id);
            $("#taskSonId").val(data.taskSonId);
            $("#status").val(data.status);
            // ~打开审核任务窗口
            showAuditorTask("审核","#sbwl_auditorTask_tmpl");

            /*form.on('radio(btn)', function(res){
                if (res.value === 'T') {
                    $(".js-hidden").addClass("sbwl-hidden");
                } else {
                    $(".js-hidden").removeClass("sbwl-hidden");
                }
            });*/
            $(".js-hidden").removeClass("sbwl-hidden");

        }else if(obj.event === 'sbwl-check-img'){
            var imgs = data.images.split(',');
            // ~查看任务截图
            // ~获取业务员相关的证件图片~
            var _html = '</br>';
            shenbi.ajax({
                type:'post',
                url:'/task/son/find/imgs',
                data:{taskSonId:data.taskSonId},
                success:function(resp){
                    if(resp.retCode === 0){
                        var data = resp.data;
                        for (var i = 0; i < data.length; i++) {
                            _html+='<div class="layui-form-item task-div-son-img-li">';
                            _html+='	<div class="sbwl-task-son-img">';
                            _html+='		<img id="sbwl-img" alt="" src="'+data[i].link+'" style="width: 400px">';
                            _html+='		<div class="scr"><b>'+data[i].typeName+'</b></div>';
                            _html+='	</div>';
                            _html+='</div>';
                        }
                        for (var i = 0; i < imgs.length; i++) {
                            _html+='<div class="layui-form-item task-div-son-img-li">';
                            _html+='	<div class="sbwl-task-son-img">';
                            _html+='		<img id="sbwl-img" alt="" src="'+imgs[i]+'" style="width: 400px">';
                            _html+='		<div class="scr"><b>申诉图</b></div>';
                            _html+='	</div>';
                            _html+='</div>';
                        }
                        // ~渲染数据~
                        $("#sbwl_taskSon_tmpl_img").html(_html);
                        // ~渲染数据~
                        showTaskSonImgSrc('<i class="layui-icon">&#xe64a;</i> 任务截图','#sbwl_taskSon_tmpl');
                    }
                }
            });
        }else if(obj.event === 'search-orderNum'){
            // 模拟鼠标双击
            if(!dblclick()){
                return false;
            }
            // 订单号
            table.reload('task-table-list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'orderNum':data.orderNum}
            });
            $("input[name='orderNum']").val(data.orderNum);
        }else if(obj.event === 'search-salesmanName'){
            // 模拟鼠标双击
            if(!dblclick()){
                return false;
            }
            // 业务员名称
            table.reload('task-table-list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'salesmanName':data.salesmanName}
            });
            $("input[name='salesmanName']").val(data.salesmanName);
        }else if(obj.event === 'search-onlineid'){
            // 模拟鼠标双击
            if(!dblclick()){
                return false;
            }
            // 号主id
            table.reload('task-table-list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'onlineid':data.onlineid}
            });
            $("input[name='onlineid']").val(data.onlineid);
        }else if(obj.event === 'search-auditorName'){
            // 模拟鼠标双击
            if(!dblclick()){
                return false;
            }
            // 审核人
            table.reload('task-table-list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'auditorName':data.auditorName}
            });
            $("input[name='auditorName']").val(data.auditorName);
        }else if(obj.event === 'search-shopName'){
            // 模拟鼠标双击
            if(!dblclick()){
                return false;
            }
            // 号主id
            table.reload('task-table-list',{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where:{'shopName':data.shopName}
            });
            $("input[name='shopName']").val(data.shopName);
        }else if(obj.event === 'is-time-out'){
            // 将任务超时更新为不超时
            layer.msg('确定要更新为【否】吗？', {
                btn: ['确定', '取消'],
                yes: function(index, layero){
                    shenbi.ajax({
                        type:'post',
                        url:'/task/son/timeout',
                        data:{
                            id:data.taskSonId,
                            isTimeOut:'NO'
                        },
                        sendMsg:'提交中...',
                        success:function(resp){
                            if(resp.retCode === 0){
                                layer.msg("已更新");
                            }else{
                                layer.msg("更新失败");
                            }
                            // ~刷新数据
                            table_reload();
                        }
                    });
                }
            });
        }else{
            layer.msg("没有具体操作事件");
        }

        // 设置表头有快联的样式
        tableThStyle(key);
    });

    //~查看子任务截图的弹出层~
    function showTaskSonImgSrc(title,doc){
        var layer_ = layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            shadeClose: false,
            area: ['620px', '730px'],
            content: $(doc),
            cancel: function(){

            }
        });
    }

    // ~审核任务窗口
    var _layer
    function showAuditorTask(title,doc){
        _layer= layer.open({
            type: 1,
            title: title,
            skin: 'layui-layer-molv',
            area: ['638px', '345px'],
            content: $(doc),
            cancel: function(){
                $("#sbwl_auditorTask_id")[0].reset();
                $(".js-hidden").addClass("sbwl-hidden");
            }
        });
    }

    // ~审核任务from表单监听提交
    form.on('submit(sbwl_auditorTask_submit)', function(data){

        if(null!=data && data.field.endStatus!=="T" && data.field.account === ""){
            layer.msg('审批原因不能为空!', {icon: 0});
            return false;
        }

        shenbi.ajax({
            type:'post',
            url: '/task/son/appeal/review',
            data:data.field,
            sendMsg: '审核中...',
            success:function(resp){
                if(resp.retCode === 0){
                    layer.msg("已审核");
                }else{
                    layer.msg("审核失败");
                }
                // ~刷新数据
                table_reload();
                $("#sbwl_auditorTask_id")[0].reset();
                $(".js-hidden").addClass("sbwl-hidden");
                layer.close(_layer);
            }
        });
        return false
    });

});