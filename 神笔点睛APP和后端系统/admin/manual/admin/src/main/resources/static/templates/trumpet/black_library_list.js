
layui.use(['table','form'], function(){
  var table = layui.table;
  var form  = layui.form;
  var $oprType = 'add';
  
  table.render({
	id:'table-data',
    elem: '#table-data',
	url:'/black/library/list',
	toolbar: '#toolbar',
	title: '小号清洗库数据',
	cols: [[
	  {type:'checkbox',sort:true,align: 'center'},
	  {type:'numbers',title:'序号',align: 'center',width:'5%'},
	  {field:'orgName', title:'所属机构', align: 'center'},
	  {field:'platformName', title:'所属平台', align: 'center'},
	  {field:'onlineid', title:'小号', align: 'center'},
	  {field:'isClean', title:'清洗', align: 'center',templet:function(resp){
		  if(resp.isClean === 'YES'){
			  return '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="sbwl-no" style="width: 50px;">是</a>';  
		  }else{
			  return '<a class="layui-btn layui-btn-xs layui-btn-disabled" lay-event="sbwl-yes" style="width: 50px;">否</a>'; 
		  }
	  }},
	  {field:'createTime', title:'创建时间', align: 'center',templet:function(rse){
		  return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
	  }},
	  {fixed: 'right', title:'操作', toolbar: '#operation', width:150,align: 'center'}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
    parseData: function(res){ 
        return shenbi.returnParse(res);
    },
	page: true
  });
  
	  //刷新列表
	  function table_reload(){
		  table.reload('table-data');
	  }
  
	  //监听搜索按钮
	  form.on('submit(sbwl_search)', function(data){
	    table.reload('table-data',{
	    	page: {
	            curr: 1 //重新从第 1 页开始
	         },
	         where:data.field
	    });
	    return false;
	   });
  
	  	
  	//渲染所属机构
	kits.select({
		elem:'#sbwl_org_select',
		url:'/sys/organization/select',
		value:'id',
		name:'name',
		form:form
	});
	
	//渲染所属平台
	 kits.select({
		 elem:'#sbwl_platform_select',
		 url:'/config/platform/select',
		 value:'id',
		 name:'name',
		 form:form
	 });
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'getCheckData':
        var data = checkStatus.data;
        layer.alert(JSON.stringify(data));
      break;
      case 'getCheckLength':
        var data = checkStatus.data;
        layer.msg('选中了：'+ data.length + ' 个');
      break;
      case 'sbwl-create':
    	  
	  	//渲染所属机构
		kits.select({
			elem:'.sbwl_org_select',
			url:'/sys/organization/select',
			value:'id',
			name:'name',
			form:form
		});
		
		//渲染所属平台
		 kits.select({
			 elem:'.sbwl_platform_select',
			 url:'/config/platform/select',
			 value:'id',
			 name:'name',
			 form:form
		 });
    	  
		showEditOrAdd('新增','#sbwl_tmpl','add');
      break;
    };
  });
  
  // 监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    if(obj.event === 'sbwl-no'){
    	layer.msg('确定要更新为【否】？', {
    	    btn: ['确定', '取消'],
    	    yes: function(index, layero){
    	    	var data = obj.data;
    	    	shenbi.ajax({
    				type:'post',
    				url:'/black/library/isclean',
    				sendMsg:'提交中...',
    				data:{id:data.id,isClean:'NO'},
    				success:function(resp){
    					if(resp.retCode === 0){
    						layer.msg("已更新"); 
    					}else{
    						layer.msg("更新失败");
    					}
    					table_reload();
    				}
    			});
    	    }
    	});
    } else if(obj.event === 'sbwl-yes'){
    	layer.msg('确定要更新为【是】？', {
    	    btn: ['确定', '取消'],
    	    yes: function(index, layero){
    	    	var data = obj.data;
    	    	shenbi.ajax({
    				type:'post',
    				url:'/black/library/isclean',
    				sendMsg:'提交中...',
    				data:{id:data.id,isClean:'YES'},
    				success:function(resp){
    					if(resp.retCode === 0){
    						layer.msg("已更新"); 
    					}else{
    						layer.msg("更新失败");
    					}
    					table_reload();
    				}
    			});
    	    }
    	});
    }else if(obj.event === 'sbwl-del'){
    	layer.msg('确定要删除吗？', {
    	    btn: ['确定', '取消'],
    	    yes: function(index, layero){
    	    	var data = obj.data;
    	    	shenbi.ajax({
    				type:'post',
    				url:'/black/library/delete',
    				sendMsg:'提交中...',
    				data:{id:data.id},
    				success:function(resp){
    					if(resp.retCode === 0){
    						layer.msg("已删除"); 
    					}else{
    						layer.msg("删除失败");
    					}
    					table_reload();
    				}
    			});
    	    }
    	});
    }
  });
  
  	//打开编辑页面或者是新增数据页面
	function showEditOrAdd(title,doc,opr){
		layer_ = layer.open({
	        type: 1,
	        title: title,
	        skin: 'layui-layer-molv',
			shadeClose: false,
			area: ['650px', '350px'],
			content: $(doc),
			cancel: function(){
				$('#sbwl_from_id')[0].reset();
			}
		});
		// 设置操作类型!
		$oprType = opr;
	}
	
	 //from表单验证规则
	form.verify({
		orgId: function(value){
			if(value === '0'){
				return "请选择所属机构";
			}
		},
		platformId:function(value){
			if(value === '0'){
				return "请选择所属平台";
			}
			
		}
	 });
	
	  //from表单监听提交
	  form.on('submit(sbwl_submit)', function(data){
	   // 提交后台数据!
		shenbi.ajax({
			post:'post',
			url:$oprType=='add'?'/black/library/save':'/black/library/update',
			data:data.field,
			sendMsg:'提交中...',
			success:function(resp){
				if(resp.retCode === 0){
					layer.close(layer_);
					if($oprType=="add"){
						layer.msg("成功"); 
					}else{
						layer.msg("已更新"); 
					}
					// 刷新数据表
					table_reload();
					// 重置表单
					$('#sbwl_from_id')[0].reset();
				}else{
					layer.msg(resp.message); 
				}
			}
		});
		return false
	  });
  
});