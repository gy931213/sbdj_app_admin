
layui.use(['table'], function(){
  var table = layui.table;
	
  table.render({
    elem: '#table-data',
	url:'/test/table/demo1.json',
	toolbar: '#toolbar',
	title: '小号云库数据',
	cols: [[
	  {type: 'checkbox', fixed: 'left'},
	  {field:'id', title:'序号', width:80, fixed: 'left', unresize: true,align: 'center'},
	  {field:'username', title:'姓名', align: 'center'},
	  {field:'username', title:'电话', align: 'center'},
	  {field:'username', title:'设备串号', align: 'center'},
	  {field:'username', title:'创建时间', align: 'center'},
	  {fixed: 'right', title:'操作', toolbar: '#operation', width:150,align: 'center'}
    ]],
    limit:50,
    limits:[50,100,200,500,1000],
	page: true,
	parseData: function(res){ 
        return {
          "code": res.retCode, 		//解析接口状态
          "msg": res.data.message, 	//解析提示文本
          "count": res.data.total, 	//解析数据长度
          "data": res.data.list 	//解析数据列表
        }
	}
  });
  
  //头工具栏事件
  table.on('toolbar(table-filter)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'getCheckData':
        var data = checkStatus.data;
        layer.alert(JSON.stringify(data));
      break;
      case 'getCheckLength':
        var data = checkStatus.data;
        layer.msg('选中了：'+ data.length + ' 个');
      break;
      case 'isAll':
        layer.msg(checkStatus.isAll ? '全选': '未全选');
      break;
    };
  });
  
  //监听行工具事件
  table.on('tool(table-filter)', function(obj){
    var data = obj.data;
    //console.log(obj)
    if(obj.event === 'del'){
      layer.confirm('真的删除行么', function(index){
        obj.del();
        layer.close(index);
      });
    } else if(obj.event === 'edit'){
      layer.prompt({
        formType: 2
        ,value: data.email
      }, function(value, index){
        obj.update({
          email: value
        });
        layer.close(index);
      });
    }
  });
  
});