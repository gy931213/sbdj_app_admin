$("#swbl_content").css({display: 'none'});
/*!
  * 监听搜索框输入的事件~
 */
	$("input[name='account']").on('input',function(){
		$("#sbwl_account_span").html($(this).val());
	});

	/*!
	 * 监听查询按钮事件~
	 */
	$(".search-button").on('click',function(){
		ajaxRequst();
	});
	
	/*!
	 * 监听回车事件
	 */
	document.onkeydown = function(e){
	    if(e.keyCode == 13){
	    	ajaxRequst();
	    }
	}

	// ajax
	function ajaxRequst(){
		var _account = $("input[name='account']").val();
		if(_account==""){
			 layer.msg('请输入账号进行查询操作');
			 return false
		}
		
		// 加载层~
		var layerMsg = layer.load(2,{ // 此处1没有意义，随便写个东西
		    icon: 0, // 0~2 ,0比较好看
		    shade: [0.5,'black'] // 黑色透明度0.5背景
		});
		
		/*!
		 * 请求后台数据~
		 */
		kits.ajax({
			method:'POST',
			url:'/trumpet/query/search',
			data:{username:_account},
			success:function(resp){
				if(resp){
					layer.close(layerMsg);  // 关闭单个
					var data = eval('(' + resp + ')');
					if(data.retCode==0 && (data.data!="" && data.data!=null)){
						// 数据渲染~
						loadData(data.data);
					}else{
						 layer.msg(data.message);
						 cleanPageData();
						 // 隐藏页面
						 $("#swbl_content").css({display: 'none'});
						 return false
					}
				}
  				
			}
		});
	}
	
	/*!
	 * 数据渲染处理
	 */
	function loadData(data){
		console.log(data)
		// 账号
		$("#sbwl_account_span").html(data.username);
		// 所在地
		$("#location").html(data.area);
		// 性别(f：女 | m：男)
		if(data.sex==""){
			$("#sex").html("保密");			
		}else{
			$("#sex").html(data.sex=="f"?"女":"男");	
		}
		// 认证情况(0 未认证；1 支付宝实名认证；2 支付宝个人认证 ；3 淘宝个人实名认证；4 手机验证；5 营业执照认证)
		$("#utype").html(ChUtype(data.utype));
		// 注册时间
		$("#created").html(kits.dateFtt("yyyy-MM-dd hh:mm:ss",data.regTime));
		// 是否为卖家(false买家， true卖家)
		$("#isSeller").html(data.seller==false?"买":"卖");
		// 买家头像
		if(data.avatar!="" && data.avatar!=null){
			$("#avatar").attr("src",data.avatar);
		}
		//买家信誉值
		$("#bscore").html(data.bscore);
		//买家好评率
		$("#bokPb").html(data.bokPb);
		//买家信誉值等级图片地址
		$("#bLevelIco").attr("src",data.bLevelIco);
		//卖家信誉值
		$("#sscore").html(data.sscore);
		//卖家好评率
		$("#sokPb").html(data.sokPb);
		//卖家家信誉值等级图片地址
		$("#sLevelIco").attr("src",data.sLevelIco);
		// 是否为卖家(false无店铺， true有店铺)
		if(data.hasShop == null){
			$("#hasShop").html("此用户未开通店铺");
		}else{
			$("#hasShop").html(data.hasShop==false?"此用户未开通店铺":"此用户已开通店铺");
		}
		// 最后登录时间
		$("#lastVisit").html(kits.dateFtt("yyyy-MM-dd hh:mm:ss",data.lastVisit));
		// 计算淘龄,单数
		tlDay(data.regTime,data.bscore);
		
		// 深度查询的结果
		// 跑单 
		$(".xhPd").html(data.xhPd == null ? 0 : data.xhPd);
		// 敲诈 
		$(".xhQz").html(data.xhQz == null ? 0 : data.xhQz);
		// 骗子 
		$(".xhPz").html(data.xhPz == null ? 0 : data.xhPz);
		// 打假 
		$(".xhDj").html(data.xhDj == null ? 0 : data.xhDj);
		// 差评 
		$(".xhCp").html(data.xhCp == null ? 0 : data.xhCp);
		// 淘客 
		$(".xhTk").html(data.xhTk == null ? 0 : data.xhTk);
		// 降权 
		$(".xhJq").html(data.xhJq == null ? 0 : data.xhJq);
		// 云黑名单 
		$(".xhHmd").html(data.xhHmd == null ? 0 : data.xhHmd);
		// 活跃度 
		$(".activityLevel").html(data.activityLevel == null ? 0 : data.activityLevel);
	/*	// 商家查询总次数 
		$(".sellerQueryTotal").html(data.sellerQueryTotal);*/
		
		// 显示页面
		$("#swbl_content").css({display: 'block'});
	}
	
	// 验证方式~
	function ChUtype(utype){
		if(utype==0){
			return "未认证";
		}else if(utype==1){
			return "支付宝实名认证";
		}else if(utype==2){
			return "支付宝个人认证";
		}else if(utype==3){
			return "淘宝个人实名认证";
		}else if(utype==4){
			return "手机验证";
		}else if(utype==5){
			return "营业执照认证";
		}
	}
	
	// 计算淘龄~
	function tlDay(regTime,bscore){
		var _regTime = new Date(regTime);
		var _currentTime = new Date();
		var _day = (1000*60*60*24);
		var rel = _currentTime-_regTime;
		var _tl = parseInt(rel/_day);
		$("#tlDay").html(_tl);
		// 计算，周，月，季平均
		average(_tl,bscore);
	}
	
	// 计算周，月，季单数~ 
	function average(tl,bscore){
		// 买家信用
		var _bscore = parseInt(bscore);
		// 周
		var _week = _bscore/(tl/7);
		$("#week").html(_week.toFixed(2));
		// 月
		var _month= _bscore/(tl/30);
		$("#month").html(_month.toFixed(2));
		// 季
		var _season = _bscore/(tl/90);
		$("#season").html(_season.toFixed(2));
	}
	
	/*!
	 * ~清除页面缓存数据~
	 */
	function cleanPageData(){
		// 账号
		$("#sbwl_account_span").html("");
		$("#location").html("----");
		$("#sex").html("----");			
		$("#utype").html("----");
		$("#created").html("----");
		$("#isSeller").html("--");
		$("#avatar").attr("src","/images/timg.gif");
		$("#bscore").html(0);
		$("#bokPb").html("0%");
		$("#bLevelIco").attr("src","");
		$("#sscore").html(0);
		$("#sokPb").html("0%");
		$("#sLevelIco").attr("src","");
		$("#hasShop").html("----");
		$("#lastVisit").html("----");
		$("#week").html(0);
		$("#month").html(0);
		$("#season").html(0);
		$("#tlDay").html(0);
		
		// 跑单 
		$(".xhPd").html(0);
		// 敲诈 
		$(".xhQz").html(0);
		// 骗子 
		$(".xhPz").html(0);
		// 打假 
		$(".xhDj").html(0);
		// 差评 
		$(".xhCp").html(0);
		// 淘客 
		$(".xhTk").html(0);
		// 降权 
		$(".xhJq").html(0);
		// 云黑名单 
		$(".xhHmd").html(0);
		// 活跃度 
		$(".activityLevel").html(0);
		// 商家查询总次数 
		$(".sellerQueryTotal").html(0);
	}
	
