
layui.use(['table','form','laydate'], function(){
    var table = layui.table;
    var laydate = layui.laydate;
    var form  = layui.form;

    //开始日期
    laydate.render({
        elem: '#beginDate',
        type: 'datetime'
    });
    //结束日期
    laydate.render({
        elem: '#endDate',
        type: 'datetime'
    });

    table.render({
        id:'table_list',
        elem: '#table-data',
        url:'/cloud/library/list',
        toolbar: '#toolbar',
        title: '小号云库数据',
        cols: [[
            {type: 'checkbox', fixed: 'left'},
            {field:'id', title:'序号', width:80, fixed: 'left', unresize: true,align: 'center'},
            {field:'username', title:'旺旺名称', align: 'center'},
            {field:'bscore', title:'买家信誉值', align: 'center'},
            {field:'utype', title:'认证情况', align: 'center', templet:function (res) {
                    if (res.utype === '0') {
                        return '未认证';
                    } else if (res.utype === '1') {
                        return  '支付宝实名认证';
                    } else if (res.utype === '2') {
                        return  '支付宝个人认证';
                    } else if (res.utype === '3') {
                        return  '淘宝个人实名认证';
                    } else if (res.utype === '4') {
                        return  '手机验证';
                    } else if (res.utype === '5') {
                        return  '营业执照认证';
                    }
                }},
            {field:'type', title:'用户类型', align: 'center', templet:function (res) {
                    if (res.type === 'B') {
                        return '天猫';
                    } else if (res.type === 'C') {
                        return '普通店铺';
                    }
                }},
            {field:'sex', title:'性别', align: 'center', templet:function (res) {
                    if (res.sex === 'f') {
                        return '女';
                    } else if (res.sex === 'm') {
                        return '男';
                    } else {
                        return '未知';
                    }
                }},
            {field:'weekAvg', title:'周平均', align: 'center'},
            {field:'monthAvg', title:'月平均', align: 'center'},
            {field:'created', title:'账户创建时间', align: 'center', hide: true, templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.created);
                }},
            {field:'area', title:'所在城市', align: 'center', hide: true},
            {field:'avatar', title:'用户头像(图片地址)', align: 'center', hide: true},
            {field:'bLevelIco', title:'买家信誉值等级', align: 'center', hide: true, templet:function (rse) {
                    return '<img style="width: 28px;height: auto;" src="'+rse.bLevelIco+'"></img>'
                }},
            {field:'bokPb', title:'买家好评率', align: 'center', hide: true},
            {field:'hasShop', title:'有无店铺', align: 'center', hide: true, templet:function (rse) {
                    return rse.hasShop ? '有': '无';
                }},
            {field:'isSeller', title:'是否为卖家', align: 'center', hide: true, templet:function (rse) {
                    return rse.isSeller ? '有': '无';
                }},
            {field:'lastVisit', title:'最后登录时间', align: 'center', hide: true, templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.lastVisit);
                }},
            {field:'regTime', title:'注册时间', align: 'center', hide: true, templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.regTime);
                }},
            {field:'sLevelIco', title:'卖家家信誉值等级', align: 'center', hide: true, templet:function (rse) {
                    return '<img style="width: 28px;height: auto;" src="'+rse.sLevelIco+'"></img>'
                }},
            {field:'safeType', title:'安全等级', align: 'center', hide: true},
            {field:'sokPb', title:'店铺好评率', align: 'center', hide: true},
            {field:'sscore', title:'卖家信誉值', align: 'center', hide: true},
            {field:'isTmall', title:'是否为天猫卖家', align: 'center', hide: true},
            {field:'vipLevel', title:'是否为超级会员', align: 'center', hide: true},
            {field:'updateTime', title:'数据更新时间', align: 'center', hide: true, templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }},
            {field:'updaeTotal', title:'数据更新总数', align: 'center', hide: true},
            {field:'status', title:'状态', align: 'center', hide: true},
            {field:'xhPd', title:'跑单', align: 'center', hide: true},
            {field:'xhQz', title:'敲诈', align: 'center', hide: true},
            {field:'xhPz', title:'骗子', align: 'center', hide: true},
            {field:'xhDj', title:'打假', align: 'center', hide: true},
            {field:'xhCp', title:'差评', align: 'center', hide: true},
            {field:'xhTk', title:'淘客', align: 'center', hide: true},
            {field:'xhJq', title:'降权', align: 'center', hide: true},
            {field:'xhHmd', title:'云黑名单', align: 'center', hide: true},
            {field:'activityLevel', title:'活跃度', align: 'center', hide: true},
            {field:'sellerQueryTotal', title:'商家查询总次数', align: 'center', hide: true},
            {field:'nearWeekShop', title:'近一周查询次数', align: 'center', hide: true},
            {field:'lastWeekShop', title:'上一周查询次数', align: 'center', hide: true},
            {field:'tbAge', title:'淘龄', align: 'center', hide: true},
            {field:'seasonAvg', title:'季平均', align: 'center', hide: true},
            {field:'createTime', title:'数据创建时间', align: 'center', templet:function(rse){
                    return  kits.dateFtt("yyyy-MM-dd hh:mm:ss",rse.createTime);
                }}/*,
            {fixed: 'right', title:'操作', toolbar: '#operation', width:150,align: 'center'}*/
        ]],
        limit:50,
        limits:[50,100,200,500,1000],
        page: true,
        parseData: function(res){
            return {
                "code": res.retCode, 		//解析接口状态
                "msg": res.data.message, 	//解析提示文本
                "count": res.data.total, 	//解析数据长度
                "data": res.data.list 	//解析数据列表
            }
        }
    });

    //监听搜索按钮
    form.on('submit(sbwl_search)', function(data){
        table.reload('table_list',{
        	page: {
                curr: 1 //重新从第 1 页开始
             },
             where:data.field
        });
        return false;
    });

    // 刷新列表
    function table_reload(){
        table.reload('table_list');
    }

    //头工具栏事件
    table.on('toolbar(table-filter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'getCheckData':
                var data = checkStatus.data;
                layer.alert(JSON.stringify(data));
                break;
            case 'getCheckLength':
                var data = checkStatus.data;
                layer.msg('选中了：'+ data.length + ' 个');
                break;
            case 'isAll':
                layer.msg(checkStatus.isAll ? '全选': '未全选');
                break;
        };
    });

    //监听行工具事件
    table.on('tool(table-filter)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除行么', function(index){
                obj.del();
                layer.close(index);
            });
        } else if(obj.event === 'edit'){
            layer.prompt({
                formType: 2
                ,value: data.email
            }, function(value, index){
                obj.update({
                    email: value
                });
                layer.close(index);
            });
        }
    });

});