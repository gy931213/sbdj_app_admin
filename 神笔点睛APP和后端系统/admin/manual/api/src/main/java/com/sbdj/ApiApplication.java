package com.sbdj;

import com.sbdj.core.base.BaseWebConfig;
import com.sbdj.core.constant.Version;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * <p>
 * API
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 14:03
 */
@SpringBootApplication
public class ApiApplication extends BaseWebConfig {
    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }

    @Bean
    @Override
    protected Docket api() {
        return super.api("API", 80, Version.V1, "com.sbdj.api");
    }
}
