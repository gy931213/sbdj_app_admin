package com.sbdj;

import com.sbdj.core.base.BaseWebConfig;
import com.sbdj.core.constant.Version;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * <p>
 * APP
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 14:10
 */
@SpringBootApplication
public class AppApplication extends BaseWebConfig {
    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class, args);
    }

    @Bean
    @Override
    protected Docket api() {
        return super.api("APP", 9090, Version.V1, "com.sbdj.app");
    }
}
