package com.sbdj.app.base;

import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.AppCacheKey;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.constant.Version;
import com.sbdj.core.util.QiNuCloudUtil;
import com.sbdj.core.util.RSAUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.entity.ConfigPlatform;
import com.sbdj.service.configure.entity.ConfigQiniu;
import com.sbdj.service.configure.service.IConfigPlatformService;
import com.sbdj.service.configure.service.IQiNiuService;
import com.sbdj.service.member.service.ISalesmanMobileCodeService;
import com.sbdj.service.member.type.SalesmanBillTypeMsg;
import com.sbdj.service.system.entity.SysArea;
import com.sbdj.service.system.entity.SysConfigDtl;
import com.sbdj.service.system.service.ISysAreaService;
import com.sbdj.service.system.service.ISysConfigDtlService;
import com.sbdj.service.system.type.SysConfigType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

/**
 * <p>
 * 基本控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "app公共的控制器")
@RestController
@RequestMapping(value = Version.V1 + "/base")
public class BaseController {
    private Logger logger = LoggerFactory.getLogger(BaseController.class);

    @Autowired
    private IQiNiuService iQiNiuService;
    @Autowired
    private ISysAreaService iAreaService;
    @Autowired
    private ISalesmanMobileCodeService iSalesmanMobileCodeService;
    @Autowired
    private IConfigPlatformService iPlatformService;
    @Autowired
    private ISysConfigDtlService iConfigDtlService;

    @RequestMapping(value = "findkey", method = RequestMethod.GET)
    @ApiOperation(value = "获取加密公钥", notes = "获取公钥接口")
    public RespResult findPublicKey(HttpServletRequest request) {
        String publicKey;
        List<SysConfigDtl> list;
        try {
            publicKey = RSAUtil.findPublicKeyStr();
            list = iConfigDtlService.findConfigDtl(SysConfigType.CODE_TXLTS);
        } catch (Exception e) {
            logger.info("获取加密公钥失败：{" + e.getMessage() + "}");
            return RespResult.error();
        }
        Map<String, String> map = new HashMap<>();
        map.put("public_key", publicKey);
        if(!list.isEmpty()) {
            map.put("count", list.get(0).getName());
        }
        return RespResult.success(map);
    }

    @AppAuth
    @PostMapping(value = "upload")
    @ApiOperation(value = "图片上传", notes = "图片上传接口")
    public RespResult upload(HttpServletRequest request) {
        try {
            //图片上传
            MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
            if (StrUtil.isNull(mRequest)) {
                logger.error("===>图片上传失败，失败原因为：上传文件的对象为空！");
                return RespResult.error("上传失败！");
            }
            Iterator<String> fns = mRequest.getFileNames();
            if (StrUtil.isNull(fns)) {
                logger.error("===>图片上传失败，失败原因为：上传文件的对象为空！");
                return RespResult.error("上传失败！");
            }
            MultipartFile file = null;
            //获取上传的文件列表
            while (fns.hasNext()) {
                String s = fns.next();
                file = mRequest.getFile(s);
            }
            request.setCharacterEncoding("UTF-8");
            if (StrUtil.isNull(file) || StrUtil.isNull(file.getInputStream())) {
                return RespResult.error("上传失败");
            }
            Long identify = request.getHeader("identify") == null ? 0L : Long.parseLong(request.getHeader("identify"));
            Long orgId = AppCacheKey.getOrgId(identify);
            ConfigQiniu qiNiu = iQiNiuService.findOneByOrgId(orgId);
            if (StrUtil.isNull(qiNiu)) {
                logger.error("===>图片上传失败，失败原因为：文件服务配置为空无法上传！");
                return RespResult.error("上传失败！");
            }
            String url = QiNuCloudUtil.uploadFileByInputStream(qiNiu.getZone(), qiNiu.getAccessKey(),qiNiu.getSecretKey(), qiNiu.getBucket(), file.getInputStream());
            if (StrUtil.isNull(url)) {
                logger.error("===>图片上传失败，失败原因为：请求的url为空！");
                return RespResult.error("上传失败！");
            }
            return RespResult.success(qiNiu.getHost() + url + qiNiu.getImageView());
        } catch (IOException e) {
            logger.error("===>图片上传失败，失败原因为：{}",e.getMessage());
            return RespResult.error();
        }
    }

    // by Redis
    @AppAuth
    @GetMapping(value = "area")
    @ApiOperation(value = "获取省市区数据", notes = "获取省市区数据接口")
    public RespResult findAreaData(@ApiParam(required = true, value = "加载页码") @RequestParam Long parentId) {
        if (StrUtil.isNull(parentId)) {
            return RespResult.error();
        }
        List<SysArea> list = iAreaService.findAreaByParentId(parentId);
        return RespResult.success(list);
    }

    // by Redis
    @AppAuth
    @GetMapping(value = "platform")
    @ApiOperation(value = "获取平台数据", notes = "获取平台数据接口")
    public RespResult<List<ConfigPlatform>> findPlatformSelect() {
        List<ConfigPlatform> list = iPlatformService.findPlatformList();
        return RespResult.success(list);
    }

    // by Redis
    /**@Authority
     @GetMapping(value = "loadDTL")
     @ApiOperation(value = "加载证件类型数据", notes = "加载证件类型数据接口")
     public RespResult<List<ConfigDtl>> findIdData(Long id, String code) {
     List<ConfigDtl> list = null;
     if (id != null) {
     list = iConfigDtlService.findConfigDtl(id);
     } else {
     list = iConfigDtlService.findConfigDtl(code);
     }
     return RespResult.success(list);
     }**/

    @AppAuth
    @GetMapping(value = "load/zjlx")
    @ApiOperation(value = "加载证件类型数据", notes = "加载证件类型数据接口")
    public RespResult<List<SysConfigDtl>> loadCodeZjlx() {
        List<SysConfigDtl> list = iConfigDtlService.findConfigDtl(SysConfigType.CODE_ZJLX);
        return RespResult.success(list);
    }

    @AppAuth
    @GetMapping(value = "sendSms")
    @ApiOperation(value = "发送短信", notes = "发送短信接口")
    public RespResult sendSms(HttpServletRequest request, @ApiParam(required = true, value = "手机号码") @RequestParam String mobile) {
        try {
            iSalesmanMobileCodeService.sendSmsBySalesmanNumber(mobile);
        } catch (Exception e) {
            if (e.getMessage().equalsIgnoreCase("号码已被注册")) {
                return RespResult.error("号码已被注册！");
            }
            logger.error("===>发送短信失败，失败原因为：{}",e.getMessage());
            return RespResult.error("服务器繁忙！");
        }
        return RespResult.success("发送成功,请在5分钟内完成注册！");
    }

    /*@AppAuth
    @GetMapping(value = "/bill/type")
    @ApiOperation(value = "账单类型", notes = "账单类型接口")
    public RespResult financeSalesmanBill() {
        Map<String, Object> map;
        List<Map> list = new ArrayList<>();
        map = new HashMap<>();
        map.put("id",SalesmanBillTypeMsg.BILL_TYPE_RECORD.getCodeType());
        map.put("name",SalesmanBillTypeMsg.BILL_TYPE_RECORD.getMessage());
        list.add(map);

        map = new HashMap<>();
        map.put("id",SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getCodeType());
        map.put("name", SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getMessage());
        list.add(map);

        map = new HashMap<>();
        map.put("id",SalesmanBillTypeMsg.BILL_TYPE_PRNALTY.getCodeType());
        map.put("name", SalesmanBillTypeMsg.BILL_TYPE_PRNALTY.getMessage());
        list.add(map);

        map = new HashMap<>();
        map.put("id",SalesmanBillTypeMsg.BILL_TYPE_DIFFER.getCodeType());
        map.put("name", SalesmanBillTypeMsg.BILL_TYPE_DIFFER.getMessage());
        list.add(map);

        map = new HashMap<>();
        map.put("id",SalesmanBillTypeMsg.BILL_TYPE_PENALTY.getCodeType());
        map.put("name", SalesmanBillTypeMsg.BILL_TYPE_PENALTY.getMessage());
        list.add(map);

        map = new HashMap<>();
        map.put("id",SalesmanBillTypeMsg.BILL_TYPE_ROYALTY.getCodeType());
        map.put("name", SalesmanBillTypeMsg.BILL_TYPE_ROYALTY.getMessage());
        list.add(map);

        return RespResult.success(list);
    }*/


    @AppAuth
    @GetMapping(value = "/bill")
    @ApiOperation(value = "账单类型", notes = "账单类型接口")
    public RespResult financeSalesmanBillType(@ApiParam(required = true, value = "加载账单类型[{支出：E},{收入：I }]") @RequestParam String type) {
        if(StrUtil.isNull(type)) {
            return RespResult.error("参数为空");
        }
        Map<String, Object> map;
        List<Map> list = new ArrayList<>();
        // 支出
        if(type.equalsIgnoreCase("E")) {
            // 提现
            map = new HashMap<>();
            map.put("id",SalesmanBillTypeMsg.BILL_TYPE_RECORD.getCodeType());
            map.put("name",SalesmanBillTypeMsg.BILL_TYPE_RECORD.getMessage());
            list.add(map);
            // 佣金
            map = new HashMap<>();
            map.put("id",SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getCodeType());
            map.put("name", SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getMessage());
            list.add(map);
            // 惩罚
            map = new HashMap<>();
            map.put("id",SalesmanBillTypeMsg.BILL_TYPE_PENALTY.getCodeType());
            map.put("name", SalesmanBillTypeMsg.BILL_TYPE_PENALTY.getMessage());
            list.add(map);
            // 差价
            map = new HashMap<>();
            map.put("id",SalesmanBillTypeMsg.BILL_TYPE_DIFFER.getCodeType());
            map.put("name", SalesmanBillTypeMsg.BILL_TYPE_DIFFER.getMessage());
            list.add(map);
        }else {
            // 佣金
            map = new HashMap<>();
            map.put("id",SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getCodeType());
            map.put("name", SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getMessage());
            list.add(map);
            // 提成
            map = new HashMap<>();
            map.put("id",SalesmanBillTypeMsg.BILL_TYPE_ROYALTY.getCodeType());
            map.put("name", SalesmanBillTypeMsg.BILL_TYPE_ROYALTY.getMessage());
            list.add(map);
            // 奖励
            map = new HashMap<>();
            map.put("id",SalesmanBillTypeMsg.BILL_TYPE_PRNALTY.getCodeType());
            map.put("name", SalesmanBillTypeMsg.BILL_TYPE_PRNALTY.getMessage());
            list.add(map);
        }
        return RespResult.success(list);
    }
}
