package com.sbdj.app.base;

import com.sbdj.app.web.AppCacheKey;
import com.sbdj.app.web.AppRequestUtil;
import com.sbdj.app.web.AppSession;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.constant.Status;
import com.sbdj.core.constant.Version;
import com.sbdj.core.util.*;
import com.sbdj.service.member.admin.vo.SalesmanRespVo;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.service.ISalesmanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 登录控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "登录控制器")
@RestController
@RequestMapping(value = Version.V1 + "/login")
public class LoginController {
    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private ISalesmanService iSalesmanService;

    @PostMapping(value = "/sign")
    @ApiOperation(value = "账号密码加密登录", notes = "账号密码加密登录接口")
    public RespResult<SalesmanRespVo> loginByPwd(HttpServletRequest request, HttpServletResponse response,
                                                 @ApiParam(required = true, value = "账号和密码加密后的字符串:账号&密码")
                                                 @RequestParam String code){
        // 判断code为空
        if (StrUtil.isNull(code)) {
            logger.error("==>登录失败，异常为：缺少参数，加密信息为：[{}]",code);
            return RespResult.error("缺失参数");
        }

        try {
            // 数据进行解密
            code = RSAUtil.decryptByPrivateKeyStr(code);
        } catch (Exception e) {
            logger.error("==>登录失败，解密加密数据异常，异常信息为：[{}]，加密信息为：[{}]", e.getMessage(), code);
            return RespResult.error("登录信息不合法");
        }

        // ~判断解密后的数据是否为空，并且是否存在指定的字符!
        if (StrUtil.isNotNull(code) && !code.contains("&")) {
            logger.error("==>登录失败，登录信息不合法，加密信息为：[{}]",code);
            return RespResult.error("登录信息不合法");
        }

        // 根据特殊字符进行切割!
        String[] arr = code.split("&");
        Salesman salesman = iSalesmanService.findSalesmanByMobile(arr[0]);
        if (StrUtil.isNull(salesman)) {
            return RespResult.error("信息不存在");
        }

        // ~ 禁用状态 ~
        if (StrUtil.isNotNull(salesman.getStatus()) && salesman.getStatus().equalsIgnoreCase(Status.STATUS_PROHIBIT)) {
            return RespResult.error("账号禁用");
        }

        // 没有审号状态
        if (StrUtil.isNotNull(salesman.getStatus()) && salesman.getStatus().equalsIgnoreCase(Status.STATUS_UNREVIEWED)) {
            return RespResult.error("账号未审核");
        }

        // 密码进行加密处理!
        String pwd = MD5Util.str2MD5(arr[1] + salesman.getKeyword());
        if (StrUtil.isNull(salesman.getPwd()) || StrUtil.isNull(pwd) || !pwd.equalsIgnoreCase(salesman.getPwd())) {
            return RespResult.error("账号或密码错误");
        }

        String token = StrUtil.getToken(salesman.getId());
        // 判断token为空
        if (StrUtil.isNull(token)) {
            logger.info("==>登录失败，失败原因为：token生成失败！");
            return RespResult.error("登录失败");
        }
        // 保存登录人信息
        SalesmanRespVo sm = new SalesmanRespVo();
        sm.setSalesmanId(salesman.getId());
        sm.setName(salesman.getName());
        sm.setBalance(salesman.getBalance());
        sm.setJobNumber(salesman.getJobNumber());
        sm.setStatus(salesman.getStatus());
        sm.setSurplusCredit(salesman.getSurplusCredit());
        sm.setTotalCredit(salesman.getTotalCredit());
        sm.setToken(token);

        // 保存登录会话信息
        AppSession appSession = new AppSession();
        appSession.setIdentify(sm.getSalesmanId());
        appSession.setAuth(true);
        appSession.setToken(token);
        appSession.setObject(sm);
        appSession.setOrgId(salesman.getOrgId());
        AppRequestUtil.setSession(request, appSession);

        // 保存当前登录会话信息=>Redis 默认保存 3天
        RedisUtil.set(AppCacheKey.getAppCacheKey(sm.getSalesmanId()),appSession, (3*24*60*60));

        // 更新当前登录人，最后一次登录时间，和登录次数.
        iSalesmanService.updateSalesmanLastLoginTimeAndLoginTotal(sm.getSalesmanId());
        // 登录的记录info
        logger.info("==>登录成功：登录信息为：[名称：{} - 账号：{} - Ip：{}]",sm.getName(),salesman.getMobile(), RequestUtil.getIpAddr(request));
        return RespResult.success(sm,"登录成功");
    }

    @PostMapping("logout")
    @ApiOperation(value = "退出登录", notes = "退出登录接口")
    public RespResult logout(HttpServletRequest request) {
        AppCacheKey.sessionInvalidate(request);
        return RespResult.success();
    }
}
