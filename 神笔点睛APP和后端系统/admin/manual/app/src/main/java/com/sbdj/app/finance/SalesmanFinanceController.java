package com.sbdj.app.finance;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.Status;
import com.sbdj.core.constant.Version;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.entity.ConfigQuotaSetting;
import com.sbdj.service.configure.service.IConfigQuotaSettingService;
import com.sbdj.service.finance.admin.vo.DrawMoneyRecordVo;
import com.sbdj.service.finance.admin.vo.PrizePenaltyVo;
import com.sbdj.service.finance.app.vo.PercentageListVo;
import com.sbdj.service.finance.app.vo.PercentageTotalVo;
import com.sbdj.service.finance.app.vo.SalesmanBillRespVo;
import com.sbdj.service.finance.entity.FinanceDrawMoneyRecord;
import com.sbdj.service.finance.service.IFinanceDrawMoneyRecordService;
import com.sbdj.service.finance.service.IFinancePercentageService;
import com.sbdj.service.finance.service.IFinancePrizePenaltyService;
import com.sbdj.service.finance.service.IFinanceSalesmanBillService;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.task.app.vo.TaskSonCommissionTotalApi;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.service.ITaskSonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 业务员财务
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "财务管理")
@RequestMapping(value = Version.V1 + "/finance")
@RestController
public class SalesmanFinanceController {
    private Logger logger = LoggerFactory.getLogger(SalesmanFinanceController.class);

    /*@Autowired
    private IFinancePrizePenaltyService iPrizePenaltyService;
    @Autowired
    private ITaskSonService iTaskSonService;
    @Autowired
    private IFinancePercentageService iPercentageService;
    @Autowired
    private IConfigQuotaSettingService iQuotaSettingService;*/
    @Autowired
    private IFinanceDrawMoneyRecordService iDrawMoneyRecordService;
    @Autowired
    private ISalesmanService iSalesmanService;
    @Autowired
    private IFinanceSalesmanBillService iSalesmanBillService;


   /* @AppAuth
    @GetMapping(value = "penalty")
    @ApiOperation(value = "惩罚奖励列表明细", notes = "惩罚奖励列表明细接口")
    public RespResult<PageData<PrizePenaltyVo>> findPenaltyList(HttpServletRequest request,
                                                                @ApiParam(required = true, value = "类型[{PR：奖励}，{PE：惩罚}]") @RequestParam String type,
                                                                @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        HttpReqParam reqParam = new HttpReqParam();
        reqParam.setLimit(30);
        IPage<PrizePenaltyVo> penaltyList = iPrizePenaltyService.findPenaltyList(uid, PageRequestUtil.buildPageRequest(reqParam), type);
         return RespResult.buildPageData(penaltyList);
    }

    // 待修改

    @AppAuth
    @GetMapping(value = "commission")
    @ApiOperation(value = "查询当前业务员佣金列表", notes = "查询当前业务员佣金接口")
    public RespResult findCommission(HttpServletRequest request,
                                     @ApiParam(value = "time（日期，time为空就获取最新的30天数据，不为空就按时间查询）") @RequestParam String time,
                                     @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        if (StrUtil.isNull(time)) {
            HttpReqParam reqParam = new HttpReqParam();
            reqParam.setLimit(30);
            IPage<TaskSonCommissionTotalApi> commissionList = iTaskSonService.findCommission(uid, PageRequestUtil.buildPageRequest(reqParam));
            return RespResult.buildPageData(commissionList);
        } else {
            List<TaskSon> commissionList = iTaskSonService.findCommission(uid, time);
            return RespResult.success(commissionList);
        }
    }


    @AppAuth
    @GetMapping(value = "record")
    @ApiOperation(value = "加载提现列表", notes = "加载提现列表接口")
    public RespResult<PageData<DrawMoneyRecordVo>> findRecord(HttpServletRequest request,
                                                              @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        HttpReqParam reqParam = new HttpReqParam();
        reqParam.setLimit(30);
        IPage<DrawMoneyRecordVo> recordPage = iDrawMoneyRecordService.findRecordList(uid, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(recordPage);
    }


    @AppAuth
    @GetMapping(value = "loadData")
    @ApiOperation(value = "加载初始数据", notes = "加载初始数据接口")
    public RespResult init(HttpServletRequest request,
                           @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        // 奖励罚款
        PrizePenaltyVo penaltyListPE = iPrizePenaltyService.findPenaltyList(uid, Status.PRICE_PUNISH);
        PrizePenaltyVo penaltyListPR = iPrizePenaltyService.findPenaltyList(uid, Status.PRICE_REWORD);
        // 业务员账户余额
        Salesman salesmanOne = iSalesmanService.getById(uid);
        // 余额上下限数据
        ConfigQuotaSetting quotaSettingByOrgId = iQuotaSettingService.findQuotaSettingByOrgId(salesmanOne.getOrgId());
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaxLimit", quotaSettingByOrgId.getMaxQuota());
        map.put("MinLimit", quotaSettingByOrgId.getMinQuota());
        map.put("record", salesmanOne.getBalance());
        map.put("prizePenaltyPE", penaltyListPE);
        map.put("prizePenaltyPR", penaltyListPR);
        return RespResult.success(map);
    }


    @AppAuth
    @GetMapping(value = "percentage")
    @ApiOperation(value = "查询提成列表", notes = "查询提成列表接口")
    public RespResult Percentage(HttpServletRequest request,
                                 @ApiParam(value = "time（日期，time为空就获取最新的30天数据，不为空就按时间查询）") @RequestParam String time,
                                 @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        if (StrUtil.isNull(time)) {
            HttpReqParam reqParam = new HttpReqParam();
            reqParam.setLimit(30);
            IPage<PercentageTotalVo> list = iPercentageService.findPercentageList(PageRequestUtil.buildPageRequest(reqParam), uid);
            return RespResult.buildPageData(list);
        } else {
            List<PercentageListVo> percentageList = iPercentageService.findPercentageList(uid, time);
            return RespResult.success(percentageList);
        }
    }*/


    @AppAuth
    @PostMapping(value = "cashIn")
    @ApiOperation(value = "提现", notes = "提现接口")
    public RespResult CashIn(HttpServletRequest request,
                             @ApiParam(required = true, value = "要提现的金额") @RequestParam BigDecimal amount,
                             @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        //Long orgId = AppCacheKey.getOrgId(uid);
        // 查询正在提现的提现记录
        FinanceDrawMoneyRecord drawMoneyRecord = iDrawMoneyRecordService.findFailRecord(uid);
        if (null != drawMoneyRecord) {
            return RespResult.error("您有一笔提现正在审核中，请稍后！");
        }
        try {
            //iDrawMoneyRecordService.saveDrawMoneyRecord(amount, uid, orgId);
            iSalesmanService.withdrawSalesmanMoney(uid, amount);
        } catch (Exception e) {
            logger.error("系统提现失败，失败原因为：{}", e.getMessage());
            return RespResult.error();
        }
        return RespResult.success("申请成功，请耐心等待！");
    }

    @AppAuth
    @GetMapping(value = "/salesman/bill")
    @ApiOperation(value = "业务员账单信息", notes = "业务员账单信息接口")
    public RespResult findSalesmanBill(
            @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid,
            @ApiParam(required = true, value = "页码") @RequestParam int page,
            @ApiParam(value = "开始日期") @RequestParam String startDate,
            @ApiParam(value = "结束日期") @RequestParam String endDate,
            @ApiParam(value = "账单类型：") @RequestParam Integer billTypeId,
            @ApiParam(value = "收支类型：[{收入：I},{支出 ：E}]") @RequestParam String consumeType) {
        HttpReqParam httpReqParam = new HttpReqParam();
        httpReqParam.setPage(page);
        httpReqParam.setLimit(20);
        IPage<SalesmanBillRespVo> sbrv = iSalesmanBillService.findSalesmanBillRespVo(uid, startDate, endDate, consumeType, billTypeId, PageRequestUtil.buildPageRequest(httpReqParam));
        BigDecimal totalMoney = iSalesmanBillService.salesmanBillTotalMoney(uid, startDate, endDate, consumeType, billTypeId);
        Map<String, Object> map = new HashMap<>();
        PageData<SalesmanBillRespVo> pd = new PageData<>();
        if (sbrv.getRecords().size() > 0) {
            pd.setPage(sbrv.getCurrent());
            pd.setLimit(sbrv.getSize());
            pd.setTotal(sbrv.getTotal());
            pd.setList(sbrv.getRecords());
        }
        map.put("sbrv", pd);
        map.put("totalMoney", totalMoney);
        return RespResult.success(map);
    }



    @AppAuth
    @GetMapping(value = "/salesman/balance")
    @ApiOperation(value = "业务员余额", notes = "业务员余额接口")
    public RespResult findSalesmanBalance(@ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        Map<String, Object> map = iSalesmanBillService.salesmanBillInfo(uid);
        return RespResult.success(map);
    }

}
