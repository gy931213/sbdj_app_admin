package com.sbdj.app.information;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.AppCacheKey;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.Version;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.information.admin.vo.InfoHelpVo;
import com.sbdj.service.information.admin.vo.InfoNoticeVo;
import com.sbdj.service.information.service.IInfoHelpService;
import com.sbdj.service.information.service.IInfoNoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 信息控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "信息控制器")
@RequestMapping(value = Version.V1 + "/notice")
@RestController
public class InformationController {

    private Logger logger = LoggerFactory.getLogger(InformationController.class);


    @Autowired
    private IInfoNoticeService iInfoNoticeService;

    @Autowired
    private IInfoHelpService iInfoHelpService;

    @AppAuth
    @GetMapping(value = "loadList")
    @ApiOperation(value = "加载公告列表", notes = "加载公告列表接口")
    public RespResult<PageData<InfoNoticeVo>> LoadList(HttpServletRequest request ,
                                                       @ApiParam(required = true, value = "请求页") @RequestParam int page,
                                                       @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        try {
            // 机构id从缓存中获取
            Long orgId = AppCacheKey.getOrgId(uid);
            HttpReqParam httpReqParam = new HttpReqParam();
            httpReqParam.setPage(page);
            httpReqParam.setLimit(10);
            IPage<InfoNoticeVo> page1 = iInfoNoticeService.findNoticePageList(orgId, PageRequestUtil.buildPageRequest(httpReqParam));
            return RespResult.buildPageData(page1);
        } catch (Exception e) {
            logger.error("===>加载公告列表失败，失败原因为：{}",e.getMessage());
            return RespResult.error("加载失败！");
        }
    }

    @AppAuth
    @GetMapping(value = "helpList")
    @ApiOperation(value = "加载新手帮助列表", notes = "加载新手帮助列表接口")
    public RespResult<PageData<InfoHelpVo>> helpList(HttpServletRequest request ,
                                                     @ApiParam(required = true, value = "请求页") @RequestParam int page,
                                                     @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid){
        try {
            // 机构id从缓存中获取
            Long orgId = AppCacheKey.getOrgId(uid);
            HttpReqParam httpReqParam = new HttpReqParam();
            httpReqParam.setPage(page);
            httpReqParam.setLimit(10);
            IPage<InfoHelpVo> helpPageList = iInfoHelpService.findHelpPageList(orgId, PageRequestUtil.buildPageRequest(httpReqParam));
            return RespResult.buildPageData(helpPageList);
        } catch (Exception e) {
            logger.error("===>加载新手帮助列表，失败原因为：{}",e.getMessage());
            return RespResult.error("加载失败！");
        }
    }
}
