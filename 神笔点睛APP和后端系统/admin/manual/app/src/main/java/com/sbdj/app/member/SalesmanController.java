package com.sbdj.app.member;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.AppRequestUtil;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.SrcType;
import com.sbdj.core.constant.Status;
import com.sbdj.core.constant.Version;
import com.sbdj.core.util.MD5Util;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.admin.query.RegisterQuery;
import com.sbdj.service.member.admin.vo.SalesmanImagesVo;
import com.sbdj.service.member.admin.vo.SalesmanListVo;
import com.sbdj.service.member.admin.vo.SalesmanRespVo;
import com.sbdj.service.member.admin.vo.SalesmanVo;
import com.sbdj.service.member.app.vo.SalesmanReqVo;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanImages;
import com.sbdj.service.member.entity.SalesmanMobileCode;
import com.sbdj.service.member.entity.SalesmanPhones;
import com.sbdj.service.member.service.ISalesmanImagesService;
import com.sbdj.service.member.service.ISalesmanMobileCodeService;
import com.sbdj.service.member.service.ISalesmanPhonesService;
import com.sbdj.service.member.service.ISalesmanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 业务员控制器
 * </p>
 *
 * @author Zc
 * @since 2019-11-28
 */
@Api(tags = "APP业务员控制器")
@RequestMapping(value = Version.V1 + "/salesman")
@RestController
public class SalesmanController {
    private Logger logger = LoggerFactory.getLogger(SalesmanController.class);

    @Autowired
    private ISalesmanImagesService iSalesmanImagesService;
    @Autowired
    private ISalesmanService iSalesmanService;
    @Autowired
    private ISalesmanMobileCodeService iSalesmanMobileCodeService;
    @Autowired
    private ISalesmanPhonesService iSalesmanPhonesService;

    @AppAuth
    @GetMapping(value = "list")
    @ApiOperation(value = "加载邀请列表", notes = "加载邀请列表接口")
    public RespResult<PageData<SalesmanListVo>> findSalesmansByPage(HttpServletRequest request,
                                                                    @ApiParam(required = true, value = "加载页码") @RequestParam Integer page,
                                                                    @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        HttpReqParam reqParam = new HttpReqParam();
        reqParam.setPage(page);
        IPage<SalesmanListVo> salesmanList = iSalesmanService.findAllSalesman(PageRequestUtil.buildPageRequest(reqParam),
                uid);
        return RespResult.buildPageData(salesmanList);
    }


    @AppAuth
    @GetMapping(value = "register")
    @ApiOperation(value = "邀请注册", notes = "邀请注册接口")
    public RespResult register(HttpServletRequest request, @ApiParam(value = "业务员信息") RegisterQuery registerParam) {
        if (iSalesmanService.findSalesmanByMobile(registerParam.getMobile()) != null) {
            return RespResult.error("该号码已注册！");
        }
        // 2019年9月2日 18:27:30 改动的地方
		/*SalesmanMobileCode salesmanMobileCode = null;
		Date expireTime = null;
		try {
			salesmanMobileCode = iSalesmanMobileCodeService.findSalesmanMobileCode(registerParam.getMobile());
			expireTime = salesmanMobileCode.getExpireTime();
		} catch (NullPointerException e) {
			return RespResult.error("请输入正确手机号！");
		}
		boolean FiveMinutes = new Date(System.currentTimeMillis()).getTime() > expireTime.getTime();
		if (!registerParam.getCode().equals("") || registerParam.getCode() != null) {
			if (FiveMinutes) {
				return RespResult.error("验证码过期！");
			}
			if (registerParam.getCode().equals(salesmanMobileCode.getCode())) {
				// iSalesmanMobileCodeService.register(registerParam,
				// RequestKit_api.identify_app(request));
				iSalesmanMobileCodeService.register(registerParam, registerParam.getSalesmanId());
			} else {
				return RespResult.error("验证码有误！");
			}
		} else {
			return RespResult.error("请输入验证码！");
		}*/
		// 去除空格
        registerParam.setMobile(StrUtil.removeAllBlank(registerParam.getMobile()));
        registerParam.setOnlineMobile(StrUtil.removeAllBlank(registerParam.getOnlineMobile()));
        registerParam.setName(StrUtil.removeAllBlank(registerParam.getName()));
        iSalesmanMobileCodeService.register(registerParam, registerParam.getSalesmanId());
        return RespResult.success();
    }

    @AppAuth
    @PostMapping(value = "update")
    @ApiOperation(value = "个人资料修改", notes = "个人资料修改接口")
    public RespResult update(HttpServletRequest request, @ApiParam(value = "个人资料") SalesmanReqVo smReqVo) {
        try {
            if (StrUtil.isNull(smReqVo) || StrUtil.isNull(smReqVo.getMobile())) {
                return RespResult.error("提交的数据不完整");
            }
            // 查看该账号是否存在
            Salesman salesmanByMobile = iSalesmanService.findSalesmanByMobile(smReqVo.getMobile());
            // 业务员原本号码
            Salesman salesman = iSalesmanService.getById(smReqVo.getUid());
            // 判断数据不等于空
            if (StrUtil.isNotNull(salesmanByMobile)) {
                if (!salesmanByMobile.getMobile().equals(salesman.getMobile())) {
                    return RespResult.error("已被注册");
                }
            }
            logger.info("==>【个人资料修改】, 这个小鬼 :【{}】,【{}】更改个人资料！原始信息：[身份证号:{},QQ:{},微信:{}],更改信息：[身份证号:{},QQ:{},微信:{}]",
                    smReqVo.getName(), smReqVo.getMobile(), salesman.getCardNum(), salesman.getQq(), salesman.getWechart(), smReqVo.getCardNum(), smReqVo.getQq(), smReqVo.getWechart());
            salesman.setCardNum(StrUtil.removeAllBlank(smReqVo.getCardNum()));
            salesman.setQq(StrUtil.removeAllBlank(smReqVo.getQq()));
            salesman.setWechart(StrUtil.removeAllBlank(smReqVo.getWechart()));
            salesman.setCityId(smReqVo.getCityId());
            salesman.setCountyId(smReqVo.getCountyId());
            salesman.setProvinceId(smReqVo.getProvinceId());
            salesman.setStatus(Status.STATUS_PROHIBIT);

            iSalesmanService.updateSalesmanInfo(salesman);
            iSalesmanImagesService.deleteSalesmanImages("SM", salesman.getId());

            // 保存资源图片
            if (null != smReqVo.getSrc() && !smReqVo.getSrc().isEmpty()) {
                List<SalesmanImages> list = JSONArray.parseArray(smReqVo.getSrc(), SalesmanImages.class);
                for (SalesmanImages salesmanImages : list) {
                    if (StrUtil.isNotNull(salesmanImages.getLink()) && StrUtil.isNotNull(salesmanImages.getTypeId())) {
                        salesmanImages.setSrcId(salesman.getId());
                        salesmanImages.setSrcType(SrcType.SRC_TYPE_SM);
                        iSalesmanImagesService.saveSalesmanImages(salesmanImages);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return RespResult.error("修改失败");
        }
        return RespResult.success("成功");
    }

    @AppAuth
    @GetMapping(value = "echo")
    @ApiOperation(value = "个人资料回显", notes = "个人资料回显接口")
    public RespResult echo(HttpServletRequest request,
                           @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        Map<String, Object> map = new HashMap<>();
        SalesmanVo salesman = iSalesmanService.findSalesmanVoInfo(uid);
        List<SalesmanImagesVo> salImg = iSalesmanImagesService.findSalesmanImagesBySrcIdAndSrcType(salesman.getId(), SrcType.SRC_TYPE_SM);
        map.put("sal", salesman);
        map.put("img", salImg.size() <= 0 ? null : salImg);
        return RespResult.success(map);
    }


    @AppAuth
    @PostMapping(value = "check")
    @ApiOperation(value = "旧密码核对", notes = "密码核对接口")
    public RespResult check(HttpServletRequest request,
                            @ApiParam(required = true, value = "密码") @RequestParam String password,
                            @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        Salesman salesmanOne = iSalesmanService.getById(uid);
        String keyword = salesmanOne.getKeyword();
        String encryptPwd = MD5Util.str2MD5(password + keyword);
        if (!encryptPwd.equals(salesmanOne.getPwd())) {
            System.out.println("错误");
            return RespResult.error("请输入正确密码！");
        }
        return RespResult.success();
    }

    @AppAuth
    @PostMapping(value = "updatePwd")
    @ApiOperation(value = "密码修改", notes = "密码修改接口")
    public RespResult updatePwd(HttpServletRequest request,
                                @ApiParam(required = true, value = "密码") @RequestParam String newPwd,
                                @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        Salesman salesmanOne = iSalesmanService.getById(uid);
        String keyword = salesmanOne.getKeyword();
        String encryptPwd = MD5Util.str2MD5(newPwd + keyword);
        iSalesmanService.updateSalesmanPassword(encryptPwd, uid);
        AppRequestUtil.sessionInvalidate(request);
        return RespResult.success("成功");
    }

    @AppAuth
    @GetMapping(value = "load")
    @ApiOperation(value = "刷新当前登录人信息", notes = "刷新当前登录人信息的接口")
    public RespResult<Salesman> loadSalesman(HttpServletRequest request,
                                             @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        Salesman ext = iSalesmanService.getById(uid);
        System.out.println(ext);
        return RespResult.success(ext);
    }

    @AppAuth
    @PostMapping(value = "phones")
    @ApiOperation(value = "保存当前登录人通讯录信息", notes = "保存当前登录人通讯录信息接口")
    public void saveSalesmanPhone(HttpServletRequest request/*,
                                  @ApiParam(required = true, value = "当前登录人的通讯录：[{'imei':'123456','name':'李四','phone':'13650654654'}]") @RequestParam String item*/,
                                  @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        List<SalesmanPhones> list = JSONArray.parseArray("[{'imei':'123456','name':'李四','phone':'13650654654'}]", SalesmanPhones.class);
        if (!list.isEmpty()) {
            for (SalesmanPhones sp : list) {
                SalesmanPhones spOne = iSalesmanPhonesService.findSalesmanPhonesByImeiAndPhone(uid, SrcType.SRC_TYPE_SM,
                        sp.getPhone(), sp.getImei());
                if (null == spOne) {
                    SalesmanPhones sp1 = new SalesmanPhones();
                    sp1.setSrcId(uid);
                    sp1.setImei(sp.getImei());
                    sp1.setName(sp.getName());
                    sp1.setPhone(sp.getPhone());
                    sp1.setSrcType(SrcType.SRC_TYPE_SM);
                    iSalesmanPhonesService.saveSalesmanPhones(sp1);
                }
            }
        }
    }

    @AppAuth
    @GetMapping(value = "/info")
    @ApiOperation(value = "获取业务员信息", notes = "获取业务员信息数据接口")
    public RespResult<SalesmanRespVo> loadSalesmanInfo(
            @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        if(StrUtil.isNull(uid)) {
            return RespResult.error();
        }
        return RespResult.success(iSalesmanService.getSalesmanInfo(uid));
    }
}
