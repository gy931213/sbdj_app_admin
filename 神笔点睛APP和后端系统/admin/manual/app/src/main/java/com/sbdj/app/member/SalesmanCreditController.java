package com.sbdj.app.member;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.Version;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.finance.service.ISalesmanCreditRecordService;
import com.sbdj.service.member.admin.vo.SalesmanVo;
import com.sbdj.service.member.scheduler.vo.SalesmanCreditRecordVo;
import com.sbdj.service.member.service.ISalesmanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 业务员信用控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "业务员信用控制器")
@RequestMapping(value = Version.V1 + "/salesman/credit")
@RestController
public class SalesmanCreditController {

    @Autowired
    private ISalesmanCreditRecordService iSalesmanCreditRecordService;
    @Autowired
    private ISalesmanService iSalesmanService;

    @AppAuth
    @GetMapping(value = "list")
    @ApiOperation(value = "加载信用记录列表", notes = "加载信用记录列表接口")
    public RespResult<PageData<SalesmanCreditRecordVo>> findCreditList(HttpServletRequest request,
                                                                       @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid){
        HttpReqParam reqParam = new HttpReqParam();
        reqParam.setLimit(30);
        IPage<SalesmanCreditRecordVo> list =  iSalesmanCreditRecordService.findCreditList(PageRequestUtil.buildPageRequest(reqParam) , uid);
        return RespResult.buildPageData(list);
    }

    @AppAuth
    @GetMapping(value = "data")
    @ApiOperation(value = "加载我的信用数据列表", notes = "加载我的信用数据接口")
    public RespResult<SalesmanVo> findSalesmanCredit(HttpServletRequest request,
                                                     @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid){
        SalesmanVo salesmanOne = iSalesmanService.findSalesmanExtCredit(uid);
        return RespResult.success(salesmanOne);
    }


}
