package com.sbdj.app.member;

import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.constant.Version;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.admin.vo.SalesmanLeaderboardVo;
import com.sbdj.service.member.service.ISalesmanPercentageLeaderboardService;
import com.sbdj.service.member.service.ISalesmanStatementLeaderboardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * <p>
 * 业务员排行榜
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "业务员排行榜控制器")
@RequestMapping(value = Version.V1 + "/salesman/leader/board")
@RestController
public class SalesmanLeaderBoardController {

    @Autowired
    private ISalesmanStatementLeaderboardService iSalesmanStatementLeaderboardService;
    @Autowired
    private ISalesmanPercentageLeaderboardService iSalesmanPercentageLeaderboardService;

    @AppAuth
    @RequestMapping("/statement")
    @ApiOperation(value = "业务员佣金排行榜查询", notes = "业务员佣金排行榜接口")
    public RespResult listStatementLeaderboard(@ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid,
                                               @ApiParam(required = false, value = "查询月份") @RequestParam String date) {
        // 机构id从缓存中获取
        Long orgId = 2L;//AppCacheKey.getOrgId(uid);
        if (StrUtil.isNull(date)) {
            date = getLastMonth();
        }
        List<SalesmanLeaderboardVo> list = iSalesmanStatementLeaderboardService.findSalesmanStatementLeaderboardByOrgIdAndDate(orgId, date);
        return RespResult.success(list);
    }

    @AppAuth
    @RequestMapping("/percentage")
    @ApiOperation(value = "业务员提成排行榜查询", notes = "业务员提成排行榜接口")
    public RespResult listPercentageLeaderboard(@ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid,
                                                @ApiParam(required = false, value = "查询月份") @RequestParam String date) {
        // 机构id从缓存中获取
        Long orgId = 2L;//AppCacheKey.getOrgId(uid);
        if (StrUtil.isNull(date)) {
            date = getLastMonth();
        }
        List<SalesmanLeaderboardVo> list = iSalesmanPercentageLeaderboardService.findSalesmanPercentageLeaderboardByOrgIdAndDate(orgId, date);
        return RespResult.success(list);
    }

    // 获取上个月份
    private String getLastMonth() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        return sdf.format(calendar.getTime());
    }

}
