package com.sbdj.app.member;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.SrcType;
import com.sbdj.core.constant.Status;
import com.sbdj.core.constant.Version;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.entity.ConfigPlatform;
import com.sbdj.service.configure.service.IConfigPlatformService;
import com.sbdj.service.member.admin.vo.SalesmanImagesVo;
import com.sbdj.service.member.admin.vo.SalesmanNumberListVo;
import com.sbdj.service.member.app.vo.SalesmanNumberRespVo;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanImages;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.member.service.ISalesmanImagesService;
import com.sbdj.service.member.service.ISalesmanNumberService;
import com.sbdj.service.member.service.ISalesmanNumberTmpService;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.task.app.vo.TaskGoodsVo;
import com.sbdj.service.task.entity.TaskSetting;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.service.ITaskSettingService;
import com.sbdj.service.task.service.ITaskSonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 号主控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "号主控制器")
@RequestMapping(value = Version.V1 + "/Salesman/number")
@RestController
public class SalesmanNumberController {
    private Logger logger = LoggerFactory.getLogger(SalesmanNumberController.class);

    @Autowired
    private ISalesmanService iSalesmanService;
    @Autowired
    private ISalesmanNumberService iSalesmanNumberService;
    @Autowired
    private ISalesmanImagesService iSalesmanImagesService;
    @Autowired
    private ITaskSonService iTaskSonService;
    @Autowired
    private ITaskSettingService iTaskSettingService;
    @Autowired
    private ISalesmanNumberTmpService iSalesmanNumberTmpService;
    @Autowired
    private IConfigPlatformService iConfigPlatformService;


    @AppAuth
    @GetMapping(value = "search")
    @ApiOperation(value = "常用|隐藏号主查询", notes = "常用号主查询接口")
    public RespResult<PageData<SalesmanNumberListVo>> findSalesmanNumberPage(HttpServletRequest request,
                 @ApiParam(required = true, value = "页码") @RequestParam int page,
                 @ApiParam(required = true, value = "启用组，启用组[{E：启用},{P：禁用}]") @RequestParam String status,
                 @ApiParam(value = "所属平台") @RequestParam Long platformId,
                 @ApiParam(value = "搜索条件") @RequestParam String onlineid,
                 @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        HttpReqParam reqParam = new HttpReqParam();
        reqParam.setPage(page);
        IPage<SalesmanNumberListVo> snlPage = iSalesmanNumberService.findSalesmanNumberPageApp(PageRequestUtil.buildPageRequest(reqParam), status,onlineid, platformId, uid);
        return RespResult.buildPageData(snlPage);
    }

    @AppAuth
    @GetMapping(value = "set")
    @ApiOperation(value = "设置[常用|隐藏]状态", notes = "设置[常用|隐藏]状态接口")
    public RespResult setUsed(HttpServletRequest request,
                              @ApiParam(required = true, value = "号主id") @RequestParam Long id,
                              @ApiParam(required = true, value = "1为常用，0不常用") @RequestParam String often) {
        try {
            iSalesmanNumberService.setUsed(id, often);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success("成功");
    }


    @AppAuth
    @GetMapping(value = "echo")
    @ApiOperation(value = "编辑时回显号主信息", notes = "编辑时回显号主信息接口")
    public RespResult echoSalesmanNumber(HttpServletRequest request,
                                         @ApiParam(required = true, value = "号主id") @RequestParam Long id) {
        Map<String, Object> map = new HashMap<>();
        SalesmanNumberRespVo salesmanNumber = iSalesmanNumberService.findSalesmanNumberById(id);
        List<SalesmanImagesVo> salImg = iSalesmanImagesService.findSalesmanImagesBySrcIdAndSrcType(id, SrcType.SRC_TYPE_SN);
        map.put("sal", salesmanNumber);
        map.put("img", salImg);
        return RespResult.success(map);
    }


    @AppAuth
    @PostMapping(value = "edit")
    @ApiOperation(value = "编辑号主信息", notes = "编辑号主信息接口")
    public RespResult editSalesmanNumber( @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid,
                                          @ApiParam(value = "号主信息") SalesmanNumberRespVo snrv) {
        SalesmanNumber salesmanNumber = new SalesmanNumber();
        salesmanNumber.setCityId(snrv.getCityId());
        salesmanNumber.setCountyId(snrv.getCountyId());
        salesmanNumber.setImei(snrv.getImei());
        salesmanNumber.setProvinceId(snrv.getProvinceId());
        salesmanNumber.setPlatformId(snrv.getPlatformId());
        salesmanNumber.setId(snrv.getId());
        // 去掉空格
        salesmanNumber.setCardNum(StrUtil.removeAllBlank(snrv.getCardNum()));
        salesmanNumber.setMobile(StrUtil.removeAllBlank(snrv.getMobile()));
        salesmanNumber.setOnlineid(StrUtil.removeAllBlank(snrv.getOnlineid()));
        salesmanNumber.setName(StrUtil.removeAllBlank(snrv.getName()));

        try {
            if(null == salesmanNumber.getId()) {
                return RespResult.error("更新失败！");
            }

            // 判断该号主是否存在
            SalesmanNumber number = iSalesmanNumberService.findSalesmanNumberByPlatformIdOnlineId(salesmanNumber.getPlatformId(), salesmanNumber.getOnlineid());
            if(null!=number && !salesmanNumber.getId().equals(number.getId())) {
                return RespResult.error("该平台ID已存在 ！");
            }

            ConfigPlatform platform = iConfigPlatformService.findPlatformOneById(salesmanNumber.getPlatformId());
            if(null == platform) {
                return RespResult.error("更新失败！");
            }

            iSalesmanNumberService.updatesalesmanNumber(salesmanNumber);
            iSalesmanImagesService.deleteSalesmanImages(SrcType.SRC_TYPE_SN, salesmanNumber.getId());
            if (StrUtil.isNotNull(snrv.getResource())) {
                List<SalesmanImages> list = JSONArray.parseArray(snrv.getResource(), SalesmanImages.class);
                for (SalesmanImages salesmanImages : list) {
                    salesmanImages.setSrcId(salesmanNumber.getId());
                    salesmanImages.setSrcType(SrcType.SRC_TYPE_SN);
                    // ~保存资源图片
                    iSalesmanImagesService.saveSalesmanImages(salesmanImages);
                }
            }
        } catch (Exception e) {
            return RespResult.error("修改失败");
        }
        return RespResult.success("成功");
    }

    @AppAuth
    @PostMapping(value = "add")
    @ApiOperation(value = "新增号主", notes = "新增号主接口")
    public RespResult addSalesmanNumber(@ApiParam(value = "号主信息")String data,
                                        @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        Gson gson = new GsonBuilder().create();
        //SalesmanNumberRespVo snrv = JSONObject.parseObject(data, SalesmanNumberRespVo.class);
        SalesmanNumberRespVo snrv = gson.fromJson(data, SalesmanNumberRespVo.class);
        // 号主只能添加一个
        Integer flag = iSalesmanNumberService.findSalesmanNumberTotalBySalesmanIdAndPlatformId(uid, snrv.getPlatformId());
        if (flag > 0) {
            return RespResult.error("号主只能录一个!");
        }
        // JSONObject to Bean
        SalesmanNumber salesmanNumber = new SalesmanNumber();
        salesmanNumber.setSalesmanId(uid);
        // 去掉空格
        salesmanNumber.setCardNum(StrUtil.removeAllBlank(snrv.getCardNum()));
        salesmanNumber.setMobile(StrUtil.removeAllBlank(snrv.getMobile()));
        salesmanNumber.setOnlineid(StrUtil.removeAllBlank(snrv.getOnlineid()));
        salesmanNumber.setName(StrUtil.removeAllBlank(snrv.getName()));
        salesmanNumber.setCityId(snrv.getCityId());
        salesmanNumber.setCountyId(snrv.getCountyId());
        salesmanNumber.setImei(snrv.getImei());
        salesmanNumber.setProvinceId(snrv.getProvinceId());
        salesmanNumber.setPlatformId(snrv.getPlatformId());


        //验证手机号
        //String regex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$";
        String phone = salesmanNumber.getMobile();
        //获取1出现在第几个位置若不是第[0]个则说明有错误
        int index = phone.indexOf("1");
        if(salesmanNumber.getMobile().length() != 11){
            return RespResult.error("手机号应为11位数");
        }else if(StrUtil.isNull(phone)||index!=0){
            return RespResult.error("请输入正确手机号");
        }
        // 判断手机号是否重复
        if (iSalesmanNumberService.isExistSalesmanNumberByMobile(phone, salesmanNumber.getPlatformId())) {
            return RespResult.error("该手机号已被注册");
        }
        // 判断新增号主信息是否存在
        if (iSalesmanNumberService.isExistSalesmanNumber(salesmanNumber.getPlatformId(), salesmanNumber.getOnlineid())) {
            return RespResult.error("号主ID已存在");
        }
        // 获取业务员信息数据
        Salesman salesmanOne = iSalesmanService.getById(uid);
        if(StrUtil.isNull(salesmanOne)) {
            return RespResult.error("新增失败");
        }

        ConfigPlatform platform = iConfigPlatformService.findPlatformOneById(salesmanNumber.getPlatformId());
        if(StrUtil.isNull(platform)) {
            return RespResult.error("新增失败");
        }

        try {
            iSalesmanNumberService.addSalesmanNumber(salesmanNumber);
            if (StrUtil.isNotNull(snrv.getResource())) {
                List<SalesmanImages> list = JSONArray.parseArray(snrv.getResource(), SalesmanImages.class);
                for (SalesmanImages salesmanImages : list) {
                    if (StrUtil.isNull(salesmanImages.getLink())) {
                        continue;
                    }
                    salesmanImages.setSrcId(salesmanNumber.getId());
                    salesmanImages.setSrcType(SrcType.SRC_TYPE_SN);
                    // 保存资源图片
                    iSalesmanImagesService.saveSalesmanImages(salesmanImages);
                }
            }
        } catch (Exception e) {
            logger.error("号主新增失败，失败原因为：{}",e.getMessage());
            return RespResult.error("新增失败");
        }
        return RespResult.success("成功");
    }



    @AppAuth
    @GetMapping(value = "salesmanNumberSearch")
    @ApiOperation(value = "搜索号主", notes = "搜索号主接口")
    public RespResult<PageData<SalesmanNumberListVo>> salesmanNumberSearch(HttpServletRequest request,
                                                                           @ApiParam(required = true, value = "平台账户 ") @RequestParam String onlineId,
                                                                           @ApiParam(value = "所属平台") @RequestParam Long platformId,
                                                                           @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        HttpReqParam reqParam = new HttpReqParam();
        IPage<SalesmanNumberListVo> dataList = iSalesmanNumberService.
                searchSalesmanNumberByPlatformIdOnlineId(PageRequestUtil.buildPageRequest(reqParam), onlineId, platformId, uid);
        return RespResult.buildPageData(dataList);
    }

    @AppAuth
    @PostMapping(value = "forbidden")
    @ApiOperation(value = "禁用号主", notes = "禁用号主接口")
    public RespResult setUsed(@ApiParam(required = true, value = "号主id") @RequestParam Long id){
        try {
            iSalesmanNumberService.updateSalesmanNumberStatus(id,Status.STATUS_PROHIBIT);
        }catch (Exception e){
            return RespResult.error("禁用失败");
        }
        return RespResult.success("禁用成功");
    }

    @AppAuth
    @PostMapping(value = "check")
    @ApiOperation(value = "风险检测", notes = "风险检测接口")
    public RespResult checkSalesmanNumber(@ApiParam(required = true, value = "号主号码") @RequestParam String mobile,
                                          @ApiParam(required = true, value = "号主名字") @RequestParam String name) {
        try {
            Map<String, Object> map = iSalesmanNumberTmpService.checkSalesmanNumberTmpInfo(mobile, name);
            return RespResult.success(map);
        } catch (Exception e) {
            return RespResult.error(e.getMessage());
        }
    }


    @AppAuth
    @GetMapping(value = "load/dtl")
    @ApiOperation(value = "号主详情", notes = "号主详情接口")
    public RespResult loadSalesmanNumber(HttpServletRequest request, @ApiParam(required = true, value = "号主id") @RequestParam Long id) {
        Map<String, Object> map = new HashMap<>();
        SalesmanNumberRespVo salesmanNumber = iSalesmanNumberService.findSalesmanNumberById(id);
        List<TaskGoodsVo> goodAndTime = iTaskSonService.findGoodAndTime(id);
        List<SalesmanImagesVo> imagesExtList = iSalesmanImagesService.findSalesmanImagesBySrcIdAndSrcType(id, SrcType.SRC_TYPE_SN);
        map.put("sale", salesmanNumber);
        map.put("src", imagesExtList);
        map.put("task", goodAndTime);
        return RespResult.success(map);
    }

    @AppAuth
    @PostMapping(value = "replace/onlineid")
    @ApiOperation(value = "更换号主", notes = "更换号主的接口")
    public RespResult replaceSalesmanNumber(HttpServletRequest request,@ApiParam(required=true,value = "子任务id") Long taskSonId,
                                            @ApiParam(required=true,value = "平台id") Long platformId,
                                            @ApiParam(required=true,value = "平台号主账号") String onlineid,
                                            @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {

        if(null == taskSonId || null == platformId || null == onlineid) {
            logger.error("更换号主失败，失败原因是：缺少参数！");
            return RespResult.error("更换失败！");
        }

        if(null == uid || uid.equals(Number.LONG_ZERO)) {
            logger.error("更换号主失败，失败原因是：获取到当前登录人id为空，怀疑是非法请求！");
            return RespResult.error("更换失败！");
        }

        // 获取当前登录人信息
        Salesman sm = iSalesmanService.getById(uid);
        if(null == sm) {
            logger.error("更换号主失败，失败原因是：获取到当前登录人数据信息为空！");
            return RespResult.error("更换失败！");
        }

        // ~获取子任务
        TaskSon ts = iTaskSonService.findTaskSonOne(taskSonId);
        if(null == ts) {
            logger.error("更换号主失败，失败原因是：所属任务的：{} 的数不存在!",taskSonId);
            return RespResult.error("更换失败！");
        }

        // 判断任务是否是待提交状态
        if(null != ts.getStatus() && !ts.getStatus().equalsIgnoreCase(Status.STATUS_NOT_AUDITED)) {
            logger.error("更换号主失败，失败原因是：所属任务的：{} 的状态不是待提交状态，当前任务的状态为：{}",taskSonId,ts.getStatus());
            return RespResult.error("更换失败！");
        }

        // 获取当前要更换的号主数据
        SalesmanNumber sn = iSalesmanNumberService.findSalesmanNumber(uid, platformId, onlineid);
        if(null == sn) {
            return RespResult.error("更换失败，是否是跨平台更换！");
        }

        // if 判断是否更换当前的同号主任务数据
        if(sn.getId().equals(ts.getSalesmanNumberId())) {
            return RespResult.error("更换失败，任务已在该号存在！");
        }

        // 判断是否是禁用状态
        if(!sn.getStatus().equalsIgnoreCase(Status.STATUS_ENABLE)) {
            return RespResult.error("更换失败，当前该号已禁用！");
        }

        // 获取要更换的号主是否接过任务
        List<TaskSon> tss = iTaskSonService.findTaskSonRemoveTheDay(uid, sn.getId(), true);
        if(null!= tss && tss.size() > 0) {
            return RespResult.error("更换失败，该号已接过任务！");
        }

        // 获取任务分配配置
        TaskSetting setting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(sm.getOrgId(), sm.getSalesmanLevelId());
        if(null == setting) {
            logger.error("更换号主失败，失败原因是：分配任务配置为空，所属机构：{}，所属等级：{}",sm.getOrgId(),sm.getSalesmanLevelId());
            return RespResult.error("更换失败！");
        }

        // 获取该号主的任务
        List<TaskSon> tss1 = iTaskSonService.findTaskSonRemoveTheDay(uid, sn.getId(), false);
        if(null != tss1 && tss1.size() >= setting.getTaskTotal()) {
            logger.error("更换号主失败，失败原因是：[{}],任务已上限!",onlineid);
            return RespResult.error("更换失败，任务已上限！");
        }

        // 判断要更换的号主是否已经领取，同店铺，同商品的任务
        TaskSon ts1 = iTaskSonService.findTaskSon(sm.getId(), sn.getId(), ts.getSellerShopId(), ts.getGoodsId());
        if(null != ts1) {
            logger.error("更换号主失败，失败原因是：当前要更换的号主，相同的任务数据!");
            return RespResult.error("更换失败，该号已领取过相同的任务！");
        }

        // 判断要更换的号主是否已经领取了，同店铺编码的任务
        if(iTaskSonService.verifyIsSameTaskSon(taskSonId, platformId, sn.getSalesmanId(), sn.getId())) {
            logger.error("更换号主失败，失败原因是：当前要更换的号主，已经领取了同编码的任务！");
            return RespResult.error("更换失败，该号已领取过同编码的任务！");
        }
        try {
            iTaskSonService.taskSonReplaceSalesmanNumber(ts.getId(), sm.getId(), ts.getSalesmanNumberId(),sn.getId(),sn.getImei());
        } catch (Exception e) {
            logger.error("更换号主失败，失败原因是：{}",e.getMessage());
            return RespResult.error("更换失败！");
        }
        return RespResult.success("更换成功");
    }
}
