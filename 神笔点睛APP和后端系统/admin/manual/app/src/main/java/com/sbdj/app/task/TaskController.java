package com.sbdj.app.task;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.base.TaskRedis;
import com.sbdj.core.constant.Chars;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.PlatformType;
import com.sbdj.core.constant.Version;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.entity.ConfigPlatform;
import com.sbdj.service.configure.service.IConfigPlatformService;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.member.service.ISalesmanNumberService;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.system.entity.SysConfigDtl;
import com.sbdj.service.system.service.ISysConfigDtlService;
import com.sbdj.service.system.type.SysConfigType;
import com.sbdj.service.task.admin.vo.TaskSonVo;
import com.sbdj.service.task.app.vo.TaskExtVo;
import com.sbdj.service.task.app.vo.TaskTypeVo;
import com.sbdj.service.task.entity.TaskSetting;
import com.sbdj.service.task.service.ITaskService;
import com.sbdj.service.task.service.ITaskSettingService;
import com.sbdj.service.task.service.ITaskSonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * <p>
 * 任务控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "任务控制器")
@RequestMapping(value = Version.V1 + "/task")
@RestController
public class TaskController {

    private Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private ITaskService iTaskService;
    @Autowired
    private ITaskSonService iTaskSonService;
    @Autowired
    private ISysConfigDtlService iConfigDtlService;
    @Autowired
    private ISalesmanNumberService iSalesmanNumberService;
    @Autowired
    private ISalesmanService iSalesmanService;
    @Autowired
    private ITaskSettingService iTaskSettingService;
    @Autowired
    private IConfigPlatformService iPlatformService;


    @AppAuth
    @GetMapping(value = "IllegalTask")
    @ApiOperation(value = "违规任务", notes = "违规任务接口")
    public RespResult<PageData<TaskSonVo>> IllegalTask(HttpServletRequest request,
                                                       @RequestParam(value = "页码", defaultValue = "1") int Page,
                                                       @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        HttpReqParam reqParam = new HttpReqParam();
        reqParam.setPage(Page);
        reqParam.setLimit(35);
        IPage<TaskSonVo> taskSonPageList = iTaskSonService.findTaskSonPageList(PageRequestUtil.buildPageRequest(reqParam), uid);
        return RespResult.buildPageData(taskSonPageList);
    }


    @AppAuth
    @GetMapping(value = "type/data")
    @ApiOperation(value = "任务类型数据", notes = "任务类型数据接口")
    public RespResult findTaskTypeDataList() {
        List<SysConfigDtl> list = iConfigDtlService.findConfigDtl(SysConfigType.TASK_TYPE);
        return RespResult.success(list);
    }


    @AppAuth
    @GetMapping(value = "total")
    @ApiOperation(value = "获取所属任务类型对应可领取的任务数量", notes = "获取所属任务类型对应可领取的任务数量接口")
    public RespResult<List<TaskTypeVo>> findTaskTypeTotal(HttpServletRequest request,
                                                          @ApiParam(required = true, value = "号主id") @RequestParam Long salNumberId,
                                                          @ApiParam(required = true, value = "所属平台") @RequestParam Long platformId,
                                                          @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {

        // 获取业务员数据！
        Salesman salesman = iSalesmanService.getById(uid);
        if (null == salesman) {
            logger.error("===>导致失败原因为：业务员信息为空！");
            return RespResult.error("任务无法分配！");
        }

        // ~获取任务配置数据
        TaskSetting setting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(salesman.getOrgId(), salesman.getSalesmanLevelId());
        if (null == setting) {
            logger.error("===>导致失败原因为：任务配置信息为空！");
            return RespResult.error("任务无法分配！");
        }
        //获取可领取的任务条数
        List<TaskExtVo> extApis = iTaskService.findTaskTypeTaskTotalBySalNumberIdAdnDays(salesman.getOrgId(), salNumberId, platformId, setting.getSameShopSmallDays());
        // 获取任务类型数据
        List<SysConfigDtl> cfds = iConfigDtlService.findConfigDtl(SysConfigType.TASK_TYPE);
        List<TaskTypeVo> ttvs = new ArrayList<>();
        TaskTypeVo ttv;

        // 遍历数据
        for (SysConfigDtl cfd : cfds) {
            ttv = new TaskTypeVo();
            ttv.setTaskTotal(0L);
            ttv.setTaskTypeId(cfd.getId());
            ttv.setTaskTypeName(cfd.getName());
            ttvs.add(ttv);
        }

        if (StrUtil.isNotNull(extApis)) {
            for (TaskTypeVo ttl : ttvs) {
                for (TaskExtVo tev : extApis) {
                    if (ttl.getTaskTypeId().equals(tev.getTaskTypeId())) {
                        ttl.setTaskTotal(tev.getTaskTotal());
                    }
                }
            }
        }

        return RespResult.success(ttvs);
    }

    @AppAuth
    @PostMapping(value = "allot")
    @ApiOperation(value = "任务分配操作", notes = "任务分配操作接口")
    public RespResult<PageData<TaskExtVo>> taskAllot(
            @ApiParam(required = true, value = "业务员ID") @RequestParam Long salId,
            @ApiParam(required = true, value = "号主ID") @RequestParam Long salNumberId,
            @ApiParam(required = true, value = "任务类型ID") @RequestParam Long taskTypeId,
            @ApiParam(required = true, value = "所属平台ID") @RequestParam Long platformId) {
        HttpReqParam param = new HttpReqParam();
        param.setPage(1);
        param.setLimit(10);

        // 获取小于当前时间所有未提交的任务数据!
        int count = iTaskSonService.findNotSubmitTaskSonBySalesmanId(salId);
        if (count > 0) {
            logger.error("===>导致任务分配失败原因为：业务员还有任务没有结束！");
            return RespResult.error("您还有 " + count + " 个任务未提交！");
        }

        // 获取这个业务员还未审核完的任务数据
        count = iTaskSonService.searchTaskSonNotReviewTotalBySalesmanId(salId);
        if (count > 0) {
            logger.error("===>导致任务分配失败原因为：业务员还有任务没有审核！");
            return RespResult.error("您还有 " + count + " 个任务未审核！");
        }

        // ~获取业务员数据！
        Salesman salesman = iSalesmanService.getById(salId);
        if (null == salesman) {
            logger.error("===>导致任务分配失败原因为：业务员数据为空！");
            return RespResult.error("任务无法分配！");
        }

		/*
		// ~判断当前业务员的信用额度是否大于0
		if(salesman.getSurplusCredit().compareTo(new BigDecimal(0)) == -1) {
			logger.error("===>导致任务分配失败原因为：业务信用额度不够！");
			return RespResult.error("信用额度不够！");
		}
		*/

        // ~先获取号主对应的数据！
        SalesmanNumber number = iSalesmanNumberService.findSalesmanNumberBySalIdAndSalNumberId(salId, salNumberId);
        if (null == number) {
            return RespResult.error("任务无法分配");
        }

        // 获取业务员等级配置数据
        TaskSetting setting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(salesman.getOrgId(), salesman.getSalesmanLevelId());
        if (StrUtil.isNull(setting)) {
            logger.error("===>导致任务分配失败原因为：业务员等级配置数据为空！");
            return RespResult.error("任务无法分配！");
        }

        // 一个业务员同一天只能分配号主的个数
        int allotCou = iTaskSonService.findSalAllotNumberCount(salesman.getId(), salNumberId, number.getPlatformId());
        if (setting.getAllotTotal() <= allotCou) {
            logger.error("===>导致任务分配失败原因为：分配号主已上限，目前该业务员【{}】,只能分配 【{}】个号！", salesman.getName(), number.getOnlineid());
            return RespResult.error("分配号主已上限！");
        }

        // 拼多多逻辑
        boolean pddFlag = true;
        // 获取所属平台数据
        ConfigPlatform platform = iPlatformService.findPlatformOneById(platformId);
        if (StrUtil.isNull(platform)) {
            logger.error("===>导致任务分配失败原因为：获取所属平台数据为空！");
            return RespResult.error("任务无法分配！");
        } else {
            if ((PlatformType.P_TYPE_PDD).equalsIgnoreCase(platform.getCode())) {
                pddFlag = false;
            }
        }

        // 号主同1天能接任务数
        int numberTaskCount = iTaskSonService.findDayTaskCountBySalNumberId(salNumberId, platformId);
        if (setting.getTaskTotal() <= numberTaskCount && pddFlag) {
            logger.error("===>导致任务分配失败原因为：【{}】分配任务已上限，限制的是号主同1天只能接【{}】个任务！", number.getOnlineid(), setting.getTaskTotal());
            return RespResult.error("分配任务已上限！");
        }

        // 号主近15天任务数不能超过
        //int  Day15TaskCou = iTaskSonService.find15DayNumberTaskCount(salNumberId,platformId);
        // 号主近7天任务数不能通过
        //int day = 7;
        int day7TaskCount = iTaskSonService.findNumberTaskCountByDay(salNumberId, platformId, setting.getSameBigDays());
        if (setting.getNotBigDays() <= day7TaskCount && pddFlag) {
            logger.error("===>导致任务分配失败原因为：【{}】分配任务已上限，限制的是号主近{}天只能接【{}】个任务！", setting.getSameBigDays(), number.getOnlineid(), setting.getTaskTotal());
            return RespResult.error("分配任务已上限！");
        }

        // 拼多多逻辑
        // 一号一单 间隔n天方能复购
        if (!pddFlag) {
            if (numberTaskCount >= Number.INT_ONE) {
                logger.error("===>导致任务分配失败原因为：【{}】分配拼多多任务已上限，限制的是号主同1天只能接【{}】个任务！", number.getOnlineid(), setting.getTaskTotal());
                return RespResult.error("分配任务已上限！");
            }
            int pddTaskCount = iTaskSonService.findNumberTaskCountByDay(salNumberId, platformId, setting.getPddIntervalDay());
            if (pddTaskCount >= Number.INT_ONE) {
                logger.error("===>导致任务分配失败原因为：【{}】分配拼多多任务已上限，限制的是号主近{}天只能接【{}】个任务！", setting.getPddIntervalDay(), number.getOnlineid(), setting.getTaskTotal());
                return RespResult.error("分配任务已上限！");
            }
        }

        // 同店铺小于多少天不可接任务
        IPage<TaskExtVo> list = iTaskService.finaTaskPageList(salesman.getOrgId(), taskTypeId, salNumberId, platformId, setting.getSameShopSmallDays(), PageRequestUtil.buildPageRequest(param));
        // 自动派单逻辑
        if (setting.getAutoGetOrderFlag() > 0) {
            List<TaskExtVo> data = list.getRecords();
            // 自动派单
            // 会员可接的任务数
            int total = setting.getTaskTotal();
            int[] topKey = new int[total];
            for (int i = 0; i < total; i++) {
                topKey[i] = -1;
            }
            int topIndex = 0;
            // 排除已经置顶的
            for (TaskExtVo tev : data) {
                if (tev.getTop() == 1) {
                    total = total > 0 ? total - 1 : total;
                    topKey[topIndex] = tev.getShopCode();
                    topIndex++;
                }
            }
            int max;
            int[] keys = new int[total];
            int tmp;
            boolean activeFlag;
            // 会员接的任务数
            for (int i = 0; i < total; i++) {
                keys[i] = -1;
                tmp = -1;
                max = 0;
                // 排除已有最大值
                // 迭代最大值
                for (int j = 0; j < data.size(); j++) {
                    // 排除已置顶的
                    if (data.get(j).getTop() == 1) {
                        continue;
                    }
                    activeFlag = false;
                    // 排除已经置顶的任务
                    for (int tk : topKey) {
                        if (tk != -1 && data.get(j).getShopCode() == tk) {
                            activeFlag = true;
                            break;
                        }
                    }
                    if (activeFlag) {
                        continue;
                    }
                    // 排除已有最大值
                    for (int m = 0; m < i + 1; m++) {
                        if (keys[m] != -1 && data.get(j).getShopCode() == data.get(keys[m]).getShopCode()) {
                            tmp = j;
                            break;
                        }
                    }
                    // 在同店铺中选择最大值
                    if (keys[i] != -1 && data.get(keys[i]).getShopCode() == data.get(j).getShopCode()) {
                        // 获取最大值
                        if (max < data.get(j).getTaskResidueTotal()) {
                            max = data.get(j).getTaskResidueTotal();
                            keys[i] = j;
                        }
                        continue;
                    }
                    // 排除同店铺编码
                    if (tmp != -1 && data.get(tmp).getShopCode() == data.get(j).getShopCode()) {
                        continue;
                    }
                    // 获取最大值
                    if (max < data.get(j).getTaskResidueTotal()) {
                        max = data.get(j).getTaskResidueTotal();
                        keys[i] = j;
                    }
                }

                // 获取分组中的最大值
                if (i == (total - 1)) {
                    Set<Integer> shopCode = new HashSet<>();
                    data.forEach(f -> shopCode.add(f.getShopCode()));
                    Map<Integer, List<TaskExtVo>> map = new HashMap<>();
                    // 统计店铺分组数据
                    for (Integer sc : shopCode) {
                        List<TaskExtVo> tevList;
                        for (TaskExtVo tev : data) {
                            if (tev.getShopCode() == sc) {
                                tevList = map.get(sc);
                                if (StrUtil.isNotNull(tevList)) {
                                    tevList.add(tev);
                                    map.put(sc, tevList);
                                } else {
                                    tevList = new ArrayList<>();
                                    tevList.add(tev);
                                    map.put(sc, tevList);
                                }
                            }
                        }
                    }
                    // 计算分组之间的和
                    int sum;
                    int mgroup = 0;
                    // 获取最大的分组
                    for (Map.Entry<Integer, List<TaskExtVo>> entry : map.entrySet()) {
                        sum = 0;
                        if (entry.getValue().size() > 1) {
                            for (TaskExtVo taskExtVo : entry.getValue()) {
                                sum += taskExtVo.getTaskResidueTotal();
                            }
                            if (max <= sum) {
                                max = sum;
                                mgroup = entry.getKey();
                            }
                        }

                    }
                    // 获取最大的分组中最大的店铺
                    boolean flag = true;
                    max = 0;
                    List<TaskExtVo> tevList = map.get(mgroup);
                    if (tevList != null) {
                        // 非分组店铺排除
                        for (int ks : keys) {
                            if (ks != -1 && data.get(ks).getShopCode() == mgroup) {
                                flag = false;
                                break;
                            }
                        }
                        // 排除已经置顶的任务
                        for (int tk : topKey) {
                            if (tk != -1 && mgroup == tk) {
                                flag = false;
                                break;
                            }
                        }
                        // 自动跳过
                        if (flag) {
                            for (TaskExtVo tev : tevList) {
                                // 获取最大任务数的店铺
                                if (max < tev.getTaskResidueTotal()) {
                                    max = tev.getTaskResidueTotal();
                                    keys[i] = data.indexOf(tev);
                                }
                            }
                        }
                    }
                }
                // 进行标记
                if (keys[i] != -1) {
                    data.get(keys[i]).setTop(1);
                }
            }
        }

        return RespResult.buildPageData(list);
    }


    @AppAuth
    @PostMapping(value = "receive")
    @ApiOperation(value = "任务分配", notes = "任务分配接口")
    public RespResult taskReceive02(HttpServletRequest request,
                                    @ApiParam(required = true, value = "任务ids，多个id使用逗号隔开（1,2....n）") @RequestParam String taskIds,
                                    @ApiParam(required = true, value = "号主id") @RequestParam Long salNumberId,
                                    @ApiParam(required = true, value = "所属平台") @RequestParam Long platformId,
                                    @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {

        // ~获取当前业务员数据
        Salesman salesman = iSalesmanService.getById(uid);
        if (null == salesman) {
            return RespResult.error("无法分配！");
        }

        // ~获取业务员等级配置数据
        TaskSetting setting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(salesman.getOrgId(), salesman.getSalesmanLevelId());
        if (null == setting) {
            return RespResult.error("无法分配！");
        }

        // 拼多多逻辑
        boolean pddFlag = false;
        ConfigPlatform platform = iPlatformService.findPlatformOneById(platformId);
        if (StrUtil.isNotNull(platform)) {
            if ((PlatformType.P_TYPE_PDD).equalsIgnoreCase(platform.getCode())) {
                pddFlag = true;
            }
        }

        // ~号主同一天能接任务数
        int numberTaskCount = iTaskSonService.findDayTaskCountBySalNumberId(salNumberId, platformId);
        if (setting.getTaskTotal() <= numberTaskCount && pddFlag) {
            return RespResult.error("任务已上限！");
        }

        int dayTaskCount = iTaskSonService.findNumberTaskCountByDay(salNumberId, platformId, setting.getSameBigDays());
        if (setting.getNotBigDays() <= dayTaskCount && pddFlag) {
            return RespResult.error("分配任务已上限！");
        }

        // 拼多多逻辑
        // 一号一单 间隔n天方能复购
        if (!pddFlag) {
            if (numberTaskCount >= Number.INT_ONE) {
                return RespResult.error("分配任务已上限！");
            }
            int pddTaskCount = iTaskSonService.findNumberTaskCountByDay(salNumberId, platformId, setting.getPddIntervalDay());
            if (pddTaskCount >= Number.INT_ONE) {
                return RespResult.error("分配任务已上限！");
            }
        }


        // 任务分配 特殊情况
        if (StrUtil.indexOf(taskIds, Chars.CHAR_COMMA)) {
            int length;
            if (pddFlag) {
                length = Number.INT_ONE;
            } else {
                length = setting.getTaskTotal() - dayTaskCount;
            }
            String[] strArray = taskIds.split(Chars.CHAR_COMMA);
            // 业务员当时操作的号主接的任务数是否超过能接的任务数
            if (strArray.length > length) {
                // 抛弃多余的任务数
                StringBuilder tmp = new StringBuilder();
                for (int i = 0; i < length; i++) {
                    tmp.append(strArray[i]).append(Chars.CHAR_COMMA);
                }
                tmp.deleteCharAt(tmp.lastIndexOf(Chars.CHAR_COMMA));
                taskIds = tmp.toString();
            }
        }

		/*// ~判断当前业务员的信用额度是否大于0
		if(salesman.getSurplusCredit().compareTo(new BigDecimal(0)) == 0) {
			return RespResult.error("任务无法分配，信用额度已上限！");
		}*/

        Long orgId = salesman.getOrgId();
        // 记录没有可领的任务个数!
        int count = 0;
        // 记录可以进行领取的任务id
        StringBuilder sb = new StringBuilder();
        if (StrUtil.indexOf(taskIds, Chars.CHAR_COMMA)) {
            String[] ids = taskIds.split(Chars.CHAR_COMMA);
            // 遍历
            for (String id : ids) {
                if (TaskRedis.decrease(id) < 0) {
                    logger.error("===>当前任务剩余数量为零！当前任务ID为：{}", id);
                    TaskRedis.remove(id);
                    continue;
                }
                sb.append(id).append(Chars.CHAR_COMMA);
                count++;
            }
        } else {
            if (TaskRedis.decrease(taskIds) < 0) {
                logger.error("===>当前任务剩余数量为零！当前任务ID为：{}", taskIds);
                TaskRedis.remove(taskIds);
                return RespResult.error("亲您手速太慢了！");
            }
            sb.append(taskIds).append(Chars.CHAR_COMMA);
            count++;
        }
        try {
            // 过滤掉没有任务可领数量的任务id
            if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
                iTaskService.taskReceiveNew(sb.toString(), salesman.getId(), salNumberId, orgId);
            }
            // 判断成功领取任务数量
            if ((count) > 0) {
                logger.info("请求IP：【{}】 uid：【{}】 领取数量：【{}】", request.getRemoteHost(), uid, count);
                return RespResult.success("成功领取 " + count + " 个任务！");
            } else {
                return RespResult.error("亲您手速太慢了！");
            }
        } catch (BaseException e) {
            return RespResult.error(e.code(),e.getMessage());
        } catch (Exception e) {
            return RespResult.error("失败");
        }
    }
}
