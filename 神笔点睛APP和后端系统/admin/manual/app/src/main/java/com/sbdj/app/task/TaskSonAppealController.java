package com.sbdj.app.task;

import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.constant.Version;
import com.sbdj.service.task.entity.TaskSonAppeal;
import com.sbdj.service.task.service.ITaskSonAppealService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 子任务申诉控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "子任务申诉控制器")
@RequestMapping(value = Version.V1 + "/task/son/appeal")
@RestController
public class TaskSonAppealController {

    private static Logger logger = LoggerFactory.getLogger(TaskSonAppealController.class);
    @Autowired
    private ITaskSonAppealService iTaskSonAppealService;

    @AppAuth
    @PostMapping("save")
    @ApiOperation(value = "保存申诉", notes = "保存申诉接口")
    public RespResult saveTaskSonAppeal(
            @ApiParam(required = true, value = "任务详情id") @RequestParam Long taskSonId,
            @ApiParam(required = true, value = "申诉原因") @RequestParam String reasons,
            @ApiParam(required = true, value = "申诉图片，多个使用逗号隔开") @RequestParam String images) {
        try {
            iTaskSonAppealService.saveTaskSonAppeal(taskSonId, reasons, images);
        } catch (Exception e) {
            return RespResult.error("提交失败！");
        }
        return RespResult.success("提交成功！");
    }

    @AppAuth
    @RequestMapping("update")
    public RespResult updateTaskSonAppeal(TaskSonAppeal taskSonAppeal) {
        try {
            iTaskSonAppealService.updateTaskSonAppeal(taskSonAppeal);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }

    @AppAuth
    @RequestMapping("delete")
    public RespResult deleteTaskSonAppeal(Long id) {
        try {
            iTaskSonAppealService.logicDeleteTaskSonAppealById(id, null);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }
}
