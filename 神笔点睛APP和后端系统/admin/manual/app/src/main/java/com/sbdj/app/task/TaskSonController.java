package com.sbdj.app.task;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.Status;
import com.sbdj.core.constant.TaskTypeCode;
import com.sbdj.core.constant.Version;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.DateUtil;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.type.PayGoodsType;
import com.sbdj.service.seller.service.ISellerShopGoodsService;
import com.sbdj.service.system.entity.SysConfigDtl;
import com.sbdj.service.system.service.ISysConfigDtlService;
import com.sbdj.service.task.admin.vo.TaskSonVo;
import com.sbdj.service.task.app.vo.TaskImgEchoVo;
import com.sbdj.service.task.app.vo.TaskPictureTypeSettingVo;
import com.sbdj.service.task.app.vo.TaskSonDetailVo;
import com.sbdj.service.task.app.vo.TaskSonVo2;
import com.sbdj.service.task.entity.TaskSetting;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.service.ITaskPictureTypeSettingService;
import com.sbdj.service.task.service.ITaskSettingService;
import com.sbdj.service.task.service.ITaskSonService;
import com.sbdj.service.task.type.IsCardType;
import com.sbdj.service.task.type.IsMarkType;
import com.sbdj.service.task.type.TaskPictureType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 子任务控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "子任务控制器")
@RequestMapping(value = Version.V1+ "/task/son")
@RestController
public class TaskSonController {
    private Logger logger = LoggerFactory.getLogger(TaskSonController.class);

    @Autowired
    private ITaskSonService iTaskSonService;
    @Autowired
    private ISellerShopGoodsService iSellerShopGoodsService;
    @Autowired
    private ISysConfigDtlService iConfigDtlService;
    @Autowired
    private ITaskPictureTypeSettingService iTaskPictureTypeSettingService;
    @Autowired
    private ITaskSettingService iTaskSettingService;


    @AppAuth
    @GetMapping(value = "list")
    @ApiOperation(value = "任务列表", notes = "任务列表接口")
    public RespResult<PageData<TaskSonVo2>> findTaskSonPageList(HttpServletRequest request,
                                                               @ApiParam(required = true, value = "分页页码") @RequestParam int page,
                                                               @ApiParam(required = true, value = "状态{开始：A， 待提交：F，已审核：T，已传图：AU，被拒绝：AN，已失败：AF }") @RequestParam String status,
                                                               @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        HttpReqParam param = new HttpReqParam();
        param.setPage(page);
        IPage<TaskSonVo2> taskSonVoList2 = iTaskSonService.findTaskSonVoList(uid, status, PageRequestUtil.buildPageRequest(param));
        return RespResult.buildPageData(taskSonVoList2);
    }


    @AppAuth
    @PostMapping(value = "start/opr")
    @ApiOperation(value = "子任务点开始操作", notes = "子任务点开始操作的接口")
    public RespResult taskStartOpr(HttpServletRequest request,
                                   @ApiParam(required = true, value = "子任务id，可以传递多个id，使用逗号进行隔开（1,2,3.....n）") @RequestParam String ids,
                                   @ApiParam(required = true, value = "操作类型，SQ：申请货款，DF：垫付货款") @RequestParam String oprType,
                                   @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        // 判断是否为空
        if (StrUtil.isNull(ids) || StrUtil.isNull(oprType) || StrUtil.isNull(uid)) {
            return RespResult.error();
        }

        try {
            iTaskSonService.batchGenerateTaskSonPayGoods(uid, ids, oprType);
            logger.info("请求IP：【{}】 uid：【{}】 子任务id：【{}】", request.getRemoteHost(), uid, ids);
        } catch (BaseException e) {
            return RespResult.error(e.getMessage());
        }
        return RespResult.success(oprType.equalsIgnoreCase("DF") ? "垫付成功" : "申请成功");
    }

    @AppAuth
    @PostMapping(value = "/img/upload")
    @ApiOperation(value = "任务批量传图", notes = "任务批量传图接口接口")
    public RespResult taskImgUpload(HttpServletRequest request, String data) {
        // 保存重复提交的次数!
        int repeatSubCou = 0;
        // 保存重复的订单号次数
        int repeatCou = 0;
        // 保存订单号为空的次数
        int spaceCou = 0;
        // 保存成功的id
        StringBuilder sucessId = new StringBuilder();

        TaskSetting setting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(2L, 1L);

        // if 判断数据为空
        if (StrUtil.isNull(data)) {
            return RespResult.error("数据为空！");
        }

        TaskSonVo tsx = null;
        List<TaskSonVo> taskList = JSONArray.parseArray(data, TaskSonVo.class);
        for (TaskSonVo ts : taskList) {
            // 判断订单号不为空，并且订单号在该平台存在
            if (StrUtil.isNotNull(ts.getOrderNum()) && iTaskSonService.isVerifyTaskSonByOrderNum(ts.getOrderNum())) {
                repeatCou++;
                logger.error("===>订单号相同：{}", ts.getOrderNum());
                continue;
            }

            // 获取要提交图片的任务信息
            TaskSon taskSon = iTaskSonService.findTaskSonOne(ts.getId());
            if (StrUtil.isNull(taskSon)) {
                logger.error("===>该任务不存在：{}", ts.getId());
                continue;
            }

            if (setting.getUploadOrderNumFlag() > 0) {
                taskSon.setOrderNum(ts.getOrderNum());
            }
            // 现付单，第一次提交，第二次提交!（待提交|不通过）状态!
            // 第一次是：（F-AU），第二次是：（AN-AU）
            if (ts.getTaskTypeCode().equalsIgnoreCase(TaskTypeCode.TASK_TYPE_XFD) && (ts.getStatus().equalsIgnoreCase(Status.STATUS_NOT_AUDITED) || ts.getStatus().equalsIgnoreCase(Status.STATUS_NOT_PASS))) {
                if (setting.getUploadOrderNumFlag() > 0) {
                    if (StrUtil.isNull(ts.getOrderNum())) {
                        spaceCou++;
                        logger.error("===>订单号为空:{}", ts.getId());
                        continue;
                    }
                }
                // 防止重复多次提交任务图!
                if (taskSon.getStatus().equalsIgnoreCase(Status.STATUS_ALREADY_UPLOAD)) {
                    repeatSubCou++;
                    continue;
                }
                taskSon.setStatus(Status.STATUS_ALREADY_UPLOAD);
                // 隔日单，第一次提交，
                // 第一次传图：（F：A），第二次传图：（F：AU）
            } else if (ts.getTaskTypeCode().equalsIgnoreCase(TaskTypeCode.TASK_TYPE_GRD) && ts.getStatus().equalsIgnoreCase(Status.STATUS_NOT_AUDITED)) {
                // 判断images字段不等于空，并且等于GRD
                if (StrUtil.isNotNull(taskSon.getImages()) && taskSon.getImages().equalsIgnoreCase(TaskTypeCode.TASK_TYPE_GRD)) {
                    // 防止重复多次提交任务图!
                    if (taskSon.getStatus().equalsIgnoreCase(Status.STATUS_ACTIVITY)) {
                        repeatSubCou++;
                        continue;
                    }
                    taskSon.setStatus(Status.STATUS_ACTIVITY);
                    // 隔日单第一次传图，将过期时间更新为第二天的中午12点13分14秒
                    taskSon.setDeadlineTime(DateUtil.genDayDate(1, 12, 13, 14));
                    taskSon.setImages(ts.getImages());
                } else {
                    // 防止重复多次提交任务图!
                    if (taskSon.getStatus().equalsIgnoreCase(Status.STATUS_ALREADY_UPLOAD)) {
                        repeatSubCou++;
                        continue;
                    }
                    // 图片数据处理
                    ts.setImages(StrUtil.stringJson(ts.getImages(), taskSon.getImages()));
                    taskSon.setStatus(Status.STATUS_ALREADY_UPLOAD);
                }
                // 隔日单，审核不通过的状态下!
                // 在审核不通过情况中，重新上传图片!
                // 数据状态的变更:（AN-AU）
            } else if (ts.getTaskTypeCode().equalsIgnoreCase(TaskTypeCode.TASK_TYPE_GRD) && ts.getStatus().equalsIgnoreCase(Status.STATUS_NOT_PASS)) {
                // 防止重复多次提交任务图!
                if (taskSon.getStatus().equalsIgnoreCase(Status.STATUS_ALREADY_UPLOAD)) {
                    repeatSubCou++;
                    continue;
                }

                // 图片数据处理
                ts.setImages(StrUtil.stringJson(ts.getImages(), taskSon.getImages()));
                taskSon.setStatus(Status.STATUS_ALREADY_UPLOAD);
            }

            // 保存子任务数据
            // 判断是隔日单，隔日第一次传图是在F-A状态，并且不用保存在task_son_images
            if (ts.getTaskTypeCode().equalsIgnoreCase(TaskTypeCode.TASK_TYPE_GRD)) {
                // if 状态为 ：A，else 状态为 ：除A外
                if (taskSon.getStatus().equalsIgnoreCase(Status.STATUS_ACTIVITY)) {
                    iTaskSonService.updateById(taskSon);
                } else {
                    if (setting.getUploadOrderNumFlag() > 0) {
                        if (StrUtil.isNull(ts.getOrderNum())) {
                            spaceCou++;
                            logger.error("===>订单号为空:{}", ts.getId());
                            continue;
                        }
                    }
                    iTaskSonService.saveTaskSonAndTaskSonImages(taskSon, ts.getImages());
                }
            } else {
                iTaskSonService.saveTaskSonAndTaskSonImages(taskSon, ts.getImages());
            }
            sucessId.append(taskSon.getId()).append(",");
        }
        // 保存成功的任务id，并且响应到APP
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("sucess_id", sucessId);

        if (repeatSubCou > 0) {
            return RespResult.error(map, repeatSubCou + " 个任务不要重复提交！");
        }
        if (repeatCou > 0) {
            return RespResult.error(map, repeatCou + " 个任务存在相同的订单号！");
        }
        if (spaceCou > 0) {
            return RespResult.error(map, spaceCou + " 个任务订单号为空!");
        }
        logger.info("请求IP：【{}】 提交任务", request.getRemoteHost());
        return RespResult.success("提交成功");
    }

    @AppAuth
    @GetMapping(value = "taskImgEcho")
    @ApiOperation(value = "任务传图界面基本数据回显", notes = "任务传图界面基本回显接口")
    public RespResult<List<TaskImgEchoVo>> taskImgEcho(HttpServletRequest request,
                                                       @ApiParam(required = true, value = "子任务ids") @RequestParam String ids) {
        List<TaskImgEchoVo> dataList = iTaskSonService.findDataById(ids);
        for (TaskImgEchoVo tiev : dataList) {
            // 现付单
            if (tiev.getTaskTypeCode().equalsIgnoreCase(TaskPictureType.TYPE_XFD)) {
                tiev.setTptsv(iTaskPictureTypeSettingService.findTaskPictureTypeSettingVoByPictureType(TaskPictureType.TYPE_XFD, tiev.getOrgId()));
                // 隔日单第一次传图
            } else if (tiev.getTaskTypeCode().equalsIgnoreCase(TaskPictureType.TYPE_GRD) && tiev.getImages().equalsIgnoreCase(TaskPictureType.TYPE_GRD)) {
                tiev.setTptsv(iTaskPictureTypeSettingService.findTaskPictureTypeSettingVoByPictureType(TaskPictureType.TYPE_GRD, tiev.getOrgId()));
                tiev.setFlag(Boolean.FALSE);
                // 隔日单二次传图
            } else if (!tiev.getImages().equalsIgnoreCase(TaskPictureType.TYPE_GRD) && tiev.getTaskTypeCode().equalsIgnoreCase(TaskPictureType.TYPE_GRD)) {
                tiev.setTptsv(iTaskPictureTypeSettingService.findTaskPictureTypeSettingVoByPictureType(TaskPictureType.TYPE_XFD, tiev.getOrgId()));
                tiev.setFlag(Boolean.TRUE);
            }
        }
        return RespResult.success(dataList);
    }

    @AppAuth
    @GetMapping(value = "/not/pass/echo")
    @ApiOperation(value = "不通过的任务传图界面基本数据回显", notes = "不通过的任务传图界面基本回显接口")
    public RespResult<List<TaskImgEchoVo>> taskImgNotPassEcho(HttpServletRequest request,
                                                              @ApiParam(required = true, value = "子任务ids") @RequestParam String ids) {
        List<TaskImgEchoVo> dataList = iTaskSonService.findNotPassDataById(ids);
        for (TaskImgEchoVo tiev : dataList) {
            tiev.setFlag(true);
            if (tiev.getTaskTypeCode().equalsIgnoreCase(TaskPictureType.TYPE_XFD) && StrUtil.isNotNull(tiev.getImages())) {
                tiev.setTptsv(iTaskPictureTypeSettingService.findTaskPictureTypeSettingVoByIds(tiev.getImages()));
            } else if (tiev.getTaskTypeCode().equalsIgnoreCase(TaskPictureType.TYPE_GRD) && StrUtil.isNotNull(tiev.getImages())) {
                tiev.setTptsv(iTaskPictureTypeSettingService.findTaskPictureTypeSettingVoByIds(tiev.getImages()));
            } else {
                //tiev.setTptsv(iTaskPictureTypeSettingService.findTaskPictureTypeSettingVoByPictureType(TaskPictureType.TYPE_XFD));
            }
        }
        return RespResult.success(dataList);
    }


    @AppAuth
    @GetMapping(value = "detail")
    @ApiOperation(value = "子任务详情", notes = "子任务详情接口")
    public RespResult<TaskSonDetailVo> taskSonDetail(HttpServletRequest request,
                                                     @ApiParam(required = true, value = "子任务id") Long taskSonId,
                                                     @ApiParam(required = true, value = "所属平台id") Long platformId,
                                                     @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {

        TaskSonDetailVo tsdv = iTaskSonService.findTaskSonDetail(taskSonId, platformId);
        // 处理查文内容为空，不展示null，展示字符空值即可
        if (StrUtil.isNotNull(tsdv) && StrUtil.isNull(tsdv.getGoodsKeyword())) {
            tsdv.setGoodsKeyword("");
        }
        if (StrUtil.isNotNull(tsdv) && StrUtil.isNotNull(tsdv.getPaymentType()) && PayGoodsType.PAY_GOODS_TYPE_DF.equalsIgnoreCase(tsdv.getPaymentType())) {
            tsdv.setPaymentType("垫付");
        } else if (StrUtil.isNotNull(tsdv) && StrUtil.isNotNull(tsdv.getPaymentType()) && PayGoodsType.PAY_GOODS_TYPE_SQ.equalsIgnoreCase(tsdv.getPaymentType())) {
            tsdv.setPaymentType("申请");
        }

        // 判断是要进行异步打标
        if (StrUtil.isNotNull(tsdv) && tsdv.getIsCard().equals(IsCardType.CARD_AUTO) && IsMarkType.MARK_TYPE_NO.equals(tsdv.getIsMark())) {
            // 异步处理
            try {
                iSellerShopGoodsService.asynMarkShopGoods(taskSonId, tsdv.getIsCard(), tsdv.getIsMark());
            } catch (Exception e) {
                logger.error("打标失败【{}】", e.getMessage());
                tsdv.setIsCard(IsCardType.CARD_YES);
            }
        }
        return RespResult.success(tsdv);
    }


    @AppAuth
    @PostMapping(value = "mark")
    @ApiOperation(value = "子任务详情，进行商品打标", notes = "子任务中商品打标接口")
    public RespResult taskSonMark(HttpServletRequest request,
                                  @ApiParam(required = true, value = "子任务id") Long taskSonId,
                                  @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        try {
            // 机构id从缓存中获取
            //Long orgId = MenberCacheKey.getOrgId(uid);
            //Long salId = MenberCacheKey.getSalesmanId(uid);
            //iSellerShopGoodsService.synchMarkShopGoods(taskSonId, salId, orgId);
            iSellerShopGoodsService.synchMarkShopGoods(taskSonId);
            // info
            logger.info("==>同步:商品打标成功，子任务ID：{}", taskSonId);
        } catch (Exception e) {
            logger.info("==>同步:进行商品打标，出现异常，异常原因：{}", e.getMessage());
            return RespResult.error("失败！");
        }
        return RespResult.success("成功");
    }


    @AppAuth
    @GetMapping(value = "load/imgtype")
    @ApiOperation(value = "加载图片类型", notes = "加载图片类型数据接口")
    public RespResult<List<SysConfigDtl>> loadImgType() {
        List<SysConfigDtl> cfd = iConfigDtlService.findConfigDtl("CODE_IMG");
        return RespResult.success(cfd);
    }


    @AppAuth
    @GetMapping(value = "load/xfd")
    @ApiOperation(value = "加载“现付单”图片类型", notes = "加载“现付单”图片类型接口")
    public RespResult<List<TaskPictureTypeSettingVo>> pictureTypeSettingXFD() {
        List<TaskPictureTypeSettingVo> list = iTaskPictureTypeSettingService
                .findTaskPictureTypeSettingVoByPictureType(TaskPictureType.TYPE_XFD);
        return RespResult.success(list);
    }


    @AppAuth
    @GetMapping(value = "load/grd")
    @ApiOperation(value = "加载“隔日单”图片类型", notes = "加载“隔日单”图片类型接口")
    public RespResult<List<TaskPictureTypeSettingVo>> pictureTypeSettingGRD() {
        List<TaskPictureTypeSettingVo> list = iTaskPictureTypeSettingService
                .findTaskPictureTypeSettingVoByPictureType(TaskPictureType.TYPE_GRD);
        return RespResult.success(list);
    }


    @AppAuth
    @GetMapping(value = "overtime")
    @ApiOperation(value = "任务超时列表", notes = "任务超时列表接口")
    public RespResult<PageData<TaskSonVo>> findTaskSonOvertimeList(
            @ApiParam(required = true, value = "分页页码") @RequestParam int page,
            @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid) {
        HttpReqParam param = new HttpReqParam();
        param.setPage(page);
        IPage<TaskSonVo> taskSonVoList = iTaskSonService.findTaskSonOvertimeList(uid, PageRequestUtil.buildPageRequest(param));
        return RespResult.buildPageData(taskSonVoList);
    }
}
