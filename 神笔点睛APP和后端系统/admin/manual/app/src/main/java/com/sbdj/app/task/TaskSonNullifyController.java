package com.sbdj.app.task;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.app.web.AppAuth;
import com.sbdj.app.web.RespResult;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.Version;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.service.task.admin.vo.TaskSonNullifyVo;
import com.sbdj.service.task.entity.TaskSonNullify;
import com.sbdj.service.task.service.ITaskSonNullifyService;
import com.sbdj.service.task.status.NullifyStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 子任务作废控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Api(tags = "子任务作废控制器")
@RequestMapping(value = Version.V1 + "/task/son/nullify")
@RestController
public class TaskSonNullifyController {


    @Autowired
    private ITaskSonNullifyService iTaskSonNullifyService;


    @AppAuth
    @PostMapping(value = "save")
    @ApiOperation(value = "保存作废", notes = "保存作废接口")
    public RespResult taskSonNullify(
            @ApiParam(required = true, value = "任务详情id") @RequestParam Long taskSonId,
            @ApiParam(required = true, value = "作废原因") @RequestParam String reasons) {
        try {
            iTaskSonNullifyService.saveTaskSonNullify(taskSonId, reasons, null);
        } catch (Exception e) {
            return RespResult.error("提交失败");
        }
        return RespResult.success("提交成功");
    }


    @AppAuth
    @PostMapping(value = "list")
    @ApiOperation(value = "作废列表", notes = "作废列表接口")
    public RespResult<PageData<TaskSonNullifyVo>> taskSonNullifyList(
            @RequestParam(value = "页码", defaultValue = "1") int page,
            @ApiParam(required = true, value = "uid（当前登录id）") @RequestParam Long uid,
            @ApiParam(required = true, value = "状态：待审核：W 已审核：T 不通过：N ") @RequestParam String status) {
        HttpReqParam reqParam = new HttpReqParam();
        reqParam.setPage(page);
        reqParam.setLimit(20);
        IPage<TaskSonNullifyVo> list = iTaskSonNullifyService.findTaskSonNullifyPage(uid, status, PageRequestUtil.buildPageRequest(reqParam));
        return RespResult.buildPageData(list);
    }


    @AppAuth
    @PostMapping(value = "update")
    @ApiOperation(value = "第二次提交作废", notes = "第二次提交作废接口")
    public RespResult updateTaskSonNullify(
            @ApiParam(required = true, value = "作废id") @RequestParam Long id,
            @ApiParam(required = true, value = "作废原因") @RequestParam String reasons,
            @ApiParam(required = true, value = "作废图片，多个使用逗号隔开") @RequestParam String images) {
        try {
            TaskSonNullify taskSonNullify = new TaskSonNullify();
            taskSonNullify.setId(id);
            taskSonNullify.setReasons(reasons);
            taskSonNullify.setImages(images);
            taskSonNullify.setStatus(NullifyStatus.STATUS_WAIT.getCode());
            iTaskSonNullifyService.updateTaskSonNullify(taskSonNullify);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success("提交成功");
    }

    @AppAuth
    @RequestMapping("delete")
    public RespResult deleteTaskSonNullify(Long id) {
        try {
            iTaskSonNullifyService.logicDeleteTaskSonNullifyById(id, null);
        } catch (Exception e) {
            return RespResult.error();
        }
        return RespResult.success();
    }
}
