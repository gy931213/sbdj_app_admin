package com.sbdj.app.web;

import com.sbdj.core.util.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * <p>
 * APP拦截器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public class AppAuthInterceptor extends HandlerInterceptorAdapter {

    private Logger logger = LoggerFactory.getLogger(AppAuthInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        AppAuth appAuth = null;
        if (handler instanceof HandlerMethod) {
            Method mtd = ((HandlerMethod) handler).getMethod();
            appAuth = mtd.getDeclaringClass().getAnnotation(AppAuth.class);
            if (appAuth == null) {
                appAuth = mtd.getAnnotation(AppAuth.class);
            }
        }
        // 判断当前请求是否需要登录验证!
        if (appAuth == null) {
            // 不需要登录验证!
            return true;
        }

        //logger.info("Authorization==【{}】",request.getHeader("Authorization"));

        // 响应数据对象
        RespResult respResult = new RespResult();
        // 从请求参数中获取当前登录人id
        String identify = request.getHeader("identify");
        if (StrUtil.isNull(identify)) {
            respResult.setCode(1000);
            respResult.setMessage("非法访问，请重新登录！");
            RespResult.respOutput(request, response, respResult);
            logger.error("===>非法访问，请重新登录===>");
            return false;
        }

        // 从缓存中获取Session会话数据。
        AppSession appSession = AppCacheKey.getAppSession(Long.valueOf(identify));

        // 判断回话对象为空处理。
        if (null == appSession) {
            respResult.setCode(1000);
            respResult.setMessage("非法访问，请重新登录！");
            RespResult.respOutput(request, response, respResult);
            logger.error("===>非法访问，请重新登录===>");
            return false;
        }

        // 判断没有登录状态的处理。
        if (!appSession.auth()) {
            respResult.setCode(1000);
            respResult.setMessage("没有登录！");
            RespResult.respOutput(request, response, respResult);
            logger.error("===>没有登录===>");
            return false;
        }

        // 从请求头中获取token数据。
        ///String tokenKey = request.getHeader("Authorization");
        // 判断tokenKey为空的处理方式。
        /**if (tokenKey == null) {
         respResult.setCode(1000);
         respResult.setMessage("非法访问，请重新登录！");
         RespResult.respOutput(request, response, respResult);
         logger.error("===>非法访问，请重新登录===>");
         return false;
         }**/
        /**
         // 从Session对象中获取当前登录人对象。
         Salesman salesman2 = (Salesman) appSession.getObject();
         // 跟APP传递过来的tokenKey，生成token验证对象。
         Claims claims = JWTUtil.parseJWT(tokenKey, RSAKit.findpublicKey());
         // 判断对象为空处理方式。
         if (salesman2 == null || claims == null) {
         respResult.setCode(1000);
         respResult.setMessage("非法访问，请重新登录");
         RespResult.respOutput(request, response, respResult);
         logger.error("===>非法访问，请重新登录===>");
         return false;
         }

         // 验证token
         if (!JWTUtil.verificationData(claims, salesman2.getPwd(), salesman2.getMobile(), salesman2.getKeyword())) {
         respResult.setCode(1000);
         respResult.setMessage("非法访问，请重新登录");
         RespResult.respOutput(request, response, respResult);
         return false;
         }
         **/

        return true;
    }

}
