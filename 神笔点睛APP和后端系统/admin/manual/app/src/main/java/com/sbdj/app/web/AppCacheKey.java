package com.sbdj.app.web;

import com.sbdj.core.util.RedisUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.entity.Salesman;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 获取Redis缓存数据的类
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public class AppCacheKey {
	/**
	 * Session会话cacheKey
	 */
	private final static String MEMBER_CACHE_KEY_ID = "SESSION_ID_";

	private static AppSession appSession = null;
	
	/**
	 * 获取缓存key
	 * @author Yly
	 * @date 2019-11-28 09:48
	 * @param id 登录人id
	 * @return java.lang.String
	 */
	public static String getAppCacheKey(Long id) {
		return MEMBER_CACHE_KEY_ID+id;
	}
	
	public static String getAppCacheKey(Object value) {
		return MEMBER_CACHE_KEY_ID+value;
	}
	
	/**
	 * 获取当前登录人id
	 * @author Yly
	 * @date 2019-11-28 09:52
	 * @param uid 登录人id
	 * @return java.lang.Long
	 */
	public static Long getSalesmanId(Long uid) {
		  // 从缓存中获取Session会话数据。
        appSession = (AppSession) RedisUtil.get(AppCacheKey.getAppCacheKey(uid));

		if(StrUtil.isNull(appSession)) {
			throw new RuntimeException("MenberCacheKey Get AppSession Is Null");
		}
        return appSession.getIdentify();
	}
	
	/**
	 * 获取当前登录人对象
	 * @author Yly
	 * @date 2019-11-28 09:51
	 * @param uid 登录人id
	 * @return com.sbdj.service.member.entity.Salesman
	 */
	public static Salesman getSalesman(Long uid) {
		  // 从缓存中获取Session会话数据。
		  appSession = (AppSession) RedisUtil.get(AppCacheKey.getAppCacheKey(uid));
		  if(StrUtil.isNull(appSession)) {
				throw new RuntimeException("MenberCacheKey Get AppSession Is Null");
		   }
		  return (Salesman) appSession.getObject();
	}
	
	/**
	 * 获取当前登录人所属机构
	 * @author Yly
	 * @date 2019-11-28 09:50
	 * @param uid 登录人id
	 * @return java.lang.Long
	 */
	public static Long getOrgId(Long uid) {
		  // 从缓存中获取Session会话数据。
		  appSession = (AppSession) RedisUtil.get(AppCacheKey.getAppCacheKey(uid));
		  if(StrUtil.isNull(appSession)) {
			  throw new RuntimeException("MenberCacheKey Get AppSession Is Null");
		  }
		  return appSession.getOrgId();
	}
	
	/**
	 * 获取当前登录人会话对象
	 * @author Yly
	 * @date 2019-11-28 09:50
	 * @param uid 登录人id
	 * @return com.sbdj.app.web.AppSession
	 */
	public static AppSession getAppSession(Long uid) {
		// 从缓存中获取Session会话数据。
		appSession = (AppSession) RedisUtil.get(AppCacheKey.getAppCacheKey(uid));
		if(StrUtil.isNull(appSession)) {
			return null;
		}
		return appSession;
	}
	
	/**
	 * 清除Session对象信息
	 * @author Yly
	 * @date 2019-11-28 09:49
	 * @param request 请求
	 * @return boolean
	 */
	public static boolean sessionInvalidate(HttpServletRequest request) {
		String id = request.getHeader("identify");
		if(StrUtil.isNull(id)) {
			throw new RuntimeException("identify is null");
		}
		String key = getAppCacheKey(id);
		if(StrUtil.isNull(key)) {
			throw new RuntimeException("key is null");
		}
		RedisUtil.del(key);
		return true;
	}
}
