package com.sbdj.app.web;

import com.sbdj.core.base.ISession;
import com.sbdj.core.constant.Number;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * APP请求工具
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public class AppRequestUtil {
    private AppRequestUtil() {}

    public static final String APP_SESSION_KEY = "APP_SESSION_KEY";

    /**
     * 设置APP回话中的Session
     * @author Yly
     * @date 2019-11-28 09:45
     * @param request 请求
     * @param session 会话
     * @return void
     */
    public static void setSession(HttpServletRequest request, ISession session) {
        request.getSession().setAttribute(APP_SESSION_KEY, session);
        request.getSession().setMaxInactiveInterval((3*24*60*60*1000));
    }

    /**
     * 获取APP回话中的Session
     * @author Yly
     * @date 2019-11-28 09:45
     * @param request 请求
     * @return com.sbdj.core.base.ISession
     */
    public static ISession getSession(HttpServletRequest request) {
        return (ISession) request.getSession().getAttribute(APP_SESSION_KEY);
    }

    /**
     * 注销APP回话中的Session
     * @author Yly
     * @date 2019-11-28 09:45
     * @param request 请求
     * @return void
     */
    public static void sessionInvalidate(HttpServletRequest request) {
        request.getSession().invalidate();
    }

    /**
     * 获取用户标识identify
     * @author Yly
     * @date 2019-11-28 09:45
     * @param request 请求
     * @return java.lang.Long
     */
    public static Long identify(HttpServletRequest request) {
        AppSession session = (AppSession) AppRequestUtil.getSession(request);
        return session == null ? Number.LONG_ZERO : session.identify();
    }

    /**
     * 所属机构
     * @author Yly
     * @date 2019-11-28 09:44
     * @param request 请求
     * @return java.lang.Long
     */
    public static Long orgId(HttpServletRequest request) {
        AppSession session = (AppSession) AppRequestUtil.getSession(request);
        return session == null ? Number.LONG_ZERO : session.getOrgId();
    }

   /**
    * 判断用户是否登录
    * @author Yly
    * @date 2019-11-28 09:44
    * @param request 请求
    * @return boolean
    */
    public static boolean isAuthApp(HttpServletRequest request) {
        ISession session = AppRequestUtil.getSession(request);
        if (null!=session && session.auth()) {
            return session.auth();
        } else {
            return Boolean.FALSE;
        }
    }
}
