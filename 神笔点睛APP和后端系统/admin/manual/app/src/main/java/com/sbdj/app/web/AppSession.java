package com.sbdj.app.web;

import com.sbdj.core.base.ISession;

import java.util.Set;

/**
 * <p>
 * APP session
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public class AppSession implements ISession {

    private boolean auth;
    private Long identify;
    private String token;
    private Long orgId;
    private Object object;

    @Override
    public boolean auth() {
        return this.auth;
    }

    @Override
    public Long identify() {
        return this.identify;
    }

    @Override
    public Long orgId() {
        return this.orgId;
    }

    @Override
    public Set<String> permissions() {
        return null;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public Long getIdentify() {
        return identify;
    }

    public void setIdentify(Long identify) {
        this.identify = identify;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
