package com.sbdj.app.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <p>
 * AppWeb控制器
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Configuration
public class AppWebConfig implements WebMvcConfigurer {
    private Logger logger = LoggerFactory.getLogger(AppWebConfig.class);

    @Value("${swagger.show}")
    private boolean swaggerShow = false;
    @Value("${app.enabled}")
    private boolean authEnable = true;

    // 配置拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 判断为false不进行登录验证拦截 authEnable为拦截
        if (authEnable) {
            registry.addInterceptor(new AppAuthInterceptor())
                    .addPathPatterns("/**")
                    .excludePathPatterns("/v1/base/findkey")
                    .excludePathPatterns("/favicon.ico");
            logger.info("==================================权限拦截器加载成功===========================");
        }
    }

    // 配置静态资源,避免静态资源请求被拦截
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if(swaggerShow) {
            registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
            registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
            registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");
            registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        }
    }
}
