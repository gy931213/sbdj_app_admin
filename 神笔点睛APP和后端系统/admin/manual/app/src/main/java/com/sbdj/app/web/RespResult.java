package com.sbdj.app.web;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.core.base.PageData;
import com.sbdj.core.constant.ResultCode;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RespResult<T> {

	private static Logger logger = LoggerFactory.getLogger(RespResult.class);

	/** 返回码，正常返回为 0 */
	@ApiModelProperty(value = "返回码，正常返回为 0")
	private int code = ResultCode.RESULT_SUCCESS_CODE;

	/** 返回的错误信息 **/
	@ApiModelProperty(value = "返回请求消息")
	private String message = "";

	/** 返回的数据对象 **/
	@ApiModelProperty(value = "返回请求成功的结果")
	private T data;

	@ApiModelProperty(value = "总记录数")
	private int total = 0;

	public RespResult() {

	}

	/**
	 * success:成功时候的调用 . <br/>
	 * 
	 * @param data 返回成功请求的数据结果
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> RespResult<T> success(T data) {
		return new RespResult<T>(data);
	}

	/**
	 * success:成功，参数可传可不传 . <br/>
	 * 
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> RespResult<T> success() {
		return success("success");
	}

	/**
	 * success:成功，
	 *
	 * @param message 返回成功信息
	 * @param <T>
	 * @return
	 */
	public static <T> RespResult<T> success(String message) {
		return new RespResult<T>(ResultCode.RESULT_SUCCESS_CODE, message);
	}

	/**
	 * Created by Htl to 2019/05/30 <br/>
	 * success:成功
	 * @param <T>
	 * @param data    响应的结果
	 * @param message 消息
	 * @return
	 */
	public static <T> RespResult<T> success(T data,String message) {
		return new RespResult<T>(data, message);
	}

	/**
	 * success:成功，
	 *
	 * @param code    代码
	 * @param message 返回成功信息
	 * @param <T>
	 * @return
	 */
	public static <T> RespResult<T> success(int code, String message) {
		return new RespResult<T>(code, message);
	}

	/**
	 * error: 失败时候的调用 . <br/>
	 * 
	 * @param message 提示消息
	 * @param code    代码
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> RespResult<T> error(int code, String message) {
		return new RespResult<T>(code, message);
	}

	/**
	 * error: 失败时候的调用 . <br/>
	 * 
	 * @author 黄天良
	 * @param message 返回请求的错误信息
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> RespResult<T> error(String message) {
		return new RespResult<>(ResultCode.RESULT_ERROR_CODE, message);
	}

	public static <T> RespResult<T> error(T data,String message) {
		return new RespResult<T>(data,ResultCode.RESULT_ERROR_CODE, message);
	}

	/**
	 * error: 失败时候的调用
	 * 
	 * @return
	 */
	public static <T> RespResult<T> error() {
		return error("error");
	}

	public RespResult(int code, String msg) {
		this.code = code;
		this.message = msg;
	}

	public RespResult(T data) {
		this.code = ResultCode.RESULT_SUCCESS_CODE;
		this.message = "success";
		this.data = data;
	}

	public RespResult(T data, String message) {
		this.code = ResultCode.RESULT_SUCCESS_CODE;
		this.message = message;
		this.data = data;
	}

	public RespResult(T data, int code , String message) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public RespResult(String message) {
		this.code = ResultCode.RESULT_SUCCESS_CODE;
		this.message = message;
	}

	/**
	 * buildPageData:pageData数据的结果. <br/>
	 *
	 * @param page 当前分页数据
	 * @return
	 * @since JDK 1.7
	 */
	public static <T> RespResult<PageData<T>> buildPageData(IPage<T> page) {
		PageData<T> pageData = new PageData<>();
		pageData.setPage(page.getCurrent());
		pageData.setLimit(page.getSize());
		pageData.setList(page.getRecords());
		pageData.setTotal(page.getTotal());
		return new RespResult<>(pageData);
	}

	/**
	 * respOutput: 返回结果<br/>
	 *
	 * @param request  请求对象
	 * @param response 响应对象
	 * @param codeMsg  消息对象
	 * @since JDK 1.8
	 */
	public static void respOutput(HttpServletRequest request, HttpServletResponse response, String codeMsg) {
		try {
			// 判断是不是ajax请求或者是不是APP请求
			if (isAjax(request) || isApp(request)) {
				response.setContentType("application/json;charset=UTF-8");
				response.getWriter().write(JSON.toJSONString(error(codeMsg)));
			} else {
				response.setContentType("application/json;charset=UTF-8");
				response.getWriter().write(JSON.toJSONString(error(codeMsg)));
				logger.info("====>您目前非法请求，APP环境存在异常！");
			}
		} catch (IOException e) {

		} finally {
			try {
				response.getWriter().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * respOutput: <br/>
	 *
	 * @param request    请求对象
	 * @param response   响应对象
	 * @param respResult 消息对象
	 * @since JDK 1.8
	 */
	public static void respOutput(HttpServletRequest request, HttpServletResponse response, RespResult respResult) {
		try {
			// 判断是不是ajax请求或者是不是APP请求
			if (isAjax(request) || isApp(request)) {
				response.setContentType("application/json;charset=UTF-8");
				response.getWriter().write(JSON.toJSONString(respResult));
			} else {
				response.setContentType("application/json;charset=UTF-8");
				response.getWriter().write(JSON.toJSONString(respResult));
				logger.info("====>您目前非法请求，APP环境存在异常！");
			}
		} catch (IOException e) {

		} finally {
			try {
				response.getWriter().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * isAjax: 判断是否ajax请求<br/>
	 *
	 * @param request
	 * @return
	 * @since JDK 1.8
	 */
	private static boolean isAjax(HttpServletRequest request) {
		return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
	}

	/**
	 * isApp: 判断是否是APP<br/>
	 *
	 * @param request
	 * @return
	 * @since JDK 1.8
	 */
	private static boolean isApp(HttpServletRequest request) {
		return "xq.app".equals(request.getHeader("client"));
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public T getData() {
		return data;
	}

	public int getTotal() {
		return total;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setData(T data) {
		this.data = data;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
