package com.sbdj.core.base;

import com.sbdj.core.util.RequestUtil;
import com.sbdj.core.util.StrUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Yly to 2019/11/8 19:38
 * 基本日志记录
 */
public abstract class BaseLogAspect {
    private Logger logger;

    protected void initLog(Logger logger) {
        this.logger = logger;
    }

    protected Object doLog(ProceedingJoinPoint pjp) throws Throwable {
        Object result;
        try {
            result = pjp.proceed();
            return result;
        } catch (Throwable e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            try {
                // 记录日志
                log(pjp);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    private void log(ProceedingJoinPoint pjp) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes != null ? attributes.getRequest() : null;
        if (null == request) {
            return;
        }
        String url = request.getRequestURI() == null ? "" : request.getRequestURI();
        String httpMethod = request.getMethod();
        String classMethod = pjp.getSignature().getDeclaringTypeName() + "." + pjp.getSignature().getName();
        logger.info("===>请求IP：【{}】", RequestUtil.getIpAddr(request));
        logger.info("===>请求地址：【{}】，请求方式：【{}】，请求方法：【{}】，参数：【{}】",url,httpMethod,classMethod,request.getQueryString());
    }

    protected void getThrows(JoinPoint jp, Exception e) {
        // 方法异常时执行.....
        String className = jp.getTarget().getClass().getName();
        String methodName = jp.getSignature().getName();
        Object[] args = jp.getArgs();
        logger.error("类:【{}】 方法:【{}】 参数:【{}】 异常:【{}】" ,className,methodName,args,e.getMessage());
    }
}
