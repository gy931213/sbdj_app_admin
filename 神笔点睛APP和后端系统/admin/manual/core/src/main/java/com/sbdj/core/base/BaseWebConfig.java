package com.sbdj.core.base;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by Yly to 2019/11/8 20:06
 * 基本控制器
 */
@EnableSwagger2
@EnableTransactionManagement
@MapperScan(basePackages = "com.sbdj.service.*.mapper")
public abstract class BaseWebConfig implements WebMvcConfigurer {
    private static final Logger logger = LoggerFactory.getLogger(BaseWebConfig.class);

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        // 设置请求的页面大于最大页后操作， true调回到首页，false 继续请求  默认false
        // paginationInterceptor.setOverflow(false);
        // 设置最大单页限制数量，默认 500 条，-1 不受限制
        // paginationInterceptor.setLimit(500);
        return paginationInterceptor;
    }

    @Value("${swagger.show}")
    private boolean swaggerShow;

    protected abstract Docket api();

    protected Docket api(String info, int port, String ver, String packageName) {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(swaggerShow)
                .groupName(info)
                .apiInfo(apiInfo(info, port, ver))
                .select()
                .apis(RequestHandlerSelectors.basePackage(packageName))
                .paths(PathSelectors.any()) //PathSelectors.any()
                .build();
    }

    private ApiInfo apiInfo(String info, int port, String ver) {
        return new ApiInfoBuilder()
                // 页面标题
                .title(info)
                .description(info+"管理中心操作文档")
                .termsOfServiceUrl("http://127.0.0.1:"+port+"/swagger-ui.html")
                // 版本号
                .version(ver)
                // 描述
                .description("API 描述").build();
    }

    @Value("${base.intercept.enabled}")
    private boolean baseIntercept;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        if (baseIntercept) {
            InterceptorRegistration intReg= registry.addInterceptor(new BaseHandlerInterceptor());
            // ~拦截的配置
            intReg.addPathPatterns("/**");
            logger.info("==================================拦截器加载成功===========================");
        }
    }
}
