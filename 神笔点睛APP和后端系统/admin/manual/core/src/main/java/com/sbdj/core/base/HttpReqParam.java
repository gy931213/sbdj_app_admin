package com.sbdj.core.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 分页请求
 * </p>
 *
 * @author Yly
 * @since 2019/11/24 14:10
 */
@ApiModel(value="SysAdmin对象", description="管理员表")
public class HttpReqParam {
	// 当前请求的页码
	@ApiModelProperty(value = "当前请求的页码")
	private Integer page = 1;
	@ApiModelProperty(value = "每页条数")
	private Integer limit = 30;
	@ApiModelProperty(value = "需要排序的字段")
	private String field = "";
	@ApiModelProperty(value = "排序方式")
	private String order = "";
	@ApiModelProperty(value = "关键字")
	private String keyword = "";
	@ApiModelProperty(value = "状态")
	private String status;
	@ApiModelProperty(value = "类型")
	private Integer type;
	@ApiModelProperty(value = "组织id")
	private Long orgId = 0L;

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
}
