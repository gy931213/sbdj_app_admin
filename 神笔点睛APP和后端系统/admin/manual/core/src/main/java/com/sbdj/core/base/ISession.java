package com.sbdj.core.base;

import java.io.Serializable;
import java.util.Set;

/**
 * <p>
 * 基本会话
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 17:29
 */
public interface ISession extends Serializable {

	/**
	 * 是否登录
	 * 
	 * @return
	 */
	boolean auth();

	/**
	 * 用户标识
	 * 
	 * @return
	 */
	Long identify();

	/**
	 * 获取所属机构
	 * @return
	 */
	Long orgId();

	/**
	 * 获取所属权限
	 * @return
	 */
	Set<String> permissions();

}
