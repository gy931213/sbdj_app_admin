package com.sbdj.core.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Collection;

/**
 * <p>
 * 分页的数据
 * </p>
 *
 * @author Yly
 * @since 2019-11-25
 */
@ApiModel(value = "分页的数据")
public class PageData<T> {
	@ApiModelProperty(value = "总计条数")
	private Long total;
	@ApiModelProperty(value = "当前的页码")
	private Long page;
	@ApiModelProperty(value = "分页条数")
	private Long limit;
	@ApiModelProperty(value = "数据集合")
	private Collection<T> list;

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	public Long getLimit() {
		return limit;
	}

	public void setLimit(Long limit) {
		this.limit = limit;
	}

	public Collection<T> getList() {
		return list;
	}

	public void setList(Collection<T> list) {
		this.list = list;
	}
}
