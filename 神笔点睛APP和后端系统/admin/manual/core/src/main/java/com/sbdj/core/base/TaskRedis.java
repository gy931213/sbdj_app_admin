package com.sbdj.core.base;

import com.sbdj.core.util.BeanUtil;
import com.sbdj.core.util.RedisUtil;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * <p>
 * 任务专用redis
 * </p>
 *
 * @author Yly
 * @since 2019-11-25
 */
public class TaskRedis {

	// 递增因子
	private static final long FACTOR_INCREASE = 1;
	// 递减因子
	private static final long FACTOR_DECREASE = -1;

    // 通过spring-bean 获取对象
    private static RedisTemplate redisTemplate = BeanUtil.getBean(RedisTemplate.class);

    public static void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        TaskRedis.redisTemplate = redisTemplate;
    }
	
    private TaskRedis(){}

    // 任务可领数量key的前缀
    private static final String TASK_TOTAL_PREFIX ="T_T_PREFIX_";

    // 任务数据的前缀
    private static final String TASK_DATE_PREFIX ="T_DATA_PREFIX_";

    //
    private static String getCacheKey(Object prefix,Object key){
        return prefix.toString()+key;
    }

    /**
     * Created by Htl to 2019/07/05 <br/>
          *   缓存任务可领数量的key 
     * @param key 
     * @return
     */
    public static  String getTaskTotalCacheKey(Object key){
        return getCacheKey(TASK_TOTAL_PREFIX,key);
    }
    
    /**
     * Created by Htl to 2019/07/05 <br/>
          *   设置缓存任务可领数量的key 
     * @param key
     * @param value
     */
    public static boolean setTaskTotalCacheKey(Object key,Object value){
        try {
			RedisUtil.set(getCacheKey(TASK_TOTAL_PREFIX, key), value);
			return true;
		} catch (Exception e) {
			return false;
		}
    }

    /**
     * Created by Htl to 2019/07/05 <br/>
          *  缓存任务数据
     * @param key
     * @return
     */
    public static String getTaskDataCacheKey(Object key){
        return getCacheKey(TASK_DATE_PREFIX,key);
    }

    /**
     * Created by Htl to 2019/07/05 <br/>
          *  递减 
     * @param key
     * @return
     */
    public static Long decrease(Object key) {
		return RedisUtil.decr(getTaskTotalCacheKey(key),1L);
    }
    
    /**
     * Created by Htl to 2019/07/05 <br/>
          *  递增 
     * @param key
     * @return
     */
    public static Long increment(Object key) {
		return RedisUtil.incr(getTaskTotalCacheKey(key),FACTOR_INCREASE);
    }

    /**
     * Created by Htl to 2019/07/05 <br/>
          *  删除 
     * @param key
     */
	public static void remove(Object key) {
    	RedisUtil.del(getTaskTotalCacheKey(key));
	}
}
