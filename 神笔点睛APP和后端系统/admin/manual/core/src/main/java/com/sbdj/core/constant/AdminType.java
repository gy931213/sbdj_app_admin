package com.sbdj.core.constant;

/**   
 * @ClassName:  AdminType   
 * @Description: 人员类型
 * @author: 黄天良  
 * @date: 2018年11月19日 上午10:32:12   
 */
public final class AdminType {
	
	private AdminType() {}

	/**
	 * {@value} 管理员
	 */
	public static final int TYPE_ADMIN = 1;
	
	/**
	 * {@value} 商家
	 */
	public static final int TYPE_SELLER = 2;
	

}
