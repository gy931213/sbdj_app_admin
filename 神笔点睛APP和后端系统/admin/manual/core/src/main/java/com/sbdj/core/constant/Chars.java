package com.sbdj.core.constant;

/**
 * <p>
 * 字符常量类
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 15:43
 */
public final class Chars {
    /** 逗号 **/
    public static final String CHAR_COMMA = ",";

    /** 横线 **/
    public static final String CHAR_TRAN = "-";
}
