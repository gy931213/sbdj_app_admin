package com.sbdj.core.constant;

/**
 * Created by Yly to 2019/11/7 21:43
 * 编码
 */
public final class Encoding {
    private Encoding() {}
    public static final String GBK = "GBK";
    public static final String UTF8 = "UTF-8";
}
