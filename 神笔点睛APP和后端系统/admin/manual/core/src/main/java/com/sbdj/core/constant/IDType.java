package com.sbdj.core.constant;
/**
 * @ClassName:  IDType
 * @Description: (id类型模板)
 * @author: 陈元祺
 * @date: 2018/12/11 10:38
 */
public final  class IDType {

    /**
     * 图片类型
     */
    public static final long CODE_IMG = 1;

    /**
     * 证件类型
     */
    public static final long CODE_ZJLX = 2;

    /**
     * 任务类型
     */
    public static final long TASK_TYPE = 3;
}
