package com.sbdj.core.constant;

public final class IsSuperType {
	
	/**
	 * 系统超级管理员 【是】
	 */
	public  static final String IS_SUPER_YES = "1";
	
	/**
	 * 系统超级管理员 【否】
	 */
	public  static final String IS_SUPER_NO = "0";

}
