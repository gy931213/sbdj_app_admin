package com.sbdj.core.constant;

/**   
 * @ClassName:  IsTimeOutType   
 * @Description: 是否超时类型 
 * @author: 黄天良  
 * @date: 2018年11月30日 下午5:45:35   
 */
public final class IsTimeOutType {
	
	/**
	 * 超时 {@value}
	 */
	public static final String TIME_OUT_TYPE_YES = "YES";
	/**
	 * 未超时 {@value}
	 */
	public static final String TIME_OUT_TYPE_NO = "NO";

}
