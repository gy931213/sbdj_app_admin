package com.sbdj.core.constant;

/**
 * Created by Yly to 2019/11/7 21:44
 * 数字常量
 */
public final class Number {
    private Number() {}
    // Long  -1
    public static final Long LONG_NEGATIVE = -1L;
    // Long 0
    public static final Long LONG_ZERO = 0L;
    // Long 1
    public static final Long LONG_ONE = 1L;
    // Int 0
    public static final Integer INT_ZERO = 0;
    // Int 1
    public static final Integer INT_ONE = 1;
    // Int -1
    public static final Integer INT_NEGATIVE_ONE = -1;
    // Double 0
    public static final Double DOUBLE_ZERO = 0.00;
}
