package com.sbdj.core.constant;

/**
 * @ClassName: PlatformType
 * @Description: 平台类型
 * @author: 黄天良
 * @date: 2019年1月11日 上午11:24:25
 */
public final class PlatformType {

	/** JD **/
	public final static String P_TYPE_JD = "JD";

	/** TB **/
	public final static String P_TYPE_TB = "TB";

	/** ALBB **/
	public final static String P_TYPE_ALBB = "ALBB";

	/** PDD **/
	public final static String P_TYPE_PDD = "PDD";
}
