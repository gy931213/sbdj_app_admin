package com.sbdj.core.constant;

/**
 * @ClassName:  PlatformTypeValue
 * @Description: 平台对应编号
 * @author: 杨凌云
 * @date: 2019/6/3 10:56
 */
public enum PlatformTypeValue {
    PLATFORM_TYPE_JD(PlatformType.P_TYPE_JD, 1L, "京东"),
    PLATFORM_TYPE_TB(PlatformType.P_TYPE_TB, 2L, "淘宝"),
    PLATFORM_TYPE_ALBB(PlatformType.P_TYPE_ALBB, 3L, "阿里巴巴"),
    PLATFORM_TYPE_PDD(PlatformType.P_TYPE_PDD, 4L, "拼多多"),

    // 第三方API需要的平台编号
    API_PLATFORM_TYPE_JD(PlatformType.P_TYPE_JD, 5L, "京东"),
    API_PLATFORM_TYPE_TB(PlatformType.P_TYPE_TB, 1L, "淘宝"),
    API_PLATFORM_TYPE_PDD(PlatformType.P_TYPE_PDD, 7L, "拼多多"),
    ;

    PlatformTypeValue(String platformCode, Long platformId, String platformName) {
        this.platformCode = platformCode;
        this.platformId = platformId;
        this.platformName = platformName;
    }

    // 平台Code
    private String platformCode;
    // 平台编号
    private Long platformId;
    // 平台名称
    private  String platformName;

    public String getPlatformCode() {
        return platformCode;
    }

    public void setPlatformCode(String platformCode) {
        this.platformCode = platformCode;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }
}
