package com.sbdj.core.constant;

/**
 * 类名: ResultCode <br/>
 * 描述: 请求响应的常量类 <br/>
 * 日期: 2018年8月14日 上午9:40:50
 *
 * @version
 * @since JDK 1.8
 */
public final class ResultCode {
	private ResultCode() { }

	/**
	 * 请求成功返回码 {@value}
	 */
	public static final int RESULT_SUCCESS_CODE = 0;

	/**
	 * 请求失败返回码 {@value}
	 */
	public static final int RESULT_ERROR_CODE = 1;

}
