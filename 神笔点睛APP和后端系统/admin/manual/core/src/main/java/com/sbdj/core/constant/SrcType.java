package com.sbdj.core.constant;

/**   
 * @ClassName:  SrcType   
 * @Description: 资源类型
 * @author: 黄天良  
 * @date: 2018年11月27日 下午6:20:22   
 */
public final class SrcType {
	/**
	 * {@value} 业务员
	 */
	public static final String SRC_TYPE_SM = "SM";
	/**
	 * {@value} 业务员号主
	 */
	public static final String SRC_TYPE_SN = "SN";

}
