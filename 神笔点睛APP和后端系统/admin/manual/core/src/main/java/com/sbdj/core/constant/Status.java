package com.sbdj.core.constant;

/**
 * Created by Yly to 2019/11/7 21:51
 * 状态
 */
public final class Status {
    private Status() {}
    // YES
    public static final String STATUS_YES = "YES";
    // NO
    public static final String STATUS_NO = "NO";
    // 活动状态
    public static final String STATUS_ACTIVITY = "A";
    // 删除状态
    public static final String STATUS_DELETE = "D";
    // 草稿状态
    public static final String STATUS_DRAFT = "C";
    // 终止状态
    public static final String  STATUS_STOP = "N";
    // 运行中
    public static final String  STATUS_RELEASED = "R";
    // 锁住状态
    public static final String STATUS_LOCK = "L";
    // 启用状态
    public static final String STATUS_ENABLE = "E";
    // 禁用状态
    public static final String STATUS_PROHIBIT = "P";
    // 待提交
    public static final String STATUS_NOT_AUDITED = "F";
    // 已审核
    public static final String STATUS_YES_AUDITED = "T";
    // 审核不通过
    public static final String STATUS_NOT_PASS = "AN";
    // 审核失败
    public static final String STATUS_AUDITED_FAIL = "AF";
    // 已传图
    public static final String STATUS_ALREADY_UPLOAD = "AU";
    // 已支付
    public static final String STATUS_ALREADY_PAY = "Y";
    // 未支付
    public static final String STATUS_NOT_PAY = "N";
    // 作废
    public static final String STATUS_CANCEL= "O";
    // 奖励
    public static final String PRICE_REWORD = "PR";
    // 惩罚
    public static final String PRICE_PUNISH = "PE";
    // NO
    public static final String NOT_OK = "NO";
    // 垫付
    public static final String ADVANCE_PAY = "DF";
    // 申请付款
    public static final String APPLY_PAY = "SQ";
    // 作废
    public static final String STATUS_NULLIFY = "NF";
    // 申诉
    public static final String STATUS_APPEAL = "AP";
    // 未完成任务
    public static final String STATUS_UNDONE = "NC";
    // 模板
    public static final String STATUS_TEMPLATE = "TMP";
    // 未审号状态
    public static final String STATUS_UNREVIEWED = "EP";
}
