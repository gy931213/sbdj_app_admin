package com.sbdj.core.constant;

/**
 * 流水类型,[{1：银行流水},{0：其他流水}]
 * @author Administrator
 *
 */
public final class StreamType {

	/**
	 * {1：银行流水}
	 */
	public static final int STREAM_TYPE_1 = 1;
	/**
	 * {0：其他流水}
	 */
	public static final int STREAM_TYPE_0 = 0;
}
