package com.sbdj.core.constant;

/**   
 * @ClassName:  TaskTypeCode   
 * @Description: 任务类型编码类
 * @author: 黄天良  
 * @date: 2018年12月25日 下午4:09:23   
 */
public final class TaskTypeCode {
	
	/**
	 * 现付单 {@value}
	 */
	public static final String TASK_TYPE_XFD = "XFD";
	/**
	 * 隔日单 {@value}
	 */
	public static final String TASK_TYPE_GRD = "GRD";
	/**
	 * 浏览单 {@value}
	 */
	public static final String TASK_TYPE_LLD = "LLD";

}
