package com.sbdj.core.constant;

/**
 * <p>
 * 版本
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 16:03
 */
public final class Version {
    public static final String  V1 = "v1";
    public static final String  V2 = "v2";
    public static final String  V3 = "v3";
}
