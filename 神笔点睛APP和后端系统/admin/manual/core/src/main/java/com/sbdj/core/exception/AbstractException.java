package com.sbdj.core.exception;

/**
 * Created by Yly to 2019/11/7 22:00
 * 抽象异常
 */
public abstract class AbstractException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    int code;
    String msg;

    /**
     * 错误编码
     * @return code
     */
    public abstract int code();

    /**
     * 错误信息
     * @return msg
     */
    public abstract String msg();

    @Override
    public String getMessage() {
        return msg();
    }
}
