package com.sbdj.core.exception;

/**
 * Created by Yly to 2019/11/7 22:04
 * 基本异常
 */
public class BaseException extends AbstractException {
    private static final long serialVersionUID = 1L;

    private String init(int code, String msg) {
        return "["+code+"]" + " " + msg;
    }

    private BaseException() {
        this.code = BASE.BASE.getCode();
        this.msg = init(BASE.BASE.getCode(), BASE.BASE.getMsg());
    }

    private BaseException(String msg) {
        this.msg = msg;
    }

    private BaseException(int code, String msg) {
        this.code = code;
        this.msg = init(code, msg);
    }

    private BaseException(BASE base) {
        this.code = base.getCode();
        this.msg = init(base.getCode(), base.getMsg());
    }

    public static BaseException base() {
        return new BaseException();
    }

    public static BaseException base(String msg) {
        return new BaseException(msg);
    }

    public static BaseException base(int code, String msg) {
        return new BaseException(code, msg);
    }

    public static BaseException base(BASE base) {
        return new BaseException(base);
    }

    @Override
    public int code() {
        return this.code;
    }

    @Override
    public String msg() {
        return this.msg;
    }

    public enum BASE {
        /** 基本异常 **/
        BASE(1000, "基本异常"),
        /** ORM异常 **/
        ORM(1001, "ORM异常"),
        /** SQL异常 **/
        SQL(1002, "SQL异常"),
        ;
        private int code;
        private String msg;

        BASE(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }
        public int getCode() {
            return code;
        }
        public void setCode(int code) {
            this.code = code;
        }
        public String getMsg() {
            return msg;
        }
        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
