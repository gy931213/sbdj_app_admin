package com.sbdj.core.util;

import java.io.*;
import java.util.Base64;

/**
 * Created by Yly to 2019/11/7 23:35
 * Base64工具
 */
public class Base64Util {

	/**
	 * Base64 编码/解码器 JDK1.8
	 */
	public static Base64.Decoder decoder = Base64.getDecoder();
	public static Base64.Encoder encoder = Base64.getEncoder();

	public static InputStream base64dataToInputStream(String base64data) throws IOException {
		if (base64data == null) {
			return null;
		}
		InputStream is = null;
		try {
			base64data = base64data.substring(base64data.indexOf(",") + 1, base64data.length());
			// Base64解码
			byte[] b = decoder.decode(base64data);
			for (int i = 0; i < b.length; ++i) {
				// 调整异常数据
				if (b[i] < 0) {
					b[i] += 256;
				}
			}
			is = new ByteArrayInputStream(b);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			is.close();
		}
		return is;
	}

	public static void main(String[] args) {
		String str = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAJmAmYDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD2aiiigAooooAKKKKACiimSyxwRNLK4RFGSzHAFAm0ldj6RmVFLMwVRySTgCuU1TxvDGPL01PNf/npIpCj6Dqa5TUNWvdTfddzmQA5VegX6AV6VHLqtTWWiPIxOcUKWkPef4fed7f+K9KsePONw3pBhv1zisK68fSupW0sljPZ5H3fpiuSJ4ppavSp5dQhurniVc4xNTZ8q8jXm8V61MCDfMo9ERV/UDNU31fU5OG1G6Yehmb/ABqkWphNdSo04/DFL5HC69afxTf3kr3ErnLyu31Ymoi59T+dITTS1WRqOLn+8fzpu8+ppN1JmkULvPqfzo3H1P503NGaBjtzep/Ok3t6n86bmjNAx29vU/nRvb1P503NGaAHb29T+dG9vU/nTc0ZoAdvb1P50b29T+dNzRmgB29vU/nRvb1P503NGaAHb29T+dG9vU/nTc0ZoAdvb1P50b29T+dNzRmgB29vU/nRvb1P503NGaNAHb29T+dG9vU/nScetJRdAO3t6n86N7ep/Okw3pSUaAO3t6n86N7ep/Om9aM0DQ7e3qfzo3t6n86bRmgQ7e3qfzo3t6n86bmjNADt7ep/Oje3qfzpuaM0AO3t6n86N7ep/Om5ozQA7e3qfzo3t6n86bmjNADt7ep/Oje3qfzpuaM0AO3n+8fzo3t6n86bmjNADw7ep/Oje3qfzpoNGaBDt7eppd59T+dMzS5oAeHPqfzpwduuT+dR5pQaYi1HeXERzHPIn+65FWU1vVEI26jdDH/TZj/Ws4NShqXJF76gpzj8La+Z0EHjDWomG66Eqj+F415/EDNbVr4/Q4F1YsPVonz+h/xriFbFO3VhPB0J7x+7Q6aeYYqntN/PX8z1Ow8Q6ZqCgxXKo5/5ZykK35d/wrTrxsGtTTfEWpaYoSCYNEP+WbjI/wAa4KuV9ab+89ShnnStH5r/ACPUKK57S/GFje7YrnNtMcDkZQn2Pb8a6AEEZByDXk1KU6TtNWPfo16dePNTd0LRRRWZsFFFFABRRRQAUUUUAFFFFABRRRQAUUVyHiHxcF32emv83R5wensv+NbUaM60uWCObE4mnh4c83/wTV1rxNaaSpjUie5zjy1b7v1NcLqerXuqTmS5kyO0akhV+gqm7PK7O7FmY5LMckmm5xX0WGwdOhruz4/GZjVxLte0ewE00mlYjFMJrtuedYQmmk0E0wmpuWkKTTSaQmm5pGiQpNJmimmkOwuaSiikVYKKKSgBaSiigAooooAKKKKACiiigAooooAKKKKACiiigApecUlWtPspdRu1tYcB37mpk0ldlRi5OyK2Ce1Gcdq66DwBcPgy3ip7AGtW3+H9gmDNKZPpn/GuOWYUktz0o5XiJbrQ88JA6mnLFJJ9yN2/3VJr1SDwjo0BH+iK5/2uavw6XYW/EVnCuPRRXPLM19mJ1QyeT+KR5NFo+pSjdHZSkepXFVCCCVI5Xg+1exaiVg02dwoG1ewrx1+dzdNzE10YTFSr8za2OTHYOOG5bPcTrSUo6Gkru7HmsKKKKBBRRRQAUUUUAFFFFABRRRQAUUUUALRmkooAWlzSUUBYXNLmm0UxWH5pQaYDS5oTFYkDU8GoQacDTuQ0TA08GoQakDVRDRJmtfR/EV7pLBVbzoOnlOTgfT0rGBpwqZ041FyzV0VTq1KUuanKzPUtL1qz1aINBIBIB80RPzL+Hce9aFeR2tzNaTCa3laKRejKcGu88P8AieLUwttc4jugABzxJ7j39q8DF4B0vehqvyPq8Bmsa9qdXSX4M6CiiivMPbCiiigAooooAKKKKACiiuP8XeIdobTLKT5uRO6np/s/41tRoyrTUInNicTDDU3UmVvE/ic3W+xsHBt+kkgHLH0HtXLjnigc0dDX1NGjChDlifDYnE1MTU55/IQ8UhNBNMJrc50gJphNBNMJqS0hCaaTQTSVLNEgpM0GkpFBRRSGgYtJRRSAKKKKACiiigAooooAKKKKACiiigAooooAKKSimMXNGaSlpAFdB4NTdqzSf881z/Ouf4rqvBMW43MueAQv6Vz4mVqMjty+PNiYndW7Z/Crg61RthwKvjpXzMj7IdSUtFSBleJJPK0K5YegH615Cp4Ga9U8aSeX4cm56so/WvKx/Wvbyxe5JnzecO9SK8g70Ud6SvVWh4jFopKKYC0UlLQAUUUUhBRRRQAUUUUAFFFFABRRRQAUUUUAFLSUUALS5pKKYDqUHmmUtAmSg04GogacDVENEwNPBqIGng1Rk0PpwYqQykqwOQRTBTvrQTsd54Y8S/bgtlesBcAfI/8Az0/+vXTV4+rFCHRirA5BBwRXofhrX11W38i4ZRdx9QP41/vV4GPwfJ+8p7dfI+syrMva/uar16Pv/wAE3aKKK8k98KKKKACiimSypDE0sjBUQZYnsKBN21ZkeJtaXSbArGT9pmBEeP4fVv1rzhnaR2d2LMxyzE8k+tXNY1KTVdRkupCdv3Y1IxtXJwP1qnwAK+pwWGVGnru9z4fMcY8TVuvhW3+Yh46UnfmgmkJrtPNQhNMY0pNMY0i0hGNRk0pNNJqGapATSUlFIoKKSigYZozSUUDFopKKAFopKKAFopKKAFopKKAFopKKAFopKKACiiigAopaa33aAIpLhUOKia8xVW4Pz1E3SkdsaUbItteEnrXf+AlLaS8v/PST+QrzMn5c1614Ptzb6BbJ03Dd+YFcONf7qx6mX00qt10OotwSBVwA4qtbjgVar5+W57wUUUVIHMeP5Nvh/aDy0i15pXoXxEk26bEnq4/rXnvrXv5cv3PzPls2f7/5B3pKWivSe55IlFFFABRRRQAUtJRQAtFJRQAtFJRQAtFJRQAtFJRRYBaKSikFhaKSimAuaWm0tAC0UlGaAHA04GmA04GmSyQGnqaiBpwNNGbRODTgaiBp4NUZtD+tT2l3PY3SXNs5SVDwarg8UoPNDSasxRbi+Zbo9X0zUYdUsUuoTw3DLnO1u4q3XnfhPV20/UxbuR5F0wVsjo3Y/ma9Er5XF4d0KnL06H3eX4tYqipPdbhRRRXId4VynjfVBFarp0f35cPJ7KDwPzH6V1TMFUsxAAGST2ryvVdRbVNRmumBAdsKD2UcCvRy6j7SrzPZHjZxifZUOSO8tPl1KXfmkpT0pM19Kj44Q00mnGmG";
		try {
			 String accessKey = "3hCVYzQ1yucRWLVvCES1sHvl08tb2oLKrSJO6Qe7";      //AccessKey的值
		        String secretKey = "AEKmE3ugOshnSSBe_r3syWU2dHSbDpEFpnGFAkki";      //SecretKey的值
		        String bucket = "ffff123";
		        String img = "F:\\Ylything\\图片\\灵梦魔理沙.png";
		        InputStream in = new FileInputStream(new File(img));
			InputStream is = base64dataToInputStream(str);
			System.out.println(is);
		String obj= QiNuCloudUtil.uploadFileByInputStream(null, in);
		System.out.println(obj);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
}
