package com.sbdj.core.util;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Yly to 2019/8/23 13:20
 * 数据封装
 */
public class CopyObjectUtil {
	
	/**
	 * Created by Yly to 2019/8/23 13:22
	 * 并且过滤掉对象中属性值为空的属性
	 * @param source 源对象
	 * @param target 目标对象
	 * @return void
	 **/
	public static void copyProperties(Object source, Object target) {
		BeanUtils.copyProperties(source, target, getNullPropertyNames(source));
	}

	/**
	 * Created by Yly to 2019/8/23 13:22
	 * 过滤对象中为空的属性字段
	 * @param source 源对象
	 * @return java.lang.String[] 字段数组
	 **/
	private static String[] getNullPropertyNames(Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<>();
		for (PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if (null == srcValue)
				emptyNames.add(pd.getName());
		}
		String[] result = new String[emptyNames.size()];
		return emptyNames.toArray(result);
	}
	
}
