package com.sbdj.core.util;

import com.sbdj.core.exception.BaseException;
import org.springframework.expression.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 日期函数工具
 * </p>
 *
 * @author Yly by Htl
 * @since 2019-11-25
 */
public class DateUtil {
	/**
	 * 字符串格式：yyyy-MM-dd HH:mm:ss.sss
	 */
	public static final String formatter_yyyyMMddHHmmsssss = "yyyy-MM-dd HH:mm:ss.sss";
	/**
	 * 字符串格式：yyyy-MM-dd HH:mm:ss
	 */
	public static final String formatter_yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 字符串格式：yyyy-MM-dd HH:mm
	 */
	public static final String formatter_yyyyMMddHHmm = "yyyy-MM-dd HH:mm";
	/**
	 * 字符串格式：yyyy-MM-dd
	 */
	public static final String formatter_yyyyMMdd = "yyyy-MM-dd";
	/**
	 * 字符串格式：yyyy-MM
	 */
	public static final String formatter_yyyyMM = "yyyy-MM";
	/**
	 * 字符串格式：yyyy
	 */
	public static final String formatter_yyyy = "yyyy";
	/**
	 * 字符串格式：MM-dd
	 */
	public static final String formatter_MMdd = "MM-dd";
	/**
	 * 字符串格式：HH:mm:ss
	 */
	public static final String formatter_HHmmss = "HH:mm:ss";
	/**
	 * 字符串格式：HH:mm
	 */
	public static final String formatter_HHmm = "HH:mm";
	/**
	 * 字符串格式：返回星期X(一、二、三、四、五、六、日)
	 */
	public static final String formatter_EEEE = "EEEE";
	
	/**
	 * @Title: genDayTimeStamp  
	 * @Description:  
	 * @param day 间隔当天的天数
	 * @param date 当前的时间
	 * @return  
	 * @return Long 返回类型
	 */
	public static Long genDayTimeStamp(int day,Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, day);
		// ~某天的时间戳
		Long dayTime = calendar.getTimeInMillis();
		// ~当前时间戳
		Long time = System.currentTimeMillis();
		// ~相隔的时间戳
		Long intervalTime = (dayTime-time);
		return (time + intervalTime);
	}

	/**
	 * @Title: genDayTimeStamp  
	 * @Description:  
	 * @param day 间隔当天的天数
	 * @param date 当前的时间
	 * @return  
	 * @return Long 返回类型
	 */
	public static Date genDayDate(int day,Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, day);
		// ~某天的时间戳
		Long dayTime = calendar.getTimeInMillis();
		// ~当前时间戳
		Long time = System.currentTimeMillis();
		// ~相隔的时间戳
		Long intervalTime = (dayTime-time);
		return new Date((time + intervalTime));
	}
	
	/**
	 * @Title: genDayDate  
	 * @Description: 当天累加多少天，小时，分钟，秒
	 * @param day 天
	 * @param hour 小时
	 * @param minute 分钟
	 * @param second 秒
	 * @return  
	 * @return Date 返回类型
	 */
	public static Date genDayDate(Integer day,Integer hour,Integer minute,Integer second) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, hour == null ? 0 : hour);
		calendar.set(Calendar.MINUTE, minute == null ? 0 : minute);
		calendar.set(Calendar.SECOND, second == null ? 0 : second);
		calendar.add(Calendar.DATE, day);
		// ~某天的时间戳
		Long dayTime = calendar.getTimeInMillis();
		// ~当前时间戳
		Long time = System.currentTimeMillis();
		// ~相隔的时间戳
		Long intervalTime = (dayTime-time);
		return new Date((time + intervalTime));
	}

	/**
	 * @Title: isSameDate  
	 * @Description: 判断两个时间是否是同一天 
	 * @param date1 时间1
	 * @param date2 时间2
	 * @return  
	 * @return boolean 返回类型
	 */
	public static boolean isSameDate(Date date1, Date date2) {
	       Calendar cal1 = Calendar.getInstance();
	       cal1.setTime(date1);
	       Calendar cal2 = Calendar.getInstance();
	       cal2.setTime(date2);
	       boolean isSameYear = cal1.get(Calendar.YEAR) == cal2 .get(Calendar.YEAR);
	       boolean isSameMonth = isSameYear && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
		return isSameMonth && cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * @Title: dateAddhHour  
	 * @Description: 当前时间累加多少个小时的方法
	 * @param hour 小时
	 * @return  
	 * @return Date 返回类型
	 */
	public static Date dateAddHour(int hour) {
		Calendar cal = Calendar.getInstance();   
        cal.setTime(new Date());   
        cal.add(Calendar.HOUR, hour);// 24小时制   
		return cal.getTime();
	}
	
	/**
	 * @Title: getMonthSpace
	 * @Description: 获取两个时间相差多少个月
	 * @param beginTime 开始时间
	 * @param endTime   结束时间
	 * @return
	 * @return int 返回类型
	 */
	public static int getMonthSpace(Date beginTime, Date endTime) {
			Calendar bef = Calendar.getInstance();
			Calendar aft = Calendar.getInstance();
			bef.setTime(beginTime);
			aft.setTime(endTime);
			int result = aft.get(Calendar.MONTH) - bef.get(Calendar.MONTH);
			int month = (aft.get(Calendar.YEAR) - bef.get(Calendar.YEAR)) * 12;
		return Math.abs(month + result);
	}

	/**
	 * @Title: stringToDate
	 * @Description: 字符串转换成时间类型
	 * @param source  时间字符串
	 * @param pattern 时间格式 [yyyy-MM-dd HH:mm:ss/yyyy-MM-dd HH:mm .........]
	 * @return
	 * @return Date 返回类型
	 */
	public static Date stringToDate(String source, String pattern) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Date date;
		try {
			date = simpleDateFormat.parse(source);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}

	/**
	 * @Title: stringToDate
	 * @Description: 字符串转换成时间类型
	 * @param source 时间字符串
	 * @return
	 * @return Date 返回类型
	 */
	public static Date stringToDate(String source) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = simpleDateFormat.parse(source);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}

	/**
	 * @Description: 将日期转换为指定格式的字符串
	 * @param @param date
	 * @param @param format
	 * @param @return
	 * @return String
	 * @author Huang.tianliang
	 * @date 2017-12-4 下午03:58:17
	 */
	public static String dateToString(Date date, String format) {
		String result = "";
		SimpleDateFormat formater = new SimpleDateFormat(format);
		try {
			result = formater.format(date);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return result;
	}

	/**
	 * @Description: 将日期转换为指定格式的字符串
	 * @param @param date
	 * @param @param format
	 * @param @return
	 * @return String
	 * @author Huang.tianliang
	 * @date 2017-12-4 下午03:58:17
	 */
	public static String dateToString(Date date) {
		String result = "";
		SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
		try {
			result = formater.format(date);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return result;
	}

	/**
	 * 
	 * @Description: 将指定格式的字符串转换为日期
	 * @param dateStr
	 * @param format
	 * @return java.util.Date
	 * @author Huang.tianliang
	 * @date 2017-12-4 下午04:18:06
	 */
	public static Date stringtoDate(String dateStr, String format) {
		SimpleDateFormat formater = new SimpleDateFormat(format);
		try {
			formater.setLenient(false);
			return formater.parse(dateStr);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @Description: 获取当前时间的指定格式的字符串
	 * @param format
	 * @return String
	 * @author Huang.tianliang
	 * @date 2017-12-4 下午05:22:04
	 */
	public static String getCurrentDate(String format) {
		return dateToString(new Date(), format);
	}

	/**
	 * 
	 * @Description: 获取给定时间第几个月第一天的指定格式字符串
	 * @param date
	 * @param format
	 * @param month
	 * @return String
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午08:18:27
	 */
	public static String getFirstDayOfMonth(Date date, String format, int month) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, month);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return dateToString(cal.getTime(), format);
	}

	/**
	 * 
	 * @Description: 获取给定时间第几个月最后一天的指定格式字符串
	 * @param date
	 * @param format
	 * @return String
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午08:29:48
	 */
	public static String getLastDayOfMonth(Date date, String format, int month) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DATE, 1);
		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DATE, -1);
		cal.add(Calendar.MONTH, month);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return dateToString(cal.getTime(), format);
	}

	/**
	 * 
	 * @Description: 求指定日期的小时数(0到23)
	 * @param date
	 * @return int
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午08:51:03
	 */
	public static int getHourByDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 
	 * @Description: 求指定日期是一个月中第几天(1到31)
	 * @param date
	 * @return int
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午08:46:32
	 */
	public static int getDayByDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DATE);
	}

	/**
	 * 
	 * @Description: 求指定日期是一年中第几月（1到12）
	 * @param date
	 * @return int
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午08:48:39
	 */
	public static int getMonthByDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH) + 1;
	}

	/**
	 * 
	 * @Description: 求指定日期的年
	 * @param date
	 * @return int
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午08:49:50
	 */
	public static int getYearByDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}

	/**
	 * @Description: 返回两个指定字符串格式的日期相差天数
	 * @param startDate
	 * @param endDate
	 * @param format
	 * @return int
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午09:30:58
	 */
	public static int daysBetween(String startDate, String endDate, String format) {
		List<Long> list = getTwoDate(startDate, endDate, format);
		Long time1 = list.get(0);
		Long time2 = list.get(1);
		Long between_days = Math.abs((time2 - time1) / (1000 * 3600 * 24));
		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * @Description: 返回两个指定字符串格式的日期相差天数
	 * @param startDate 开始时间
	 * @param endDate   结束时间
	 * @return int
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午09:30:58
	 */
	public static int daysBetween(Date startDate, Date endDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(endDate);
		long time2 = cal.getTimeInMillis();
		long between_days = Math.abs((time2 - time1) / (1000 * 3600 * 24));
		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * @Description: 获取两个时间之间的小时差（时间格式必须是yyyy-MM-dd HH yyyy-MM-dd HH:mm yyyy-MM-dd
	 *               HH:mm:ss yyyy-MM-dd HH:mm:ss.sss）
	 * @param startDate
	 * @param endDate
	 * @param format
	 * @return float
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午10:06:35
	 */
	public static float hoursBetween(String startDate, String endDate, String format) {
		List<Long> list = getTwoDate(startDate, endDate, format);
		Long time1 = list.get(0);
		Long time2 = list.get(1);
		return (float) ((time2 - time1) / (1000 * 3600.0));
	}

	/**
	 * 
	 * @Description: 获取两个时间之间的分钟差（时间格式必须是yyyy-MM-dd HH:mm yyyy-MM-dd HH:mm:ss
	 *               yyyy-MM-dd HH:mm:ss.sss）
	 * @param startDate
	 * @param endDate
	 * @param format
	 * @return float
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午10:04:11
	 */
	public static float minutesBetweenFloat(String startDate, String endDate, String format) {
		List<Long> list = getTwoDate(startDate, endDate, format);
		Long time1 = list.get(0);
		Long time2 = list.get(1);
		return (float) ((time2 - time1) / (1000 * 60.0));
	}
	
	/**
	 * @Title: minutesBetweenFloat  
	 * @Description: 获取两个时间之间的分钟差 
	 * @param startDate 开始时间
	 * @param endDate 结束时间
	 * @return  返回两个时间的相差多少分钟
	 * @return int 返回类型
	 */
	public static int minutesBetweenFloat(Date startDate, Date endDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(endDate);
		long time2 = cal.getTimeInMillis();
		float between_minutes = (float) ((time2 - time1) / (1000 * 60.0));
		return (int)between_minutes;
	}
	
	/**
	 * Created by Htl to 2019/06/12 <br/>
	 *  获取两个时间的相差的时间戳
	 * @param startDate 开始时间
	 * @param endDate 结束时间
	 * @return
	 */
	public static int timestamp(Date startDate, Date endDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(endDate);
		long time2 = cal.getTimeInMillis();
		float between_minutes = (float) ((time2 - time1));// / (1000 * 60.0));
		return (int)between_minutes;
	}

	/**
	 * 
	 * @Description: 获取两个时间之间的分钟差（时间格式必须是yyyy-MM-dd HH:mm yyyy-MM-dd HH:mm:ss
	 *               yyyy-MM-dd HH:mm:ss.sss）
	 * @param startDate
	 * @param endDate
	 * @param format
	 * @return int
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午10:02:59
	 */
	public static int minutesBetweenInt(String startDate, String endDate, String format) {
		List<Long> list = getTwoDate(startDate, endDate, format);
		Long time1 = list.get(0);
		Long time2 = list.get(1);
		long between_minutes = Math.abs((time2 - time1) / (1000 * 60));
		return Integer.parseInt(String.valueOf(between_minutes));
	}


	private static List<Long> getTwoDate(String startDate, String endDate, String format) {
		Date start = stringtoDate(startDate, format);
		Date end = stringtoDate(endDate, format);
		if (StrUtil.isNull(start) || StrUtil.isNull(end)) {
			throw BaseException.base("开始日期或结束日期为空");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		Long time1 = cal.getTimeInMillis();
		cal.setTime(end);
		Long time2 = cal.getTimeInMillis();
		List<Long> list = new ArrayList<>();
		list.add(time1);
		list.add(time2);
		return list;
	}

	
	/**
	 * @Title: hourBetween  
	 * @Description: 获取两个时间之间的小时差
	 * @param startDate 开始时间
	 * @param endDate 结束时间
	 * @return  返回两个时间的相差多少分钟
	 * @return int 返回类型
	 */
	public static int hourBetween(Date startDate, Date endDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(endDate);
		long time2 = cal.getTimeInMillis();
		float between_minutes = (float) ((time2 - time1) / (1000 * 60 * 60));
		return (int)between_minutes;
	}

	/**
	 * 
	 * @Description: 求指定字符串格式的天数或月份加减
	 * @param dateStr
	 * @param format  常用格式：yyyy-MM-dd HH:mm:ss&&yyyy-MM-dd
	 * @param counts
	 * @param type    month or day
	 * @return String
	 * @author Huang.tianliang
	 * @date 2017-12-5 上午10:14:25
	 */
	public static String getNewDateStr(String dateStr, String format, int counts, String type) {
		Date date = stringtoDate(dateStr, format);
		if (StrUtil.isNull(date)) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (type != null && type.equals("day")) {
			cal.add(Calendar.DAY_OF_YEAR, counts);
		}
		if (type != null && type.equals("month")) {
			cal.add(Calendar.MONTH, counts);
		}
		return dateToString(cal.getTime(), format);
	}

	/**
	 * @Description: 比较字符串是否和格式完全相同
	 * @param str
	 * @param format
	 * @return boolean
	 * @author Huang.tianliang
	 * 
	 */
	public static boolean isSameFormat(String str, String format) {
		boolean convertSuccess = true;
		SimpleDateFormat formater = new SimpleDateFormat(format);
		try {
			formater.setLenient(false);
			try {
				formater.parse(str);
			} catch (java.text.ParseException e) {
				convertSuccess = false;
			}
		} catch (ParseException e) {
			convertSuccess = false;
		}
		return convertSuccess;
	}

	/**
	 * 
	 * @Description: 比较字符串是否和格式类似相同
	 * @param str
	 * @param format
	 * @return boolean
	 * @author Huang.tianliang
	 * 
	 */
	public static boolean isLikeFormat(String str, String format) {
		boolean convertSuccess = true;
		SimpleDateFormat formater = new SimpleDateFormat(format);
		try {
			formater.setLenient(true);
			try {
				formater.parse(str);
			} catch (java.text.ParseException e) {
				convertSuccess = false;
			}
		} catch (ParseException e) {
			convertSuccess = false;
		}
		return convertSuccess;
	}
	
	/**
	 * @Title: plusMinute  
	 * @Description:当前时间累积分钟的方法
	 * @param minute 分钟
	 * @return  
	 * @return Date 返回类型
	 */
	public static Date plusMinute(int minute) {
		long plus = minute * 60 * 1000;
		long time = System.currentTimeMillis() + plus;
		return new Date(time);
	} 
	
	/**
	 * @Title: compareTwoTimeSizes  
	 * @Description: 判断开始时间是否大于或者小于的方法
	 * @param startTime 开始时间
	 * @param endTime 结束时间
	 * @return  
	 * @return boolean 返回类型
	 */
	public static boolean compareTwoTimeSizeb(Date startTime,Date endTime) {
		//compareTo 方法  是对象比较 大于 1  ,  等于返0  ,  小于 返 -1
		int result = startTime.compareTo(endTime);
		if(result > 0) {
			return true;
		}else {
			return result == 0;
		}
	}
	
	/**
	 * @Title: compareTwoTimeSize
	 * @Description: 判断开始时间是否大于或者小于的方法
	 * @param startTime 开始时间
	 * @param endTime 结束时间
	 * @return  
	 * @return int 返回类型 [{大于： 1}，{等于：0}，{小于：-1}]
	 */
	public static int compareTwoTimeSizei(Date startTime,Date endTime) {
		//compareTo 方法  是对象比较 大于 1  ,  等于返0  ,  小于 返 -1
		return startTime.compareTo(endTime);
	}
	
	/**
	 * 根据参数获取前几个月的数据
	 * @2019年4月24日
	 * @param month
	 * @return
	 */
	public static String findMonthByMonth(int month) {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat(formatter_yyyyMMddHHmmss);
		 // 过去几个月
        c.setTime(new Date());
        c.add(Calendar.MONTH, month);
        Date m = c.getTime();
        return format.format(m); 
	}

	/**
	 * 获取上个月的日期yyyy-MM
	 * @author Yly
	 * @date 2019-11-28 16:39
	 * @param
	 * @return java.lang.String
	 */
	public static String getBeforeMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, -1);
		SimpleDateFormat sdf = new SimpleDateFormat(formatter_yyyyMM);
		return sdf.format(calendar.getTime());
	}
}
