package com.sbdj.core.util;

import javafx.geometry.Point2D;

import java.math.BigDecimal;

/**
 * Created by Yly to 2019/10/17 14:28
 * GPS工具
 */
public class GPSUtil {
    /*
    极半径，从地球中心至南极或北极的距离， 相当于6356.7523km；
    赤道半径，从地球中心到赤道的距离，大约6378.137km；
    平均半径，6371.393km，表示地球中心到地球表面所有各点距离的平均值；
    RE，地球半径，有时被使用作为距离单位, 特别是在天文学和地质学中常用，大概距离是6370.856km；
    */
    private static final double EARTH_RADIUS = 6370.856;// 赤道半径(单位m)

    /**
     * 转化为弧度(rad)
     */
    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }

    /**
     * 基于googleMap中的算法得到两经纬度之间的距离,计算精度与谷歌地图的距离精度差不多，相差范围在0.2米以下（单位m）
     *
     * @param lon1 第一点的精度
     * @param lat1 第一点的纬度
     * @param lon2 第二点的精度
     * @param lat2 第二点的纬度
     * @return 返回的距离，单位km
     */
    public static double calcDistance(double lon1, double lat1, double lon2, double lat2) {
        // 转换为弧度
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        // 计算纬度之差
        double a = radLat1 - radLat2;
        // 计算经度之差
        double b = rad(lon1) - rad(lon2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        // s = Math.round(s * 10000) / 10000;
        /*// 纬度
        double lat1 = Math.toRadians(latitude1);
        double lat2 = Math.toRadians(latitude2);
        // 经度
        double lng1 = Math.toRadians(longitude1);
        double lng2 = Math.toRadians(longitude2);
        // 纬度之差
        double a = lat1 - lat2;
        // 经度之差
        double b = lng1 - lng2;
        // 计算两点距离的公式
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
                Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(b / 2), 2)));
        // 弧长乘地球半径, 返回单位: 千米
        s =  s * EARTH_RADIUS;*/
        return s;

    }


    /**(米勒投影算法）将经纬度转化为平面坐标 （单位m）
     * @param lon 经度
     * @param lat 维度
     * @return
     * @author shaosen
     * @Date 2018年8月24日
     */
    public static Point2D millierConvertion(double lon, double lat)
    {
        double L = 6381372  * Math.PI * 2;//地球周长
        double W=L;// 平面展开后，x轴等于周长
        double H=L/2;// y轴约等于周长一半
        double mill=2.3;// 米勒投影中的一个常数，范围大约在正负2.3之间
        double x = lon * Math.PI / 180;// 将经度从度数转换为弧度
        double y = lat * Math.PI / 180;// 将纬度从度数转换为弧度
        y=1.25 * Math.log( Math.tan( 0.25 * Math.PI + 0.4 * y ) );// 米勒投影的转换
        // 弧度转为实际距离
        x = ( W / 2 ) + ( W / (2 * Math.PI) ) * x;
        y = ( H / 2 ) - ( H / ( 2 * mill ) ) * y;
//         double[] result=new double[2];
//         result[0]=x;
//         result[1]=y;
        return new Point2D(x, y);
    }

    /**
     * Created by Yly to 2019/10/17 17:52
     * 获取两个坐标系的距离 单位(km)
     * @param lon1 经度1
     * @param lat1 纬度1
     * @param lon2 经度2
     * @param lat2 纬度2
     * @return double
     **/
    public static double getDistance(double lon1, double lat1, double lon2, double lat2) {
        double distance = calcDistance(lon1, lat1, lon2, lat2);
        BigDecimal bd = new BigDecimal(distance);
        distance = bd.setScale(3, BigDecimal.ROUND_DOWN).doubleValue();
        return distance;
    }

    public static void main(String[] args) {
        // 蔡丽津
        double lon1 = 117.684950;
        double lat1 = 36.838315;
        // 户雷振
        double lon2 = 117.684949;
        double lat2 = 36.838330;
        // 杨凌云
        double lon3 = 113.366071;
        double lat3 = 22.97815;
        // 科技创新大厦616
        double lon4 = 113.3666;
        double lat4 = 22.978823;
        // 玖嘉久
        double lon5 = 113.381761;
        double lat5 = 22.9878;
        // 我家
        // 112.491211,27.757406
        double lon6 = 112.491211;
        double lat6 = 27.757406;

        // 计算他们之间的距离
        double distance = getDistance(lon4, lat4, lon6, lat6);
        System.out.println("截取3位小数: " + distance + "km");
        if (distance < 0.01) {
            System.out.println("靠的太近");
        }
    }
}
