package com.sbdj.core.util;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.sbdj.core.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yly to 2019/10/21 10:25
 * 商品打标
 */
public class GoodsMarkingUtil {
    private static Logger logger = LoggerFactory.getLogger(GoodsMarkingUtil.class);
    private static final String url = "https://www.dianshangjiaofu.com/api/dabiao/push.html";
    private static final String token = "340dbae54fea5489a96755d5f0512fef1";

    public static String markingGoods(String product_id, String nicks, String keyword) {
        Map<String, String> params = new HashMap<>();
        params.put("token",token);// 密钥
        params.put("product_id", product_id.trim()); // 淘宝商品id
        params.put("keyword", keyword.trim());// 搜索词
        params.put("nicks", nicks.trim());// 旺旺号

        try {
            // 请求第三方接口
            String result = HttpUtil.sendPostResult(url, params);
            logger.info("===>商品打标...：【{}】，打标结果：【{}】",params.toString(),new Gson().toJson(JsonParser.parseString(result).getAsJsonObject()));
            return result;
        } catch (IOException e) {
            logger.error("===>商品打标异常...：异常信息为：【{}】",e.getMessage());
            throw BaseException.base("打标失败");
        }
    }
}
