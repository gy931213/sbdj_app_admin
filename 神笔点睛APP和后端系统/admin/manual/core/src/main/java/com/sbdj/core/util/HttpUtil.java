package com.sbdj.core.util;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Created by Yly to 2019/8/23 16:08
 * Http请求
 */
public class HttpUtil {

    private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    private static String defaultContentEncoding;

    public HttpUtil() {
        defaultContentEncoding = Charset.defaultCharset().name();
    }

    /**
     * 发送GET请求
     * @param urlString  URL地址
     * @return 响应对象
     * @throws IOException
     */
    public void sendGet(String urlString) throws IOException {
        send(urlString, "GET", null, null);
    }

    /**
     * 发送GET请求
     * @param urlString URL地址
     * @param params 参数集合
     * @param propertys 请求属性
     * @return 响应对象
     * @throws IOException
     */
    public void sendGet(String urlString, Map<String, String> params, Map<String, String> propertys) throws IOException {
        send(urlString, "GET", params, propertys);
    }

    /**
     * 发送POST请求
     * @param urlString URL地址
     * @return 响应对象
     * @throws IOException
     */
    public void sendPost(String urlString) throws IOException {
        send(urlString, "POST", null, null);
    }

    /**
     * 发送POST请求
     * @param urlString URL地址
     * @param params 参数集合
     * @return 响应对象
     * @throws IOException
     */
    public void sendPost(String urlString, Map<String, String> params) throws IOException {
        send(urlString, "POST", params, null);
    }

    /**
     * 发送POST请求
     * @param urlString URL地址
     * @param params 参数集合
     * @param propertys 请求属性
     * @return 响应对象
     * @throws IOException
     */
    public void sendPost(String urlString, Map<String, String> params, Map<String, String> propertys) throws IOException {
        send(urlString, "POST", params, propertys);
    }

    /**
     * 默认的响应字符集
     */
    public String getDefaultContentEncoding() {
        return defaultContentEncoding;
    }

    /**
     *  设置默认的响应字符集
     */
    public void setDefaultContentEncoding(String defaultContentEncoding) {
        HttpUtil.defaultContentEncoding = defaultContentEncoding;
    }


    /**
     * 发送GET请求
     * @param urlString  URL地址
     * @param params 参数集合
     * @return 响应对象
     * @throws IOException
     */
    public static void sendGet(String urlString, Map<String, String> params) throws IOException {
        send(urlString, "GET", params, null);
    }

    /**
     * 发送GET请求
     * @param urlString  URL地址
     * @param params 参数集合
     * @return 响应对象
     * @throws IOException
     */
    public static String sendGetResult(String urlString, Map<String, String> params) throws IOException {
        return sendResult(urlString, "GET", params, null);
    }

    /**
     * GET请求
     * @param urlString 地址
     * @param params 参数
     * @param propertys 头部
     * @return 结果
     * @throws IOException
     */
    public static String sendGetResult(String urlString, Map<String, String> params, Map<String, String> propertys) throws IOException {
        return sendResult(urlString, "GET", params, propertys);
    }

    /**
     * 发送POST请求
     * @param urlString URL地址
     * @param params 参数集合
     * @param propertys 请求属性
     * @return 响应对象
     * @throws IOException
     */
    public static String sendPostResult(String urlString, Map<String, String> params, Map<String, String> propertys) throws IOException {
        return sendResult(urlString, "POST", params, propertys);
    }

    /**
     * 发送POST请求
     * @param urlString URL地址
     * @param params 参数集合
     * @return 响应对象
     * @throws IOException
     */
    public static String sendPostResult(String urlString, Map<String, String> params) throws IOException {
        return sendResult(urlString, "POST", params, null);
    }

    /**
     * 发送HTTP请求
     * @param urlString 地址
     * @param method 方法
     * @param parameters 参数
     * @param propertys 头部
     * @return 结果
     * @throws IOException IO异常
     */
    public static String sendResult(String urlString, String method,String parameters, Map<String, String> propertys) throws IOException {
        HttpURLConnection urlConnection = null;
        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod(method);
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        urlConnection.setUseCaches(false);
        urlConnection.getOutputStream().write(parameters.getBytes());
        urlConnection.getOutputStream().flush();
        urlConnection.getOutputStream().close();
        // ~解析响应对象数据
        StringBuffer temp = getResult(urlConnection);
        return temp.toString();
    }

    /**
     * 发送HTTP请求
     * @param urlString 地址
     * @param method 方法
     * @param parameters 参数
     * @param propertys 头部
     * @return 结果
     * @throws IOException IO异常
     */
    private static String sendResult(String urlString, String method, Map<String, String> parameters, Map<String, String> propertys) throws IOException {
        HttpURLConnection urlConnection = null;

        if (method.equalsIgnoreCase("GET") && parameters != null) {
            StringBuffer param = new StringBuffer();
            param.append(urlString);
            for (Entry<String, String> entry : parameters.entrySet()) {
                if (!param.toString().contains("?")) {
                    param.append("?");
                } else {
                    param.append("&");
                }
                param.append(entry.getKey()).append("=").append(entry.getValue());
            }
            urlString = param.toString();
        }

        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod(method);
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        urlConnection.setUseCaches(false);

        if (propertys != null) {
            for (Entry<String, String> entry : propertys.entrySet()) {
                urlConnection.addRequestProperty(entry.getKey(), entry.getValue());
            }
        }


        if (method.equalsIgnoreCase("POST") && parameters != null) {
            StringBuffer param = new StringBuffer();
            for (Entry<String, String> entry : parameters.entrySet()) {
                param.append("&");
                param.append(entry.getKey()).append("=").append(entry.getValue());
            }
            urlConnection.getOutputStream().write(param.toString().getBytes());
            urlConnection.getOutputStream().flush();
            urlConnection.getOutputStream().close();
        }

        // ~解析响应对象数据
        StringBuffer temp = getResult(urlConnection);
        return temp.toString();
    }


    /**
     * 发送HTTP请求
     * @param urlString 地址
     * @param method 方法
     * @param parameters 参数
     * @param propertys 头部
     * @throws IOException
     */
    private static void send(String urlString, String method, Map<String, String> parameters, Map<String, String> propertys) throws IOException {
        HttpURLConnection urlConnection = null;

        if (method.equalsIgnoreCase("GET") && parameters != null) {
            StringBuffer param = new StringBuffer();
            int i = 0;
            for (String key : parameters.keySet()) {
                if (i == 0)
                    param.append("?");
                else
                    param.append("&");
                param.append(key).append("=").append(parameters.get(key));
                i++;
            }
            urlString += param;
        }

        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod(method);
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        urlConnection.setUseCaches(false);

        if (propertys != null)
            for (String key : propertys.keySet()) {
                urlConnection.addRequestProperty(key, propertys.get(key));
            }

        if (method.equalsIgnoreCase("POST") && parameters != null) {
            StringBuffer param = new StringBuffer();
            for (String key : parameters.keySet()) {
                param.append("&");
                param.append(key).append("=").append(parameters.get(key));
            }
            urlConnection.getOutputStream().write(param.toString().getBytes());
            urlConnection.getOutputStream().flush();
            urlConnection.getOutputStream().close();
        }
        makeContent(urlString, urlConnection);
    }

    /**
     * 得到响应对象
     * @param urlString 地址
     * @param urlConnection 连接
     * @throws IOException 异常
     */
    private static void makeContent(String urlString, HttpURLConnection urlConnection) throws IOException {
        try {
            InputStream in = urlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            StringBuffer temp = new StringBuffer();
            String line = bufferedReader.readLine();
            while (line != null) {
                temp.append(line).append("\r\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            String ecod = urlConnection.getContentEncoding();
            if (ecod == null)
                ecod = defaultContentEncoding;
        } catch (IOException e) {
            throw e;
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
    }


    /**
     * 发送GET请求
     * @param url 地址
     * @param params 参数
     * @param headers 头部
     * @return URLConnection 连接
     * @throws Exception 异常
     */
    public static URLConnection sendGetRequest(String url,
                                               Map<String, String> params, Map<String, String> headers)
            throws Exception {
        StringBuilder buf = new StringBuilder(url);
        Set<Entry<String, String>> entrys = null;
        // 如果是GET请求，则请求参数在URL中
        if (params != null && !params.isEmpty()) {
            buf.append("?");
            entrys = params.entrySet();
            for (Entry<String, String> entry : entrys) {
                buf.append(entry.getKey()).append("=")
                        .append(URLEncoder.encode(entry.getValue(), "UTF-8"))
                        .append("&");
            }
            buf.deleteCharAt(buf.length() - 1);
        }
        URL url1 = new URL(buf.toString());
        HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
        conn.setRequestMethod("GET");
        // 设置请求头
        if (headers != null && !headers.isEmpty()) {
            entrys = headers.entrySet();
            for (Entry<String, String> entry : entrys) {
                conn.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        conn.getResponseCode();
        return conn;
    }

    /**
     * 发送POST请求
     * @param url 地址
     * @param params 参数
     * @param headers 头部
     * @return URLConnection 连接
     * @throws Exception 异常
     */
    public static URLConnection sendPostRequest(String url,Map<String, String> params, Map<String, String> headers)
            throws Exception {
        StringBuilder buf = new StringBuilder();
        Set<Entry<String, String>> entrys = null;
        // 如果存在参数，则放在HTTP请求体，形如name=aaa&age=10
        if (params != null && !params.isEmpty()) {
            entrys = params.entrySet();
            for (Entry<String, String> entry : entrys) {
                buf.append(entry.getKey()).append("=")
                        .append(URLEncoder.encode(entry.getValue(), "UTF-8"))
                        .append("&");
            }
            buf.deleteCharAt(buf.length() - 1);
        }
        URL url1 = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        OutputStream out = conn.getOutputStream();
        out.write(buf.toString().getBytes(StandardCharsets.UTF_8));
        if (headers != null && !headers.isEmpty()) {
            entrys = headers.entrySet();
            for (Entry<String, String> entry : entrys) {
                conn.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        conn.getResponseCode(); // 为了发送成功
        return conn;
    }

    /**
     * Created by Yly to 2019/8/23 16:25
     * 将输入流转为字节数组
     * @param inStream 输入流
     * @return byte[]
     **/
    public static byte[] read2Byte(InputStream inStream)throws Exception{
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while( (len = inStream.read(buffer)) !=-1 ){
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();
        return outSteam.toByteArray();
    }
    /**
     * Created by Yly to 2019/8/23 16:25
     * 将输入流转为字符串
     * @param inStream 输入流
     * @return java.lang.String
     **/
    public static String read2String(InputStream inStream)throws Exception{
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while( (len = inStream.read(buffer)) !=-1 ){
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();
        return new String(outSteam.toByteArray(), StandardCharsets.UTF_8);
    }

    /**
     * Created by Yly to 2019/8/23 16:25
     * 根据链接获取输入流的方法
     * @param strUrl 地址
     * @return java.io.InputStream
     **/
    public static InputStream getInputStreamByUrl(String strUrl){
        HttpURLConnection conn = null;
        try {
            URL url = new URL(strUrl);
            conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(20 * 1000);
            final ByteArrayOutputStream output = new ByteArrayOutputStream();
            IOUtils.copy(conn.getInputStream(),output);
            return  new ByteArrayInputStream(output.toByteArray());
        } catch (Exception e) {
            logger.error("===>请求异常，异常信息为：{}",e.getMessage());
        }finally {
            try{
                if (conn != null) {
                    conn.disconnect();
                }
            }catch (Exception e){
                logger.error("===>请求异常，异常信息为：{}",e.getMessage());
            }
        }
        return null;
    }

    private static StringBuffer getResult(HttpURLConnection urlConnection) throws IOException {
        StringBuffer temp = new StringBuffer();
        try {
            InputStream in = urlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            String line = bufferedReader.readLine();
            while (line != null) {
                temp.append(line).append("\r\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            String ecod = urlConnection.getContentEncoding();
            if (ecod == null)
                ecod = defaultContentEncoding;
        } catch (IOException e) {
            throw e;
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return temp;
    }
}
