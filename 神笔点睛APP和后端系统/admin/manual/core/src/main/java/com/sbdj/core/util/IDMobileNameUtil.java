package com.sbdj.core.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yly to 2019/9/16 16:23
 * 身份证、手机和姓名认证
 */
public class IDMobileNameUtil {
    private static Logger logger = LoggerFactory.getLogger(IDMobileNameUtil.class);

    private final static String APPCODE = "0a784fbc1cb64914a08928941e3e6bd0";
    private final static String URL = "http://telvertify.market.alicloudapi.com/lianzhuo/telvertify";

    /// 0	信息匹配	信息匹配
    // 1	信息不匹配	信息不匹配
    // 2	系统无记录	系统无记录
    // 10	身份证号码错误，请输入正确的身份证号码	身份证号码错误，请输入正确的身份证号码
    // 96	交易失败，请稍后重试	交易失败，请稍后重试

    // id 身份证号码
    // name 姓名
    // telnumber 手机号码

    /**
     * Created by Yly to 2019/9/16 16:41
     * 三网实名认证
     * @param id 身份证号
     * @param name 姓名
     * @param telnumber 手机号
     * @return java.lang.String
     **/
    public static String getIDMobileNameInfo(String id, String name, String telnumber) {
        String result = "三网实名认证失败";
        Map<String,String> heads = new HashMap<>();
        heads.put("Content-Type", "application/json; charset=utf-8");
        heads.put("Authorization","APPCODE " + APPCODE);
        Map<String,String> params = new HashMap<>();
        params.put("id", id);
        name = StrUtil.encodeURLStr(name, "utf-8");
        params.put("name", name);
        params.put("telnumber", telnumber);
        try {
            result = HttpUtil.sendGetResult(URL, params, heads);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return result;
        }
        if (StrUtil.isNotNull(result)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonObject json = JsonParser.parseString(result).getAsJsonObject();
            logger.info(gson.toJson(json));
            json = json.getAsJsonObject("resp");
            int code = json.get("code").getAsInt();
            switch (code) {
                case 0:
                    result = "信息匹配";
                    break;
                case 1:
                    result = "信息不匹配";
                    break;
                case 2:
                    result = "系统无记录";
                    break;
                case 10:
                    result = "身份证号码错误";
                    break;
                case 96:
                    result = "交易失败,请稍后操作";
                    break;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        // 模拟数据
        String data = LeftBigParentheses
                +DoubleQuotes+"data"+DoubleQuotes+Colon+LeftBigParentheses
                +DoubleQuotes+"Mobile"+DoubleQuotes+Colon+DoubleQuotes+"15073246701"+DoubleQuotes+Comma
                +DoubleQuotes+"Name"+DoubleQuotes+Colon+DoubleQuotes+"杨凌云"+DoubleQuotes+Comma
                +DoubleQuotes+"id"+DoubleQuotes+Colon+DoubleQuotes+"430381199911022311"+DoubleQuotes
                +RightBigParentheses+Comma
                +DoubleQuotes+"resp"+DoubleQuotes+Colon+LeftBigParentheses
                +DoubleQuotes+"code"+DoubleQuotes+Colon+DoubleQuotes+"0"+DoubleQuotes+Comma
                +DoubleQuotes+"desc"+DoubleQuotes+Colon+DoubleQuotes+"信息匹配"+DoubleQuotes
                +RightBigParentheses
                +RightBigParentheses;

        System.out.println(data);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonObject json = JsonParser.parseString(data).getAsJsonObject();
        json = json.getAsJsonObject("resp");
        // 获取指定数据
        int code = json.get("code").getAsInt();
        String desc = json.get("desc").getAsString();
        System.out.println("code: " + code + " desc: " + desc);
        System.out.println(gson.toJson(json));
    }

    private static final String LeftBigParentheses = "{";
    private static final String RightBigParentheses = "}";
    private static final String LeftBracket = "[";
    private static final String RightBracket = "]";
    private static final String LeftParentheses = "(";
    private static final String RightParentheses = ")";
    private static final String DoubleQuotes = "\"";
    private static final String SingleQuotes = "'";
    private static final String Colon = ":";
    private static final String Comma = ",";
}
