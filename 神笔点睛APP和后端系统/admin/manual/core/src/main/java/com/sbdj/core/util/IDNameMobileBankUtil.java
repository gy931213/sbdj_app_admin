package com.sbdj.core.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yly to 2019/9/27 15:08
 * 银行卡四要素认证
 */
public class IDNameMobileBankUtil {
    private static Logger logger = LoggerFactory.getLogger(IDNameMobileBankUtil.class);

    private final static String APPCODE = "0a784fbc1cb64914a08928941e3e6bd0";
    private final static String URL = "http://lundroid.market.alicloudapi.com/lianzhuo/verifi";

    public static String getIDNameMobileBankInfo(String idCard, String name, String mobile, String bankNum) {
        String result = "银行卡四要素认证失败";
        Map<String,String> heads = new HashMap<>();
        heads.put("Content-Type", "application/json; charset=utf-8");
        heads.put("Authorization","APPCODE " + APPCODE);
        Map<String,String> params = new HashMap<>();
        params.put("acct_pan", bankNum);
        params.put("cert_id", idCard);
        params.put("phone_num", mobile);
        name = StrUtil.encodeURLStr(name, "utf-8");
        params.put("acct_name", name);
        try {
            result = HttpUtil.sendGetResult(URL, params, heads);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return result;
        }
        if (StrUtil.isNotNull(result)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonObject json = JsonParser.parseString(result).getAsJsonObject();
            logger.info(gson.toJson(json));
            json = json.get("resp").getAsJsonObject();
            String code = json.get("code").getAsString();
            switch (code) {
                case "0":
                    result = "信息一致";
                    break;
                case "1":
                    result = "交易失败，请咨询发卡行";
                    break;
                case "4":
                    result = "此卡被没收，请于发卡方联系";
                    break;
                case "5":
                    result = "信息不一致";
                    break;
                case "14":
                    result = "无效卡号";
                    break;
                case "15":
                    result = "此卡无对应发卡方";
                    break;
                case "21":
                    result = "该卡未初始化或睡眠卡";
                    break;
                case "34":
                    result = "作弊卡，吞卡";
                    break;
                case "40":
                    result = "发卡方不支持的交易";
                    break;
                case "41":
                    result = "此卡已经挂失";
                    break;
                case "43":
                    result = "此卡被没收";
                    break;
                case "54":
                    result = "该卡已过期";
                    break;
                case "57":
                    result = "发卡方不允许此交易";
                    break;
                case "62":
                    result = "受限制的卡";
                    break;
                case "68":
                    result = "您的银行卡暂不支持该业务，请咨询发卡行";
                    break;
                case "75":
                    result = "密码错误次数超限";
                    break;
                case "77":
                    result = "您的卡未开通该功能，请在银行网银或柜面开通";
                    break;
                case "96":
                    result = "交易失败，请稍后重试";
                    break;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        String idCard = "430381198707310039";
        String mobile = "";
        String name = "陈诚";
        String bankNum = "6217002930116357542";

        String result = getIDNameMobileBankInfo(idCard, name, mobile, bankNum);
        System.out.println(result);
    }
}
