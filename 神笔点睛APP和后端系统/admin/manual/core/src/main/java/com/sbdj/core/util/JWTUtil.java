package com.sbdj.core.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Yly to 2019/10/14 17:45
 * JWT工具
 */
public class JWTUtil {
    // token过期时间,这个值表示3天过期
    private static final long TOKEN_EXPIRED_TIME = 3 * 24 * 60 * 60 * 1000;
    //private static final long TOKEN_EXPIRED_TIME = 30 * 1000;

    // id
    private static final String FINAL_ID = "id";
    // 账号
    public static final String FINAL_MOBILE = "mobile";
    // 密码
    public static final String FINAL_PASSWORD = "password";
    // 随机关键子
    public static final String FINAL_KEYWORD = "keyword";


    /**
     * 创建jwt
     * @param id
     * @param mobile
     * @param password
     * @param privateKey
     * @return
     */
    public static String createJWT(long id, String keyword,String mobile, String password, PrivateKey privateKey) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(FINAL_ID, id);
        claims.put(FINAL_MOBILE, mobile);
        claims.put(FINAL_PASSWORD, password);
        claims.put(FINAL_KEYWORD, keyword);

        Date now = new Date(System.currentTimeMillis());


        //下面就是在为payload添加各种标准声明和私有声明了
        JwtBuilder builder = Jwts.builder()
                /** 如果有私有声明，一定要先设置这个自己创建的私有的声明，这个是给builder的claim赋值，一旦写在标准的声明赋值之后，就是覆盖了那些标准的声明的 */
                .setClaims(claims)
                /** 设置jti(JWT ID)：是JWT的唯一标识，根据业务需要，这个可以设置为一个不重复的值，主要用来作为一次性token,从而回避重放攻击。*/
                .setId(UUID.randomUUID().toString())
                /** iat: jwt的签发时间 */
                .setIssuedAt(now)
                /** 代表这个JWT的主体，即它的所有人，这个是一个json格式的字符串，可以存放什么userid，roldid之类的，作为什么用户的唯一标志。*/
                .setSubject((String) claims.get("moblie"))
                /** 设置签名使用的签名算法rsa和签名使用的秘钥 */
                .signWith(SignatureAlgorithm.RS512, privateKey);
        /** 设置过期时间 */
        long expMillis = System.currentTimeMillis() + TOKEN_EXPIRED_TIME;
        Date exp = new Date(expMillis);
        builder.setExpiration(exp);
        return builder.compact();
    }


    /**
     * 解析jwt
     * @param token
     * @param publicKey
     * @return
     */
    public static Claims parseJWT(String token, PublicKey publicKey) {
        //签名秘钥，和生成的签名的秘钥一模一样
        Claims claims;
        try {
            claims = Jwts.parser()                     //得到DefaultJwtParser
                    .setSigningKey(publicKey)          //设置签名的秘钥
                    .parseClaimsJws(token).getBody(); //设置需要解析的jwt
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }


    /**
     * @Title: verificationData
     * @Description: 校验数据的方法
     * @param claims 后台数
     * @param pwd 密码
     * @param mobile 账号
     * @param keyword 关键子
     * @return boolean 返回类型
     */
    public static boolean verificationData(Claims claims,String pwd,String mobile,String keyword) {
        String pwd1 =  claims.get(FINAL_PASSWORD) == null ? null : claims.get(FINAL_PASSWORD).toString();
        String mobile1 =  claims.get(FINAL_MOBILE) == null ? null : claims.get(FINAL_MOBILE).toString();
        String keyword1 =  claims.get(FINAL_KEYWORD) == null ? null : claims.get(FINAL_KEYWORD).toString();

        if(pwd1 == null || mobile1 == null || keyword == null) {
            return false;
        }

        if(pwd == null || mobile == null || keyword == null) {
            return false;
        }

        if(!pwd1.equalsIgnoreCase(pwd) || !mobile1.equalsIgnoreCase(mobile) || !keyword1.equalsIgnoreCase(keyword)) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) throws Exception {
        String phone = "15073246701";
        String password = "f51737bb500410667b64ce05b00ec055";
        String keyword = "isB3IDSz1dSCzbRsjh";
        String jwt = JWTUtil.createJWT(1L, keyword, phone, password, RSAUtil.findPrivateKey());
        System.out.println(jwt);
        Claims claims = JWTUtil.parseJWT(jwt, RSAUtil.findPublicKey());
        System.out.println(claims.get("mobile"));
        System.out.println(claims.get("password"));
    }
}
