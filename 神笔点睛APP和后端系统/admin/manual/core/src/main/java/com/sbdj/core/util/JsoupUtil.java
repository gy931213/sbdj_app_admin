package com.sbdj.core.util;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Htl to 2019/06/08 <br/>
 * 爬出工具类
 */
public class JsoupUtil {

	private static Logger logger = LoggerFactory.getLogger(JsoupUtil.class);

	private JsoupUtil() { }

	/**
	 * 过期时间默认是 5 秒.
	 */
	private static final int TIME_OUT = 50000;

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * 尝试链接（验证url是否可用）
	 * 
	 * @param url 请求链接
	 * @return [{是 ：true}，{否 ： false}]
	 */
	private static boolean attempConnect(String url) {
		try {
			Document doc = Jsoup.connect(url).timeout(3 * 1000).get();
			System.out.println(doc);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * 获取连接
	 * 
	 * @param url     请求url
	 * @param params  请求参数
	 * @param headers 请求头
	 * @param charset 字符编码
	 * @return
	 */
	private static Connection getConnection(String url, Map<String, String> params, Map<String, String> headers,
			String charset) {
		Connection conn = Jsoup.connect(url);
		// 设置请求参数
		if (null != params) {
			conn.data(params);
		}
		// 设置请求头
		if (null != headers) {
			conn.headers(headers);
		}
		return conn;
	}

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * get request
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	private static Document get(String url, Integer timeout) throws Exception {
		Connection conn = getConnection(url, null, null, null);
		try {
			if (StrUtil.isNull(timeout)) {
				timeout = TIME_OUT;
			}
			return conn.timeout(timeout).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * get request
	 *
	 * @param url
	 * @param params
	 * @param headers
	 * @param charset
	 * @return
	 * @throws Exception
	 */
	private static Document get(String url, Map<String, String> params, Map<String, String> headers, String charset)
			throws Exception {
		Connection conn = getConnection(url, params, headers, charset);
		try {
			return conn.timeout(5 * 1000).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * post request
	 *
	 * @param url
	 * @param timeout
	 * @return
	 * @throws Exception
	 */
	private static Document post(String url, Integer timeout) throws Exception {
		Connection conn = getConnection(url, null, null, null);
		try {
			if (StrUtil.isNull(timeout)) {
				timeout = TIME_OUT;
			}
			return conn.timeout(timeout).post();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// =========================================Jsoup解析==========================================
	// //

	public static void main(String[] args) throws Exception {
		// "https://detail.tmall.com/item.htm?id=577284665556","https://detail.1688.com/offer/557786986442.html"
		/**String [] strs = new String[]{"https://item.taobao.com/item.htm?id=595440592534","https://detail.tmall.com/item.htm?id=577284665556","https://detail.1688.com/offer/557786986442.html","https://item.jd.com/12784088654.html"};
		  for (int i = 0; i < strs.length; i++) {
			  RespElement pe = analysisShopGoods(strs[i]);
			  System.out.println(pe.toString());
		  }**/
		//Document doc = get("https://item.taobao.com/item.htm?id=595440592534", null);
		 //System.out.println(obj.body());
		//Elements el = doc.select("a[href*='www.taobao.com']");
		//System.out.println(doc);
		//Elements el = doc.select("a:contains('天猫')");
		//System.out.println(el);
		//System.out.println(el.isEmpty());
		//el.select("a[herf]")

		// System.out.println(StrUtil.changeHttp("http://gd1.alicdn.com/imgextra/i4/2018570452/O1CN01mudHzV1FD3WLsiJ5I_!!2018570452.jpg_400x400.jpg"));
		/**String http = StrUtil.changeHttp(
				"//gd1.alicdn.com/imgextra/i4/2018570452/O1CN01mudHzV1FD3WLsiJ5I_!!2018570452.jpg_400x400.jpg");
		InputStream is = HttpUtil.getInputStreamByUrl(http);
         System.out.println(is);**/
        String str = "http://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i3/766439547/O1CN01t2Dkeq2KOZl4CXANj_!!766439547.jpg_60x60q90.jpg";
        String ss = str.substring(0, str.indexOf(".jpg_")+4);
        System.out.println(ss);

        // 阿里巴巴
        RespElement re = analysisShopGoods("https://detail.1688.com/offer/579070736099.html");
        System.out.println(re);
        // 淘宝
        re = analysisShopGoods("https://item.taobao.com/item.htm?id=558019086807");
        System.out.println(re);
        // 天猫
        re = analysisShopGoods("https://detail.tmall.com/item.htm?id=592802678071");
        System.out.println(re);
        // 京东
        re = analysisShopGoods("https://item.jd.com/100006513890.html");
        System.out.println(re);
    }

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * 根据id获取元素的方法
	 *
	 * @param doc
	 * @param id  格式 ：el#id: 元素+ID，比如： div#logo
	 * @return
	 */
	private static Element getElementById(Document doc, String id) {
		return doc.select(id).first();
	}

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * 根据class获取元素的方法
	 *
	 * @param doc
	 * @param classs 格式 ：el.class: 元素+class
	 * @return
	 */
	private static Element getElementByClass(Document doc, String classs) {
		return doc.select(classs).first();
	}

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * 根据class获取元素的方法
	 *
	 * @param el
	 * @param classs 格式 ：el.class: 元素+class
	 * @return
	 */
	private static Element getElementByClass(Element el, String classs) {
		return el.select(classs).first();
	}

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * 验证该元素是否存在方法
	 *
	 * @param doc
	 * @param select 格式：div.class ，示例：div.sn-container (el.class: 元素+class) <br/>
	 * @return [{是 ：true}，{否 ：false}]
	 */
	private static boolean isElement(Document doc, String select) {
		if (null == doc.select(select).first()) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * 根据商品链接解析数据
	 *
	 * @param shopGoodsLink 商品链接
	 * @return
	 * @throws Exception
	 */
	public static RespElement analysisShopGoods(String shopGoodsLink) throws Exception {
		Document doc = null;
		try {
			doc = get(shopGoodsLink, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 判断为空
		if (StrUtil.isNull(doc)) {
			logger.error("===>【爬虫】爬出的数据为空，当前的爬虫请求的链接为：【{}】", shopGoodsLink);
			return null;
		}

		// 淘宝
		if(!doc.select(Platform.PLATFORM_TB.getLabel()+ Platform.PLATFORM_TB.getTarget()).isEmpty()) {
			return RespTaoBao(doc);
		// 天猫
		}else if(!doc.select(Platform.PLATFORM_TM.getLabel()+ Platform.PLATFORM_TM.getTarget()).isEmpty()){
			return RespTianMao(doc);
		// 京东
		}else if(!doc.select(Platform.PLATFORM_JD.getLabel()+ Platform.PLATFORM_JD.getTarget()).isEmpty()) {
			return RespJD(doc);
		// 阿里巴巴
		}else if(!doc.select(Platform.PLATFORM_ALBB.getLabel()+ Platform.PLATFORM_ALBB.getTarget()).isEmpty()) {
			return RespALBB(doc);
		}else {
			return null;
		}


		// 验证是淘宝还是天猫的商品链接.
		// div.sn-container 这个class属性是天猫独有的特性.
		// if 天猫，else 淘宝
		/**if (isElement(doc, "div.sn-container")) {
			return RespTianMao(doc);
		} else {
			return RespTaoBao(doc);
		}**/
	}

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * 解析【淘宝】商品链接数据
	 *
	 * @param doc
	 * @return
	 */
	private static RespElement RespTaoBao(Document doc) {
		JsoupUtil.RespElement resp = new JsoupUtil().new RespElement();
		Element el = getElementById(doc, Platform.PLATFORM_TB.getLabel()+ Platform.PLATFORM_TB.getTarget());
		// 标题
		String title = getElementByClass(el, "h3.tb-main-title").text();
		// 主图
		//String link = getElementById(doc, "img#J_ImgBooth").attr("src");
		String link = getElementById(doc, "ul#J_UlThumb").getElementsByClass("tb-selected").select("img").attr("data-src");
		if(null != link) {
			if (StrUtil.indexOf(link, "https://") || StrUtil.indexOf(link, "http://")) {
				if (link.indexOf(".png_") != -1) {
					link = link.substring(0, link.indexOf(".png_")+4);
				} else if (link.indexOf(".jpg_") != -1) {
					link = link.substring(0, link.indexOf(".jpg_")+4);
				}
			} else {
				if (link.indexOf(".png_") != -1) {
					link = "https:" + link.substring(0, link.indexOf(".png_")+4);
				} else if (link.indexOf(".jpg_") != -1) {
					link = "https:" + link.substring(0, link.indexOf(".jpg_")+4);
				}
			}
		}
		resp.setTitle(title);
		resp.setLink(link);
		resp.setPlatform(Platform.PLATFORM_TB.getName());
		return resp;
	}

	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * 解析【天猫】商品链接数据
	 *
	 * @param doc
	 * @return
	 */
	private static RespElement RespTianMao(Document doc) {
		JsoupUtil.RespElement resp = new JsoupUtil().new RespElement();
		Element el = getElementById(doc, Platform.PLATFORM_TM.getLabel()+ Platform.PLATFORM_TM.getTarget());
		// 标题
		String title = getElementByClass(el, "div.tb-detail-hd").select("h1").text();
		// 主图
		String link = getElementById(doc, "ul#J_UlThumb").select("li").eq(0).select("img").attr("src");
		if(null != link && (StrUtil.indexOf(link, "https://") || StrUtil.indexOf(link, "http://") || StrUtil.indexOf(link, ".jpg_"))) {
			link = "https:" + link.substring(0, link.indexOf(".jpg_")+4);
		}
		resp.setTitle(title);
		resp.setLink(link);
		resp.setPlatform(Platform.PLATFORM_TM.getName());
		return resp;
	}

	/**
	 * Created by Htl to 2019/06/10 <br/>
	 * 解析【京东】商品链接数据
	 * @param doc
	 * @return
	 */
	private static RespElement RespJD(Document doc) {
		JsoupUtil.RespElement resp = new JsoupUtil().new RespElement();
		Element el = getElementByClass(doc, Platform.PLATFORM_JD.getLabel()+ Platform.PLATFORM_JD.getTarget());
		// 标题
		String title = getElementByClass(el, "div.sku-name").text();
		// 主图
		String link = "https:" + getElementById(doc, "div#preview").select("img#spec-img").attr("data-origin");
		resp.setTitle(title);
		resp.setLink(link);
		resp.setPlatform(Platform.PLATFORM_JD.getName());
		return resp;
	}

	/**
	 * Created by Htl to 2019/06/10 <br/>
	 * 解析【阿里巴巴】商品链接数据
	 * @param doc
	 * @return
	 */
	private static RespElement RespALBB(Document doc) {
		JsoupUtil.RespElement resp = new JsoupUtil().new RespElement();
		Element el = getElementById(doc, Platform.PLATFORM_ALBB.getLabel()+ Platform.PLATFORM_ALBB.getTarget());
		// 标题
		String title = getElementByClass(el, "h1.d-title").text();
		// 主图
		String link = getElementByClass(doc, "div.vertical-img").select("img").attr("src");
		resp.setTitle(title);
		resp.setLink(link);
		resp.setPlatform(Platform.PLATFORM_ALBB.getName());
		return resp;
	}
	
	
	/**
	 * Created by Htl to 2019/06/08 <br/>
	 * 数据组装类
	 */
	public class RespElement {
		private String title;
		private String link;
		private String platform;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getLink() {
			return link;
		}

		public void setLink(String link) {
			this.link = link;
		}

		public String getPlatform() {
			return platform;
		}

		public void setPlatform(String platform) {
			this.platform = platform;
		}

		@Override
		public String toString() {
			return "RespElement [title=" + title + ", link=" + link + ", platform=" + platform + "]";
		}

	}

	/**
	 * Created by Htl to 2019/06/10 <br/>
	 * 所属平台
	 */
	enum Platform {
		PLATFORM_TB("淘宝", "TB", "www.taobao.com","tb-detail-bd","div.","class"),
		PLATFORM_TM("天猫", "TM", "www.tmall.com","J_DetailMeta","div#","id"),
		PLATFORM_JD("京东", "JD", "www.jd.com","product-intro","div.","class"),
		PLATFORM_ALBB("阿里巴巴", "ALBB", "www.1688.com","site_content_fluid-box","div#","id");

		// 名称
		private String name;
		// 编码
		private String code;
		// 服务器地址
		private String host;
		// 目标元素
		private String target;
		// 标签
		private String label;
		// 标签类型
		private String labelType;

		// 构造函数私有化
		private Platform() {
		}
		
		/**
		 * Created by Htl to 2019/06/10 <br/>
		 * 
		 * @param name
		 * @param code
		 * @param host
		 * @param target
		 * @param label
		 * @param labelType
		 */
		private Platform(String name, String code, String host,String target,String label,String labelType) {
			this.name = name;
			this.code = code;
			this.host = host;
			this.target = target;
			this.label = label;
			this.labelType = labelType;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}

		public String getTarget() {
			return target;
		}

		public void setTarget(String target) {
			this.target = target;
		}
		
		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getLabelType() {
			return labelType;
		}

		public void setLabelType(String labelType) {
			this.labelType = labelType;
		}
	}

}
