package com.sbdj.core.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * Created by Yly to 2019/10/14 15:12
 * MD5工具
 */
public class MD5Util {

    public static String str2MD5(String strs) {
        /*
         * 加密需要使用JDK中提供的类
         */
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] bs = digest.digest(strs.getBytes());
            /*
             * 加密后的数据是-128 到 127 之间的数字，这个数字也不安全。 取出每个数组的某些二进制位进行某些运算，得到一个新的加密结果
             *
             * 0000 0011 0000 0100 0010 0000 0110 0001 &0000 0000 0000 0000 0000 0000 1111
             * 1111 --------------------------------------------- 0000 0000 0000 0000 0000
             * 0000 0110 0001
             *
             * 把取出的数据转成十六进制数
             */
            for (byte b : bs) {
                int x = b & 255;
                String s = Integer.toHexString(x);
                if (x < 16) {
                    sb.append("0");
                }
                sb.append(s);
            }
        } catch (Exception e) {
            System.out.println("加密失败");
        }
        return sb.toString();
    }

    private final static String[] hexDigits = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d",
            "e", "f" };

    /**
     * 转换字节数组为16进制字串
     * @param b 字节数组
     * @return 16进制字串
     */
    public static String byteArrayToHexString(byte[] b) {
        StringBuilder resultSb = new StringBuilder();
        for (byte aB : b) {
            resultSb.append(byteToHexString(aB));
        }
        return resultSb.toString();
    }

    /**
     * 转换byte到16进制
     * @param b 要转换的byte
     * @return 16进制格式
     */
    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n = 256 + n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }

    /**
     * MD5编码
     * @param origin 原始字符串
     * @return 经过MD5加密之后的结果
     */
    public static String MD5Encode(String origin) {
        String resultString;
        resultString = getResultStr(origin, null);
        return resultString;
    }

    private static String getResultStr(String origin, String resultString) {
        try {
            resultString = origin;
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(resultString.getBytes(StandardCharsets.UTF_8));
            resultString = byteArrayToHexString(md.digest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }

    /**
     * @Description: MD5大写
     */
    public static String encodeToUpperCase(String origin) {
        String resultString;
        resultString = getResultStr(origin, null);
        return resultString.toUpperCase();
    }

    /**
     * @Description: MD5小写
     */
    public static String encodeToLowerCase(String origin) {
        String resultString;
        resultString = getResultStr(origin, null);
        return resultString.toLowerCase();
    }

    public static void main(String[] args) {
        String str = StrUtil.getRandomString(18);
        System.out.println(str);
        String pwd = "123456";
        String keyword = "isB3IDSz1dSCzbRsjh";
        String pwdNew = MD5Util.str2MD5(pwd + keyword);
        System.out.println(pwdNew);
        // be12d8ecbf9e0fe50d7a150385e41116

        // System.out.println(str2MD5(pwd+str));

        //System.out.println((str2MD5(pwd) + str).toUpperCase());
        //System.err.println(encodeToUpperCase(pwd));
        //System.err.println(encodeToLowerCase(pwd));

    }
}
