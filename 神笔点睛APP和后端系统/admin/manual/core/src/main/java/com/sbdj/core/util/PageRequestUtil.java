package com.sbdj.core.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sbdj.core.base.HttpReqParam;

/**
 * <p>
 * 分页参数封装
 * </p>
 *
 * @author Yly
 * @since 2019/11/24 10:29
 */
public class PageRequestUtil {
    private PageRequestUtil() {}
    public static <T> IPage<T> buildPageRequest(HttpReqParam param) {
        IPage<T> iPage = new Page<>();
        int page = param.getPage();
        int size = param.getLimit();
        iPage.setCurrent(page);
        iPage.setSize(size);
        return iPage;
    }
}
