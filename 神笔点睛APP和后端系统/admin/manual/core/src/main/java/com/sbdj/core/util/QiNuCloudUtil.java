package com.sbdj.core.util;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.BatchStatus;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.storage.model.FetchRet;
import com.qiniu.storage.model.FileInfo;
import com.qiniu.storage.persistent.FileRecorder;
import com.qiniu.util.Auth;
import com.qiniu.util.UrlSafeBase64;
import com.sbdj.core.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Yly to 2019/10/24 10:51
 * 七牛云
 */
public class QiNuCloudUtil {
    private final static Logger logger = LoggerFactory.getLogger(QiNuCloudUtil.class);
    // 认证信息
    private final static String accessKey = "DgC8xWT7_ZHIg6R1sXOqN-4afqPi1CuP88Cd8qx1";
    private final static String secretKey = "lLoF-TZs521ShOGXuvgEDK4twhCOBAU0dNtagYUJ";
    // oss存储命名
    private final static String bucket = "sbdj-oss";
    //默认不指定key的情况下，以文件内容的hash值作为文件名
    private final static String key = null;
    // 区域名称：z0 华东  z1 华北  z2 华南  na0 北美  as0 东南亚
    //构造一个带指定 Region 对象的配置类
    private static Configuration cfg = null;
    // 临时域名
    public static final String url = "http://img.sbdj.wang/";

    public static void init(int region) {
        switch (region) {
            case 0:
                cfg = new Configuration(Region.huadong());
                break;
            case 1:
                cfg = new Configuration(Region.huabei());
                break;
            case 2:
                cfg = new Configuration(Region.huanan());
                break;
            case 3:
                cfg = new Configuration(Region.beimei());
                break;
            case 4:
                cfg = new Configuration(Region.xinjiapo());
                break;
            default:
                cfg = new Configuration(Region.region2());
                break;
        }
    }


    // 通过文件路径上传文件
    public static String uploadFile(int region, String accessKey, String secretKey, String bucket, String key, String localFilePath) {
        if (StrUtil.isNull(localFilePath)) {
            throw BaseException.base("文件地址为空");
        }
        init(region);
        UploadManager uploadManager = new UploadManager(cfg);
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);
        DefaultPutRet putRet = upload(uploadManager, key, upToken, localFilePath);
        logger.info("key:【{}】",putRet.key);
        logger.info("hash:【{}】",putRet.hash);
        return putRet.key;
    }

    public static String uploadFile(String key, String localFilePath) {
        return uploadFile(2, accessKey, secretKey, bucket, key, localFilePath);
    }

    // 通过文件字节数组上传文件
    public static String uploadFileByBytes(int region, String accessKey, String secretKey, String bucket, String key, byte[] uploadBytes) {
        if (StrUtil.isNull(uploadBytes)) {
            throw BaseException.base("字节数组为空");
        }
        init(region);
        UploadManager uploadManager = new UploadManager(cfg);
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);
        DefaultPutRet putRet = upload(uploadManager, key, upToken, uploadBytes);
        logger.info("key:【{}】",putRet.key);
        logger.info("hash:【{}】",putRet.hash);
        return putRet.key;
    }

    public static String uploadFileByBytes(String key, byte[] uploadBytes) {
        return uploadFileByBytes(2, accessKey,secretKey, bucket, key, uploadBytes);
    }

    // 通过文件输入流上传文件
    public static String uploadFileByInputStream(int region, String accessKey, String secretKey, String bucket, String key, InputStream inputStream) {
        if (StrUtil.isNull(inputStream)) {
            throw BaseException.base("输入流为空");
        }
        init(region);
        UploadManager uploadManager = new UploadManager(cfg);
        //byte[] uploadBytes = "hello qiniu cloud".getBytes("utf-8");
        //ByteArrayInputStream byteInputStream=new ByteArrayInputStream(uploadBytes);
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);
        DefaultPutRet putRet = upload(uploadManager, key, upToken, inputStream);
        logger.info("key:【{}】",putRet.key);
        logger.info("hash:【{}】",putRet.hash);
        return putRet.key;
    }

    public static String uploadFileByInputStream(String key, InputStream inputStream) {
        return uploadFileByInputStream(2, accessKey, secretKey, bucket, key, inputStream);
    }

    public static String uploadFileByInputStream(Integer region, String accessKey, String secretKey, String bucket, InputStream inputStream) {
        String key = UrlSafeBase64.encodeToString(StrUtil.getUUID());
        return uploadFileByInputStream(region, accessKey, secretKey, bucket, key, inputStream);
    }

    // 支持文件断点续传
    public static String uploadFileByFtp(int region, String accessKey, String secretKey, String bucket, String key, String localFilePath) {
        //如果是Windows情况下，格式是 D:\\qiniu\\test.png
        init(region);
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);
        String localTempDir = Paths.get(System.getenv("java.io.tmpdir"), bucket).toString();
        try {
            //设置断点续传文件进度保存目录
            FileRecorder fileRecorder = new FileRecorder(localTempDir);
            UploadManager uploadManager = new UploadManager(cfg, fileRecorder);
            DefaultPutRet putRet = upload(uploadManager, key, upToken, localFilePath);
            logger.info("key:【{}】",putRet.key);
            logger.info("hash:【{}】",putRet.hash);
            return putRet.key;
        } catch (IOException ex) {
            ex.printStackTrace();
            throw BaseException.base(ex.getMessage());
        }
    }

    public static String uploadFileByFtp(String key, String localFilePath) {
        return uploadFileByFtp(2, accessKey, secretKey, bucket, key, localFilePath);
    }

    private static DefaultPutRet upload(UploadManager uploadManager, String key, String upToken, String localFilePath) {
        return upload(uploadManager, key, upToken, localFilePath, null, null);
    }

    private static DefaultPutRet upload(UploadManager uploadManager, String key, String upToken, byte[] uploadBytes) {
        return upload(uploadManager, key, upToken, null, uploadBytes, null);
    }

    private static DefaultPutRet upload(UploadManager uploadManager, String key, String upToken, InputStream inputStream) {
        return upload(uploadManager, key, upToken, null, null, inputStream);
    }

    private static DefaultPutRet upload(UploadManager uploadManager, String key, String upToken, String localFilePath, byte[] uploadBytes, InputStream inputStream) {
        try {
            Response response = null;
            if (StrUtil.isNotNull(localFilePath)) {
                response = uploadManager.put(localFilePath, key, upToken);
            } else if (StrUtil.isNotNull(uploadBytes)) {
                response = uploadManager.put(uploadBytes, key, upToken);
            } else if (StrUtil.isNotNull(inputStream)) {
                response = uploadManager.put(inputStream, key, upToken,null, null);
            }
            //解析上传成功的结果
            /*DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            logger.info(putRet.key);
            logger.info(putRet.hash);
            return putRet;*/
            return new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
        } catch (QiniuException ex) {
            /*Response r = ex.response;
            System.err.println(r.toString());
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {
                //ignore
            }*/
            throw BaseException.base(ex.error());
        }
    }

    // 获取文件信息
    public static Map<String, String> getFileInfo(int region, String accessKey, String secretKey, String bucket, String key) {
        init(region);
        Map<String,String> map = new HashMap<>();
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            FileInfo fileInfo = bucketManager.stat(bucket, key);
            logger.info("hash:【{}】",fileInfo.hash);
            logger.info("fSize:【{}】",fileInfo.fsize);
            logger.info("mimeType:【{}】",fileInfo.mimeType);
            logger.info("putTime:【{}】",fileInfo.putTime);
            map.put("hash", fileInfo.hash);
            map.put("fsize", String.valueOf(fileInfo.fsize));
            map.put("mimeType", fileInfo.mimeType);
            map.put("putTime", String.valueOf(fileInfo.putTime));
            return map;
        } catch (QiniuException ex) {
            System.err.println(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    public static Map<String, String> getFileInfo(String key) {
        return getFileInfo(2, accessKey, secretKey, bucket, key);
    }

    // 批量获取文件信息
    public static void getFileInfoMore(int region, String accessKey, String secretKey, String bucket, String[] keyList) {
        if (StrUtil.isNull(keyList)) {
            throw BaseException.base("文件列表不能为空");
        } else if (keyList.length > 999) {
            //单次批量请求的文件数量不得超过1000
            throw BaseException.base("[获取文件信息]单次批量请求的文件数量不得超过1000");
        }
        init(region);
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            BucketManager.BatchOperations batchOperations = new BucketManager.BatchOperations();
            batchOperations.addStatOps(bucket, keyList);
            Response response = bucketManager.batch(batchOperations);
            BatchStatus[] batchStatusList = response.jsonToObject(BatchStatus[].class);
            for (int i = 0; i < keyList.length; i++) {
                BatchStatus status = batchStatusList[i];
                String key = keyList[i];
                System.out.print(key+"\t");
                if (status.code == 200) {
                    //文件存在
                    logger.info("hash:【{}】",status.data.hash);
                    logger.info("mimeType:【{}】",status.data.mimeType);
                    logger.info("fSize:【{}】",status.data.fsize);
                    logger.info("putTime:【{}】",status.data.putTime);
                } else {
                    logger.info(status.data.error);
                }
            }
        } catch (QiniuException ex) {
            System.err.println(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    // 更新文件类型
    public static void updateFileMimeType(int region, String accessKey, String secretKey, String bucket, String key, String type) {
        init(region);
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        //修改文件类型
        try {
            bucketManager.changeMime(bucket, key, type);
        } catch (QiniuException ex) {
            logger.info(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    public static void updateFileMimeType(String key, String type) {
        updateFileMimeType(2, accessKey, secretKey, bucket, key, type);
    }

    // 批量更新文件类型
    public static void updateFileMimeType(int region, String accessKey, String secretKey, String bucket, Map<String, String> keyMimeMap) {
        if (StrUtil.isNull(keyMimeMap)) {
            throw BaseException.base("需要修改的文件类型列表为空");
        } else if (keyMimeMap.size() > 999) {
            //单次批量请求的文件数量不得超过1000
            throw BaseException.base("[修改文件类型]单次批量请求的文件数量不得超过1000");
        }
        init(region);
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            BucketManager.BatchOperations batchOperations = new BucketManager.BatchOperations();
            //添加指令
            for (Map.Entry<String, String> entry : keyMimeMap.entrySet()) {
                String key = entry.getKey();
                String newMimeType = entry.getValue();
                batchOperations.addChgmOp(bucket, key, newMimeType);
            }
            Response response = bucketManager.batch(batchOperations);
            BatchStatus[] batchStatusList = response.jsonToObject(BatchStatus[].class);
            int index = 0;
            for (Map.Entry<String, String> entry : keyMimeMap.entrySet()) {
                String key = entry.getKey();
                System.out.print(key + "\t");
                BatchStatus status = batchStatusList[index];
                if (status.code == 200) {
                    logger.info("change mime success");
                } else {
                    logger.info(status.data.error);
                }
                index += 1;
            }
        } catch (QiniuException ex) {
            logger.error(ex.response.toString());
        }
    }

    // 重新命名或移动文件
    public static void renameOrMoveFile(int region, String accessKey, String secretKey, String fromBucket, String toBucket, String fromKey, String toKey) {
        init(region);
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.move(fromBucket, fromKey, toBucket, toKey);
        } catch (QiniuException ex) {
            //如果遇到异常，说明移动失败
            logger.error("code:【{}】",ex.code());
            logger.error(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    public static void renameOrMoveFile(String fromBucket, String toBucket, String fromKey, String toKey) {
        renameOrMoveFile(2, accessKey, secretKey, fromBucket, toBucket, fromKey, toKey);
    }

    // 批量重新命名或移动文件
    public static void renameOrMoveFileMore(int region, String accessKey, String secretKey, String bucket, String fromBucket, String toBucket, String fromKey, String toKey) {
        init(region);
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            //单次批量请求的文件数量不得超过1000
            String[] keyList = new String[]{
                    "qiniu.jpg",
                    "qiniu.mp4",
                    "qiniu.png",
            };
            BucketManager.BatchOperations batchOperations = new BucketManager.BatchOperations();
            for (String key : keyList) {
                batchOperations.addMoveOp(bucket, key, bucket, key + "_move");
            }
            Response response = bucketManager.batch(batchOperations);
            BatchStatus[] batchStatusList = response.jsonToObject(BatchStatus[].class);
            for (int i = 0; i < keyList.length; i++) {
                BatchStatus status = batchStatusList[i];
                String key = keyList[i];
                System.out.print(key + "\t");
                if (status.code == 200) {
                    logger.info("move success");
                } else {
                    logger.info(status.data.error);
                }
            }
        } catch (QiniuException ex) {
            logger.error(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    // 复制文件
    public static void copyFile(int region, String accessKey, String secretKey, String fromBucket, String toBucket, String fromKey, String toKey) {
        init(region);
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.copy(fromBucket, fromKey, toBucket, toKey);
        } catch (QiniuException ex) {
            //如果遇到异常，说明复制失败
            logger.error("code:【{}】",ex.code());
            logger.error(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    public static void copyFile(String fromBucket, String toBucket, String fromKey, String toKey) {
        copyFile(2, accessKey, secretKey, fromBucket, toBucket, fromKey, toKey);
    }

    // 批量复制文件
    public static void copyFileMore(int region, String accessKey, String secretKey, String fromBucket, String toBucket, String fromKey, String toKey) {
        init(region);
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            //单次批量请求的文件数量不得超过1000
            String[] keyList = new String[]{
                    "qiniu.jpg",
                    "qiniu.mp4",
                    "qiniu.png",
            };
            BucketManager.BatchOperations batchOperations = new BucketManager.BatchOperations();
            for (String key : keyList) {
                batchOperations.addCopyOp(bucket, key, bucket, key + "_copy");
            }
            Response response = bucketManager.batch(batchOperations);
            BatchStatus[] batchStatusList = response.jsonToObject(BatchStatus[].class);
            for (int i = 0; i < keyList.length; i++) {
                BatchStatus status = batchStatusList[i];
                String key = keyList[i];
                System.out.print(key + "\t");
                if (status.code == 200) {
                    logger.info("copy success");
                } else {
                    logger.info(status.data.error);
                }
            }
        } catch (QiniuException ex) {
            logger.error(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    // 删除文件
    public static void deleteFile(int region, String accessKey, String secretKey, String bucket, String key) {
        init(region);
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.delete(bucket, key);
        } catch (QiniuException ex) {
            //如果遇到异常，说明删除失败
            logger.error("code:【{}】",ex.code());
            logger.error(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    public static void deleteFile(String key) {
        deleteFile(2, accessKey, secretKey, bucket, key);
    }

    // 批量删除文件
    public static void deleteFileMore(int region, String accessKey, String secretKey, String bucket, String[] keyList) {
        if (StrUtil.isNull(keyList)) {
            throw BaseException.base("文件列表不能为空");
        } else if (keyList.length > 999) {
            //单次批量请求的文件数量不得超过1000
            throw BaseException.base("[删除文件]单次批量请求的文件数量不得超过1000");
        }
        init(region);
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            BucketManager.BatchOperations batchOperations = new BucketManager.BatchOperations();
            batchOperations.addDeleteOp(bucket, keyList);
            Response response = bucketManager.batch(batchOperations);
            BatchStatus[] batchStatusList = response.jsonToObject(BatchStatus[].class);
            for (int i = 0; i < keyList.length; i++) {
                BatchStatus status = batchStatusList[i];
                String key = keyList[i];
                System.out.print(key + "\t");
                if (status.code == 200) {
                    logger.info("delete success");
                } else {
                    logger.info(status.data.error);
                }
            }
        } catch (QiniuException ex) {
            logger.error(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    // 设置或更新文件的生存时间
    public static void updateFileAliveDay(int region, String accessKey, String secretKey, String bucket, String key, int days) {
        init(region);
        //过期天数，该文件在days天后删除
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.deleteAfterDays(bucket, key, days);
        } catch (QiniuException ex) {
            logger.error(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    public static void updateFileAliveDay(String key, int days) {
        updateFileAliveDay(2, accessKey, secretKey, bucket, key, days);
    }

    // 获取空间文件列表
    public static List<Map<String, String>> getFileList(int region, String accessKey, String secretKey, String bucket) {
        init(region);
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map;
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        //文件名前缀
        String prefix = "";
        //每次迭代的长度限制，最大1000，推荐值 1000
        int limit = 1000;
        //指定目录分隔符，列出所有公共前缀（模拟列出目录效果）。缺省值为空字符串
        String delimiter = "";
        //列举空间文件列表
        BucketManager.FileListIterator fileListIterator = bucketManager.createFileListIterator(bucket, prefix, limit, delimiter);
        while (fileListIterator.hasNext()) {
            //处理获取的file list结果
            FileInfo[] items = fileListIterator.next();
            for (FileInfo item : items) {
                logger.info("key:【{}】",item.key);
                logger.info("hash:【{}】",item.hash);
                logger.info("fSize:【{}】",item.fsize);
                logger.info("mimeType:【{}】",item.mimeType);
                logger.info("putTime:【{}】",item.putTime);
                logger.info("endUser:【{}】",item.endUser);
                map = new HashMap<>();
                map.put("hash", item.hash);
                map.put("fsize", String.valueOf(item.fsize));
                map.put("mimeType", item.mimeType);
                map.put("putTime", String.valueOf(item.putTime));
                map.put("endUser", item.endUser);
                list.add(map);
            }
        }
        return list;
    }

    public static List<Map<String, String>> getFileList() {
        return getFileList(2, accessKey, secretKey, bucket);
    }

    // 抓取网络资源到空间
    public static Map<String, String> saveRemoteSrcUrl(int region, String accessKey, String secretKey, String bucket, String remoteSrcUrl) {
        init(region);
        Map<String, String> map = new HashMap<>();
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        //抓取网络资源到空间
        try {
            FetchRet fetchRet = bucketManager.fetch(remoteSrcUrl, bucket, key);
            logger.info("hash:【{}】",fetchRet.hash);
            logger.info("key:【{}】",fetchRet.key);
            logger.info("mimeType:【{}】",fetchRet.mimeType);
            logger.info("fSize:【{}】",fetchRet.fsize);
            map.put("hash", fetchRet.hash);
            map.put("key", fetchRet.key);
            map.put("fsize", String.valueOf(fetchRet.fsize));
            map.put("mimeType", fetchRet.mimeType);
            return map;
        } catch (QiniuException ex) {
            logger.error(ex.response.toString());
            throw BaseException.base(ex.error());
        }
    }

    public static Map<String, String> saveRemoteSrcUrl(String remoteSrcUrl) {
        return saveRemoteSrcUrl(2, accessKey, secretKey, bucket, remoteSrcUrl);
    }

    public static void main(String[] args) {
        //uploadFile(accessKey, secretKey, bucket, key, "F:\\Ylything\\图片\\江南烧酒.png");
        // FuAsN7hMQcpUqS9PgcHtRX8jfsDJ
        // http://pzuvap8q3.bkt.clouddn.com/FuAsN7hMQcpUqS9PgcHtRX8jfsDJ
        //getFileInfo("坠海少女.jpg");
        ///renameOrMoveFile(bucket, bucket, "17.jpg", "坠海少女.jpg");
        //getFileList(accessKey, secretKey, bucket);
        //renameOrMoveFile(bucket, bucket, "FuAsN7hMQcpUqS9PgcHtRX8jfsDJ", "江南烧酒.png");

    }
}
