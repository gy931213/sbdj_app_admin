package com.sbdj.core.util;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yly to 2019/10/13 15:58
 * 公钥-私钥工具
 */
public class RSAUtil {
    // 非对称密钥算法
    private static final String KEY_ALGORITHM = "RSA";
    // 密钥长度，DH算法的默认密钥长度是1024 密钥长度必须是64的倍数，在512到65536位之间
    private static final int KEY_SIZE = 1024;
    // 公钥
    private static final String PUBLIC_KEY = "publicKey";
    // 私钥
    private static final String PRIVATE_KEY = "privateKey";

    // 公钥
    private static final String PUBLIC_KEY_STR = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCVFDUgzzAkgsKtfZbWawoCiJNkkUtoa24ii5fpL15xv+0WMl4ZHbZW8+fOryCnyyQw/hUAxIK0R73axkqpK0v4qdKGG2QYi3th7Kxx6KUZkHtV9a42fytbcR2BDwNfBaLuV0+nV2++Fik61e/HxWjRV2AMBCUm3uy0cDCHs4qdtwIDAQAB";
    // 私钥
    private static final String PRIVATE_KEY_STR = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJUUNSDPMCSCwq19ltZrCgKIk2SRS2hrbiKLl+kvXnG/7RYyXhkdtlbz586vIKfLJDD+FQDEgrRHvdrGSqkrS/ip0oYbZBiLe2HsrHHopRmQe1X1rjZ/K1txHYEPA18Fou5XT6dXb74WKTrV78fFaNFXYAwEJSbe7LRwMIezip23AgMBAAECgYB8VsddHxGTUjCXTQlyuwI1TL5Kgdi4Neo9C9rfFbAbBa3z9nx/TEXZzkLII08ERC1iY3kXE8/EF5YlE4AQCtmoPei7SHmEDBw0C7vviPVsB9/oTlMci3DPymZX1voVhASPpoWSW/Z67yxbOsu7dmM7gQuNhd8/Dzy6Z3xm0v7UIQJBAMZ7fsSWKoJIxA4FJhmKs0ZipJUwl4pH0lmwhyCMgZ6hh/uWWY4FzwjIdpuFesSOc42y9sWyBEChZL4r6KldKCcCQQDAR7G8FYqb3L7ejiPoJUF590VtiDS/jp/JTRJfa1FkKr3rQpDcL4g5kb5mvAs6mwdFEwr7CHm7cw0/AibRw0fxAkAFQfAxtOibWuIWnPxNYOcuzh4d9ZOODij8RzjNG+uzEARSHcFxiaXaTbUWmYqO+lcFQeBXjEegjqXkg7U+B0GxAkBvOMlph3hfAghySfwhK3O9bjf9zLzeVUy+L1PCWByDeWnULPQIOYJt8o0Vdg50vvosjYBbhyKLOFjgKolz6qaxAkA5eacmjst9WmLKigIHEtpZCyBu+gLWr8K4BNp05PcWx9Vn4bS+KtJw1WFqW+tOe2ZLZSxtfMdJ4cHWwnVxgI/z";

    /**
     * Base64 编码/解码器 JDK1.8
     */
    private static Base64.Decoder decoder = Base64.getDecoder();
    private static Base64.Encoder encoder = Base64.getEncoder();

    /**
     * 初始化密钥对
     * @return Map
     */
    public static Map<String, Object> initKey() throws Exception {
        // 实例化密钥生成器
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KEY_ALGORITHM);
        // 初始化密钥生成器
        keyPairGenerator.initialize(KEY_SIZE);
        // 生成密钥对
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        // 甲方公钥
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        // System.out.println("系数："+publicKey.getModulus()+"
        // 加密指数："+publicKey.getPublicExponent());
        // 甲方私钥
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        // System.out.println("系数："+privateKey.getModulus()+"解密指数："+privateKey.getPrivateExponent());
        // 将密钥存储在map中
        Map<String, Object> keyMap = new HashMap<>();
        keyMap.put(PUBLIC_KEY, publicKey);
        keyMap.put(PRIVATE_KEY, privateKey);
        return keyMap;
    }

    /**
     * 私钥加密
     * @param data 待加密数据
     * @param key       密钥
     * @return byte[] 加密数据
     */
    public static byte[] encryptByPrivateKey(byte[] data, byte[] key) throws Exception {
        // 取得私钥
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 生成私钥
        PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        // 数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        return cipher.doFinal(data);
    }

    /**
     * 私钥加密
     * @param data 待加密数据
     * @param key       密钥
     * @return byte[] 加密数据
     */
    public static String encryptByPrivateKeyStr(byte[] data, byte[] key) throws Exception {
        // 取得私钥
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 生成私钥
        PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        // 数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        return encoder.encodeToString(cipher.doFinal(data));
    }

    /**
     * 公钥加密
     * @param data 待加密数据
     * @param key       密钥
     * @return byte[] 加密数据
     */
    public static byte[] encryptByPublicKey(byte[] data, byte[] key) throws Exception {
        // 实例化密钥工厂
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 初始化公钥
        // 密钥材料转换
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);
        // 产生公钥
        PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);
        // 数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        return cipher.doFinal(data);
    }

    /**
     * 公钥加密
     * @param data 待加密数据
     * @return byte[] 加密数据
     */
    public static String encryptByPublicKeyStr(String data) throws Exception {
        // 实例化密钥工厂
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 初始化公钥
        // 密钥材料转换
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(decoder.decode(PUBLIC_KEY_STR));
        // 产生公钥
        PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);
        // 数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        return encoder.encodeToString(cipher.doFinal(data.getBytes()));
    }

    /**
     * 私钥解密
     *
     * @param data 待解密数据
     * @param key  密钥
     * @return byte[] 解密数据
     */
    public static byte[] decryptByPrivateKey(byte[] data, byte[] key) throws Exception {
        // 取得私钥
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 生成私钥
        PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        // 数据解密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher.doFinal(data);
    }

    private static String getDecryptData(String data, String key) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        // 取得私钥
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(decoder.decode(key));
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 生成私钥
        PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        // 数据解密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(decoder.decode(data)));
    }

    /**
     * 私钥解密
     *
     * @param data 待解密数据
     * @param key  密钥
     * @return String 解密数据
     */
    public static String decryptByPrivateKeyStr(String data, String key) throws Exception {
        return getDecryptData(data, key);
    }

    /**
     * 私钥解密
     *
     * @param data 待解密数据
     * @return String 解密数据
     */
    public static String decryptByPrivateKeyStr(String data) throws Exception {
        // 取得私钥
        return getDecryptData(data, PRIVATE_KEY_STR);
    }


    /**
     * 公钥解密
     *
     * @param data 待解密数据
     * @param key  密钥
     * @return byte[] 解密数据
     */
    public static byte[] decryptByPublicKey(byte[] data, byte[] key) throws Exception {
        // 实例化密钥工厂
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 初始化公钥
        // 密钥材料转换
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);
        // 产生公钥
        PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);
        // 数据解密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, pubKey);
        return cipher.doFinal(data);
    }

    /**
     * 取得私钥
     *
     * @param keyMap 密钥map
     * @return byte[] 私钥
     */
    public static byte[] getPrivateKey(Map<String, Object> keyMap) {
        Key key = (Key) keyMap.get(PRIVATE_KEY);
        return key.getEncoded();
    }

    /**
     * 取得公钥
     *
     * @param keyMap 密钥map
     * @return byte[] 公钥
     */
    public static byte[] getPublicKey(Map<String, Object> keyMap) throws Exception {
        Key key = (Key) keyMap.get(PUBLIC_KEY);
        return key.getEncoded();
    }

    /**
     * 取得私钥
     * @param keyMap 密钥map
     * @return String 私钥
     */
    public static String findPrivateKeyStr(Map<String, Object> keyMap) {
        Key key = (Key) keyMap.get(PRIVATE_KEY);
        return encoder.encodeToString(key.getEncoded());
    }

    public static String findPrivateKeyStr() {
        return PRIVATE_KEY_STR;
    }

    /**
     * 取得公钥
     * @return String 公钥
     */
    public static String findPublicKeyStr(Map<String, Object> keyMap) {
        Key key = (Key) keyMap.get(PUBLIC_KEY);
        return encoder.encodeToString(key.getEncoded());
    }

    /**
     * 取得公钥
     * @return String 公钥
     */
    public static String findPublicKeyStr() {
        return PUBLIC_KEY_STR;
    }

    /**
     * @return PrivateKey
     * @Title: findPrivateKey
     * @Description: (根据私钥获取私钥对象)
     */
    public static PrivateKey findPrivateKey() throws Exception {
        // 取得私钥
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(decoder.decode(PRIVATE_KEY_STR));
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 生成私钥
        PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        // 数据解密
        return privateKey;
    }


    /**
     * @return PublicKey
     * @Title: findpublicKey
     * @Description: (根据公钥获取公钥对象)
     */
    public static PublicKey findPublicKey() throws Exception {
        // 实例化密钥工厂
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 初始化公钥
        // 密钥材料转换
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(decoder.decode(PUBLIC_KEY_STR));
        // 产生公钥
        PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);
        return pubKey;
    }


    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        /*// 初始化密钥
        // 生成密钥对
        Map<String, Object> keyMap = RSAUtil.initKey();
        // 公钥
        System.out.println(findPublicKey(keyMap));
        // 私钥
        System.out.println(findPrivateKey(keyMap));*/
        // 公钥
        //byte[] publicKey = decoder.decode("MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIy3FFkLtQRtH72jM8fkEyfiKk6QHRnjshjGMB41e+gHLOwbLlpb+TMaawz1oE4XZ6PhK+26HzXLNjHaysS9gPMCAwEAAQ=="); // RSAKit.getPublicKey(keyMap);
        // byte[] publicKey = b;
        // 私钥
        //String privateKey = "MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAjLcUWQu1BG0fvaMzx+QTJ+IqTpAdGeOyGMYwHjV76Acs7BsuWlv5MxprDPWgThdno+Er7bofNcs2MdrKxL2A8wIDAQABAkBXhT+Yqtlp6V2Gm82HFGp9sgPSXXxcL+0Dz7D5+RYaF/y5eWDbnjapWUkwPvzlxKrB/Cosnsp8HDDv9sC/kJEhAiEA4gIJuZivQ/ZKnjGE/6r1LWdVVHglQtW9W0TlSZqDnJsCIQCfY3eFu3zwEx9+npHReysRIj0CcyBHIrp7rT2MYTI2iQIhAKLeTRvOiB810bgGf42vKVg66ycgHdn4JaksEP/ltYqZAiB87dG5GIONCPAepttUfl37SALZ8LLUDvxsBBXB+k0JQQIgd09YGskN3nmv1vUEfUJYmfZiKnlR91Q0bclckps20S0="; // RSAKit.getPrivateKey(keyMap);
        //System.out.println("公钥：" + encoder.encodeToString(publicKey));
        //System.out.println("私钥：" + encoder.encodeToString(privateKey))

        System.out.println("================密钥对构造完毕,甲方将公钥公布给乙方，开始进行加密数据的传输=============");
        String str = "15073246701&Yly@19991102";
        System.out.println("===========甲方向乙方发送加密数据==============");
        System.out.println("原文:" + str);
        // 甲方进行数据的加密
        byte[] code1 = RSAUtil.encryptByPublicKey(str.getBytes(), decoder.decode(PUBLIC_KEY_STR));
        System.out.println("甲方 使用乙方公钥加密后的数据：" + encoder.encodeToString(code1));
        String data = RSAUtil.encryptByPublicKeyStr(str);
        System.out.println("加密 " + data);
        System.out.println("解密 " + RSAUtil.decryptByPrivateKeyStr(data));
        System.out.println("===========乙方使用甲方提供的公钥对数据进行解密==============");
        // 乙方进行数据的解密
        // byte[] decode1=RSACoder.decryptByPublicKey(code1, publicKey);
        //byte[] decode1 = RSAKit.decryptByPrivateKey();
        System.out.println("乙方解密后的数据：" + new String(RSAUtil.decryptByPrivateKey(code1, decoder.decode(PRIVATE_KEY_STR))) + "");


        /*
         * System.out.println("===========反向进行操作，乙方向甲方发送数据==============");
         *
         * str="乙方向甲方发送数据RSA算法";
         *
         * System.out.println("原文:"+str);
         *
         * //乙方使用公钥对数据进行加密 byte[] code2=RSAKit0.encryptByPublicKey(str.getBytes(),
         * publicKey); System.out.println("===========乙方使用公钥对数据进行加密==============");
         * System.out.println("加密后的数据："+encoder.encodeToString(code2));
         *
         * System.out.println("=============乙方将数据传送给甲方======================");
         * System.out.println("===========甲方使用私钥对数据进行解密==============");
         *
         * //甲方使用私钥对数据进行解密 byte[] decode2=RSAKit0.decryptByPrivateKey(code2,
         * privateKey);
         *
         * System.out.println("甲方解密后的数据："+new String(decode2));
         */

    }
}
