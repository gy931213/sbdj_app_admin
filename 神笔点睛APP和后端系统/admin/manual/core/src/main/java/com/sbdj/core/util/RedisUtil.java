package com.sbdj.core.util;

import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by Yly to 2019/11/8 9:54
 * Redis缓存工具类
 */
public class RedisUtil {
	
	// 通过spring-bean 获取对象
	private static RedisTemplate redisTemplate = BeanUtil.getBean(RedisTemplate.class);

	public static void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
		RedisUtil.redisTemplate = redisTemplate;
	}


	/**
	 * Created by Htl to 2019/07/08 <br/>
	 * 设置保存的数据库到哪个Redis数据库中 
	 * @param num
	 */
	public void setDataBase(int num) {
		LettuceConnectionFactory connectionFactory = (LettuceConnectionFactory) redisTemplate.getConnectionFactory();
		if (connectionFactory != null && num != connectionFactory.getDatabase()) {
			connectionFactory.setDatabase(num);
			redisTemplate.setConnectionFactory(connectionFactory);
			connectionFactory.resetConnection();
		}
	}


	// =============================common============================
	
	/**
	 * 指定缓存失效时间
	 * 
	 * @param key  键
	 * @param time 时间(秒)
	 * @return
	 */
	public static boolean expire(String key, long time) {
		try {
			if (time > 0) {
				redisTemplate.expire(key, time, TimeUnit.SECONDS);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 指定缓存失效时间
	 * 
	 * @param key  键
	 * @param time 时间(秒)
	 * @return
	 */
	public static boolean expire(String key, long time,TimeUnit tu) {
		try {
			if (time > 0) {
				redisTemplate.expire(key, time, tu);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据key 获取过期时间
	 * 
	 * @param key 键 不能为null
	 * @return 时间(秒) 返回0代表为永久有效
	 */
	public static long getExpire(String key) {
		return redisTemplate.getExpire(key, TimeUnit.SECONDS);
	}

	/**
	 * 判断key是否存在
	 * 
	 * @param key 键
	 * @return true 存在 false不存在
	 */
	public static boolean hasKey(String key) {
		try {
			return redisTemplate.hasKey(key);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 删除缓存
	 * 
	 * @param key 可以传一个值 或多个
	 */
	public static void del(String... key) {
		if (key != null && key.length > 0) {
			if (key.length == 1) {
				redisTemplate.delete(key[0]);
			} else {
				redisTemplate.delete(CollectionUtils.arrayToList(key));
			}
		}
	}
	
	/**
	 * Created by Htl to 2019/06/15 <br/>
	 * 删除缓存
	 * @param keys List<String> keys 多个key
	 */
	public static void del(List<String> keys) {
		if (keys.size() > 0) {
			redisTemplate.delete(keys);
		}
	}
	
	// ============================String=============================
	
	/**
	 * 普通缓存获取
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static Object get(String key) {
		return key == null ? null : redisTemplate.opsForValue().get(key);
	}

	/**
	 * 普通缓存放入
	 * 
	 * @param key   键
	 * @param value 值
	 * @return true成功 false失败
	 */
	public static boolean set(String key, Object value) {
		try {
			redisTemplate.opsForValue().set(key, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * 普通缓存放入并设置时间
	 * 
	 * @param key   键
	 * @param value 值
	 * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
	 * @return true成功 false 失败
	 */
	public static boolean set(String key, Object value, long time) {
		try {
			if (time > 0) {
				redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
			} else {
				set(key, value);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 递增
	 * 
	 * @param key 键
	 * @param delta  要增加几(大于0)
	 * @return
	 */
	public static long incr(String key, long delta) {
		if (delta < 0) {
			throw new RuntimeException("递增因子必须大于0");
		}
		return redisTemplate.opsForValue().increment(key, delta);
	}

	/**
	 * 递减
	 * 
	 * @param key 键
	 * @param delta  要减少几(小于0)
	 * @return
	 */
	public static long decr(String key, long delta) {
		if (delta < 0) {
			throw new RuntimeException("递减因子必须大于0");
		}
		return redisTemplate.opsForValue().increment(key, -delta);
	}
	
	// ================================Map=================================
	
	/**
	 * HashGet
	 * 
	 * @param key  键 不能为null
	 * @param item 项 不能为null
	 * @return 值
	 */
	public static Object hget(String key, String item) {
		return redisTemplate.opsForHash().get(key, item);
	}

	/**
	 * 获取hashKey对应的所有键值
	 * 
	 * @param key 键
	 * @return 对应的多个键值
	 */
	public static Map<Object, Object> hmget(String key) {
		return redisTemplate.opsForHash().entries(key);
	}

	/**
	 * HashSet
	 * 
	 * @param key 键
	 * @param map 对应多个键值
	 * @return true 成功 false 失败
	 */
	public static boolean hmset(String key, Map<String, Object> map) {
		try {
			redisTemplate.opsForHash().putAll(key, map);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * HashSet 并设置时间
	 * 
	 * @param key  键
	 * @param map  对应多个键值
	 * @param time 时间(秒)
	 * @return true成功 false失败
	 */
	public static boolean hmset(String key, Map<String, Object> map, long time) {
		try {
			redisTemplate.opsForHash().putAll(key, map);
			if (time > 0) {
				expire(key, time);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 向一张hash表中放入数据,如果不存在将创建
	 * 
	 * @param key   键
	 * @param item  项
	 * @param value 值
	 * @return true 成功 false失败
	 */
	public static boolean hset(String key, String item, Object value) {
		try {
			redisTemplate.opsForHash().put(key, item, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 向一张hash表中放入数据,如果不存在将创建
	 * 
	 * @param key   键
	 * @param item  项
	 * @param value 值
	 * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
	 * @return true 成功 false失败
	 */
	public static boolean hset(String key, String item, Object value, long time) {
		try {
			redisTemplate.opsForHash().put(key, item, value);
			if (time > 0) {
				expire(key, time);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 删除hash表中的值
	 * 
	 * @param key  键 不能为null
	 * @param item 项 可以使多个 不能为null
	 */
	public static void hdel(String key, Object... item) {
		redisTemplate.opsForHash().delete(key, item);
	}

	/**
	 * 判断hash表中是否有该项的值
	 * 
	 * @param key  键 不能为null
	 * @param item 项 不能为null
	 * @return true 存在 false不存在
	 */
	public static boolean hHasKey(String key, String item) {
		return redisTemplate.opsForHash().hasKey(key, item);
	}

	/**
	 * hash递增 如果不存在,就会创建一个 并把新增后的值返回
	 * 
	 * @param key  键
	 * @param item 项
	 * @param by   要增加几(大于0)
	 * @return
	 */
	public static double hincr(String key, String item, double by) {
		return redisTemplate.opsForHash().increment(key, item, by);
	}

	/**
	 * hash递减
	 * 
	 * @param key  键
	 * @param item 项
	 * @param by   要减少记(小于0)
	 * @return
	 */
	public static double hdecr(String key, String item, double by) {
		return redisTemplate.opsForHash().increment(key, item, -by);
	}

	// ============================set=============================
	
	/**
	 * 根据key获取Set中的所有值
	 * 
	 * @param key 键
	 * @return
	 */
	public static Set<Object> sGet(String key) {
		try {
			return redisTemplate.opsForSet().members(key);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 根据value从一个set中查询,是否存在
	 * 
	 * @param key   键
	 * @param value 值
	 * @return true 存在 false不存在
	 */
	public static boolean sHasKey(String key, Object value) {
		try {
			return redisTemplate.opsForSet().isMember(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 将数据放入set缓存
	 * 
	 * @param key    键
	 * @param values 值 可以是多个
	 * @return 成功个数
	 */
	public static long sSet(String key, Object... values) {
		try {
			return redisTemplate.opsForSet().add(key, values);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 将set数据放入缓存
	 * 
	 * @param key    键
	 * @param time   时间(秒)
	 * @param values 值 可以是多个
	 * @return 成功个数
	 */
	public static long sSetAndTime(String key, long time, Object... values) {
		try {
			Long count = redisTemplate.opsForSet().add(key, values);
			if (time > 0)
				expire(key, time);
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 获取set缓存的长度
	 * 
	 * @param key 键
	 * @return
	 */
	public static long sGetSetSize(String key) {
		try {
			return redisTemplate.opsForSet().size(key);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 移除值为value的
	 * 
	 * @param key    键
	 * @param values 值 可以是多个
	 * @return 移除的个数
	 */
	public static long setRemove(String key, Object... values) {
		try {
			Long count = redisTemplate.opsForSet().remove(key, values);
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	// ===============================list=================================

	/**
	 * 获取list缓存的内容
	 * 
	 * @param key   键
	 * @param start 开始
	 * @param end   结束 0 到 -1代表所有值
	 * @return
	 */
	public static List<Object> lGet(String key, long start, long end) {
		try {
			return redisTemplate.opsForList().range(key, start, end);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获取list缓存的长度
	 * 
	 * @param key 键
	 * @return
	 */
	public static long lGetListSize(String key) {
		try {
			return redisTemplate.opsForList().size(key);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 通过索引 获取list中的值
	 * 
	 * @param key   键
	 * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
	 * @return
	 */
	public static Object lGetIndex(String key, long index) {
		try {
			return redisTemplate.opsForList().index(key, index);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 将list放入缓存
	 * 
	 * @param key   键
	 * @param value 值
	 * @return
	 */
	public static boolean lSet(String key, Object value) {
		try {
			redisTemplate.opsForList().rightPush(key, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 将list放入缓存
	 * 
	 * @param key   键
	 * @param value 值
	 * @param time  时间(秒)
	 * @return
	 */
	public static boolean lSet(String key, Object value, long time) {
		try {
			redisTemplate.opsForList().rightPush(key, value);
			if (time > 0)
				expire(key, time);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 将list放入缓存
	 * 
	 * @param key   键
	 * @param value 值
	 * @return
	 */
	public static boolean lSet(String key, List<Object> value) {
		try {
			redisTemplate.opsForList().rightPushAll(key, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 将list放入缓存
	 * 
	 * @param key   键
	 * @param value 值
	 * @param time  时间(秒)
	 * @return
	 */
	public static boolean lSet(String key, List<Object> value, long time) {
		try {
			redisTemplate.opsForList().rightPushAll(key, value);
			if (time > 0)
				expire(key, time);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据索引修改list中的某条数据
	 * 
	 * @param key   键
	 * @param index 索引
	 * @param value 值
	 * @return
	 */
	public static boolean lUpdateIndex(String key, long index, Object value) {
		try {
			redisTemplate.opsForList().set(key, index, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 移除N个值为value
	 * 
	 * @param key   键
	 * @param count 移除多少个
	 * @param value 值
	 * @return 移除的个数
	 */
	public static long lRemove(String key, long count, Object value) {
		try {
			Long remove = redisTemplate.opsForList().remove(key, count, value);
			return remove;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	// ===========================================================
	
	/**
	 * @Title: leftPush  
	 * @Description: 往队列头部插入一个元素
	 * @param key
	 * @param value
	 * @return  
	 * @return long 返回类型
	 */
	public static long leftPush(String key,Object value) {
		try {
			return redisTemplate.opsForList().leftPush(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	
	/**
	 * @Title: leftPush  
	 * @Description: 往队列头部插入一个元素
	 * @param key
	 * @param value
	 * @return  
	 * @return long 返回类型
	 */
	public static long leftPush(String key,String value) {
		try {
			return redisTemplate.opsForList().leftPush(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * @Title: leftPush  
	 * @Description: 往队列头部插入一个元素
	 * @param key
	 * @param value
	 * @return  
	 * @return long 返回类型
	 */
	public static long leftPush(String key,Long value) {
		try {
			return redisTemplate.opsForList().leftPush(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	
	/**
	 * @Title: rightPush  
	 * @Description: 从尾部插入一个元素  
	 * @param key
	 * @param value
	 * @return  
	 * @return long 返回类型
	 */
	public static long rightPush(String key,Object value) {
		try {
			return redisTemplate.opsForList().rightPush(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * @Title: rightPush  
	 * @Description: 从尾部插入一个元素  
	 * @param key
	 * @param value
	 * @return  
	 * @return long 返回类型
	 */
	public static long rightPush(String key,String value) {
		try {
			return redisTemplate.opsForList().rightPush(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	
	/**
	 * @Title: rightPush  
	 * @Description: 从尾部插入一个元素  
	 * @param key
	 * @param value
	 * @return  
	 * @return long 返回类型
	 */
	public static long rightPush(String key,Long value) {
		try {
			return redisTemplate.opsForList().rightPush(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * @Title: leftPop  
	 * @Description: 从队列头部删掉一个元素，并返回被删除元素的值
	 * @param key
	 * @return  
	 * @return Object 返回类型
	 */
	public static Object leftPop (String key) {
		try {
		 return	redisTemplate.opsForList().leftPop(key);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @Title: leftPop  
	 * @Description: 从队列头部删掉一个元素，并返回被删除元素的值
	 * @param key
	 * @return  
	 * @return long 返回类型
	 */
	public static long longLeftPop (String key) {
		try {
		 return	(long) redisTemplate.opsForList().leftPop(key);
		} catch (Exception e) {
			e.printStackTrace();
			return 0L;
		}
	}
	
	/**
	 * @Title: rightPop  
	 * @Description:  从队列尾部删掉一个元素，并返回被删除元素的值
	 * @param key
	 * @return  
	 * @return Object 返回类型
	 */
	public static Object rightPop (String key) {
		try {
		 return	redisTemplate.opsForList().rightPop(key);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @Title: rightPop  
	 * @Description:  从队列尾部删掉一个元素，并返回被删除元素的值
	 * @param key
	 * @return  
	 * @return Object 返回类型
	 */
	public static long longRightPop (String key) {
		try {
			Object obj = redisTemplate.opsForList().rightPop(key);
			if(null!=obj) {
				return  Long.parseLong(obj.toString());
			}
			return	0L;
		} catch (Exception e) {
			e.printStackTrace();
			return 0L;
		}
	}

	/**
	 * @Title: queueLength  
	 * @Description: 返回队列的长度，即里面有多少个元素。不存在key返回0，不为队列类型的key会返回报错。
	 * @param key
	 * @return  
	 * @return Long 返回类型
	 */
	public static Long queueLength(String key) {
		try {
			return redisTemplate.opsForList().size(key);
		} catch (Exception e) {
			e.printStackTrace();
			return 0L;
		}
	}
	
	/**
	 * @Title: startAndEndAnge  
	 * @Description: 返回队列从start到end之间的元素信息  
	 * @param key
	 * @param start
	 * @param end
	 * @return  
	 * @return Object 返回类型
	 */
	public static Object startAndEndAnge(String key,long start,long end) {
		try {
			return redisTemplate.opsForList().range(key, start, end);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @Title: startAdnEndTrim  
	 * @Description: 截取一个队列，只保留指定区间内的元素  
	 * @param key
	 * @param start
	 * @param end  
	 * @return void 返回类型
	 */
	public static void startAdnEndTrim(String key,long start,long end) {
		try {
			redisTemplate.opsForList().trim(key, start, end);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @Title: opsForListRemove  
	 * @Description: 删除队列中的值
	 * @param key 键
	 * @param value 值 
	 * @return void 返回类型
	 */
	public static void opsForListRemove(String key,String value) {
		try {
			redisTemplate.opsForList().remove(key, queueLength(key), value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @Title: opsForListRemove  
	 * @Description: 删除队列中的值
	 * @param key 键
	 * @param value 值 
	 * @return void 返回类型
	 */
	public static void opsForListRemove(String key,Long value) {
		try {
			redisTemplate.opsForList().remove(key, queueLength(key), value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @Title: saveleftPush  
	 * @Description: 根据数据类型保存缓存  
	 * @param key
	 * @param value
	 * @param total  
	 * @return void 返回类型
	 * @throws Exception 
	 */
	public static void saveleftPush(String key,String value,int total) throws Exception {
		// ~先移除队列
		del(key);
		
		// ~再添加到队列中
		for (int i = 0; i < total; i++) {
			if(leftPush(key, value) == 0) {
				throw new Exception("redis service has stop or redis not ....start");
			}
		}
	}

	// ================================HashOps====================
	
	/**
	 * Created by Htl to 2019/05/20 <br/>
	 * HashOps 设置数据
	 * @param key
	 * @param objKey
	 * @param value
	 */
	public static void setBoundHashOps(String key,Object objKey,Object value) {
		redisTemplate.boundHashOps(key).put(key, value);
		//redisTemplate.boundHashOps(key).putIfAbsent(objKey, value)
	}
	
	/**
	 * Created by Htl to 2019/05/20 <br/>
	 * 获取数据
	 * @param key
	 * @param objKey
	 */
	public static Object getBoundHashOps(String key,Object objKey) {
		return redisTemplate.boundHashOps(key).get(objKey);
	}
	
	/**
	 * Created by Htl to 2019/05/20 <br/>
	 * 删除数据
	 * @param key
	 * @param objKey
	 */
	public static void delBoundHashOps(String key,Object objKey) {
		redisTemplate.boundHashOps(key).delete(objKey);
	}
}
