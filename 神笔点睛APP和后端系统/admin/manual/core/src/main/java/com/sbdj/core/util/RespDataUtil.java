package com.sbdj.core.util;

/**
 * Created by Yly to 2019/10/15 17:00
 * 消息响应
 */
public class RespDataUtil<T> extends RespUtil {
    private T data;

    private RespDataUtil() { }

    public RespDataUtil(int code, String msg, T data) {
        super(code, msg);
        this.data = data;
    }

    // --------------------------------- 返回成功 ---------------------------------

    public static RespUtil success() {
        return new RespUtil(0, "success");
    }

    public static RespUtil success(int code) {
        return new RespUtil(code, "");
    }

    public static RespUtil success(String msg) {
        return new RespUtil(0, msg);
    }

    public static RespUtil success(int code, String msg) {
        return new RespUtil(code, msg);
    }

    public static <T> RespDataUtil<T> successData() { return new RespDataUtil<>(0, "success", null); }

    public static <T> RespDataUtil<T> successData(T data) { return new RespDataUtil<>(0, "success", data); }

    public static <T> RespDataUtil<T> successData(int code, String msg) { return new RespDataUtil<>(code, msg, null); }

    public static <T> RespDataUtil<T> successData(int code, String msg, T data) { return new RespDataUtil<>(code, msg, data); }

    // --------------------------------- 返回成功 ---------------------------------

    // --------------------------------- 返回失败 ---------------------------------

    public static RespUtil error() {
        return new RespUtil(-1, "error");
    }

    public static RespUtil error(int code) {
        return new RespUtil(code, "");
    }

    public static RespUtil error(String msg) {
        return new RespUtil(-1, msg);
    }

    public static RespUtil error(int code, String msg) {
        return new RespUtil(code, msg);
    }

    public static <T> RespDataUtil<T> errorData() { return new RespDataUtil<>(-1, "error", null); }

    public static <T> RespDataUtil<T> errorData(T data) { return new RespDataUtil<>(-1, "error", data); }

    public static <T> RespDataUtil<T> errorData(int code, String msg) { return new RespDataUtil<>(code, msg, null); }

    public static <T> RespDataUtil<T> errorData(int code, String msg, T data) { return new RespDataUtil<>(code, msg, data); }

    // --------------------------------- 返回失败 ---------------------------------

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
