package com.sbdj.core.util;

/**
 * Created by Yly to 2019/10/15 16:58
 * 消息响应
 */
public class RespUtil {
    private int code;
    private String msg;

    public RespUtil() {}

    public RespUtil(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
