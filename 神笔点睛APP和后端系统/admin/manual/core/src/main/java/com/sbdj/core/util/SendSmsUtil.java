package com.sbdj.core.util;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 阿里云短信发送工具
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 22:01
 */
public class SendSmsUtil {
    private final static String AccessKeyID = "LTAI4FtwoSjsW5EqQrX3Ba5W";
    private final static String AccessKeySecret = "GnaOB7aevFvZsCnMhaGmO8gRY3Lcqy";

    public static void main(String[] args) {
        //sendSms("15073246701");

    }

    public static String sendSms(String accessKeyID, String accessKeySecret, String phone, String signName, String templateCode, String templateParam) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyID, accessKeySecret);
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        // 地域
        request.putQueryParameter("RegionId", "cn-hangzhou");
        // 需要发送的手机号码
        request.putQueryParameter("PhoneNumbers", phone);
        // 签名
        request.putQueryParameter("SignName", signName);
        // 短信模板
        request.putQueryParameter("TemplateCode", templateCode);
        // 短信模板变量对应的实际值
        request.putQueryParameter("TemplateParam", templateParam);
        try {
            CommonResponse response = client.getCommonResponse(request);
            return response.getData();
        } catch (ClientException e) {
            e.printStackTrace();
            return null;
        }
    }


    // 发送验证码
    public static String sendSms(String phone) {
        // 初始化JSON
        Gson gson = new GsonBuilder().create();
        // 生成固定6位随机整数
        int num = (int)((Math.random()*9+1)*100000);
        // 生成JSON
        Map<String, String> map = new HashMap<>();
        map.put("code", String.valueOf(num));
        String temp = gson.toJson(map);
        String data = sendSms(AccessKeyID, AccessKeySecret, phone, "点睛", "SMS_177241611", temp);
        if (StrUtil.isNull(data)) {
            return null;
        }
        return String.valueOf(num);
    }

    public static String sendSms(String phone, String sigName, String templateCode, String temp) {
        String data = sendSms(AccessKeyID, AccessKeySecret, phone, sigName, templateCode, temp);
        if (StrUtil.isNull(data)) {
            return null;
        }
        return data;
    }

    public static void querySendDetails() {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", AccessKeyID, AccessKeySecret);
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("QuerySendDetails");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumber", "15073246701");
        request.putQueryParameter("SendDate", "20191111");
        request.putQueryParameter("PageSize", "10");
        request.putQueryParameter("CurrentPage", "1");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}
