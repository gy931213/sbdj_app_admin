package com.sbdj.core.util;

/**
 * Created by Yly to 2019/8/21 9:21
 * 店铺类型
 */
public enum  ShopTypeUtil {
    TB(1, "淘宝"),
    TM(2, "天猫"),
    ALBB(3, "阿里巴巴"),
    JD(4, "京东")
    ;

    ShopTypeUtil(int code, String name) {
        this.code = code;
        this.name = name;
    }

    private int code;
    private String name;

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public static ShopTypeUtil getShopType(int code) {
        switch (code) {
            case 1:
                return ShopTypeUtil.TB;
            case 2:
                return ShopTypeUtil.TM;
            case 3:
                return ShopTypeUtil.ALBB;
            case 4:
                return ShopTypeUtil.JD;
        }
        return null;
    }
}
