package com.sbdj.core.util;

/**
 * Created by Yly to 2019/8/19 16:17
 * 通用状态
 */
public enum StatusUtil {
    /** (-1, "已删除") */
    DELETED(-1, "已删除"),
    /** (1, "已启用") */
    ENABLED(1, "已启用"),
    /** (0, "已禁用") */
    DISABLED(0, "已禁用");



    StatusUtil(int code, String name) {
        this.code = code;
        this.name = name;
    }

    private int code;
    private String name;

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
