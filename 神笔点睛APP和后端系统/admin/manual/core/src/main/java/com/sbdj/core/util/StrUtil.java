package com.sbdj.core.util;

import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StrUtil {
	
	private static final String CHAE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	/**
	 * 生成随机的字符串
	 * @param str     要随机的字符
	 * @param length  生成字符长度
	 * @return
	 */
	public static String getReadomChar(String str,int length) {
		if(StrUtil.isBlank(str)) {
			return getRandomString(CHAE,length);	
		}
		return getRandomString(str,length);
	}
	
	/**
	 * 生成指定长度的字符
	 * @param length 生成字符长度
	 * @return
	 */
	public static String getReadomChar(int length) {
		return getRandomString(CHAE,length);
	}
	
	private static String getRandomString(String str,int length) {
	    Random random = new Random();
	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i < length; i++) {
	        int number = random.nextInt(str.length());
	        sb.append(str.charAt(number));
	    }
	    return sb.toString();
	}

	/**
	 *  生成指定length的随机字符串（A-Z，a-z，0-9）
	 * @param length
	 * @return
	 */
	public static String getRandomString(int length) {
	    Random random = new Random();
	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i < length; i++) {
	        int number = random.nextInt(3);
	        long result = 0;
	        switch (number) {
	            case 0:
	                result = Math.round(Math.random() * 25 + 65);
	                sb.append(String.valueOf((char) result));
	                break;
	            case 1:
	                result = Math.round(Math.random() * 25 + 97);
	                sb.append(String.valueOf((char) result));
	                break;
	            case 2:
	                sb.append(String.valueOf(new Random().nextInt(10)));
	                break;
	        }
	    }
	    return sb.toString();
	}
	
	// --------- [is null，is not null] start -----------

    /**
     * Created by Yly to 2019/8/19 9:14
     * 判断对象为空
     * @param obj
     * @return boolean
     **/
	public static boolean isNull(Object obj) {
		return isBlank(obj);
	}

	/**
	 * 判断对象不为空
	 * @param obj
	 * @return 是：true，否：false
	 */
	public static boolean isNotNull(Object obj) {
		return !isBlank(obj);
	}

	/**
	 * Created by Yly to 2019/8/19 9:17
	 * 判断字符为空
	 * @param str
	 * @return boolean
	 **/
    public static boolean isNull(String str) {
        return isBlank(str);
    }

	/**
	 * 判断字符不为空
	 * @param str
	 * @return 是：true，否：false
	 */
	public static boolean isNotNull(String str) {
		return !isBlank(str);
	}

    public static boolean isNull(String... strings) {
        if (null == strings) {
            return true;
        }
        for (String str : strings) {
            if (isBlank(str)) {
                return true;
            }
        }
        return false;
    }

	public static boolean isNotNull(String... strings) {
		if (null == strings) {
			return false;
		}
		for (String str : strings) {
			if (isBlank(str)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 判断对象为空
	 * 
	 * @param obj 对象
	 * @return is null ： true，is not null ：false
	 */
	private static boolean isBlank(Object obj) {
		if (obj == null) {
			return true;
		}
		return false;
	}

	/**
	 * 字符串为 null 或者内部字符全部为 ' ' '\t' '\n' '\r' 这四类字符时返回 true <br/>
	 * 
	 * @param str 字符串
	 * @return
	 */
	private static boolean isBlank(String str) {
		if (str == null) {
			return true;
		}
		int len = str.length();
		if (len == 0) {
			return true;
		}
		for (int i = 0; i < len; i++) {
			switch (str.charAt(i)) {
			case ' ':
			case '\t':
			case '\n':
			case '\r':
				break;
			default:
				return false;
			}
		}
		return true;
	}

	// --------- [is null，is not null] end -------------

	/**
	 * Created by Yly to 2019/8/23 16:02
	 * 判断该字符串中是否包含指定的字符（包含：true，不包含：false）
	 * @param str 字符串
	 * @param strChar 指定字符
	 * @return boolean
	 **/
	public static boolean indexOf(String str, String strChar) {
        return str.contains(strChar);
    }
	
	public static String toCamelCase(String stringWithUnderline) {
		if (stringWithUnderline.indexOf('_') == -1) {
			return stringWithUnderline;
		}
		stringWithUnderline = stringWithUnderline.toLowerCase();
		char[] fromArray = stringWithUnderline.toCharArray();
		char[] toArray = new char[fromArray.length];
		int j = 0;
		for (int i = 0; i < fromArray.length; i++) {
			if (fromArray[i] == '_') {
				// 当前字符为下划线时，将指针后移一位，将紧随下划线后面一个字符转成大写并存放
				i++;
				if (i < fromArray.length) {
					toArray[j++] = Character.toUpperCase(fromArray[i]);
				}
			} else {
				toArray[j++] = fromArray[i];
			}
		}
		return new String(toArray, 0, j);
	}

    /**
     * Created by Yly to 2019/8/24 9:57
     * 去除字符串中的空格、回车、换行符、制表符等
     * @param str 字符串
     * @return java.lang.String
     **/
    public static String replaceSpecialStr(String str) {
        String repl = "";
        if (str!=null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            repl = m.replaceAll(",");
        }
        return repl.replaceAll(",","");
    }

    /**
     *  过滤字符的方法，最终保存右边的字符 。 2019/05/01
     * @param str 要过滤的字符串
     * @param schar 过滤的字符
     * @return
     */
    public static String filterChar(String str,String schar) {
        if(indexOf(str, schar)) {
            int index = str.indexOf(schar);
            return str.substring((index+1));
        }
        return str;
    }

    /**
     * 过滤字符，最终保留左边字符串  2019/05/01
     * @param str  字符串
     * @param schar 要过滤的字符
     * @return
     */
    public static String filterRight(String str,String schar) {
        if(indexOf(str, schar)) {
            int index = str.indexOf(schar);
            return str.substring(0, index);
        }
        return str;
    }

    /**
     * 随机字符串{@value}
     */
    private static final String RANDOM_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final int NUMBER_CODE = 18;

    /**
     *   去除字符串中所有空格（包括首尾、中间）
     * @2019年4月25日
     * @param str 要去除空格的字符串
     * @return
     */
    public static String removeAllBlank(String str) {
        if (isNull(str)) {
            return null;
        }
        return str.replace(" ", "");
    }

    /**
     * Created by Yly to 2019/8/24 9:53
     * 获取指定长度的随机字符串
     * @param length 字符串长度 默认18位
     * @return java.lang.String
     **/
    public static String getRandomString(Integer length) {
        // 判断非空!
        if (length == null) {
            length = NUMBER_CODE;
        }
        StringBuilder sb = new StringBuilder();
        int len = RANDOM_STRING.length();
        for (int i = 0; i < length; i++) {
            sb.append(RANDOM_STRING.charAt(getRandom(len - 1)));
        }
        return sb.toString();
    }

    /**
     * Created by Yly to 2019/8/24 9:53
     * 获取随机数字的方法
     * @param count 数字
     * @return int
     **/
    public static int getRandom(int count) {
        return (int) Math.round(Math.random() * (count));
    }

    /**
     * Created by Yly to 2019/8/24 9:53
     * 获取随机数字的方法
     * @param min 最小
     * @param max 最大
     * @return int
     **/
    public static int getRandom(int min, int max) {
        return (int) (min + (Math.random() * (max - min)));
    }

    /**
     * Created by Yly to 2019/8/24 9:54
     * 获取长度为8的UUID编码
     * @return java.lang.String
     **/
    public static String getUUID() {
        UUID id = UUID.randomUUID();
        String[] idd = id.toString().split("-");
        return idd[0];
    }

    /**
     * Created by Yly to 2019/8/24 9:54
     * 过滤字母
     * @param alphabet 字符串
     * @return java.lang.String
     **/
    public static String filterAlphabet(String alphabet) {
        return alphabet.replaceAll("[A-Za-z]", "");
    }

    /**
     * Created by Yly to 2019/8/24 9:54
     * 过滤数字
     * @param digital 字符串
     * @return java.lang.String
     **/
    public static String filterDigital(String digital) {
        return digital.replaceAll("[0-9]", "");
    }

    /**
     * Created by Yly to 2019/8/24 9:55
     * 过滤汉字
     * @param chin 字符串
     * @return java.lang.String
     **/
    public static String filterChinese(String chin) {
        return chin.replaceAll("[\\u4e00-\\u9fa5]", "");
    }

    /**
     * Created by Yly to 2019/8/24 9:55
     * 过滤 字母、数字、汉字
     * @param character 字符串
     * @return java.lang.String
     **/
    public static String filterAll(String character) {
        return character.replaceAll("[a-zA-Z0-9\\u4e00-\\u9fa5]", "");
    }

    public static String arrayToString(String[] str, Integer count) {
        count = (count - 1);
        StringBuilder sb = getStr(str, count);
        return sb.toString();
    }

    private static StringBuilder getStr(String[] str, Integer count) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length; i++) {
            if (null != count && i <= count) {
                if (i == 0) {
                    sb.append(str[i]);
                } else {
                    sb.append("," + str[i]);
                }
            }
        }
        return sb;
    }

    public static String stringSplitToString(String str, Integer count) {
        StringBuilder sb = new StringBuilder();
        String[] s = str.split(",");
        count = (count - 1);
        sb = getStr(s, count);
        return sb.toString();
    }

    /**
     * Created by Yly to 2019/8/24 9:52
     * 两个json类型的字符串拼接数据的方法
     * @param json1 json1
     * @param json2 json2
     * @return java.lang.String
     **/
    public static String stringJson(String json1, String json2) {
        StringBuilder sb = new StringBuilder();
        if (null != json1 && null != json2 && !json1.isEmpty() && !json2.isEmpty()) {
            int index1 = json1.indexOf("]");
            int index2 = json2.indexOf("[") + 1;
            sb.append(json1.substring(0, index1) + "," + json2.substring(index2, json2.length()));
        }
        return sb.toString();
    }

    /**
     * Created by Yly to 2019/8/24 9:51
     * 生成一个6位不可重复的字符编码是
     * @return java.lang.String
     **/
    public static String unRepeatSixCode() {
        String sixChar = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        Date date = new Date();
        String time = sdf.format(date);
        for (int i = 0; i < time.length() / 2; i++) {
            String singleChar;
            String x = time.substring(i * 2, (i + 1) * 2);
            int b = Integer.parseInt(x);
            if (b < 10) {
                singleChar = Integer.toHexString(Integer.parseInt(x));
            } else if (b >= 10 && b < 36) {
                singleChar = String.valueOf((char) (Integer.parseInt(x) + 55));
            } else {
                singleChar = String.valueOf((char) (Integer.parseInt(x) + 61));
            }
            sixChar = sixChar + singleChar;
        }
        return sixChar;
    }


    /**
     * Created by Yly to 2019/8/24 9:51
     * 把list转换为逗号分隔的字符串
     * @param list
     * @return java.lang.String
     **/
    public static String listToString(List<String> list) {
        StringBuilder sb = new StringBuilder();
        if (list != null && list.size() > 0) {
            //Lambda表达式
            return list.stream().collect(Collectors.joining(","));
        }else {
            return "";
        }
    }

    /**
     * Created by Yly to 2019/8/24 9:50
     * 字符串进行http请求链接转换方法
     * @param http 请求链接
     * @return java.lang.String
     **/
    public static String changeHttp(String http) {
        if(indexOf(http, "http://") || indexOf(http, "https://")) {
            return http;
        }
        if(indexOf(http, "//")) {
            return "http:"+http;
        }else {
            return "http://"+http;
        }
    }

    /**
     * Created by Yly to 2019/8/24 9:50
     * 隐藏银行卡号只显示最后4位
     * @param cardNo 银行卡号
     * @return java.lang.String
     **/
    public static String hideCardNo(String cardNo) {
        if(isNull(cardNo)) {
            return cardNo;
        }
        int length = cardNo.length();
        int afterLength = 4;
        //替换字符串，当前使用“*”
        String replaceSymbol = "*";
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<length; i++) {
            if(i >= (length - afterLength)) {
                sb.append(cardNo.charAt(i));
            } else {
                sb.append(replaceSymbol);
            }
        }
        return sb.toString();
    }

    /**
     * Created by Yly to 2019/9/16 22:26
     * 将字符串转换为URL编码格式的字符串
     * @param str 字符串
     * @param etc 编码
     * @return java.lang.String
     **/
    public static String encodeURLStr(String str, String etc) {
        try {
            return URLEncoder.encode(str, etc);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Created by Yly to 2019/9/16 22:27
     * 将URL编码格式的字符串解码为正常字符串
     * @param str
     * @param etc
     * @return java.lang.String
     **/
    public static String decodeURLStr(String str, String etc) {
        try {
            return URLDecoder.decode(str, etc);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

	// 平台数据封装
    public static String toPlatformData(Integer code) {
        ShopTypeUtil shopType = ShopTypeUtil.getShopType(code);
        if (isNull(shopType)) {
            return "未知";
        }
        switch (shopType) {
            case TB:
                return ShopTypeUtil.TB.getName();
            case TM:
                return ShopTypeUtil.TM.getName();
            case ALBB:
                return ShopTypeUtil.ALBB.getName();
            case JD:
                return ShopTypeUtil.JD.getName();
            default:
                return null;
        }
    }

    /**
     * 将id集合用逗号拼接
     * @author Yly
     * @date 2019/11/21 11:18
     * @param list 集合
     * @param fieldName 字段名
     * @return java.lang.String
     */
    public static <T> String getIds(List<T> list, String fieldName) {
        if (list.size() > 0) {
            StringBuilder ids = new StringBuilder();
            Class<?> aClass;
            Field field;
            for (T t : list) {
                try {
                    aClass = t.getClass();
                    field = aClass.getDeclaredField(fieldName);
                    field.setAccessible(true);
                    ids.append(field.get(t)).append(",");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
            ids.deleteCharAt(ids.lastIndexOf(","));
            return ids.toString();
        }
        return null;
    }

    public static <T> String getIds(List<T> list) {
        if (list.size() > 0) {
            StringBuilder ids = new StringBuilder();
            for (T t : list) {
                ids.append(t).append(",");
            }
            ids.deleteCharAt(ids.lastIndexOf(","));
            return ids.toString();
        }
        return null;
    }

    public static String getToken(Long id) {
        UUID uuid  =  UUID.randomUUID();
        String token = uuid.toString().replaceAll("\\-", "")+id;
        return token.toUpperCase();
    }

    public static List<Integer> getIdsByInteger(String ids) {
        if (isNotNull(ids) && ids.length() > 0) {
            List<Integer> list = new ArrayList<>();
            String[] array = ids.split(",");
            for (String str : array) {
                if (isNotNull(str)) {
                    list.add(Integer.valueOf(str));
                }
            }
            return list;
        }
        return null;
    }

    public static List<Long> getIdsByLong(String ids) {
        if (isNotNull(ids) && ids.length() > 0) {
            List<Long> list = new ArrayList<>();
            String[] array = ids.split(",");
            for (String str : array) {
                if (isNotNull(str)) {
                    list.add(Long.valueOf(str));
                }
            }
            return list;
        }
        return null;
    }

    public static List<String> getIdsByString(String ids) {
        if (isNotNull(ids) && ids.length() > 0) {
            List<String> list = new ArrayList<>();
            String[] arrays = ids.split(",");
            for (String array : arrays) {
                if (StrUtil.isNotNull(array)) {
                    list.add(array);
                }
            }
            return list;
        }
        return null;
    }
}