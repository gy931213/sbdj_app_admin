package com.sbdj.core.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yly to 2019/10/24 16:54
 * 淘宝号检测
 */
public class TBOnlineIdCheckUtil {
    private final static Logger logger = LoggerFactory.getLogger(TBOnlineIdCheckUtil.class);

    private static final String url = "https://www.chadianshang.com";
    private static final String path_check = "/api/check/checkNum";
    private static final String path_get_info = "/api/customer/getCurrentInfo";
    private static final String appKey = "6b9796028179ad0b87fb3666f584ed99";
    private static final String appSecret = "Heros_yly";
    private static Map<String, String> head = null;

    private static void initCertification() {
        head = new HashMap<>();
        head.put("appKey", appKey);
        head.put("appSecret", appSecret);
    }

    public static String searchOnlineId(String onlineId) {
        initCertification();
        Map<String, String> param = new HashMap<>();
        param.put("wangwang", onlineId);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try {
            String result = HttpUtil.sendPostResult(url+path_check, param, head);
            JsonObject json = JsonParser.parseString(result).getAsJsonObject();
            logger.info(gson.toJson(json));
            //logger.info(gson.toJson(JsonParser.parseString(result)));
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static void getTotal() {
        initCertification();
        try {
            String result = HttpUtil.sendGetResult(url+path_get_info, null, head);
            System.out.println(result);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonObject json = JsonParser.parseString(result).getAsJsonObject();
            System.out.println(gson.toJson(json));
            System.out.println(json.get("code").getAsInt());
            JsonObject data = json.get("data").getAsJsonObject();
            System.out.println(data.get("checkCount").getAsInt());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static final String info = "{" +
            "  \"code\": \"0\"," +
            "  \"msg\": \"请求成功\"," +
            "  \"bizStatus\": \"0\"," +
            "  \"data\": {" +
            "    \"buyerGoodNum\": \"4心\"," +
            "    \"gender\": \"男\"," +
            "    \"wangwang\": \"he******\"," +
            "    \"weekCount\": 2," +
            "    \"fox\": \"0\"," +
            "    \"wwcreatedStr\": \"2015-10-04\"," +
            "    \"isOk\": true," +
            "    \"weekCreditAverage\": 0.43," +
            "    \"receivedRate\": \"100%\"," +
            "    \"renZheng\": \"\\u003ctd colspan\\u003d\\\"1\\\"\\u003e\\u003cstrong\\u003e实名认证：\\u003c/strong\\u003e\\u0026nbsp;\\u003cspan style\\u003d\\\"color:green\\\"\\u003e已实名认证\\u003c/span\\u003e\\u0026nbsp;\\u003cspan style\\u003d\\\"color:#ddc18d\\\"\\u003e普通会员\\u003c/span\\u003e\\u0026nbsp;\\u003cspan style\\u003d\\\"color:#fa7c02\\\"\\u003eVIP1会员\\u003c/span\\u003e\\u003c/td\\u003e\"," +
            "    \"sentRate\": \"100.00\"," +
            "    \"sellerTotalNum\": \"非商家\"," +
            "    \"jiangNum\": 0," +
            "    \"countBefore\": 1," +
            "    \"purchaseRecords\": []," +
            "    \"yunBlack\": 0," +
            "    \"taoling\": 4" +
            "  }" +
            "}";

    /*
    buyerGoodNum	    string	买家信誉点
    gender	            string	性别
    fox	                int	    狐狸
    wwcreatedStr	    string	注册时间
    weekCount	        int	    本周被商家查询的次数
    countBefore	        int	    上周被商家查询的次数
    taoling	            int	    淘龄 年为单位
    purchaseRecords	    List	购买记录
    jiangNum	        int	    降权数
    sentRate	        string	发出的好评
    weekCreditAverage	float	买家总周平均
    receivedRate	    string	收到的好评
    yunBlack	        int	    云黑单数量
    renZheng	        string	认证信息
    sellerTotalNum	    string	商家信誉点
    */

    public static void main(String[] args) {
        //getTotal();
        //searchOnlineId("heros_zero");
        System.out.println(info);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonObject json = JsonParser.parseString(info).getAsJsonObject();
        System.out.println(json);
        System.out.println(gson.toJson(json));
        System.out.println("code: " + json.get("code").getAsInt());
        JsonObject data = json.get("data").getAsJsonObject();
        String buyerGoodNumKey = "buyerGoodNum";
        String genderKey = "gender";
        String foxKey = "fox";
        String wwcreatedStrKey = "wwcreatedStr";
        String weekCountKey = "weekCount";
        String countBeforeKey = "countBefore";
        String taolingKey = "taoling";
        String jiangNumkey = "jiangNum";
        String sentRateKey = "sentRate";
        String yunBlackKey = "yunBlack";
        Map<String, String> map = new HashMap<>();
        map.put(buyerGoodNumKey, data.get(buyerGoodNumKey).getAsString());
        map.put(genderKey, data.get(genderKey).getAsString());
        map.put(foxKey, data.get(foxKey).getAsString());
        map.put(wwcreatedStrKey, data.get(wwcreatedStrKey).getAsString());
        map.put(weekCountKey, data.get(weekCountKey).getAsString());
        map.put(countBeforeKey, data.get(countBeforeKey).getAsString());
        map.put(taolingKey, data.get(taolingKey).getAsString());
        map.put(jiangNumkey, data.get(jiangNumkey).getAsString());
        map.put(sentRateKey, data.get(sentRateKey).getAsString());
        map.put(yunBlackKey, data.get(yunBlackKey).getAsString());

        System.out.println(map.get(buyerGoodNumKey));
        String buyerGoodNum = map.get(buyerGoodNumKey);
        System.out.println("等级: " + getLevel("1粉冠"));
        System.out.println("等级: " + getLevel(buyerGoodNum));
    }

    public static int getLevel(String info) {
        for (BuyerReputationLevel level : BuyerReputationLevel.values()) {
            if (level.getInfo().equals(info)) {
                return level.getCode();
            }
        }
       return -1;
    }

    public static int getLevel(BuyerReputationLevel brl) {
        return brl.getCode();
    }

    public enum BuyerReputationLevel {
        lv1(1, "1心"),
        lv2(2, "2心"),
        lv3(3, "3心"),
        lv4(4, "4心"),
        lv5(5, "5心"),
        lv6(6, "1黄钻"),
        lv7(7, "2黄钻"),
        lv8(8, "3黄钻"),
        lv9(9, "4黄钻"),
        lv10(10, "5黄钻"),
        lv11(11, "1黄冠"),
        lv12(12, "2黄冠"),
        lv13(13, "3黄冠"),
        lv14(14, "4黄冠"),
        lv15(15, "5黄冠"),
        lv16(16, "1粉冠"),
        lv17(17, "2粉冠"),
        lv18(18, "3粉冠"),
        lv19(19, "4粉冠"),
        lv20(20, "5粉冠"),
        ;

        private int code;
        private String info;

        BuyerReputationLevel(int code, String info) {
            this.code = code;
            this.info = info;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }
    }
}