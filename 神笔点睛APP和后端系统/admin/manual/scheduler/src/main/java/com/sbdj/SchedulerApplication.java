package com.sbdj;

import com.sbdj.core.base.BaseWebConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * <p>
 * SCHEDULER
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 14:11
 */
@EnableScheduling
@SpringBootApplication
public class SchedulerApplication extends BaseWebConfig {
    public static void main(String[] args) {
        SpringApplication.run(SchedulerApplication.class, args);
    }

    @Override
    protected Docket api() {
        return null;
    }
}
