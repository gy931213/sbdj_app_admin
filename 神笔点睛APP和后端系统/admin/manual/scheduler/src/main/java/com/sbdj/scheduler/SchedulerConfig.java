package com.sbdj.scheduler;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * <p>
 * 任务调度配置
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
@Configuration
public class SchedulerConfig implements SchedulingConfigurer {
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(taskExecutor());
    }
    // 线程池大小
    @Bean
    public Executor taskExecutor() {
        return Executors.newScheduledThreadPool(5);
    }
}
