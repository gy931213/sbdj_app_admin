package com.sbdj.scheduler.member;

import com.sbdj.service.member.service.ISalesmanPercentageLeaderboardService;
import com.sbdj.service.member.service.ISalesmanStatementLeaderboardService;
import com.sbdj.service.system.entity.SysOrganization;
import com.sbdj.service.system.service.ISysOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * 业务员排行榜
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
//@Component
public class SalesmanBillLeadBoardScheduler {
    @Autowired
    private ISalesmanPercentageLeaderboardService iSalesmanPercentageLeaderboardService;
    @Autowired
    private ISalesmanStatementLeaderboardService iSalesmanStatementLeaderboardService;
    @Autowired
    private ISysOrganizationService iOrganizationService;

    //@Scheduled(cron = "0 0 0 0 1/1 ?")
    public void getSalesmanLeaderBoard() {
        List<SysOrganization> list = iOrganizationService.findOrganizationList();
        for (SysOrganization so : list) {
            iSalesmanPercentageLeaderboardService.saveSalesmanPercentageLeaderboard(so.getId());
            iSalesmanStatementLeaderboardService.saveSalesmanStatementLeaderboard(so.getId());
        }
    }
}
