package com.sbdj.scheduler.member;

import com.sbdj.service.member.service.ISalesmanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 业务员
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
@Component
public class SalesmanScheduler {
    private Logger logger = LoggerFactory.getLogger(SalesmanScheduler.class);
    @Autowired
    private ISalesmanService iSalesmanService;

    // 每天凌晨2点自动启用已解封的业务
    @Scheduled(cron = "0 0 2 * * ?")
    public void autoEnableSalesman() {
        logger.info("===>正在启用已解封的业务....start");
        iSalesmanService.autoEnableSalesmanByDisableTime();
        logger.info("===>正在启用已解封的业务....end");
    }

    //@Scheduled(cron = "0 10 5 * * ?")
    public void TimeOut12HourOne(){
        logger.info(">>>>>>>>>每天凌晨5:10点更新业务员的新增额度!");
        iSalesmanService.updateSalesmanTotalCreditsAndSurplusCredit();
        //5天不做自动减额度
        //iSalesmanService.reduceSalesmanCredit();
    }

    //计算业务员的余额，余额来源于，任务佣金
    //@Scheduled(cron = "20 10 2 * * ?")
    public void computeTaskBrokerageToSalesmnBalance() {
        logger.info(">>>>>>>>>计算业务员的余额，余额来源于，任务佣金!");
        // 计算业务员佣金 新
        iSalesmanService.computeTaskBrokerageToSalesmanBalance();
    }

    //计算业务员的余额，余额来源于，业务员所做的任务，业务员上级所得的提成
    //@Scheduled(cron = "20 10 4 * * ?")
    public void computeTaskPercentageToSalesmnBalance() {
        logger.info(">>>>>>>>>计算业务员的余额，余额来源于，业务员所做的任务，业务员上级所得的提成!");
        // 测试业务员提成 新
        iSalesmanService.computeTaskPercentageToSalesmanBalance();
    }

    // 【现付单】每天凌晨5点检查出前一天没有提交的业务员
    // 并且将前一天没有提交的业务员进行禁用
    //@Scheduled(cron = "0 0 5 * * ?")
    public void disablesSalesmanByFactorXFD(){
        logger.info("===>【现付单】检查前一天没有提交任务的业务员.....start");
        iSalesmanService.disablesSalesmanByFactorXFD();
        logger.info("===>【现付单】检查前一天没有提交任务的业务员.....stop");
    }

    //【隔日单】每天凌晨5点检查出前一天没有提交的业务员
    // 并且将前一天没有提交的业务员进行禁用<
    //@Scheduled(cron = "30 11 15 * * ?")
    public void disablesSalesmanByFactorGRD(){
        logger.info("===>【隔日单】检查前一天没有提交任务的业务员.....start");
        iSalesmanService.disablesSalesmanByFactorGRD();
        logger.info("===>【隔日单】检查前一天没有提交任务的业务员.....stop");
    }
}
