package com.sbdj.scheduler.task;

import com.sbdj.service.task.service.ITaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 任务
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
@Component
public class TaskScheduler {
    private Logger logger = LoggerFactory.getLogger(TaskScheduler.class);
    @Autowired
    private ITaskService iTaskService;

    // 终止之前的任务
    @Scheduled(cron = "0 20 0 * * ?")
    public void stopTask() {
        logger.info(">>>>>>>>>>>>>>>>>>>>>> 开始终止任务 <<<<<<<<<<<<<<<<<<<<<<<<");
        iTaskService.schedulerStopTask();
        logger.info(">>>>>>>>>>>>>>>>>>>>>> 结束终止任务 <<<<<<<<<<<<<<<<<<<<<<<<");
    }

    // 获取未完成的任务
    @Scheduled(cron = "0 1 0 * * ?")
    public void undoneTask() {
        logger.info(">>>>>>>>>>>>>>>>>>>>>> 开始获取未完成任务 <<<<<<<<<<<<<<<<<<<<<<<<");
        iTaskService.updateUndoneTaskByDays();
        logger.info(">>>>>>>>>>>>>>>>>>>>>> 结束获取未完成任务 <<<<<<<<<<<<<<<<<<<<<<<<");
    }
}
