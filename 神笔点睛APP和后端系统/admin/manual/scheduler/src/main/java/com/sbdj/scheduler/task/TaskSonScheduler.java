package com.sbdj.scheduler.task;

import com.sbdj.core.constant.Status;
import com.sbdj.core.util.DateUtil;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.service.ITaskSonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * 子任务
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
@Component
public class TaskSonScheduler {
    private Logger logger = LoggerFactory.getLogger(TaskSonScheduler.class);
    @Autowired
    private ITaskSonService iTaskSonService;

    // 处理隔日单忘记首次传图,导致明天无法正常操作
    @Scheduled(cron = "0 1 0 * * ?")
    public void doGRDTaskForget() {
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>开始处理隔日单<<<<<<<<<<<<<<<<<<<<<<<<<");
        // 获取昨天未提交的隔日单
        List<TaskSon> list = iTaskSonService.searchGRDNotSubmitYesterday();
        if (list.size() > 0) {
            logger.info("昨日总共有{}个隔日单没有提交",list.size());
            for (TaskSon taskSon : list) {
                // 隔日单第一次传图，将过期时间更新为第二天的中午12点13分14秒
                taskSon.setDeadlineTime(DateUtil.genDayDate(0, 12, 13, 14));
                taskSon.setStatus(Status.STATUS_ACTIVITY);
                taskSon.setImages("[]");
                iTaskSonService.updateTaskSon(taskSon);
            }
        }
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>结束处理隔日单<<<<<<<<<<<<<<<<<<<<<<<<<");
    }

    // 更新是否超时30分钟（任务调度）
    // 每1分钟执行一次 cron="0 */1 * * * ?"
    //@Scheduled(cron="0 */1 * * * ?")
    public void taskSonIsTimeOut30Minute() {
        iTaskSonService.updateTaskSonIsTimeOut30Minute();
    }

    // 更新是否超时6小时 （任务调度）
    // 每30分钟执行一次 cron="0 0/30 * * * ?"
    //@Scheduled(cron="0 0/30 * * * ?")
    public void taskSonIsTimeOut6Hour() {
        iTaskSonService.updateTaskSonIsTimeOut6Hour();
    }

    // 获取已经超时的任务数据返回到任务中
    // 每5分钟执行一次 cron="0 */5 * * * ?"
    //@Scheduled(cron="0 */1 * * * ?")
    public void backTask() {
        iTaskSonService.findTaskSonIsTimeOut();
    }
}
