package com.sbdj;

import com.sbdj.core.base.BaseWebConfig;
import com.sbdj.core.constant.Version;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * <p>
 * 查小号启动器
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 16:00
 */
@SpringBootApplication
public class SearchApplication extends BaseWebConfig {
    public static void main(String[] args) {
        SpringApplication.run(SearchApplication.class, args);
    }

    @Bean
    @Override
    protected Docket api() {
        return super.api("SEARCH", 9096, Version.V3, "com.sbdj.search");
    }

    // ======================解决跨域问题======================
    private CorsConfiguration buildConfig() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*"); // 1允许任何域名使用
        corsConfiguration.addAllowedHeader("*"); // 2允许任何头
        corsConfiguration.addAllowedMethod("*"); // 3允许任何方法（post、get等）
        return corsConfiguration;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", buildConfig()); // 4
        return new CorsFilter(source);
    }
    // ======================解决跨域问题======================
}
