package com.sbdj;

import com.sbdj.core.base.BaseLogAspect;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 查小号日志记录
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 16:04
 */
@Aspect
@Component
public class SearchLogAspect extends BaseLogAspect {
    private static Logger logger = LoggerFactory.getLogger(SearchLogAspect.class);

    // 定义切面
    @Pointcut("execution(public * com.sbdj.search.controller.*.*(..))")
    public void searchLog() {}

    // 方法开始时执行
    @Before("searchLog()")
    public void deBefore(JoinPoint joinPoint) {
        // 方法开始时执行.....
    }

    // 方法处理完请求,返回内容时执行
    @AfterReturning(returning = "obj", pointcut = "searchLog()")
    public void doAfterReturning(Object obj) {
        // 方法的返回值
    }

    // 方法异常时执行
    @AfterThrowing(value = "execution(public * com.sbdj.service..*.*(..)))", throwing = "e")
    public void doThrows(JoinPoint jp, Exception e) {
        this.initLog(logger);
        this.getThrows(jp, e);
    }

    // 后置最终通知, final增强，不管是抛出异常或者正常退出都会执行
    @After("searchLog()")
    public void after(JoinPoint jp) {
        // 方法最后执行.....
    }

    // 环绕通知,环绕增强
    @Around("searchLog()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        this.initLog(logger);
        return this.doLog(pjp);
    }
}
