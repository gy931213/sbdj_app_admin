package com.sbdj.search;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * TODO
 * </p>
 *
 * @author Yly
 * @since 2019-12-12
 */
@Component
@EnableScheduling
public class WebSocketCheck {
    // 检测有效连接
    @Scheduled(cron = "0 0/1 * * * ?")
    public void checkConnect() {
        List<WebSocketSubject> list = WebSocketSubject.Holder.getSubjects();
        if (list == null || list.size() < 1) {
            return;
        }
        for (WebSocketSubject wss : list) {
            wss.notify("");
        }
    }
}
