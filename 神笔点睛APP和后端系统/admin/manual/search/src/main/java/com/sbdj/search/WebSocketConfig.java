package com.sbdj.search;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * <p>
 * websocket注册
 * </p>
 *
 * @author Yly
 * @since 2019-12-11
 */
@Configuration
public class WebSocketConfig {
    // 扫描并注册带有@ServerEndpoint注解的所有服务端
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
