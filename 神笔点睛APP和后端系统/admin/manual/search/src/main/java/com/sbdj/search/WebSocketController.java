package com.sbdj.search;

import com.sbdj.search.web.response.SearchRespResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * websocket控制器
 * </p>
 *
 * @author Yly
 * @since 2019-12-11
 */
@RestController
public class WebSocketController {

    @RequestMapping("online/all")
    public SearchRespResult onlineAll() {
        Map<String, Object> map = new HashMap<>();
        int sum = WebSocketSubject.Holder.getOnlineCount();
        List<String> list = WebSocketSubject.Holder.getPrincipals();
        map.put("online", sum);
        map.put("member", list);
        return SearchRespResult.success(map);
    }

    @RequestMapping("online")
    public SearchRespResult online(String id) {
        WebSocketSubject subject = WebSocketSubject.Holder.getSubject(id);
        if (subject == null) {
            return SearchRespResult.error("离线或不存在");
        }
        return SearchRespResult.success("在线");
    }

    @RequestMapping("message")
    public SearchRespResult message(String id, String msg) {
        WebSocketSubject subject = WebSocketSubject.Holder.getSubject(id);
        if (subject == null) {
            return SearchRespResult.error("发送失败,需要发送的人不存在或离线");
        }
        // 发送消息
        subject.notify(msg);
        return SearchRespResult.success();
    }

    @RequestMapping("message/all")
    public SearchRespResult messageAll(String msg) {
        List<WebSocketSubject> list = WebSocketSubject.Holder.getSubjects();
        if (list == null) {
            return SearchRespResult.error("群发失败,需要发送的人不存在或离线");
        }
        // 发送消息
        for (WebSocketSubject wss : list) {
            wss.notify(msg);
        }
        return SearchRespResult.success();
    }
}
