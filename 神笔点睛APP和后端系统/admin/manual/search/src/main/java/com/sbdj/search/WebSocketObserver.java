package com.sbdj.search;


import com.alibaba.fastjson.JSON;
import com.sbdj.core.exception.BaseException;

import javax.websocket.Session;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

/**
 * <p>
 * WebSocketObserver
 * </p>
 *
 * @author Yly
 * @since 2019-12-11
 */
public class WebSocketObserver implements Observer {
    /** Web Socket session */
    private Session session;

    public WebSocketObserver(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    @Override
    public void update(Observable o, Object arg) {
        try {
            if (session.isOpen()) {
                if (arg instanceof String) {
                    session.getBasicRemote().sendText(String.valueOf(arg));
                } else {
                    session.getBasicRemote().sendText(JSON.toJSONString(arg).replace("\\", ""));
                }
            }
        } catch (IOException e) {
            throw BaseException.base("websocket sendText error. " + e.getMessage());
        }
    }
}
