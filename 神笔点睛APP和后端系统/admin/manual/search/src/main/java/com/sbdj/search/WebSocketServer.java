package com.sbdj.search;

import com.sbdj.core.constant.Version;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

/**
 * <p>
 * websocket服务
 * </p>
 *
 * @author Yly
 * @since 2019-12-11
 */
@Component
@ServerEndpoint(value = "/"+Version.V3+"/websocket/{principal}")
public class WebSocketServer {
    /** Web Socket连接建立成功的回调方法 */
    @OnOpen
    public void onOpen(@PathParam("principal") String principal, Session session) {
        WebSocketSubject.Holder.getSubject(principal, session);
    }

    /** 服务端收到客户端发来的消息 */
    @OnMessage
    public void onMessage(@PathParam("principal") String principal, String message, Session session) {
        WebSocketSubject.Holder.getSubject(principal, session).notify(message);
    }

    @OnClose
    public void onClose(@PathParam("principal") String principal, Session session) {
        // get subject
        WebSocketSubject subject = WebSocketSubject.Holder.getSubject(principal, session);
        if (subject != null) {
            subject.delete(session);
        }
    }

    @OnError
    public void onError(@PathParam("principal") String principal, Session session, Throwable error) {
        // get subject
        WebSocketSubject subject = WebSocketSubject.Holder.getSubject(principal, session);
        if (subject != null) {
            subject.delete(session);
        }
    }
}
