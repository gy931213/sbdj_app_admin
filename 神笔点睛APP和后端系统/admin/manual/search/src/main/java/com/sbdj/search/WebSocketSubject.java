package com.sbdj.search;

import com.sbdj.core.exception.BaseException;

import javax.websocket.Session;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>
 * WebSocketSubject
 * </p>
 *
 * @author Yly
 * @since 2019-12-11
 */
public class WebSocketSubject extends Observable {
    /** subject键值 */
    private String principal;

    private WebSocketSubject(String principal) {
        this.principal = principal;
    }

    public String getPrincipal() {
        return principal;
    }

    public void notify(String type, String data) {
        super.setChanged();
        Map<String, Object> json = new HashMap<>();
        json.put("type", type);
        json.put("data", data);
        super.notifyObservers(json);
    }

    public void notify(String data) {
        super.setChanged();
        super.notifyObservers(data);
    }

    public  void add(Session session) {
        super.addObserver(new WebSocketObserver(session));
    }

    public void delete(Session session) {
        Holder.subjects.remove(this.principal);
        super.deleteObserver(new WebSocketObserver(session));
        // close session and close Web Socket connection
        try {
            if (session.isOpen()) {
                session.close();
            }
        } catch (IOException e) {
            throw BaseException.base("close web socket session error. " + e.getMessage());
        }
    }

    public static class Holder {
        private static ConcurrentHashMap<String, WebSocketSubject> subjects = new ConcurrentHashMap<>();

        public static List<String> getPrincipals() {
            if (subjects.size() < 1) {
                return null;
            }
            List<String> list = new ArrayList<>();
            for (Map.Entry<String, WebSocketSubject> entry : subjects.entrySet()) {
                list.add(entry.getKey());
            }
            return list;
        }

        public static List<WebSocketSubject> getSubjects() {
            if (subjects.size() < 1) {
                return null;
            }
            return new ArrayList<>(subjects.values());
        }

        public static WebSocketSubject getSubject(String principal, Session session) {
            if (subjects.containsKey(principal)) {
                return subjects.get(principal);
            }

            WebSocketSubject subject = new WebSocketSubject(principal);
            subjects.put(principal, subject);
            subject.add(session);
            return subject;
        }

        public static WebSocketSubject getSubject(String principal) {
            if (subjects.containsKey(principal)) {
                return subjects.get(principal);
            }
            return null;
        }

        // 获取在线连接数
        public static int getOnlineCount() {
            return subjects.size();
        }
    }
}
