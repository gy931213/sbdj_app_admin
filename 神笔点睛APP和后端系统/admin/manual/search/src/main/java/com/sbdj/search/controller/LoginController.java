package com.sbdj.search.controller;

import com.sbdj.core.constant.Version;
import com.sbdj.core.util.StrUtil;
import com.sbdj.search.web.SearchAppSession;
import com.sbdj.search.web.cache.SearchCacheKey;
import com.sbdj.search.web.response.SearchRespResult;
import com.sbdj.service.member.entity.SalesmanMobileCode;
import com.sbdj.service.member.entity.SalesmanNumberTmp;
import com.sbdj.service.member.service.ISalesmanMobileCodeService;
import com.sbdj.service.member.service.ISalesmanNumberTmpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 登录控制器
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 17:24
 */
@Api(value = "点睛查小号")
@RequestMapping(value = Version.V3 + "/login")
@RestController
public class LoginController {
    private static Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private ISalesmanNumberTmpService iSalesmanNumberTmpService;
    @Autowired
    private ISalesmanMobileCodeService iSalesmanMobileCodeService;

    @PostMapping(value = "/sign")
    @ApiOperation(value = "登录", notes = "登录接口")
    public SearchRespResult login(HttpServletRequest request,
                              @ApiParam(required = true, value = "名称") @RequestParam String name,
                              @ApiParam(required = true, value = "电话号码") @RequestParam String phone,
                              @ApiParam(required = true, value = "串号") @RequestParam String imei) {
        if (StrUtil.isNull(name) || StrUtil.isNull(phone)) {
            logger.error("姓名或电话为空");
            return SearchRespResult.error("姓名或电话为空");
        }
        SalesmanNumberTmp salesmanNumberTmp = iSalesmanNumberTmpService.findSalesmanNumberByPhone(phone);
        // 如果不存在，则添加
        if (StrUtil.isNull(salesmanNumberTmp)) {
            salesmanNumberTmp = new SalesmanNumberTmp();
            salesmanNumberTmp.setMobile(phone);
            salesmanNumberTmp.setName(name);
            salesmanNumberTmp.setImei(imei);
            salesmanNumberTmp.setRequestCount(10);
            salesmanNumberTmp.setIsChat(0);
            try {
                iSalesmanNumberTmpService.addSalesmanNumberTmp(salesmanNumberTmp);
            } catch (Exception e) {
                logger.error("新增失败");
                return SearchRespResult.error("服务器内部错误");
            }
        } else {
            if (!name.equals(salesmanNumberTmp.getName())) {
                logger.error("姓名错误");
                return SearchRespResult.error("姓名错误");
            }
        }

        if (StrUtil.isNotNull(imei) && !imei.equals(salesmanNumberTmp.getImei())) {
            String oldImei = salesmanNumberTmp.getImei();
            salesmanNumberTmp.setImei(imei);
            salesmanNumberTmp.setUpdateTime(new Date());
            iSalesmanNumberTmpService.updateSalesmanNumber(salesmanNumberTmp);
            logger.info("串码已改--> 手机:【{}】 姓名:【{}】 旧串码: 【{}】 新串码: 【{}】", salesmanNumberTmp.getMobile(),
                    salesmanNumberTmp.getName(), oldImei, imei);
        }

        SearchAppSession searchAppSession = new SearchAppSession();
        searchAppSession.setAuth(true);
        searchAppSession.setIdentify(salesmanNumberTmp.getId());
        searchAppSession.setObject(salesmanNumberTmp);
        // 保存对应的登录会话,返回对应应得token
        String token = StrUtil.getToken(salesmanNumberTmp.getId());
        searchAppSession.setToken(token);
        SearchCacheKey.setSearchAppSession(token, searchAppSession);
        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        //map.put("count",10);
        // 判断为空赋值为
        /*if(StrKit.isNull(RedisKit.get(SmSnCacheKey.getQueryCount(salesmanNumberTmp.getId())))){
            RedisKit.set(SmSnCacheKey.getQueryCount(salesmanNumberTmp.getId()),10L);
        }*/
        logger.info("===>登录成功，【{},{},{}】",name,phone,imei);
        return SearchRespResult.success(map);
    }

    @PostMapping("check/code")
    public SearchRespResult checkCode(
            @ApiParam(required = true, value = "手机号") @RequestParam String phone,
            @ApiParam(required = true, value = "验证码") @RequestParam String code) {
        SalesmanMobileCode salesmanMobileCode;
        Date expireTime;
        salesmanMobileCode = iSalesmanMobileCodeService.findSalesmanMobileCode(phone);
        if (null == salesmanMobileCode) {
            logger.error("验证码有误");
            return SearchRespResult.error("验证码有误");
        }
        expireTime = salesmanMobileCode.getExpireTime();
        boolean FiveMinutes = new Date(System.currentTimeMillis()).getTime() > expireTime.getTime();

        if (code == null || code.isEmpty()) {
            logger.error("请输入验证码");
            return SearchRespResult.error("请输入验证码");
        }
        if (!code.equals(salesmanMobileCode.getCode())) {
            logger.error("验证码有误");
            return SearchRespResult.error("验证码有误");
        }
        if (FiveMinutes) {
            logger.error("验证码过期");
            return SearchRespResult.error("验证码过期");
        }

        return SearchRespResult.success("验证成功");
    }

    @ApiOperation(value = "获取验证码", notes = "获取验证码接口")
    @PostMapping("get/code")
    public SearchRespResult getCode(@ApiParam(required = true, value = "手机号") @RequestParam String phone) {
        if (null == phone) {
            logger.error("手机号为空");
        }
        try {
            iSalesmanMobileCodeService.sendSmsBySalesmanNumber(phone);
            logger.info("短信发送成功 手机号:{}", phone);
        } catch (Exception e) {
            logger.error("短息发送错误 异常信息: 【{}】 ", e.getMessage());
            return SearchRespResult.error("发送失败");
        }
        return SearchRespResult.success("发送成功");
    }

    @ApiOperation(value = "退出登录", notes = "退出登录接口")
    @PostMapping("logout")
    public SearchRespResult logout(HttpServletRequest request) {
        SearchCacheKey.setSearchAppSessionInvalidate(request);
        return SearchRespResult.success();
    }
}
