package com.sbdj.search.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sbdj.core.constant.PlatformTypeValue;
import com.sbdj.core.constant.Status;
import com.sbdj.core.constant.Version;
import com.sbdj.core.util.DateUtil;
import com.sbdj.core.util.RequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.search.web.SearchAppSession;
import com.sbdj.search.web.cache.SearchCacheKey;
import com.sbdj.search.web.interceptor.SearchAuthority;
import com.sbdj.search.web.response.SearchRespResult;
import com.sbdj.service.member.admin.query.TBInfo;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.member.entity.SalesmanNumberPhonesTmp;
import com.sbdj.service.member.entity.SalesmanNumberTmp;
import com.sbdj.service.member.service.ISalesmanNumberPhonesTmpService;
import com.sbdj.service.member.service.ISalesmanNumberService;
import com.sbdj.service.member.service.ISalesmanNumberTmpService;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.system.entity.SysMapTrajectory;
import com.sbdj.service.system.service.ISysMapTrajectoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 临时号主控制器
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 22:11
 */
@RequestMapping(value = Version.V3 + "/salesman/number")
@RestController
public class SalesmanNumberTmpController {
    private static Logger logger = LoggerFactory.getLogger(SalesmanNumberTmpController.class);

    @Autowired
    private ISalesmanService iSalesmanService;
    @Autowired
    private ISalesmanNumberPhonesTmpService iSalesmanNumberPhonesTmpService;
    @Autowired
    private ISysMapTrajectoryService iSysMapTrajectoryService;
    @Autowired
    private ISalesmanNumberTmpService iSalesmanNumberTmpService;
    @Autowired
    private ISalesmanNumberService iSalesmanNumberService;


    @SearchAuthority
    @PostMapping(value = "save")
    @ApiOperation(value = "保存用户", notes = "保存用户信息接口")
    public SearchRespResult saveSalesman(TBInfo tbInfo) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        //return SnRespResult.success("暂未开放");
        try {
            // 以防前端更新数据后id有缓存值，后台进行id置空处理
            tbInfo.setId(null);
            // if 判断为空，进行处理
            if (StrUtil.isNull(tbInfo.getMobile())) {
                return SearchRespResult.error("缺少参数！");
            }
            // 去除空格
            tbInfo.setSalesmanMobile(StrUtil.removeAllBlank(tbInfo.getSalesmanMobile()));
            tbInfo.setMobile(StrUtil.removeAllBlank(tbInfo.getMobile()));
            tbInfo.setName(StrUtil.removeAllBlank(tbInfo.getName()));
            tbInfo.setOnlineid(StrUtil.removeAllBlank(tbInfo.getOnlineid()));
            tbInfo.setBankNum(StrUtil.removeAllBlank(tbInfo.getBankNum()));

            // if 判断不为空，进行处理
            if (StrUtil.isNotNull(tbInfo.getMobile())) {
                Salesman salesman3 = iSalesmanService.findSalesmanByOrgIdAndMobile(2L,tbInfo.getMobile());
                if (null != salesman3) {
                    return SearchRespResult.error("业务员已存在!");
                }
            }

            // 检测身份证是否注册过
            if (iSalesmanService.isExistSalesmanByIDCard(tbInfo.getCardNum())) {
                return SearchRespResult.error("该身份证已被注册过!");
            }
            // 检测银行卡是否注册过
            if (iSalesmanService.isExistSalesmanByBank(tbInfo.getBankNum())) {
                return SearchRespResult.error("该银行卡已被注册过!");
            }

            // 检测qq是否注册过
            if (iSalesmanService.isExistSalesmanByQQ(tbInfo.getQq())) {
                return SearchRespResult.error("该QQ已被注册过!");
            }

            // 检测微信号是否注册过
            if (iSalesmanService.isExistSalesmanByWeChat(tbInfo.getWechart())) {
                return SearchRespResult.error("该微信已被注册过!");
            }

            // if 判断有推荐人，进行处理
            if(StrUtil.isNotNull(tbInfo.getSalesmanMobile())) {
                // 获取业务员数据
                Salesman salesman2 = iSalesmanService.findSalesmanByOrgIdAndMobile(2L, tbInfo.getSalesmanMobile());
                // if 判断推荐人不存在，进行处理
                if (StrUtil.isNull(salesman2)) {
                    return SearchRespResult.error("推荐人不存在！");
                }
                // if 判断业务员是禁用状态，进行处理
                if(salesman2.getStatus().equalsIgnoreCase(Status.STATUS_PROHIBIT)) {
                    if (StrUtil.isNotNull(salesman2.getDisableTime())) {
                        return SearchRespResult.error("推荐人需要"+ DateUtil.daysBetween(new Date(), salesman2.getDisableTime())+"天解封");
                    } else {
                        return SearchRespResult.error("推荐人已永久禁用！");
                    }
                }
                if (salesman2.getStatus().equalsIgnoreCase(Status.STATUS_UNREVIEWED)) {
                    return SearchRespResult.error("推荐人还未审核");
                }
                // 设置推荐人
                tbInfo.setSalesmanId(salesman2.getId());
            }
            // 判断该淘宝会员名是否存在
            if (StrUtil.isNotNull(tbInfo.getOnlineid())) {
                SalesmanNumber salesmanNumber = iSalesmanNumberService.findSalesmanNumberByPlatformIdOnlineId(PlatformTypeValue.PLATFORM_TYPE_TB.getPlatformId(), tbInfo.getOnlineid());
                if (StrUtil.isNotNull(salesmanNumber)) {
                    return SearchRespResult.error("该淘宝号已经存在!");
                }
            }
            // 保存业务员信息
            iSalesmanService.saveTBInfoTmp(tbInfo);
            logger.info("生成用户->{}", gson.toJson(tbInfo));
            return SearchRespResult.success("提交成功,等待审核");
        } catch (Exception e) {
            return SearchRespResult.error("提交失败");
        }
    }

    @PostMapping(value = "phones")
    @ApiOperation(value = "保存通讯录", notes = "保存通讯录接口")
    public SearchRespResult saveSalesmanPhone(HttpServletRequest request,
                                              @ApiParam(required = true, value = "通讯录数据") @RequestParam  String item) {
        Gson gson = new GsonBuilder().create();
        SearchAppSession snAppSession = SearchCacheKey.getSearchAppSession(request);
        Long salesmanNumberId = snAppSession.getIdentify();

        if (StrUtil.isNull(item)|| item.equalsIgnoreCase("undefined") || item.equalsIgnoreCase("[]")) {
            return SearchRespResult.error();
        }
        // List<SalesmanNumberPhonesTmp> list = JSONArray.parseArray(item, SalesmanNumberPhonesTmp.class);
        Type type = new TypeToken<List<SalesmanNumberPhonesTmp>>() {}.getType();
        List<SalesmanNumberPhonesTmp> list = gson.fromJson(item, type);

        if (!list.isEmpty()) {
            SalesmanNumberPhonesTmp snptKey;
            for (SalesmanNumberPhonesTmp snpt : list) {
                SalesmanNumberPhonesTmp snptTemp = iSalesmanNumberPhonesTmpService.findSalesmanNumberPhonesTmp(salesmanNumberId, snpt.getPhone(), snpt.getImei());
                if (StrUtil.isNull(snptTemp)) {
                    snptKey = new SalesmanNumberPhonesTmp();
                    snptKey.setImei(snpt.getImei());
                    snptKey.setTargetId(salesmanNumberId);
                    snptKey.setName(snpt.getName());
                    snptKey.setPhone(snpt.getPhone());
                    iSalesmanNumberPhonesTmpService.saveSalesmanNumberPhonesTmp(snptKey);
                }
            }
        }
        logger.info("===>通讯录上传成功，通讯录信息为：【{}】",item);
        return SearchRespResult.success();
    }

    @PostMapping(value = "position")
    @ApiOperation(value = "保存坐标", notes = "保存坐标接口")
    public SearchRespResult saveMapTrajectory(HttpServletRequest request, SysMapTrajectory mapTrajectory) {
        SearchAppSession snAppSession = SearchCacheKey.getSearchAppSession(request);
        String ipAddr = RequestUtil.getIpAddr(request);
        Long id = snAppSession.getIdentify();
        if (StrUtil.isNull(mapTrajectory)) {
            return SearchRespResult.error();
        }
        mapTrajectory.setTargetId(id);
        // 默认 号主
        mapTrajectory.setTargetType("0");
        mapTrajectory.setIp(ipAddr);
        iSysMapTrajectoryService.saveSysMapTrajectory(mapTrajectory);
        logger.info("===>坐标上传成功，坐标信息为，经度：【{}】，纬度：【{}】",mapTrajectory.getLongitude(),mapTrajectory.getLatitude());
        return SearchRespResult.success();
    }

    @SearchAuthority
    @PostMapping(value = "chat")
    @ApiOperation(value = "更新聊天", notes = "更新聊天")
    public SearchRespResult isChat(HttpServletRequest request, String phone, String name) {
        logger.info("会话请求: 手机:【{}】 姓名:【{}】", phone, name);
        SalesmanNumberTmp salesmanNumberTmp = iSalesmanNumberTmpService.findSalesmanNumberByPhone(phone);
        if (StrUtil.isNull(salesmanNumberTmp)) {
            salesmanNumberTmp = new SalesmanNumberTmp();
            salesmanNumberTmp.setIsChat(0);
        }
        if (StrUtil.isNotNull(salesmanNumberTmp.getId()) && salesmanNumberTmp.getIsChat() < 1) {
            iSalesmanNumberTmpService.updateSalesmanNumberTmpIsChat(salesmanNumberTmp.getId(), 1);
        }
        return SearchRespResult.success(salesmanNumberTmp);
    }
}
