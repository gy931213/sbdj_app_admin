package com.sbdj.search.web;

import com.sbdj.core.base.ISession;

import java.util.Set;

/**
 * <p>
 * 查小号会话
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 20:55
 */
public class SearchAppSession implements ISession {
    // 是否登录
    private boolean auth;
    // 当前登录人id
    private Long identify;
    // 当前登录人秘钥
    private String token;
    // 登录信息
    private Object object;

    @Override
    public boolean auth() {
        return this.auth;
    }

    @Override
    public Long identify() {
        return this.identify;
    }

    @Override
    public Long orgId() {
        return null;
    }

    @Override
    public Set<String> permissions() {
        return null;
    }

    public boolean isAuth() {
        return auth;
    }

    public Long getIdentify() {
        return identify;
    }

    public String getToken() {
        return token;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public void setIdentify(Long identify) {
        this.identify = identify;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
