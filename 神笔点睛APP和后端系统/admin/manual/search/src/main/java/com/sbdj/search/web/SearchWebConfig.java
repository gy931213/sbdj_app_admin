package com.sbdj.search.web;

import com.sbdj.search.web.interceptor.SearchAuthorityInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <p>
 * 查小号拦截器
 * </p>
 *
 * @author Yly
 * @since 2019-12-03
 */
@Configuration
public class SearchWebConfig implements WebMvcConfigurer {
    private Logger logger = LoggerFactory.getLogger(SearchWebConfig.class);

    @Value("${swagger.show}")
    private boolean swaggerShow = false;
    @Value("${search.enabled}")
    private boolean authEnable = true;

    // 配置拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 判断为false不进行登录验证拦截 authEnable为拦截
        if (authEnable) {
            registry.addInterceptor(new SearchAuthorityInterceptor())
                    .addPathPatterns("/**");
        }
        logger.info("==================================权限拦截器加载成功===========================");
    }

    // 配置静态资源,避免静态资源请求被拦截
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if(swaggerShow) {
            registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
            registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
            registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");
            registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        }
    }
}
