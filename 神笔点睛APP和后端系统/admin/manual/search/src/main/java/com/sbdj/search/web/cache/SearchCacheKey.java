package com.sbdj.search.web.cache;

import com.sbdj.core.util.RedisUtil;
import com.sbdj.search.web.SearchAppSession;
import com.sbdj.search.web.interceptor.SearchAuthorityInterceptor;
import com.sbdj.search.web.response.SearchRespResult;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 临时号主会话键值
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 21:08
 */
public class SearchCacheKey {
    private static final String SM_SN_CACHE = "SESSION_SN_";
    private static final String QUERY_COUNT = "QU_COUNT_";

    private static String getSearchCacheKey(Object key) {
        return SM_SN_CACHE+key;
    }

    public static SearchAppSession getSearchAppSession(Object key) {
        return (SearchAppSession) RedisUtil.get(getSearchCacheKey(key));
    }

    public static SearchAppSession getSearchAppSession(HttpServletRequest request) {
        return (SearchAppSession) RedisUtil.get(getSearchCacheKey(request.getHeader(SearchAuthorityInterceptor.TOKEN_HEADER_KEY)));
    }

    public static String setSearchAppSession(Object key,Object value) {
        RedisUtil.set(getSearchCacheKey(key), value, (1*24*60*60));
        return key.toString();
    }

    public static void setSearchAppSessionInvalidate(Object key) {
        RedisUtil.del(getSearchCacheKey(key));
    }

    public static void setSearchAppSessionInvalidate(HttpServletRequest request) {
        RedisUtil.del(getSearchCacheKey(request.getHeader(SearchAuthorityInterceptor.TOKEN_HEADER_KEY)));
    }

    public static String getQueryCount(Object key){
        return QUERY_COUNT+key;
    }

    public static Long decrQueryCount(Object key){
        long decr = RedisUtil.decr(getQueryCount(key), 1L);
        if(decr == 0){
            RedisUtil.expire(getQueryCount(key),(24*60*60));
        }
        return decr;
    }
}
