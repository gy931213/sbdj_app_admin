package com.sbdj.search.web.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 点睛查小号权限
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 21:16
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SearchAuthority {
    String value() default "user";
}
