package com.sbdj.search.web.interceptor;

import com.sbdj.core.util.StrUtil;
import com.sbdj.search.web.SearchAppSession;
import com.sbdj.search.web.cache.SearchCacheKey;
import com.sbdj.search.web.response.SearchRespResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * <p>
 * 点睛查小号拦截器
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 21:15
 */
public class SearchAuthorityInterceptor extends HandlerInterceptorAdapter {

    private static Logger logger = LoggerFactory.getLogger(SearchAuthorityInterceptor.class);

    public static final String TOKEN_HEADER_KEY = "Authorization";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        SearchAuthority authority = null;
        if (handler instanceof HandlerMethod) {
            Method mtd = ((HandlerMethod) handler).getMethod();
            authority = mtd.getDeclaringClass().getAnnotation(SearchAuthority.class);
            if (authority == null) {
                authority = mtd.getAnnotation(SearchAuthority.class);
            }
        }

        // 判断当前请求是否需要登录验证!
        if (authority == null) {
            // 不需要登录验证!
            return true;
        }
        SearchRespResult respResult = new SearchRespResult();
        // token
        String tokenKey = request.getHeader(TOKEN_HEADER_KEY);
        if(StrUtil.isNull(tokenKey)) {
            respResult.setCode(1000);
            respResult.setMessage("非法访问，请重新登录！");
            SearchRespResult.respOutput(request, response, respResult);
            logger.error("===>非法访问，请重新登录");
            return false;
        }

        // 从缓存中获取Session会话数据。
        SearchAppSession appSession = SearchCacheKey.getSearchAppSession(tokenKey);
        if (StrUtil.isNull(appSession)) {
            respResult.setCode(1000);
            respResult.setMessage("非法访问，请重新登录！");
            SearchRespResult.respOutput(request, response, respResult);
            logger.error("===>非法访问，请重新登录");
            return false;
        }

        // 判断没有登录状态的处理。
        if (!appSession.auth()) {
            respResult.setCode(1000);
            respResult.setMessage("没有登录！");
            SearchRespResult.respOutput(request, response, respResult);
            logger.error("===>非法访问，请重新登录");
            return false;
        }
        return true;
    }
}
