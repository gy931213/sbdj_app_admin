package com.sbdj.search.web.response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbdj.core.constant.ResultCode;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 * 临时号主响应
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 20:59
 */
public class SearchRespResult<T> {
    private static Logger logger = LoggerFactory.getLogger(SearchRespResult.class);

    @ApiModelProperty(value = "返回码，正常返回为 0")
    private int code = ResultCode.RESULT_SUCCESS_CODE;
    @ApiModelProperty(value = "返回请求消息")
    private String message = "";
    @ApiModelProperty(value = "返回请求成功的结果")
    private T data;
    @ApiModelProperty(value = "总记录数")
    private int total = 0;
    public SearchRespResult() {}


    public static <T> SearchRespResult<T> success(T data) {
        return new SearchRespResult<T>(data);
    }

    public static <T> SearchRespResult<T> success() {
        return success("success");
    }


    public static <T> SearchRespResult<T> success(String message) {
        return new SearchRespResult<T>(ResultCode.RESULT_SUCCESS_CODE, message);
    }


    public static <T> SearchRespResult<T> success(T data,String message) {
        return new SearchRespResult<T>(data, message);
    }


    public static <T> SearchRespResult<T> success(int code, String message) {
        return new SearchRespResult<T>(code, message);
    }


    public static <T> SearchRespResult<T> error(int code, String message) {
        return new SearchRespResult<T>(code, message);
    }


    public static <T> SearchRespResult<T> error(String message) {
        return new SearchRespResult<T>(ResultCode.RESULT_ERROR_CODE, message);
    }

    public static <T> SearchRespResult<T> error(T data,String message) {
        return new SearchRespResult<T>(data,ResultCode.RESULT_ERROR_CODE, message);
    }


    public static <T> SearchRespResult<T> error() {
        return error("error");
    }

    public SearchRespResult(int code, String msg) {
        this.code = code;
        this.message = msg;
    }

    public SearchRespResult(T data) {
        this.code = ResultCode.RESULT_SUCCESS_CODE;
        this.message = "success";
        this.data = data;
    }

    public SearchRespResult(T data,String message) {
        this.code = ResultCode.RESULT_SUCCESS_CODE;
        this.message = message;
        this.data = data;
    }

    public SearchRespResult(T data,int code ,String message) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public SearchRespResult(String message) {
        this.code = ResultCode.RESULT_SUCCESS_CODE;
        this.message = message;
    }

    /**
     * 返回结果
     * @author Yly
     * @date 2019/11/22 21:03
     * @param request 请求
     * @param response 响应
     * @param codeMsg 消息
     * @return void
     */
    public static void respOutput(HttpServletRequest request, HttpServletResponse response, String codeMsg) {
        Gson gson = new GsonBuilder().create();
        try {
            // 判断是不是ajax请求或者是不是APP请求
            if (isAjax(request) || isApp(request)) {
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write(gson.toJson(error(codeMsg)));
            } else {
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write(gson.toJson(error("您目前非法请求，APP环境存在异常！")));
                logger.info("====>您目前非法请求，APP环境存在异常！");
            }
        } catch (IOException e) {

        } finally {
            try {
                response.getWriter().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 输出
     * @author Yly
     * @date 2019/11/22 21:02
     * @param request 请求
     * @param response 响应
     * @param searchRespResult 临时号主响应
     * @return void
     */
    public static void respOutput(HttpServletRequest request, HttpServletResponse response, SearchRespResult searchRespResult) {
        Gson gson = new GsonBuilder().create();
        try {
            // 判断是不是ajax请求或者是不是APP请求
            if (isAjax(request) || isApp(request)) {
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write(gson.toJson(searchRespResult));
            } else {
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write(gson.toJson(searchRespResult));
                logger.info("====>您目前非法请求，APP环境存在异常！");
            }
        } catch (IOException e) {

        } finally {
            try {
                response.getWriter().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 判断是否ajax请求
     * @author Yly
     * @date 2019/11/22 21:02
     * @param request 请求
     * @return boolean
     */
    private static boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

    /**
     * 判断是否是APP
     * @author Yly
     * @date 2019/11/22 21:02
     * @param request 请求
     * @return boolean
     */
    private static boolean isApp(HttpServletRequest request) {
        return "app".equals(request.getHeader("client"));
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }

    public int getTotal() {
        return total;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
