package com.sbdj.service.configure.admin.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="银行卡流水对象", description="银行卡流水对象")
public class BankStreamQuery {

    @ApiModelProperty(value="所属机构")
    private Long orgId;
    @ApiModelProperty(value="银行名称")
    private String bankName;
    @ApiModelProperty(value="所属银行id")
    private Long bankId;
    @ApiModelProperty(value="用户名称")
    private String userName;
    @ApiModelProperty(value="是否是商家")
    private Integer isSeller;
    @ApiModelProperty(value="银行流水类型")
    private Integer streamType;
    @ApiModelProperty(value="开始日期")
    private String startTime;
    @ApiModelProperty(value="结束日期")
    private String endTime;
    @ApiModelProperty(value="备注")
    private String remark;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getIsSeller() {
        return isSeller;
    }

    public void setIsSeller(Integer isSeller) {
        this.isSeller = isSeller;
    }

    public Integer getStreamType() {
        return streamType;
    }

    public void setStreamType(Integer streamType) {
        this.streamType = streamType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}
