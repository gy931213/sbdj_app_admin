package com.sbdj.service.configure.admin.query;

/**
 * <p>
 * 银行配置query
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
public class ConfigBankQuery {
    // 关键子
    private String keyword;
    // 机构id
    private Long orgId;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
}
