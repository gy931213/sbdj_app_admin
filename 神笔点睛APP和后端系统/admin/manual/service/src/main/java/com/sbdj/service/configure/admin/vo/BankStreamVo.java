package com.sbdj.service.configure.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 描述：银行流水数据模型
 *
 * @author xiaoqi
 * @date 2019/03/19
 */
@ApiModel(value = "银行流水额度Vo", description = "银行流水额度")
public class BankStreamVo {

    @ApiModelProperty( value="自增主键")
    private Long id;
    @ApiModelProperty( value="所属机构id")

    private Long orgId;
    private String orgName;

    @ApiModelProperty( value="所属银行卡id，关联config_bank")
    private Long bankId;
    private String bankName;

    @ApiModelProperty( value="用户名称")
    private String userName;

    @ApiModelProperty( value="流水支出")
    private BigDecimal defray;
    @ApiModelProperty( value="流水收入")

    private BigDecimal earning;

    @ApiModelProperty( value="余额")
    private BigDecimal balance;

    @ApiModelProperty( value="是否是商家,[{1：是},{0：否}]")

    private Integer isSeller;
    @ApiModelProperty( value="流水类型,[{1：是},{0：否}]")

    private Integer streamType;
    @ApiModelProperty( value="备注")
    private String remark;

    @ApiModelProperty( value="流水日期")
    private Date streamDate;

    @ApiModelProperty( value="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public BigDecimal getDefray() {
        return defray;
    }

    public void setDefray(BigDecimal defray) {
        this.defray = defray;
    }

    public BigDecimal getEarning() {
        return earning;
    }

    public void setEarning(BigDecimal earning) {
        this.earning = earning;
    }

    public Integer getIsSeller() {
        return isSeller;
    }

    public void setIsSeller(Integer isSeller) {
        this.isSeller = isSeller;
    }

    public Integer getStreamType() {
        return streamType;
    }

    public void setStreamType(Integer streamType) {
        this.streamType = streamType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getStreamDate() {
        return streamDate;
    }

    public void setStreamDate(Date streamDate) {
        this.streamDate = streamDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

}