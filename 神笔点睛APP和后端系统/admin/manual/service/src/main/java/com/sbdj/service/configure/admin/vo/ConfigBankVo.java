package com.sbdj.service.configure.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 银行配置Vo
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
public class ConfigBankVo {
    /** id 自增 **/
    private Long id;
    /**所属机构id**/
    private Long orgId;
    /**机构名称**/
    private String orgName;
    /** 银行卡名称{招商银行，中国银行.......} **/
    private String name;
    /** 银行卡归属地{广州番禺区招商银行} **/
    private String detail;
    /** 银行卡所属人名称{李四} **/
    private String userName;
    /** 银行卡号 {48899554466445544} **/
    private String number;
    /** 创建时间 **/
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    /** 创建人id **/
    private Long createId;
    /** 状态{A：活动状态，D：删除状态} **/
    private String status;
    /** 账户余额 **/
    private BigDecimal balance;
    /**保存银行卡的ids**/
    private String bankIds;
    /**负责人**/
    private Long adminId;
    /**负责人名字**/
    private String adminName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getBankIds() {
        return bankIds;
    }

    public void setBankIds(String bankIds) {
        this.bankIds = bankIds;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }
}
