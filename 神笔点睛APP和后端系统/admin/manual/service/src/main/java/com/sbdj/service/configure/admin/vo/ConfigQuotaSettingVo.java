package com.sbdj.service.configure.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 提现额度vo
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
@ApiModel(value = "提现额度Vo", description = "提现额度")
public class ConfigQuotaSettingVo {
    @ApiModelProperty(value = "主键自增")
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "提现最小额度")
    private BigDecimal minQuota;

    @ApiModelProperty(value = "提现最大额度")
    private BigDecimal maxQuota;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @Transient
    @ApiModelProperty(value = "（机构名称）不持久化")
    private String orgName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public BigDecimal getMinQuota() {
        return minQuota;
    }

    public void setMinQuota(BigDecimal minQuota) {
        this.minQuota = minQuota;
    }

    public BigDecimal getMaxQuota() {
        return maxQuota;
    }

    public void setMaxQuota(BigDecimal maxQuota) {
        this.maxQuota = maxQuota;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
}
