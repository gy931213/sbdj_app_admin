package com.sbdj.service.configure.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 银行流水表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="BankStream对象", description="银行流水表")
public class BankStream implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构id")
    private Long orgId;

    @ApiModelProperty(value = "所属银行卡id，关联config_bank")
    private Long bankId;

    @ApiModelProperty(value = "用户名称")
    private String userName;

    @ApiModelProperty(value = "流水支出")
    private BigDecimal defray;

    @ApiModelProperty(value = "流水收入")
    private BigDecimal earning;

    @ApiModelProperty(value = "所属银行卡余额")
    private BigDecimal balance;

    @ApiModelProperty(value = "是否是商家,[{1：是},{0：否}]",example = "0")
    private Integer isSeller;

    @ApiModelProperty(value = "流水类型,[{1：银行流水},{0：其他流水}]",example = "0")
    private Integer streamType;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "流水日期")
    private Date streamDate;

    @ApiModelProperty(value = "操作人")
    private Long oprId;

    @ApiModelProperty(value = "操作时间")
    private Date oprTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "状态[{A：活动状态},{D：删除状态}]")
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public BigDecimal getDefray() {
        return defray;
    }

    public void setDefray(BigDecimal defray) {
        this.defray = defray;
    }
    public BigDecimal getEarning() {
        return earning;
    }

    public void setEarning(BigDecimal earning) {
        this.earning = earning;
    }
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
    public Integer getIsSeller() {
        return isSeller;
    }

    public void setIsSeller(Integer isSeller) {
        this.isSeller = isSeller;
    }
    public Integer getStreamType() {
        return streamType;
    }

    public void setStreamType(Integer streamType) {
        this.streamType = streamType;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Date getStreamDate() {
        return streamDate;
    }

    public void setStreamDate(Date streamDate) {
        this.streamDate = streamDate;
    }
    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }
    public Date getOprTime() {
        return oprTime;
    }

    public void setOprTime(Date oprTime) {
        this.oprTime = oprTime;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BankStream{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", bankId=" + bankId +
            ", userName=" + userName +
            ", defray=" + defray +
            ", earning=" + earning +
            ", balance=" + balance +
            ", isSeller=" + isSeller +
            ", streamType=" + streamType +
            ", remark=" + remark +
            ", streamDate=" + streamDate +
            ", oprId=" + oprId +
            ", oprTime=" + oprTime +
            ", createTime=" + createTime +
            ", status=" + status +
        "}";
    }
}
