package com.sbdj.service.configure.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 银行卡配置
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="ConfigBank对象", description="银行卡配置")
public class ConfigBank implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id 自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "银行卡名称{招商银行，中国银行.......}")
    private String name;

    @ApiModelProperty(value = "银行卡归属地{广州番禺区招商银行}")
    private String detail;

    @ApiModelProperty(value = "银行卡所属人名称{李四}")
    private String userName;

    @ApiModelProperty(value = "银行卡号 {48899554466445544}")
    private String number;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人id")
    private Long createId;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "银行卡余额")
    private BigDecimal balance;

    @ApiModelProperty(value = "所属负责人")
    private Long adminId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    @Override
    public String toString() {
        return "ConfigBank{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", name=" + name +
            ", detail=" + detail +
            ", userName=" + userName +
            ", number=" + number +
            ", createTime=" + createTime +
            ", createId=" + createId +
            ", status=" + status +
            ", balance=" + balance +
            ", adminId=" + adminId +
        "}";
    }
}
