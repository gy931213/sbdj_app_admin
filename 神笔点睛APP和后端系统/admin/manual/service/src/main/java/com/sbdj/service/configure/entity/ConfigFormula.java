package com.sbdj.service.configure.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 计算公式配置
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="ConfigFormula对象", description="计算公式配置")
public class ConfigFormula implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "计算公式")
    private String formula;

    @ApiModelProperty(value = "预留字段")
    private String type;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ConfigFormula{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", formula=" + formula +
            ", type=" + type +
            ", createTime=" + createTime +
        "}";
    }
}
