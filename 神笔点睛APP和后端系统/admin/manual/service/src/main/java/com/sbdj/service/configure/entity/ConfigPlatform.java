package com.sbdj.service.configure.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 平台表配置
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="ConfigPlatform对象", description="平台表配置")
public class ConfigPlatform implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id 自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "平台编码")
    private String code;

    @ApiModelProperty(value = "平台名称{淘宝，京东........n}")
    private String name;

    @ApiModelProperty(value = "平台图片链接地址")
    private String url;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人id")
    private Long createId;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "排序号{1,2,3........n}")
    private Integer sort;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "ConfigPlatform{" +
            "id=" + id +
            ", code=" + code +
            ", name=" + name +
            ", url=" + url +
            ", createTime=" + createTime +
            ", createId=" + createId +
            ", status=" + status +
            ", sort=" + sort +
        "}";
    }
}
