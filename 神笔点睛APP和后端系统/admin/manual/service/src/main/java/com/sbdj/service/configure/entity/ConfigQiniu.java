package com.sbdj.service.configure.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: QiNiu
 * @Description: 七牛配置的实体类
 * @author: 黄天良
 * @date: 2018年11月24日 下午6:02:02
 */

@ApiModel(value="七牛对象", description="七牛对象")
public class ConfigQiniu implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value="所属机构")
    private Long orgId;

    @ApiModelProperty(value="服务器所属区域{华东:0,华北:1,华南:2,北美:3,东南亚:4}")
    private Integer zone;

    @ApiModelProperty(value="AccessKey的值")
    private String accessKey;

    @ApiModelProperty(value="SecretKey的值")
    private String secretKey;

    @ApiModelProperty(value="存储空间名")
    private String bucket;

    @ApiModelProperty(value="imageView2 提供简单快捷的图片格式转换、缩略、剪裁功能。")
    private String imageView;

    @ApiModelProperty(value="服务器地址")
    private String host;

    @ApiModelProperty(value="创建时间")
    private Date createTime;

    @ApiModelProperty("状态")
    private String status;

    @ApiModelProperty("机构名称")
    @TableField(exist = false)
    private String orgName;

    public Long getId() {
        return id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public Integer getZone() {
        return zone;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getBucket() {
        return bucket;
    }

    public String getHost() {
        return host;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public void setZone(Integer zone) {
        this.zone = zone;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImageView() {
        return imageView;
    }

    public void setImageView(String imageView) {
        this.imageView = imageView;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

}
