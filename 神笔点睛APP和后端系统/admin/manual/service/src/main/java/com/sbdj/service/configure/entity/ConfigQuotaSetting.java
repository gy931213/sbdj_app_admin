package com.sbdj.service.configure.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 提现额度配置
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
@ApiModel(value="ConfigQuotaSetting对象", description="提现额度配置")
public class ConfigQuotaSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "提现最小额度")
    private BigDecimal minQuota;

    @ApiModelProperty(value = "提现最大额度")
    private BigDecimal maxQuota;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public BigDecimal getMinQuota() {
        return minQuota;
    }

    public void setMinQuota(BigDecimal minQuota) {
        this.minQuota = minQuota;
    }
    public BigDecimal getMaxQuota() {
        return maxQuota;
    }

    public void setMaxQuota(BigDecimal maxQuota) {
        this.maxQuota = maxQuota;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ConfigQuotaSetting{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", minQuota=" + minQuota +
            ", maxQuota=" + maxQuota +
            ", status=" + status +
            ", createTime=" + createTime +
        "}";
    }
}
