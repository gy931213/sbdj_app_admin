package com.sbdj.service.configure.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 配置警告信息表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="ConfigWarnMessage对象", description="配置警告信息表")
public class ConfigWarnMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "所属平台")
    private Long platformId;

    @ApiModelProperty(value = "警告信息")
    private String warnMsg;

    @ApiModelProperty(value = "搜索提示")
    private String searchTips;

    @ApiModelProperty(value = "搜索说明")
    private String searchExplain;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "机构名称")
    @TableField(exist = false)
    private String orgName;

    @ApiModelProperty(value = "平台名称")
    @TableField(exist = false)
    private String platformName;

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }
    public String getWarnMsg() {
        return warnMsg;
    }

    public void setWarnMsg(String warnMsg) {
        this.warnMsg = warnMsg;
    }
    public String getSearchTips() {
        return searchTips;
    }

    public void setSearchTips(String searchTips) {
        this.searchTips = searchTips;
    }
    public String getSearchExplain() {
        return searchExplain;
    }

    public void setSearchExplain(String searchExplain) {
        this.searchExplain = searchExplain;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ConfigWarnMessage{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", platformId=" + platformId +
            ", warnMsg=" + warnMsg +
            ", searchTips=" + searchTips +
            ", searchExplain=" + searchExplain +
            ", status=" + status +
            ", createTime=" + createTime +
        "}";
    }
}
