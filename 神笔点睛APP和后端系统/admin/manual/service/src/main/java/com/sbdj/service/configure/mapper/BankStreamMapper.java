package com.sbdj.service.configure.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.configure.admin.vo.BankStreamVo;
import com.sbdj.service.configure.entity.BankStream;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 银行流水表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface BankStreamMapper extends BaseMapper<BankStream> {

    IPage<BankStreamVo> findBankStreamExtPage(IPage<BankStream> page, @Param("ew") QueryWrapper<BankStreamVo> queryWrapper);

}
