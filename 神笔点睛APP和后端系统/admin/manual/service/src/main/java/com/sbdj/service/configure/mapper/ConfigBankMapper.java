package com.sbdj.service.configure.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.configure.admin.query.ConfigBankQuery;
import com.sbdj.service.configure.admin.vo.ConfigBankVo;
import com.sbdj.service.configure.entity.ConfigBank;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 银行卡配置 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ConfigBankMapper extends BaseMapper<ConfigBank> {
    /**
     * 分页展示银行卡配置
     * @author Yly
     * @date 2019-11-27 20:37
     * @param iPage 分页
     * @param query 银行卡查询
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.configure.admin.vo.ConfigBankVo>
     */
    IPage<ConfigBankVo> findBankPageList(@Param("page") IPage<ConfigBankVo> iPage, @Param(Constants.WRAPPER) Wrapper<ConfigBankQuery> query);
}
