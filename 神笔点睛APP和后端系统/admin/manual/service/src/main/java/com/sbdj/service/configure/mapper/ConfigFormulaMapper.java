package com.sbdj.service.configure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sbdj.service.configure.entity.ConfigFormula;

/**
 * <p>
 * 计算公式配置 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ConfigFormulaMapper extends BaseMapper<ConfigFormula> {

}
