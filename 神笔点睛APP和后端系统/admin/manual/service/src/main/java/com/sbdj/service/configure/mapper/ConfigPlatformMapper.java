package com.sbdj.service.configure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sbdj.service.configure.entity.ConfigPlatform;

/**
 * <p>
 * 平台表配置 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ConfigPlatformMapper extends BaseMapper<ConfigPlatform> {

}
