package com.sbdj.service.configure.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.configure.admin.vo.ConfigQuotaSettingVo;
import com.sbdj.service.configure.entity.ConfigQuotaSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 提现额度配置 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
public interface ConfigQuotaSettingMapper extends BaseMapper<ConfigQuotaSetting> {

    /**
     * 分页展示提现额度配置
     * @author Yly
     * @date 2019-11-27 20:05
     * @param iPage 分页
     * @param query 查询
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.configure.admin.vo.ConfigQuotaSettingVo>
     */
    IPage<ConfigQuotaSettingVo> findQuotaSettingPage(@Param("page") IPage<ConfigQuotaSettingVo> iPage, @Param(Constants.WRAPPER) Wrapper<ConfigQuotaSettingVo> query);
}
