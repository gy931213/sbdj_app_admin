package com.sbdj.service.configure.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.configure.entity.ConfigWarnMessage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 配置警告信息表 Mapper 接口
 * </p>
 *
 * @author Zc
 * @since 2019-11-08
 */
public interface ConfigWarnMessageMapper extends BaseMapper<ConfigWarnMessage> {
    IPage<ConfigWarnMessage> findWarnMessagePageList(IPage<ConfigWarnMessage> pageParam,@Param("ew") QueryWrapper<ConfigWarnMessage> queryWrapper);
}
