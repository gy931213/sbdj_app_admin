package com.sbdj.service.configure.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.configure.entity.ConfigQiniu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * TODO
 * </p>
 *
 * @author Zc
 * @since 2019-11-27
 */
public interface QiNiuMapper extends BaseMapper<ConfigQiniu> {

    List<ConfigQiniu> findOneByAdminId(Long adminId);

    IPage<ConfigQiniu> findQiNiuPage(IPage<ConfigQiniu> pageable, @Param("ew") QueryWrapper<ConfigQiniu> queryWrapper);
}
