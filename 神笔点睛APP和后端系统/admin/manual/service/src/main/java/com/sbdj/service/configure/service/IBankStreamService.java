package com.sbdj.service.configure.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.configure.admin.query.BankStreamQuery;
import com.sbdj.service.configure.admin.vo.BankStreamVo;
import com.sbdj.service.configure.entity.BankStream;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 银行流水表 服务类
 * </p>
 *
 * @author Zc
 * @since 2019-11-08
 */
public interface IBankStreamService extends IService<BankStream> {

    IPage<BankStreamVo> findBankStreamExtPage(BankStreamQuery query,  IPage<BankStream> buildPageRequest);

    void saveBankStream(BankStream stream);

    void saveAllBankStream(List<String[]> list, Long bankId, Long orgId);

    void updateBankStream(Long id, String param, Long identify, String field);

    void logicDeleteBankStream(Long id, String statusDelete, Long identify);

}
