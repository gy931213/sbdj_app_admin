package com.sbdj.service.configure.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.configure.admin.query.ConfigBankQuery;
import com.sbdj.service.configure.admin.vo.ConfigBankVo;
import com.sbdj.service.configure.entity.ConfigBank;

import java.util.List;

/**
 * <p>
 * 银行卡配置 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface IConfigBankService extends IService<ConfigBank> {
    /**
     * 分页展示银行卡配置
     * @author Yly
     * @date 2019-11-27 20:28
     * @param query 银行卡配置
     * @param iPage 分页
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.configure.admin.vo.ConfigBankVo>
     */
    IPage<ConfigBankVo> findBankPageList(ConfigBankQuery query, IPage<ConfigBankVo> iPage);

    /**
     * 保存银行卡配置
     * @author Yly
     * @date 2019-11-27 20:29
     * @param bank 银行卡
     * @return void
     */
    void saveBank(ConfigBank bank);

    /**
     * 更新银行卡配置
     * @author Yly
     * @date 2019-11-27 20:30
     * @param bank 银行卡
     * @return void
     */
    void updateBank(ConfigBank bank);

    /**
     * 逻辑删除银行卡配置
     * @author Yly
     * @date 2019-11-27 20:30
     * @param id 银行卡配置id
     * @return void
     */
    void logicDeleteBank(Long id);

    /**
     * 批量逻辑删除银行卡配置
     * @author Yly
     * @date 2019-11-27 20:30
     * @param ids
     * @return void
     */
    void logicDeleteBanks(String ids);

    /**
     * 银行卡列表
     * @author Yly
     * @date 2019-11-27 20:43
     * @param orgId 机构id
     * @return java.util.List<com.sbdj.service.configure.entity.ConfigBank>
     */
    List<ConfigBank> findBankList(Long orgId);
}
