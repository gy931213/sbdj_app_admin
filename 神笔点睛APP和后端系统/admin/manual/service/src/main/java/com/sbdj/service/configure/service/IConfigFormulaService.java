package com.sbdj.service.configure.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.configure.entity.ConfigFormula;

/**
 * <p>
 * 计算公式配置 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface IConfigFormulaService extends IService<ConfigFormula> {

}
