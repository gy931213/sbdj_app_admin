package com.sbdj.service.configure.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.configure.entity.ConfigPlatform;

import java.util.List;

/**
 * <p>
 * 平台表配置 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface IConfigPlatformService extends IService<ConfigPlatform> {
    /**
     * 通过关键词获取平台列表
     * @author Yly
     * @date 2019/11/24 19:10
     * @param keyword 关键词
     * @param iPage 分页组件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.configure.entity.ConfigPlatform>
     */
    IPage<ConfigPlatform> findPlatformPageList(String keyword, IPage<ConfigPlatform> iPage);

    /**
     * 获取所有平台信息
     * @author Yly
     * @date 2019/11/24 19:16
     * @return java.util.List<com.sbdj.service.configure.entity.ConfigPlatform>
     */
    List<ConfigPlatform> findPlatformList();

    /**
     * 保存平台信息
     * @author Yly
     * @date 2019/11/24 19:20
     * @param platform 平台
     * @return void
     */
    void savePlatform(ConfigPlatform platform);

    /**
     * 更新平台
     * @author Yly
     * @date 2019/11/24 19:22
     * @param platform 平台
     * @return void
     */
    void updatePlatform(ConfigPlatform platform);

    /**
     * 逻辑删除平台
     * @author Yly
     * @date 2019/11/24 19:26
     * @param id 平台id
     * @return void
     */
    void logicDeletePlatformById(Long id);

    /**
     * 批量逻辑删除平台
     * @author Yly
     * @date 2019/11/24 19:26
     * @param ids 平台ids
     * @return void
     */
    void logicDeletePlatformByIds(String ids);

    ConfigPlatform findPlatformOneById(Long platformId);
}
