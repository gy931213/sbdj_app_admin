package com.sbdj.service.configure.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.configure.admin.vo.ConfigQuotaSettingVo;
import com.sbdj.service.configure.entity.ConfigQuotaSetting;

/**
 * <p>
 * 提现额度配置 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
public interface IConfigQuotaSettingService extends IService<ConfigQuotaSetting> {
    /**
     * 分页展示提现额度配置
     * @author Yly
     * @date 2019-11-27 19:50
     * @param orgId 机构id
     * @param iPage 分页
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.configure.admin.vo.ConfigQuotaSettingVo>
     */
    IPage<ConfigQuotaSettingVo> findQuotaSettingPage(Long orgId, IPage<ConfigQuotaSettingVo> iPage);

    /**
     * 保存提现额度配置
     * @author Yly
     * @date 2019-11-27 19:51
     * @param quotaSetting 提现额度配置
     * @return void
     */
    void saveQuotaSetting(ConfigQuotaSetting quotaSetting);

    /**
     * 更新提现额度配置
     * @author Yly
     * @date 2019-11-27 19:52
     * @param quotaSetting 提现额度配置
     * @return void
     */
    void updateQuotaSetting(ConfigQuotaSetting quotaSetting);

    /**
     * 逻辑删除提现额度配置
     * @author Yly
     * @date 2019-11-27 19:53
     * @param id 提现额度id
     * @return void
     */
    void logicDeleteQuotaSetting(Long id);

    /**
     * 批量逻辑删除提现额度配置
     * @author Yly
     * @date 2019-11-27 19:54
     * @param ids 提现额度ids
     * @return void
     */
    void logicDeleteQuotaSettingByIds(String ids);

    /**
     * @param orgId 机构id
     * @return QuotaSetting 返回类型
     * @Title: findQuotaSettingByOrgId
     * @Description: 根据机构id获取数据的接口
     */
    ConfigQuotaSetting findQuotaSettingByOrgId(Long orgId);
}
