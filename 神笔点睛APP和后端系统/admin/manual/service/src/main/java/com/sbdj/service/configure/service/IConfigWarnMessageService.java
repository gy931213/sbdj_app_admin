package com.sbdj.service.configure.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.service.configure.entity.ConfigWarnMessage;
import org.springframework.data.domain.Page;

/**
 * <p>
 * 配置警告信息表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface IConfigWarnMessageService extends IService<ConfigWarnMessage> {

    /**
     * 获取分页数据的接口
     * @author ZC
     * @date 2019-11-27 14:05
     * @param orgId 机构id
     * @param pageParam 分页参数
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.configure.entity.ConfigWarnMessage>
     */
    IPage<ConfigWarnMessage> findWarnMessagePageList(Long orgId, IPage<ConfigWarnMessage> pageParam);

    /**
     * 根据所属机构和所属平台获取数据的接口
     * @author ZC
     * @date 2019-11-27 15:07
     * @param orgId 所属机构
     * @param platformId 所属平台
     * @return com.sbdj.service.configure.entity.ConfigWarnMessage
     */
    ConfigWarnMessage findWarnMessageOne(Long orgId, Long platformId);

    /**
     * 保存数据的接口
     * @author ZC
     * @date 2019-11-27 15:08
     * @param warnMessage 要保存的数据
     * @return void
     */
    void savaWarnMessage(ConfigWarnMessage warnMessage);

    void updateWarnMessage(ConfigWarnMessage warnMessage);
}
