package com.sbdj.service.configure.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.configure.entity.ConfigQiniu;

/**
 * @ClassName: IQiNiuService
 * @Description: 七牛配置的接口
 * @author: 黄天良
 * @date: 2018年11月24日 下午6:05:22
 */
public interface IQiNiuService {


    /**
     * 保存数据的接口
     * @author ZC
     * @date 2019-11-27 15:55
     * @param configQiniu 要保存的数据
     * @return void
     */
    void saveQiNiu(ConfigQiniu configQiniu);

    /**
     * 更新数据的接口
     * @author ZC
     * @date 2019-11-27 15:56
     * @param configQiniu 要更新的数据
     * @return void
     */
    void updateQiNiu(ConfigQiniu configQiniu);

    /**
     * 删除数据的接口
     * @author ZC
     * @date 2019-11-27 15:56
     * @param id 要删除的id
     * @return void
     */
    void deleteQiNiu(Long id);



    /**
     * 根据机构id获取数据的接口
     * @author ZC
     * @date 2019-11-27 15:57
     * @param orgId 机构id
     * @return com.sbdj.service.configure.entity.QiNiu
     */
    ConfigQiniu findOneByOrgId(Long orgId);

    /**
     * 根据登录人id获取对应的机构中的文件上传配置
     * @author ZC
     * @date 2019-11-27 15:57
     * @param adminId 登录人id
     * @return com.sbdj.service.configure.entity.QiNiu
     */
    ConfigQiniu findOneByAdminId(Long adminId);

    /**
     * 获取一条数据的接口
     * @author ZC
     * @date 2019-11-27 15:57
     * @param
     * @return com.sbdj.service.configure.entity.QiNiu
     */
    ConfigQiniu findQiNiuOne();

    /**
     * 获取分页数据的接口
     * @author ZC
     * @date 2019-11-27 15:58
     * @param pageable 分页参数
     * @return org.springframework.data.domain.Page<com.sbdj.service.configure.entity.QiNiu>
     */
    IPage<ConfigQiniu> findQiNiuPage(IPage<ConfigQiniu> pageable);

}
