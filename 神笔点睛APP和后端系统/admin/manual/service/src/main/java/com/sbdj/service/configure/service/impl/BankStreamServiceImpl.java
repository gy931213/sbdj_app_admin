package com.sbdj.service.configure.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Chars;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.constant.StreamType;
import com.sbdj.core.util.ExcelUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.admin.query.BankStreamQuery;
import com.sbdj.service.configure.admin.vo.BankStreamVo;
import com.sbdj.service.configure.entity.BankStream;
import com.sbdj.service.configure.entity.ConfigBank;
import com.sbdj.service.configure.mapper.BankStreamMapper;
import com.sbdj.service.configure.service.IBankStreamService;
import com.sbdj.service.configure.service.IConfigBankService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 银行流水表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class BankStreamServiceImpl extends ServiceImpl<BankStreamMapper, BankStream> implements IBankStreamService {

    private Logger logger = LoggerFactory.getLogger(BankStreamServiceImpl.class);

    @Autowired
    private IConfigBankService iConfigBankService;

    @Override
    public IPage<BankStreamVo> findBankStreamExtPage(BankStreamQuery query, IPage<BankStream> page) {
        QueryWrapper<BankStreamVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cb.status",Status.STATUS_ACTIVITY).eq("bs.status",Status.STATUS_ACTIVITY);
        // 所属银行卡
        if (StrUtil.isNotNull(query.getBankId()) && !Number.LONG_ZERO.equals(query.getBankId())) {
            queryWrapper.eq("bs.bank_id", + query.getBankId());
        }
        // 根据银行卡名称查询的条件进行数据拆分!（银行名称）
        if (StrUtil.isNotNull(query.getBankName())) {
            if (StrUtil.indexOf(query.getBankName(), Chars.CHAR_TRAN)) {
                // 包含横线，进行数据拆分
                String[] str = query.getBankName().split(Chars.CHAR_TRAN);
                queryWrapper.eq(StrUtil.isNotNull(str[0]),"cb.user_name",str[0] );
                queryWrapper.eq(StrUtil.isNotNull(str[1]),"cb.name",str[1]);
            } else {
                // 不包含横线
                queryWrapper.eq("cb.name", query.getBankName());
            }
        }
        // 所属机构
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId())," bs.org_id", query.getOrgId());
        // 是否是商家
        queryWrapper.eq(StrUtil.isNotNull(query.getIsSeller())," bs.is_seller", query.getIsSeller());
        // 银行流水类型
        queryWrapper.eq(StrUtil.isNotNull(query.getStreamType())," bs.stream_type", query.getStreamType());
        // 用户名称
        queryWrapper.eq(StrUtil.isNotNull(query.getUserName())," bs.user_name", query.getUserName());
        // 开始日期
        queryWrapper.ge(StrUtil.isNotNull(query.getStartTime()),"bs.stream_date",query.getStartTime());
        // 结束日期
        queryWrapper.le(StrUtil.isNotNull(query.getEndTime()),"bs.stream_date",query.getEndTime());
        // 备注
        queryWrapper.and(StrUtil.isNotNull(query.getRemark()),f->f.like("bs.remark",query.getRemark()));
        //sql.append(" order by bs.stream_date desc");
        return baseMapper.findBankStreamExtPage(page,queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveBankStream(BankStream bankStream) {
        bankStream.setCreateTime(new Date());
        bankStream.setStatus(Status.STATUS_ACTIVITY);
        save(bankStream);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateBankStream(Long id, String param, Long identify, String field) {
        UpdateWrapper<BankStream> wrapper = new UpdateWrapper<>();
        if("userName".equals(field)){
            //获取当前时间
            wrapper.set("user_name",param).set("opr_id",identify).set("opr_time",new Date());
        }else{
            wrapper.set("remark",param).set("opr_id",identify).set("opr_time",new Date());
        }
        wrapper.eq("id",id);
        update(wrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteBankStream(Long id, String status, Long oprId) {
        UpdateWrapper<BankStream> wrapper = new UpdateWrapper<>();
        wrapper.set("status",status).set("opr_time",new Date()).set("opr_id",oprId);
        wrapper.eq("id",id);
        update(wrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAllBankStream(List<String[]> list, Long bankId, Long orgId) {
        // 银行卡所有者
        ConfigBank bank = iConfigBankService.getById(bankId);
        if (null == bank) {
            logger.error("银行卡为空");
            return;
        }

        // 银行流水
        BankStream bankStream;
        // 收入/支出
        String income = "income";
        String expenditure = "expenditure";
        Map<String, BigDecimal> map = new HashMap<>();
        map.put(income, BigDecimal.ZERO);
        map.put(expenditure, BigDecimal.ZERO);
        BigDecimal balance = bank.getBalance();

        for (int k = 1; k < list.size(); k++) {
            bankStream = new BankStream();
            String[] str = list.get(k);

            if (StrUtil.isNull(str[1])) {
                continue;
            }

            // 计算
            balance = balance.add(StrUtil.isNotNull(str[2])?new BigDecimal(str[2]):BigDecimal.ZERO);
            balance = balance.subtract(StrUtil.isNotNull(str[3])?new BigDecimal(str[3]):BigDecimal.ZERO);

            bankStream.setOrgId(bank.getOrgId());
            bankStream.setBankId(bankId);
            bankStream.setStreamDate(ExcelUtil.getTime(ExcelUtil.getDate(Integer.parseInt(str[0])), 0.468333));
            bankStream.setUserName(str[1]);
            bankStream.setEarning(StrUtil.isNotNull(str[2])?new BigDecimal(str[2]):BigDecimal.ZERO);
            bankStream.setDefray(StrUtil.isNotNull(str[3])?new BigDecimal(str[3]):BigDecimal.ZERO);
            bankStream.setIsSeller(StrUtil.isNotNull(str[4])?Integer.parseInt(str[4]):0);
            bankStream.setRemark(str[5]);
            bankStream.setStreamType(StreamType.STREAM_TYPE_1);
            bankStream.setBalance(balance);
            bankStream.setCreateTime(new Date());
            bankStream.setStatus(Status.STATUS_ACTIVITY);

            save(bankStream);

            map.put(income, map.get(income).add(StrUtil.isNotNull(str[2])?new BigDecimal(str[2]):BigDecimal.ZERO));
            map.put(expenditure, map.get(expenditure).add(StrUtil.isNotNull(str[3])?new BigDecimal(str[3]):BigDecimal.ZERO));
        }


        logger.info("收入: --> " + map.get(income));
        logger.info("支出: --> " + map.get(expenditure));

        BigDecimal money = bank.getBalance().add(map.get(income)).subtract(map.get(expenditure));

        /*if (money.compareTo(new BigDecimal(0)) < 0) {
            logger.error("过度支出");
            throw new Exception("过度支出");
        }*/
        UpdateWrapper<ConfigBank> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("balance", money);
        updateWrapper.eq("id", bankId);
        iConfigBankService.update(updateWrapper);
    }
}
