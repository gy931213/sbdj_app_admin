package com.sbdj.service.configure.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.PageRequestUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.admin.query.ConfigBankQuery;
import com.sbdj.service.configure.admin.vo.ConfigBankVo;
import com.sbdj.service.configure.entity.ConfigBank;
import com.sbdj.service.configure.mapper.ConfigBankMapper;
import com.sbdj.service.configure.service.IConfigBankService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 银行卡配置 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class ConfigBankServiceImpl extends ServiceImpl<ConfigBankMapper, ConfigBank> implements IConfigBankService {
    @Override
    public IPage<ConfigBankVo> findBankPageList(ConfigBankQuery query, IPage<ConfigBankVo> iPage) {
        QueryWrapper<ConfigBankQuery> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cb.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("so.status", Status.STATUS_ACTIVITY);
        queryWrapper.and(StrUtil.isNotNull(query.getKeyword()), f -> f.like("cb.name", query.getKeyword())
                .or().like("cb.detail", query.getKeyword()).or().like("cb.user_name", query.getKeyword())
                .or().like("cb.number", query.getKeyword()));
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId()), "cb.org_id", query.getOrgId());
        return baseMapper.findBankPageList(iPage, queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveBank(ConfigBank bank) {
        bank.setCreateTime(new Date());
        bank.setBalance(BigDecimal.ZERO);
        bank.setStatus(Status.STATUS_ACTIVITY);
        save(bank);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateBank(ConfigBank bank) {
        if (StrUtil.isNull(bank) || StrUtil.isNull(bank.getId())) {
            throw BaseException.base("更新银行卡失败,id为空");
        }
        updateById(bank);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteBank(Long id) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除银行卡配置失败,id为空");
        }
        UpdateWrapper<ConfigBank> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteBanks(String ids) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("逻辑删除银行卡配置失败,ids为空");
        }
        UpdateWrapper<ConfigBank> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.in("id", list);
        update(updateWrapper);
    }

    @Override
    public List<ConfigBank> findBankList(Long orgId) {
        QueryWrapper<ConfigBank> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId), "org_id", orgId);
        queryWrapper.select("id,concat(user_name,'-',name) name");
        return list(queryWrapper);
    }
}
