package com.sbdj.service.configure.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.service.configure.entity.ConfigFormula;
import com.sbdj.service.configure.mapper.ConfigFormulaMapper;
import com.sbdj.service.configure.service.IConfigFormulaService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 计算公式配置 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Service
public class ConfigFormulaServiceImpl extends ServiceImpl<ConfigFormulaMapper, ConfigFormula> implements IConfigFormulaService {

}
