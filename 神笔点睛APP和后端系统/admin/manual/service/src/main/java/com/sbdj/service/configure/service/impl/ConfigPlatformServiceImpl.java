package com.sbdj.service.configure.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.entity.ConfigPlatform;
import com.sbdj.service.configure.mapper.ConfigPlatformMapper;
import com.sbdj.service.configure.service.IConfigPlatformService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 平台表配置 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class ConfigPlatformServiceImpl extends ServiceImpl<ConfigPlatformMapper, ConfigPlatform> implements IConfigPlatformService {
    @Override
    public IPage<ConfigPlatform> findPlatformPageList(String keyword, IPage<ConfigPlatform> iPage) {
        QueryWrapper<ConfigPlatform> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.like("name", keyword);
        queryWrapper.orderByAsc("sort");
        return page(iPage, queryWrapper);
    }

    @Cacheable(cacheNames = "PlatformList")
    @Override
    public List<ConfigPlatform> findPlatformList() {
        QueryWrapper<ConfigPlatform> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.orderByAsc("sort");
        queryWrapper.select("id", "name");
        return list(queryWrapper);
    }

    @CacheEvict(cacheNames = "PlatformList", allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void savePlatform(ConfigPlatform platform) {
        platform.setCreateTime(new Date());
        platform.setStatus(Status.STATUS_ACTIVITY);
        save(platform);
    }

    @CacheEvict(cacheNames = "PlatformList", allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updatePlatform(ConfigPlatform platform) {
        if (StrUtil.isNull(platform) || StrUtil.isNull(platform.getId())) {
            throw BaseException.base("更新平台失败,平台或id为空");
        }
        updateById(platform);
    }

    @CacheEvict(cacheNames = "PlatformList", allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeletePlatformById(Long id) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除平台失败,id为空");
        }
        UpdateWrapper<ConfigPlatform> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @CacheEvict(cacheNames = "PlatformList", allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeletePlatformByIds(String ids) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("批量逻辑删除平台失败,ids为空");
        }
        UpdateWrapper<ConfigPlatform> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.in("id", list);
        update(updateWrapper);
    }

    @Cacheable(cacheNames = "PlatformList", sync = true, key = "#id")
    @Override
    public ConfigPlatform findPlatformOneById(Long id) {
        QueryWrapper<ConfigPlatform> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY).eq("id", id);
        return getOne(queryWrapper);
    }
}
