package com.sbdj.service.configure.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.admin.vo.ConfigQuotaSettingVo;
import com.sbdj.service.configure.entity.ConfigQuotaSetting;
import com.sbdj.service.configure.mapper.ConfigQuotaSettingMapper;
import com.sbdj.service.configure.service.IConfigQuotaSettingService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 提现额度配置 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
@Transactional(readOnly = true)
@Service
public class ConfigQuotaSettingServiceImpl extends ServiceImpl<ConfigQuotaSettingMapper, ConfigQuotaSetting> implements IConfigQuotaSettingService {
    @Override
    public IPage<ConfigQuotaSettingVo> findQuotaSettingPage(Long orgId, IPage<ConfigQuotaSettingVo> iPage) {
        QueryWrapper<ConfigQuotaSettingVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cqs.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("so.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId), "cqs.org_id", orgId);
        return baseMapper.findQuotaSettingPage(iPage, queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveQuotaSetting(ConfigQuotaSetting quotaSetting) {
        quotaSetting.setCreateTime(new Date());
        quotaSetting.setStatus(Status.STATUS_ACTIVITY);
        save(quotaSetting);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateQuotaSetting(ConfigQuotaSetting quotaSetting) {
        if (StrUtil.isNull(quotaSetting) || StrUtil.isNull(quotaSetting.getId())) {
            throw BaseException.base("更新提现额度失败,id为空");
        }
        updateById(quotaSetting);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteQuotaSetting(Long id) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除提现额度失败,id为空");
        }
        UpdateWrapper<ConfigQuotaSetting> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteQuotaSettingByIds(String ids) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("逻辑删除提现额度失败,ids为空");
        }
        UpdateWrapper<ConfigQuotaSetting> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.in("id", list);
        update(updateWrapper);
    }

    @Override
    public ConfigQuotaSetting findQuotaSettingByOrgId(Long orgId) {

        QueryWrapper<ConfigQuotaSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id","org_id","min_quota","max_quota");
        queryWrapper.eq(StrUtil.isNotNull(orgId),"org_id",orgId);
        queryWrapper.eq("status",Status.STATUS_ACTIVITY);
        queryWrapper.orderByDesc("create_time");
        queryWrapper.last("limit 1");

        return getOne(queryWrapper);
    }
}
