package com.sbdj.service.configure.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.entity.ConfigWarnMessage;
import com.sbdj.service.configure.mapper.ConfigWarnMessageMapper;
import com.sbdj.service.configure.service.IConfigWarnMessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 配置警告信息表 服务实现类
 * </p>
 *
 * @author Zc
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class ConfigWarnMessageServiceImpl extends ServiceImpl<ConfigWarnMessageMapper, ConfigWarnMessage> implements IConfigWarnMessageService {

    @Override
    public IPage<ConfigWarnMessage> findWarnMessagePageList(Long orgId, IPage<ConfigWarnMessage> pageParam) {
        QueryWrapper<ConfigWarnMessage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cwm.status",Status.STATUS_ACTIVITY).eq("so.status",Status.STATUS_ACTIVITY).eq("cp.status",Status.STATUS_ACTIVITY);
        // 机构
        if (null != orgId && !Number.LONG_ZERO.equals(orgId)) {
            queryWrapper.eq("cwm.org_id",orgId);
        }
        queryWrapper.orderByDesc("cwm.create_time");
        return baseMapper.findWarnMessagePageList(pageParam,queryWrapper);
    }

    @Override
    public ConfigWarnMessage findWarnMessageOne(Long orgId, Long platformId) {
        QueryWrapper<ConfigWarnMessage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("org_Id",orgId).eq("platform_id",platformId).eq("status",Status.STATUS_ACTIVITY);
        return getOne(queryWrapper);
    }
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void savaWarnMessage(ConfigWarnMessage warnMessage) {
        warnMessage.setCreateTime(new Date());
        warnMessage.setStatus(Status.STATUS_ACTIVITY);
        save(warnMessage);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateWarnMessage(ConfigWarnMessage warnMessage) {
        if (StrUtil.isNull(warnMessage) || StrUtil.isNull(warnMessage.getId())) {
            throw BaseException.base("更新警告信息错误,id为空");
        }
        updateById(warnMessage);
    }
}
