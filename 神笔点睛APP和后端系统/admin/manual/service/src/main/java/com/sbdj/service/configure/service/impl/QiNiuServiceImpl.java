package com.sbdj.service.configure.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.entity.ConfigQiniu;
import com.sbdj.service.configure.mapper.QiNiuMapper;
import com.sbdj.service.configure.service.IQiNiuService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 七牛配置的接口的实现类
 * </p>
 *
 * @author Yly
 * @since 2019-12-01
 */
@Transactional(readOnly = true)
@Service
public class QiNiuServiceImpl extends ServiceImpl<QiNiuMapper, ConfigQiniu> implements IQiNiuService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveQiNiu(ConfigQiniu configQiniu) {
        configQiniu.setCreateTime(new Date());
        configQiniu.setStatus(Status.STATUS_ACTIVITY);
        save(configQiniu);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    @CacheEvict(cacheNames = "QiNiuConfigSetting", allEntries = true)
    public void updateQiNiu(ConfigQiniu configQiniu) {
        if (StrUtil.isNull(configQiniu) || StrUtil.isNull(configQiniu.getId())) {
            throw BaseException.base("更新七牛配置失败,id为空");
        }
        updateById(configQiniu);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames = "QiNiuConfigSetting", allEntries = true)
    public void deleteQiNiu(Long id) {
        UpdateWrapper<ConfigQiniu> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status",Status.STATUS_DELETE);
        updateWrapper.eq("id",id);
        update(updateWrapper);
    }

    @Override
    @Cacheable(cacheNames = "QiNiuConfigSetting", key = "#orgId")
    public ConfigQiniu findOneByOrgId(Long orgId) {
        QueryWrapper<ConfigQiniu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",Status.STATUS_ACTIVITY).eq("org_id",orgId).orderByDesc("create_time");
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Override
    public ConfigQiniu findOneByAdminId(Long adminId) {
        List<ConfigQiniu> list = baseMapper.findOneByAdminId(adminId);
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public ConfigQiniu findQiNiuOne() {
        QueryWrapper<ConfigQiniu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",Status.STATUS_ACTIVITY).orderByDesc("create_time");
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Override
    public IPage<ConfigQiniu> findQiNiuPage(IPage<ConfigQiniu> pageable){
        QueryWrapper<ConfigQiniu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cq.status",Status.STATUS_ACTIVITY);
        return baseMapper.findQiNiuPage(pageable,queryWrapper);
    }
}
