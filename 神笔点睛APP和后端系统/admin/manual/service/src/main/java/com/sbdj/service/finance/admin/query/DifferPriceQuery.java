package com.sbdj.service.finance.admin.query;

/**
 * @ClassName: DifferPriceParams
 * @Description: 差价 查询
 * @author: 杨凌云
 * @date: 2019/5/27 15:00
 */
public class DifferPriceQuery {
    /**
     * 编号
     **/
    private Long id;
    /**
     * 机构编号
     **/
    private Long orgId;
    /**
     * 店铺名称
     **/
    private String shopName;
    /**
     * 商品标题
     **/
    private String title;
    /**
     * 商品链接
     **/
    private String link;
    /**
     * 业务员名称
     **/
    private String salesmanName;
    /**
     * 号主昵称
     **/
    private String onlineid;
    /**
     * 操作人名称
     **/
    private String oprName;
    /**
     * 是否审核 YES:是 NO:否 D:删除
     **/
    private String status;
    /**
     * 差价类型 GT 大于(收入) LT 小于(支出)
     **/
    private String differType;
    /**
     * 开始时间
     **/
    private String beginTime;
    /**
     * 结束时间
     **/
    private String endTime;
    /**
     * 任务类型id
     **/
    private Long taskTypeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public String getOprName() {
        return oprName;
    }

    public void setOprName(String oprName) {
        this.oprName = oprName;
    }

    public String getDifferType() {
        return differType;
    }

    public void setDifferType(String differType) {
        this.differType = differType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

}
