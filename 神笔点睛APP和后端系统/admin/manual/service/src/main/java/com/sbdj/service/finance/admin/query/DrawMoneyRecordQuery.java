package com.sbdj.service.finance.admin.query;

import java.math.BigDecimal;

/**
 * @ClassName: DrawMoneyRecordParams
 * @Description: 提现查询条件类
 * @author: 黄天良
 * @date: 2018年12月31日 下午2:10:39
 */
public class DrawMoneyRecordQuery {
    // 所属机构
    private Long orgId;
    // 业务员工号
    private String jobNumber;
    // 业务员姓名
    private String salesmanName;
    // 业务员电话号码
    private String mobile;
    // 提现金额
    private BigDecimal applyMoney;
    // 开始时间
    private String startTime;
    // 结束时间
    private String endTime;
    // 状态
    private String status;

    public String getJobNumber() {
        return jobNumber;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public BigDecimal getApplyMoney() {
        return applyMoney;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public void setApplyMoney(BigDecimal applyMoney) {
        this.applyMoney = applyMoney;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

}
