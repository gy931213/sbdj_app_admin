package com.sbdj.service.finance.admin.query;


import com.sbdj.core.constant.Status;

/**
 * @ClassName: PayGoodsParams
 * @Description: 货款查询参数类
 * @author: 黄天良
 * @date: 2018年12月24日 下午4:49:44
 */
public class PayGoodsQuery {

    /**
     * 所属机构id
     **/
    private Long orgId;
    /**
     * 业务员工号
     **/
    private String jobNumber;
    /**
     * 业务员手机号码
     **/
    private String mobile;
    /**
     * 业务员姓名
     **/
    private String name;
    /**
     * 开始时间
     **/
    private String startTime;
    /**
     * 结束时间
     **/
    private String endTime;
    /**
     * 状态
     **/
    private String status;
    /**
     * 关键词
     **/
    private String keyword;
    /**
     * 货款类型，垫付：DF，申请：SQ
     **/
    private String pgtype;
    /**
     * 订单类型id
     **/
    private Long taskTypeId;
    /**
     * 是否开启历史数据查询
     */
    private String isHistory;
    /**
     * 任务状态
     **/
    private String taskStatus;

    public Long getOrgId() {
        return orgId;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public String getMobile() {
        return mobile;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPgtype() {
        return pgtype;
    }

    public void setPgtype(String pgtype) {
        this.pgtype = pgtype;
    }

    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public String getIsHistory() {
        if (null == isHistory || "" == isHistory) {
            isHistory = Status.STATUS_NO;
        }
        return isHistory;
    }

    public void setIsHistory(String isHistory) {
        this.isHistory = isHistory;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

}
