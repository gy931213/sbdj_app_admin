package com.sbdj.service.finance.admin.query;

/**
 * 提成数据条件查询类
 *
 * @author 黄天良
 */
public class PercentageQuery {
    // 工号
    private String jobNumber;
    // 业务员名称
    private String name;
    // 日期
    private String date;
    // 所属机构id
    private Long orgId;

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

}
