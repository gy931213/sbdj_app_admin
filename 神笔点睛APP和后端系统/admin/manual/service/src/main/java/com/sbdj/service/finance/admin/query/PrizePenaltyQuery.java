package com.sbdj.service.finance.admin.query;

import java.math.BigDecimal;

/**
 * @ClassName: PrizePenaltyParams
 * @Description: 业务员奖罚参数
 * @author: 杨凌云
 * @date: 2019/2/17 10:21
 */
public class PrizePenaltyQuery {
    /**
     * 所属机构
     */
    private Long orgId;

    /**
     * 所属业务员
     */
    private Long salesmanId;

    /**
     * 所属业务员姓名
     **/
    private String salesmanName;
    /**
     * 所属业务员工号
     **/
    private String jobNumber;
    /**
     * 业务员手机号码
     **/
    private String mobile;

    /**
     * 奖/罚/金额
     */
    private BigDecimal money;

    /**
     * 奖/罚/原因
     */
    private String reason;

    /**
     * 类型[{PR：奖励}，{PE：惩罚}]
     */
    private String prizeType;

    /**
     * 审核人
     */
    private Long auditorId;

    /**
     * 审核人姓名
     **/
    private String auditorName;

    /**
     * 开始时间
     */
    private String startTime;

    // 结束时间
    private String endTime;


    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAuditorName() {
        return auditorName;
    }

    public void setAuditorName(String auditorName) {
        this.auditorName = auditorName;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(String prizeType) {
        this.prizeType = prizeType;
    }

    public Long getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(Long auditorId) {
        this.auditorId = auditorId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
