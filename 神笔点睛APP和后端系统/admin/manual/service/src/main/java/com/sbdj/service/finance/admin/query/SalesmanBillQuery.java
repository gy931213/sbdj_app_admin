package com.sbdj.service.finance.admin.query;

/**
 * <p>
 * 业务员账单query
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
public class SalesmanBillQuery {
    /** 业务员名称 **/
    private String salesmanName;
    /** 业务员工号 **/
    private String jobNumber;
    /** 机构编号 **/
    private Long orgId;
    /** 业务员手机号 **/
    private String mobile;
    /** 账单类型 **/
    private Integer billType;
    /** 开始时间 **/
    private String startDate;
    /** 结束时间 **/
    private String endDate;

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getBillType() {
        return billType;
    }

    public void setBillType(Integer billType) {
        this.billType = billType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
