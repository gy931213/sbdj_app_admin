package com.sbdj.service.finance.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: DifferPriceExt
 * @Description: 差价 扩展
 * @author: 杨凌云
 * @date: 2019/5/27 14:59
 */
@ApiModel(value = "差价Vo", description = "差价Vo")
public class DifferPriceVo {
    @ApiModelProperty(value="主键编号")
    private Long id;
    @ApiModelProperty(value="所属机构ID")
    private Long orgId;
    @ApiModelProperty(value="机构名称")
    private String orgName;
    @ApiModelProperty(value="所属店铺")
    private Long shopId;
    @ApiModelProperty(value="店铺名称")
    private String shopName;
    @ApiModelProperty(value="所属商品")
    private Long shopGoodsId;
    @ApiModelProperty(value="商品标题")
    private String title;
    @ApiModelProperty(value="所属业务员")
    private Long salesmanId;
    @ApiModelProperty(value="业务员名称")
    private String salesmanName;
    @ApiModelProperty(value="所属号主")
    private Long salesmanNumberId;
    @ApiModelProperty(value="号主昵称")
    private String onlineid;
    @ApiModelProperty(value="应付金额")
    private BigDecimal realPrice;
    @ApiModelProperty(value="实付金额")
    private BigDecimal payment;
    @ApiModelProperty(value="差价金额 (应付金额 减去 实付金额) 负给正取")
    private BigDecimal differPrice;
    @ApiModelProperty(value="是否审核 YES:是 NO:否 D:删除")
    private String status;
    @ApiModelProperty(value="创建时间")
    private Date createTime;
    @ApiModelProperty(value="所属操作人")
    private Long oprId;
    @ApiModelProperty(value="操作人名称")
    private String oprName;
    @ApiModelProperty(value="操作时间")
    private Date oprTime;
    @ApiModelProperty(value="任务编号")
    private Long taskId;
    @ApiModelProperty(value="子任务编号")
    private Long taskSonId;
    @ApiModelProperty(value="商品链接")
    private String link;
    @ApiModelProperty(value="业务员工号")
    private String jobNumber;
    @ApiModelProperty(value="任务类型编码")
    private String taskTypeCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Long getShopGoodsId() {
        return shopGoodsId;
    }

    public void setShopGoodsId(Long shopGoodsId) {
        this.shopGoodsId = shopGoodsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public BigDecimal getDifferPrice() {
        return differPrice;
    }

    public void setDifferPrice(BigDecimal differPrice) {
        this.differPrice = differPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }

    public String getOprName() {
        return oprName;
    }

    public void setOprName(String oprName) {
        this.oprName = oprName;
    }

    public Date getOprTime() {
        return oprTime;
    }

    public void setOprTime(Date oprTime) {
        this.oprTime = oprTime;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getTaskTypeCode() {
        return taskTypeCode;
    }

    public void setTaskTypeCode(String taskTypeCode) {
        this.taskTypeCode = taskTypeCode;
    }

}
