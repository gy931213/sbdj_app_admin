package com.sbdj.service.finance.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: DrawMoneyRecordExt
 * @Description: 财务模块-业务员提现记录-实体扩展
 * @author: 黄天良
 * @date: 2018年12月31日 上午10:55:11
 */
public class DrawMoneyRecordVo {

    /**
     * 主键自增
     **/
    private Long id;
    /**
     * 所属机构
     **/
    private String orgName;
    /**
     * 所属业务员
     **/
    private Long salesmanId;
    /**
     * 业务员工号
     **/
    private String jobNumber;
    /**
     * 所属业务员姓名
     **/
    private String salesmanName;
    /**
     * 业务员账号
     **/
    private String mobile;
    /**
     * 银行卡号
     **/
    private String bankNum;
    /**
     * 开户行名称
     **/
    private String bankName;
    /**
     * 支行名称
     **/
    private String bankad;
    /**
     * 账户余额
     **/
    private BigDecimal balance;
    /**
     * 所属省
     **/
    private String provinceName;
    /**
     * 所属市
     **/
    private String cityName;
    /**
     * 所属县
     **/
    private String countyName;

    /**
     * 申请提现金额
     **/
    private BigDecimal applyMoney;
    /**
     * 已支付金额
     **/
    private BigDecimal payMoney;
    /**
     * 状态
     **/
    private String status;
    /**
     * 审核人
     **/
    private Long auditorId;
    /**
     * 审核人姓名
     **/
    private String auditorName;

    /**
     * 审核时间
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date auditorTime;
    /**
     * 创建时间
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getMobile() {
        return mobile;
    }

    public String getBankNum() {
        return bankNum;
    }

    public String getBankName() {
        return bankName;
    }

    public String getBankad() {
        return bankad;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public String getCountyName() {
        return countyName;
    }

    public BigDecimal getApplyMoney() {
        return applyMoney;
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public String getStatus() {
        return status;
    }

    public Long getAuditorId() {
        return auditorId;
    }

    public String getAuditorName() {
        return auditorName;
    }

    public Date getAuditorTime() {
        return auditorTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setBankNum(String bankNum) {
        this.bankNum = bankNum;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void setBankad(String bankad) {
        this.bankad = bankad;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public void setApplyMoney(BigDecimal applyMoney) {
        this.applyMoney = applyMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setAuditorId(Long auditorId) {
        this.auditorId = auditorId;
    }

    public void setAuditorName(String auditorName) {
        this.auditorName = auditorName;
    }

    public void setAuditorTime(Date auditorTime) {
        this.auditorTime = auditorTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
