package com.sbdj.service.finance.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: PayGoodsExt
 * @Description: 货款实体扩展类
 * @author: 黄天良
 * @date: 2018年12月24日 下午4:01:19
 */
public class PayGoodsVo {
    /**
     * 主键
     **/
    private Long id;
    /**
     * 所属机构
     **/
    private Long orgId;
    /**
     * 所属机构名称
     **/
    private String orgName;
    /**
     * 所属子任务
     **/
    private Long taskSonId;

    /**
     * 所属业务员
     **/
    private Long salesmanId;
    /**
     * 所属业务员姓名
     **/
    private String salesmanName;
    /**
     * 所属业务员工号
     **/
    private String jobNumber;
    /**
     * 业务员手机号码
     **/
    private String mobile;

    /**
     * 银行卡号
     **/
    private String bankNum;
    /**
     * 开户行名称
     **/
    private String bankName;
    /**
     * 支行名称
     **/
    private String bankad;
    /**
     * 省
     **/
    private String province;
    /**
     * 市
     **/
    private String city;
    /**
     * 区/镇
     **/
    private String district;

    /**
     * 申请货款的单号
     **/
    private String applyNumber;
    /**
     * 申请货款金额
     **/
    private BigDecimal applyMoney;
    /**
     * 支付金额
     **/
    private BigDecimal payMoney;
    /**
     * 创建货款时间
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 支付Date
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date payTime;
    /**
     * 备注说明
     **/
    private String comment;
    /**
     * 是否导出：{未导出：0，已导出：1}
     **/
    private int export;
    /**
     * 状态：{已支付：Y，未支付：N}
     **/
    private String status;
    /**
     * 类型：{申请：SQ，垫付：DF}
     **/
    private String type;
    /**
     * 审核人姓名
     **/
    private String auditorName;

    /**
     * 店铺名称
     **/
    private String sellerShopName;
    /**
     * 平台会员号
     **/
    private String onlineid;
    /**
     * 订单类型名称
     **/
    private String taskTypeName;
    /**
     * 号主id
     **/
    private Long salesmanNumberId;
    /**
     * 任务状态
     **/
    private String taskStatus;
    /**
     * 任务状态名称
     **/
    private String taskStatusName;
    /**
     * 类型名称
     **/
    private String typeName;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrgName() {
        return orgName;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public String getMobile() {
        return mobile;
    }

    public String getBankNum() {
        return bankNum;
    }

    public String getBankName() {
        return bankName;
    }

    public String getBankad() {
        return bankad;
    }

    public String getProvince() {
        return province;
    }

    public String getCity() {
        return city;
    }

    public String getDistrict() {
        return district;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setBankNum(String bankNum) {
        this.bankNum = bankNum;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void setBankad(String bankad) {
        this.bankad = bankad;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Long getId() {
        return id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public Long getTaskSonId() {
        return taskSonId;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public String getApplyNumber() {
        return applyNumber;
    }

    public BigDecimal getApplyMoney() {
        return applyMoney;
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public String getComment() {
        return comment;
    }

    public int getExport() {
        return export;
    }

    public String getStatus() {
        return status;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public void setApplyNumber(String applyNumber) {
        this.applyNumber = applyNumber;
    }

    public void setApplyMoney(BigDecimal applyMoney) {
        this.applyMoney = applyMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setExport(int export) {
        this.export = export;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAuditorName() {
        return auditorName;
    }

    public void setAuditorName(String auditorName) {
        this.auditorName = auditorName;
    }

    public String getSellerShopName() {
        return sellerShopName;
    }

    public void setSellerShopName(String sellerShopName) {
        this.sellerShopName = sellerShopName;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public String getTaskTypeName() {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName) {
        this.taskTypeName = taskTypeName;
    }

    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskStatusName() {
        return taskStatusName;
    }

    public void setTaskStatusName(String taskStatusName) {
        this.taskStatusName = taskStatusName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
