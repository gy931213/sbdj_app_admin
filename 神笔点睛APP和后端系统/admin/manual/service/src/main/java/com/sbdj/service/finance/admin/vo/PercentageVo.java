package com.sbdj.service.finance.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 提成扩展类
 * </p>
 *
 * @author Zc
 * @since 2019-11-28
 */
public class PercentageVo {

    @ApiModelProperty(value="业务员id")
    private Long salesmanId;
    @ApiModelProperty(value="业务员名称")
    private String name;
    @ApiModelProperty(value="工号")
    private String jobNumber;
    @ApiModelProperty(value="提成金额")
    private BigDecimal percentage;
    @ApiModelProperty(value="前一天的账户余额")
    private BigDecimal balanceOld;
    @ApiModelProperty(value="提成总金额（该字段的值是和业务员中的账户余额相同的")
    private BigDecimal balanceNew;
    @ApiModelProperty(value="业务员账户余额")
    private BigDecimal balance;
    @ApiModelProperty(value="日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createDate;
    @ApiModelProperty(value="所属机构名称")
    private String orgName;

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getBalanceOld() {
        return balanceOld;
    }

    public void setBalanceOld(BigDecimal balanceOld) {
        this.balanceOld = balanceOld;
    }

    public BigDecimal getBalanceNew() {
        return balanceNew;
    }

    public void setBalanceNew(BigDecimal balanceNew) {
        this.balanceNew = balanceNew;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

}
