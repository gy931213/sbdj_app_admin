package com.sbdj.service.finance.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: PrizePenaltyExt
 * @Description: (这里用一句话描述这个类的作用)
 * @author: 陈元祺
 * @date: 2018/12/31 11:24
 */
public class PrizePenaltyVo {
    /**
     * 所属平台的订单号
     **/
    private String orderNum;

    /**
     * 所属机构
     */
    private Long orgId;

    // 机构名称
    private String orgName;

    /**
     * 所属业务员
     **/
    private Long salesmanId;
    /**
     * 所属业务员姓名
     **/
    private String salesmanName;
    /**
     * 所属业务员工号
     **/
    private String jobNumber;
    /**
     * 业务员手机号码
     **/
    private String mobile;

    /**
     * 状态
     */
    private String status;

    /**
     * 奖/罚/原因
     */
    private String reason;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 奖/罚/金额
     */
    private BigDecimal money;

    /**
     * 类型[{PR：奖励}，{PE：惩罚}]
     */
    private String type;

    /**
     * 审核人
     */
    private Long auditorId;

    /**
     * 审核人姓名
     **/
    private String auditorName;

    public Long getOrgId() {
        return orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(Long auditorId) {
        this.auditorId = auditorId;
    }

    public String getAuditorName() {
        return auditorName;
    }

    public void setAuditorName(String auditorName) {
        this.auditorName = auditorName;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
