package com.sbdj.service.finance.admin.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 业务员账单vo
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
public class SalesmanBillVo {
    /** 业务员账单编号 **/
    private Long id;
    /** 机构编号 **/
    private Long orgId;
    /** 机构名称 **/
    private String orgName;
    /** 业务员编号 **/
    private Long salesmanId;
    /** 目标ID **/
    private String targetId;
    /** 业务员账单类型 {提现:1 佣金:2 提成:3 惩罚:4 奖励:5 差价:6} **/
    private Integer billType;
    /** 之前金额 **/
    private BigDecimal lastMoney;
    /** 金额 **/
    private BigDecimal money;
    /** 之后金额 **/
    private BigDecimal finalMoney;
    /** 原因 **/
    private String account;
    /** 账单日期 **/
    private Date billDate;
    /** 业务员名称 **/
    private String salesmanName;
    /** 业务员工号 **/
    private String jobNumber;
    /** 业务员号码 **/
    private String mobile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public Integer getBillType() {
        return billType;
    }

    public void setBillType(Integer billType) {
        this.billType = billType;
    }

    public BigDecimal getLastMoney() {
        return lastMoney;
    }

    public void setLastMoney(BigDecimal lastMoney) {
        this.lastMoney = lastMoney;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getFinalMoney() {
        return finalMoney;
    }

    public void setFinalMoney(BigDecimal finalMoney) {
        this.finalMoney = finalMoney;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
