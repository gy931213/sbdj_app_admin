package com.sbdj.service.finance.app.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: PercentageVo
 * @Description: 业务员提成明细
 * @author: 杨凌云
 * @date: 2019/4/26 15:44
 */
@ApiModel
public class PercentageListVo {
    @ApiModelProperty(value = "下级业务员名称")
    private String name;
    @ApiModelProperty(value = "下级业务员(1级)所做的子任务编号(T)")
    private String taskSonNumber;
    @ApiModelProperty(value = "完成时间")
    private Date finishTime;
    @ApiModelProperty(value = "每单提成")
    private BigDecimal percentage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaskSonNumber() {
        return taskSonNumber;
    }

    public void setTaskSonNumber(String taskSonNumber) {
        this.taskSonNumber = taskSonNumber;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
}
