package com.sbdj.service.finance.app.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: PercentageVo
 * @Description: 业务员提成总额
 * @author: 杨凌云
 * @date: 2019/4/26 15:36
 */
@ApiModel
public class PercentageTotalVo {
    @ApiModelProperty(value = "业务员id")
    private Long salesmanId;

    @ApiModelProperty(value = "完成时间")
    private Date finishTime;

    @ApiModelProperty(value = "提成总额")
    private BigDecimal total;

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
