package com.sbdj.service.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 财务模块-（子任务差价表）
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="FinanceDifferPrice对象", description="财务模块-（子任务差价表）")
public class FinanceDifferPrice implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构id")
    private Long orgId;

    @ApiModelProperty(value = "所属任务")
    private Long taskId;

    @ApiModelProperty(value = "所属子任务")
    private Long taskSonId;

    @ApiModelProperty(value = "所属任务类型")
    private Long taskTypeId;

    @ApiModelProperty(value = "所属店铺")
    private Long shopId;

    @ApiModelProperty(value = "所属商品")
    private Long shopGoodsId;

    @ApiModelProperty(value = "应付金额")
    private BigDecimal realPrice;

    @ApiModelProperty(value = "实付金额")
    private BigDecimal payment;

    @ApiModelProperty(value = "差价金额")
    private BigDecimal differPrice;

    @ApiModelProperty(value = "所属业务员")
    private Long salesmanId;

    @ApiModelProperty(value = "所属号主")
    private Long salesmanNumberId;

    @ApiModelProperty(value = "是否审核 YES:是 NO:否 D:删除")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "所属操作人（对应sys_admin）")
    private Long oprId;

    @ApiModelProperty(value = "操作时间")
    private Date oprTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }
    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }
    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }
    public Long getShopGoodsId() {
        return shopGoodsId;
    }

    public void setShopGoodsId(Long shopGoodsId) {
        this.shopGoodsId = shopGoodsId;
    }
    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }
    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }
    public BigDecimal getDifferPrice() {
        return differPrice;
    }

    public void setDifferPrice(BigDecimal differPrice) {
        this.differPrice = differPrice;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }
    public Date getOprTime() {
        return oprTime;
    }

    public void setOprTime(Date oprTime) {
        this.oprTime = oprTime;
    }

    @Override
    public String toString() {
        return "FinanceDifferPrice{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", taskId=" + taskId +
            ", taskSonId=" + taskSonId +
            ", taskTypeId=" + taskTypeId +
            ", shopId=" + shopId +
            ", shopGoodsId=" + shopGoodsId +
            ", realPrice=" + realPrice +
            ", payment=" + payment +
            ", differPrice=" + differPrice +
            ", salesmanId=" + salesmanId +
            ", salesmanNumberId=" + salesmanNumberId +
            ", status=" + status +
            ", createTime=" + createTime +
            ", oprId=" + oprId +
            ", oprTime=" + oprTime +
        "}";
    }
}
