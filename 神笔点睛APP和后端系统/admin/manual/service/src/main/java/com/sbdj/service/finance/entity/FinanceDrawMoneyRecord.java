package com.sbdj.service.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 财务模块-业务员提现记录
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="FinanceDrawMoneyRecord对象", description="财务模块-业务员提现记录")
public class FinanceDrawMoneyRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "所属业务员")
    private Long salesmanId;

    @ApiModelProperty(value = "申请提现金额")
    private BigDecimal applyMoney;

    @ApiModelProperty(value = "已支付金额")
    private BigDecimal payMoney;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "审核人")
    private Long auditorId;

    @ApiModelProperty(value = "审核时间")
    private Date auditorTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public BigDecimal getApplyMoney() {
        return applyMoney;
    }

    public void setApplyMoney(BigDecimal applyMoney) {
        this.applyMoney = applyMoney;
    }
    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Long getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(Long auditorId) {
        this.auditorId = auditorId;
    }
    public Date getAuditorTime() {
        return auditorTime;
    }

    public void setAuditorTime(Date auditorTime) {
        this.auditorTime = auditorTime;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "FinanceDrawMoneyRecord{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", salesmanId=" + salesmanId +
            ", applyMoney=" + applyMoney +
            ", payMoney=" + payMoney +
            ", status=" + status +
            ", auditorId=" + auditorId +
            ", auditorTime=" + auditorTime +
            ", createTime=" + createTime +
        "}";
    }
}
