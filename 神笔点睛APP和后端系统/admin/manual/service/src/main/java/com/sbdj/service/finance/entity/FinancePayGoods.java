package com.sbdj.service.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 财务模块-货款表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="FinancePayGoods对象", description="财务模块-货款表")
public class FinancePayGoods implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "所属子任务")
    private Long taskSonId;

    @ApiModelProperty(value = "所属业务员")
    private Long salesmanId;

    @ApiModelProperty(value = "号主id")
    private Long salesmanNumberId;

    @ApiModelProperty(value = "申请货款的单号")
    private String applyNumber;

    @ApiModelProperty(value = "申请货款金额")
    private BigDecimal applyMoney;

    @ApiModelProperty(value = "支付金额")
    private BigDecimal payMoney;

    @ApiModelProperty(value = "支付时间")
    private Date payTime;

    @ApiModelProperty(value = "备注说明")
    private String comment;

    @ApiModelProperty(value = "是否导出：{未导出：0，已导出：1}")
    private Integer export;

    @ApiModelProperty(value = "状态：{已支付：Y，未支付：N ,作废：O}")
    private String status;

    @ApiModelProperty(value = "类型：{申请：SQ，垫付：DF}")
    private String type;

    @ApiModelProperty(value = "审核人")
    private Long auditorId;

    @ApiModelProperty(value = "创建货款时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }
    public String getApplyNumber() {
        return applyNumber;
    }

    public void setApplyNumber(String applyNumber) {
        this.applyNumber = applyNumber;
    }
    public BigDecimal getApplyMoney() {
        return applyMoney;
    }

    public void setApplyMoney(BigDecimal applyMoney) {
        this.applyMoney = applyMoney;
    }
    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }
    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    public Integer getExport() {
        return export;
    }

    public void setExport(Integer export) {
        this.export = export;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public Long getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(Long auditorId) {
        this.auditorId = auditorId;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "FinancePayGoods{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", taskSonId=" + taskSonId +
            ", salesmanId=" + salesmanId +
            ", salesmanNumberId=" + salesmanNumberId +
            ", applyNumber=" + applyNumber +
            ", applyMoney=" + applyMoney +
            ", payMoney=" + payMoney +
            ", payTime=" + payTime +
            ", comment=" + comment +
            ", export=" + export +
            ", status=" + status +
            ", type=" + type +
            ", auditorId=" + auditorId +
            ", createTime=" + createTime +
        "}";
    }
}
