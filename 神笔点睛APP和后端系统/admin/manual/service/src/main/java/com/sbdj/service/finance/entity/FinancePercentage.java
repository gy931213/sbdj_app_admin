package com.sbdj.service.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 业务员提成明细表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="FinancePercentage对象", description="业务员提成明细表")
public class FinancePercentage implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "业务员id")
    private Long salesmanId;

    @ApiModelProperty(value = "下级(1级)业务员id")
    private Long salesmansId;

    @ApiModelProperty(value = "组织id")
    private Long orgId;

    @ApiModelProperty(value = "子任务id")
    private Long taskSonId;

    @ApiModelProperty(value = "下级业务员(1级)所做的子任务编号(T)")
    private String taskSonNumber;

    @ApiModelProperty(value = "每单提成")
    private BigDecimal percentage;

    @ApiModelProperty(value = "完成时间")
    private Date finishTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Long getSalesmansId() {
        return salesmansId;
    }

    public void setSalesmansId(Long salesmansId) {
        this.salesmansId = salesmansId;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }
    public String getTaskSonNumber() {
        return taskSonNumber;
    }

    public void setTaskSonNumber(String taskSonNumber) {
        this.taskSonNumber = taskSonNumber;
    }
    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    @Override
    public String toString() {
        return "FinancePercentage{" +
            "id=" + id +
            ", salesmanId=" + salesmanId +
            ", salesmansId=" + salesmansId +
            ", orgId=" + orgId +
            ", taskSonId=" + taskSonId +
            ", taskSonNumber=" + taskSonNumber +
            ", percentage=" + percentage +
            ", finishTime=" + finishTime +
        "}";
    }
}
