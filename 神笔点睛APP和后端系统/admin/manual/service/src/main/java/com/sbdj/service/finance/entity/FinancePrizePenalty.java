package com.sbdj.service.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 财务模块-业务员奖惩表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="FinancePrizePenalty对象", description="财务模块-业务员奖惩表")
public class FinancePrizePenalty implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "所属业务员")
    private Long salesmanId;

    @ApiModelProperty(value = "所属号主")
    private Long salesmanNumberId;

    @ApiModelProperty(value = "所属主任务")
    private Long taskId;

    @ApiModelProperty(value = "所属子任务")
    private Long taskSonId;

    @ApiModelProperty(value = "奖/罚/金额")
    private BigDecimal money;

    @ApiModelProperty(value = "奖/罚/原因")
    private String reason;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "类型[{PR：奖励}，{PE：惩罚}]")
    private String type;

    @ApiModelProperty(value = "审核人")
    private Long auditorId;

    @ApiModelProperty(value = "审核时间")
    private Date auditorTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }
    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }
    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public Long getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(Long auditorId) {
        this.auditorId = auditorId;
    }
    public Date getAuditorTime() {
        return auditorTime;
    }

    public void setAuditorTime(Date auditorTime) {
        this.auditorTime = auditorTime;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "FinancePrizePenalty{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", salesmanId=" + salesmanId +
            ", salesmanNumberId=" + salesmanNumberId +
            ", taskId=" + taskId +
            ", taskSonId=" + taskSonId +
            ", money=" + money +
            ", reason=" + reason +
            ", status=" + status +
            ", type=" + type +
            ", auditorId=" + auditorId +
            ", auditorTime=" + auditorTime +
            ", createTime=" + createTime +
        "}";
    }
}
