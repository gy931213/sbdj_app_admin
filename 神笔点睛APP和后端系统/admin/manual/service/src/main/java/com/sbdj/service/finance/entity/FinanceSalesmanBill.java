package com.sbdj.service.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 财务模块-(业务员账单)
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="FinanceSalesmanBill对象", description="财务模块-(业务员账单)")
public class FinanceSalesmanBill implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "所属业务员")
    private Long salesmanId;

    @ApiModelProperty(value = "账单类型")
    private Integer billType;

    @ApiModelProperty(value = "所属目标（id）")
    private String targetId;

    @ApiModelProperty(value = "当前账户余额")
    private BigDecimal lastMoney;

    @ApiModelProperty(value = "扣除或加入的金额")
    private BigDecimal money;

    @ApiModelProperty(value = "当前账户最终余额")
    private BigDecimal finalMoney;

    @ApiModelProperty(value = "原因，说明，解释")
    private String account;

    @ApiModelProperty(value = "账单日期")
    private Date billDate;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Integer getBillType() {
        return billType;
    }

    public void setBillType(Integer billType) {
        this.billType = billType;
    }
    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }
    public BigDecimal getLastMoney() {
        return lastMoney;
    }

    public void setLastMoney(BigDecimal lastMoney) {
        this.lastMoney = lastMoney;
    }
    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
    public BigDecimal getFinalMoney() {
        return finalMoney;
    }

    public void setFinalMoney(BigDecimal finalMoney) {
        this.finalMoney = finalMoney;
    }
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "FinanceSalesmanBill{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", salesmanId=" + salesmanId +
            ", billType=" + billType +
            ", targetId=" + targetId +
            ", lastMoney=" + lastMoney +
            ", money=" + money +
            ", finalMoney=" + finalMoney +
            ", account=" + account +
            ", billDate=" + billDate +
            ", createTime=" + createTime +
        "}";
    }
}
