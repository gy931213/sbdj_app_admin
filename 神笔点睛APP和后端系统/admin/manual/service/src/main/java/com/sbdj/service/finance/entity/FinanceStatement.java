package com.sbdj.service.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 佣金流水表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="FinanceStatement对象", description="佣金流水表")
public class FinanceStatement implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "业务员id")
    private Long salesmanId;

    @ApiModelProperty(value = "子任务id")
    private Long taskSonId;

    @ApiModelProperty(value = "佣金金额")
    private BigDecimal money;

    @ApiModelProperty(value = "子任务状态")
    private String taskSonStatus;

    @ApiModelProperty(value = "状态:{1为增加，0为减少}")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }
    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
    public String getTaskSonStatus() {
        return taskSonStatus;
    }

    public void setTaskSonStatus(String taskSonStatus) {
        this.taskSonStatus = taskSonStatus;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "FinanceStatement{" +
            "id=" + id +
            ", salesmanId=" + salesmanId +
            ", taskSonId=" + taskSonId +
            ", money=" + money +
            ", taskSonStatus=" + taskSonStatus +
            ", status=" + status +
            ", createTime=" + createTime +
        "}";
    }
}
