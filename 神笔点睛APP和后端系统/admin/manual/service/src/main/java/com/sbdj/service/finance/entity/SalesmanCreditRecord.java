package com.sbdj.service.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 业务员信用增减记录表
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@ApiModel(value="SalesmanCreditRecord对象", description="业务员信用增减记录表")
public class SalesmanCreditRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构id")
    private Long orgId;

    @ApiModelProperty(value = "业务员id")
    private Long salesmanId;

    @ApiModelProperty(value = "子任务id")
    private Long taskSonId;

    @ApiModelProperty(value = "原因说明")
    private String reason;

    @ApiModelProperty(value = "增/减的金额额度")
    private BigDecimal increment;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    public BigDecimal getIncrement() {
        return increment;
    }

    public void setIncrement(BigDecimal increment) {
        this.increment = increment;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "SalesmanCreditRecord{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", salesmanId=" + salesmanId +
            ", taskSonId=" + taskSonId +
            ", reason=" + reason +
            ", increment=" + increment +
            ", createTime=" + createTime +
        "}";
    }
}
