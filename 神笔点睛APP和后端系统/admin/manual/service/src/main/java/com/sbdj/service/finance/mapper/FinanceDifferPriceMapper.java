package com.sbdj.service.finance.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.finance.admin.vo.DifferPriceVo;
import com.sbdj.service.finance.admin.query.DifferPriceQuery;
import com.sbdj.service.finance.entity.FinanceDifferPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 财务模块-（子任务差价表） Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface FinanceDifferPriceMapper extends BaseMapper<FinanceDifferPrice> {
    /**
     * 显示差价列表信息
     * @author Gy
     * @date 2019-11-28 15:14
     * @param page  分页参数
     * @param queryWrapper      条件查询构造器
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.finance.admin.Vo.DifferPriceVo>
     */
    IPage<DifferPriceVo> findDifferPriceByPage(IPage<DifferPriceVo> page, @Param(Constants.WRAPPER) QueryWrapper<DifferPriceQuery> queryWrapper);

    /**
     * 按ID查询差价列表
     * @author Gy
     * @date 2019-11-28 15:15
     * @param ids
     * @return java.util.List<com.sbdj.service.finance.admin.Vo.DifferPriceVo>
     */
    DifferPriceVo findDifferPriceByIds(@Param("ids") String ids);
}
