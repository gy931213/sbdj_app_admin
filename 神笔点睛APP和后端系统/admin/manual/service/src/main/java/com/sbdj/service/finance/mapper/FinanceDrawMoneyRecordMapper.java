package com.sbdj.service.finance.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.finance.admin.vo.DrawMoneyRecordVo;
import com.sbdj.service.finance.entity.FinanceDrawMoneyRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 财务模块-业务员提现记录 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface FinanceDrawMoneyRecordMapper extends BaseMapper<FinanceDrawMoneyRecord> {

    IPage<DrawMoneyRecordVo> findDrawMoneyRecordPage(@Param("page") IPage<DrawMoneyRecordVo> iPage, @Param("ew") QueryWrapper<DrawMoneyRecordVo> queryWrapper);
    /**
     * 查找记录
     * @author Gy
     * @date 2019-11-30 10:38
     * @param page
     * @param salesmanId
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.finance.admin.vo.DrawMoneyRecordVo>
     */
    IPage<DrawMoneyRecordVo> findRecordList(IPage<DrawMoneyRecordVo> page,@Param("salesmanId") Long salesmanId);
}
