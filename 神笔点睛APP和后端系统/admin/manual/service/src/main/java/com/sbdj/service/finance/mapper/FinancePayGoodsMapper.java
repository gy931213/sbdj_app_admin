package com.sbdj.service.finance.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.finance.admin.vo.PayGoodsVo;
import com.sbdj.service.finance.entity.FinancePayGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 财务模块-货款表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface FinancePayGoodsMapper extends BaseMapper<FinancePayGoods> {

    /**
     * 更新贷款商品状态
     * @author Gy
     * @date 2019-11-28 10:18
     * @param id   贷款ID
     * @param auditorId 审核员ID
     * @param status  状态
     * @return void
     */
    void updatePayGoodsStatus(@Param("id") Long id,@Param("auditorId") Long auditorId,@Param("status") String status);
    /**
     * TODO
     * @author Gy
     * @date 2019-11-28 10:23
     * @param auditorId  审核员ID
     * @return void
     */
    void payGoodsBalance(@Param("auditorId") Long auditorId, @Param("ids") List<String> ids, @Param("pays") List<String> pays);
    /**
     * 获取带框列表信息
     * @author Gy
     * @date 2019-11-28 11:03
     * @param page  分页信息
     * @param queryWrapper  查询条件构造器
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.finance.admin.Vo.PayGoodsVo>
     */
    IPage<PayGoodsVo> findPayGoodsPageList(IPage<PayGoodsVo> page,@Param(Constants.WRAPPER) QueryWrapper queryWrapper);
}
