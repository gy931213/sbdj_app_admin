package com.sbdj.service.finance.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.finance.admin.vo.PercentageVo;
import com.sbdj.service.finance.app.vo.PercentageListVo;
import com.sbdj.service.finance.app.vo.PercentageTotalVo;
import com.sbdj.service.finance.entity.FinancePercentage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 业务员提成明细表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface FinancePercentageMapper extends BaseMapper<FinancePercentage> {

    IPage<PercentageVo> findPercentagePage(IPage<PercentageVo> page,@Param("ew")QueryWrapper<PercentageVo> queryWrapper);

    String returnGrantStatementByDate(@Param("date")String date);
    /**
     * 查询每天提成总额
     * @author Gy
     * @date 2019-11-30 11:17
     * @param page
     * @param salesmanId
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.finance.app.vo.PercentageTotalVo>
     */
    IPage<PercentageTotalVo> findPercentageList(IPage<PercentageTotalVo> page,@Param("salesmanId") Long salesmanId);

    /**
     * @param salesmanId 业务员id
     * @param time       时间
     * @return List<PercentageListVo> 返回类型
     * @Title:findPercentageList
     * @Description: (查询每天提成详细列表)
     */
    List<PercentageListVo> findPercentageListByTime( @Param("salesmanId") Long salesmanId,@Param("time") String time);
}
