package com.sbdj.service.finance.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.finance.admin.vo.PrizePenaltyVo;
import com.sbdj.service.finance.admin.query.PrizePenaltyQuery;
import com.sbdj.service.finance.entity.FinancePrizePenalty;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 财务模块-业务员奖惩表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface FinancePrizePenaltyMapper extends BaseMapper<FinancePrizePenalty> {
    /**
     * 显示奖惩列表信息
     * @author Gy
     * @date 2019-11-28 16:59
     * @param page 分页信息
     * @param queryWrapper  查询条件构造器
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.finance.admin.Vo.PrizePenaltyVo>
     */
    IPage<PrizePenaltyVo> findPrizePenaltyRecordPage(IPage<PrizePenaltyVo> page, @Param(Constants.WRAPPER) QueryWrapper<PrizePenaltyQuery> queryWrapper);
    /**
     * 查询当前业务员奖惩列表
     * @author Gy
     * @date 2019-11-30 09:37
     * @param page
     * @param queryWrapper
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.finance.admin.vo.PrizePenaltyVo>
     */
    IPage<PrizePenaltyVo> findPenaltyList(IPage<PrizePenaltyVo> page,@Param(Constants.WRAPPER) QueryWrapper<PrizePenaltyVo> queryWrapper);

    /**
     * @param salesmanId
     * @return PrizePenalty 返回类型
     * @Title: findPenaltyList
     * @Description: (查询当前业务员奖惩列表)
     */
    PrizePenaltyVo findPenaltyList2(@Param("salesmanId") Long salesmanId,@Param("type") String type);
}
