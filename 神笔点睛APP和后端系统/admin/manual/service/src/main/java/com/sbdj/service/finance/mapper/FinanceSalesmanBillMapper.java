package com.sbdj.service.finance.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.finance.admin.query.SalesmanBillQuery;
import com.sbdj.service.finance.admin.vo.SalesmanBillVo;
import com.sbdj.service.finance.app.vo.SalesmanBillRespVo;
import com.sbdj.service.finance.entity.FinanceSalesmanBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * 财务模块-(业务员账单) Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface FinanceSalesmanBillMapper extends BaseMapper<FinanceSalesmanBill> {

    IPage<SalesmanBillVo> findSalesmanBillByPage(@Param("page") IPage<SalesmanBillVo> iPage,@Param(Constants.WRAPPER) Wrapper<SalesmanBillQuery> query);
    /**
     * 获取账单分页信息的接口
     * @author Gy
     * @date 2019-11-30 11:28
     * @param page
     * @param queryWrapper
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.finance.app.vo.SalesmanBillRespVo>
     */

    IPage<SalesmanBillRespVo> findSalesmanBillRespVo(IPage<SalesmanBillRespVo> page, @Param(Constants.WRAPPER) QueryWrapper<SalesmanBillRespVo> queryWrapper);
    /**
     * 获取业务员总金额的接口
     * @author Gy
     * @date 2019-11-30 11:44
     * @param queryWrapper
     * @return java.math.BigDecimal
     */
    BigDecimal salesmanBillTotalMoney(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);
    /***
     * 获取业务员的总收入
     * @author Gy
     * @date 2019-11-30 13:22
     * @param salesmanId
     * @return java.math.BigDecimal
     */
    BigDecimal salesmanBillInfo(@Param("salesmanId") Long salesmanId);
    /***
     * 获取业务员的昨日收益
     * @author Gy
     * @date 2019-11-30 13:22
     * @param salesmanId
     * @return java.math.BigDecimal
     */
    BigDecimal salesmanBillInfo2(@Param("salesmanId") Long salesmanId);
}
