package com.sbdj.service.finance.mapper;

import com.sbdj.service.finance.entity.FinanceStatement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sbdj.service.finance.scheduler.vo.StatementTaskCreditVo;

import java.util.List;

/**
 * <p>
 * 佣金流水表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface FinanceStatementMapper extends BaseMapper<FinanceStatement> {
    /**
     * 获取任务佣金流水宝
     * @author Yly
     * @date 2019-11-28 17:16
     * @param
     * @return java.util.List<com.sbdj.service.finance.scheduler.vo.StatementTaskCreditVo>
     */
    List<StatementTaskCreditVo> findStatementTaskSon();
}
