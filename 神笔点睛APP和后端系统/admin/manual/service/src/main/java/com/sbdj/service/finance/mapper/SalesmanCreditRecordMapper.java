package com.sbdj.service.finance.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.finance.entity.SalesmanCreditRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sbdj.service.member.scheduler.vo.SalesmanCreditRecordVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 业务员信用增减记录表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public interface SalesmanCreditRecordMapper extends BaseMapper<SalesmanCreditRecord> {

    IPage<SalesmanCreditRecordVo> findCreditList(IPage<Object> page, @Param("ew") QueryWrapper<SalesmanCreditRecordVo> queryWrapper);

}
