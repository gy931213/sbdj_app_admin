package com.sbdj.service.finance.scheduler.vo;

import java.util.Date;

/**
 * <p>
 * 业务员提成流水
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public class StatementPercentageVo {
    /** 下级业务员id **/
    private Long salesmansId;
    /** 子任务id **/
    private Long taskSonId;
    /** 流水表状态 **/
    private Integer status;
    /** 上级业务员id **/
    private Long salesmanId;
    /** 机构id **/
    private Long orgId;
    /** 子任务编号 **/
    private String taskSonNumber;
    /** 任务创建时间 **/
    private Date updateTime;

    public Long getSalesmansId() {
        return salesmansId;
    }

    public void setSalesmansId(Long salesmansId) {
        this.salesmansId = salesmansId;
    }

    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getTaskSonNumber() {
        return taskSonNumber;
    }

    public void setTaskSonNumber(String taskSonNumber) {
        this.taskSonNumber = taskSonNumber;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
