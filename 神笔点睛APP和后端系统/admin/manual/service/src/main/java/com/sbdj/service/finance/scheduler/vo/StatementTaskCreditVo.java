package com.sbdj.service.finance.scheduler.vo;

/**
 * <p>
 * 业务员任务流水表
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public class StatementTaskCreditVo {
    /** 机构id **/
    private Long orgId;
    /** 子任务id **/
    private Long id;
    /** 业务员id **/
    private Long salesmanId;
    /** 子任务类型 **/
    private String taskTypeName;
    /** 子任务类型编号 **/
    private String taskTypeCode;
    /** 流水表状态 **/
    private Integer status;
    /** 子任务订单号 **/
    private String taskSonNumber;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getTaskTypeName() {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName) {
        this.taskTypeName = taskTypeName;
    }

    public String getTaskTypeCode() {
        return taskTypeCode;
    }

    public void setTaskTypeCode(String taskTypeCode) {
        this.taskTypeCode = taskTypeCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTaskSonNumber() {
        return taskSonNumber;
    }

    public void setTaskSonNumber(String taskSonNumber) {
        this.taskSonNumber = taskSonNumber;
    }
}
