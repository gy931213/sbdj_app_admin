package com.sbdj.service.finance.scheduler.vo;

import java.math.BigDecimal;

/**
 * <p>
 * 子任务佣金流水
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public class StatementTaskSonVo {
    /** 流水表id **/
    private Long id;
    /** 业务员id **/
    private Long salesmanId;
    /** 流水表金额 **/
    private BigDecimal money;
    /** 号主id **/
    private Long salesmanNumberId;
    /** 店铺id **/
    private Long sellerShopId;
    /** 商品id **/
    private Long goodsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }

    public Long getSellerShopId() {
        return sellerShopId;
    }

    public void setSellerShopId(Long sellerShopId) {
        this.sellerShopId = sellerShopId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }
}
