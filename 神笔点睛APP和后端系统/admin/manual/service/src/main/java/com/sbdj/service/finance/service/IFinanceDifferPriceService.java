package com.sbdj.service.finance.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.finance.admin.vo.DifferPriceVo;
import com.sbdj.service.finance.admin.query.DifferPriceQuery;
import com.sbdj.service.finance.entity.FinanceDifferPrice;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 财务模块-（子任务差价表） 服务类
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
public interface IFinanceDifferPriceService extends IService<FinanceDifferPrice> {
    /**
     * @return void 返回类型
     * @Title: saveDifferPrice
     * @Description 增加差价
     * @Param [differPrice]
     * @Author 杨凌云
     * @Date 2019/5/27 15:04
     **/
    void saveDifferPrice(FinanceDifferPrice differPrice);

    /**
     * @return void 返回类型
     * @Title: deleteDifferPrice
     * @Description 删除差价
     * @Param [id]
     * @Author 杨凌云
     * @Date 2019/5/27 15:04
     **/
    void deleteDifferPrice(Long id);

    /**
     * @return void 返回类型
     * @Title: updateDifferPrice
     * @Description 更新差价
     * @Param [differPrice]
     * @Author 杨凌云
     * @Date 2019/5/27 15:27
     **/
    void updateDifferPrice(FinanceDifferPrice differPrice);

    /**
     * @return com.shenbi.biz.finance.domain.DifferPrice 返回类型
     * @Title: findDifferPriceByTaskSonId
     * @Description 通过子任务编号
     * @Param [taskSonId]
     * @Author 杨凌云
     * @Date 2019/5/27 16:35
     **/
    FinanceDifferPrice findDifferPriceByTaskSonId(Long taskSonId);

    /**
     * @return org.springframework.data.domain.Page<com.shenbi.biz.finance.extend.DifferPriceExt> 返回类型
     * @Title: findDifferPriceByPage
     * @Description 分页查询
     * @Param [params, pageable]
     * @Author 杨凌云
     * @Date 2019/5/27 16:02
     **/
    IPage<DifferPriceVo> findDifferPriceByPage(DifferPriceQuery params, IPage<DifferPriceVo> page);

    /**
     * @return java.util.List<com.shenbi.biz.finance.extend.DifferPriceExt> 返回类型
     * @Title: findDifferPriceByIds
     * @Description 通过id数组查询差价
     * @Param [ids]
     * @Author 杨凌云
     * @Date 2019/5/29 14:48
     **/
    List<DifferPriceVo> findDifferPriceByIds(String ids);

    /**
     * @return void 返回类型
     * @Title: reviewDifferPrice
     * @Description 审核差价
     * @Param [id, startStatus, endStatus, oprId, onlineid]
     * @Author 杨凌云
     * @Date 2019/5/29 14:49
     **/
    void reviewDifferPrice(Long id, String beginStatus, String endStatus, Long oprId, String onlineid) throws Exception;
}
