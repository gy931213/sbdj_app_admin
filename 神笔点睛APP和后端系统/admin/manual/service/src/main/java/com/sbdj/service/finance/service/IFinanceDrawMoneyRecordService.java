package com.sbdj.service.finance.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.finance.admin.query.DrawMoneyRecordQuery;
import com.sbdj.service.finance.admin.vo.DrawMoneyRecordVo;
import com.sbdj.service.finance.entity.FinanceDrawMoneyRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 财务模块-业务员提现记录 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface IFinanceDrawMoneyRecordService extends IService<FinanceDrawMoneyRecord> {

    IPage<DrawMoneyRecordVo> findDrawMoneyRecordPage(DrawMoneyRecordQuery params, IPage<DrawMoneyRecordVo> buildPageRequest);

    void updateDrawMoneyRecordStatusById(Long id, Long identify, String statusAlreadyPay);

    /**
     * @param salesmanId
     * @return Page<DrawMoneyRecordVo> 返回类型
     * @Title: findRecordList
     * @Description: (这里用一句话描述这个方法的作用)
     */
    IPage<DrawMoneyRecordVo> findRecordList(Long salesmanId, IPage<DrawMoneyRecordVo> page);
    /**
     * @param salesmanId 业务员id
     * @return DrawMoneyRecord 返回类型
     * @Title: findFailRecord
     * @Description: (查询审核中的提现记录)
     */
    FinanceDrawMoneyRecord findFailRecord(Long salesmanId);

    /**
     * 批量提现操作
     * @author Yly
     * @date 2019-11-30 17:57
     * @param auditorId 审核人
     * @param list 提现数据
     * @return void
     */
    void batchCashWithdrawal(Long auditorId, List<String[]> list);
}
