package com.sbdj.service.finance.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.finance.admin.vo.PayGoodsVo;
import com.sbdj.service.finance.admin.query.PayGoodsQuery;
import com.sbdj.service.finance.entity.FinancePayGoods;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 财务模块-货款表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface IFinancePayGoodsService extends IService<FinancePayGoods> {

    /**
     * @param id 编号
     * @return FinancePayGoods 返回类型
     * @Title: findPayGoodsOne
     * @Description: 获取单条数据的接口
     */
    public FinancePayGoods findPayGoodsOne(Long id);

    /**
     * @param payGoods 要保存的数据
     * @return void 返回类型
     * @Title: savePayGoods
     * @Description: 保存数据的接口
     */
    public void savePayGoods(FinancePayGoods payGoods);

    /**
     * @param payGoods 要更新的数据
     * @return void 返回类型
     * @Title: updatePayGoods
     * @Description: 更新数据的接口
     */
    public void updatePayGoods(FinancePayGoods payGoods);

    /**
     * @param id 要删除的id
     * @return void 返回类型
     * @Title: deletePayGoods
     * @Description: 删除数据的接口
     */
    public void deletePayGoods(Long id);

    void deletesPayGoods(String ids);

    /**
     * @param id        要更新的id
     * @param auditorId 审核人
     * @param status    状态
     * @return void 返回类型
     * @Title: updatePayGoodsStatus
     * @Description:更新数据状态的接口
     */
    public void updatePayGoodsStatus(Long id, Long auditorId, String status);

    /**
     * @param params   查询参数
     * @param page 分页参数
     * @param page 分页参数
     * @return Page<PayGoodsExt> 返回类型
     * @Title: findPayGoodsPageList
     * @Description: 获取货款分页数据的接口
     */
    public IPage<PayGoodsVo> findPayGoodsPageList(PayGoodsQuery params, IPage<PayGoodsVo> page);

    /**
     * 进行货款对账接口
     *
     * @param auditorId 审核人
     * @param list      待对账数据
     */
    public void payGoodsBalance(Long auditorId, List<String[]> list);

   // FinancePayGoods findPayGoodsRe(Long taskSonId, Long salesmanId, Long salesmanNumberId);

    /**
     * Created by Htl to 2019/06/11 <br/>
     * 根据参数验证该货款是否已经生成的验证接口
     *
     * @param taskSonId        子任务id
     * @param salesmanId       业务员id
     * @param salesmanNumberId 号主id
     * @return [是：true，否：false]
     */
    boolean verifyPayGoods(Long taskSonId, Long salesmanId, Long salesmanNumberId);
}
