package com.sbdj.service.finance.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.finance.admin.query.PercentageQuery;
import com.sbdj.service.finance.admin.vo.PercentageVo;
import com.sbdj.service.finance.app.vo.PercentageListVo;
import com.sbdj.service.finance.app.vo.PercentageTotalVo;
import com.sbdj.service.finance.entity.FinancePercentage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 业务员提成明细表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface IFinancePercentageService extends IService<FinancePercentage> {

    IPage<PercentageVo> findPercentagePage(PercentageQuery params, IPage<PercentageVo> buildPageRequest);
    /**
     * @param salesmanId 业务员id
     * @return Page<PercentageTotalVo> 返回类型
     * @Title: findPercentageList
     * @Description: (查询每天提成总额)
     */
    IPage<PercentageTotalVo> findPercentageList(IPage<PercentageTotalVo> page, Long salesmanId);

    /**
     * @param salesmanId 业务员id
     * @param time       时间
     * @return List<PercentageListVo> 返回类型
     * @Title:findPercentageList
     * @Description: (查询每天提成详细列表)
     */
    List<PercentageListVo> findPercentageList(Long salesmanId, String time);
}
