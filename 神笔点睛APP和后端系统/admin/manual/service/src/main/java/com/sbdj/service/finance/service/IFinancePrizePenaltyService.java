package com.sbdj.service.finance.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.finance.admin.vo.PrizePenaltyVo;
import com.sbdj.service.finance.admin.query.PrizePenaltyQuery;
import com.sbdj.service.finance.entity.FinancePrizePenalty;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.poi.ss.formula.functions.Finance;

/**
 * <p>
 * 财务模块-业务员奖惩表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface IFinancePrizePenaltyService extends IService<FinancePrizePenalty> {
    /**
     * @return org.springframework.data.domain.Page<com.shenbi.biz.finance.extend.PrizePenaltyExt> 返回类型
     * @Title: findPrizePenaltyRecordPage
     * @Description 查询业务员奖罚列表
     * @Param [params, pageable]
     * @Author 杨凌云
     * @Date 2019/2/17 16:04
     **/
    IPage<PrizePenaltyVo> findPrizePenaltyRecordPage(PrizePenaltyQuery params, IPage<PrizePenaltyVo> page);

    /**
     * @param salesmanId
     * @return Page<PrizePenaltyVo> 返回类型
     * @Title: findPenaltyList
     * @Description: (查询当前业务员奖惩列表)
     */
    IPage<PrizePenaltyVo> findPenaltyList(Long salesmanId, IPage<PrizePenaltyVo> page, String type);

    /**
     * @param salesmanId
     * @return PrizePenalty 返回类型
     * @Title: findPenaltyList
     * @Description: (查询当前业务员奖惩列表)
     */
    PrizePenaltyVo findPenaltyList(Long salesmanId, String type);

    /**
     * 保存奖罚记录
     * @author Yly
     * @date 2019-11-30 17:08
     * @param prizePenalty 奖罚实体
     * @return void
     */
    void savePrizePenal(FinancePrizePenalty prizePenalty);
}
