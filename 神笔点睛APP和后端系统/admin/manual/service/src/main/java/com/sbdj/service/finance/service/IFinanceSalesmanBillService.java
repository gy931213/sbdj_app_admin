package com.sbdj.service.finance.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.finance.admin.query.SalesmanBillQuery;
import com.sbdj.service.finance.admin.vo.SalesmanBillVo;
import com.sbdj.service.finance.app.vo.SalesmanBillRespVo;
import com.sbdj.service.finance.entity.FinanceSalesmanBill;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 财务模块-(业务员账单) 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface IFinanceSalesmanBillService extends IService<FinanceSalesmanBill> {
    /**
     * 分页展示业务员账单
     * @author Yly
     * @date 2019-11-27 21:46
     * @param query
     * @param iPage
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.finance.admin.vo.SalesmanBillVo>
     */
    IPage<SalesmanBillVo> findSalesmanBillByPage(SalesmanBillQuery query, IPage<SalesmanBillVo> iPage);
    /**
     * Created by Htl to 2019/07/09 <br/>
     * 获取账单分页信息的接口
     *
     * @param salesmanId  业务员id
     * @param startDate   开始日期
     * @param endDate     结束日期
     * @param consumeType 收支类型
     * @param billTypeId  账单类型
     * @param page    分页参数
     * @return
     */
    IPage<SalesmanBillRespVo> findSalesmanBillRespVo(Long salesmanId, String startDate, String endDate, String consumeType, Integer billTypeId, IPage<SalesmanBillRespVo> page);
    /**
     * Created by Htl to 2019/07/09 <br/>
     * 获取业务员总金额的接口
     *
     * @param salesmanId  业务员id
     * @param startDate   开始日期
     * @param endDate     结束日期
     * @param consumeType 收支类型
     * @param billTypeId  账单类型
     * @return
     */
    BigDecimal salesmanBillTotalMoney(Long salesmanId, String startDate, String endDate, String consumeType, Integer billTypeId);

    /**
     * Created by Htl to 2019/07/09 <br/>
     * 获取业务员的总收入，昨日收益的数据接口
     *
     * @param salesmanId 业务员id
     * @return
     */
    Map<String, Object> salesmanBillInfo(Long salesmanId);
}
