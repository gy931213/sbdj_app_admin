package com.sbdj.service.finance.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.finance.entity.SalesmanCreditRecord;
import com.sbdj.service.member.scheduler.vo.SalesmanCreditRecordVo;

/**
 * <p>
 * 业务员信用增减记录表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public interface ISalesmanCreditRecordService extends IService<SalesmanCreditRecord> {

    IPage<SalesmanCreditRecordVo> findCreditList(IPage<Object> buildPageRequest, Long uid);
}
