package com.sbdj.service.finance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.finance.admin.query.DifferPriceQuery;
import com.sbdj.service.finance.admin.vo.DifferPriceVo;
import com.sbdj.service.finance.entity.FinanceDifferPrice;
import com.sbdj.service.finance.entity.FinancePrizePenalty;
import com.sbdj.service.finance.entity.FinanceSalesmanBill;
import com.sbdj.service.finance.mapper.FinanceDifferPriceMapper;
import com.sbdj.service.finance.service.IFinanceDifferPriceService;
import com.sbdj.service.finance.service.IFinancePrizePenaltyService;
import com.sbdj.service.finance.service.IFinanceSalesmanBillService;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.member.type.SalesmanBillTypeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 财务模块-（子任务差价表） 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class FinanceDifferPriceServiceImpl extends ServiceImpl<FinanceDifferPriceMapper, FinanceDifferPrice> implements IFinanceDifferPriceService {

    private static Logger logger = LoggerFactory.getLogger(FinanceDifferPriceServiceImpl.class);
 
    @Lazy
    @Autowired
    private IFinanceDifferPriceService iDifferPriceService;
    @Lazy
    @Autowired
    private ISalesmanService iSalesmanService;
    @Autowired
    private IFinanceSalesmanBillService iFinanceSalesmanBillService;
    @Lazy
    @Autowired
    private IFinancePrizePenaltyService iFinancePrizePenaltyService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveDifferPrice(FinanceDifferPrice differPrice) {
        differPrice.setStatus(Status.STATUS_NO);
        save(differPrice);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteDifferPrice(Long id) {
        baseMapper.deleteById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateDifferPrice(FinanceDifferPrice differPrice) {
        if (StrUtil.isNull(differPrice)) {
            throw BaseException.base("差价实体为空");
        } else if (StrUtil.isNull(differPrice.getId())) {
            throw BaseException.base("差价编号为空");
        }
        updateById(differPrice);
    }

    @Override
    public FinanceDifferPrice findDifferPriceByTaskSonId(Long taskSonId) {
        QueryWrapper<FinanceDifferPrice> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(taskSonId),"task_son_id",taskSonId);
        return getOne(queryWrapper);
    }

    @Override
    public IPage<DifferPriceVo> findDifferPriceByPage(DifferPriceQuery params, IPage<DifferPriceVo> page) {
        QueryWrapper<DifferPriceQuery> queryWrapper = new QueryWrapper<>();
        // 机构号
        queryWrapper.eq(StrUtil.isNotNull(params.getOrgId()),"fdp.org_id",params.getOrgId());
        // 店铺名称
        queryWrapper.eq(StrUtil.isNotNull(params.getShopName()),"ss.shop_name",params.getShopName());
        // 商品链接
        queryWrapper.like(StrUtil.isNotNull(params.getLink()),"ssg.link",params.getLink());
        // 业务员名称
        queryWrapper.like(StrUtil.isNotNull(params.getSalesmanName()),"s.`name`",params.getSalesmanName());
        // 号主名称
        queryWrapper.like(StrUtil.isNotNull(params.getOnlineid()),"sn.onlineid",params.getOnlineid());
        // 数据状态
        queryWrapper.eq(StrUtil.isNotNull(params.getStatus()),"fdp.`status`",params.getStatus());
        // 开始时间
        queryWrapper.ge(StrUtil.isNotNull(params.getBeginTime()),"fdp.create_time",params.getBeginTime());
        // 结束时间
        queryWrapper.le(StrUtil.isNotNull(params.getEndTime()),"fdp.create_time",params.getEndTime());
        // 差价类型 GT 大于(收入) LT 小于(支出)
        if (StrUtil.isNotNull(params.getDifferType())) {
            if (params.getDifferType().equalsIgnoreCase("GT")) {
                queryWrapper.gt("fdp.differ_price",0);
            } else if (params.getDifferType().equalsIgnoreCase("LT")) {
                queryWrapper.lt(StrUtil.isNotNull(),"fdp.differ_price",0);
            }
        }
        // 过滤状态为'D'
        queryWrapper.notIn("fdp.`status`",Status.STATUS_DELETE);
        // 任务类型
        queryWrapper.eq(StrUtil.isNotNull(params.getTaskTypeId()) && !Number.LONG_ZERO.equals(params.getTaskTypeId()),"fdp.task_type_id",params.getTaskTypeId());
        return baseMapper.findDifferPriceByPage(page,queryWrapper);
    }


    @Override
    public List<DifferPriceVo> findDifferPriceByIds(String ids) {
        if (StrUtil.isNull(ids)){
            throw BaseException.base("ID不能为空！");
        }
        String[] idss = ids.split(",");
        List<DifferPriceVo> list = new ArrayList<>();
        for (String  id : idss) {
            list.add(baseMapper.findDifferPriceByIds(id));
        }
        return  list ;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void reviewDifferPrice(Long id, String beginStatus, String endStatus, Long oprId, String onlineid) throws Exception {
        // 过滤已审核的差价
        if (beginStatus.equals(Status.STATUS_YES)) {
            return;
        }
        // 获取差价
        FinanceDifferPrice dp = getById(id);
        dp.setOprId(oprId);
        dp.setOprTime(new Date());
        dp.setStatus(endStatus);

        if (endStatus.equals(Status.STATUS_YES)) {
            BigDecimal money = dp.getDifferPrice().abs();

            // 获取业务员
            Salesman salesman = iSalesmanService.getById(dp.getSalesmanId());
            // 判断业务员余额是否能够支付差价
            if (dp.getDifferPrice().compareTo(BigDecimal.ZERO) > 0 && money.compareTo(salesman.getBalance()) > 0) {
                logger.error("业务员余额不足,无法处理 业务员编号:{} 当前余额:{} 需要扣除金额:{}", salesman.getId(), salesman.getBalance(), money);
                dp.setStatus(Status.STATUS_NO);
                throw new Exception("业务员余额不足,无法处理 业务员工号:" +salesman.getJobNumber());
            }

            // 生成奖罚
            FinancePrizePenalty prizePenalty = new FinancePrizePenalty();
            prizePenalty.setCreateTime(new Date());
            prizePenalty.setOrgId(dp.getOrgId());
            prizePenalty.setSalesmanId(dp.getSalesmanId());
            prizePenalty.setSalesmanNumberId(dp.getSalesmanNumberId());
            prizePenalty.setTaskId(dp.getTaskId());
            prizePenalty.setTaskSonId(dp.getTaskSonId());
            prizePenalty.setReason(SalesmanBillTypeMsg.BILL_TYPE_DIFFER.getMessage()+ " - " + onlineid);
            prizePenalty.setAuditorId(dp.getOprId());
            prizePenalty.setAuditorTime(new Date());
            prizePenalty.setMoney(money);
            // 差价为负则为奖励,差价为正则为惩罚
            prizePenalty.setType(dp.getDifferPrice().compareTo(BigDecimal.ZERO)>0?Status.PRICE_PUNISH:Status.PRICE_REWORD);
            prizePenalty.setStatus(Status.STATUS_ENABLE);

            // 生成账单
            FinanceSalesmanBill salesmanBill = new FinanceSalesmanBill();
            salesmanBill.setOrgId(dp.getOrgId());
            salesmanBill.setSalesmanId(dp.getSalesmanId());
            salesmanBill.setBillType(SalesmanBillTypeMsg.BILL_TYPE_DIFFER.getCodeType());
            salesmanBill.setTargetId(dp.getId().toString());
            salesmanBill.setLastMoney(salesman.getBalance());
            salesmanBill.setMoney(dp.getDifferPrice().negate());
            // 差价为负则为增加,差价为正则为减少
            salesmanBill.setFinalMoney(salesman.getBalance().subtract(dp.getDifferPrice()));
            salesmanBill.setAccount(SalesmanBillTypeMsg.BILL_TYPE_DIFFER.getMessage() + " - " + onlineid);
            salesmanBill.setBillDate(new Date());
            salesmanBill.setCreateTime(new Date());
            // 审核差价
            iDifferPriceService.updateDifferPrice(dp);
            // 更新业务员余额 差价为负则为增加,差价为正则为减少
            iSalesmanService.updateSalesmanMoney(prizePenalty.getSalesmanId(), salesman.getBalance().subtract(dp.getDifferPrice()));
            // 保存奖罚
            iFinancePrizePenaltyService.savePrizePenal(prizePenalty);
            // 保存账单
            iFinanceSalesmanBillService.save(salesmanBill);
            // 审核差价
            iDifferPriceService.updateDifferPrice(dp);
        } else if (endStatus.equals(Status.STATUS_DELETE)) {
            // 审核差价
            iDifferPriceService.updateDifferPrice(dp);
        }
    }


}
