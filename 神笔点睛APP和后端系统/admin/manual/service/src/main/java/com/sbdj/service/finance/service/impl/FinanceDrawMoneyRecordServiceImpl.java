package com.sbdj.service.finance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.finance.admin.query.DrawMoneyRecordQuery;
import com.sbdj.service.finance.admin.vo.DrawMoneyRecordVo;
import com.sbdj.service.finance.entity.FinanceDrawMoneyRecord;
import com.sbdj.service.finance.mapper.FinanceDrawMoneyRecordMapper;
import com.sbdj.service.finance.service.IFinanceDrawMoneyRecordService;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.service.ISalesmanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 财务模块-业务员提现记录 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class FinanceDrawMoneyRecordServiceImpl extends ServiceImpl<FinanceDrawMoneyRecordMapper, FinanceDrawMoneyRecord> implements IFinanceDrawMoneyRecordService {

    @Autowired
    @Lazy
    private IFinanceDrawMoneyRecordService iFinanceDrawMoneyRecordService;
    @Autowired
    private ISalesmanService iSalesmanService;

    @Override
    public IPage<DrawMoneyRecordVo> findDrawMoneyRecordPage(DrawMoneyRecordQuery params, IPage<DrawMoneyRecordVo> buildPageRequest) {
        QueryWrapper<DrawMoneyRecordVo> queryWrapper = new QueryWrapper<>();
        // ~工号
        queryWrapper.eq(StrUtil.isNotNull(params.getJobNumber()),"s.job_number",params.getJobNumber());
        // ~业务员姓名
        queryWrapper.eq(StrUtil.isNotNull(params.getSalesmanName()),"s.name",params.getSalesmanName());
        // ~电话号码
        queryWrapper.eq(StrUtil.isNotNull(params.getMobile()),"s.mobile",params.getMobile());
        // ~申请提现金额
        queryWrapper.eq(StrUtil.isNotNull(params.getApplyMoney()),"fdmr.apply_money",params.getApplyMoney());
        // ~状态
        queryWrapper.eq(StrUtil.isNotNull(params.getStatus()),"fdmr.status",params.getStatus());
        // 所属机构
        queryWrapper.eq(StrUtil.isNotNull(params.getOrgId()) && !Number.LONG_ZERO.equals(params.getOrgId()),"fdmr.org_id",params.getOrgId());
        // ~申请开始提现时间
        queryWrapper.ge(StrUtil.isNotNull(params.getStartTime()),"fdmr.create_time",  params.getStartTime());
        // ~申请结束提现时间
        queryWrapper.le(StrUtil.isNotNull(params.getEndTime()),"fdmr.create_time",params.getEndTime());
        return baseMapper.findDrawMoneyRecordPage(buildPageRequest, queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateDrawMoneyRecordStatusById(Long id, Long identify, String status) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新提现金额失败,id为空");
        }
        UpdateWrapper<FinanceDrawMoneyRecord> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status",status).set("auditor_id",identify).set("auditor_time",new Date()).eq("id",id);
        update(updateWrapper);
    }

    @Override
    public IPage<DrawMoneyRecordVo> findRecordList(Long salesmanId, IPage<DrawMoneyRecordVo> page) {
        return  baseMapper.findRecordList(page,salesmanId);
    }

    @Override
    public FinanceDrawMoneyRecord findFailRecord(Long salesmanId) {
        QueryWrapper<FinanceDrawMoneyRecord> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id" ,"salesman_id","apply_money" ,"`status`");
        queryWrapper.eq(StrUtil.isNotNull(salesmanId),"salesman_id",salesmanId);
        queryWrapper.eq("`status`",Status.STATUS_STOP);
        queryWrapper.last("limit 1");
        return  getOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchCashWithdrawal(Long auditorId, List<String[]> list) {
        for (int i = 1; i < list.size(); i++) {
            String[] array = list.get(i);
            if(null == array ||  null == array[0]) {
                continue;
            }
            // 获取提现数据！
            FinanceDrawMoneyRecord dmr = getById(Long.valueOf(array[0]));
            if(null == dmr) {
                continue;
            }

            if(dmr.getStatus().equalsIgnoreCase(Status.STATUS_ALREADY_PAY)) {
                continue;
            }

            updateDrawMoneyRecordStatusById(dmr.getId(), auditorId,Status.STATUS_ALREADY_PAY);
			// 获取提现，所属业务员信息！
			Salesman sm = iSalesmanService.getById(dmr.getSalesmanId());
			if(null == sm || null ==sm.getBalance()) {
				continue;
			}

			// 判断提现的金额是否是大于所属业务员剩余余额！
			// -1小于，0等于，1 大于
			if(dmr.getApplyMoney().compareTo(sm.getBalance()) < 0 || dmr.getApplyMoney().compareTo(sm.getBalance()) == 0) {
				iSalesmanService.updateSalesmanMoney(dmr.getSalesmanId(), sm.getBalance().subtract(dmr.getApplyMoney()));
				updateDrawMoneyRecordStatusById(dmr.getId(), auditorId,Status.STATUS_ALREADY_PAY);
			}
        }
    }
}
