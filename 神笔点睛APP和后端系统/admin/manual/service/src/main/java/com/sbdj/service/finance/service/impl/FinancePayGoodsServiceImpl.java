package com.sbdj.service.finance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.DateUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.finance.admin.query.PayGoodsQuery;
import com.sbdj.service.finance.admin.vo.PayGoodsVo;
import com.sbdj.service.finance.entity.FinancePayGoods;
import com.sbdj.service.finance.mapper.FinancePayGoodsMapper;
import com.sbdj.service.finance.service.IFinancePayGoodsService;
import com.sbdj.service.system.type.SysConfigType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 财务模块-货款表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class FinancePayGoodsServiceImpl extends ServiceImpl<FinancePayGoodsMapper, FinancePayGoods> implements IFinancePayGoodsService {

    @Override
    public FinancePayGoods findPayGoodsOne(Long id) {
        return baseMapper.selectById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void savePayGoods(FinancePayGoods payGoods) {
        payGoods.setCreateTime(new Date());
        payGoods.setStatus(Status.STATUS_NOT_PAY);
        payGoods.setExport(Number.INT_ZERO);
        save(payGoods);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updatePayGoods(FinancePayGoods payGoods) {
        updateById(payGoods);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deletePayGoods(Long id) {
        removeById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deletesPayGoods(String ids) {
        if (StrUtil.isNotNull(ids)) {
            String [] str = ids.split(",");
            for (String  id : str) {
                if (StrUtil.isNotNull(id)) {
                    removeById(Long.parseLong(id));
                }
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updatePayGoodsStatus(Long id, Long auditorId, String status) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新货款状态失败,id为空");
        }
        UpdateWrapper<FinancePayGoods> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", status);
        updateWrapper.set("auditor_id", auditorId);
        updateWrapper.apply("pay_money=apply_money");
        updateWrapper.set("pay_time", new Date());
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Override
    public IPage<PayGoodsVo> findPayGoodsPageList(PayGoodsQuery params, IPage<PayGoodsVo> page) {
        QueryWrapper<PayGoodsQuery> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ssg.status",Status.STATUS_ACTIVITY);
        queryWrapper.eq("ss.status",Status.STATUS_ACTIVITY);
        queryWrapper.eq("sc.code",SysConfigType.TASK_TYPE);
        // 所属机构
        queryWrapper.eq(StrUtil.isNotNull(params.getOrgId()) && !Number.LONG_ZERO.equals(params.getOrgId())," fpg.org_id",params.getOrgId());
        // 判断是否开启历史查询 目前只展示5天前的数据。
        Date date = DateUtil.genDayDate(-5, new Date());
        queryWrapper.ge(StrUtil.isNotNull(params.getIsHistory()) && Status.STATUS_NO.equalsIgnoreCase(params.getIsHistory()),"fpg.create_time",date);
        // 状态
        queryWrapper.eq(StrUtil.isNotNull( params.getStatus()),"fpg.status", params.getStatus());
        // 开始时间
        queryWrapper.ge(StrUtil.isNotNull(params.getStartTime()),"fpg.create_time",params.getStartTime());
        // 结束时间
        queryWrapper.le(StrUtil.isNotNull(params.getEndTime()),"fpg.create_time",params.getEndTime());
        // 业务员工号
        queryWrapper.eq(StrUtil.isNotNull(params.getJobNumber()),"s.job_number",params.getJobNumber());
        // 姓名
        queryWrapper.eq(StrUtil.isNotNull(params.getName()),"s.name",params.getName());
        // 货款类型
        queryWrapper.eq(StrUtil.isNotNull(params.getPgtype()),"fpg.type",params.getPgtype());
        // 订单类型
        queryWrapper.eq(StrUtil.isNotNull(params.getTaskTypeId()) && !Number.LONG_ZERO.equals(params.getTaskTypeId()),"ts.task_type_id",params.getTaskTypeId());

        // 作废状态
        boolean flag = true;
        // 任务状态
        if(StrUtil.isNotNull(params.getTaskStatus())){
            queryWrapper.eq("ts.status",params.getTaskStatus());
            if (params.getTaskStatus().equals(Status.STATUS_NULLIFY)) {
                flag = false;
            }
        }
        queryWrapper.notIn(flag,"ts.status",Status.STATUS_NULLIFY);
        // 关键词
        queryWrapper.and(StrUtil.isNotNull(params.getKeyword()),f->f.like("fpg.apply_number",params.getKeyword())
                .or().like(StrUtil.isNotNull(params.getKeyword()),"s.job_number",params.getKeyword())
                .or().like(StrUtil.isNotNull(params.getKeyword()),"s.mobile",params.getKeyword())
                .or().like(StrUtil.isNotNull(params.getKeyword()),"s.name",params.getKeyword()));
        queryWrapper.orderByAsc("fpg.id");

        IPage<PayGoodsVo> pageList = baseMapper.findPayGoodsPageList(page,queryWrapper);
        // 数据拼接
        for (PayGoodsVo payGoodsVo : pageList.getRecords()) {
            // 货款类型
            if (Status.APPLY_PAY.equalsIgnoreCase(payGoodsVo.getType())) {
                payGoodsVo.setTypeName("申请");
            } else if (Status.ADVANCE_PAY.equalsIgnoreCase(payGoodsVo.getType())) {
                payGoodsVo.setTypeName("垫付");
            }
            // 任务类型
            switch (payGoodsVo.getTaskStatus()) {
                case Status.STATUS_ACTIVITY:
                    payGoodsVo.setTaskStatusName("开始");
                    break;
                case Status.STATUS_NOT_AUDITED:
                    payGoodsVo.setTaskStatusName("待提交");
                    break;
                case Status.STATUS_ALREADY_UPLOAD:
                    payGoodsVo.setTaskStatusName("已传图");
                    break;
                case Status.STATUS_NOT_PASS:
                    payGoodsVo.setTaskStatusName("不通过");
                    break;
                case Status.STATUS_AUDITED_FAIL:
                    payGoodsVo.setTaskStatusName("已失败");
                    break;
                case Status.STATUS_YES_AUDITED:
                    payGoodsVo.setTaskStatusName("已审核");
                    break;
                case Status.STATUS_APPEAL:
                    payGoodsVo.setTaskStatusName("申述中");
                    break;
                case Status.STATUS_NULLIFY:
                    payGoodsVo.setTaskStatusName("已作废");
                    break;
            }
        }
        return pageList;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void payGoodsBalance(Long auditorId, List<String[]> list) {
        List<String> arid = new ArrayList<>();
        List<String> arpay = new ArrayList<>();
        for (int i = 1; i < list.size(); i++) {
            String[] array = list.get(i);
            if (null != array) {
                if (null != array[0]) {
                    arid.add(array[0]);
                }
                if (null != array[1]) {
                    arpay.add(array[1]);
                }
            }
        }
        if (arid.size() == arpay.size()) {
            UpdateWrapper<FinancePayGoods> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("status", Status.STATUS_ALREADY_PAY);
            updateWrapper.set("auditor_id", auditorId);
            updateWrapper.apply("pay_money=apply_money");
            updateWrapper.set("pay_time", new Date());
            updateWrapper.eq("status", Status.STATUS_NOT_PAY);
            updateWrapper.in("id", arid);
            updateWrapper.in("apply_number", arpay);
            update(updateWrapper);
        }
        //baseMapper.payGoodsBalance(auditorId,arid,arpay);
    }

    @Override
    public boolean verifyPayGoods(Long taskSonId, Long salesmanId, Long salesmanNumberId) {
        QueryWrapper<FinancePayGoods> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(taskSonId),"task_son_id",taskSonId);
        queryWrapper.eq(StrUtil.isNotNull(salesmanId),"salesman_id",salesmanId);
        queryWrapper.eq(StrUtil.isNotNull(salesmanNumberId),"salesman_number_id",salesmanNumberId);
        Integer count= baseMapper.selectCount(queryWrapper);
        return StrUtil.isNotNull(count) && count > 0;
    }
}
