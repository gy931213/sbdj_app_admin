package com.sbdj.service.finance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.finance.admin.query.PercentageQuery;
import com.sbdj.service.finance.admin.vo.PercentageVo;
import com.sbdj.service.finance.app.vo.PercentageListVo;
import com.sbdj.service.finance.app.vo.PercentageTotalVo;
import com.sbdj.service.finance.entity.FinancePercentage;
import com.sbdj.service.finance.mapper.FinancePercentageMapper;
import com.sbdj.service.finance.service.IFinancePercentageService;
import com.sbdj.service.member.type.SalesmanBillType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 业务员提成明细表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class FinancePercentageServiceImpl extends ServiceImpl<FinancePercentageMapper, FinancePercentage> implements IFinancePercentageService {

    @Override
    public IPage<PercentageVo> findPercentagePage(PercentageQuery params, IPage<PercentageVo> page) {
        QueryWrapper<PercentageVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("fsb.bill_type", SalesmanBillType.BILL_TYPE_ROYALTY);
        queryWrapper.eq(StrUtil.isNotNull(params.getJobNumber()), "sm.job_number", params.getJobNumber());
        queryWrapper.eq(StrUtil.isNotNull(params.getName()), "sm.name", params.getName());
        queryWrapper.eq(StrUtil.isNotNull(params.getDate()), "fsb.bill_date", params.getDate());
        queryWrapper.eq(StrUtil.isNotNull(params.getOrgId()) && !Number.LONG_ZERO.equals(params.getOrgId()), "fsb.org_id", params.getOrgId());
        queryWrapper.orderByDesc("fsb.bill_date");
        return baseMapper.findPercentagePage(page, queryWrapper);
    }

    @Override
    public IPage<PercentageTotalVo> findPercentageList(IPage<PercentageTotalVo> page, Long salesmanId) {
        return baseMapper.findPercentageList(page,salesmanId);
    }

    @Override
    public List<PercentageListVo> findPercentageList(Long salesmanId, String time) {
        return baseMapper.findPercentageListByTime(salesmanId,time);
    }
}
