package com.sbdj.service.finance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.finance.admin.query.PrizePenaltyQuery;
import com.sbdj.service.finance.admin.vo.PrizePenaltyVo;
import com.sbdj.service.finance.entity.FinancePrizePenalty;
import com.sbdj.service.finance.entity.FinanceSalesmanBill;
import com.sbdj.service.finance.mapper.FinancePrizePenaltyMapper;
import com.sbdj.service.finance.service.IFinancePrizePenaltyService;
import com.sbdj.service.finance.service.IFinanceSalesmanBillService;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.member.type.SalesmanBillTypeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 财务模块-业务员奖惩表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class FinancePrizePenaltyServiceImpl extends ServiceImpl<FinancePrizePenaltyMapper, FinancePrizePenalty> implements IFinancePrizePenaltyService {

    private Logger logger = LoggerFactory.getLogger(FinancePrizePenalty.class);


    @Autowired
    private ISalesmanService iSalesmanService;
    @Autowired
    private IFinanceSalesmanBillService iFinanceSalesmanBillService;

    @Override
    public IPage<PrizePenaltyVo> findPrizePenaltyRecordPage(PrizePenaltyQuery params, IPage<PrizePenaltyVo> page) {
        QueryWrapper<PrizePenaltyQuery> queryWrapper = new QueryWrapper<>();
        // 工号
        queryWrapper.eq(StrUtil.isNotNull(params.getJobNumber()),"s.job_number",params.getJobNumber());
        // 业务员姓名
        queryWrapper.like(StrUtil.isNotNull(params.getSalesmanName()),"s.`name`",params.getSalesmanName());
        // 电话号码
        queryWrapper.eq(StrUtil.isNotNull(params.getMobile()),"s.mobile",params.getMobile());
        // 原因
        queryWrapper.like(StrUtil.isNotNull(params.getReason()),"fpp.reason",params.getReason());
        // 金额
        queryWrapper.eq(StrUtil.isNotNull(params.getMoney()) && params.getMoney().compareTo(BigDecimal.ZERO) != 0,"fpp.money",params.getMoney());
        // 类型
        queryWrapper.eq(StrUtil.isNotNull(params.getPrizeType()),"fpp.type",params.getPrizeType());
        // 审核人姓名
        queryWrapper.like(StrUtil.isNotNull(params.getAuditorName()),"sa.`name`",params.getAuditorName());
        // 开始时间
        queryWrapper.ge(StrUtil.isNotNull(params.getStartTime()),"fpp.create_time",params.getStartTime());
        // 结束时间
        queryWrapper.le(StrUtil.isNotNull(params.getEndTime()),"fpp.create_time",params.getEndTime());
        // 所属机构
        queryWrapper.eq(StrUtil.isNotNull(params.getOrgId()) && !Number.LONG_ZERO.equals(params.getOrgId()),"fpp.org_id",params.getOrgId());
        baseMapper.findPrizePenaltyRecordPage(page,queryWrapper);
        return page;
    }

    @Override
    public IPage<PrizePenaltyVo> findPenaltyList(Long salesmanId, IPage<PrizePenaltyVo> page, String type) {
        QueryWrapper<PrizePenaltyVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(salesmanId)," fp.salesman_id",salesmanId);
        queryWrapper.eq(StrUtil.isNotNull(type),"fp.`type`",type);
        queryWrapper.orderByDesc("fp.create_time");
        return baseMapper.findPenaltyList(page,queryWrapper);
    }

    @Override
    public PrizePenaltyVo findPenaltyList(Long salesmanId, String type) {
        return  baseMapper.findPenaltyList2(salesmanId,type);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void savePrizePenal(FinancePrizePenalty prizePenalty) {
        // 查询业务员
        Salesman salesman = iSalesmanService.getById(prizePenalty.getSalesmanId());
        if (StrUtil.isNull(salesman)) {
            logger.error("处理奖罚失败 业务员信息为空 业务员id:{}", prizePenalty.getSalesmanId());
            throw BaseException.base("业务员信息为空");
        }
        // 业务员余额
        BigDecimal balance = salesman.getBalance();
        // 奖罚金额
        BigDecimal money = BigDecimal.ZERO;
        if (prizePenalty.getType().equalsIgnoreCase("PR")) {
            money = prizePenalty.getMoney();
        } else if (prizePenalty.getType().equalsIgnoreCase("PE")) {
            money = prizePenalty.getMoney().negate();
        }
        // 判断惩罚金额
        if (money.compareTo(BigDecimal.ZERO) < 0 && money.negate().compareTo(balance) > 0) {
            logger.error("处理奖罚失败 惩罚金额过大 业务员id:{}", prizePenalty.getSalesmanId());
            throw BaseException.base("惩罚金额过大");
        }

        // 以后台时间为基准
        Date date = new Date();
        prizePenalty.setAuditorTime(date);
        prizePenalty.setCreateTime(date);
        // 更新业务员余额
        UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("balance", balance.add(money));
        updateWrapper.eq("id", prizePenalty.getSalesmanId());
        iSalesmanService.update(updateWrapper);
        // 生成奖罚信息
        save(prizePenalty);
        // 账单信息
        FinanceSalesmanBill salesmanBill = new FinanceSalesmanBill();
        salesmanBill.setSalesmanId(salesman.getId());
        salesmanBill.setOrgId(salesman.getOrgId());
        salesmanBill.setTargetId(prizePenalty.getId().toString());
        salesmanBill.setBillType(money.compareTo(BigDecimal.ZERO) > 0 ? SalesmanBillTypeMsg.BILL_TYPE_PRNALTY.getCodeType() : SalesmanBillTypeMsg.BILL_TYPE_PENALTY.getCodeType());
        salesmanBill.setAccount(prizePenalty.getReason());
        salesmanBill.setBillDate(date);
        salesmanBill.setMoney(money);
        salesmanBill.setLastMoney(salesman.getBalance());
        salesmanBill.setFinalMoney(salesman.getBalance().add(salesmanBill.getMoney()));

        salesmanBill.setCreateTime(date);
        // 生成账单
        iFinanceSalesmanBillService.save(salesmanBill);
    }
}
