package com.sbdj.service.finance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.finance.admin.query.SalesmanBillQuery;
import com.sbdj.service.finance.admin.vo.SalesmanBillVo;
import com.sbdj.service.finance.app.vo.SalesmanBillRespVo;
import com.sbdj.service.finance.entity.FinanceSalesmanBill;
import com.sbdj.service.finance.mapper.FinanceSalesmanBillMapper;
import com.sbdj.service.finance.service.IFinanceSalesmanBillService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 财务模块-(业务员账单) 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class FinanceSalesmanBillServiceImpl extends ServiceImpl<FinanceSalesmanBillMapper, FinanceSalesmanBill> implements IFinanceSalesmanBillService {

    @Override
    public IPage<SalesmanBillVo> findSalesmanBillByPage(SalesmanBillQuery query, IPage<SalesmanBillVo> iPage) {
        QueryWrapper<SalesmanBillQuery> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(query.getBillType()), "fsb.bill_type", query.getBillType());
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId()), "fsb.org_id", query.getOrgId());
        queryWrapper.eq(StrUtil.isNotNull(query.getJobNumber()), "s.job_number", query.getJobNumber());
        queryWrapper.eq(StrUtil.isNotNull(query.getSalesmanName()), "s.`name`", query.getSalesmanName());
        queryWrapper.eq(StrUtil.isNotNull(query.getMobile()), "s.mobile", query.getMobile());
        queryWrapper.ge(StrUtil.isNotNull(query.getStartDate()), "fsb.bill_date", query.getStartDate());
        queryWrapper.le(StrUtil.isNotNull(query.getEndDate()), "fsb.bill_date", query.getEndDate());
        return baseMapper.findSalesmanBillByPage(iPage, queryWrapper);
    }

    @Override
    public IPage<SalesmanBillRespVo> findSalesmanBillRespVo(Long salesmanId, String startDate, String endDate, String consumeType, Integer billTypeId, IPage<SalesmanBillRespVo> page) {
        QueryWrapper<SalesmanBillRespVo> queryWrapper = getFinanceSalesmanBillQueryWrapper(salesmanId, startDate, endDate, consumeType, billTypeId);
        queryWrapper.orderByDesc("fsb.bill_date");
        return baseMapper.findSalesmanBillRespVo(page,queryWrapper);
    }

    @Override
    public BigDecimal salesmanBillTotalMoney(Long salesmanId, String startDate, String endDate, String consumeType, Integer billTypeId) {
        QueryWrapper<FinanceSalesmanBill> queryWrapper = getFinanceSalesmanBillQueryWrapper(salesmanId, startDate, endDate, consumeType, billTypeId);
        return baseMapper.salesmanBillTotalMoney(queryWrapper);
    }

    private <T> QueryWrapper<T> getFinanceSalesmanBillQueryWrapper(Long salesmanId, String startDate, String endDate, String consumeType, Integer billTypeId) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(salesmanId), "fsb.salesman_id", salesmanId);
        // 账单类型
        queryWrapper.eq(StrUtil.isNotNull(billTypeId), "fsb.bill_type", billTypeId);
        // 时间段查询
        if (StrUtil.isNotNull(startDate) && StrUtil.isNotNull(endDate)) {
            queryWrapper.between("fsb.bill_date", startDate, endDate);
        } else if (StrUtil.isNotNull(startDate)) {
            queryWrapper.apply("date_format(fsb.bill_date,'%Y-%m') = '" + startDate + "'");
        }

        // 收支类型 ：[{收入 ：I},{支出 ：E}]
        if (StrUtil.isNotNull(consumeType) && consumeType.equalsIgnoreCase("I")) {
            queryWrapper.gt("fsb.money", 0);
        } else {
            queryWrapper.lt("fsb.money", 0);
        }
        return queryWrapper;
    }

    @Override
    public Map<String, Object> salesmanBillInfo(Long salesmanId) {
        Map<String, Object> map = new HashMap<>();
        // 总收入
        BigDecimal totalIncome = baseMapper.salesmanBillInfo(salesmanId);
        // 昨日收益
        BigDecimal yesterdayIncome = baseMapper.salesmanBillInfo2(salesmanId);

        map.put("totalIncome", totalIncome == null ? 0 : totalIncome);
        map.put("yesterdayIncome", yesterdayIncome == null ? 0 : yesterdayIncome);
        return map;
    }
}
