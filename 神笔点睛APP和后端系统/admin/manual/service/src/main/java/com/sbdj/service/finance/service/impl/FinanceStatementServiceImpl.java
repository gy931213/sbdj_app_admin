package com.sbdj.service.finance.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.service.finance.entity.FinanceStatement;
import com.sbdj.service.finance.mapper.FinanceStatementMapper;
import com.sbdj.service.finance.scheduler.vo.StatementTaskCreditVo;
import com.sbdj.service.finance.service.IFinanceStatementService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 佣金流水表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class FinanceStatementServiceImpl extends ServiceImpl<FinanceStatementMapper, FinanceStatement> implements IFinanceStatementService {
    @Override
    public List<StatementTaskCreditVo> findStatementTaskSon() {
        return baseMapper.findStatementTaskSon();
    }
}
