package com.sbdj.service.finance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.service.finance.entity.SalesmanCreditRecord;
import com.sbdj.service.finance.mapper.SalesmanCreditRecordMapper;
import com.sbdj.service.finance.service.ISalesmanCreditRecordService;
import com.sbdj.service.member.scheduler.vo.SalesmanCreditRecordVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 业务员信用增减记录表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@Transactional(rollbackFor = Exception.class)
@Service
public class SalesmanCreditRecordServiceImpl extends ServiceImpl<SalesmanCreditRecordMapper, SalesmanCreditRecord> implements ISalesmanCreditRecordService {

    @Override
    public IPage<SalesmanCreditRecordVo> findCreditList(IPage<Object> page, Long salesmanId) {
        QueryWrapper<SalesmanCreditRecordVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("salesman_id",salesmanId).orderByDesc("create_time");
        return baseMapper.findCreditList(page,queryWrapper);
    }


}
