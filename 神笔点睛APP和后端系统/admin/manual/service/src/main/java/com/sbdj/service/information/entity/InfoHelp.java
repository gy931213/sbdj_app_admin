package com.sbdj.service.information.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Transient;

/**
 * <p>
 * 帮助手册
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="InfoHelp对象", description="帮助手册")
public class InfoHelp implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id自增长")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "封面图片地址")
    private String coverImage;

    @ApiModelProperty(value = "类型：[TXT ：纯文字，IMG ：图片+文字，VID ：视频+文字]")
    private String type;

    @ApiModelProperty(value = "资源地址")
    private String resource;

    @ApiModelProperty(value = "创建人id")
    private Long createId;

    @ApiModelProperty(value = "状态")
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }
    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "InfoHelp{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", title=" + title +
            ", content=" + content +
            ", createTime=" + createTime +
            ", coverImage=" + coverImage +
            ", type=" + type +
            ", resource=" + resource +
            ", createId=" + createId +
            ", status=" + status +
        "}";
    }
}
