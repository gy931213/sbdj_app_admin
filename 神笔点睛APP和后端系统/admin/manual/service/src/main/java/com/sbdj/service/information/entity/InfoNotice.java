package com.sbdj.service.information.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Transient;

/**
 * <p>
 * 公告
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="InfoNotice对象", description="公告")
public class InfoNotice implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id自增长")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "公告标题")
    private String title;

    private String type;

    private String resource;

    private String coverImage;

    @ApiModelProperty(value = "公告内容")
    private String content;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人id")
    private Long createId;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }
    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "InfoNotice{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", title=" + title +
            ", type=" + type +
            ", resource=" + resource +
            ", coverImage=" + coverImage +
            ", content=" + content +
            ", createTime=" + createTime +
            ", createId=" + createId +
            ", status=" + status +
        "}";
    }
}
