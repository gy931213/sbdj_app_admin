package com.sbdj.service.information.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.information.admin.vo.InfoHelpVo;
import com.sbdj.service.information.entity.InfoHelp;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 帮助手册 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface InfoHelpMapper extends BaseMapper<InfoHelp> {
    /**
     * 通过关键词或组织id获取帮助手册
     * @author Yly
     * @date 2019-11-26 10:03
     * @param keyword 关键词
     * @param orgId 机构id
     * @param iPage 分页组件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.information.admin.vo.InfoHelpVo>
     */
    IPage<InfoHelpVo> findHelpPageList(@Param("page") IPage<InfoHelpVo> iPage, @Param("keyword") String keyword, @Param("orgId") Long orgId);
}
