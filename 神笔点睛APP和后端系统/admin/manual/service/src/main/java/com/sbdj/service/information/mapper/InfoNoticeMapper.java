package com.sbdj.service.information.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.information.admin.vo.InfoNoticeVo;
import com.sbdj.service.information.entity.InfoHelp;
import com.sbdj.service.information.entity.InfoNotice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 公告 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface InfoNoticeMapper extends BaseMapper<InfoNotice> {
    /**
     * 通过关键词与机构id获取公告id
     * @author Yly
     * @date 2019-11-26 11:25
     * @param iPage 分页组件
     * @param keyword 关键词
     * @param orgId 机构id
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.information.admin.vo.InfoNoticeVo>
     */
    IPage<InfoNoticeVo> findNoticePageList(@Param("page") IPage<InfoNoticeVo> iPage, @Param("keyword") String keyword, @Param("orgId") Long orgId);
}
