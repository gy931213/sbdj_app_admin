package com.sbdj.service.information.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.information.admin.vo.InfoHelpVo;
import com.sbdj.service.information.entity.InfoHelp;

/**
 * <p>
 * 帮助手册 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface IInfoHelpService extends IService<InfoHelp> {
    /**
     * 通过关键词或组织id获取帮助手册
     * @author Yly
     * @date 2019-11-26 10:03
     * @param keyword 关键词
     * @param orgId 机构id
     * @param iPage 分页组件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.information.admin.vo.InfoHelpVo>
     */
    IPage<InfoHelpVo> findHelpPageList(String keyword, Long orgId, IPage<InfoHelpVo> iPage);

    /**
     * 通过组织id获取帮助手册
     * @author Yly
     * @date 2019-12-02 15:40
     * @param orgId 组织id
     * @param iPage 分页
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.information.admin.vo.InfoHelpVo>
     */
    IPage<InfoHelpVo> findHelpPageList(Long orgId, IPage<InfoHelpVo> iPage);

    /**
     * 更新帮助手册
     * @author Yly
     * @date 2019-11-26 10:19
     * @param help 帮助手册
     * @return void
     */
    void updateHelp(InfoHelp help);

    /**
     * 逻辑删除帮助手册
     * @author Yly
     * @date 2019-11-26 10:21
     * @param id 帮助手册id
     * @return void
     */
    void logicDeleteHelp(Long id);

    /**
     * 批量逻辑删除帮助手册
     * @author Yly
     * @date 2019-11-26 10:24
     * @param ids 帮助手册ids
     * @return void
     */
    void logicDeleteHelps(String ids);

    /**
     * 更新帮助手册的状态
     * @author Yly
     * @date 2019-11-26 10:26
     * @param id 帮助手册id
     * @param status 状态
     * @return void
     */
    void updateHelpStatus(Long id,String status);

    /**
     * 保存帮助手册
     * @author Yly
     * @date 2019-11-26 10:31
     * @param help 帮助手册
     * @return void
     */
    void saveHelp(InfoHelp help);
}
