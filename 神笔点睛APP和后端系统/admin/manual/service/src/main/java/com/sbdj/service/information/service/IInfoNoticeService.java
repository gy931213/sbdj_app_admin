package com.sbdj.service.information.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.information.admin.vo.InfoNoticeVo;
import com.sbdj.service.information.entity.InfoNotice;

/**
 * <p>
 * 公告 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface IInfoNoticeService extends IService<InfoNotice> {
    /**
     * 通过关键词与机构id获取公告
     * @author Yly
     * @date 2019-11-26 11:22
     * @param keyword 关键词
     * @param orgId 机构id
     * @param iPage 分页组件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.information.admin.vo.InfoNoticeVo>
     */
    IPage<InfoNoticeVo> findNoticePageList(String keyword, Long orgId, IPage<InfoNoticeVo> iPage);

    /**
     * 通过机构id获取公告
     * @author Yly
     * @date 2019-12-02 15:43
     * @param orgId 机构id
     * @param iPage 分页
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.information.admin.vo.InfoNoticeVo>
     */
    IPage<InfoNoticeVo> findNoticePageList(Long orgId, IPage<InfoNoticeVo> iPage);

    /**
     * 更新公告
     * @author Yly
     * @date 2019-11-26 11:30
     * @param notice 公告
     * @return void
     */
    void updateNotice(InfoNotice notice);

    /**
     * 逻辑删除公告
     * @author Yly
     * @date 2019-11-26 11:34
     * @param id 公告id
     * @return void
     */
    void logicDeleteNotice(Long id);

    /**
     * 批量逻辑删除公告
     * @author Yly
     * @date 2019-11-26 11:34
     * @param ids 公告ids
     * @return void
     */
    void logicDeleteNotices(String ids);

    /**
     * 修改公告状态
     * @author Yly
     * @date 2019-11-26 11:38
     * @param id 公告id
     * @param status 状态信息
     * @return void
     */
    void updateNoticeStatus(Long id, String status);

    /**
     * 保存公告
     * @author Yly
     * @date 2019-11-26 11:40
     * @param notice 公告
     * @return void
     */
    void saveNotice(InfoNotice notice);
}
