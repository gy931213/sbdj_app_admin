package com.sbdj.service.information.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.information.admin.vo.InfoHelpVo;
import com.sbdj.service.information.entity.InfoHelp;
import com.sbdj.service.information.mapper.InfoHelpMapper;
import com.sbdj.service.information.service.IInfoHelpService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 帮助手册 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class InfoHelpServiceImpl extends ServiceImpl<InfoHelpMapper, InfoHelp> implements IInfoHelpService {
    @Override
    public IPage<InfoHelpVo> findHelpPageList(String keyword, Long orgId, IPage<InfoHelpVo> iPage) {
        return baseMapper.findHelpPageList(iPage, keyword, orgId);
    }

    @Override
    public IPage<InfoHelpVo> findHelpPageList(Long orgId, IPage<InfoHelpVo> iPage) {
        return baseMapper.findHelpPageList(iPage, null, orgId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateHelp(InfoHelp help) {
        if (StrUtil.isNull(help) || StrUtil.isNull(help.getId())) {
            throw BaseException.base("更新帮助失败,id为空");
        }
        updateById(help);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteHelp(Long id) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除帮助手册失败,id为空");
        }
        UpdateWrapper<InfoHelp> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteHelps(String ids) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("批量逻辑删除帮助手册失败,ids为空");
        }
        UpdateWrapper<InfoHelp> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.in("id", list);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateHelpStatus(Long id, String status) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新帮助手册状态失败,id为空");
        }
        UpdateWrapper<InfoHelp> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", status);
        updateWrapper.eq("id", id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveHelp(InfoHelp help) {
        help.setCreateTime(new Date());
        help.setStatus(Status.STATUS_ACTIVITY);
        save(help);
    }
}
