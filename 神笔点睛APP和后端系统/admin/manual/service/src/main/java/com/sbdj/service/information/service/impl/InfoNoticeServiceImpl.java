package com.sbdj.service.information.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.information.admin.vo.InfoNoticeVo;
import com.sbdj.service.information.entity.InfoNotice;
import com.sbdj.service.information.mapper.InfoNoticeMapper;
import com.sbdj.service.information.service.IInfoNoticeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 公告 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class InfoNoticeServiceImpl extends ServiceImpl<InfoNoticeMapper, InfoNotice> implements IInfoNoticeService {
    @Override
    public IPage<InfoNoticeVo> findNoticePageList(String keyword, Long orgId, IPage<InfoNoticeVo> iPage) {
        return baseMapper.findNoticePageList(iPage, keyword, orgId);
    }

    @Override
    public IPage<InfoNoticeVo> findNoticePageList(Long orgId, IPage<InfoNoticeVo> iPage) {
        return baseMapper.findNoticePageList(iPage, null, orgId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateNotice(InfoNotice notice) {
        if (StrUtil.isNull(notice) || StrUtil.isNull(notice.getId())) {
            throw BaseException.base("更新公告失败,id为空");
        }
        updateById(notice);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteNotice(Long id) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除公告失败,id为空");
        }
        UpdateWrapper<InfoNotice> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteNotices(String ids) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("批量逻辑删除公告失败,id为空");
        }
        UpdateWrapper<InfoNotice> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.in("id", list);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateNoticeStatus(Long id, String status) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新公告状态失败,id为空");
        }
        UpdateWrapper<InfoNotice> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveNotice(InfoNotice notice) {
        notice.setCreateTime(new Date());
        notice.setStatus(Status.STATUS_ACTIVITY);
        save(notice);
    }
}
