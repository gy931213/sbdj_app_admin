package com.sbdj.service.member.admin.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @ClassName: registerParam
 * @Description: 邀约注册参数实体类
 * @author: 陈元祺
 * @date: 2018年12月01日 下午5:18:45
 */
@ApiModel
public class RegisterQuery {

    /*上级手机号*/
    @ApiModelProperty(value = "上级手机号")
    private String onlineMobile;
    /*邀约人*/
    @ApiModelProperty(value = "邀约人")
    private Long salesmanId;
    /*会员名称*/
    @ApiModelProperty(value = "会员名称")
    private String name;
    /*手机号*/
    @ApiModelProperty(value = "手机号")
    private String mobile;
    /*密码*/
    @ApiModelProperty(value = "密码")
    private String password;
    /*验证码*/
    @ApiModelProperty(value = "验证码")
    private String code;


    public String getOnlineMobile() {
        return onlineMobile;
    }

    public void setOnlineMobile(String onlineMobile) {
        this.onlineMobile = onlineMobile;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
