package com.sbdj.service.member.admin.query;

/**
 * @ClassName: SalesmanNumberParams
 * @Description: (这里用一句话描述这个类的作用)
 * @author: 黄天良
 * @date: 2018年11月30日 上午10:01:38
 */
public class SalesmanNumberQuery {
    // 所属机构
    private Long orgId;
    // 省
    private Long provinceId;
    // 市
    private Long cityId;
    // 县/区
    private Long countyId;
    // 所属平台
    private Long platformId;
    // 业务员姓名
    private String salesmanName;
    // 号主姓名
    private String name;
    // 号主ID
    private String onlineid;
    // 号主电话
    private String mobile;
    // 关键字查询
    private String keyword;
    // 状态
    private String status;
    // 身份证号
    private String cardNum;

    public Long getProvinceId() {
        return provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public Long getCountyId() {
        return countyId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public String getName() {
        return name;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

}
