package com.sbdj.service.member.admin.query;

/**
 * @ClassName: SalesmanParams
 * @Description:
 * @author: 黄天良
 * @date: 2018年11月26日 下午5:37:42
 */
public class SalesmanQuery {

    // 所属机构
    private Long orgId;
    // 邀请人
    private String salesmanName;
    // 工号
    private String jobNumber;
    // 姓名
    private String name;
    // 登录账号
    private String mobile;
    // 关键字
    private String keyword;
    // 所属等级
    private Long salesmanLevelId;
    // 状态
    private String status;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Long getSalesmanLevelId() {
        return salesmanLevelId;
    }

    public void setSalesmanLevelId(Long salesmanLevelId) {
        this.salesmanLevelId = salesmanLevelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
