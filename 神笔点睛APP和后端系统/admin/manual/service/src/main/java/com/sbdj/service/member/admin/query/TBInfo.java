package com.sbdj.service.member.admin.query;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by Yly to 2019/9/11 10:36
 * 淘宝刷手
 */
public class TBInfo {
    /**
     * 业务员编号
     */
    private Long id;
    /**
     * 机构编号
     */
    private Long orgId;
    /**
     * 职位编码
     **/
    private String jobNumber;
    /**
     * 业务员名称
     **/
    private String name;
    /**
     * 业务员账号
     **/
    private String mobile;
    /**
     * 银行卡号
     **/
    private String bankNum;
    /**
     * 开户行名称
     **/
    private String bankName;
    /**
     * 支行名称
     **/
    private String bankad;
    /**
     * 微信号
     **/
    private String wechart;
    /**
     * QQ号
     **/
    private String qq;
    /**
     * 身份证号
     **/
    private String cardNum;
    /**
     * 身份证照片
     **/
    private String cardImg;
    /**
     * 最后登录时间
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;
    /**
     * 登录次数
     **/
    private Integer loginTotal;
    /**
     * 更新时间
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    /**
     * 创建时间
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 状态{A：启用，N：禁用}
     **/
    private String status;
    /**
     * 状态{A：启用，N：禁用}
     **/
    private String pStatus;
    /**
     * 所属业务员{推荐人}
     **/
    private Long salesmanId;
    private String salesmanName;
    private String salesmanMobile;
    /**
     * 所属省
     **/
    private Long provinceId;
    private String provinceName;
    /**
     * 所属市
     **/
    private Long cityId;
    private String cityName;
    /**
     * 所属县
     **/
    private Long countyId;
    private String countyName;
    /**
     * 是否首次任务 0:否 1:是 默认:0
     **/
    private Integer firstMission = 0;
    /**
     * 手机在网时长
     **/
    private String mobileOnline;
    /**
     * 是否安装查小号 1:是 0:否 默认:0
     **/
    private Integer isSearch;
    /**
     * 所属平台账号
     **/
    private String onlineid;
    /**
     * 三网实名认证结果
     */
    private String idMobileName;
    /**
     * 银行卡四要素认证结果
     */
    private String idMobileNameBank;
    /**
     * 可接现付单数
     */
    private Integer remainderXFD;
    /**
     * 可接现隔日单数
     */
    private Integer remainderGRD;
    /**
     * 平台ID
     */
    private Long platformId;
    /**
     * 平台名称
     */
    private String platformName;
    /**
     * 号主ID
     */
    private Long salesmanNumberId;
    /**
     * 操作人
     **/
    private Long oprId;
    /**
     * 操作人名字
     **/
    private String oprName;
    /**
     * 接取的任务数
     **/
    private Integer taskTotal;
    /**
     * 创建时间
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date disableTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBankNum() {
        return bankNum;
    }

    public void setBankNum(String bankNum) {
        this.bankNum = bankNum;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankad() {
        return bankad;
    }

    public void setBankad(String bankad) {
        this.bankad = bankad;
    }

    public String getWechart() {
        return wechart;
    }

    public void setWechart(String wechart) {
        this.wechart = wechart;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getCardImg() {
        return cardImg;
    }

    public void setCardImg(String cardImg) {
        this.cardImg = cardImg;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Integer getLoginTotal() {
        return loginTotal;
    }

    public void setLoginTotal(Integer loginTotal) {
        this.loginTotal = loginTotal;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getSalesmanMobile() {
        return salesmanMobile;
    }

    public void setSalesmanMobile(String salesmanMobile) {
        this.salesmanMobile = salesmanMobile;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Long getCountyId() {
        return countyId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public Integer getFirstMission() {
        return firstMission;
    }

    public void setFirstMission(Integer firstMission) {
        this.firstMission = firstMission;
    }

    public String getMobileOnline() {
        return mobileOnline;
    }

    public void setMobileOnline(String mobileOnline) {
        this.mobileOnline = mobileOnline;
    }

    public Integer getIsSearch() {
        return isSearch;
    }

    public void setIsSearch(Integer isSearch) {
        this.isSearch = isSearch;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public String getIdMobileName() {
        return idMobileName;
    }

    public void setIdMobileName(String idMobileName) {
        this.idMobileName = idMobileName;
    }

    public String getIdMobileNameBank() {
        return idMobileNameBank;
    }

    public void setIdMobileNameBank(String idMobileNameBank) {
        this.idMobileNameBank = idMobileNameBank;
    }

    public Integer getRemainderXFD() {
        return remainderXFD;
    }

    public void setRemainderXFD(Integer remainderXFD) {
        this.remainderXFD = remainderXFD;
    }

    public Integer getRemainderGRD() {
        return remainderGRD;
    }

    public void setRemainderGRD(Integer remainderGRD) {
        this.remainderGRD = remainderGRD;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }

    public String getOprName() {
        return oprName;
    }

    public void setOprName(String oprName) {
        this.oprName = oprName;
    }

    public Integer getTaskTotal() {
        return taskTotal;
    }

    public void setTaskTotal(Integer taskTotal) {
        this.taskTotal = taskTotal;
    }

    public Date getDisableTime() {
        return disableTime;
    }

    public void setDisableTime(Date disableTime) {
        this.disableTime = disableTime;
    }
}
