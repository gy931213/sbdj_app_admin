package com.sbdj.service.member.admin.query;

/**
 * Created by Yly to 2019/9/11 10:49
 * 淘宝查询
 */
public class TBQuery {
    // 邀请人
    private String salesmanName;
    // 工号
    private String jobNumber;
    // 姓名
    private String  name;
    // 登录账号
    private String mobile;
    // 关键字
    private String keyword;
    // 所属等级
    private Long salesmanLevelId;
    // 业务状态
    private String status;
    // 号主状态
    private String pStatus;
    // 平台
    private Long platformId;
    /**
     * 开始时间
     **/
    private String beginTime;
    /**
     * 结束时间
     **/
    private String endTime;
    /**
     * 操作人
     **/
    private String oprName;
    /** 上级业务手机号 **/
    private String salesmanOnlineMobile;


    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Long getSalesmanLevelId() {
        return salesmanLevelId;
    }

    public void setSalesmanLevelId(Long salesmanLevelId) {
        this.salesmanLevelId = salesmanLevelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOprName() {
        return oprName;
    }

    public void setOprName(String oprName) {
        this.oprName = oprName;
    }

    public String getSalesmanOnlineMobile() {
        return salesmanOnlineMobile;
    }

    public void setSalesmanOnlineMobile(String salesmanOnlineMobile) {
        this.salesmanOnlineMobile = salesmanOnlineMobile;
    }
}
