package com.sbdj.service.member.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @className: SalesmanCreditRecordExt
 * @description: 业务员信用额度
 * @author: Yly
 * @date: 2019/7/26 13:18
 */
@ApiModel(value="业务员信用额度Vo", description="业务员信用额度Vo")
public class SalesmanCreditRecordVo {
    @ApiModelProperty(value="id")
    private Long id;
    @ApiModelProperty(value="机构id")
    private Long orgId;
    @ApiModelProperty(value="业务员id")
    private Long salesmanId;
    @ApiModelProperty(value="子任务id")
    private Long taskSonId;
    @ApiModelProperty(value="原因说明")
    private String reason;
    @ApiModelProperty(value="增/减的金额额度")
    private BigDecimal increment;
    @ApiModelProperty(value="创建时间")
    private Date createTime;
    @ApiModelProperty(value="信用额度")
    private BigDecimal credit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public BigDecimal getIncrement() {
        return increment;
    }

    public void setIncrement(BigDecimal increment) {
        this.increment = increment;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }
}
