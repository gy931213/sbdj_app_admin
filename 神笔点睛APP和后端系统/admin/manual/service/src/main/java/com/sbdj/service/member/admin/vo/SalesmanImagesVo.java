package com.sbdj.service.member.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @ClassName: SalesmanImages
 * @Description: 业务员\业务员号主，相关的证件图片集合扩展类
 * @author: 黄天良
 * @date: 2018年11月27日 下午4:34:57
 */
@ApiModel(value = "业务员/业务员号主，相关的证件图片集合Vo", description = "业务员/业务员号主，相关的证件图片集合Vo")
public class SalesmanImagesVo implements Serializable {


    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value="主键自增")
    private Long id;
    @ApiModelProperty(value="资源id，对应在，salesman，salesman_number 表中 id")
    private Long srcId;
    @ApiModelProperty(value="资源类型，[{SM：业务员资源图片},{SN：业务员号主资源图片}]")
    private String srcType;
    @ApiModelProperty(value="资源链接")
    private String link;

    @ApiModelProperty(value="证件类型id ,对应到 sys_config_dtl 中的主键id")
    private Long typeId;
    @ApiModelProperty(value="证件名称")
    private String typeName;

    public Long getId() {
        return id;
    }

    public Long getSrcId() {
        return srcId;
    }

    public String getSrcType() {
        return srcType;
    }

    public Long getTypeId() {
        return typeId;
    }

    public String getLink() {
        return link;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSrcId(Long srcId) {
        this.srcId = srcId;
    }

    public void setSrcType(String srcType) {
        this.srcType = srcType;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

}
