package com.sbdj.service.member.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @ClassName: SalesmanLeaderboardExt
 * @Description: 业务员佣金/提成排行榜扩展
 * @author: 杨凌云
 * @date: 2019/6/25 17:11
 */
@ApiModel(value = "业务员佣金/提成排行榜Vo", description = "业务员佣金/提成排行榜Vo")
public class SalesmanLeaderboardVo implements Serializable {
    @ApiModelProperty(value="自增编号")
    private Long id;
    @ApiModelProperty(value="机构编号")
    private Long orgId;
    @ApiModelProperty(value="工号")
    private String jobNumber;
    @ApiModelProperty(value="金额")
    private BigDecimal money;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
