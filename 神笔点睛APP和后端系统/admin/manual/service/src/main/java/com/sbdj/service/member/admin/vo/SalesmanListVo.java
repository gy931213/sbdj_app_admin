package com.sbdj.service.member.admin.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @ClassName: SalesmanListExt
 * @Description: (api salesmancontroller业务)
 * @author: 陈元祺
 * @date: 2018年12月04日 下午5:18:45
 */
@ApiModel(value = "SalesmanListVo", description = "SalesmanListVo")
public class SalesmanListVo {
    @ApiModelProperty(value="业务员id")
    private long id;

    @ApiModelProperty(value="业务员姓名")
    private String name;

    @ApiModelProperty(value="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(value="任务完成数量")
    private long num;

    @ApiModelProperty(value="状态{A：启用，N：禁用}")
    private String status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public long getNum() {
        return num;
    }

    public void setNum(long num) {
        this.num = num;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
