package com.sbdj.service.member.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sbdj.service.task.entity.TaskSonImages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
@ApiModel(value = "SalesmanNumberCashReturnRecordVo", description = "SalesmanNumberCashReturnRecordVo")
public class SalesmanNumberCashReturnRecordVo {

    @ApiModelProperty(value="主键自增")
    private Long id;
    @ApiModelProperty(value="子任务id")
    private Long taskSonId;
    @ApiModelProperty(value="所属机构")
    private String orgName;
    @ApiModelProperty(value="所属子任务")
    private String taskSonNumber;
    @ApiModelProperty(value="任务领取时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date taskCreateTime;
    @ApiModelProperty(value="所属平台的订单号")
    private String orderNum;
    @ApiModelProperty(value="所属平台账号，对应到 salesman_number表中的onlineid字段'")
    private String onlineid;
    @ApiModelProperty(value="订单实付金额")
    private BigDecimal realPrice;
    @ApiModelProperty(value="所属平台")
    private String platformName;
    @ApiModelProperty(value="所属业务员")
    private String salesmanName;
    @ApiModelProperty(value="所属店铺")
    private String sellerShopName;
    @ApiModelProperty(value="原因")
    private String reason;
    @ApiModelProperty(value="返现状态")
    private String returnStatus;
    @ApiModelProperty(value="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @ApiModelProperty(value="操作人名称")
    private String oprName;
    @ApiModelProperty(value="操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date oprTime;
    @ApiModelProperty(value="状态，[{A:活动状态}，{D:删除状态}]")
    private String status;
    @ApiModelProperty(value="资源图地址")
    private String resources;
    @ApiModelProperty(value="子任务图片")
    List<TaskSonImages> taskSonImages;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getTaskSonNumber() {
        return taskSonNumber;
    }

    public void setTaskSonNumber(String taskSonNumber) {
        this.taskSonNumber = taskSonNumber;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getSellerShopName() {
        return sellerShopName;
    }

    public void setSellerShopName(String sellerShopName) {
        this.sellerShopName = sellerShopName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getOprTime() {
        return oprTime;
    }

    public void setOprTime(Date oprTime) {
        this.oprTime = oprTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getTaskCreateTime() {
        return taskCreateTime;
    }

    public void setTaskCreateTime(Date taskCreateTime) {
        this.taskCreateTime = taskCreateTime;
    }

    public String getOprName() {
        return oprName;
    }

    public void setOprName(String oprName) {
        this.oprName = oprName;
    }

    public String getResources() {
        return resources;
    }

    public void setResources(String resources) {
        this.resources = resources;
    }

    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }

    public List<TaskSonImages> getTaskSonImages() {
        return taskSonImages;
    }

    public void setTaskSonImages(List<TaskSonImages> taskSonImages) {
        this.taskSonImages = taskSonImages;
    }

}
