package com.sbdj.service.member.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * <p>
 * 号主Vo
 * </p>
 *
 * @author Yly
 * @since 2019-12-02
 */
public class SalesmanNumberListVo {

    @ApiModelProperty(value = "号主id")
    private long id;
    @ApiModelProperty(value = "号主名称")
    private String name;
    @ApiModelProperty(value = "所属平台账号")
    private String onlineid;
    @ApiModelProperty(value = "是否常用")
    private String often;
    @ApiModelProperty(value = "任务完成数量")
    private int num;
    @ApiModelProperty(value = "状态")
    private String status;
    @ApiModelProperty(value = "平台id")
    private Long platformId;
    @ApiModelProperty(value = "平台名称")
    private String platformName;
    @ApiModelProperty(value = "平台图片")
    private String platformUrl;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    @ApiModelProperty(value = "串号")
    private String imei;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public String getOften() {
        return often;
    }

    public void setOften(String often) {
        this.often = often;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getPlatformUrl() {
        return platformUrl;
    }

    public void setPlatformUrl(String platformUrl) {
        this.platformUrl = platformUrl;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
}
