package com.sbdj.service.member.admin.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @ClassName: SalesmanNumberSrcExt
 * @Description: (这里用一句话描述这个类的作用)
 * @author: 陈元祺
 * @date: 2018/12/20 16:22
 */
@ApiModel(value = "SalesmanNumberSrcVo", description = "SalesmanNumberSrcVo")
public class SalesmanNumberSrcVo {

    private Long id;
    @ApiModelProperty(value="所属业务员")
    private Long salesmanId;
    @ApiModelProperty(value="号主名称")
    private String name;
    @ApiModelProperty(value="身份证号")
    private String cardNum;
    @ApiModelProperty(value="所属省")
    private Long provinceId;
    @ApiModelProperty(value="所属市")
    private Long cityId;
    @ApiModelProperty(value="所属县")
    private Long countyId;
    @ApiModelProperty(value="所属平台")
    private Long platformId;
    @ApiModelProperty(value="所属平台账号")
    private String onlineid;
    @ApiModelProperty(value="手机唯一串号")
    private String imei;
    @ApiModelProperty(value="号主电话")
    private String mobile;
    @ApiModelProperty(value="禁用原因")
    private String disabledreason;
    @ApiModelProperty(value="是否是常用号主，默认1为常用，0不常用")
    private String often;
    @ApiModelProperty(value="更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    @ApiModelProperty(value="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @ApiModelProperty(value="禁用状态：{E：是启用 P：禁用}")
    private String status;

    @ApiModelProperty(value="号主证件图片")
    private String src;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getCountyId() {
        return countyId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDisabledreason() {
        return disabledreason;
    }

    public void setDisabledreason(String disabledreason) {
        this.disabledreason = disabledreason;
    }

    public String getOften() {
        return often;
    }

    public void setOften(String often) {
        this.often = often;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
}
