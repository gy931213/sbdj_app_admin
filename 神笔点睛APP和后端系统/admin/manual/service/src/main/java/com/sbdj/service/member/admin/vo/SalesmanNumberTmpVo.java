package com.sbdj.service.member.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @ClassName: SalesmanNumberTmpExt
 * @Description: 临时号主扩展
 * @author: Yly
 * @date: 2019/7/14 11:43
 */
@ApiModel(value = "临时号主Vo", description = "临时号主Vo")
public class SalesmanNumberTmpVo {
    @ApiModelProperty(value="临时号主ID")
    private Long id;
    @ApiModelProperty(value="手机")
    private String mobile;
    @ApiModelProperty(value="名字")
    private String name;
    @ApiModelProperty(value="经度")
    private String longitude;
    @ApiModelProperty(value="纬度")
    private String latitude;
    @ApiModelProperty(value="经纬度")
    private String longLati;
    @ApiModelProperty(value="ip地址")
    private String ip;
    @ApiModelProperty(value="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongLati() {
        return longLati;
    }

    public void setLongLati(String longLati) {
        this.longLati = longLati;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
