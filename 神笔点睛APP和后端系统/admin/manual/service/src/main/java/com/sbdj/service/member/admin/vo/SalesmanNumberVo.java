package com.sbdj.service.member.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @ClassName: SalesmanNumber
 * @Description: 业务员的号主类扩展类
 * @author: 黄天良
 * @date: 2018年11月29日 上午9:22:30
 */
@ApiModel(value = "业务员的号主Vo", description = "业务员的号主Vo")
public class    SalesmanNumberVo {
    @ApiModelProperty(value="主键自增")
    private Long id;
    @ApiModelProperty(value="号主名称")
    private String name;
    @ApiModelProperty(value="身份证号")
    private String cardNum;
    @ApiModelProperty(value="所属平台账号")
    private String onlineid;
    @ApiModelProperty(value="手机唯一串号")
    private String imei;
    @ApiModelProperty(value="号主电话")
    private String mobile;
    @ApiModelProperty(value="禁用原因")
    private String disabledreason;
    @ApiModelProperty(value="是否是常用号主，默认1为常用，0不常用")
    private String often;
    @ApiModelProperty(value="更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    @ApiModelProperty(value="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @ApiModelProperty(value="禁用状态：{E：是启用 P：禁用}")
    private String status;

    @ApiModelProperty(value="所属省")
    private Long provinceId;
    private String provinceName;

    @ApiModelProperty(value="所属市")
    private Long cityId;
    private String cityName;

    @ApiModelProperty(value="所属县")
    private Long countyId;
    private String countyName;

    @ApiModelProperty(value="所属平台")
    private Long platformId;
    private String platformName;

    @ApiModelProperty(value="所属业务员")
    private Long salesmanId;
    private String salesmanName;

    public Long getId() {
        return id;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public String getName() {
        return name;
    }

    public String getCardNum() {
        return cardNum;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public Long getCountyId() {
        return countyId;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public String getImei() {
        return imei;
    }

    public String getMobile() {
        return mobile;
    }

    public String getDisabledreason() {
        return disabledreason;
    }

    public String getOften() {
        return often;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setDisabledreason(String disabledreason) {
        this.disabledreason = disabledreason;
    }

    public void setOften(String often) {
        this.often = often;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public String getCountyName() {
        return countyName;
    }

    public String getPlatformName() {
        return platformName;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

}
