package com.sbdj.service.member.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @ClassName: SalesmanNumberWechatTyingExt
 * @Description: 业务员号主绑定微信账号扩展
 * @author: 杨凌云
 * @date: 2019/2/23 9:14
 */
@ApiModel(value = "业务员号主绑定微信账号Vo", description = "业务员号主绑定微信账号Vo")
public class SalesmanNumberWechatTyingVo {
    @ApiModelProperty(value="主键")
    private Long id;
    @ApiModelProperty(value="所属号主")
    private Long salesmanNumberId;
    @ApiModelProperty(value="所属平台")
    private Long platformId;
    @ApiModelProperty(value="所属微信")
    private Long wechatMemberInfoId;
    @ApiModelProperty(value="状态")
    private String status;
    @ApiModelProperty(value="创建时间")
    private Date createTime;
    @ApiModelProperty(value="更新时间")
    private Date updateTime;
    @ApiModelProperty(value="所属机构")
    private Long orgId;
    @ApiModelProperty(value="用户的标识，对当前公众号唯一")
    private String openid;
    @ApiModelProperty(value="用户的昵称")
    private String nickname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public Long getWechatMemberInfoId() {
        return wechatMemberInfoId;
    }

    public void setWechatMemberInfoId(Long wechatMemberInfoId) {
        this.wechatMemberInfoId = wechatMemberInfoId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
