package com.sbdj.service.member.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * <p>
 * 业务员登录相关
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
@ApiModel(value = "业务员登录相关")
public class SalesmanRespVo {
	
	@ApiModelProperty(value = "业务员id")
	private Long salesmanId;
	@ApiModelProperty(value = "业务员工号")
	private String jobNumber;
	@ApiModelProperty(value = "名称")
	private String name;
	@ApiModelProperty(value = "总信用额度")
	private BigDecimal balance;
	@ApiModelProperty(value = "状态[{E:启用},{P:禁用}]")
	private String status;
	@ApiModelProperty(value = "总信用额度")
	private BigDecimal totalCredit;
	@ApiModelProperty(value = "剩余信用额度")
	private BigDecimal surplusCredit;
	@ApiModelProperty(value = "等级名称")
	private String levelName;
	@ApiModelProperty(value = "登录成功token")
	private String token;

	public Long getSalesmanId() {
		return salesmanId;
	}

	public void setSalesmanId(Long salesmanId) {
		this.salesmanId = salesmanId;
	}

	public String getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(BigDecimal totalCredit) {
		this.totalCredit = totalCredit;
	}

	public BigDecimal getSurplusCredit() {
		return surplusCredit;
	}

	public void setSurplusCredit(BigDecimal surplusCredit) {
		this.surplusCredit = surplusCredit;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
