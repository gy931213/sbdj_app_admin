package com.sbdj.service.member.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: Salesman
 * @Description: (这里用一句话描述这个类的作用)
 * @author: 黄天良
 * @date: 2018年11月26日 下午5:18:45
 */
@ApiModel(value = "SalesmanVo", description = "SalesmanVo")
public class SalesmanVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键自增")
    private Long id;
    @ApiModelProperty(value="职位编码")
    private String jobNumber;
    @ApiModelProperty(value="业务员名称")
    private String name;
    @ApiModelProperty(value="密码")
    private String pwd;
    @ApiModelProperty(value="关键子")
    private String keyword;
    @ApiModelProperty(value="业务员账号")
    private String mobile;
    @ApiModelProperty(value="银行卡号")
    private String bankNum;
    @ApiModelProperty(value="开户行名称")
    private String bankName;
    @ApiModelProperty(value="支行名称")
    private String bankad;
    @ApiModelProperty(value="微信号")
    private String wechart;
    @ApiModelProperty(value="QQ号")
    private String qq;
    @ApiModelProperty(value="身份证号")
    private String cardNum;
    @ApiModelProperty(value="身份证照片")
    private String cardImg;
    @ApiModelProperty(value="总信用额度")
    private BigDecimal totalCredit;
    @ApiModelProperty(value="剩余信用额度")
    private BigDecimal surplusCredit;
    @ApiModelProperty(value="账户余额")
    private BigDecimal balance;
    @ApiModelProperty(value="最后登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;
    @ApiModelProperty(value="登录次数")
    private Integer loginTotal;
    @ApiModelProperty(value="更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    @ApiModelProperty(value="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @ApiModelProperty(value="业务员等级")
    private Long salesmanLevelId;
    @ApiModelProperty(value="业务员等级名称")
    private String salesmanLevelName;
    @ApiModelProperty(value="状态{A：启用，N：禁用}")
    private String status;
    @ApiModelProperty(value="验证结果")
    private String validateResult;
    @ApiModelProperty(value="风险评估请求次数，默认一天10次")
    private Integer requestCount;
    @ApiModelProperty(value="所属机构")
    private Long orgId;
    @ApiModelProperty(value="所属机构名称")
    private String orgName;
    @ApiModelProperty(value="所属业务员{推荐人}")
    private Long salesmanId;
    private String salesmanName;
    private String salesmanMobile;
    @ApiModelProperty(value="所属省")
    private Long provinceId;
    private String provinceName;
    @ApiModelProperty(value="所属市")
    private Long cityId;
    private String cityName;
    @ApiModelProperty(value="所属县")
    private Long countyId;
    private String countyName;
    @ApiModelProperty(value="等级名称")
    private String levelName;
    @ApiModelProperty(value="所属银行卡")
    private Long bankId;
    @ApiModelProperty(value="所属银行名称")
    private String bank;
    @ApiModelProperty(value="count计数")
    private Long scount;
    @ApiModelProperty(value="是否首次任务 0:否 1:是 默认:0")
    private Integer firstMission = 0;
    @ApiModelProperty(value="手机在网时长")
    private String mobileOnline;
    @ApiModelProperty(value="是否安装查小号 1:是 0:否 默认:0")
    private Integer isSearch;
    @ApiModelProperty(value="三网实名认证结果")
    private String idMobileName;
    @ApiModelProperty(value="银行卡四要素认证结果")
    private String idMobileNameBank;
    @ApiModelProperty(value = "省市区名称")
    private String pcc;

    public Long getId() {
        return id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public String getName() {
        return name;
    }

    public String getMobile() {
        return mobile;
    }

    public String getBankNum() {
        return bankNum;
    }

    public String getBankName() {
        return bankName;
    }

    public String getBankad() {
        return bankad;
    }

    public String getWechart() {
        return wechart;
    }

    public String getQq() {
        return qq;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public Long getCountyId() {
        return countyId;
    }

    public Integer getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(Integer requestCount) {
        this.requestCount = requestCount;
    }

    public String getCardNum() {
        return cardNum;
    }

    public String getCardImg() {
        return cardImg;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public Integer getLoginTotal() {
        return loginTotal;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setBankNum(String bankNum) {
        this.bankNum = bankNum;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void setBankad(String bankad) {
        this.bankad = bankad;
    }

    public void setWechart(String wechart) {
        this.wechart = wechart;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public void setCardImg(String cardImg) {
        this.cardImg = cardImg;
    }

    public BigDecimal getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(BigDecimal totalCredit) {
        this.totalCredit = totalCredit;
    }

    public BigDecimal getSurplusCredit() {
        return surplusCredit;
    }

    public void setSurplusCredit(BigDecimal surplusCredit) {
        this.surplusCredit = surplusCredit;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public void setLoginTotal(Integer loginTotal) {
        this.loginTotal = loginTotal;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrgName() {
        return orgName;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public Long getSalesmanLevelId() {
        return salesmanLevelId;
    }

    public void setSalesmanLevelId(Long salesmanLevelId) {
        this.salesmanLevelId = salesmanLevelId;
    }

    public String getSalesmanLevelName() {
        return salesmanLevelName;
    }

    public void setSalesmanLevelName(String salesmanLevelName) {
        this.salesmanLevelName = salesmanLevelName;
    }

    public String getPwd() {
        return pwd;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getValidateResult() {
        return validateResult;
    }

    public void setValidateResult(String validateResult) {
        this.validateResult = validateResult;
    }

    public String getSalesmanMobile() {
        return salesmanMobile;
    }

    public void setSalesmanMobile(String salesmanMobile) {
        this.salesmanMobile = salesmanMobile;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Long getScount() {
        return scount;
    }

    public void setScount(Long scount) {
        this.scount = scount;
    }

    public Integer getFirstMission() {
        return firstMission;
    }

    public void setFirstMission(Integer firstMission) {
        this.firstMission = firstMission;
    }

    public String getMobileOnline() {
        return mobileOnline;
    }

    public void setMobileOnline(String mobileOnline) {
        this.mobileOnline = mobileOnline;
    }

    public Integer getIsSearch() {
        return isSearch;
    }

    public void setIsSearch(Integer isSearch) {
        this.isSearch = isSearch;
    }

    public String getPcc() {
        return pcc;
    }

    public void setPcc(String pcc) {
        this.pcc = pcc;
    }
}
