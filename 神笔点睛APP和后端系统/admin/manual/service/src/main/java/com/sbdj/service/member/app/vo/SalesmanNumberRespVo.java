package com.sbdj.service.member.app.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @ClassName:  SalesmanNumberReqVo
 * @Description: 业务员号主
 * @author: Htl
 * @date: 2019/5/28
 */
@ApiModel
public class SalesmanNumberRespVo {
	@ApiModelProperty(value = "id")
	private Long id;
	@ApiModelProperty(value = "所属平台 id")
	private Long platformId;
	@ApiModelProperty(value = "所属平台 名称")
	private String platformName;
	@ApiModelProperty(value = "号主ID")
	private String onlineid;
	@ApiModelProperty(value = "姓名")
	private String name;
	@ApiModelProperty(value = "串号")
	private String imei;
	@ApiModelProperty(value = "号主电话")
	private String mobile;
	@ApiModelProperty(value = "所属省id")
	private Long provinceId;
	@ApiModelProperty(value = "所属市id")
	private Long cityId;
	@ApiModelProperty(value = "所属县id")
	private Long countyId;
	@ApiModelProperty(value = "省市区名称")
	private String pcc;
	@ApiModelProperty(value = "身份证号")
	private String cardNum;
	@ApiModelProperty(value = "图片资源[{'typeId':'证件类型id','link':'图片链接'},{'typeId':'证件类型id','link':'图片链接'}] 字符串！")
	private String resource;
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getCardNum() {
		return cardNum;
	}
	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}
	public Long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public Long getCountyId() {
		return countyId;
	}
	public void setCountyId(Long countyId) {
		this.countyId = countyId;
	}
	public Long getPlatformId() {
		return platformId;
	}
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	public String getOnlineid() {
		return onlineid;
	}
	public void setOnlineid(String onlineid) {
		this.onlineid = onlineid;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPlatformName() {
		return platformName;
	}
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
	public String getPcc() {
		return pcc;
	}
	public void setPcc(String pcc) {
		this.pcc = pcc;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
