package com.sbdj.service.member.app.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @ClassName:  SalesmanVo
 * @Description: 业务员信用相关
 * @author: 杨凌云
 * @date: 2019/4/27 10:20
 */
@ApiModel
public class SalesmanReqVo {

	@ApiModelProperty(value = "uid（登录人id）")
	private Long uid;
	@ApiModelProperty(value = "账号")
	private String mobile;
	@ApiModelProperty(value = "姓名")
	private String name;
	@ApiModelProperty(value = "身份证号")
	private String cardNum;
	@ApiModelProperty(value = "QQ号")
	private String qq;
	@ApiModelProperty(value = "微信号")
	private String wechart;
	@ApiModelProperty(value = "所属省id")
	private Long provinceId;
	@ApiModelProperty(value = "所属市id")
	private Long cityId;
	@ApiModelProperty(value = "所属县id")
	private Long countyId;
	@ApiModelProperty(value = "证件类型资源图片")
	private String src;
	/*
	 * @ApiModelProperty(value = "银行卡信息") private String bankCardInfo;
	 */
	
	public String getMobile() {
		return mobile;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCardNum() {
		return cardNum;
	}
	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getWechart() {
		return wechart;
	}
	public void setWechart(String wechart) {
		this.wechart = wechart;
	}
	public Long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public Long getCountyId() {
		return countyId;
	}
	public void setCountyId(Long countyId) {
		this.countyId = countyId;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}

}
