package com.sbdj.service.member.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 业务员表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="Salesman对象", description="业务员表")
public class Salesman implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "职位编码")
    private String jobNumber;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "所属业务员{推荐人}")
    private Long salesmanId;

    @ApiModelProperty(value = "业务员名称")
    private String name;

    @ApiModelProperty(value = "业务员账号")
    private String mobile;

    @ApiModelProperty(value = "登陆密码")
    private String pwd;

    @ApiModelProperty(value = "关键字随机字符")
    private String keyword;

    @ApiModelProperty(value = "银行卡号")
    private String bankNum;

    @ApiModelProperty(value = "开户行名称")
    private String bankName;

    @ApiModelProperty(value = "支行名称")
    private String bankad;

    @ApiModelProperty(value = "微信号 // 更改为号主ID")
    private String wechart;

    @ApiModelProperty(value = "QQ号  // 手机串号")
    private String qq;

    @ApiModelProperty(value = "所属省")
    private Long provinceId;

    @ApiModelProperty(value = "所属市")
    private Long cityId;

    @ApiModelProperty(value = "所属县")
    private Long countyId;

    @ApiModelProperty(value = "身份证号")
    private String cardNum;

    @ApiModelProperty(value = "所属银行卡")
    private Long bankId;

    @ApiModelProperty(value = "身份证照片")
    private String cardImg;

    @ApiModelProperty(value = "总信用额度")
    private BigDecimal totalCredit;

    @ApiModelProperty(value = "剩余信用额度")
    private BigDecimal surplusCredit;

    @ApiModelProperty(value = "账户余额")
    private BigDecimal balance;

    @ApiModelProperty(value = "最后登录时间")
    private Date lastLoginTime;

    @ApiModelProperty(value = "登录次数")
    private Integer loginTotal;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "禁用时间")
    private Date disableTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "业务员等级{.....................................!}")
    private Long salesmanLevelId;

    @ApiModelProperty(value = "操作人 sys_admin id")
    private Long oprId;

    @ApiModelProperty(value = "状态{E：启用，P：禁用}")
    private String status;

    @ApiModelProperty(value = "验证结果")
    private String validateResult;

    @ApiModelProperty(value = "风险评估请求次数，默认一天10次",example = "10")
    private Integer requestCount;

    @ApiModelProperty(value = "接取的任务总数",example = "0")
    private Integer taskTotal;

    @ApiModelProperty(value = "是否首次任务 0:否 1:是 默认:0",example = "0")
    private Integer firstMission;

    @ApiModelProperty(value = "手机在网时长")
    private String mobileOnline;

    @ApiModelProperty(value = "是否安装查小号",example = "0")
    private Integer isSearch;

    @ApiModelProperty(value = "三网实名认证结果")
    private String idMobileName;

    @ApiModelProperty(value = "银行卡四要素认证结果")
    private String idMobileNameBank;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    public String getBankNum() {
        return bankNum;
    }

    public void setBankNum(String bankNum) {
        this.bankNum = bankNum;
    }
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    public String getBankad() {
        return bankad;
    }

    public void setBankad(String bankad) {
        this.bankad = bankad;
    }
    public String getWechart() {
        return wechart;
    }

    public void setWechart(String wechart) {
        this.wechart = wechart;
    }
    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }
    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }
    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }
    public Long getCountyId() {
        return countyId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }
    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }
    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }
    public String getCardImg() {
        return cardImg;
    }

    public void setCardImg(String cardImg) {
        this.cardImg = cardImg;
    }
    public BigDecimal getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(BigDecimal totalCredit) {
        this.totalCredit = totalCredit;
    }
    public BigDecimal getSurplusCredit() {
        return surplusCredit;
    }

    public void setSurplusCredit(BigDecimal surplusCredit) {
        this.surplusCredit = surplusCredit;
    }
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
    public Integer getLoginTotal() {
        return loginTotal;
    }

    public void setLoginTotal(Integer loginTotal) {
        this.loginTotal = loginTotal;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public Date getDisableTime() {
        return disableTime;
    }

    public void setDisableTime(Date disableTime) {
        this.disableTime = disableTime;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Long getSalesmanLevelId() {
        return salesmanLevelId;
    }

    public void setSalesmanLevelId(Long salesmanLevelId) {
        this.salesmanLevelId = salesmanLevelId;
    }
    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getValidateResult() {
        return validateResult;
    }

    public void setValidateResult(String validateResult) {
        this.validateResult = validateResult;
    }
    public Integer getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(Integer requestCount) {
        this.requestCount = requestCount;
    }
    public Integer getTaskTotal() {
        return taskTotal;
    }

    public void setTaskTotal(Integer taskTotal) {
        this.taskTotal = taskTotal;
    }
    public Integer getFirstMission() {
        return firstMission;
    }

    public void setFirstMission(Integer firstMission) {
        this.firstMission = firstMission;
    }
    public String getMobileOnline() {
        return mobileOnline;
    }

    public void setMobileOnline(String mobileOnline) {
        this.mobileOnline = mobileOnline;
    }
    public Integer getIsSearch() {
        return isSearch;
    }

    public void setIsSearch(Integer isSearch) {
        this.isSearch = isSearch;
    }
    public String getIdMobileName() {
        return idMobileName;
    }

    public void setIdMobileName(String idMobileName) {
        this.idMobileName = idMobileName;
    }
    public String getIdMobileNameBank() {
        return idMobileNameBank;
    }

    public void setIdMobileNameBank(String idMobileNameBank) {
        this.idMobileNameBank = idMobileNameBank;
    }

    @Override
    public String toString() {
        return "Salesman{" +
            "id=" + id +
            ", jobNumber=" + jobNumber +
            ", orgId=" + orgId +
            ", salesmanId=" + salesmanId +
            ", name=" + name +
            ", mobile=" + mobile +
            ", pwd=" + pwd +
            ", keyword=" + keyword +
            ", bankNum=" + bankNum +
            ", bankName=" + bankName +
            ", bankad=" + bankad +
            ", wechart=" + wechart +
            ", qq=" + qq +
            ", provinceId=" + provinceId +
            ", cityId=" + cityId +
            ", countyId=" + countyId +
            ", cardNum=" + cardNum +
            ", bankId=" + bankId +
            ", cardImg=" + cardImg +
            ", totalCredit=" + totalCredit +
            ", surplusCredit=" + surplusCredit +
            ", balance=" + balance +
            ", lastLoginTime=" + lastLoginTime +
            ", loginTotal=" + loginTotal +
            ", updateTime=" + updateTime +
            ", disableTime=" + disableTime +
            ", createTime=" + createTime +
            ", salesmanLevelId=" + salesmanLevelId +
            ", oprId=" + oprId +
            ", status=" + status +
            ", validateResult=" + validateResult +
            ", requestCount=" + requestCount +
            ", taskTotal=" + taskTotal +
            ", firstMission=" + firstMission +
            ", mobileOnline=" + mobileOnline +
            ", isSearch=" + isSearch +
            ", idMobileName=" + idMobileName +
            ", idMobileNameBank=" + idMobileNameBank +
        "}";
    }
}
