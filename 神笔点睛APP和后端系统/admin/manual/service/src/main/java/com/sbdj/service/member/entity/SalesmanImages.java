package com.sbdj.service.member.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 业务员\业务员号主，相关的证件图片集合表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="SalesmanImages对象", description="业务员\\业务员号主，相关的证件图片集合表")
public class SalesmanImages implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "资源id，对应在，salesman，salesman_number 表中 id")
    private Long srcId;

    @ApiModelProperty(value = "资源类型，[{SM：业务员资源图片},{SN：业务员号主资源图片}]")
    private String srcType;

    @ApiModelProperty(value = "证件类型id ,对应到 sys_config_dtl 中的主键id")
    private Long typeId;

    @ApiModelProperty(value = "资源链接")
    private String link;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "资源状态 {A：活动状态，D：删除状态}")
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getSrcId() {
        return srcId;
    }

    public void setSrcId(Long srcId) {
        this.srcId = srcId;
    }
    public String getSrcType() {
        return srcType;
    }

    public void setSrcType(String srcType) {
        this.srcType = srcType;
    }
    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SalesmanImages{" +
            "id=" + id +
            ", srcId=" + srcId +
            ", srcType=" + srcType +
            ", typeId=" + typeId +
            ", link=" + link +
            ", createTime=" + createTime +
            ", status=" + status +
        "}";
    }
}
