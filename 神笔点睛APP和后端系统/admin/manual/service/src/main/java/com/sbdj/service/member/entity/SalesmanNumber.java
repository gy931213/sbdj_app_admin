package com.sbdj.service.member.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 号主表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="SalesmanNumber对象", description="号主表")
public class SalesmanNumber implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属业务员")
    private Long salesmanId;

    @ApiModelProperty(value = "号主名称")
    private String name;

    @ApiModelProperty(value = "身份证号")
    private String cardNum;

    @ApiModelProperty(value = "所属省")
    private Long provinceId;

    @ApiModelProperty(value = "所属市")
    private Long cityId;

    @ApiModelProperty(value = "所属县/区")
    private Long countyId;

    @ApiModelProperty(value = "所属平台")
    private Long platformId;

    @ApiModelProperty(value = "操作人 sys_admin id")
    private Long oprId;

    @ApiModelProperty(value = "所属平台账号")
    private String onlineid;

    @ApiModelProperty(value = "手机唯一串号")
    private String imei;

    @ApiModelProperty(value = "号主电话")
    private String mobile;

    @ApiModelProperty(value = "禁用原因")
    private String disabledreason;

    @ApiModelProperty(value = "是否是常用号主，默认1为常用，0不常用")
    private String often;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "禁用状态：{E：是启用 P：禁用}")
    private String status;

    @ApiModelProperty(value = "请求次数",example = "0")
    private Integer requestCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }
    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }
    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }
    public Long getCountyId() {
        return countyId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }
    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }
    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }
    public String getOnlineid() {
        return onlineid;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }
    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getDisabledreason() {
        return disabledreason;
    }

    public void setDisabledreason(String disabledreason) {
        this.disabledreason = disabledreason;
    }
    public String getOften() {
        return often;
    }

    public void setOften(String often) {
        this.often = often;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Integer getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(Integer requestCount) {
        this.requestCount = requestCount;
    }

    @Override
    public String toString() {
        return "SalesmanNumber{" +
            "id=" + id +
            ", salesmanId=" + salesmanId +
            ", name=" + name +
            ", cardNum=" + cardNum +
            ", provinceId=" + provinceId +
            ", cityId=" + cityId +
            ", countyId=" + countyId +
            ", platformId=" + platformId +
            ", oprId=" + oprId +
            ", onlineid=" + onlineid +
            ", imei=" + imei +
            ", mobile=" + mobile +
            ", disabledreason=" + disabledreason +
            ", often=" + often +
            ", updateTime=" + updateTime +
            ", createTime=" + createTime +
            ", status=" + status +
            ", requestCount=" + requestCount +
        "}";
    }
}
