package com.sbdj.service.member.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 业务员\业务员号主\通讯录表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="SalesmanPhones对象", description="业务员\\业务员号主\\通讯录表")
public class SalesmanPhones implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "资源id，对应在，salesman，salesman_number 表中 id")
    private Long srcId;

    @ApiModelProperty(value = "资源类型，[{SM：业务员},{SN：业务员号主}]")
    private String srcType;

    @ApiModelProperty(value = "通讯录姓名")
    private String name;

    @ApiModelProperty(value = "电话号码")
    private String phone;

    @ApiModelProperty(value = "uuid")
    private String imei;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getSrcId() {
        return srcId;
    }

    public void setSrcId(Long srcId) {
        this.srcId = srcId;
    }
    public String getSrcType() {
        return srcType;
    }

    public void setSrcType(String srcType) {
        this.srcType = srcType;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "SalesmanPhones{" +
            "id=" + id +
            ", srcId=" + srcId +
            ", srcType=" + srcType +
            ", name=" + name +
            ", phone=" + phone +
            ", imei=" + imei +
            ", createTime=" + createTime +
        "}";
    }
}
