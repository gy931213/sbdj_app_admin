package com.sbdj.service.member.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 业务员额度增长配置表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="SalesmanRoyalty对象", description="业务员额度增长配置表")
public class SalesmanRoyalty implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "现付单")
    private BigDecimal payment;

    @ApiModelProperty(value = "隔日单")
    private BigDecimal nextDay;

    @ApiModelProperty(value = "浏览单")
    private BigDecimal browse;

    @ApiModelProperty(value = "提成")
    private BigDecimal percentage;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }
    public BigDecimal getNextDay() {
        return nextDay;
    }

    public void setNextDay(BigDecimal nextDay) {
        this.nextDay = nextDay;
    }
    public BigDecimal getBrowse() {
        return browse;
    }

    public void setBrowse(BigDecimal browse) {
        this.browse = browse;
    }
    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "SalesmanRoyalty{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", payment=" + payment +
            ", nextDay=" + nextDay +
            ", browse=" + browse +
            ", percentage=" + percentage +
            ", status=" + status +
            ", createTime=" + createTime +
        "}";
    }
}
