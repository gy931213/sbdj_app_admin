package com.sbdj.service.member.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 业务员佣金排行榜
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="SalesmanStatementLeaderboard对象", description="业务员佣金排行榜")
public class SalesmanStatementLeaderboard implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增字段")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "机构编号")
    private Long orgId;

    @ApiModelProperty(value = "业务员")
    private Long salesmanId;

    @ApiModelProperty(value = "金额")
    private BigDecimal money;

    @ApiModelProperty(value = "日期")
    private String createDate;

    @ApiModelProperty(value = "启用:A 删除:D")
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SalesmanStatementLeaderboard{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", salesmanId=" + salesmanId +
            ", money=" + money +
            ", createDate=" + createDate +
            ", status=" + status +
        "}";
    }
}
