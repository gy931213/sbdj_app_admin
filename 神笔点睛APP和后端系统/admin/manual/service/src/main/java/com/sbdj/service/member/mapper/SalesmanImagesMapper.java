package com.sbdj.service.member.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sbdj.service.member.admin.vo.SalesmanImagesVo;
import com.sbdj.service.member.entity.SalesmanImages;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 业务员\业务员号主，相关的证件图片集合表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanImagesMapper extends BaseMapper<SalesmanImages> {

    List<SalesmanImagesVo> findSalesmanImagesBySrcIdAndSrcType(@Param("ew") QueryWrapper<SalesmanImages> queryWrapper);

}
