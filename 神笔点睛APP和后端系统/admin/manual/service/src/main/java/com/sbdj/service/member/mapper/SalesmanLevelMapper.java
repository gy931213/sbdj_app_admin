package com.sbdj.service.member.mapper;

import com.sbdj.service.member.entity.SalesmanLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 业务员等级表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanLevelMapper extends BaseMapper<SalesmanLevel> {
    /**
     * 据业务员等级id获取比业务员低一级的数据
     * @author Yly
     * @date 2019-11-28 18:54
     * @param salesmanLevelId 业务员等级
     * @return com.sbdj.service.member.entity.SalesmanLevel
     */
    SalesmanLevel findSalesmanLowerLevel(@Param("salesmanLevelId") Long salesmanLevelId);
}
