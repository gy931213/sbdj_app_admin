package com.sbdj.service.member.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.member.admin.query.TBInfo;
import com.sbdj.service.member.admin.query.TBQuery;
import com.sbdj.service.member.admin.vo.SalesmanListVo;
import com.sbdj.service.member.admin.vo.SalesmanRespVo;
import com.sbdj.service.member.admin.vo.SalesmanVo;
import com.sbdj.service.member.entity.Salesman;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 业务员表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanMapper extends BaseMapper<Salesman> {

    /**
     * 更新业务员完成单量
     * @param salesmanId
     * @param total
     */
    void updateSalesmanTaskTotal(@Param("salesmanId") Long salesmanId, @Param("total") int total);
    IPage<TBInfo> selectSalesmanPage(IPage<SalesmanVo> pageParam, @Param("ew") QueryWrapper<TBQuery> queryWrapper);

    SalesmanVo findOnlineSalesmanVo(Long salesmanId);

    void updateSalesmanLastLoginTimeAndLoginTotal(@Param("id") Long id);

    /**
     * 前一天没有提交的现付单任务业务员
     * @author Yly
     * @date 2019-11-28 19:44
     * @return java.util.List<com.sbdj.service.member.admin.vo.SalesmanVo>
     */
    List<SalesmanVo> getSalesmanByFactorXFD();

    /**
     * 前一天没有提交的隔日单任务业务员
     * @author Yly
     * @date 2019-11-28 19:51
     * @return java.util.List<com.sbdj.service.member.admin.vo.SalesmanVo>
     */
    List<SalesmanVo> getSalesmanByFactorGRD();

    IPage<SalesmanListVo> findAllSalesman(IPage<SalesmanListVo> page, @Param("ew") QueryWrapper<SalesmanListVo> queryWrapper);

    SalesmanVo findSalesmanVoInfo(@Param("ew") QueryWrapper<SalesmanVo> salesmanVoQueryWrapper);

    SalesmanRespVo getSalesmanInfo(@Param("ew")QueryWrapper<SalesmanRespVo> queryWrapper);

    SalesmanVo findSalesmanExtCredit(@Param("ew") QueryWrapper<SalesmanVo> queryWrapper);

}
