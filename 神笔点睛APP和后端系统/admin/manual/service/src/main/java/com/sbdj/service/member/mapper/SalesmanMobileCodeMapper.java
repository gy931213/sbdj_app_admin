package com.sbdj.service.member.mapper;

import com.sbdj.service.member.entity.SalesmanMobileCode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 手机号验证码表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanMobileCodeMapper extends BaseMapper<SalesmanMobileCode> {

}
