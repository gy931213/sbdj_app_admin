package com.sbdj.service.member.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.member.admin.query.SalesmanNumberQuery;
import com.sbdj.service.member.admin.vo.SalesmanNumberListVo;
import com.sbdj.service.member.admin.vo.SalesmanNumberVo;
import com.sbdj.service.member.app.vo.SalesmanNumberRespVo;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sbdj.service.system.admin.vo.AreaVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 号主表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanNumberMapper extends BaseMapper<SalesmanNumber> {

    List<AreaVo> findSalesmanNumberAreaByAreaCode(String city, String district, String province, String areaCode);

    IPage<SalesmanNumberVo> findSalesmanNumberPage(@Param("page") IPage<SalesmanNumberVo> page, @Param("params") SalesmanNumberQuery params, @Param("ew") QueryWrapper<SalesmanNumberQuery> queryWrapper);


    IPage<SalesmanNumberListVo> findSalesmanNumberPageApp(IPage<SalesmanNumberListVo> page, @Param("ew") QueryWrapper<SalesmanNumberListVo> queryWrapper);

    SalesmanNumberRespVo findSalesmanNumberById(@Param("ew")QueryWrapper<SalesmanNumberRespVo> queryWrapper);

    IPage<SalesmanNumberListVo> searchSalesmanNumberByPlatformIdOnlineId(IPage<SalesmanNumberListVo> page,@Param("ew")QueryWrapper<SalesmanNumberListVo> queryWrapper);

}
