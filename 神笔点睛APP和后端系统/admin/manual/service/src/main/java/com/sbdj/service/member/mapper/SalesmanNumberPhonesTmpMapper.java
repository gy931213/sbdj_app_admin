package com.sbdj.service.member.mapper;

import com.sbdj.service.member.entity.SalesmanNumberPhonesTmp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 临时号主\通讯录表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanNumberPhonesTmpMapper extends BaseMapper<SalesmanNumberPhonesTmp> {

}
