package com.sbdj.service.member.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sbdj.service.member.admin.vo.SalesmanNumberTmpVo;
import com.sbdj.service.member.entity.SalesmanNumberTmp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 临时号主表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanNumberTmpMapper extends BaseMapper<SalesmanNumberTmp> {
    List<SalesmanNumberTmpVo> findSalesmanNumberTmpPositionById(@Param("ew") QueryWrapper<SalesmanNumberTmp> queryWrapper);

    List<SalesmanNumberTmpVo> findSalesmanNumberTmpPositionAll();
}
