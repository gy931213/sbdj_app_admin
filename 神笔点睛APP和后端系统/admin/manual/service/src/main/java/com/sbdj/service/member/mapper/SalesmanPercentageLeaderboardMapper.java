package com.sbdj.service.member.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.member.admin.vo.SalesmanLeaderboardVo;
import com.sbdj.service.member.entity.SalesmanPercentageLeaderboard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 业务员提成排行榜 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanPercentageLeaderboardMapper extends BaseMapper<SalesmanPercentageLeaderboard> {

   /**
    * 获取业务员提成排行榜
    * @author Yly
    * @date 2019-11-28 16:54
    * @param query 查询条件
    * @return java.util.List<com.sbdj.service.member.entity.SalesmanPercentageLeaderboard>
    */
    List<SalesmanPercentageLeaderboard> findSalesmanPercentageLeaderboardByOrgIdAndDate(@Param(Constants.WRAPPER) Wrapper<SalesmanPercentageLeaderboard> query);


    List<SalesmanLeaderboardVo> findSalesmanPercentageLeaderboardByOrgIdAndDate2(@Param("ew")QueryWrapper<SalesmanLeaderboardVo> queryWrapper);

}
