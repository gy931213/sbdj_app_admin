package com.sbdj.service.member.mapper;

import com.sbdj.service.member.entity.SalesmanPhones;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 业务员\业务员号主\通讯录表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanPhonesMapper extends BaseMapper<SalesmanPhones> {

}
