package com.sbdj.service.member.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.member.admin.vo.SalesmanRoyaltyVo;
import com.sbdj.service.member.entity.SalesmanRoyalty;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 业务员额度增长配置表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanRoyaltyMapper extends BaseMapper<SalesmanRoyalty> {
    IPage<SalesmanRoyaltyVo> findSalesmanRoyaltyPageList(IPage<SalesmanRoyaltyVo> page, @Param("ew") QueryWrapper<SalesmanRoyalty> queryWrapper) ;

}
