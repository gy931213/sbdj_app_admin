package com.sbdj.service.member.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.member.admin.vo.SalesmanLeaderboardVo;
import com.sbdj.service.member.entity.SalesmanStatementLeaderboard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 业务员佣金排行榜 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SalesmanStatementLeaderboardMapper extends BaseMapper<SalesmanStatementLeaderboard> {
    /**
     * 获取业务员佣金排行榜
     * @author Yly
     * @date 2019-11-28 16:57
     * @param query 查询条件
     * @return java.util.List<com.sbdj.service.member.entity.SalesmanStatementLeaderboard>
     */
    List<SalesmanStatementLeaderboard> findSalesmanStatementLeaderboardByOrgIdAndDate(@Param(Constants.WRAPPER)Wrapper<SalesmanStatementLeaderboard> query);

    List<SalesmanLeaderboardVo> findSalesmanPercentageLeaderboardByOrgIdAndDate2(@Param(Constants.WRAPPER)QueryWrapper<SalesmanLeaderboardVo> queryWrapper);

}
