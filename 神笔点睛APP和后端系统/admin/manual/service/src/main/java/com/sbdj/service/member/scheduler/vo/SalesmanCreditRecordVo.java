package com.sbdj.service.member.scheduler.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 业务员信用额度Vo
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public class SalesmanCreditRecordVo {
    /** id **/
    private Long id;
    /** 机构id **/
    private Long orgId;
    /** 业务员id **/
    private Long salesmanId;
    /** 子任务id **/
    private Long taskSonId;
    /** 原因说明 **/
    private String reason;
    /** 增/减的金额额度 **/
    private BigDecimal increment;
    /** 创建时间 **/
    private Date createTime;
    /** 信用额度 **/
    private BigDecimal credit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public BigDecimal getIncrement() {
        return increment;
    }

    public void setIncrement(BigDecimal increment) {
        this.increment = increment;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }
}
