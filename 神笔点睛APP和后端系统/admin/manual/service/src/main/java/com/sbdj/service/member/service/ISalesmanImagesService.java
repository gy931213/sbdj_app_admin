package com.sbdj.service.member.service;

import com.sbdj.service.member.admin.vo.SalesmanImagesVo;
import com.sbdj.service.member.entity.SalesmanImages;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 业务员\业务员号主，相关的证件图片集合表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanImagesService extends IService<SalesmanImages> {

    List<SalesmanImagesVo> findSalesmanImagesBySrcIdAndSrcType(Long id, String srcType);

    void saveSalesmanImages(SalesmanImages salesmanImages);

    void deleteSalesmanImages(String srcType, Long srcId);

}
