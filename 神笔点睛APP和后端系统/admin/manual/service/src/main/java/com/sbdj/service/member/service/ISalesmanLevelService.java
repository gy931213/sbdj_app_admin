package com.sbdj.service.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.member.entity.SalesmanLevel;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 业务员等级表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanLevelService extends IService<SalesmanLevel> {



    /**
     * 获取分页数据的接口
     * @author ZC
     * @date 2019-11-25 17:57
     * @param pageQuery 前端分页参数
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.member.entity.SalesmanLevel>
     */
    IPage<SalesmanLevel> findSalesmanLevelPageList(IPage<SalesmanLevel> pageQuery);


    /**
     * 保存数据
     * @author ZC
     * @date 2019-11-25 17:57
     * @param salesmanLevel 等级数据
     * @return void
     */
    void saveSalesmanLevel(SalesmanLevel salesmanLevel);


    /**
     * 修改等级数据
     * @author ZC
     * @date 2019-11-25 17:56
     * @param salesmanLevel 等级数据
     * @return void
     */
    void updateSalesmanLevel(SalesmanLevel salesmanLevel);


    /**
     * 获取下拉选项数据的接口
     * @author ZC
     * @date 2019-11-25 17:56
     * @param
     * @return java.util.List<com.sbdj.service.member.entity.SalesmanLevel>
     */
    List<SalesmanLevel> findSalesmanLevelSelect();


    /**
     * 删除数据的接口
     * @author ZC
     * @date 2019-11-25 17:55
     * @param id 等级ID
     * @return void
     */
    void loginDeleteSalesmanLevel(Long id);


    /**
     * 获取业务员最低等级
     * @author Yly
     * @date 2019-11-28 18:23
     * @return com.sbdj.service.member.entity.SalesmanLevel
     */
    SalesmanLevel findSalesmanLowestLevel();

    /**
     * 获取业务员等级id低一级的数据
     * @author Yly
     * @date 2019-11-28 18:50
     * @param salesmanLevelId 业务员等级id
     * @return com.sbdj.service.member.entity.SalesmanLevel
     */
    SalesmanLevel findSalesmanLowerLevel(Long salesmanLevelId);
}
