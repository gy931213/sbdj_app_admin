package com.sbdj.service.member.service;

import com.sbdj.service.member.admin.query.RegisterQuery;
import com.sbdj.service.member.entity.SalesmanMobileCode;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 手机号验证码表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanMobileCodeService extends IService<SalesmanMobileCode> {

    /**
     * 通过手机号码获取对应的验证码信息
     * @author Yly
     * @date 2019/11/22 21:51
     * @param mobile
     * @return com.sbdj.service.member.entity.SalesmanMobileCode
     */
    SalesmanMobileCode findSalesmanMobileCode(String mobile);

    /**
     * 通过手机号码发送验证码
     * @author Yly
     * @date 2019/11/22 21:54
     * @param mobile 手机号
     * @return java.lang.String
     */
    String sendSmsBySalesmanNumber(String mobile);

    /**
     * 给业务员发送短信
     * @author Yly
     * @date 2019-11-28 10:54
     * @param mobile 手机号
     * @return java.lang.String
     */
    String sendSmsBySalesman(String mobile);

    /**
     * 保存手机验证码数据
     * @author Yly
     * @date 2019/11/22 22:07
     * @param mobile
     * @param temp
     * @return void
     */
    void saveSalesmanMobileCode(String mobile , String temp);


    /**
     * 邀约注册
     * @author ZC
     * @date 2019-11-29 11:52
     * @param registerParam 注册信息
     * @param salesmanId 推荐人ID
     * @return void
     */
    void register(RegisterQuery registerParam, Long salesmanId);
}
