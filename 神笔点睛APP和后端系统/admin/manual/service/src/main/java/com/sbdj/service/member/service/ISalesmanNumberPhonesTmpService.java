package com.sbdj.service.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.member.entity.SalesmanNumberPhonesTmp;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * <p>
 * 临时号主\通讯录表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanNumberPhonesTmpService extends IService<SalesmanNumberPhonesTmp> {
    /**
     * 通过目标ID与号码,串号获取临时号主通讯录
     * @author Yly
     * @date 2019/11/22 22:58
     * @param targetId 目标ID
     * @param phone 手机号
     * @param imei 串号
     * @return com.sbdj.service.member.entity.SalesmanNumberPhonesTmp
     */
    SalesmanNumberPhonesTmp findSalesmanNumberPhonesTmp(Long targetId, String phone, String imei);

    /**
     * 保存临时号主通讯录
     * @author Yly
     * @date 2019/11/22 23:01
     * @param salesmanNumberPhonesTmp 临时号主通讯录
     * @return void
     */
    void saveSalesmanNumberPhonesTmp(SalesmanNumberPhonesTmp salesmanNumberPhonesTmp);

    /**
     * 通过临时号主ID获取通讯录信息
     * @author ZC
     * @date 2019-11-26 10:35
     * @param id
     * @param page
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.member.entity.SalesmanNumberPhonesTmp>
     */
    IPage<SalesmanNumberPhonesTmp> findSalesmanNumberPhonesTmpPage(Long id,IPage<SalesmanNumberPhonesTmp> page);

    List<SalesmanNumberPhonesTmp> findSalesmanNumberPhonesTmpList(Long id);

    /**
     * 获取临时号主通讯录数量
     * @author ZC
     * @date 2019-11-30 13:36
     * @param id 目标ID
     * @return int
     */
    int countSalesmanNumberPhonesTmpByTargetId(Long id);
}
