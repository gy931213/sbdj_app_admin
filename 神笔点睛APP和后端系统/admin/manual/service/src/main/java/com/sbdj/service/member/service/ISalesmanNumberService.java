package com.sbdj.service.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.service.member.admin.query.SalesmanNumberQuery;
import com.sbdj.service.member.admin.vo.SalesmanNumberListVo;
import com.sbdj.service.member.admin.vo.SalesmanNumberVo;
import com.sbdj.service.member.app.vo.SalesmanNumberRespVo;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.system.admin.vo.AreaVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 号主表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanNumberService extends IService<SalesmanNumber> {

    /**
     * 禁用号主
     * @param ids
     */
    void disableSalesmanNumberByIds(String ids);
    /**
     * Created by Yly to 2019/11/10 17:48
     * 通过业务员编号修改号主状态
     *
     * @param salesmanId 业务员编号
     * @param status     状态
     * @return void
     **/
    void updateSalesmanNumberStatusBySalesmanId(Long salesmanId, String status);
    /**
     * 判断此平台是否已经有号
     * @author ZC
     * @date 2019-11-25 15:02
     * @param id 号主ID
     * @param platformId 平台ID
     * @return com.sbdj.service.member.entity.SalesmanNumber
     */
    SalesmanNumber findSalesmanNumberBySalesmanIdAndPlatformId(Long id, Long platformId);

    /**
     * 统计号主都是来自哪个区域的
     * @author ZC
     * @date 2019-11-25 14:37
     * @param province 省市区编码
     * @return java.util.List<com.sbdj.service.system.admin.vo.AreaVo>
     */
    List<AreaVo> findSalesmanNumberAreaByAreaCode(String province);

    void updateSalesmanNumberStatus(Long id, String status);

    /**
     * 获取号主分页数据
     * @author ZC
     * @date 2019-11-26 10:43
     * @param params
     * @param page
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.member.admin.vo.SalesmanNumberVo>
     */
    IPage<SalesmanNumberVo> findSalesmanNumberPage(@Param("params") SalesmanNumberQuery params, @Param("page") IPage<SalesmanNumberVo> page);

    /**
     * Created by Yly to 2019/9/2 0002 17:31
     * 通过业务员编号与平台
     *
     * @param salesmanId 业务员编号
     * @param platformId 平台编号
     * @return java.lang.Integer
     **/
    Integer findSalesmanNumberTotalBySalesmanIdAndPlatformId(Long salesmanId, Long platformId);

    /**
     * 判断某平台某账号是否存在
     * @author ZC
     * @date 2019-11-25 17:38
     * @param platformId 平台ID
     * @param onlineid 平台账号
     * @return boolean
     */
    boolean isExistSalesmanNumber(Long platformId, String onlineid);

    void addSalesmanNumber(SalesmanNumber salesmanNumber);

    /**
     * 更新数据的接口
     *
     * @param sn 要更新的对象
     */
    void updateSalesmanNumberData(SalesmanNumber sn);

    /**
     * 通过平台id与平台账号获取号主
     * @author Yly
     * @date 2019-11-27 17:11
     * @param platformId 平台id
     * @param onlineId 平台账号
     * @return com.sbdj.service.member.entity.SalesmanNumber
     */
    SalesmanNumber findSalesmanNumberByPlatformIdOnlineId(Long platformId , String onlineId);

    /**
     * (api根据业务员id和号主id获取业务员所属的号主数据的接口)
     * @author ZC
     * @date 2019-11-29 13:41
     * @param salId 业务员id
     * @param salNumberId 号主id
     * @return com.sbdj.service.member.entity.SalesmanNumber
     */
    SalesmanNumber findSalesmanNumberBySalIdAndSalNumberId(Long salId,Long salNumberId);


    IPage<SalesmanNumberListVo> findSalesmanNumberPageApp(IPage<SalesmanNumberListVo> buildPageRequest, String status, String onlineid, Long platformId, Long uid);

    void setUsed(Long id, String often);

    SalesmanNumberRespVo findSalesmanNumberById(Long id);

    Integer findSalesmanNumberCountByImei(String trim, Long platformId);

    void updatesalesmanNumber(SalesmanNumber salesmanNumber);


    /**
     * @Title: searchSalesmanNumberByPlatformIdOnlineId
     * @Description: 号主列表数据
     * @param  pageable
     * @param  onlineId 平台账号
     * @param  platformId 平台id
     * @param  salId 业务员id
     * @return Page<SalesmanNumberListVo>
     */
    /**
     * 号主列表数据
     * @author ZC
     * @date 2019-11-30 11:10
     * @param buildPageRequest 分页
     * @param onlineId 平台账号
     * @param platformId 平台id
     * @param uid 业务员id
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.member.admin.vo.SalesmanNumberListVo>
     */
    IPage<SalesmanNumberListVo> searchSalesmanNumberByPlatformIdOnlineId(IPage<SalesmanNumberListVo> buildPageRequest, String onlineId, Long platformId, Long uid);

    /**
     * 根据参数获取数据的接口
     * @author ZC
     * @date 2019-11-30 14:14
     * @param salesmanId 业务员id
     * @param platformId 平台id
     * @param onlineid 号主账号
     * @return com.sbdj.service.member.entity.SalesmanNumber
     */
    SalesmanNumber findSalesmanNumber(Long salesmanId, Long platformId, String onlineid);

    /**
     * 检测该手机号是否注册过号主
     * @author Yly
     * @date 2019-12-07 11:40
     * @param mobile 手机号
     * @param platformId 平台id
     * @return boolean
     */
    boolean isExistSalesmanNumberByMobile(String mobile, Long platformId);

    /**
     * 检测该银行卡号是否注册过号主
     * @author Yly
     * @date 2019-12-07 11:41
     * @param bankNum 银行卡号
     * @param platformId 平台id
     * @return boolean
     */
    boolean isExistSalesmanNumberByBank(String bankNum, Long platformId);

    /**
     * 检测该身份证是否注册过号主
     * @author Yly
     * @date 2019-12-07 11:41
     * @param idCard 身份证
     * @param platformId 平台id
     * @return boolean
     */
    boolean isExistSalesmanNumberByIDCard(String idCard, Long platformId);
}
