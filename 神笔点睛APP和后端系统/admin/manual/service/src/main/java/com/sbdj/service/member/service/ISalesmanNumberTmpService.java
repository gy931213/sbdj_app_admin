package com.sbdj.service.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.member.admin.query.SalesmanNumberQuery;
import com.sbdj.service.member.admin.vo.SalesmanNumberTmpVo;
import com.sbdj.service.member.entity.SalesmanNumberTmp;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 临时号主表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanNumberTmpService extends IService<SalesmanNumberTmp> {
    /**
     * 根据手机号查询号主 只查单条
     * @author Yly
     * @date 2019/11/22 21:33
     * @param phone 手机号
     * @return com.sbdj.service.member.entity.SalesmanNumberTmp
     */
    SalesmanNumberTmp findSalesmanNumberByPhone(String phone);

    /**
     * 添加临时号主
     * @author Yly
     * @date 2019/11/22 21:39
     * @param salesmanNumberTmp 临时号主
     * @return void
     */
    void addSalesmanNumberTmp(SalesmanNumberTmp salesmanNumberTmp);

    /**
     * 更新临时号主
     * @author Yly
     * @date 2019/11/22 21:44
     * @param salesmanNumber 临时号主
     * @return void
     */
    void updateSalesmanNumber(SalesmanNumberTmp salesmanNumber);

    /**
     * 更新是否首次聊天
     * @author Yly
     * @date 2019/11/22 22:13
     * @param id 编号
     * @param isChat 是否首次聊天
     * @return void
     */
    void updateSalesmanNumberTmpIsChat(Long id, Integer isChat);









    /**
     * 获取分页数据的接口
     *
     * @param params   查询参数
     */
    IPage<SalesmanNumberTmp> findSalesmanNumberPage(SalesmanNumberQuery params, IPage<SalesmanNumberTmp> page);

    /**
     * @return com.shenbi.biz.member.domain.SalesmanNumberTmp 返回类型
     * @Title: findSalesmanOne
     * @Description 根据编号查询临时号主
     * @Param [id 临时号主编号]
     * @Author 杨凌云
     * @Date 2019/3/28 14:24
     **/
    SalesmanNumberTmp findSalesmanOne(long id);


    /**
     * @return void 返回类型
     * @Title: updatesalesmanNumber
     * @Description 更新临时号主
     * @Param [salesmanNumber]
     * @Author 杨凌云
     * @Date 2019/3/28 14:24
     **/
    void updatesalesmanNumber(SalesmanNumberTmp salesmanNumber);




    /**
     * @return void 返回类型
     * @Title: deleteSalesmanNumberTmpById
     * @Description 删除临时号主
     * @Param [id 临时号主编号]
     * @Author 杨凌云
     * @Date 2019/3/28 14:19
     **/
    void deleteSalesmanNumberTmpById(Long id);

    /**
     * @title: logicDeleteSalesmanNumberTmpById
     * @description: 禁用临时号主
     * @Param id 自增编号
     * @author: Yly
     * @return: void
     * @date: 2019-07-13 17:15
     **/
    void logicDeleteSalesmanNumberTmpById(Long id);

    /**
     * @return void 返回类型
     * @Title: updateSalesmanNumberRequestCountAll
     * @Description 恢复查询次数
     * @Param [count 次数]
     * @Author 杨凌云
     * @Date 2019/3/23 14:39
     **/
    void updateSalesmanNumberRequestCountAll(Long count);

    /**
     * @return void 返回类型
     * @Title: updateSalesmanNumberRequestCount
     * @Description 每次查询次数减1
     * @Param [id 编号]
     * @Author 杨凌云
     * @Date 2019/3/23 15:03
     **/
    void updateSalesmanNumberRequestCount(Long id);



    /**
     * @title: findSalesmanNumberTmpPositionById
     * @description: 通过临时号主编号获取位置信息
     * @Param id 临时号主编号
     * @author: Yly
     * @return: java.util.List<com.shenbi.biz.member.extend.SalesmanNumberTmpExt>
     * @date: 2019-07-14 14:08
     **/
    List<SalesmanNumberTmpVo> findSalesmanNumberTmpPositionById(Long id);

    /**
     * @title: findSalesmanNumberTmpPositionAll
     * @description: 获取所有临时号主位置信息 (取最新的日期)
     * @Param
     * @author: Yly
     * @return: java.util.List<com.shenbi.biz.member.extend.SalesmanNumberTmpExt>
     * @date: 2019-07-14 14:08
     **/
    List<SalesmanNumberTmpVo> findSalesmanNumberTmpPositionAll();

    /**
     * @title: findSalesmanNumberTmpByMobileAndName
     * @description: 通过电话与名字查询临时号主信息
     * @Param mobile 手机号
     * @Param name 名字
     * @author: Yly
     * @return: com.shenbi.biz.member.domain.SalesmanNumberTmp
     * @date: 2019-07-14 16:18
     **/
    SalesmanNumberTmp findSalesmanNumberTmpByMobileAndName(String mobile, String name);

    /**
     * @title: checkSalesmanNumberTmpInfo
     * @description: 获取临时号主范围与通讯录相关数量
     * @Param mobile 临时号主手机号
     * @Param name 临时号主名称
     * @author: Yly
     * @return: java.util.Map<java.lang.String, java.lang.Object>
     * @date: 2019-07-16 18:15
     **/
    Map<String, Object> checkSalesmanNumberTmpInfo(String mobile, String name);

}
