package com.sbdj.service.member.service;

import com.sbdj.service.member.admin.vo.SalesmanLeaderboardVo;
import com.sbdj.service.member.entity.SalesmanPercentageLeaderboard;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 业务员提成排行榜 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanPercentageLeaderboardService extends IService<SalesmanPercentageLeaderboard> {
    /**
     * 通过机构id获取业务员提成排行榜
     * @author Yly
     * @date 2019-11-28 16:13
     * @param orgId 机构id
     * @return void
     */
    void saveSalesmanPercentageLeaderboard(Long orgId);

    List<SalesmanLeaderboardVo> findSalesmanPercentageLeaderboardByOrgIdAndDate(Long orgId, String date);
}
