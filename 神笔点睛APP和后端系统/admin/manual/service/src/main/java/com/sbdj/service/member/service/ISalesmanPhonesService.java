package com.sbdj.service.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.core.base.HttpReqParam;
import com.sbdj.service.member.entity.SalesmanNumberPhonesTmp;
import com.sbdj.service.member.entity.SalesmanPhones;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * <p>
 * 业务员\业务员号主\通讯录表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanPhonesService extends IService<SalesmanPhones> {

    IPage<SalesmanPhones> findSalesmanPhonesPag(Long srcId, String srcType, IPage<SalesmanPhones> iPage);

    void saveSalesmanPhones(SalesmanPhones salesmanPhones);

    SalesmanPhones findSalesmanPhonesByImeiAndPhone(Long salesmanId, String type, String phone, String imei);

}
