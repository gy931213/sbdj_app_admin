package com.sbdj.service.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.member.entity.SalesmanNumberPhonesTmp;
import com.sbdj.service.member.entity.SalesmanPhonesTmp;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.data.domain.PageRequest;

/**
 * <p>
 * 临时号主\通讯录表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanPhonesTmpService extends IService<SalesmanNumberPhonesTmp> {

    IPage<SalesmanNumberPhonesTmp> findSalesmanNumberPhonesTmpPage(Long id);
}
