package com.sbdj.service.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.member.admin.vo.SalesmanRoyaltyVo;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanRoyalty;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.data.domain.PageRequest;

/**
 * <p>
 * 业务员额度增长配置表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanRoyaltyService extends IService<SalesmanRoyalty> {

    /**
     * 获取分页数据的接口
     * @author ZC
     * @date 2019-11-25 18:08
     * @param orgId 机构id
     * @param buildPageRequest 分页参数
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.member.admin.vo.SalesmanRoyaltyVo>
     */
    IPage<SalesmanRoyaltyVo> findSalesmanRoyaltyPageList(Long orgId, IPage<SalesmanRoyaltyVo> buildPageRequest);

    /**
     * 通过机构查询额度增长配置是否存在
     * @author ZC
     * @date 2019-11-26 10:59
     * @param orgId 机构ID
     * @return com.sbdj.service.member.entity.SalesmanRoyalty
     */
    SalesmanRoyalty findSalesmanRoyaltyByOrgId(Long orgId);

    /**
     * 保存额度增长配置
     * @author ZC
     * @date 2019-11-26 11:00
     * @param royalty 额度增长配置信息
     * @return void
     */
    void saveSalesmanRoyalty(SalesmanRoyalty royalty);



    /**
     * 更新数据的接口
     * @author ZC
     * @date 2019-11-25 18:08
     * @param royalty 要更新的数据
     * @return void
     */
    void updateSalesmanRoyalty(SalesmanRoyalty royalty);

    void logicDeleteSalesmanRoyalty(Long id);


}
