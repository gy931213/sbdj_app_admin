package com.sbdj.service.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.finance.scheduler.vo.StatementPercentageVo;
import com.sbdj.service.member.admin.query.TBInfo;
import com.sbdj.service.member.admin.query.TBQuery;
import com.sbdj.service.member.admin.vo.SalesmanListVo;
import com.sbdj.service.member.admin.vo.SalesmanRespVo;
import com.sbdj.service.member.admin.vo.SalesmanVo;
import com.sbdj.service.member.entity.Salesman;
import io.swagger.annotations.Api;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 业务员表 服务类
 * </p>
 *
 * @author Zc
 * @since 2019-11-22
 */
@Api(value = "用户管理", tags = {"查询业务员接口"})
public interface ISalesmanService extends IService<Salesman> {



    /**
     * 分页数据
     * @author ZC
     * @date 2019-11-26 15:27
     * @param pageParam 分页信息
     * @param params 查询条件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.member.admin.query.TBInfo>
     */
    IPage<TBInfo> findSalesmanPageList(IPage<SalesmanVo> pageParam, TBQuery params);


    /**
     * TODO
     * @author ZC
     * @date 2019-11-26 15:28
     * @param tbInfo 保存信息
     * @return void
     */
    void saveTBInfo(TBInfo tbInfo);



    /**
     * 根据机构ID与业务员手机号进行查询
     * @author ZC
     * @date 2019-11-26 15:28
     * @param org_id 机构ID
     * @param salesmanMobile 手机号
     * @return com.sbdj.service.member.entity.Salesman
     */
    Salesman findSalesmanByOrgIdAndMobile(long org_id, String salesmanMobile);

    /**
     * 通过业务员ID获取上线业务员(关系按钮)
     * @author ZC
     * @date 2019-11-26 15:28
     * @param salesmanId 业务员ID
     * @return java.util.List<com.sbdj.service.member.admin.vo.SalesmanVo>
     *
     */
    List<SalesmanVo> findOnlineRelation(Long salesmanId);

    /**
     * 通过TBInfo修改业务员数据
     * @author ZC
     * @date 2019-11-26 15:29
     * @param tbInfo 淘宝刷手信息
     * @return void
     */
    void updateTBInfo(TBInfo tbInfo);

    /**
     * 获取数据的接口
     * @author ZC
     * @date 2019-11-26 15:31
     * @param mobile 手机号
     * @return com.sbdj.service.member.entity.Salesman
     */
    Salesman findSalesmanByMobile(String mobile);

    /**
     * 判断淘宝用户是否已存在
     * @author ZC
     * @date 2019-11-26 15:31
     * @param tbInfo 淘宝用户信息
     * @return boolean
     */
    boolean tbVipExist(TBInfo tbInfo);

    /**
     *修改淘宝刷手状态
     * @param id 刷手ID
     * @param status 状态
     */
    void updateTBInfoStatusById(Long id, String status);


    // 修改刷手状态
    void updateTBInfoStatusById(Long id, Long oprId, String startStatus, String endStatus);
    /**
     * 禁用业务员账户多少天
     * @author ZC
     * @date 2019-11-26 15:32
     * @param ids 业务员ID
     * @param days 禁用天数
     * @return void
     */
    void disableSalesmanByIdsAndDays(String ids, Integer days);

    /**
     * 更新业务员是否安装过查小号
     * @author ZC
     * @date 2019-11-26 15:31
     * @param id 业务员编号
     * @param isSearch 是否安装查小号 0:否 1:是
     * @return void
     */
    void updateSalesmanSearch(Long id, Integer isSearch);

    /**
     * 更新数据的接口
     * @author ZC
     * @date 2019-11-26 15:32
     * @param salesman 要更新的数据
     * @return void
     */
    void updateSalesman(Salesman salesman);

    void updateSalesmanFirstMission(Long salesmanId);


    // 更新业务员的完成单量
    void updateSalesmanTaskTotal(Long salesmanId, int total);


    /**
     * 根据业务员id，更新信用度的接口
     * @author ZC
     * @date 2019-11-30 15:05
     * @param id 业务员id
     * @param credit 信息度
     * @param istf true:减信用度，false:加信用度
     * @return void
     */
    void updateSalesmanCreditById(Long id, BigDecimal credit, boolean istf);

    /**
     * 启用已被禁用的业务
     * @author Yly
     * @date 2019-11-27 11:28
     * @return void
     */
    void autoEnableSalesmanByDisableTime();

    /**
     * 保存待审核的业务
     * @author Yly
     * @date 2019-11-27 17:15
     * @param tbInfo 业务信息
     * @return void
     */
    void saveTBInfoTmp(TBInfo tbInfo);

    /**
     * 更新最后一次登录时间，和登录次数
     * @author Yly
     * @date 2019-11-28 09:55
     * @param id 业务员id
     * @return void
     */
    void updateSalesmanLastLoginTimeAndLoginTotal(Long id);

    /**
     * 更新业务员的信用额度
     * @author Yly
     * @date 2019-11-28 17:08
     * @return void
     */
    void updateSalesmanTotalCreditsAndSurplusCredit();

    /**
     * 通过业务员id与金额更新业务员信用额度
     * @author Yly
     * @date 2019-11-28 17:48
     * @param id 业务员id
     * @param credit 金额
     * @return void
     */
    void updateSalesmanTotalCreditAndSurplusCredit(Long id, BigDecimal credit);

    /**
     * 多少天不做单自动减额度
     * @author Yly
     * @date 2019-11-28 18:12
     * @param day 天数
     * @return void
     */
    void reduceSalesmanCredit(Integer day);

    /**
     * 计算业务员的余额，余额来源于，任务佣金
     * @author Yly
     * @date 2019-11-28 19:00
     * @return void
     */
    void computeTaskBrokerageToSalesmanBalance();

    /**
     * 计算业务员的余额，余额来源于，业务员所做的任务，业务员上级所得的提成
     * @author Yly
     * @date 2019-11-28 19:13
     * @return void
     */
    void computeTaskPercentageToSalesmanBalance();

    /**
     * 生成业务员提成
     * @author Yly
     * @date 2019-12-01 13:04
     * @param date 日期
     * @param jobNumber 工号
     * @return void
     */
    void computeTaskPercentageToSalesmanBalance(String date, String jobNumber);

    /**
     * 保存业务员提成
     * @author Yly
     * @date 2019-12-01 14:05
     * @param statementPercentageVoList 数据
     * @param date 日期
     * @return void
     */
    void saveSalesmanPercentage(List<StatementPercentageVo> statementPercentageVoList, Date date);

    /**
     * 没有按时提交现付单
     * @author Yly
     * @date 2019-11-28 19:38
     * @return void
     */
    void disablesSalesmanByFactorXFD();

    /**
     * 没有按时提交隔日单
     * @author Yly
     * @date 2019-11-28 19:39
     * @return void
     */
    void disablesSalesmanByFactorGRD();

    /**
     * Created by Yly to 2019/8/31 14:50
     * 通过号主编号获取上线业务员
     * @param salesmanNumberId 号主编号
     * @return com.shenbi.biz.member.domain.Salesman
     **/
    Salesman findSalesmanOnlineById(Long salesmanNumberId);


    /**
     * 查询业务员列表
     * @author ZC
     * @date 2019-11-29 10:57
     * @param buildPageRequest 分页参数
     * @param uid 业务员ID
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.member.admin.vo.SalesmanListVo>
     */
    IPage<SalesmanListVo> findAllSalesman(IPage<SalesmanListVo> buildPageRequest, Long uid);

    Salesman findSalesmanBySalesmanNumberId(Long salesmanNumberId);

    /**
     * Created by Yly to 2019/9/7 20:02
     * 获取该上线的所有下线业务员
     * @param salesmanId 上线业务员id
     * @return java.util.List<com.shenbi.biz.member.domain.Salesman>
     **/
    List<Salesman> findSalesmanOffline(Long salesmanId);


    /**
     * 保存数据
     * @author ZC
     * @date 2019-11-29 13:08
     * @param salesman 要保存的数据
     * @return void
     */
    void saveSalesman(Salesman salesman);

    /**
     * 更新业务员信息的接口
     * @author ZC
     * @date 2019-11-30 15:06
     * @param salesman 业务员
     * @return void
     */
    void updateSalesmanInfo(Salesman salesman);

    /**
     * 获取业务员信息的接口
     * @author ZC
     * @date 2019-11-30 15:06
     * @param uid 业务员id
     * @return com.sbdj.service.member.admin.vo.SalesmanVo
     */
    SalesmanVo findSalesmanVoInfo(Long uid);

    /**
     * 修改业务员密码
     * @author ZC
     * @date 2019-11-29 15:27
     * @param encryptPwd 密码
     * @param uid 业务员ID
     * @return void
     */
    void updateSalesmanPassword(String encryptPwd, Long uid);

    /**
     * 获取业务员登录信息（api）
     * @author ZC
     * @date 2019-11-30 15:07
     * @param uid 业务员id
     * @return com.sbdj.service.member.admin.vo.SalesmanRespVo
     */
    SalesmanRespVo getSalesmanInfo(Long uid);

    SalesmanVo findSalesmanExtCredit(Long uid);

    /**
     * @return void 返回类型
     * @Title: withdrawSalesmanMoney
     * @Description 提现业务员金额
     * @Param [salesmanId, money]
     * @Author 杨凌云
     * @Date 2019/6/21 9:57
     **/
    void withdrawSalesmanMoney(Long salesmanId, BigDecimal money);

    /**
     * 更新业务员余额
     * @author Yly
     * @date 2019-12-01 10:37
     * @param id id
     * @param money 金额
     * @return void
     */
    void updateSalesmanMoney(Long id, BigDecimal money);

    /**
     * 检测该银行卡是否注册过业务
     * @author Yly
     * @date 2019-12-07 11:50
     * @param bankNum 银行卡号
     * @return boolean
     */
    boolean isExistSalesmanByBank(String bankNum);

    /**
     * 检测该身份证是否注册过业务
     * @author Yly
     * @date 2019-12-07 11:51
     * @param idCard 证件号
     * @return boolean
     */
    boolean isExistSalesmanByIDCard(String idCard);

    /**
     * 检测该QQ是否注册过业务
     * @author Yly
     * @date 2019-12-15 14:23
     * @param qq qq
     * @return boolean
     */
    boolean isExistSalesmanByQQ(String qq);

    /**
     * 检测该微信号是否注册过业务
     * @author Yly
     * @date 2019-12-15 14:23
     * @param weChat 微信号
     * @return boolean
     */
    boolean isExistSalesmanByWeChat(String weChat);
}
