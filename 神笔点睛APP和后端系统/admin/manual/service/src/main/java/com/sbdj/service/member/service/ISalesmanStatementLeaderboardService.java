package com.sbdj.service.member.service;

import com.sbdj.service.member.admin.vo.SalesmanLeaderboardVo;
import com.sbdj.service.member.entity.SalesmanStatementLeaderboard;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 业务员佣金排行榜 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISalesmanStatementLeaderboardService extends IService<SalesmanStatementLeaderboard> {
    /**
     * 通过机构获取业务员佣金排行榜
     * @author Yly
     * @date 2019-11-28 16:30
     * @param orgId 机构id
     * @return void
     */
    void saveSalesmanStatementLeaderboard(Long orgId);

    List<SalesmanLeaderboardVo> findSalesmanStatementLeaderboardByOrgIdAndDate(Long orgId, String date);
}
