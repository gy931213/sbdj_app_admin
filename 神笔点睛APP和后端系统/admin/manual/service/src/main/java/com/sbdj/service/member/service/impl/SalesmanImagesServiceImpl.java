package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.service.member.admin.vo.SalesmanImagesVo;
import com.sbdj.service.member.entity.SalesmanImages;
import com.sbdj.service.member.mapper.SalesmanImagesMapper;
import com.sbdj.service.member.service.ISalesmanImagesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 业务员\业务员号主，相关的证件图片集合表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanImagesServiceImpl extends ServiceImpl<SalesmanImagesMapper, SalesmanImages> implements ISalesmanImagesService {

    @Override
    public List<SalesmanImagesVo> findSalesmanImagesBySrcIdAndSrcType(Long srcId, String srcType) {
        QueryWrapper<SalesmanImages> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("si.src_id",srcId);
        queryWrapper.eq("si.src_type",srcType);
        queryWrapper.eq("si.status",Status.STATUS_ACTIVITY);
        queryWrapper.orderByAsc("scd.sort");
        return baseMapper.findSalesmanImagesBySrcIdAndSrcType(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSalesmanImages(SalesmanImages salesmanImages) {
        salesmanImages.setCreateTime(new Date());
        salesmanImages.setStatus(Status.STATUS_ACTIVITY);
        save(salesmanImages);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteSalesmanImages(String srcType, Long srcId) {
        UpdateWrapper<SalesmanImages> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("src_type", srcType);
        updateWrapper.eq("src_id", srcId);
        update(updateWrapper);
    }
}
