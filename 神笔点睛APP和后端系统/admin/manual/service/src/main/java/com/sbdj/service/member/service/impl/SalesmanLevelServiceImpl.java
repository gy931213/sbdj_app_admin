package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.entity.SalesmanLevel;
import com.sbdj.service.member.mapper.SalesmanLevelMapper;
import com.sbdj.service.member.service.ISalesmanLevelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 业务员等级表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanLevelServiceImpl extends ServiceImpl<SalesmanLevelMapper, SalesmanLevel> implements ISalesmanLevelService {

    @Override
    public IPage<SalesmanLevel> findSalesmanLevelPageList(IPage<SalesmanLevel> reqpPage) {
        QueryWrapper<SalesmanLevel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",Status.STATUS_ACTIVITY).orderByAsc("sort");
        return baseMapper.selectPage(reqpPage, queryWrapper);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSalesmanLevel(SalesmanLevel salesmanLevel) {
        salesmanLevel.setCreateTime(new Date());
        salesmanLevel.setStatus(Status.STATUS_ACTIVITY);
        save(salesmanLevel);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanLevel(SalesmanLevel salesmanLevel) {
        if (StrUtil.isNull(salesmanLevel) || StrUtil.isNull(salesmanLevel.getId())) {
            throw BaseException.base("更新业务员等级失败,id为空");
        }
        updateById(salesmanLevel);
    }

    @Override
    public List<SalesmanLevel> findSalesmanLevelSelect() {
        QueryWrapper<SalesmanLevel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",Status.STATUS_ACTIVITY);
        return baseMapper.selectList(queryWrapper);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void loginDeleteSalesmanLevel(Long id) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除等级配置失败,id为空");
        }
        SalesmanLevel salesmanLevel = baseMapper.selectById(id);
        //将对象状态改为删除状态
        salesmanLevel.setStatus(Status.STATUS_DELETE);
        baseMapper.updateById(salesmanLevel);

    }

    @Override
    public SalesmanLevel findSalesmanLowestLevel() {
        QueryWrapper<SalesmanLevel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.orderByAsc("sort");
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Override
    public SalesmanLevel findSalesmanLowerLevel(Long salesmanLevelId) {
        SalesmanLevel sl = getById(salesmanLevelId);
        if(null != sl && sl.getSort() == 1) {
            return sl;
        }
        return baseMapper.findSalesmanLowerLevel(salesmanLevelId);
    }
}
