package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.SendSmsUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.admin.query.RegisterQuery;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanMobileCode;
import com.sbdj.service.member.mapper.SalesmanMobileCodeMapper;
import com.sbdj.service.member.service.ISalesmanMobileCodeService;
import com.sbdj.service.member.service.ISalesmanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 手机号验证码表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanMobileCodeServiceImpl extends ServiceImpl<SalesmanMobileCodeMapper, SalesmanMobileCode> implements ISalesmanMobileCodeService {

    @Autowired
    private ISalesmanService iSalesmanService;
    @Lazy
    @Autowired
    private ISalesmanMobileCodeService iSalesmanMobileCodeService;

    @Override
    public SalesmanMobileCode findSalesmanMobileCode(String mobile) {
        if (StrUtil.isNull(mobile)) {
            throw BaseException.base("手机号为空,停止获取临时号主验证码信息");
        }
        QueryWrapper<SalesmanMobileCode> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mobile", mobile);
        queryWrapper.orderByDesc("create_time");
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String sendSmsBySalesmanNumber(String mobile) {
        Map<String, String> map = generateSmsTmp();
        SendSmsUtil.sendSms(mobile, "点睛", "SMS_177241611", map.get("temp"));
        iSalesmanMobileCodeService.saveSalesmanMobileCode(mobile, map.get("code"));
        return map.get("code");
    }

    private Map<String, String> generateSmsTmp() {
        // 初始化JSON
        Gson gson = new GsonBuilder().create();
        // 生成固定6位随机整数
        int num = (int)((Math.random()*9+1)*100000);
        String result = String.valueOf(num);
        // 生成JSON
        Map<String, String> map = new HashMap<>();
        map.put("code", result);
        String temp = gson.toJson(map);
        map.put("temp", temp);
        return map;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String sendSmsBySalesman(String mobile) {
        Salesman salesman = iSalesmanService.findSalesmanByMobile(mobile);
        if (StrUtil.isNotNull(salesman)) {
            throw BaseException.base("号码已被注册");
        }
        Map<String, String> map = generateSmsTmp();
        SendSmsUtil.sendSms(mobile, "点睛", "SMS_177241611", map.get("temp"));
        iSalesmanMobileCodeService.saveSalesmanMobileCode(mobile, map.get("code"));
        return map.get("code");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSalesmanMobileCode(String mobile, String temp) {
        SalesmanMobileCode mobileCode = new SalesmanMobileCode();
        // ~创建时间累积5分钟>>>>过期时间
        long time = System.currentTimeMillis() + 5 * 60 * 1000;
        mobileCode.setExpireTime(new Date(time));
        mobileCode.setCreateTime(new Date());
        mobileCode.setMobile(mobile);
        mobileCode.setCode(temp);
        save(mobileCode);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void register(RegisterQuery registerParam, Long salesmanId) {
        // 2019年9月2日 18:45:41 手工单 邀请注册
        //Salesman salesmanOne = iSalesmanService.findSalesmanOne(id);
        //SalesmanLevel salesmanLowestLevel = iSalesmanLevelService.findSalesmanLowestLevel();
        Salesman salesman = new Salesman();
        salesman.setName(registerParam.getName());
        salesman.setSalesmanId(registerParam.getSalesmanId());
        salesman.setMobile(registerParam.getMobile());
        // 默认密码
        salesman.setPwd("123456");
        salesman.setCreateTime(new Date());
        salesman.setOrgId(2L);
        //salesman.setOrgId(salesmanOne.getOrgId());
        /*if(salesmanLowestLevel != null){
            salesman.setSalesmanLevelId(salesmanLowestLevel.getId());
        }else {
            salesman.setSalesmanLevelId(-1l);
        }*/
        salesman.setSalesmanLevelId(1L);
        iSalesmanService.saveSalesman(salesman);
    }
}
