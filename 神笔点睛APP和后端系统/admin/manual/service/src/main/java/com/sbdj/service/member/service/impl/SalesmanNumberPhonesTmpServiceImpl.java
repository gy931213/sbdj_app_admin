package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.entity.SalesmanNumberPhonesTmp;
import com.sbdj.service.member.mapper.SalesmanNumberPhonesTmpMapper;
import com.sbdj.service.member.service.ISalesmanNumberPhonesTmpService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 临时号主\通讯录表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanNumberPhonesTmpServiceImpl extends ServiceImpl<SalesmanNumberPhonesTmpMapper, SalesmanNumberPhonesTmp> implements ISalesmanNumberPhonesTmpService {
    @Override
    public SalesmanNumberPhonesTmp findSalesmanNumberPhonesTmp(Long targetId, String phone, String imei) {
        QueryWrapper<SalesmanNumberPhonesTmp> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("target_id", targetId);
        queryWrapper.eq("phone", phone);
        queryWrapper.eq(StrUtil.isNotNull(imei), "imei", imei);
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSalesmanNumberPhonesTmp(SalesmanNumberPhonesTmp salesmanNumberPhonesTmp) {
        salesmanNumberPhonesTmp.setCreateTime(new Date());
        save(salesmanNumberPhonesTmp);
    }

    @Override
    public IPage<SalesmanNumberPhonesTmp> findSalesmanNumberPhonesTmpPage(Long targetId,IPage<SalesmanNumberPhonesTmp> page) {
        QueryWrapper<SalesmanNumberPhonesTmp> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("target_id",targetId);
        return page(page, queryWrapper);
    }

    @Transactional(readOnly = true)
    @Override
    public List<SalesmanNumberPhonesTmp> findSalesmanNumberPhonesTmpList(Long targetId) {
        QueryWrapper<SalesmanNumberPhonesTmp> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("target_id",targetId);
        return list(queryWrapper);
    }

    @Override
    public int countSalesmanNumberPhonesTmpByTargetId(Long id) {
        QueryWrapper<SalesmanNumberPhonesTmp> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("target_id",id);
        return count(queryWrapper);
    }

}
