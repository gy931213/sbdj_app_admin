package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.admin.query.SalesmanNumberQuery;
import com.sbdj.service.member.admin.vo.SalesmanNumberListVo;
import com.sbdj.service.member.admin.vo.SalesmanNumberVo;
import com.sbdj.service.member.app.vo.SalesmanNumberRespVo;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.member.mapper.SalesmanNumberMapper;
import com.sbdj.service.member.service.ISalesmanNumberService;
import com.sbdj.service.system.admin.vo.AreaVo;
import com.sbdj.service.system.type.AreaType;
import com.sbdj.service.task.service.ITaskSonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 号主表 服务实现类
 * </p>
 *
 * @author Zc
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanNumberServiceImpl extends ServiceImpl<SalesmanNumberMapper, SalesmanNumber> implements ISalesmanNumberService {
    @Autowired
    private ITaskSonService iTaskSonService;

    @Override
    public SalesmanNumber findSalesmanNumberBySalesmanIdAndPlatformId(Long salesmanId, Long platformId) {
        QueryWrapper<SalesmanNumber> sysNumberQueryWrapper = new QueryWrapper<>();
        sysNumberQueryWrapper.eq("salesman_id", salesmanId);
        sysNumberQueryWrapper.eq("platform_Id", platformId);
        return getOne(sysNumberQueryWrapper);
    }

    @Override
    public List<AreaVo> findSalesmanNumberAreaByAreaCode(String areaCode) {
        return baseMapper.findSalesmanNumberAreaByAreaCode(AreaType.city, AreaType.district, AreaType.province, areaCode);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanNumberStatus(Long id, String status) {
        SalesmanNumber salesmanNumber = baseMapper.selectById(id);
        salesmanNumber.setStatus(status);
        baseMapper.updateById(salesmanNumber);
    }

    @Override
    public IPage<SalesmanNumberVo> findSalesmanNumberPage(SalesmanNumberQuery params, IPage<SalesmanNumberVo> page) {
        QueryWrapper<SalesmanNumberQuery> queryWrapper = new QueryWrapper<>();
        // ~ 关键字查询模糊匹配~
        queryWrapper.and(StrUtil.isNotNull(params.getKeyword()), f->f.like("sn.name", params.getKeyword())
                .or().like( "sn.card_num", params.getKeyword())
                .or().like( "sn.onlineid", params.getKeyword())
                .or().like( "sn.imei", params.getKeyword())
                .or().like( "sn.mobile", params.getKeyword())
                .or().like( "sm.name", params.getKeyword()));

        // ~ 机构
        queryWrapper.eq(StrUtil.isNotNull(params.getOrgId()) && !Number.LONG_ZERO.equals(params.getOrgId()), "sm.org_id", params.getOrgId());
        // ~ 省
        queryWrapper.eq(StrUtil.isNotNull(params.getProvinceId()) && !Number.LONG_ZERO.equals(params.getProvinceId()), " sn.province_id", params.getProvinceId());
        // ~ 市
        queryWrapper.eq(StrUtil.isNotNull(params.getCityId()) && !Number.LONG_ZERO.equals(params.getCityId()), " sn.city_id", params.getCityId());
        // ~ 县/区
        queryWrapper.eq(StrUtil.isNotNull(params.getCountyId()) && !Number.LONG_ZERO.equals(params.getCountyId()), " sn.county_id", params.getCountyId());
        // ~ 所属平台
        queryWrapper.eq(StrUtil.isNotNull(params.getPlatformId()) && !Number.LONG_ZERO.equals(params.getPlatformId()), " sn.platform_id", params.getPlatformId());
        // ~ 号主姓名
        queryWrapper.eq(StrUtil.isNotNull(params.getName()), "sn.name", params.getName());
        // ~ 号主ID
        queryWrapper.eq(StrUtil.isNotNull(params.getOnlineid()), " sn.onlineid", params.getOnlineid());
        // ~ 号主电话
        queryWrapper.eq(StrUtil.isNotNull(params.getMobile()), "sn.mobile", params.getMobile());
        // ~ 所属业务员姓名
        queryWrapper.eq(StrUtil.isNotNull(params.getSalesmanName()), " sm.name", params.getSalesmanName());
        // ~身份证号
        queryWrapper.eq(StrUtil.isNotNull(params.getCardNum()), " sn.card_num", params.getCardNum());
        // ~ 状态
        queryWrapper.eq(StrUtil.isNotNull(params.getStatus()), " sn.status", params.getStatus());
        return baseMapper.findSalesmanNumberPage(page, params, queryWrapper);
    }

    @Override
    public Integer findSalesmanNumberTotalBySalesmanIdAndPlatformId(Long salesmanId, Long platformId) {
        QueryWrapper<SalesmanNumber> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("salesman_Id", salesmanId);
        queryWrapper.eq("platform_Id", platformId);
        return baseMapper.selectCount(queryWrapper);
    }


    @Override
    public boolean isExistSalesmanNumber(Long platformId, String onlineid) {
        QueryWrapper<SalesmanNumber> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("platform_id", platformId);
        queryWrapper.eq("onlineid", onlineid);
        BigDecimal bigDecimal = BigDecimal.valueOf(baseMapper.selectCount(queryWrapper).longValue());
        return StrUtil.isNotNull(bigDecimal) && bigDecimal.intValue() > 0;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addSalesmanNumber(SalesmanNumber salesmanNumber) {
        salesmanNumber.setOften("1");
        salesmanNumber.setCreateTime(new Date());
        salesmanNumber.setStatus(Status.STATUS_ENABLE);
        salesmanNumber.setRequestCount(10);
        baseMapper.insert(salesmanNumber);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanNumberData(SalesmanNumber sn) {
        if (StrUtil.isNull(sn) || StrUtil.isNull(sn.getId())) {
            throw BaseException.base("更新号主信息失败,id为空");
        }
        updateById(sn);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void disableSalesmanNumberByIds(String ids) {
        /*if (!ids.contains(",")) {
            SalesmanNumber salesmanNumber = new SalesmanNumber();
            salesmanNumber.setStatus(Status.STATUS_PROHIBIT);
            salesmanNumber.setUpdateTime(new Date());
            UpdateWrapper<SalesmanNumber> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq(StrUtil.isNotNull(ids), "salesman_id", Long.valueOf(ids));
            baseMapper.update(salesmanNumber, updateWrapper);
        } else {
            String[] tmp = ids.split(",");
            for (String s : tmp) {
                SalesmanNumber salesmanNumber = new SalesmanNumber();
                salesmanNumber.setStatus(Status.STATUS_PROHIBIT);
                salesmanNumber.setUpdateTime(new Date());
                UpdateWrapper<SalesmanNumber> updateWrapper = new UpdateWrapper<>();
                updateWrapper.eq("salesman_id", Long.valueOf(s));
                baseMapper.update(salesmanNumber, updateWrapper);
            }
        }*/
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("禁用号主对应的业务员失败,业务员id为空");
        }
        UpdateWrapper<SalesmanNumber> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_PROHIBIT);
        updateWrapper.set("update_time", new Date());
        updateWrapper.in("salesman_id", list);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanNumberStatusBySalesmanId(Long salesmanId, String status) {
        /*SalesmanNumber salesmanNumber = new SalesmanNumber();
        salesmanNumber.setStatus(status);
        salesmanNumber.setUpdateTime(new Date());
        UpdateWrapper<SalesmanNumber> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq(StrUtil.isNotNull(salesmanId), "salesman_id", salesmanId);
        baseMapper.update(salesmanNumber, updateWrapper);*/
        if (StrUtil.isNull(salesmanId)) {
            throw BaseException.base("禁用号主对应的业务员失败,业务员id为空");
        }
        UpdateWrapper<SalesmanNumber> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", status);
        updateWrapper.set("update_time", new Date());
        updateWrapper.eq("salesman_id", salesmanId);
        update(updateWrapper);
    }

    @Override
    public SalesmanNumber findSalesmanNumberByPlatformIdOnlineId(Long platformId, String onlineId) {
        QueryWrapper<SalesmanNumber> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("platform_id", platformId);
        queryWrapper.eq("onlineid", onlineId);
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Override
    public SalesmanNumber findSalesmanNumberBySalIdAndSalNumberId(Long salId, Long salNumberId) {
        QueryWrapper<SalesmanNumber> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("salesman_id", salId).eq("id", salNumberId);
        return getOne(queryWrapper);
    }

    @Override
    public IPage<SalesmanNumberListVo> findSalesmanNumberPageApp(IPage<SalesmanNumberListVo> buildPageRequest, String status, String onlineid, Long platformId, Long salId) {
        QueryWrapper<SalesmanNumberListVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cp.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("sn.salesman_id", salId);
        // 判断搜索条件不为空
        queryWrapper.like(StrUtil.isNotNull(onlineid), "sn.onlineid like", onlineid);
        queryWrapper.eq(platformId != null && !platformId.equals(Number.LONG_ZERO), "sn.platform_id", platformId);
        queryWrapper.eq(StrUtil.isNotNull(status), "sn.status", status);

        // sql.append(" group by ts.salesman_number_id");

        // 获取号主的任务已完成的数据！
        IPage<SalesmanNumberListVo> page = baseMapper.findSalesmanNumberPageApp(buildPageRequest,queryWrapper);

        for (SalesmanNumberListVo number : page.getRecords()) {
            number.setNum(iTaskSonService.completedTaskCountBySalNumberId(number.getId()));
        }
        return page;


    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void setUsed(Long id, String often) {
        UpdateWrapper<SalesmanNumber> wrapper = new UpdateWrapper<>();
        wrapper.set("often",often).eq("id",id);
        update(wrapper);
    }

    @Override
    public SalesmanNumberRespVo findSalesmanNumberById(Long id) {
        QueryWrapper<SalesmanNumberRespVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sn.id",id);
        return baseMapper.findSalesmanNumberById(queryWrapper);
    }

    @Override
    public Integer findSalesmanNumberCountByImei(String imei, Long platformId) {
        QueryWrapper<SalesmanNumber> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("imei",imei).eq("platform_id",platformId);
        queryWrapper.select("id");
        return count(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updatesalesmanNumber(SalesmanNumber salesmanNumber) {
        if (StrUtil.isNull(salesmanNumber) || StrUtil.isNull(salesmanNumber.getId())) {
            throw BaseException.base("更新号主信息失败,id为空");
        }
        updateById(salesmanNumber);
    }

    @Override
    public IPage<SalesmanNumberListVo> searchSalesmanNumberByPlatformIdOnlineId(IPage<SalesmanNumberListVo> buildPageRequest, String onlineId, Long platformId, Long salId) {
        QueryWrapper<SalesmanNumberListVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cp.status",Status.STATUS_ACTIVITY);
        queryWrapper.like("sn.onlineid",onlineId);
        queryWrapper.eq("sn.salesman_id",salId);
            queryWrapper.eq(StrUtil.isNotNull(platformId) && !Number.LONG_ZERO.equals(platformId),"sn.platform_id",platformId);

        // sql.append(" group by ts.salesman_number_id");

        // 获取号主的任务已完成的数据！
        IPage<SalesmanNumberListVo> page = baseMapper.searchSalesmanNumberByPlatformIdOnlineId(buildPageRequest,queryWrapper);
        for (SalesmanNumberListVo number : page.getRecords()) {
            number.setNum(iTaskSonService.completedTaskCountBySalNumberId(number.getId()));
        }

        return page;
    }

    @Override
    public SalesmanNumber findSalesmanNumber(Long salesmanId, Long platformId, String onlineId) {
        QueryWrapper<SalesmanNumber> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id","salesman_id","name","platform_id","onlineid","status","imei","mobile")
                .eq("salesman_id",salesmanId).eq("platform_id",platformId).eq("onlineid",onlineId);
        
        return getOne(queryWrapper);
    }

    @Override
    public boolean isExistSalesmanNumberByMobile(String mobile, Long platformId) {
        if (StrUtil.isNull(mobile) || StrUtil.isNull(platformId)) {
            return false;
        }
        return count(new QueryWrapper<SalesmanNumber>().eq("mobile", mobile).eq("platform_id", platformId).select("id")) > 0;
    }

    @Override
    public boolean isExistSalesmanNumberByBank(String bankNum, Long platformId) {
        if (StrUtil.isNull(bankNum) || StrUtil.isNull(platformId)) {
            return false;
        }
        return count(new QueryWrapper<SalesmanNumber>().eq("bank_num", bankNum).eq("platform_id", platformId).select("id")) > 0;
    }

    @Override
    public boolean isExistSalesmanNumberByIDCard(String idCard, Long platformId) {
        if (StrUtil.isNull(idCard) || StrUtil.isNull(platformId)) {
            return false;
        }
        return count(new QueryWrapper<SalesmanNumber>().eq("card_num", idCard).eq("platform_id", platformId).select("id")) > 0;
    }
}
