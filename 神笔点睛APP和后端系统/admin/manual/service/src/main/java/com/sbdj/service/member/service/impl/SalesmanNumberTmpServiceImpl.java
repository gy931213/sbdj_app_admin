package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.admin.query.SalesmanNumberQuery;
import com.sbdj.service.member.admin.vo.SalesmanNumberTmpVo;
import com.sbdj.service.member.entity.SalesmanNumberTmp;
import com.sbdj.service.member.mapper.SalesmanNumberTmpMapper;
import com.sbdj.service.member.service.ISalesmanNumberPhonesTmpService;
import com.sbdj.service.member.service.ISalesmanNumberTmpService;
import com.sbdj.service.system.service.ISysMapTrajectoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 临时号主表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanNumberTmpServiceImpl extends ServiceImpl<SalesmanNumberTmpMapper, SalesmanNumberTmp> implements ISalesmanNumberTmpService {

    @Lazy
    @Autowired
    private ISalesmanNumberTmpService iSalesmanNumberTmpService;
    @Autowired
    private ISysMapTrajectoryService iSysMapTrajectoryService;
    @Autowired
    private ISalesmanNumberPhonesTmpService iSalesmanNumberPhonesTmpService;

    @Override
    public SalesmanNumberTmp findSalesmanNumberByPhone(String phone) {
        QueryWrapper<SalesmanNumberTmp> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mobile", phone);
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addSalesmanNumberTmp(SalesmanNumberTmp salesmanNumberTmp) {
        if (StrUtil.isNull(salesmanNumberTmp)) {
            throw BaseException.base("临时号主信息为空");
        }
        salesmanNumberTmp.setCreateTime(new Date());
        salesmanNumberTmp.setRequestCount(10);
        salesmanNumberTmp.setStatus(Status.STATUS_ACTIVITY);
        save(salesmanNumberTmp);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanNumber(SalesmanNumberTmp salesmanNumber) {
        if (StrUtil.isNull(salesmanNumber) || StrUtil.isNull(salesmanNumber.getId())) {
            throw BaseException.base("临时号主ID为空,停止更新");
        }
        UpdateWrapper<SalesmanNumberTmp> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set(StrUtil.isNotNull(salesmanNumber.getName()), "name", salesmanNumber.getName());
        updateWrapper.set(StrUtil.isNotNull(salesmanNumber.getImei()), "imei", salesmanNumber.getImei());
        updateWrapper.set(StrUtil.isNotNull(salesmanNumber.getMobile()), "mobile", salesmanNumber.getMobile());
        updateWrapper.set("update_time", new Date());
        updateWrapper.eq("id", salesmanNumber.getId());
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanNumberTmpIsChat(Long id, Integer isChat) {
        if (StrUtil.isNull(id) || StrUtil.isNull(isChat)) {
            throw BaseException.base("缺失必要信息,更新临时号主首次聊天失败");
        }
        UpdateWrapper<SalesmanNumberTmp> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("is_chat", isChat);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Override
    public IPage<SalesmanNumberTmp> findSalesmanNumberPage(SalesmanNumberQuery params, IPage<SalesmanNumberTmp> page) {
        QueryWrapper<SalesmanNumberTmp> queryWrapper = new QueryWrapper<>();

        // ~ 号主姓名
        queryWrapper.eq(StrUtil.isNotNull(params.getName()),  "name",params.getName());
        // ~ 号主电话
        queryWrapper.eq(StrUtil.isNotNull(params.getMobile()), "mobile",params.getMobile());
        // ~ 关键字查询模糊匹配~
        queryWrapper.and(StrUtil.isNotNull(params.getKeyword()), f->f.like("name",params.getKeyword())
                .or().like("imei",params.getKeyword()).or().like("mobile",params.getKeyword()));

        return baseMapper.selectPage(page,queryWrapper);
    }

    @Override
    public SalesmanNumberTmp findSalesmanOne(long id) {
        return getById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updatesalesmanNumber(SalesmanNumberTmp salesmanNumber) {
        if (StrUtil.isNull(salesmanNumber) || StrUtil.isNull(salesmanNumber.getId())) {
            throw BaseException.base("更新临时号主失败,id为空");
        }
        updateById(salesmanNumber);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteSalesmanNumberTmpById(Long id) {
        removeById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteSalesmanNumberTmpById(Long id) {
        SalesmanNumberTmp salesmanNumberTmp = baseMapper.selectById(id);
        salesmanNumberTmp.setStatus(Status.STATUS_DELETE);
        baseMapper.updateById(salesmanNumberTmp);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanNumberRequestCountAll(Long count) {
        UpdateWrapper<SalesmanNumberTmp> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("request_count", count);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanNumberRequestCount(Long id) {
        UpdateWrapper<SalesmanNumberTmp> updateWrapper = new UpdateWrapper<>();
        updateWrapper.apply("request_count=(request_count-1)");
        updateWrapper.eq("id", id);
        updateWrapper.gt("request_count", 0);
        update(updateWrapper);
    }

    @Override
    public List<SalesmanNumberTmpVo> findSalesmanNumberTmpPositionById(Long id) {
        QueryWrapper<SalesmanNumberTmp> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("snt.id",id);
        return baseMapper.findSalesmanNumberTmpPositionById(queryWrapper);
    }

    @Override
    public List<SalesmanNumberTmpVo> findSalesmanNumberTmpPositionAll() {
        List<SalesmanNumberTmpVo> list = baseMapper.findSalesmanNumberTmpPositionAll();
        Map<Long, SalesmanNumberTmpVo> map = new HashMap<>();
        // 选取最新的数据
        for (SalesmanNumberTmpVo salesmanNumberTmpExt : list) {
            deduplication(map, salesmanNumberTmpExt);
        }
        // 重新封装
        list.clear();
        for (Map.Entry<Long, SalesmanNumberTmpVo> entry : map.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    private void deduplication(Map<Long, SalesmanNumberTmpVo> map, SalesmanNumberTmpVo snte) {
        SalesmanNumberTmpVo snteKey = map.get(snte.getId());
        if (snteKey != null) {
            if (snte.getCreateTime().compareTo(snteKey.getCreateTime()) > 0) {
                map.put(snte.getId(), snte);
            }
        } else {
            // 存入数据 key:临时号主id value:临时号主相关信息
            map.put(snte.getId(), snte);
        }
    }

    @Override
    public SalesmanNumberTmp findSalesmanNumberTmpByMobileAndName(String mobile, String name) {
        QueryWrapper<SalesmanNumberTmp> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mobile",mobile).eq("name",name);
        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public Map<String, Object> checkSalesmanNumberTmpInfo(String mobile, String name) {
        Map<String,Object> map = new HashMap<>();
        // 范围: 是 否
        String positionKey = "position";
        // 通讯录 数量
        String contactKey = "contactCount";
        SalesmanNumberTmp snt = iSalesmanNumberTmpService.findSalesmanNumberTmpByMobileAndName(mobile, name);
        if (StrUtil.isNull(snt)) { throw BaseException.base("信息不存在");}
        int positionCount = iSysMapTrajectoryService.countMapTrajectoryByTargetId(snt.getId());
        if (positionCount > 0) {
            map.put(positionKey, true);
        } else {
            map.put(positionKey, false);
        }
        int contactCount = iSalesmanNumberPhonesTmpService.countSalesmanNumberPhonesTmpByTargetId(snt.getId());
        // 1.通讯录条数 必须满足20条及以上
        if (contactCount < 20) {
            map.put(contactKey, 10);
        } else if (contactCount > 20 && contactCount < 50) {
            map.put(contactKey, 20);
        } else if (contactCount > 50 && contactCount < 100) {
            map.put(contactKey, 50);
        } else {
            map.put(contactKey, 100);
        }
        return map;

    }
}
