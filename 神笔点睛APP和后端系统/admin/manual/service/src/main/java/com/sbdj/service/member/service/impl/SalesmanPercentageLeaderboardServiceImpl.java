package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.DateUtil;
import com.sbdj.service.member.admin.vo.SalesmanLeaderboardVo;
import com.sbdj.service.member.entity.SalesmanPercentageLeaderboard;
import com.sbdj.service.member.mapper.SalesmanPercentageLeaderboardMapper;
import com.sbdj.service.member.service.ISalesmanPercentageLeaderboardService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 业务员提成排行榜 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanPercentageLeaderboardServiceImpl extends ServiceImpl<SalesmanPercentageLeaderboardMapper, SalesmanPercentageLeaderboard> implements ISalesmanPercentageLeaderboardService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSalesmanPercentageLeaderboard(Long orgId) {
        // 获取上个月份
        String createDate = DateUtil.getBeforeMonth();;
        QueryWrapper<SalesmanPercentageLeaderboard> totalWrapper = new QueryWrapper<>();
        totalWrapper.eq("org_id", orgId);
        totalWrapper.eq("create_date", createDate);
        totalWrapper.select("id");
        int count = count(totalWrapper);
        if (count >= 0) {
            return;
        }
        QueryWrapper<SalesmanPercentageLeaderboard> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("fp.org_id", orgId);
        queryWrapper.eq("DATE_FORMAT(fp.finish_time,'%Y-%m')", createDate);
        queryWrapper.groupBy("fp.salesman_id");
        queryWrapper.orderByDesc("money");
        queryWrapper.last("LIMIT 10");
        List<SalesmanPercentageLeaderboard> list = baseMapper.findSalesmanPercentageLeaderboardByOrgIdAndDate(queryWrapper);
        for (SalesmanPercentageLeaderboard salesmanPercentageLeaderboard : list) {
            salesmanPercentageLeaderboard.setStatus(Status.STATUS_ACTIVITY);
        }
        saveBatch(list, 100);
    }

    @Override
    public List<SalesmanLeaderboardVo> findSalesmanPercentageLeaderboardByOrgIdAndDate(Long orgId, String date) {
        QueryWrapper<SalesmanLeaderboardVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("spld.org_id",orgId);
        queryWrapper.eq("spld.create_date",date);
        return baseMapper.findSalesmanPercentageLeaderboardByOrgIdAndDate2(queryWrapper);
    }
}
