package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.service.member.entity.SalesmanPhones;
import com.sbdj.service.member.mapper.SalesmanPhonesMapper;
import com.sbdj.service.member.service.ISalesmanPhonesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 业务员\业务员号主\通讯录表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanPhonesServiceImpl extends ServiceImpl<SalesmanPhonesMapper, SalesmanPhones> implements ISalesmanPhonesService {

    @Override
    public IPage<SalesmanPhones> findSalesmanPhonesPag(Long srcId, String srcType, IPage<SalesmanPhones> iPage) {
        QueryWrapper<SalesmanPhones> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("src_id", srcId);
        queryWrapper.eq("src_type", srcType);
        return page(iPage, queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSalesmanPhones(SalesmanPhones salesmanPhones) {
        salesmanPhones.setCreateTime(new Date());
        save(salesmanPhones);
    }

    @Override
    public SalesmanPhones findSalesmanPhonesByImeiAndPhone(Long salesmanId, String type, String phone, String imei) {
        QueryWrapper<SalesmanPhones> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id").select("name");
        queryWrapper.eq("src_id",salesmanId).eq("src_type",type).eq("phone",phone).eq("imei",imei);
        return baseMapper.selectOne(queryWrapper);
    }
}
