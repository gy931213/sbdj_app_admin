package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.service.member.entity.SalesmanNumberPhonesTmp;
import com.sbdj.service.member.mapper.SalesmanNumberPhonesTmpMapper;
import com.sbdj.service.member.service.ISalesmanPhonesTmpService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 临时号主\通讯录表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanPhonesTmpServiceImpl extends ServiceImpl<SalesmanNumberPhonesTmpMapper, SalesmanNumberPhonesTmp> implements ISalesmanPhonesTmpService {

    @Override
    public IPage<SalesmanNumberPhonesTmp> findSalesmanNumberPhonesTmpPage(Long targetId) {
        QueryWrapper<SalesmanNumberPhonesTmp> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("target_id", targetId);
        IPage<SalesmanNumberPhonesTmp> salesmanNumberPhonesTmpIPage = new Page<>();
        return page(salesmanNumberPhonesTmpIPage, queryWrapper);
    }
}