package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.admin.vo.SalesmanRoyaltyVo;
import com.sbdj.service.member.entity.SalesmanRoyalty;
import com.sbdj.service.member.mapper.SalesmanRoyaltyMapper;
import com.sbdj.service.member.service.ISalesmanRoyaltyService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 业务员额度增长配置表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanRoyaltyServiceImpl extends ServiceImpl<SalesmanRoyaltyMapper, SalesmanRoyalty> implements ISalesmanRoyaltyService {
    @Override
    public IPage<SalesmanRoyaltyVo> findSalesmanRoyaltyPageList(Long orgId, IPage<SalesmanRoyaltyVo> reqPage) {
        QueryWrapper<SalesmanRoyalty> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sr.status",Status.STATUS_ACTIVITY);
        queryWrapper.eq("so.status",Status.STATUS_ACTIVITY);
        // ~机构
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId),"sr.org_id",orgId );
        return baseMapper.findSalesmanRoyaltyPageList(reqPage,queryWrapper);
    }

    @Cacheable(cacheNames = "SalesmanRoyalty", sync = true, key = "#orgId")
    @Override
    public SalesmanRoyalty findSalesmanRoyaltyByOrgId(Long orgId) {
        QueryWrapper<SalesmanRoyalty> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",Status.STATUS_ACTIVITY);
        queryWrapper.eq("org_id",orgId);
        queryWrapper.orderByDesc("create_time");
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames="SalesmanRoyalty", allEntries=true)
    @Override
    public void saveSalesmanRoyalty(SalesmanRoyalty royalty) {
        royalty.setStatus(Status.STATUS_ACTIVITY);
        royalty.setCreateTime(new Date());
        save(royalty);
    }

    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames="SalesmanRoyalty", allEntries=true)
    @Override
    public void updateSalesmanRoyalty(SalesmanRoyalty royalty) {
        if (StrUtil.isNull(royalty) || StrUtil.isNull(royalty.getId())) {
            throw BaseException.base("更新业务员额度失败,id为空");
        }
        updateById(royalty);
    }

    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames="SalesmanRoyalty", allEntries=true)
    @Override
    public void logicDeleteSalesmanRoyalty(Long id) {
        SalesmanRoyalty salesmanRoyalty = baseMapper.selectById(id);
        salesmanRoyalty.setStatus(Status.STATUS_DELETE);
        updateById(salesmanRoyalty);
    }

}
