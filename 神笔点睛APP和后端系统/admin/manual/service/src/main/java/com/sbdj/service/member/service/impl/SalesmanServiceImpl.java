package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.PlatformTypeValue;
import com.sbdj.core.constant.Status;
import com.sbdj.core.constant.TaskTypeCode;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.CopyObjectUtil;
import com.sbdj.core.util.DateUtil;
import com.sbdj.core.util.MD5Util;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.finance.entity.FinanceDrawMoneyRecord;
import com.sbdj.service.finance.entity.FinancePercentage;
import com.sbdj.service.finance.entity.FinanceSalesmanBill;
import com.sbdj.service.finance.entity.SalesmanCreditRecord;
import com.sbdj.service.finance.scheduler.vo.StatementPercentageVo;
import com.sbdj.service.finance.scheduler.vo.StatementTaskCreditVo;
import com.sbdj.service.finance.scheduler.vo.StatementTaskSonVo;
import com.sbdj.service.finance.service.*;
import com.sbdj.service.member.admin.query.TBInfo;
import com.sbdj.service.member.admin.query.TBQuery;
import com.sbdj.service.member.admin.vo.SalesmanCreditRecordVo;
import com.sbdj.service.member.admin.vo.SalesmanListVo;
import com.sbdj.service.member.admin.vo.SalesmanRespVo;
import com.sbdj.service.member.admin.vo.SalesmanVo;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanLevel;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.member.entity.SalesmanRoyalty;
import com.sbdj.service.member.mapper.SalesmanMapper;
import com.sbdj.service.member.service.ISalesmanLevelService;
import com.sbdj.service.member.service.ISalesmanNumberService;
import com.sbdj.service.member.service.ISalesmanRoyaltyService;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.member.type.SalesmanBillTypeMsg;
import com.sbdj.service.system.service.ISysNumberService;
import com.sbdj.service.system.type.SysNumberType;
import com.sbdj.service.task.entity.TaskSetting;
import com.sbdj.service.task.scheduler.vo.TaskSonSalesmanVo;
import com.sbdj.service.task.service.ITaskSettingService;
import com.sbdj.service.task.service.ITaskSonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 业务员表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanServiceImpl extends ServiceImpl<SalesmanMapper, Salesman> implements ISalesmanService {

    private Logger logger = LoggerFactory.getLogger(SalesmanServiceImpl.class);

    @Autowired
    private ISalesmanNumberService iSalesmanNumberService;
    @Autowired
    private IFinanceStatementService iFinanceStatementService;
    @Autowired
    private ISysNumberService iNumberService;
    @Autowired
    private ISalesmanRoyaltyService iSalesmanRoyaltyService;
    @Autowired
    private ISalesmanCreditRecordService iSalesmanCreditRecordService;
    @Autowired
    @Lazy
    private ISalesmanService iSalesmanService;
    @Autowired
    private ITaskSettingService iTaskSettingService;
    @Autowired
    private ITaskSonService iTaskSonService;
    @Autowired
    private ISalesmanLevelService iSalesmanLevelService;
    @Autowired
    private IFinanceSalesmanBillService iFinanceSalesmanBillService;
    @Autowired
    private IFinancePercentageService iFinancePercentageService;
    @Lazy
    @Autowired
    private IFinanceDrawMoneyRecordService iDrawMoneyRecordService;
    @Autowired
    private IFinanceSalesmanBillService iSalesmanBillService;

    @Override
    public IPage<TBInfo> findSalesmanPageList(IPage<SalesmanVo> pageParam, TBQuery tbparams) {
        // 设置状态
        boolean flag = true;
        QueryWrapper<TBQuery> queryWrapper = new QueryWrapper<>();
        //拼接SQL
        encapsulationQueryWrapperSQL(tbparams, flag, queryWrapper);
        return baseMapper.selectSalesmanPage(pageParam, queryWrapper);
    }

    /**
     * 封装分页查询条件构造器拼接SQL语句
     *
     * @param tbparams     条件
     * @param flag         是否拼接部分SQL
     * @param queryWrapper 条件构造器
     * @return com.baomidou.mybatisplus.core.conditions.query.QueryWrapper<com.sbdj.service.member.admin.query.TBQuery>
     * @author ZC
     * @date 2019-11-27 10:56
     */
    private void encapsulationQueryWrapperSQL(TBQuery tbparams, boolean flag, QueryWrapper<TBQuery> queryWrapper) {
        /*若查询账户条件为1则显示(默认不显示)*/
        if (StrUtil.isNotNull(tbparams.getMobile()) && "1".equals(tbparams.getMobile())) {
            flag = false;
        }
        /*若查询条件为神笔点睛则显示(默认不显示)*/
        queryWrapper.eq(StrUtil.isNotNull(tbparams.getName()), "sm.name", tbparams.getName());
        if (StrUtil.isNotNull(tbparams.getName()) && "神笔点睛".equals(tbparams.getName())) {
            flag = false;
        }
        queryWrapper.notIn(flag, "sm.id", 1);
        queryWrapper.eq(StrUtil.isNotNull(tbparams.getMobile()), "sm.mobile", tbparams.getMobile());
        queryWrapper.eq(StrUtil.isNotNull(tbparams.getSalesmanName()), "sm1.name", tbparams.getSalesmanName());
        // 平台
        queryWrapper.eq(StrUtil.isNotNull(tbparams.getPlatformId()) && !Number.LONG_ZERO.equals(tbparams.getPlatformId()), "sn.platform_id", tbparams.getPlatformId());
        // 工号
        queryWrapper.eq(StrUtil.isNotNull(tbparams.getJobNumber()), "sm.job_number", tbparams.getJobNumber());
        // 业务状态
        queryWrapper.eq(StrUtil.isNotNull(tbparams.getStatus()), "sm.status", tbparams.getStatus());
        // 操作人
        queryWrapper.eq(StrUtil.isNotNull(tbparams.getOprName()), "sa.`name`", tbparams.getOprName());
        // 上级业务员手机
        queryWrapper.eq(StrUtil.isNotNull(tbparams.getSalesmanOnlineMobile()), "sm1.mobile", tbparams.getSalesmanOnlineMobile());
        // 开始时间
        queryWrapper.ge(StrUtil.isNotNull(tbparams.getBeginTime()), "sm.create_time", tbparams.getBeginTime());
        // 结束时间
        queryWrapper.le(StrUtil.isNotNull(tbparams.getEndTime()), "sm.create_time", tbparams.getEndTime());
        // 号主状态
        queryWrapper.eq(StrUtil.isNotNull(tbparams.getpStatus()), "sn.status", tbparams.getpStatus());
        // 关键词
        queryWrapper.and(StrUtil.isNotNull(tbparams.getKeyword()), f -> f.like("sm.mobile", tbparams.getKeyword())
                .or().like(StrUtil.isNotNull(tbparams.getKeyword()), "sm.name", tbparams.getKeyword())
                .or().like(StrUtil.isNotNull(tbparams.getKeyword()), "sm1.name", tbparams.getKeyword()));
    }

    @Override
    public List<SalesmanVo> findOnlineRelation(Long salesmanId) {
        List<SalesmanVo> list = new ArrayList<>();
        SalesmanVo salesman;
        salesman = baseMapper.findOnlineSalesmanVo(salesmanId);
        if (StrUtil.isNull(salesman)) {
            throw BaseException.base("没有上级信息");
        }
        //判断 若是系统号则不添加
        if (!salesman.getId().equals(1L)) {
            list.add(salesman);
        }
        //循环查询上级信息
        while (true) {
            salesman = baseMapper.findOnlineSalesmanVo(salesman.getSalesmanId());
            //判断是不是系统号
            if (StrUtil.isNull(salesman)) {
                break;
            }
            //判断上级是不是系统号
            if (salesman.getId().equals(1L)) {
                break;
            }
            //不是则添加 -- 继续循环
            list.add(salesman);
        }
        return list;
    }

    @Override
    public boolean tbVipExist(TBInfo tbInfo) {
        if (StrUtil.isNotNull(tbInfo.getOnlineid()) && StrUtil.isNotNull(tbInfo.getPlatformId())) {
            QueryWrapper<SalesmanNumber> nqueryWrapper = new QueryWrapper<>();
            //1平台
            nqueryWrapper.eq("platform_id", PlatformTypeValue.PLATFORM_TYPE_TB.getPlatformId());
            //2账户
            nqueryWrapper.eq("onlineid", tbInfo.getOnlineid());
            return StrUtil.isNotNull(iSalesmanNumberService.getOne(nqueryWrapper));
        }
        return false;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTBInfoStatusById(Long id, String status) {
        // 跳过系统号
        if (!id.equals(1L)) {
            Salesman salesman = baseMapper.selectById(id);
            salesman.setStatus(status);
            // 禁用
            updateById(salesman);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTBInfoStatusById(Long id, Long oprId, String startStatus, String endStatus) {
        // 跳过系统号
        if (!id.equals(1L)) {
            if (Status.STATUS_UNREVIEWED.equals(startStatus)) {
                UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
                updateWrapper.set("status",endStatus).set("opr_id",oprId).set("update_time",new Date());
                updateWrapper.eq("id",id);
                update(updateWrapper);
            }
        }
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanSearch(Long id, Integer isSearch) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新业务员是否安装查小号失败,id为空");
        }
        UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("is_search", isSearch);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesman(Salesman salesman) {
        if (StrUtil.isNull(salesman) || StrUtil.isNull(salesman.getId())) {
            throw BaseException.base("更新业务员失败,id为空");
        }
        updateById(salesman);
    }


    @Override
    public Salesman findSalesmanByOrgIdAndMobile(long orgId, String salesmanMobile) {
        QueryWrapper<Salesman> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("org_id", orgId);
        queryWrapper.eq("mobile", salesmanMobile);
        return baseMapper.selectOne(queryWrapper);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTBInfo(TBInfo tbInfo) {
        if (StrUtil.isNull(tbInfo)) {
            throw BaseException.base("信息不能为空");
        }
        boolean tbFlag = StrUtil.isNotNull(tbInfo.getOnlineid());
        Salesman salesman = new Salesman();
        //工具类将 复制 源对象(param1)的 相同属性 值 赋值给目标对象(param2)的属性
        CopyObjectUtil.copyProperties(tbInfo, salesman);
        //封装默认赋值
        defaultSalesman(tbInfo, salesman);
        //添加数据
        save(salesman);

        //若淘宝ID不为空 则向号主表也添加数据
        if (tbFlag) {
            SalesmanNumber salesmanNumber = new SalesmanNumber();
            CopyObjectUtil.copyProperties(tbInfo, salesmanNumber);
            //封装默认赋值
            defaultSalesmanNumber(tbInfo, salesman, salesmanNumber);
            iSalesmanNumberService.save(salesmanNumber);
        }
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTBInfo(TBInfo tbInfo) {
        Salesman salesman = baseMapper.selectById(tbInfo.getId());
        if (StrUtil.isNull(salesman)) {
            throw BaseException.base("信息为空");
        }
        //获取是否有填写号主账号
        boolean tbFlag = StrUtil.isNotNull(tbInfo.getOnlineid());
        //以防万一
        tbInfo.setId(null);
        CopyObjectUtil.copyProperties(tbInfo, salesman);
        updateById(salesman);

        if (tbFlag) {
            SalesmanNumber salesmanNumber = iSalesmanNumberService.findSalesmanNumberBySalesmanIdAndPlatformId(salesman.getId(), tbInfo.getPlatformId());
            if (StrUtil.isNull(salesmanNumber)) {
                salesmanNumber = new SalesmanNumber();
                CopyObjectUtil.copyProperties(tbInfo, salesmanNumber);
                salesmanNumber.setSalesmanId(salesman.getId());
                salesmanNumber.setStatus(Status.STATUS_ENABLE);
                salesmanNumber.setPlatformId(PlatformTypeValue.PLATFORM_TYPE_TB.getPlatformId());
                salesmanNumber.setCreateTime(new Date());
                iSalesmanNumberService.save(salesmanNumber);
            } else {
                CopyObjectUtil.copyProperties(tbInfo, salesmanNumber);
                salesmanNumber.setOnlineid(tbInfo.getOnlineid());
                salesmanNumber.setSalesmanId(salesman.getId());
                iSalesmanNumberService.updateById(salesmanNumber);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void disableSalesmanByIdsAndDays(String ids, Integer days) {
        Date date = DateUtil.genDayDate(days, new Date());
        List<Long> list = StrUtil.getIdsByLong(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("禁用业务与号主失败,ids为空");
        }
        Salesman salesman;
        for (Long id : list) {
            salesman = iSalesmanService.getById(id);
            if (days > 0) {
                // 禁用业务推荐一段时间
                UpdateWrapper<Salesman> salesmanWrapper = new UpdateWrapper<>();
                salesmanWrapper.set("status", Status.STATUS_PROHIBIT);
                salesmanWrapper.set("disable_time", date);
                salesmanWrapper.eq("id", id);
                update(salesmanWrapper);
                if (!salesman.getSalesmanId().equals(1L)) {
                    UpdateWrapper<Salesman> onlineSalesmanWrapper = new UpdateWrapper<>();
                    salesmanWrapper.set("status", Status.STATUS_PROHIBIT);
                    salesmanWrapper.set("disable_time", date);
                    salesmanWrapper.eq("id", salesman.getSalesmanId());
                    update(salesmanWrapper);
                }
            } else {
                UpdateWrapper<Salesman> salesmanWrapper = new UpdateWrapper<>();
                salesmanWrapper.set("status", Status.STATUS_PROHIBIT);
                salesmanWrapper.eq("id", id);
                update(salesmanWrapper);
                if (!salesman.getSalesmanId().equals(1L)) {
                    UpdateWrapper<Salesman> onlineSalesmanWrapper = new UpdateWrapper<>();
                    salesmanWrapper.set("status", Status.STATUS_PROHIBIT);
                    salesmanWrapper.eq("id", salesman.getSalesmanId());
                    update(salesmanWrapper);
                }
            }
            // 永久禁用号主
            iSalesmanNumberService.updateSalesmanNumberStatusBySalesmanId(id, Status.STATUS_PROHIBIT);
        }
    }

    @Override
    public Salesman findSalesmanByMobile(String mobile) {
        QueryWrapper<Salesman> salesmanQueryWrapper = new QueryWrapper<>();
        salesmanQueryWrapper.eq("mobile", mobile);
        return baseMapper.selectOne(salesmanQueryWrapper);
    }


    /**
     * 封装业务员号主默认赋值
     *
     * @param tbInfo         淘宝刷手条件
     * @param salesman       业务员
     * @param salesmanNumber 业务员号主
     * @return void
     * @author ZC
     * @date 2019-11-26 10:23
     */
    private void defaultSalesmanNumber(TBInfo tbInfo, Salesman salesman, SalesmanNumber salesmanNumber) {
        salesmanNumber.setSalesmanId(salesman.getId());
        salesmanNumber.setPlatformId(PlatformTypeValue.PLATFORM_TYPE_TB.getPlatformId());
        salesmanNumber.setStatus(Status.STATUS_ENABLE);
        salesmanNumber.setCreateTime(new Date());
        if (StrUtil.isNotNull(tbInfo)) {
            salesmanNumber.setOprId(tbInfo.getOprId());
        }
    }

    /**
     * 封装默认赋值
     *
     * @param tbInfo   条件数据
     * @param salesman 业务员数据
     * @return void
     * @author ZC
     * @date 2019-11-26 10:23
     */
    private void defaultSalesman(TBInfo tbInfo, Salesman salesman) {
        //设置职位编码
        salesman.setJobNumber(iNumberService.findSysNumberJOBByNumberCoe(SysNumberType.CODE_JOB));
        // 默认神笔点睛
        salesman.setOrgId(2L);
        // 密码默认123456
        salesman.setPwd("123456");
        salesman.setKeyword(StrUtil.getRandomString(18));
        salesman.setCreateTime(new Date());
        // 默认启用
        salesman.setStatus(Status.STATUS_ENABLE);
        salesman.setTotalCredit(new BigDecimal(99999999));
        salesman.setSurplusCredit(new BigDecimal(99999999));
        salesman.setBalance(new BigDecimal(0.00));
        salesman.setLoginTotal(Number.INT_ZERO);
        // 默认手工单级别
        salesman.setSalesmanLevelId(1L);
        salesman.setLastLoginTime(new Date());
        salesman.setRequestCount(10); // 默认为10次
        salesman.setTaskTotal(0); // 默认为0
        if (StrUtil.isNotNull(tbInfo)) {
            // 设置操作人
            salesman.setOprId(tbInfo.getOprId());
        }
        if (!"".equals(salesman.getKeyword()) && !"".equals(salesman.getPwd())) {
            // ~密码进行加密处理MD5
            String pwd = MD5Util.str2MD5(salesman.getPwd() + salesman.getKeyword());
            salesman.setPwd(pwd);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanFirstMission(Long salesmanId) {
        Salesman salesman = getById(salesmanId);
        if (salesman.getFirstMission() == 0) {
            UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("first_mission", 1);
            updateWrapper.eq("id", salesmanId);
            update(updateWrapper);
        }
    }



    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanTaskTotal(Long salesmanId, int total) {
        baseMapper.updateSalesmanTaskTotal(salesmanId, total);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanCreditById(Long id, BigDecimal credit, boolean istf) {
        // 获取数据
        Salesman salesman = getById(id);
        if (StrUtil.isNotNull(salesman)) {
            // istf true:减信用度，false:加信用度
            if (istf) {
                salesman.setSurplusCredit(salesman.getSurplusCredit().subtract(credit));
            } else {
                // 进行可用额度控制!
                // 业务员当前的可用额度加上任务返回的额度相加
                // 得出的结果不能大于，当前业务员封顶额度
                // 如果得出的结果大于封顶额度，则将封顶额度值，赋给可用额度!
                BigDecimal sum = salesman.getSurplusCredit().add(credit);
                // -1：小于/0：等于/1：大于
                if (sum.compareTo(salesman.getTotalCredit()) < 1) {
                    salesman.setSurplusCredit(salesman.getSurplusCredit().add(credit));
                } else {
                    salesman.setSurplusCredit(salesman.getTotalCredit());
                }
            }
            updateById(salesman);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void autoEnableSalesmanByDisableTime() {
        QueryWrapper<Salesman> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_PROHIBIT);
        queryWrapper.lt("disable_time", new Date());
        queryWrapper.select("id");
        List<Salesman> list = list(queryWrapper);
        List<Long> ids = new ArrayList<>();
        for (Salesman s : list) {
            ids.add(s.getId());
        }
        UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_ENABLE);
        updateWrapper.set("disable_time", null);
        updateWrapper.in("id", ids);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTBInfoTmp(TBInfo tbInfo) {
        if (StrUtil.isNull(tbInfo)) {
            throw new RuntimeException("信息不能为空");
        }
        boolean tbFlag = StrUtil.isNotNull(tbInfo.getOnlineid());
        Salesman salesman = new Salesman();
        CopyObjectUtil.copyProperties(tbInfo, salesman);
        defaultSalesman(tbInfo, salesman);
        salesman.setStatus(Status.STATUS_UNREVIEWED);
        save(salesman);

        if (tbFlag) {
            SalesmanNumber salesmanNumber = new SalesmanNumber();
            CopyObjectUtil.copyProperties(tbInfo, salesmanNumber);
            defaultSalesmanNumber(tbInfo, salesman, salesmanNumber);
            salesmanNumber.setStatus(Status.STATUS_PROHIBIT);
            iSalesmanNumberService.save(salesmanNumber);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanLastLoginTimeAndLoginTotal(Long id) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新业务员登录时间，次数错误,id为空");
        }
        baseMapper.updateSalesmanLastLoginTimeAndLoginTotal(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanTotalCreditsAndSurplusCredit() {
        logger.info(">>>>>>>>【任务调度】执行更新业务员的信用额度，根据所做的任务对应提额，接口：【updateSalesmanTotalCreditsAndSurplusCredit】>>> start");
        // ~获取要计算的任务数据
        // ~根据机构获取信用额度配置
        // ~根据任务中的业务员id获取业务员信息
        // ~根据业务员所属机构和业务员的等级获取信用额度最高
        SalesmanRoyalty royalty = null;
        // 业务员信用额度
        SalesmanCreditRecordVo salesmanCreditRecordVo;
        Map<Long, List<SalesmanCreditRecordVo>> data = new HashMap<>();
        List<SalesmanCreditRecordVo> salesmanCreditRecordVoList;
        // ~获取前一天任务数据
        List<StatementTaskCreditVo> list = iFinanceStatementService.findStatementTaskSon();
        for (StatementTaskCreditVo stcv : list) {
            // ~业务员任务类型信用额度配置
            if (null == royalty) {
                royalty = iSalesmanRoyaltyService.findSalesmanRoyaltyByOrgId(stcv.getOrgId());
            } else if (!royalty.getOrgId().equals(stcv.getOrgId())) {
                royalty = iSalesmanRoyaltyService.findSalesmanRoyaltyByOrgId(stcv.getOrgId());
            }
            // ~保存更新业务员的信用额度数据
            salesmanCreditRecordVo = new SalesmanCreditRecordVo();
            salesmanCreditRecordVo.setOrgId(stcv.getOrgId());
            salesmanCreditRecordVo.setSalesmanId(stcv.getSalesmanId());
            salesmanCreditRecordVo.setReason(stcv.getTaskSonNumber());
            salesmanCreditRecordVo.setTaskSonId(stcv.getId());

            // ~判断是什么任务类型
            switch (stcv.getTaskTypeCode()) {
                case TaskTypeCode.TASK_TYPE_XFD:
                    salesmanCreditRecordVo.setIncrement(royalty.getPayment());
                    break;
                case TaskTypeCode.TASK_TYPE_LLD:
                    salesmanCreditRecordVo.setIncrement(royalty.getBrowse());
                    break;
                case TaskTypeCode.TASK_TYPE_GRD:
                    salesmanCreditRecordVo.setIncrement(royalty.getNextDay());
                    break;
            }
            // 记录信用额度
            if (stcv.getStatus().equals(Number.INT_ONE)) {
                salesmanCreditRecordVo.setCredit(salesmanCreditRecordVo.getIncrement());
            } else {
                salesmanCreditRecordVo.setCredit(salesmanCreditRecordVo.getIncrement().negate());
            }

            salesmanCreditRecordVoList = new ArrayList<>();
            // 判断键是否存在
            if (data.containsKey(stcv.getSalesmanId())) {
                salesmanCreditRecordVoList = data.get(stcv.getSalesmanId());
                salesmanCreditRecordVoList.add(salesmanCreditRecordVo);
                data.put(stcv.getSalesmanId(), salesmanCreditRecordVoList);
            } else {
                // 添加数据
                salesmanCreditRecordVoList.add(salesmanCreditRecordVo);
                data.put(stcv.getSalesmanId(), salesmanCreditRecordVoList);
            }
        }
        List<SalesmanCreditRecord> scrList;
        SalesmanCreditRecord salesmanCreditRecord;
        // 修改业务员的数据 生成业务员的信用额度流水
        for (Map.Entry<Long, List<SalesmanCreditRecordVo>> entry : data.entrySet()) {
            try {
                List<SalesmanCreditRecordVo> scrvList = entry.getValue();
                scrList = new ArrayList<>();
                for (SalesmanCreditRecordVo creditRecordVo : scrvList) {
                    iSalesmanService.updateSalesmanTotalCreditAndSurplusCredit(entry.getKey(), creditRecordVo.getCredit());
                    salesmanCreditRecord = new SalesmanCreditRecord();
                    salesmanCreditRecord.setOrgId(creditRecordVo.getOrgId());
                    salesmanCreditRecord.setSalesmanId(creditRecordVo.getSalesmanId());
                    salesmanCreditRecord.setTaskSonId(creditRecordVo.getTaskSonId());
                    salesmanCreditRecord.setIncrement(creditRecordVo.getIncrement());
                    salesmanCreditRecord.setReason(creditRecordVo.getReason());
                    salesmanCreditRecord.setCreateTime(new Date());
                    scrList.add(salesmanCreditRecord);
                }
                iSalesmanCreditRecordService.saveBatch(scrList, 100);
            } catch (Exception e) {
                logger.error("业务员信用额度流水部分记录失败 --> 截止于业务员id:" + entry.getKey());
            }
        }
        logger.info(">>>>>>>>【任务调度】执行更新业务员的信用额度，根据所做的任务对应提额，接口：【updateSalesmanTotalCreditsAndSurplusCredit】>>> end");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanTotalCreditAndSurplusCredit(Long id, BigDecimal credit) {
        Salesman salesman = getById(id);
        if (null == salesman) {
            logger.error("业务员查询为空");
            return;
        }

        // ~根据业务员的等级和所属机构获取最高信用额度配置数据
        TaskSetting setting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(salesman.getOrgId(), salesman.getSalesmanLevelId());
        if (null == setting || null == setting.getCredit()) {
            logger.error("根据业务员的等级和所属机构获取最高信用额度配置数据，获取到的数据为空!!!!!");
            return;
        }

        // 额度增减
        // ~判断当前累积的信用额度是否超过等级的最大额度限制!
        // 业务员根据等级 更新总信用值
        salesman.setTotalCredit(setting.getCredit());
        // ~返回值    -1 小于   0 等于    1 大于
        if (credit.compareTo(salesman.getTotalCredit()) >= 0) {
            // 信用额度已经最大
            salesman.setSurplusCredit(setting.getCredit());
            logger.info("信用额度已满");
        } else if (credit.compareTo(BigDecimal.ZERO) < 0) {
            // 最低额度阈值
            setting = iTaskSettingService.findTaskSettingMinByOrgId(salesman.getOrgId());
            if (salesman.getSurplusCredit().compareTo(setting.getCredit()) <= 0) {
                // 信用额度已经最小
                logger.info("可用信用额度已最低");
            } else {
                if (null != setting.getCredit() && salesman.getTotalCredit().compareTo(salesman.getSurplusCredit()) <= 0) {
                    salesman.setSurplusCredit(salesman.getTotalCredit());
                }
                // 减去信用额度
                salesman.setSurplusCredit(salesman.getSurplusCredit().add(credit));
                if (salesman.getTotalCredit().compareTo(setting.getCredit()) <= 0 || salesman.getSurplusCredit().compareTo(setting.getCredit()) <= 0) {
                    salesman.setSurplusCredit(setting.getCredit());
                } else {
                    setting = iTaskSettingService.findTaskSettingByOrgIdAndCredit(salesman.getOrgId(), salesman.getSurplusCredit());
                }
            }
        } else {
            // 增加信用额度 只能通过人工审核才能提高等级
            salesman.setSurplusCredit(salesman.getSurplusCredit().add(credit));
            if (null != setting.getCredit() && setting.getCredit().compareTo(salesman.getTotalCredit()) != 0) {
                salesman.setTotalCredit(setting.getCredit());
                salesman.setSurplusCredit(setting.getCredit());
            }
            // 额度已满
            if (salesman.getSurplusCredit().compareTo(salesman.getTotalCredit()) > 0) {
                salesman.setSurplusCredit(salesman.getTotalCredit());
            }
        }
        try {
            // 修改业务员可用余额和等级
            UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("surplus_credit", salesman.getSurplusCredit());
            updateWrapper.set("total_credit", setting.getCredit());
            updateWrapper.set("salesman_level_id", setting.getSalesmanLevelId());
            updateWrapper.eq("id", salesman.getId());
            update(updateWrapper);
        } catch (Exception e) {
            logger.error("修改业务员可用余额和等级失败");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void reduceSalesmanCredit(Integer day) {
        // day 天任务数据为0 为禁用状态
        //查询每个业务员day天内是否有T的子任务单
        List<TaskSonSalesmanVo> list = iTaskSonService.checkSalesmanIsInactive(day);
        for (TaskSonSalesmanVo tssv : list) {
            //没有做单则总额度与剩余额度相等
            if (null != tssv && null != tssv.getSalesmanId()) {
                Salesman salesmanOne = getById(tssv.getSalesmanId());
                //额度配置表
                SalesmanRoyalty sr = iSalesmanRoyaltyService.findSalesmanRoyaltyByOrgId(salesmanOne.getOrgId());
                if (null == sr) {
                    logger.info("获取到业务员的额度配置数据为空，数据为空的原因：没有所属机构【{}】配置数据", salesmanOne.getOrgId());
                    continue;
                }
                BigDecimal all = sr.getBrowse().add(sr.getNextDay().add(sr.getPayment()));
                //获取业务员最低等级
                SalesmanLevel salesmanLowestLevel = iSalesmanLevelService.findSalesmanLowestLevel();
                //获取比当前业务员低一级的等级数据
                SalesmanLevel salesmanLowerLevel = iSalesmanLevelService.findSalesmanLowerLevel(salesmanOne.getSalesmanLevelId());
                if (null == salesmanLowerLevel) {
                    logger.info("获取业务员等级数据数据为空，数据为空的原因：所属业务员第【{}】等级的配置", salesmanOne.getSalesmanLevelId());
                    continue;
                }
                //减业务员额度
                salesmanOne.setTotalCredit(salesmanOne.getTotalCredit().subtract(all));
                salesmanOne.setSurplusCredit(salesmanOne.getSurplusCredit().subtract(all));
                salesmanOne.setUpdateTime(new Date());
                //获取等级配置
                TaskSetting setting;
                if (salesmanOne.getSalesmanLevelId().equals(salesmanLowestLevel.getId())) {
                    if (salesmanOne.getTotalCredit().compareTo(new BigDecimal(1000)) < 1) {
                        //~返回值   -1 小于     0 等于    1 大于
                        salesmanOne.setTotalCredit(new BigDecimal(1000));
                        salesmanOne.setSurplusCredit(new BigDecimal(1000));
                    }
                } else {
                    setting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(salesmanOne.getOrgId(), salesmanLowerLevel.getId());
                    if (salesmanOne.getTotalCredit().compareTo(setting.getCredit()) < 1) {
                        salesmanOne.setSalesmanLevelId(salesmanLowerLevel.getId());
                    }
                }
                updateById(salesmanOne);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void computeTaskBrokerageToSalesmanBalance() {
        //查询每个业务员的佣金
        logger.info(">>>>>>>>【任务调度】执行更新业务员的余额(佣金)，接口：【computeTaskBrokerageToSalesmnBalance】>>> start");
        // 声明业务员佣金中间流水
        Map<Long, FinanceSalesmanBill> salesmanBillMap = new HashMap<>();
        // 声明账单目标ID
        Map<Long, String> targetMap = new HashMap<>();
        // 业务员账单
        FinanceSalesmanBill salesmanBill;
        // 业务员账单Key
        FinanceSalesmanBill salesmanBillKey;
        // 业务员ID
        Long salesmanId;
        // 计算隔天佣金
        Date date = DateUtil.genDayDate(-1, new Date());
        // 查询隔天业务员佣金记录
        List<StatementTaskSonVo> brokerageList = iTaskSonService.findSalesmanBrokerage();
        for (StatementTaskSonVo entity : brokerageList) {
            Salesman sm = getById(entity.getSalesmanId());
            if (StrUtil.isNotNull(sm)) {
                BigDecimal total = sm.getBalance().add(entity.getMoney());

                // 处理业务员佣金中间流水
                // --------------------------------------><--------------------------------------------------
                salesmanBill = new FinanceSalesmanBill();
                salesmanBill.setSalesmanId(sm.getId());
                salesmanBill.setMoney(entity.getMoney());
                salesmanBill.setLastMoney(sm.getBalance());
                salesmanBill.setFinalMoney(total);
                salesmanBill.setOrgId(sm.getOrgId());
                salesmanBill.setBillType(SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getCodeType());
                salesmanBill.setAccount(SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getMessage());
                salesmanBill.setBillDate(date);
                salesmanBill.setCreateTime(date);

                salesmanId = entity.getSalesmanId();
                salesmanBillKey = salesmanBillMap.get(salesmanId);

                if (StrUtil.isNotNull(salesmanBillKey) && salesmanId.equals(salesmanBillKey.getSalesmanId())) {
                    salesmanBillKey.setMoney(salesmanBillKey.getMoney().add(entity.getMoney()));
                    salesmanBillKey.setFinalMoney(salesmanBillKey.getFinalMoney().add(entity.getMoney()));
                    // 添加目标ID
                    String value = targetMap.get(salesmanId);
                    value += "," + entity.getId().toString();
                    targetMap.put(salesmanId, value);
                } else {
                    salesmanBillMap.put(salesmanId, salesmanBill);
                    targetMap.put(salesmanId, entity.getId().toString());
                }
                // -------------------------------------><-----------------------------------------------------
            }
        }
        UpdateWrapper<Salesman> updateWrapper;
        // 遍历生成数据
        for (Map.Entry<Long, FinanceSalesmanBill> en : salesmanBillMap.entrySet()) {
            FinanceSalesmanBill entity = en.getValue();
            if (StrUtil.isNotNull(entity)) {
                // 默认 false 当遇到异常时停止那条数据操作,跳至下一条
                boolean active = false;
                updateWrapper = new UpdateWrapper<>();
                try {
                    updateWrapper.set("balance", entity.getFinalMoney());
                    updateWrapper.eq("id", entity.getSalesmanId());
                    update(updateWrapper);
                } catch (Exception e) {
                    active = true;
                    logger.error("更新业务员余额失败 业务员id:{} 之前余额:{} 金额:{} 之后余额:{} 错误信息:{}",
                            entity.getSalesmanId(), entity.getLastMoney(), entity.getMoney(), entity.getFinalMoney(), e.getMessage());
                }
                // 当业务员余额更新失败时,停止以下账单记录,跳往下一条数据处理
                if (active) {
                    continue;
                }
                // 保存账单目标ID
                entity.setTargetId(targetMap.get(en.getKey()));
                try {
                    iFinanceSalesmanBillService.save(entity);
                } catch (Exception e) {
                    logger.error("业务员佣金中间流水保存失败 错误信息:{} ", e.getMessage());
                }
            }
        }

        logger.info(">>>>>>>>【任务调度】执行更新业务员的余额(佣金)，接口：【computeTaskBrokerageToSalesmnBalance】>>> end...同步【{}】条数据！", brokerageList.size());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void computeTaskPercentageToSalesmanBalance() {
        logger.info(">>>>>>>>【任务调度】执行更新业务员的余额(提成)，接口：【computeTaskPercentageToSalesmanBalance】>>> start");
        //查询每个业务员提成
        List<StatementPercentageVo> statementPercentageVoList = iTaskSonService.findSalesmanPercentage();

        if (StrUtil.isNull(statementPercentageVoList) || 0 == statementPercentageVoList.size()) {
            logger.error("所处理的数据为空或已处理过");
            return;
        }

        // 隔天日期
        Date date = DateUtil.genDayDate(-1, new Date());

        iSalesmanService.saveSalesmanPercentage(statementPercentageVoList, date);

        logger.info(">>>>>>>>【任务调度】执行更新业务员的余额(提成)，接口：【computeTaskPercentageToSalesmanBalance】>>> end");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void computeTaskPercentageToSalesmanBalance(String date, String jobNumber) {
        logger.info(">>>>>>>>【任务调度】执行更新业务员的余额(提成)，接口：【computeTaskPercentageToSalesmanBalance】>>> start");
        //查询每个业务员提成
        List<StatementPercentageVo> statementPercentageVoList = iTaskSonService.findSalesmanPercentageByDateAndJobNumber(date, jobNumber);
        if (StrUtil.isNull(statementPercentageVoList) || 0 == statementPercentageVoList.size()) {
            logger.error("所处理的数据为空或已处理过");
            return;
        }
        // 日期转化
        Date createDate;
        try {
            createDate = new SimpleDateFormat(DateUtil.formatter_yyyyMMdd).parse(date);
        } catch (Exception e) {
            throw BaseException.base(e.getMessage());
        }

        iSalesmanService.saveSalesmanPercentage(statementPercentageVoList, createDate);
        logger.info(">>>>>>>>【任务调度】执行更新业务员的余额(提成)，接口：【computeTaskPercentageToSalesmanBalance】>>> end");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSalesmanPercentage(List<StatementPercentageVo> statementPercentageVoList, Date date) {
        //根据不同机构获取不同提成值
        SalesmanRoyalty royalty = null;
        // 业务员提成 相关
        Map<Long, FinancePercentage> percentageMap = new HashMap<>();
        // 声明账单目标ID
        Map<Long, String> targetMap = new HashMap<>();

        // 业务员提成
        FinancePercentage percentage;
        // 业务员提成Key
        FinancePercentage percentageKey;
        // 子任务ID
        Long taskSonId;
        // 提成金额
        BigDecimal money;

        // 业务员中间流水记录
        Map<Long, FinanceSalesmanBill> salesmanBillMap = new HashMap<>();
        // 业务员提成流水Key
        FinanceSalesmanBill salesmanBillKey;
        // 业务员ID
        Long salesmanId;
        // 业务员提成流水
        FinanceSalesmanBill salesmanBill;

        // 新增提成信息数据
        for (StatementPercentageVo spv : statementPercentageVoList) {
            // 根据业务员所属机构获取提成配置信息
            if (StrUtil.isNotNull(spv.getOrgId()) && StrUtil.isNull(royalty)) {
                royalty = iSalesmanRoyaltyService.findSalesmanRoyaltyByOrgId(spv.getOrgId());
            } else if (StrUtil.isNotNull(royalty) && !royalty.getOrgId().equals(spv.getOrgId())) {
                royalty = iSalesmanRoyaltyService.findSalesmanRoyaltyByOrgId(spv.getOrgId());
            }

            if (StrUtil.isNull(royalty)) {
                logger.error("提成配置信息为空");
                continue;
            }

            // 判断数据是否完整
            if (StrUtil.isNull(spv.getSalesmanId()) || StrUtil.isNull(spv.getSalesmansId()) || StrUtil.isNull(spv.getOrgId())
                    || StrUtil.isNull(spv.getTaskSonId()) || StrUtil.isNull(spv.getTaskSonNumber()) || StrUtil.isNull(spv.getUpdateTime())) {
                logger.error("该数据异常 ---> 业务员编号: " + spv.getSalesmanId() + " 子任务编号: " + spv.getTaskSonId());
                continue;
            }

            Salesman salesmanOne = iSalesmanService.getById(spv.getSalesmanId());

            if (StrUtil.isNull(salesmanOne)) {
                logger.error("查询业务员为空 --> 编号: " + spv.getSalesmanId());
                continue;
            }

            // 初始化业务员提成
            percentage = new FinancePercentage();
            percentage.setSalesmanId(spv.getSalesmanId());
            percentage.setSalesmansId(spv.getSalesmansId());
            percentage.setOrgId(spv.getOrgId());
            percentage.setTaskSonId(spv.getTaskSonId());
            percentage.setTaskSonNumber(spv.getTaskSonNumber());
            percentage.setFinishTime(spv.getUpdateTime());

            if (spv.getStatus().equals(Number.INT_ZERO)) {
                percentage.setPercentage(royalty.getPercentage().negate());
            } else {
                percentage.setPercentage(royalty.getPercentage());
            }

            // 拦截数据  加减 抵消 这些数据不会处理
            // --------------------------------------------------><----------------------------------------------------
            // 遇到相同的数据处理
            // 子任务编号
            taskSonId = percentage.getTaskSonId();
            // 金额
            money = percentage.getPercentage();

            // 当前业务员的余额
            BigDecimal salBalance = salesmanOne.getBalance();
            // 提成配置
            BigDecimal perGroop = royalty.getPercentage();
            //更新上级业务员的余额
            BigDecimal all;

            // status [{1:增加提成},{0:减少提成}]
            if (spv.getStatus().equals(Number.INT_ZERO)) {
                all = perGroop.negate();
            } else {
                all = perGroop;
            }

            // 获取业务员编号
            salesmanId = salesmanOne.getId();
            // 生成业务员提成中间流水
            salesmanBill = new FinanceSalesmanBill();
            salesmanBill.setSalesmanId(salesmanId);
            salesmanBill.setBillType(SalesmanBillTypeMsg.BILL_TYPE_ROYALTY.getCodeType());
            salesmanBill.setAccount(SalesmanBillTypeMsg.BILL_TYPE_ROYALTY.getMessage());
            salesmanBill.setCreateTime(date);
            salesmanBill.setBillDate(date);
            salesmanBill.setOrgId(salesmanOne.getOrgId());
            salesmanBill.setLastMoney(salBalance);
            salesmanBill.setMoney(all);
            // 增加或者减少余额
            salesmanBill.setFinalMoney(salesmanBill.getLastMoney().add(salesmanBill.getMoney()));

            // 获取的业务员提成对象
            percentageKey = percentageMap.get(taskSonId);
            // 如果遇到相同的子任务编号
            if (StrUtil.isNotNull(percentageKey) && (taskSonId).equals(percentageKey.getTaskSonId())) {
                // 查看是否抵消
                percentageKey.setPercentage(percentageKey.getPercentage().add(money));
                if (percentageKey.getPercentage().compareTo(BigDecimal.ZERO) == 0) {
                    percentageMap.remove(taskSonId);
                } else {
                    percentageMap.put(taskSonId, percentage);
                }
            } else {
                percentageMap.put(taskSonId, percentage);
            }

            salesmanBillKey = salesmanBillMap.get(salesmanId);
            if (StrUtil.isNotNull(salesmanBillKey) && salesmanId.equals(salesmanBillKey.getSalesmanId())) {
                salesmanBillKey.setMoney(salesmanBillKey.getMoney().add(salesmanBill.getMoney()));
                salesmanBillKey.setFinalMoney(salesmanBillKey.getFinalMoney().add(salesmanBill.getMoney()));
            } else {
                salesmanBillMap.put(salesmanId, salesmanBill);
            }
        }

        // 迭代添加
        for (Map.Entry<Long, FinancePercentage> en : percentageMap.entrySet()) {
            FinancePercentage entity = en.getValue();
            if (StrUtil.isNotNull(entity)) {
                try {
                    // 保存提成数据
                    iFinancePercentageService.save(entity);
                } catch (Exception e) {
                    logger.error("业务员提成 数据保存失败 业务员id:{} 错误信息:{} ", entity.getSalesmanId(), e.getMessage());
                }
                // 获取业务员ID
                salesmanId = entity.getSalesmanId();
                // 保存业务员账单目标ID
                String value = targetMap.get(salesmanId);
                value = StrUtil.isNull(value) ? entity.getId().toString() : (value + "," + entity.getId().toString());
                targetMap.put(salesmanId, value);
            }
        }

        UpdateWrapper<Salesman> updateWrapper;
        // 遍历添加数据
        for (Map.Entry<Long, FinanceSalesmanBill> en : salesmanBillMap.entrySet()) {
            FinanceSalesmanBill entity = en.getValue();
            if (StrUtil.isNotNull(entity)) {
                // 状态值 默认 false 业务员余额更新失败时,停止以下操作
                boolean active = false;
                try {
                    // 更新业务员余额
                    updateWrapper = new UpdateWrapper<>();
                    updateWrapper.set("balance", entity.getFinalMoney());
                    updateWrapper.eq("id", entity.getSalesmanId());
                    update(updateWrapper);
                } catch (Exception e) {
                    active = true;
                    logger.error("更新业务员余额失败 业务员id:{} 之前余额:{} 金额:{} 之后余额:{} 错误信息:{}",
                            entity.getSalesmanId(), entity.getLastMoney(), entity.getMoney(), entity.getFinalMoney(), e.getMessage());
                }
                // 停止当前操作,跳往下一条数据
                if (active) {
                    continue;
                }
                // 更新目标ID
                String value = targetMap.get(en.getKey());
                entity.setTargetId(value);
                try {
                    // 保存业务员账单
                    iFinanceSalesmanBillService.save(entity);
                } catch (Exception e) {
                    logger.error("业务员提成中间流水生成失败， 业务员id:{} 日期：[{}] 错误信息:{} ", entity.getSalesmanId(), date, e.getMessage());
                }
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void disablesSalesmanByFactorXFD() {
        // 这个接口主要处理【现付单】前一天没有提交的任务业务员信息.
        // 并统计业务员前一天共有多少任务没有提交.
        // 并且使用日志记录的方式进行数据记录.
        List<SalesmanVo> list = baseMapper.getSalesmanByFactorXFD();
        if (list.isEmpty()) {
            logger.info("===>[现付单]前一天待提交的任务为 0 个！");
        } else {
            // 记录要禁用的业务员id
            List<Long> ids = new ArrayList<>();
            for (SalesmanVo item : list) {
                ids.add(item.getId());
                logger.error("===>[现付单]==工号：{}，业务员为：{}，账号：{}，未提交量：{}", item.getJobNumber(), item.getName(), item.getMobile(), item.getScount());
            }

            // 禁用业务员状态跳转【E=P】
            UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("status", Status.STATUS_PROHIBIT);
            updateWrapper.in("id", ids);
            updateWrapper.eq("status", Status.STATUS_ENABLE);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void disablesSalesmanByFactorGRD() {
        // 这个接口主要处理【隔日单】前一天没有提交的任务业务员信息.
        // 并统计业务员前一天共有多少任务没有提交.
        // 并且使用日志记录的方式进行数据记录.
        List<SalesmanVo> list = baseMapper.getSalesmanByFactorGRD();
        if (list.isEmpty()) {
            logger.info("===>[隔日单]前一天待提交的任务为 0 个！");
        } else {
            // 记录要禁用的业务员id
            List<Long> ids = new ArrayList<>();
            for (SalesmanVo item : list) {
                ids.add(item.getId());
                logger.error("===>[隔日单]==工号：{}，业务员为：{}，账号：{}，未提交量：{}", item.getJobNumber(), item.getName(), item.getMobile(), item.getScount());
            }

            // 禁用业务员状态跳转【E=P】
            UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("status", Status.STATUS_PROHIBIT);
            updateWrapper.in("id", ids);
            updateWrapper.eq("status", Status.STATUS_ENABLE);
        }
    }

    @Override
    public Salesman findSalesmanOnlineById(Long salesmanNumberId) {
        SalesmanNumber salesmanNumber = iSalesmanNumberService.getById(salesmanNumberId);
        Salesman salesman = getById(salesmanNumber.getSalesmanId());
        Long onlineId = salesman.getSalesmanId();
        if (StrUtil.isNotNull(onlineId) && onlineId != -1) {
            salesman = getById(onlineId);
        }
        return salesman;
    }

    @Override
    public IPage<SalesmanListVo> findAllSalesman(IPage<SalesmanListVo> page, Long uid) {
        QueryWrapper<SalesmanListVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("salesman_id", uid);

        IPage<SalesmanListVo> pageList = baseMapper.findAllSalesman(page, queryWrapper);
        for (SalesmanListVo number : pageList.getRecords()) {
            number.setNum(iTaskSonService.completedTaskCountBySalManId(number.getId()));
        }

        return pageList;
    }

    @Override
    public Salesman findSalesmanBySalesmanNumberId(Long salesmanNumberId) {
        SalesmanNumber salesmanNumber = iSalesmanNumberService.getById(salesmanNumberId);
        Salesman salesman = getById(salesmanNumber.getSalesmanId());
        if (StrUtil.isNotNull(salesman) && salesman.getId() != 1) {
            return salesman;
        }
        return null;
    }

    @Override
    public List<Salesman> findSalesmanOffline(Long salesmanId) {
        QueryWrapper<Salesman> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("salesman_id", salesmanId);
        return list(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSalesman(Salesman salesman) {
        defaultSalesman(null, salesman);
        save(salesman);
        // 关联银行卡 前提:省份 城市 区/镇 开户行 银行卡号 支行名均不能为空!!!
        /*if (StrKit.isNotNull(salesman.getProvinceId()) && StrKit.isNotNull(salesman.getCityId()) && StrKit.isNotNull(salesman.getCountyId())
                && StrKit.isNotNull(salesman.getBankName()) && StrKit.isNotNull(salesman.getBankNum()) && StrKit.isNotNull(salesman.getBankad())) {
            SalesmanBankCard salesmanBankCard = new SalesmanBankCard();
            salesmanBankCard.setSalesmanId(salesman.getId());
            salesmanBankCard.setProvinceId(salesman.getProvinceId());
            salesmanBankCard.setCityId(salesman.getCityId());
            salesmanBankCard.setCountryId(salesman.getCountyId());
            salesmanBankCard.setBankName(salesman.getBankName());
            salesmanBankCard.setBankNum(salesman.getBankNum());
            salesmanBankCard.setBranchName(salesman.getBankad());
            iSalesmanBankCardService.saveSalesmanBankCard(salesmanBankCard);
            salesman.setBankId(salesmanBankCard.getId());
        }*/
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanInfo(Salesman salesman) {
        salesman.setUpdateTime(new Date());
        updateById(salesman);
    }

    @Override
    public SalesmanVo findSalesmanVoInfo(Long uid) {
        QueryWrapper<SalesmanVo> salesmanVoQueryWrapper = new QueryWrapper<>();
        salesmanVoQueryWrapper.eq("sm.id", uid);
        return baseMapper.findSalesmanVoInfo(salesmanVoQueryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanPassword(String encryptPwd, Long uid) {
        UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("pwd",encryptPwd).set("update_time",new Date()).eq("id",uid);
        update(updateWrapper);
    }

    @Override
    public SalesmanRespVo getSalesmanInfo(Long salesmanId) {
        QueryWrapper<SalesmanRespVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id",salesmanId);
        return baseMapper.getSalesmanInfo(queryWrapper);
    }

    @Override
    public SalesmanVo findSalesmanExtCredit(Long salesmanId) {
        QueryWrapper<SalesmanVo> queryWrapper = new QueryWrapper<>();
        //状态。。。。。。
        queryWrapper.eq("s.id",salesmanId).eq("sl.status",Status.STATUS_ACTIVITY);
        return baseMapper.findSalesmanExtCredit(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void withdrawSalesmanMoney(Long salesmanId, BigDecimal money) {
        // 查询业务员金额
        Salesman salesman =baseMapper.selectById(salesmanId);
        // 之前余额
        BigDecimal beforeMoney = salesman.getBalance();
        // 之后余额
        BigDecimal lastMoney;

        // 判断业务员余额是否充足
        if (salesman.getBalance().compareTo(money) < 0) {
            String msg = "当前余额不足,无法完成提现操作";
            logger.error(msg + " 业务员编号:{}", salesmanId);
            throw BaseException.base(msg);
        }
        lastMoney = salesman.getBalance().subtract(money);

        // 更新业务员余额
        try {
            Salesman salesman1 = new Salesman();
            salesman1.setBalance(lastMoney);
            UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("id",salesmanId);
            baseMapper.update(salesman1,updateWrapper);
        } catch (Exception e) {
            String msg = "更新余额失败,无法完成提现操作";
            logger.error(msg + " 业务员编号:{} 异常信息:{}", salesmanId, e.getMessage());
            throw BaseException.base(msg);
        }

        FinanceDrawMoneyRecord drawMoneyRecord = new FinanceDrawMoneyRecord();
        drawMoneyRecord.setOrgId(salesman.getOrgId());
        drawMoneyRecord.setSalesmanId(salesmanId);
        drawMoneyRecord.setApplyMoney(money);
        drawMoneyRecord.setStatus(Status.STATUS_NOT_PAY);
        drawMoneyRecord.setCreateTime(new Date());
        // 保存提现记录
        try {
            iDrawMoneyRecordService.save(drawMoneyRecord);
        } catch (Exception e) {
            String msg = "保存提现记录失败,无法完成提现操作";
            logger.info(msg + " 业务员编号:{} 异常信息:{}", salesmanId, e.getMessage());
            throw BaseException.base(msg);
        }

        // 记录提现记录
        FinanceSalesmanBill salesmanBill = new FinanceSalesmanBill();
        salesmanBill.setOrgId(salesman.getOrgId());
        salesmanBill.setBillType(SalesmanBillTypeMsg.BILL_TYPE_RECORD.getCodeType());
        salesmanBill.setSalesmanId(salesmanId);
        salesmanBill.setTargetId(drawMoneyRecord.getId().toString());
        salesmanBill.setLastMoney(beforeMoney);
        salesmanBill.setMoney(money.negate());
        salesmanBill.setFinalMoney(lastMoney);
        salesmanBill.setAccount(SalesmanBillTypeMsg.BILL_TYPE_RECORD.getMessage());
        salesmanBill.setBillDate(new Date());
        salesmanBill.setCreateTime(new Date());

        try {
            iSalesmanBillService.save(salesmanBill);
        } catch (Exception e) {
            String msg = "保存账单记录失败,无法完成提现操作";
            logger.error(msg + " 业务员编号:{} 异常信息:{}", salesmanId, e.getMessage());
            throw BaseException.base(msg);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSalesmanMoney(Long id, BigDecimal money) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新业务员余额失败,id为空");
        }
        UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("balance", money);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Override
    public boolean isExistSalesmanByBank(String bankNum) {
        if (StrUtil.isNull(bankNum)) {
            return false;
        }
        return count(new QueryWrapper<Salesman>().eq("bank_num", bankNum).select("id")) > 0;
    }

    @Override
    public boolean isExistSalesmanByIDCard(String idCard) {
        if (StrUtil.isNull(idCard)) {
            return false;
        }
        return count(new QueryWrapper<Salesman>().eq("card_num", idCard).select("id")) > 0;
    }

    @Override
    public boolean isExistSalesmanByQQ(String qq) {
        if (StrUtil.isNull(qq)) {
            return false;
        }
        return count(new QueryWrapper<Salesman>().eq("qq", qq).select("id")) > 0;
    }

    @Override
    public boolean isExistSalesmanByWeChat(String weChat) {
        if (StrUtil.isNull(weChat)) {
            return false;
        }
        return count(new QueryWrapper<Salesman>().eq("wechart", weChat).select("id")) > 0;
    }
}
