package com.sbdj.service.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.DateUtil;
import com.sbdj.service.member.admin.vo.SalesmanLeaderboardVo;
import com.sbdj.service.member.entity.SalesmanStatementLeaderboard;
import com.sbdj.service.member.mapper.SalesmanStatementLeaderboardMapper;
import com.sbdj.service.member.service.ISalesmanStatementLeaderboardService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 业务员佣金排行榜 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SalesmanStatementLeaderboardServiceImpl extends ServiceImpl<SalesmanStatementLeaderboardMapper, SalesmanStatementLeaderboard> implements ISalesmanStatementLeaderboardService {
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSalesmanStatementLeaderboard(Long orgId) {
        // 获取上个月份
        String createDate = DateUtil.getBeforeMonth();
        QueryWrapper<SalesmanStatementLeaderboard> totalWrapper = new QueryWrapper<>();
        totalWrapper.eq("org_id", orgId);
        totalWrapper.eq("create_date", createDate);
        totalWrapper.select("id");
        int count = count(totalWrapper);
        if (count >= 0) {
            return;
        }
        QueryWrapper<SalesmanStatementLeaderboard> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ts.org_id", orgId);
        queryWrapper.eq("DATE_FORMAT(ts.update_time,'%Y-%m')", createDate);
        queryWrapper.groupBy("ts.salesman_id");
        queryWrapper.orderByDesc("money");
        queryWrapper.last("LIMIT 10");
        List<SalesmanStatementLeaderboard> list = baseMapper.findSalesmanStatementLeaderboardByOrgIdAndDate(queryWrapper);
        for (SalesmanStatementLeaderboard salesmanStatementLeaderboard : list) {
            salesmanStatementLeaderboard.setStatus(Status.STATUS_ACTIVITY);
        }
        saveBatch(list, 100);
    }

    @Cacheable(cacheNames="StatementLeaderboard",sync=true,key="#orgId +'-'+#date")
    @Override
    public List<SalesmanLeaderboardVo> findSalesmanStatementLeaderboardByOrgIdAndDate(Long orgId, String date) {
        QueryWrapper<SalesmanLeaderboardVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ssld.org_id",orgId);
        queryWrapper.eq("ssld.create_date",date);
        return baseMapper.findSalesmanPercentageLeaderboardByOrgIdAndDate2(queryWrapper);
    }
}
