package com.sbdj.service.member.type;

/**
 * @ClassName: PayGoodsType
 * @Description: 货款类型
 * @author: 黄天良
 * @date: 2019年1月4日 下午2:45:23
 */
public final class PayGoodsType {

    /**
     * 申请 {@value}
     */
    public static final String PAY_GOODS_TYPE_SQ = "SQ";

    /**
     * 垫付 {@value}
     */
    public static final String PAY_GOODS_TYPE_DF = "DF";

}
