package com.sbdj.service.member.type;

/**
 * Created by Htl to 2019/05/27 <br/>
 * 业务员账单业类型
 */
public final class SalesmanBillType {

    /**
     * 系统提现
     */
    public static final Integer BILL_TYPE_RECORD = 1;

    /**
     * 业务员佣金
     */
    public static final Integer BILL_TYPE_BROKERAGE = 2;

    /**
     * 业务员提成（业务员的下级提成）
     */
    public static final Integer BILL_TYPE_ROYALTY = 3;

    /**
     * 惩罚类型
     */
    public static final Integer BILL_TYPE_PENALTY = 4;

    /**
     * 奖励类型
     */
    public static final Integer BILL_TYPE_PRNALTY = 5;

    /**
     * 差价类型
     */
    public static final Integer BILL_TYPE_DIFFER = 6;
}
