package com.sbdj.service.member.type;

/**
 * 账单类型消息提示
 */
public enum SalesmanBillTypeMsg {

    /**
     * 系统提现
     */
    BILL_TYPE_RECORD(SalesmanBillType.BILL_TYPE_RECORD, "提现"),
    BILL_TYPE_BROKERAGE(SalesmanBillType.BILL_TYPE_BROKERAGE, "佣金"),
    BILL_TYPE_ROYALTY(SalesmanBillType.BILL_TYPE_ROYALTY, "提成"),
    BILL_TYPE_PENALTY(SalesmanBillType.BILL_TYPE_PENALTY, "惩罚"),
    BILL_TYPE_PRNALTY(SalesmanBillType.BILL_TYPE_PRNALTY, "奖励"),
    BILL_TYPE_DIFFER(SalesmanBillType.BILL_TYPE_DIFFER, "差价"),
    ;

    SalesmanBillTypeMsg(Integer codeType, String message) {
        this.codeType = codeType;
        this.message = message;
    }

    // 消息类型
    private Integer codeType;
    // 消息体
    private String message;

    public Integer getCodeType() {
        return codeType;
    }

    public void setCodeType(Integer codeType) {
        this.codeType = codeType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
