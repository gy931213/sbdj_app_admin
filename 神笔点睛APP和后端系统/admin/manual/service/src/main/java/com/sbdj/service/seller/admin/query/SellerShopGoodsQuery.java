package com.sbdj.service.seller.admin.query;

/**
 * <p>
 * 商品query
 * </p>
 *
 * @author Yly
 * @since 2019-11-26
 */
public class SellerShopGoodsQuery {
    /** 所属机构 **/
    private Long orgId;
    /** 店铺id **/
    private Long shopId;
    /** 关键字 **/
    private String keyword;
    // 店铺名称
    private String shopName;
    // 商品标题
    private String title;
    // 平台id
    private Long platformId;
    // 商品是否卡位[{0：否},{1：是}]
    private Integer isCard;
    // 是否有优惠卷[{0：否}，{1：是}]
    private Integer isCoupon;
    /** 商品链接 **/
    private String link;
    // 商品类目
    private String category;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public Integer getIsCard() {
        return isCard;
    }

    public void setIsCard(Integer isCard) {
        this.isCard = isCard;
    }

    public Integer getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(Integer isCoupon) {
        this.isCoupon = isCoupon;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
