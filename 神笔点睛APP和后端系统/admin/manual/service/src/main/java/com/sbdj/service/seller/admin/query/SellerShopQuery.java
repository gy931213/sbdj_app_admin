package com.sbdj.service.seller.admin.query;

import io.swagger.annotations.Api;

/**
 * <p>
 * 店铺Query
 * </p>
 *
 * @author Yly
 * @since 2019-11-26
 */
@Api(tags = "店铺query")
public class SellerShopQuery {
    /**所属机构*/
    private Long orgId;
    /**平台id**/
    private Long platformId;
    /**店铺名称**/
    private String shopName;
    /**店铺编码**/
    private String shopCode;
    /**商家id**/
    private Long adminId;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }
}
