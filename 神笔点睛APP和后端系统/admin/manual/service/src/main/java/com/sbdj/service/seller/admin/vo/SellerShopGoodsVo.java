package com.sbdj.service.seller.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * <p>
 * 商品vo
 * </p>
 *
 * @author Yly
 * @since 2019-11-26
 */
public class SellerShopGoodsVo {
    /** id 自增 **/
    private Long id;
    /** 所属店铺id（关联到seller_shop表） **/
    private Long shopId;
    /** 店铺名称 **/
    private String shopName;
    /**所属机构id**/
    private Long orgId;
    /** 商家名称 **/
    private String sellerName;
    /** 商品标题 **/
    private String title;
    /** 商品链接 **/
    private String link;
    /** 商品图片链接 **/
    private String imgLink;
    /** 创建时间 **/
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    /**平台id**/
    private Long platformId;
    /**平台名称**/
    private String platformName;
    /**商品打标[{0：否},{1：是}]**/
    private Integer isCard;
    /** 是否有优惠卷[{0：否}，{1：是}] */
    private Integer isCoupon;
    /** 优惠卷链接（图片链接）*/
    private String couponLink;
    /** 旗帜标志，0(灰色), 1(红色), 2(黄色), 3(绿色), 4(蓝色), 5(粉红色) ,默认值：5**/
    private Integer flag = 5;
    /** 备注信息 ,默认值：2**/
    private String remark = "2";
    /** 商品重量 **/
    private String weight;
    /** 商品关键词 **/
    private String keywords;
    /** 商品类目 **/
    private String category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImgLink() {
        return imgLink;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public Integer getIsCard() {
        return isCard;
    }

    public void setIsCard(Integer isCard) {
        this.isCard = isCard;
    }

    public Integer getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(Integer isCoupon) {
        this.isCoupon = isCoupon;
    }

    public String getCouponLink() {
        return couponLink;
    }

    public void setCouponLink(String couponLink) {
        this.couponLink = couponLink;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
