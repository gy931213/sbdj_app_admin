package com.sbdj.service.seller.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 店铺vo
 * </p>
 *
 * @author Yly
 * @since 2019-11-26
 */
@ApiModel(value = "商家店铺", description = "商家店铺")
public class SellerShopVo {
    /** 店铺id **/
    private Long id;
    /** 所属商家id **/
    private Long adminId;
    /** 所属商家名称 **/
    private String adminName;
    /** 店铺名称 **/
    private String shopName;
    /** 店铺编码 **/
    private Integer shopCode;
    /** 联系电话 **/
    private String userPhone;
    /** 联系人名称 **/
    private String userName;
    /** 创建时间 **/
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    /** 返现金额 **/
    private BigDecimal shopAccount;
    /** 状态{A：活动状态，D：删除状态} **/
    private String status;
    /** 平台platformId **/
    private Long platformId;
    /** 平台名称{淘宝，京东........n} **/
    private String platformName;

    // ===== 2019/1/22
    /**卖家昵称**/
    private String sellerNick;
    /**公众号id**/
    private Integer uniacid;

    // 2019-02-27
    /** 旗帜标志，0(灰色), 1(红色), 2(黄色), 3(绿色), 4(蓝色), 5(粉红色) ,默认值：5**/
    private Integer flag = 5;
    /** 备注信息 ,默认值：2**/
    private String remark = "2";
    /** 发件人地址*/
    private String address;
    /** 商品重量 **/
    private String weight;
    /** 授权码 **/
    private String sessionkey;
    /** 授权码ID **/
    private Long codeId;
    /** 是否购买应用 YES:有 NO:没有 **/
    private String isBuy;
    /** 是否平台发货 [{YES ：是},{NO ： 否}]  默认为 NO **/
    private String isExpress;
    /**负责人id**/
    private Long principalId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getShopCode() {
        return shopCode;
    }

    public void setShopCode(Integer shopCode) {
        this.shopCode = shopCode;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getShopAccount() {
        return shopAccount;
    }

    public void setShopAccount(BigDecimal shopAccount) {
        this.shopAccount = shopAccount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getSellerNick() {
        return sellerNick;
    }

    public void setSellerNick(String sellerNick) {
        this.sellerNick = sellerNick;
    }

    public Integer getUniacid() {
        return uniacid;
    }

    public void setUniacid(Integer uniacid) {
        this.uniacid = uniacid;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getSessionkey() {
        return sessionkey;
    }

    public void setSessionkey(String sessionkey) {
        this.sessionkey = sessionkey;
    }

    public Long getCodeId() {
        return codeId;
    }

    public void setCodeId(Long codeId) {
        this.codeId = codeId;
    }

    public String getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(String isBuy) {
        this.isBuy = isBuy;
    }

    public String getIsExpress() {
        return isExpress;
    }

    public void setIsExpress(String isExpress) {
        this.isExpress = isExpress;
    }

    public Long getPrincipalId() {
        return principalId;
    }

    public void setPrincipalId(Long principalId) {
        this.principalId = principalId;
    }
}
