package com.sbdj.service.seller.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 商家/卖方（店铺）
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="SellerShop对象", description="商家/卖方（店铺）")
public class SellerShop implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id 自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构id")
    private Long orgId;

    @ApiModelProperty(value = "店铺所属商家id/{关联到sys_admin表中的type为2}")
    private Long adminId;

    @ApiModelProperty(value = "店铺名称")
    private String shopName;

    @ApiModelProperty(value = "店铺编码{默认为：0,}")
    private Integer shopCode;

    @ApiModelProperty(value = "平台id{淘宝，京东........n}")
    private Long platformId;

    @ApiModelProperty(value = "联系电话")
    private String userPhone;

    @ApiModelProperty(value = "联系人名称")
    private String userNaem;

    @ApiModelProperty(value = "发件人地址")
    private String address;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人id")
    private Long createId;

    @ApiModelProperty(value = "更新时间")
    private Date oprTime;

    @ApiModelProperty(value = "更新人id")
    private Long oprId;

    @ApiModelProperty(value = "返现金额")
    private BigDecimal shopAccount;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "公众号id")
    private Integer uniacid;

    @ApiModelProperty(value = "卖家昵称")
    private String sellerNick;

    @ApiModelProperty(value = "旗帜")
    private Integer flag;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "授权码")
    private String sessionkey;

    @ApiModelProperty(value = "授权码ID -> 店铺ID")
    private Long codeId;

    @ApiModelProperty(value = "是否购买应用 YES:有 NO:没有")
    private String isBuy;

    @ApiModelProperty(value = "是否平台发货 [{YES ：是},{NO ： 否}]  默认为 NO")
    private String isExpress;

    @ApiModelProperty(value = "负责人id")
    private Long principalId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getShopCode() {
        return shopCode;
    }

    public void setShopCode(Integer shopCode) {
        this.shopCode = shopCode;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserNaem() {
        return userNaem;
    }

    public void setUserNaem(String userNaem) {
        this.userNaem = userNaem;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public Date getOprTime() {
        return oprTime;
    }

    public void setOprTime(Date oprTime) {
        this.oprTime = oprTime;
    }

    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }

    public BigDecimal getShopAccount() {
        return shopAccount;
    }

    public void setShopAccount(BigDecimal shopAccount) {
        this.shopAccount = shopAccount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUniacid() {
        return uniacid;
    }

    public void setUniacid(Integer uniacid) {
        this.uniacid = uniacid;
    }

    public String getSellerNick() {
        return sellerNick;
    }

    public void setSellerNick(String sellerNick) {
        this.sellerNick = sellerNick;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSessionkey() {
        return sessionkey;
    }

    public void setSessionkey(String sessionkey) {
        this.sessionkey = sessionkey;
    }

    public Long getCodeId() {
        return codeId;
    }

    public void setCodeId(Long codeId) {
        this.codeId = codeId;
    }

    public String getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(String isBuy) {
        this.isBuy = isBuy;
    }

    public String getIsExpress() {
        return isExpress;
    }

    public void setIsExpress(String isExpress) {
        this.isExpress = isExpress;
    }

    public Long getPrincipalId() {
        return principalId;
    }

    public void setPrincipalId(Long principalId) {
        this.principalId = principalId;
    }

    @Override
    public String toString() {
        return "SellerShop{" +
                "id=" + id +
                ", orgId=" + orgId +
                ", adminId=" + adminId +
                ", shopName='" + shopName + '\'' +
                ", shopCode=" + shopCode +
                ", platformId=" + platformId +
                ", userPhone='" + userPhone + '\'' +
                ", userNaem='" + userNaem + '\'' +
                ", address='" + address + '\'' +
                ", createTime=" + createTime +
                ", createId=" + createId +
                ", oprTime=" + oprTime +
                ", oprId=" + oprId +
                ", shopAccount=" + shopAccount +
                ", status='" + status + '\'' +
                ", uniacid=" + uniacid +
                ", sellerNick='" + sellerNick + '\'' +
                ", flag=" + flag +
                ", remark='" + remark + '\'' +
                ", sessionkey='" + sessionkey + '\'' +
                ", codeId=" + codeId +
                ", isBuy='" + isBuy + '\'' +
                ", isExpress='" + isExpress + '\'' +
                ", principalId=" + principalId +
                '}';
    }
}
