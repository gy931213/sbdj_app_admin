package com.sbdj.service.seller.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 商家/卖方（店铺商品）
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="SellerShopGoods对象", description="商家/卖方（店铺商品）")
public class SellerShopGoods implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id 自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属店铺id（关联到seller_shop表）")
    private Long shopId;

    @ApiModelProperty(value = "商品标题")
    private String title;

    @ApiModelProperty(value = "商品链接")
    private String link;

    @ApiModelProperty(value = "商品图片")
    private String imgLink;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人id")
    private Long createId;

    @ApiModelProperty(value = "更新时间")
    private Date oprTime;

    @ApiModelProperty(value = "更新人id")
    private Long oprId;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "商品是否卡位[{0：否},{1：是}]")
    private Integer isCard;

    @ApiModelProperty(value = "是否有优惠卷[{0：否}，{1：是}] ")
    private Integer isCoupon;

    @ApiModelProperty(value = "优惠卷链接（图seller_shop_goods片链接）")
    private String couponLink;

    @ApiModelProperty(value = "旗帜标志，0(灰色), 1(红色), 2(黄色), 3(绿色), 4(蓝色), 5(粉红色) ，默认值：5")
    private Integer flag;

    @ApiModelProperty(value = "备注信息")
    private String remark;

    @ApiModelProperty(value = "商品重量")
    private String weight;

    @ApiModelProperty(value = "商品关键词，多个使用换行隔开即可")
    private String keywords;

    @ApiModelProperty(value = "商品类目")
    private String category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    public String getImgLink() {
        return imgLink;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }
    public Date getOprTime() {
        return oprTime;
    }

    public void setOprTime(Date oprTime) {
        this.oprTime = oprTime;
    }
    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Integer getIsCard() {
        return isCard;
    }

    public void setIsCard(Integer isCard) {
        this.isCard = isCard;
    }
    public Integer getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(Integer isCoupon) {
        this.isCoupon = isCoupon;
    }
    public String getCouponLink() {
        return couponLink;
    }

    public void setCouponLink(String couponLink) {
        this.couponLink = couponLink;
    }
    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "SellerShopGoods{" +
                "id=" + id +
                ", shopId=" + shopId +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", imgLink='" + imgLink + '\'' +
                ", createTime=" + createTime +
                ", createId=" + createId +
                ", oprTime=" + oprTime +
                ", oprId=" + oprId +
                ", status='" + status + '\'' +
                ", isCard=" + isCard +
                ", isCoupon=" + isCoupon +
                ", couponLink='" + couponLink + '\'' +
                ", flag=" + flag +
                ", remark='" + remark + '\'' +
                ", weight='" + weight + '\'' +
                ", keywords='" + keywords + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
