package com.sbdj.service.seller.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.seller.admin.query.SellerShopGoodsQuery;
import com.sbdj.service.seller.admin.vo.SellerShopGoodsVo;
import com.sbdj.service.seller.entity.SellerShopGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商家/卖方（店铺商品） Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SellerShopGoodsMapper extends BaseMapper<SellerShopGoods> {
    /**
     * 分页展示商品
     * @author Yly
     * @date 2019-11-26 20:58
     * @param iPage 分页
     * @param query 商品查询参数
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.seller.admin.vo.SellerShopGoodsVo>
     */
    IPage<SellerShopGoodsVo> findSellerShopGoodsList(@Param("page") IPage<SellerShopGoodsVo> iPage, @Param(Constants.WRAPPER) Wrapper<SellerShopGoodsQuery> query);
}
