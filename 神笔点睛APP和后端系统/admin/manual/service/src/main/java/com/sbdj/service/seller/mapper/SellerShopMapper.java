package com.sbdj.service.seller.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.seller.admin.query.SellerShopQuery;
import com.sbdj.service.seller.admin.vo.SellerShopVo;
import com.sbdj.service.seller.entity.SellerShop;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商家/卖方（店铺） Mapper 接口
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
public interface SellerShopMapper extends BaseMapper<SellerShop> {

    /**
     * @param shopId   店铺id
     * @param shopCode 店铺简码
     * @param orgId    所属机构id
     * @return String 返回类型
     * @Title: findSellerShopByShopIdShopCode
     * @Description: 根据参数获取数据的接口
     */
    String findSellerShopByShopIdShopCode(@Param("shopId") Long shopId, @Param("shopCode") int shopCode, @Param("orgId") Long orgId);

    /**
     * 分页展示店铺数据
     * @author Yly
     * @date 2019-11-26 19:50
     * @param iPage
     * @param query
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.seller.admin.vo.SellerShopVo>
     */
    IPage<SellerShopVo> findSellerShopPageList(@Param("page") IPage<SellerShopVo> iPage, @Param(Constants.WRAPPER) Wrapper<SellerShopQuery> query);
}
