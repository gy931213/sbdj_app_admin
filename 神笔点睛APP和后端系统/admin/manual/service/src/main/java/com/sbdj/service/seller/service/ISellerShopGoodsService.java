package com.sbdj.service.seller.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.seller.admin.query.SellerShopGoodsQuery;
import com.sbdj.service.seller.admin.vo.SellerShopGoodsVo;
import com.sbdj.service.seller.entity.SellerShopGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商家/卖方（店铺商品） 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISellerShopGoodsService extends IService<SellerShopGoods> {
    /**
     * 分页展示商品信息
     * @author Yly
     * @date 2019-11-26 20:56
     * @param query 商品query
     * @param iPage 分页
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.seller.admin.vo.SellerShopGoodsVo>
     */
    IPage<SellerShopGoodsVo> findSellerShopGoodsList(SellerShopGoodsQuery query, IPage<SellerShopGoodsVo> iPage);

    /**
     * 保存商品
     * @author Yly
     * @date 2019-11-26 21:09
     * @param shopGoods 商品
     * @return void
     */
    void saveSellerShopGoods(SellerShopGoods shopGoods);

    /**
     * 更新商品
     * @author Yly
     * @date 2019-11-26 21:15
     * @param shopGoods 商品
     * @return void
     */
    void updateSellerShopGoods(SellerShopGoods shopGoods);

    /**
     * 逻辑删除商品
     * @author Yly
     * @date 2019-11-26 21:16
     * @param id 商品id
     * @param oprId 操作人
     * @return void
     */
    void logicDeleteSellerShopGoods(Long id,Long oprId);

    /**
     * 更新商品打标状态
     * @author Yly
     * @date 2019-11-26 21:17
     * @param id 商品id
     * @param isCard 打标状态
     * @return void
     */
    void updateSellerShopGoodsByIsCard(Long id, Integer isCard);

    /**
     * Created by Htl to 2019/07/24 <br/>
     * 异步处理商品打标（快搜）
     *
     * @param taskSonId 子任务id
     * @param isCard    是否打标
     * @param isMark    是否标记
     */
    public void asynMarkShopGoods(Long taskSonId, Integer isCard, Integer isMark);

    /**
     * Created by Htl to 2019/07/24 <br/>
     * 同步处理商品打标 （快搜）
     *
     * @param taskSonId 子任务id
     */
    public void synchMarkShopGoods(Long taskSonId);
}
