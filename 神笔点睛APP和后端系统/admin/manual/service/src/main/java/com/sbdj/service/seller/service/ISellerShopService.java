package com.sbdj.service.seller.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.seller.admin.query.SellerShopQuery;
import com.sbdj.service.seller.admin.vo.SellerShopVo;
import com.sbdj.service.seller.entity.SellerShop;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商家/卖方（店铺） 服务类
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
public interface ISellerShopService extends IService<SellerShop> {


    /**
     * @param shopId   店铺id
     * @param shopCode 店铺简码
     * @param orgId    所属机构id
     * @return String 返回类型
     * @Title: findSellerShopByShopIdShopCode
     * @Description: 根据参数获取数据的接口
     */
    String findSellerShopByShopIdShopCode(Long shopId, int shopCode, Long orgId);

    /**
     * 分页展示店铺数据
     * @author Yly
     * @date 2019-11-26 19:45
     * @param query 店铺query
     * @param iPage 分页
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.seller.admin.vo.SellerShopVo>
     */
    IPage<SellerShopVo> findSellerShopPageList(SellerShopQuery query, IPage<SellerShopVo> iPage);

    /**
     * 通过平台id，名称或机构id获取店铺
     * @author Yly
     * @date 2019-11-26 20:04
     * @param platformId 平台id
     * @param shopName 店铺名称
     * @param orgId 机构id
     * @return com.sbdj.service.seller.entity.SellerShop
     */
    SellerShop findSellerShopByPlatformIdAndShopName(Long platformId,String shopName,Long orgId);

    /**
     * 保存店铺
     * @author Yly
     * @date 2019-11-26 20:07
     * @param sellerShop 店铺
     * @return void
     */
    void saveSellerShop(SellerShop sellerShop);

    /**
     * 更新店铺
     * @author Yly
     * @date 2019-11-26 20:10
     * @param sellerShop 店铺
     * @return void
     */
    void updateSellerShop(SellerShop sellerShop);

    /**
     * 删除店铺
     * @author Yly
     * @date 2019-11-26 20:16
     * @param id 店铺id
     * @param oprId 操作人
     * @return void
     */
    void logicDeleteSellerShop(Long id,Long oprId);

    /**
     * 批量删除店铺
     * @author Yly
     * @date 2019-11-26 20:16
     * @param ids 店铺ids
     * @param oprId 操作人
     * @return void
     */
    void logicDeleteSellerShops(String ids,Long oprId);

    List<SellerShop> findSellerShopByOrgId(Long orgId);
}
