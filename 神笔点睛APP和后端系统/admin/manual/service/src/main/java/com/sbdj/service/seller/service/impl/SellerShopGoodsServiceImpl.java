package com.sbdj.service.seller.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.PlatformType;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.GoodsMarkingUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.seller.admin.query.SellerShopGoodsQuery;
import com.sbdj.service.seller.admin.vo.SellerShopGoodsVo;
import com.sbdj.service.seller.entity.SellerShopGoods;
import com.sbdj.service.seller.mapper.SellerShopGoodsMapper;
import com.sbdj.service.seller.service.ISellerShopGoodsService;
import com.sbdj.service.task.admin.vo.TaskSonVo;
import com.sbdj.service.task.service.ITaskSonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 商家/卖方（店铺商品） 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SellerShopGoodsServiceImpl extends ServiceImpl<SellerShopGoodsMapper, SellerShopGoods> implements ISellerShopGoodsService {

    private Logger logger = LoggerFactory.getLogger(SellerShopGoodsServiceImpl.class);

    @Autowired
    private ITaskSonService iTaskSonService;

    @Override
    public IPage<SellerShopGoodsVo> findSellerShopGoodsList(SellerShopGoodsQuery query, IPage<SellerShopGoodsVo> iPage) {
        QueryWrapper<SellerShopGoodsQuery> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ssg.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId()), "ss.org_id", query.getOrgId());
        queryWrapper.eq(StrUtil.isNotNull(query.getPlatformId()) && !Number.LONG_ZERO.equals(query.getPlatformId()),"ss.platform_id", query.getPlatformId());
        queryWrapper.eq(StrUtil.isNotNull(query.getShopId()) && !Number.LONG_ZERO.equals(query.getShopId()),"ssg.shop_id", query.getShopId());
        queryWrapper.eq(StrUtil.isNotNull(query.getIsCard()) && !Number.INT_ZERO.equals(query.getIsCard()),"ssg.is_card", query.getIsCard());
        queryWrapper.eq(StrUtil.isNotNull(query.getIsCoupon()) && !Number.INT_ZERO.equals(query.getIsCoupon()),"ssg.is_coupon", query.getIsCoupon());
        queryWrapper.like(StrUtil.isNotNull(query.getTitle()), "ssg.title", query.getTitle());
        queryWrapper.like(StrUtil.isNotNull(query.getShopName()), "ss.shop_name", query.getShopName());
        queryWrapper.like(StrUtil.isNotNull(query.getLink()), "ssg.link", query.getLink());
        queryWrapper.like(StrUtil.isNotNull(query.getCategory()), "ssg.category", query.getCategory());
        return baseMapper.findSellerShopGoodsList(iPage, queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSellerShopGoods(SellerShopGoods shopGoods) {
        shopGoods.setCreateTime(new Date());
        shopGoods.setStatus(Status.STATUS_ACTIVITY);
        shopGoods.setIsCard(0);
        save(shopGoods);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSellerShopGoods(SellerShopGoods shopGoods) {
        if (StrUtil.isNull(shopGoods) || StrUtil.isNull(shopGoods.getId())) {
            throw BaseException.base("更新商品失败,id为空");
        }
        updateById(shopGoods);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteSellerShopGoods(Long id, Long oprId) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除商品失败,id为空");
        }
        UpdateWrapper<SellerShopGoods> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.set("opr_id", oprId);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSellerShopGoodsByIsCard(Long id, Integer isCard) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新商品打标状态失败,id为空");
        }
        UpdateWrapper<SellerShopGoods> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("is_card", isCard);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Override
    public void asynMarkShopGoods(Long taskSonId, Integer isCard, Integer isMark) {
        TaskSonVo tse = iTaskSonService.findTaskSonExt(taskSonId);
        // 判断对象为空
        if (StrUtil.isNull(tse)) {
            logger.info("==>打标：子任务数据为空！");
            throw BaseException.base("子任务数据为空");
        }
        // 判断平台不等于淘宝
        if (StrUtil.isNotNull(tse.getPlatformCode()) && !tse.getPlatformCode().equalsIgnoreCase(PlatformType.P_TYPE_TB)) {
            throw BaseException.base("非淘宝平台");
        }

        // 获取打标配置
        // 判断打标配置为空
		/*MarkGoodsSetting mgs = iMarkGoodsSettingService.findMarkGoodsSettingByOrgId(tse.getOrgId());
		if (StrUtil.isNull(mgs)) {
            // 默认采用神笔机构
            mgs = iMarkGoodsSettingService.findMarkGoodsSettingByOrgId(2L);
        }
		if(StrUtil.isNull(mgs)) {
			logger.info("==>打标：配置数据为空！");
			return;
		}

		Integer total = iOrganizationService.getMarkGoodsTotal(tse.getOrgId());
		if (total == 0) {
		    logger.error("==>所属机构【{}】,打标:打标次数不足!",tse.getOrgId());
		    return;
        }*/

        // 处理淘宝链接，获取id
        if (StrUtil.isNull(tse.getLink())) {
            logger.info("==>打标：淘宝链接为空！");
            throw BaseException.base("淘宝链接为空");
        }
        // 链接
        String link = tse.getLink();
        // 链接进行处理
        String tbid = link.substring((link.indexOf("id=") + 3));

        // 请求第三方商品打标的
        //iMarkGoodsSettingService.mark(mgs, tbid, tse.getOnlineid(), tse.getKeyword());
        String result = GoodsMarkingUtil.markingGoods(tbid, tse.getOnlineid(), tse.getKeyword());
        JSONObject json = JSONObject.parseObject(result);
        boolean flag = json.getBoolean("state");
        // 状态
        if (!flag) {
            throw BaseException.base(json.getString("msg"));
        }
        // 修改打标记录，从未打标更改为已打标
        iTaskSonService.updateTaskSonisMark(taskSonId);
        // 所属机构次数减1
        ///iOrganizationService.subOrganizationTotal(tse.getOrgId());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void synchMarkShopGoods(Long taskSonId) {
        TaskSonVo tse = iTaskSonService.findTaskSonExt(taskSonId);
        // 判断对象为空
        if (StrUtil.isNull(tse)) {
            logger.info("==>打标：子任务数据为空！");
            throw BaseException.base("子任务数据为空");
        }
        // 判断平台不等于淘宝
        if (StrUtil.isNotNull(tse.getPlatformCode()) && !tse.getPlatformCode().equalsIgnoreCase(PlatformType.P_TYPE_TB)) {
            throw BaseException.base("非淘宝平台");
        }
        /*Integer total = iOrganizationService.getMarkGoodsTotal(tse.getOrgId());
		if (total == 0) {
		    logger.error("==>打标:打标次数不足!");
		    throw new RuntimeException("打标次数不足");
        }*/
        // 获取打标配置
        // 判断打标配置为空
		/*MarkGoodsSetting mgs = iMarkGoodsSettingService.findMarkGoodsSettingByOrgId(tse.getOrgId());
		if (StrUtil.isNull(mgs)) {
            // 默认采用神笔机构
            mgs = iMarkGoodsSettingService.findMarkGoodsSettingByOrgId(2L);
        }
		if(StrUtil.isNull(mgs)) {
			logger.info("==>打标：配置数据为空！");
			throw new RuntimeException("配置数据为空");
		}*/

        // 处理淘宝链接，获取id
        if (StrUtil.isNull(tse.getLink())) {
            logger.info("==>打标：淘宝链接为空！");
            throw BaseException.base("淘宝链接为空");
        }
        // 链接
        String link = tse.getLink();
        // 链接进行处理
        String tbid = link.substring((link.indexOf("id=") + 3));

        try {
            String result = GoodsMarkingUtil.markingGoods(tbid, tse.getOnlineid(), tse.getKeyword());
            JSONObject json = JSONObject.parseObject(result);
            boolean flag = json.getBoolean("state");
            // 状态
            if (!flag) {
                throw BaseException.base(json.getString("msg"));
            }
            // 修改打标记录，从未打标更改为已打标
            iTaskSonService.updateTaskSonisMark(taskSonId);
            //iMarkGoodsSettingService.mark(mgs, tbid, tse.getOnlineid(), tse.getKeyword());

            // 所属机构次数减1
            //iOrganizationService.subOrganizationTotal(tse.getOrgId());
			/*
			// 扣除打标金额，配置中配置按配置进行扣除
			// 配置没有配置默认不进行扣除操作
			if(StrUtil.isNotNull(mgs.getMoney()) && mgs.getMoney().compareTo(new BigDecimal(0)) == 1) {
				// 获取业务员信息
				Salesman sm = iSalesmanService.findSalesmanById(tse.getSalesmanId());
				if(StrUtil.isNull(sm)) {
					logger.info("==>同步打标，扣除业务员余额，业务员信息不存在！");
					return;
				}
				
				SalesmanBill sb = new SalesmanBill();
				sb.setBillDate(new Date());
				sb.setOrgId(sm.getOrgId());
				sb.setSalesmanId(sm.getId());
				sb.setCreateTime(new Date());
				sb.setBillType(SalesmanBillTypeMsg.BILL_TYPE_PENALTY.getCodeType());
				sb.setAccount("商品打标");
				sb.setTargetId("-1");
				sb.setLastMoney(sm.getBalance());
				sb.setMoney(mgs.getMoney().negate());
				sb.setFinalMoney(sm.getBalance().subtract(mgs.getMoney()));
				iSalesmanBillService.saveSalesmanBill(sb);
				
				// 更新业务员余额
				iSalesmanService.updateSelesmnDeductBalance(sm.getId(), mgs.getMoney());
			}
			*/
        } catch (Exception e) {
            logger.info("==>打标失败：异常信息为：【{}】", e.getMessage());
            throw BaseException.base("打标失败");
        }
    }
}
