package com.sbdj.service.seller.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.seller.admin.query.SellerShopQuery;
import com.sbdj.service.seller.admin.vo.SellerShopVo;
import com.sbdj.service.seller.entity.SellerShop;
import com.sbdj.service.seller.mapper.SellerShopMapper;
import com.sbdj.service.seller.service.ISellerShopService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 商家/卖方（店铺） 服务实现类
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SellerShopServiceImpl extends ServiceImpl<SellerShopMapper, SellerShop> implements ISellerShopService {
    private static Logger logger = LoggerFactory.getLogger(SellerShopServiceImpl.class);

    @Override
    public String findSellerShopByShopIdShopCode(Long shopId, int shopCode, Long orgId) {
        return baseMapper.findSellerShopByShopIdShopCode(shopId,shopCode,orgId);
    }

    @Override
    public IPage<SellerShopVo> findSellerShopPageList(SellerShopQuery query, IPage<SellerShopVo> iPage) {
        QueryWrapper<SellerShopQuery> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ss.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId()), "ss.org_id", query.getOrgId());
        queryWrapper.eq(StrUtil.isNotNull(query.getPlatformId()) && !Number.LONG_ZERO.equals(query.getPlatformId()),"ss.platform_id", query.getPlatformId());
        queryWrapper.eq(StrUtil.isNotNull(query.getAdminId()) && !Number.LONG_ZERO.equals(query.getAdminId()),"ss.admin_id", query.getAdminId());
        queryWrapper.like(StrUtil.isNotNull(query.getShopName()),"ss.shop_name", query.getShopName());
        queryWrapper.like(StrUtil.isNotNull(query.getShopCode()),"ss.shop_code", query.getShopCode());
        queryWrapper.orderByAsc("ss.id");
        return baseMapper.findSellerShopPageList(iPage, queryWrapper);
    }

    @Override
    public SellerShop findSellerShopByPlatformIdAndShopName(Long platformId, String shopName, Long orgId) {
        QueryWrapper<SellerShop> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("shop_name", shopName);
        queryWrapper.eq("platform_id", platformId);
        queryWrapper.eq("org_id", orgId);
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSellerShop(SellerShop sellerShop) {
        sellerShop.setCreateTime(new Date());
        sellerShop.setStatus(Status.STATUS_ACTIVITY);
        sellerShop.setIsBuy(Status.STATUS_NO);
        save(sellerShop);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSellerShop(SellerShop sellerShop) {
        if (StrUtil.isNull(sellerShop.getId())) {
            throw BaseException.base("更新店铺失败,id为空");
        }
        updateById(sellerShop);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteSellerShop(Long id, Long oprId) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("删除店铺失败,id为空");
        }
        UpdateWrapper<SellerShop> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.set("opr_id", oprId);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteSellerShops(String ids, Long oprId) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("批量删除店铺失败,ids为空");
        }
    }

    @Override
    public List<SellerShop> findSellerShopByOrgId(Long orgId) {
        QueryWrapper<SellerShop> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId), "org_id", orgId);
        queryWrapper.select("id","shop_name");
        return list(queryWrapper);
    }
}
