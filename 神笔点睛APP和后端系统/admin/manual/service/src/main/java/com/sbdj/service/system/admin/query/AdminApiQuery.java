package com.sbdj.service.system.admin.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 管理员记录Query
 * </p>
 *
 * @author Yly
 * @since 2019/11/24 19:42
 */
@ApiModel(value = "管理员Query", description = "管理员Query")
public class AdminApiQuery {
    @ApiModelProperty(value = "名字")
    private String name;
    @ApiModelProperty(value = "所属机构")
    private Long orgId;
    @ApiModelProperty(value = "开始时间")
    private String beginTime;
    @ApiModelProperty(value = "结束时间")
    private String endTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
