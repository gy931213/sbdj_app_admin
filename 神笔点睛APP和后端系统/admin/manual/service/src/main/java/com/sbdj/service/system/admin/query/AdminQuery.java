package com.sbdj.service.system.admin.query;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 管理员Query
 * </p>
 *
 * @author Yly
 * @since 2019/11/24 20:38
 */
@Api(tags = "管理员Query")
public class AdminQuery {
	@ApiModelProperty(value = "关键子")
	private String keyword;
	@ApiModelProperty(value = "所属机构")
	private Long orgId;
	@ApiModelProperty(value = "状态")
	private String status;
	@ApiModelProperty(value = "用户类型: {0:全部}，{1：管理员}，{2：商家}")
	private Integer type;

	public String getKeyword() {
		return keyword;
	}

	public Long getOrgId() {
		return orgId;
	}

	public String getStatus() {
		return status;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
