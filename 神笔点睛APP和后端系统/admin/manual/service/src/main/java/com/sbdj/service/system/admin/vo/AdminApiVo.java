package com.sbdj.service.system.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * <p>
 * 管理员记录Vo
 * </p>
 *
 * @author Yly
 * @since 2019/11/24 19:40
 */
@ApiModel(value = "管理员记录Vo", description = "管理员记录Vo")
public class AdminApiVo {
    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "关联管理员ID")
    private Long adminId;
    @ApiModelProperty(value = "调用次数")
    private Integer count;
    @ApiModelProperty(value = "调用日期")
    private Date createTime;
    @ApiModelProperty(value = "名字")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
