package com.sbdj.service.system.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * <p>
 * 管理员信息Vo
 * </p>
 *
 * @author Yly
 * @since 2019-11-25
 */
@ApiModel(value = "管理员信息Vo", description = "管理员信息")
public class AdminInfoVo {
    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "组织id")
    private Long orgId;
    @ApiModelProperty(value = "组织名称")
    private String orgName;
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "手机号码")
    private String mobile;
    @ApiModelProperty(value = "性别")
    private String gender;
    @ApiModelProperty(value = "管理员代码")
    private String code;
    @ApiModelProperty(value = "登录的ip")
    private String ip;
    @ApiModelProperty(value = "状态")
    private String status;
    @ApiModelProperty(value = "操作时间")
    private Date oprTime;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @ApiModelProperty(value = "管理员类型{0：管理员，1：商家}")
    private Integer type;
    @ApiModelProperty(value = "最后登录时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date lastLoginTime;
    @ApiModelProperty(value = "是否系统管理员")
    private String isSuper;
    @ApiModelProperty(value = "头像url")
    private String headUrl;
    @ApiModelProperty(value = "昵称/代号")
    private String anotherName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getOprTime() {
        return oprTime;
    }

    public void setOprTime(Date oprTime) {
        this.oprTime = oprTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getIsSuper() {
        return isSuper;
    }

    public void setIsSuper(String isSuper) {
        this.isSuper = isSuper;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public String getAnotherName() {
        return anotherName;
    }

    public void setAnotherName(String anotherName) {
        this.anotherName = anotherName;
    }

    @Override
    public String toString() {
        return "AdminInfoVo{" +
                "id=" + id +
                ", orgId=" + orgId +
                ", orgName='" + orgName + '\'' +
                ", name='" + name + '\'' +
                ", mobile='" + mobile + '\'' +
                ", gender='" + gender + '\'' +
                ", code='" + code + '\'' +
                ", ip='" + ip + '\'' +
                ", status='" + status + '\'' +
                ", oprTime=" + oprTime +
                ", createTime=" + createTime +
                ", type=" + type +
                ", lastLoginTime=" + lastLoginTime +
                ", isSuper='" + isSuper + '\'' +
                ", headUrl='" + headUrl + '\'' +
                ", anotherName='" + anotherName + '\'' +
                '}';
    }
}
