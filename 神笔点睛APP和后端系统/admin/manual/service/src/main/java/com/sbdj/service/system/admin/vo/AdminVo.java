package com.sbdj.service.system.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * <p>
 * 管理员Vo
 * </p>
 *
 * @author Yly
 * @since 2019/11/24 20:32
 */
@ApiModel(value = "管理员Vo", description = "管理员Vo")
public class AdminVo {
	@ApiModelProperty(value = "id")
	private Long id;
	@ApiModelProperty(value = "组织架构id")
	private Long orgId;
	@ApiModelProperty(value = "组织名称")
	private String orgName;
	@ApiModelProperty(value = "名称")
	private String name;
	@ApiModelProperty(value = "手机号码")
	private String mobile;
	@ApiModelProperty(value = "密码")
	private String pwd;
	@ApiModelProperty(value = "性别")
	private String gender;
	@ApiModelProperty(value = "管理员代码")
	private String code;
	@ApiModelProperty(value = "登录的ip")
	private String ip;
	@ApiModelProperty(value = "状态")
	private String status;
	@ApiModelProperty(value = "操作时间")
	private Date oprTime;
	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;
	@ApiModelProperty(value = "管理员类型{0：管理员，1：商家}")
	private Integer type;
	@ApiModelProperty(value = "最后登录时间")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date lastLoginTime;
	@ApiModelProperty(value = "是否系统管理员")
	private String isSuper;
	@ApiModelProperty(value = "管理员所属的角色id")
	private String roleIds;
	@ApiModelProperty(value = "银行卡ids")
	private String bankIds;
	@ApiModelProperty(value = "所属银行卡名称")
	private String bankName;
	@ApiModelProperty(value = "头像url")
	private String headUrl;
	@ApiModelProperty(value = "推荐人")
	private String referrer;
	@ApiModelProperty(value = "昵称/代号")
	private String anotherName;

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public Long getId() {
		return id;
	}

	public Long getOrgId() {
		return orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public String getName() {
		return name;
	}

	public String getMobile() {
		return mobile;
	}

	public String getPwd() {
		return pwd;
	}

	public String getGender() {
		return gender;
	}

	public String getCode() {
		return code;
	}

	public String getIp() {
		return ip;
	}

	public String getStatus() {
		return status;
	}

	public Date getOprTime() {
		return oprTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public Integer getType() {
		return type;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setOprTime(Date oprTime) {
		this.oprTime = oprTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}

	public String getIsSuper() {
		return isSuper;
	}

	public void setIsSuper(String isSuper) {
		this.isSuper = isSuper;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankIds() {
		return bankIds;
	}

	public void setBankIds(String bankIds) {
		this.bankIds = bankIds;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

	public String getAnotherName() {
		return anotherName;
	}

	public void setAnotherName(String anotherName) {
		this.anotherName = anotherName;
	}
}
