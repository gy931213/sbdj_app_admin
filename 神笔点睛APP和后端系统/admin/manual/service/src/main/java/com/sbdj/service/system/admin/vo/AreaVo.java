package com.sbdj.service.system.admin.vo;

/**
 * @ClassName: Area
 * @Description: 省市区扩展类
 * @author: 黄天良
 * @date: 2018年11月27日 上午11:40:03
 */
public class AreaVo {
    /**
     * 地区名称
     **/
    private String name;
    /**
     * 数量
     **/
    private Long value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }


}
