package com.sbdj.service.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 管理员表
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@ApiModel(value="SysAdmin对象", description="管理员表")
public class SysAdmin implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "管理员id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "组织架构id/sys_organization")
    private Long orgId;

    @ApiModelProperty(value = "管理员代码")
    private String code;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "管理员头像")
    private String haedUrl;

    @ApiModelProperty(value = "管理员账号")
    private String mobile;

    @ApiModelProperty(value = "管理员名称")
    private String name;

    @ApiModelProperty(value = "管理员登录密码")
    private String pwd;

    @ApiModelProperty(value = "管理员状态{A：活动状态，L：锁住/禁用，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "关键字")
    private String wbKey;

    @ApiModelProperty(value = "登录的ip")
    private String ip;

    @ApiModelProperty(value = "创建人id")
    private Long createrId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "推荐人")
    private String referrer;

    @ApiModelProperty(value = "操作时间")
    private Date oprTime;

    @ApiModelProperty(value = "操作人id")
    private Long oprId;

    @ApiModelProperty(value = "管理员类型{1：管理员，2：商家}")
    private Integer type;

    @ApiModelProperty(value = "所属角色id，使用逗号隔开1,2,3,4......n")
    private String roleIds;

    @ApiModelProperty(value = "最后登录时间")
    private Date lastLoginTime;

    @ApiModelProperty(value = "是否为超级管理员，否：0/是：1")
    private String isSuper;

    @ApiModelProperty(value = "别名")
    private String anotherName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHaedUrl() {
        return haedUrl;
    }

    public void setHaedUrl(String haedUrl) {
        this.haedUrl = haedUrl;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWbKey() {
        return wbKey;
    }

    public void setWbKey(String wbKey) {
        this.wbKey = wbKey;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getCreaterId() {
        return createrId;
    }

    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public Date getOprTime() {
        return oprTime;
    }

    public void setOprTime(Date oprTime) {
        this.oprTime = oprTime;
    }

    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getIsSuper() {
        return isSuper;
    }

    public void setIsSuper(String isSuper) {
        this.isSuper = isSuper;
    }

    public String getAnotherName() {
        return anotherName;
    }

    public void setAnotherName(String anotherName) {
        this.anotherName = anotherName;
    }

    @Override
    public String toString() {
        return "SysAdmin{" +
                "id=" + id +
                ", orgId=" + orgId +
                ", code='" + code + '\'' +
                ", gender='" + gender + '\'' +
                ", haedUrl='" + haedUrl + '\'' +
                ", mobile='" + mobile + '\'' +
                ", name='" + name + '\'' +
                ", pwd='" + pwd + '\'' +
                ", status='" + status + '\'' +
                ", wbKey='" + wbKey + '\'' +
                ", ip='" + ip + '\'' +
                ", createrId=" + createrId +
                ", createTime=" + createTime +
                ", referrer='" + referrer + '\'' +
                ", oprTime=" + oprTime +
                ", oprId=" + oprId +
                ", type=" + type +
                ", roleIds='" + roleIds + '\'' +
                ", lastLoginTime=" + lastLoginTime +
                ", isSuper='" + isSuper + '\'' +
                ", anotherName='" + anotherName + '\'' +
                '}';
    }
}
