package com.sbdj.service.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 管理员记录
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="SysAdminApi对象", description="管理员记录")
public class SysAdminApi implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "管理员")
    private Long adminId;

    @ApiModelProperty(value = "调用次数")
    private Integer count;

    @ApiModelProperty(value = "调用时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "SysAdminInfo{" +
            "id=" + id +
            ", adminId=" + adminId +
            ", count=" + count +
            ", createTime=" + createTime +
        "}";
    }
}
