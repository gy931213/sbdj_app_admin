package com.sbdj.service.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 管理员[商家]-对应的银行开表
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@ApiModel(value="SysAdminBank对象", description="管理员[商家]-对应的银行开表")
public class SysAdminBank implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "管理员id（商家号）")
    private Long adminId;

    @ApiModelProperty(value = "所属银行卡")
    private Long bankId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "SysAdminBank{" +
                "id=" + id +
                ", adminId=" + adminId +
                ", bankId=" + bankId +
                ", createTime=" + createTime +
                '}';
    }
}
