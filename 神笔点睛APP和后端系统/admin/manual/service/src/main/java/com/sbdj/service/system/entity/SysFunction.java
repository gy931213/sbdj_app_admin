package com.sbdj.service.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 功能表
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@ApiModel(value="SysFunction对象", description="功能表")
public class SysFunction implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "功能id：自增长")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "功能名称")
    private String name;

    @ApiModelProperty(value = "功能代码")
    private String code;

    @ApiModelProperty(value = "功能路径")
    private String path;

    @ApiModelProperty(value = "功能图标")
    private String icon;

    @ApiModelProperty(value = "功能请求url地址")
    private String url;

    @ApiModelProperty(value = "功能类型\\1：目录\\2：菜单\\3：按钮")
    private String type;

    @ApiModelProperty(value = "功能的唯一标识")
    private String authority;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "操作时间")
    private Date oprTime;

    @ApiModelProperty(value = "创建人id")
    private Long createId;

    @ApiModelProperty(value = "操作人id")
    private Long oprId;

    @ApiModelProperty(value = "父级id")
    private Long pid;

    @ApiModelProperty(value = "功能状态 {A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "排序 {1,2,3.......n}")
    private Integer sort;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getOprTime() {
        return oprTime;
    }

    public void setOprTime(Date oprTime) {
        this.oprTime = oprTime;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "SysFunction{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", path='" + path + '\'' +
                ", icon='" + icon + '\'' +
                ", url='" + url + '\'' +
                ", type='" + type + '\'' +
                ", authority='" + authority + '\'' +
                ", createTime=" + createTime +
                ", oprTime=" + oprTime +
                ", createId=" + createId +
                ", oprId=" + oprId +
                ", pid=" + pid +
                ", status='" + status + '\'' +
                ", sort=" + sort +
                '}';
    }
}
