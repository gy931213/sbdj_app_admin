package com.sbdj.service.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 单号的生成策略表
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@ApiModel(value="SysNumber对象", description="单号的生成策略表")
public class SysNumber implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id 自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "单号编码")
    private String code;

    @ApiModelProperty(value = "单号名称")
    private String name;

    @ApiModelProperty(value = "单号前缀字符")
    private String prefix;

    @ApiModelProperty(value = "单号长度")
    private Integer length;

    @ApiModelProperty(value = "单号号递增值")
    private Integer addValue;

    @ApiModelProperty(value = "单号最大递增值")
    private Integer maxValue;

    @ApiModelProperty(value = "单号长度不够最后填充的值")
    private String lastValue;

    @ApiModelProperty(value = "最后生成单号的时间")
    private Date lastTime;

    @ApiModelProperty(value = "最后生成单号的流水号")
    private String lastNumber;

    @ApiModelProperty(value = "单号策略创建时间")
    private Date createTime;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getAddValue() {
        return addValue;
    }

    public void setAddValue(Integer addValue) {
        this.addValue = addValue;
    }

    public Integer getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Integer maxValue) {
        this.maxValue = maxValue;
    }

    public String getLastValue() {
        return lastValue;
    }

    public void setLastValue(String lastValue) {
        this.lastValue = lastValue;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    public String getLastNumber() {
        return lastNumber;
    }

    public void setLastNumber(String lastNumber) {
        this.lastNumber = lastNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SysNumber{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", prefix='" + prefix + '\'' +
                ", length=" + length +
                ", addValue=" + addValue +
                ", maxValue=" + maxValue +
                ", lastValue='" + lastValue + '\'' +
                ", lastTime=" + lastTime +
                ", lastNumber='" + lastNumber + '\'' +
                ", createTime=" + createTime +
                ", status='" + status + '\'' +
                '}';
    }
}
