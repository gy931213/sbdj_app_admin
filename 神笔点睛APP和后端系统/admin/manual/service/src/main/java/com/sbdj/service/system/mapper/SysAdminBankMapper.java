package com.sbdj.service.system.mapper;

import com.sbdj.service.system.entity.SysAdminBank;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员[商家]-对应的银行开表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysAdminBankMapper extends BaseMapper<SysAdminBank> {

}
