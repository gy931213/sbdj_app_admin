package com.sbdj.service.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.system.admin.query.AdminApiQuery;
import com.sbdj.service.system.admin.vo.AdminApiVo;
import com.sbdj.service.system.entity.SysAdminApi;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 管理员记录 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SysAdminInfoMapper extends BaseMapper<SysAdminApi> {
    /**
     * 管理员记录分页展示
     * @author Yly
     * @date 2019/11/24 19:44
     * @param query 查询参数
     * @param iPage 分页插件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.system.admin.vo.AdminInfoVo>
     */
    IPage<AdminApiVo> findAdminApiPageByParams(@Param("page") IPage<AdminApiVo> iPage, @Param(Constants.WRAPPER) Wrapper<AdminApiQuery> query);
}
