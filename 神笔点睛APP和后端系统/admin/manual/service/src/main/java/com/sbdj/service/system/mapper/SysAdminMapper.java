package com.sbdj.service.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.system.admin.query.AdminQuery;
import com.sbdj.service.system.admin.vo.AdminInfoVo;
import com.sbdj.service.system.admin.vo.AdminVo;
import com.sbdj.service.system.entity.SysAdmin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 管理员表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysAdminMapper extends BaseMapper<SysAdmin> {
    /**
     * 获取管理员分页数据
     * @author Yly
     * @date 2019/11/24 20:40
     * @param query 查询参数
     * @param iPage 分页插件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.system.admin.vo.AdminVo>
     */
    IPage<AdminVo> findAdminPageList(@Param("page") IPage<AdminVo> iPage, @Param(Constants.WRAPPER) Wrapper<AdminQuery> query);

    /**
     * 通过管理员id获取管理员信息
     * @author Yly
     * @date 2019-11-25 13:38
     * @param adminId 管理员id
     * @return com.sbdj.service.system.admin.vo.AdminInfoVo
     */
    AdminInfoVo findAdminInfoVoByAdminId(@Param("adminId") Long adminId);
}
