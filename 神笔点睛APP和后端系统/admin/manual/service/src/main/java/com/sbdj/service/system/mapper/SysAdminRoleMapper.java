package com.sbdj.service.system.mapper;

import com.sbdj.service.system.entity.SysAdminRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 管理员和角色关系表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysAdminRoleMapper extends BaseMapper<SysAdminRole> {
    /**
     * 根据管理员id获取对应的角色id接口
     * @author Yly
     * @date 2019/11/21 10:17
     * @param adminId 管理员id
     * @return java.util.List<java.lang.Long>
     */
    List<Long> getAdminRoleByAdminId(@Param("adminId") Long adminId);
}
