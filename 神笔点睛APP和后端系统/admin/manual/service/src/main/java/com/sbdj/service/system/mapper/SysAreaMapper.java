package com.sbdj.service.system.mapper;

import com.sbdj.service.system.entity.SysArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 省市区表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysAreaMapper extends BaseMapper<SysArea> {

}
