package com.sbdj.service.system.mapper;

import com.sbdj.service.system.entity.SysConfigDtl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 系统配置详情表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysConfigDtlMapper extends BaseMapper<SysConfigDtl> {

    /**
     * 根据任务类型查询配置信息
     * @param code
     */
    List<SysConfigDtl> findConfigDtl(String code);
}
