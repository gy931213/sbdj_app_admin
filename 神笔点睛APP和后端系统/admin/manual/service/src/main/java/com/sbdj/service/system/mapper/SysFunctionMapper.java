package com.sbdj.service.system.mapper;

import com.sbdj.service.system.entity.SysFunction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 功能表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysFunctionMapper extends BaseMapper<SysFunction> {
    /**
     * 获取所属机构的功能菜单数据
     * @author Yly
     * @date 2019/11/23 15:57
     * @param orgId 所属机构
     * @return java.util.List<com.sbdj.service.system.entity.SysFunction>
     */
    List<SysFunction> findFunctionList(@Param("orgId") Long orgId);

    /**
     * 根据角色ID获取对应的权限
     * @author Yly
     * @date 2019/11/23 16:22
     * @param roleId 角色ID
     * @return java.util.List<java.lang.Long>
     */
    List<Long> getRoleFunctionByRoleId(@Param("roleId") Long roleId);

    /**
     * 通过角色id获取对应的权限
     * @author Yly
     * @date 2019/11/23 20:19
     * @param roleId 角色id
     * @return java.util.List<java.lang.Long>
     */
    List<Long> findFunctionListByRoleId(@Param("roleId") Long roleId);
}
