package com.sbdj.service.system.mapper;

import com.sbdj.service.system.entity.SysMapTrajectory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface SysMapTrajectoryMapper extends BaseMapper<SysMapTrajectory> {

}
