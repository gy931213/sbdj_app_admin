package com.sbdj.service.system.mapper;

import com.sbdj.service.system.entity.SysNumber;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 单号的生成策略表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysNumberMapper extends BaseMapper<SysNumber> {

    /**
     * @param code 要锁的数据code
     * @return void 返回类型
     * @Title: updateSysNumberLock
     * @Description: 锁行的接口
     */
    @Transactional
    SysNumber sysNumberLock(@Param("code") String code);

    SysNumber findOne(@Param("status") String status,@Param("code") String code);

    @Transactional
    void updateSysNumber(@Param("id") Long id,@Param("addvalue") Integer addValue,@Param("lastNumber") String lastNumber);
}
