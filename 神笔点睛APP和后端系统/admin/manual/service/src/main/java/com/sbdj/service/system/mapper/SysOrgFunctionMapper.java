package com.sbdj.service.system.mapper;

import com.sbdj.service.system.entity.SysOrgFunction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统组织权限 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysOrgFunctionMapper extends BaseMapper<SysOrgFunction> {

}
