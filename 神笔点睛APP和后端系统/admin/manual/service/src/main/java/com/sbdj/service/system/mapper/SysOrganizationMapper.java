package com.sbdj.service.system.mapper;

import com.sbdj.service.system.entity.SysOrganization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 组织表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysOrganizationMapper extends BaseMapper<SysOrganization> {

}
