package com.sbdj.service.system.mapper;

import com.sbdj.service.system.entity.SysFunction;
import com.sbdj.service.system.entity.SysRoleFunction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色功能关系表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysRoleFunctionMapper extends BaseMapper<SysRoleFunction> {
    /**
     * 根据角色id获取角色对应的权限id
     * @author Yly
     * @date 2019/11/21 10:34
     * @param roleIds 角色id
     * @return java.util.List<java.lang.Long>
     */
    List<Long> getRoleFunctionByRoleIds(@Param("roleIds") String roleIds);

    /**
     * 批量物理删除数据
     * @author Yly
     * @date 2019/11/24 11:39
     * @param roleIds 角色ids
     * @return void
     */
    void deleteRoleFunctionByRoleIds(@Param("roleIds") String roleIds);
}
