package com.sbdj.service.system.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.system.admin.vo.SysRoleVo;
import com.sbdj.service.system.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 角色表
 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {
    /**
     * 获取角色分页数据
     * @author Yly
     * @date 2019/11/24 11:11
     * @param page 分页插件
     * @param keyword 关键词
     * @param orgId 机构号
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.system.admin.vo.SysRoleVo>
     */
    IPage<SysRoleVo> findRolePage(@Param("page") IPage<SysRoleVo> page, @Param("keyword") String keyword,
                                  @Param("orgId") Long orgId);
}
