package com.sbdj.service.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.system.admin.query.AdminApiQuery;
import com.sbdj.service.system.admin.vo.AdminApiVo;
import com.sbdj.service.system.entity.SysAdminApi;

/**
 * <p>
 * 管理员记录 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISysAdminApiService extends IService<SysAdminApi> {
    /**
     * 管理员记录分页展示
     * @author Yly
     * @date 2019/11/24 19:44
     * @param query 查询参数
     * @param iPage 分页插件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.system.admin.vo.AdminInfoVo>
     */
    IPage<AdminApiVo> findAdminApiPageByParams(AdminApiQuery query, IPage<AdminApiVo> iPage);

    /**
     * 记录管理员的调用次数
     * @author Yly
     * @date 2019-12-11 15:58
     * @param adminId 管理员ID
     * @return void
     */
    void recordAdminInfo(Long adminId);
}
