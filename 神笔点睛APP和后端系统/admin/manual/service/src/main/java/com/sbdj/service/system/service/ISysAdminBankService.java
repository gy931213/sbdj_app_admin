package com.sbdj.service.system.service;

import com.sbdj.service.system.entity.SysAdminBank;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员[商家]-对应的银行开表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysAdminBankService extends IService<SysAdminBank> {

}
