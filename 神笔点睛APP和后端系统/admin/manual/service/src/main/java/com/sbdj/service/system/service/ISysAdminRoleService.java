package com.sbdj.service.system.service;

import com.sbdj.service.system.entity.SysAdminRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 管理员和角色关系表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysAdminRoleService extends IService<SysAdminRole> {
    /**
     * 根据管理员id获取对应的角色id接口
     * @author Yly
     * @date 2019/11/21 10:15
     * @param adminId 管理员id
     * @return java.util.List<java.lang.Long>
     */
    List<Long> getAdminRoleByAdminId(Long adminId);

    /**
     * 通过管理员id获取对应的角色
     * @author Yly
     * @date 2019-11-25 10:10
     * @param adminId 管理员id
     * @return java.util.List<com.sbdj.service.system.entity.SysAdminRole>
     */
    List<SysAdminRole> findAdminRoleByAdminId(Long adminId);

    /**
     * 保存管理员对应的角色
     * @author Yly
     * @date 2019-11-25 10:11
     * @param adminId 管理员id
     * @param roleIds 角色ids
     * @return void
     */
    void saveAdminRole(Long adminId,String roleIds);
}
