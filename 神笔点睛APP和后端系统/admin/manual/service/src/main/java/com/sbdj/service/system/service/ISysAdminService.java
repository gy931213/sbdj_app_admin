package com.sbdj.service.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.system.admin.query.AdminQuery;
import com.sbdj.service.system.admin.vo.AdminInfoVo;
import com.sbdj.service.system.admin.vo.AdminVo;
import com.sbdj.service.system.entity.SysAdmin;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 管理员表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysAdminService extends IService<SysAdmin> {
    /**
     * 通过手机号获取管理员
     * @author Yly
     * @date 2019/11/21 9:51
     * @param mobile 手机号
     * @return com.sbdj.service.system.entity.SysAdmin
     */
    SysAdmin findSysAdminByMobile(String mobile);

    /**
     * 通过手机号与密码获取管理员信息
     * @author Yly
     * @date 2019/11/21 9:55
     * @param mobile 手机号
     * @param pwd 密码
     * @return com.sbdj.service.system.entity.SysAdmin
     */
    SysAdmin findSysAdminByMobileAndPwd(String mobile, String pwd);

    /**
     * 获取管理员分页数据
     * @author Yly
     * @date 2019/11/24 20:40
     * @param query 查询参数
     * @param iPage 分页插件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.system.admin.vo.AdminVo>
     */
    IPage<AdminVo> findAdminPageList(AdminQuery query, IPage<AdminVo> iPage);

    /**
     * 保存管理员
     * @author Yly
     * @date 2019-11-25 13:06
     * @param admin 管理员
     * @return void
     */
    void saveAdmin(SysAdmin admin);

    /**
     * 获取对应机构下的管理员
     * @author Yly
     * @date 2019-11-25 13:06
     * @param orgId 机构id
     * @return java.util.List<com.sbdj.service.system.entity.SysAdmin>
     */
    List<SysAdmin> findAdminSelect(Long orgId);

    /**
     * 通过类型与机构id获取数据
     * @author Yly
     * @date 2019-11-25 19:57
     * @param orgId 机构id
     * @return java.util.List<com.sbdj.service.system.entity.SysAdmin>
     */
    List<SysAdmin> findAdminByTypeAndOrgId(Long orgId);

    /**
     * 修改管理员头像
     * @author Yly
     * @date 2019-11-25 13:21
     * @param id 管理员id
     * @param headUrl 头像地址
     * @return void
     */
    void updateAdminHeadUrlById(Long id,String headUrl);

    /**
     * 通过管理员id获取管理员信息
     * @author Yly
     * @date 2019-11-25 13:38
     * @param adminId 管理员id
     * @return com.sbdj.service.system.admin.vo.AdminInfoVo
     */
    AdminInfoVo findAdminInfoVoByAdminId(Long adminId);

    /**
     * 修改管理员的状态
     * @author Yly
     * @date 2019-11-25 14:52
     * @param id 管理员id
     * @param status 状态
     * @param oprId 操作人
     * @return void
     */
    void updateStatus(Long id,String status,Long oprId);

    /**
     * 更新管理员数据
     * @author Yly
     * @date 2019-11-25 14:55
     * @param admin 管理员
     * @return void
     */
    void updateAdmin(SysAdmin admin);

    /**
     * 逻辑删除管理员
     * @author Yly
     * @date 2019-11-25 15:08
     * @param id 管理员id
     * @param oprId 操作人id
     * @return void
     */
    void logicDeleteAdmin(Long id,Long oprId);

    /**
     * 批量逻辑删除管理员
     * @author Yly
     * @date 2019-11-25 15:10
     * @param ids 管理员ids
     * @param oprId 操作人id
     * @return void
     */
    void logicDeleteAdmins(String ids,Long oprId);
}
