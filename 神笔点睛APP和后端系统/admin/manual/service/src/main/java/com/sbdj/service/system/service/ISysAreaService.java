package com.sbdj.service.system.service;

import com.sbdj.service.system.entity.SysArea;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 省市区表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysAreaService extends IService<SysArea> {
    /**
     * 通过父级id获取省市区数据
     * @author Yly
     * @date 2019/11/24 18:55
     * @param parentId 父级id
     * @return java.util.List<com.sbdj.service.system.entity.SysArea>
     */
    List<SysArea> findAreaByParentId(Long parentId);
}
