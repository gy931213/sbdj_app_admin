package com.sbdj.service.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.system.entity.SysConfigDtl;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 系统配置详情表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysConfigDtlService extends IService<SysConfigDtl> {
    /**
     * 通过系统配置id获取系统详细配置
     * @author Yly
     * @date 2019-11-25 17:22
     * @param configId 系统配置id
     * @param iPage 分页组件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.system.entity.SysConfigDtl>
     */
    IPage<SysConfigDtl> findConfigDtlList(Long configId, IPage<SysConfigDtl> iPage);

    /**
     * 通过系统配置id获取详细系统配置列表
     * @author Yly
     * @date 2019-11-25 17:30
     * @param configId 系统配置id
     * @return java.util.List<com.sbdj.service.system.entity.SysConfigDtl>
     */
    List<SysConfigDtl> findConfigDtl(Long configId);

    /**
     * 保存系统详细配置
     * @author Yly
     * @date 2019-11-25 17:48
     * @param configDtl 系统详细配置
     * @return void
     */
    void saveConfigDtl(SysConfigDtl configDtl);

    /**
     * 更新系统详细配置
     * @author Yly
     * @date 2019-11-27 21:14
     * @param configDtl 系统详细配置
     * @return void
     */
    void updateConfigDtl(SysConfigDtl configDtl);

    /**
     * 逻辑删除系统详细配置
     * @author Yly
     * @date 2019-11-25 17:58
     * @param id 系统详细配置id
     * @return void
     */
    void logicDeleteConfigDtl(Long id);

    /**
     * 批量逻辑删除系统详细配置
     * @author Yly
     * @date 2019-11-25 18:00
     * @param ids 系统详细配置ids
     * @return void
     */
    void logicDeleteConfigDtls(String ids);

    /**
     * 通过编码获取系统详细配置
     * @author Yly
     * @date 2019-11-27 14:40
     * @param code 编码
     * @return java.util.List<com.sbdj.service.system.entity.SysConfigDtl>
     */
    List<SysConfigDtl> findConfigDtl(String code);
}
