package com.sbdj.service.system.service;

import com.sbdj.service.system.entity.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 系统配置表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysConfigService extends IService<SysConfig> {
    /**
     * 获取系统配置列表
     * @author Yly
     * @date 2019-11-25 15:37
     * @return java.util.List<com.sbdj.service.system.entity.SysConfig>
     */
    List<SysConfig> findSysConfigList();

    /**
     * 保存系统配置
     * @author Yly
     * @date 2019-11-25 17:05
     * @param config 系统配置
     * @return void
     */
    void saveConfig(SysConfig config);

    /**
     * 更新系统配置
     * @author Yly
     * @date 2019-11-27 21:03
     * @param config 系统配置
     * @return void
     */
    void updateConfig(SysConfig config);

    /**
     * 逻辑删除系统配置
     * @author Yly
     * @date 2019-11-25 17:13
     * @param id 系统配置id
     * @return void
     */
    void logicDeleteConfig(Long id);

    /**
     * 批量删除系统配置
     * @author Yly
     * @date 2019-11-25 17:14
     * @param ids 系统配置ids
     * @return void
     */
    void logicDeleteConfigs(String ids);
}
