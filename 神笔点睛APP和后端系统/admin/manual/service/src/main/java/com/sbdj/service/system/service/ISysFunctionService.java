package com.sbdj.service.system.service;

import com.sbdj.service.system.entity.SysFunction;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 功能表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysFunctionService extends IService<SysFunction> {
    /**
     * 通过管理员ID获取功能列表
     * @author Yly
     * @date 2019/11/21 9:59
     * @param adminId 管理员ID
     * @return java.util.List<com.sbdj.service.system.entity.SysFunction>
     */
    List<SysFunction> findFunctionByAdminId(Long adminId);

    /**
     * 获取功能菜单列表数据
     * @author Yly
     * @date 2019/11/23 15:51
     * @return java.util.List<com.sbdj.service.system.entity.SysFunction>
     */
    List<SysFunction> findFunctionPageList();

    /**
     * 获取所属机构的功能菜单数据
     * @author Yly
     * @date 2019/11/23 15:57
     * @param orgId 所属机构
     * @return java.util.List<com.sbdj.service.system.entity.SysFunction>
     */
    List<SysFunction> findFunctionList(Long orgId);

    /**
     * 保存功能菜单
     * @author Yly
     * @date 2019/11/23 16:02
     * @param function 功能菜单
     * @return void
     */
    void saveFunction(SysFunction function);

    /**
     * 更新功能菜单
     * @author Yly
     * @date 2019/11/23 16:11
     * @param function 功能菜单
     * @return void
     */
    void updateFunction(SysFunction function);

    /**
     * 根据角色ID获取对应的权限
     * @author Yly
     * @date 2019/11/23 16:22
     * @param roleId 角色ID
     * @return java.util.List<java.lang.Long>
     */
    List<Long> getRoleFunctionByRoleId(Long roleId);

    /**
     * 通过id获取权限集合
     * @author Yly
     * @date 2019/11/23 16:27
     * @param ids id集合
     * @return java.util.List<com.sbdj.service.system.entity.SysFunction>
     */
    List<SysFunction> findFunctionByIds(List<Long> ids);

    /**
     * 通过组织id更新权限
     * @author Yly
     * @date 2019/11/23 20:01
     * @param orgId 组织id
     * @return void
     */
    void updateAuthorizeByOrgId(Long orgId);

    /**
     * 获取该角色的所有权限id
     * @author Yly
     * @date 2019/11/23 20:11
     * @param roleId 角色id
     * @return java.util.List<java.lang.Long>
     */
    List<Long> findFunctionListByRoleId(Long roleId);

    /**
     * 批量删除权限
     * @author Yly
     * @date 2019/11/24 16:58
     * @param ids 权限ids
     * @return void
     */
    void deleteFunctionByIds(String ids);
}
