package com.sbdj.service.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.system.entity.SysMapTrajectory;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ISysMapTrajectoryService extends IService<SysMapTrajectory> {
    /**
     * 保存定位坐标
     * @author Yly
     * @date 2019/11/22 22:28
     * @param sysMapTrajectory 定位坐标
     * @return void
     */
    void saveSysMapTrajectory(SysMapTrajectory sysMapTrajectory);



    /**
     * @title: countMapTrajectoryByTargetId
     * @description: 获取目标ID的坐标条数
     * @Param targetId 目标ID
     * @author: Yly
     * @return: java.lang.Integer
     * @date: 2019-07-16 15:34
     **/
    Integer countMapTrajectoryByTargetId(Long targetId);
}
