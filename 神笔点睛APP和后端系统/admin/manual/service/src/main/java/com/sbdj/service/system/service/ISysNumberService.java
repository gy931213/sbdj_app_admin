package com.sbdj.service.system.service;

import com.sbdj.service.system.entity.SysNumber;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 单号的生成策略表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysNumberService extends IService<SysNumber> {


    /**
     * 据员工编码获取到最大编号
     * @author ZC
     * @date 2019-11-25 14:01
     * @param codeJob
     * @return java.lang.String
     */
    String findSysNumberJOBByNumberCoe(String codeJob);





    /**
     * 获取到当前锁住的数据
     * @author ZC
     * @date 2019-11-25 14:01
     * @param code
     * @return com.sbdj.service.system.entity.SysNumber
     */
    SysNumber findOne(String code);


    /**
     * 将最大编号自增1
     * @author ZC
     * @date 2019-11-25 14:01
     * @param maxNumber 最大编号
     * @return void
     */
    void updateSysNumberByParams(SysNumber maxNumber);
}
