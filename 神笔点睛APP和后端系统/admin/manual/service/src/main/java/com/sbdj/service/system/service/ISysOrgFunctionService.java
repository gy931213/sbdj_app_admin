package com.sbdj.service.system.service;

import com.sbdj.service.system.entity.SysOrgFunction;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统组织权限 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysOrgFunctionService extends IService<SysOrgFunction> {

    /**
     * 保存组织权限
     * @author Yly
     * @date 2019/11/23 19:49
     * @param orgId 组织id
     * @param oprId 操作人id
     * @param funcIds 权限id
     * @return void
     */
    void saveOrgFunction(Long orgId, Long oprId, String funcIds);
}
