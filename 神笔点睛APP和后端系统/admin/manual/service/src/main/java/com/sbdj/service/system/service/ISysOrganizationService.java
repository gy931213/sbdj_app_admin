package com.sbdj.service.system.service;

import com.sbdj.service.system.entity.SysOrganization;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 组织表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysOrganizationService extends IService<SysOrganization> {
    /**
     * 获取组织架构
     * @author Yly
     * @date 2019/11/23 19:22
     * @return java.util.List<com.sbdj.service.system.entity.SysOrganization>
     */
    List<SysOrganization> findOrganizationList();

    /**
     * 保存组织
     * @author Yly
     * @date 2019/11/23 19:28
     * @param organization 组织
     * @return void
     */
    void saveOrganization(SysOrganization organization);

    /**
     * 更新组织
     * @author Yly
     * @date 2019/11/23 19:30
     * @param organization 组织
     * @return void
     */
    void updateOrganization(SysOrganization organization);

    /**
     * 通过id删除组织
     * @author Yly
     * @date 2019/11/23 19:34
     * @param ids id
     * @param oprId 操作人id
     * @return void
     */
    void deleteOrganization(String ids,Long oprId);

    /**
     * 获取组织架构列表(仅超级管理员)
     * @author Yly
     * @date 2019/11/23 19:46
     * @param
     * @return java.util.List<com.sbdj.service.system.entity.SysOrganization>
     */
    List<SysOrganization> findOrganizationSelect();

    /**
     * 通过组织id获取组织架构列表
     * @author Yly
     * @date 2019/11/23 19:41
     * @param orgId 组织id
     * @return java.util.List<com.sbdj.service.system.entity.SysOrganization>
     */
    List<SysOrganization> findOrganizationSelect(Long orgId);
}
