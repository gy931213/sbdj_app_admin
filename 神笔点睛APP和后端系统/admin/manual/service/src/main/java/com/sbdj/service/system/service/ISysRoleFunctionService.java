package com.sbdj.service.system.service;

import com.sbdj.service.system.entity.SysRoleFunction;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色功能关系表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysRoleFunctionService extends IService<SysRoleFunction> {
    /**
     * 根据角色id获取权限
     * @author Yly
     * @date 2019/11/21 12:01
     * @param roleIds 角色id
     * @return java.util.List<java.lang.Long>
     */
    List<Long> getRoleFunctionByRoleIds(String roleIds);

    /**
     * 通过角色id物理删除该角色下的所有权限
     * @author Yly
     * @date 2019/11/23 20:28
     * @param roleId 角色ID
     * @return void
     */
    void deleteRoleFunctionByRoleId(Long roleId);

    /**
     * 保存该角色下的所拥有的权限
     * @author Yly
     * @date 2019/11/23 20:31
     * @param roleId 角色id
     * @param funcIds 权限列表
     * @return void
     */
    void saveRoleFunction(Long roleId, List<Long> funcIds);

    /**
     * 保存该角色下的所拥有的权限
     * @author Yly
     * @date 2019/11/24 11:36
     * @param roleId 角色id
     * @param funcIds 权限id
     * @return void
     */
    void saveRoleFunction(Long roleId, String funcIds);

    /**
     * 批量物理删除数据
     * @author Yly
     * @date 2019/11/24 11:39
     * @param roleIds 角色ids
     * @return void
     */
    void deleteRoleFunctionByRoleIds(String roleIds);
}
