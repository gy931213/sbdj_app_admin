package com.sbdj.service.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sbdj.service.system.admin.vo.SysRoleVo;
import com.sbdj.service.system.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色表
 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
public interface ISysRoleService extends IService<SysRole> {
    /**
     * 通过组织id获取角色
     * @author Yly
     * @date 2019/11/23 20:05
     * @param orgId 组织id
     * @return java.util.List<com.sbdj.service.system.entity.SysRole>
     */
    List<SysRole> findRoleSelect(Long orgId);

    /**
     * 保存角色
     * @author Yly
     * @date 2019/11/23 20:40
     * @param role 角色
     * @return void
     */
    void saveRole(SysRole role);

    /**
     * 更新角色
     * @author Yly
     * @date 2019/11/24 9:29
     * @param role 角色
     * @return void
     */
    void updateRole(SysRole role);

    /**
     * 逻辑删除角色
     * @author Yly
     * @date 2019/11/24 10:11
     * @param id
     * @return void
     */
    void logicDeleteRole(Long id);

    /**
     * 批量逻辑删除角色
     * @author Yly
     * @date 2019/11/24 9:42
     * @param ids 角色id
     * @return void
     */
    void logicDeleteRoles(String ids);

    /**
     * 获取角色的分页数据
     * @author Yly
     * @date 2019/11/24 11:01
     * @param keyword 关键词
     * @param orgId 机构id
     * @param iPage 分页组件
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.system.admin.vo.SysRoleVo>
     */
    IPage<SysRoleVo> findRolePage(String keyword, Long orgId, IPage<SysRoleVo> iPage);
}
