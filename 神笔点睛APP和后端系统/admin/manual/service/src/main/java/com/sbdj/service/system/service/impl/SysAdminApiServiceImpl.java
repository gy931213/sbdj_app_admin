package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.admin.query.AdminApiQuery;
import com.sbdj.service.system.admin.vo.AdminApiVo;
import com.sbdj.service.system.entity.SysAdminApi;
import com.sbdj.service.system.mapper.SysAdminInfoMapper;
import com.sbdj.service.system.service.ISysAdminApiService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>
 * 管理员记录 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SysAdminApiServiceImpl extends ServiceImpl<SysAdminInfoMapper, SysAdminApi> implements ISysAdminApiService {

    @Override
    public IPage<AdminApiVo> findAdminApiPageByParams(AdminApiQuery query, IPage<AdminApiVo> iPage) {
        QueryWrapper<AdminApiQuery> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(query.getName()), "sa.name", query.getName());
        queryWrapper.eq((StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId())), "sa.org_id", query.getOrgId());
        queryWrapper.ge(StrUtil.isNotNull(query.getBeginTime()), "sai.create_time", query.getBeginTime());
        queryWrapper.le(StrUtil.isNotNull(query.getBeginTime()), "sai.create_time", query.getEndTime());
        return baseMapper.findAdminApiPageByParams(iPage, queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void recordAdminInfo(Long adminId) {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        QueryWrapper<SysAdminApi> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("admin_id", adminId);
        queryWrapper.eq("create_time", df.format(date));
        queryWrapper.last("limit 1");
        SysAdminApi sysAdminApi = getOne(queryWrapper);
        // 如果数据不为空
        if (StrUtil.isNotNull(sysAdminApi)) {
            sysAdminApi.setCount(sysAdminApi.getCount()+1);
            updateById(sysAdminApi);
        } else {
            sysAdminApi = new SysAdminApi();
            sysAdminApi.setAdminId(adminId);
            sysAdminApi.setCount(1);
            sysAdminApi.setCreateTime(date);
            save(sysAdminApi);
        }
    }
}
