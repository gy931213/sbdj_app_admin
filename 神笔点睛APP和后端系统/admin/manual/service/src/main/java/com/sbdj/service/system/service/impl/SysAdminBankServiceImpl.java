package com.sbdj.service.system.service.impl;

import com.sbdj.service.system.entity.SysAdminBank;
import com.sbdj.service.system.mapper.SysAdminBankMapper;
import com.sbdj.service.system.service.ISysAdminBankService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员[商家]-对应的银行开表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Service
public class SysAdminBankServiceImpl extends ServiceImpl<SysAdminBankMapper, SysAdminBank> implements ISysAdminBankService {

}
