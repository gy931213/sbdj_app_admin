package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysAdminRole;
import com.sbdj.service.system.mapper.SysAdminRoleMapper;
import com.sbdj.service.system.service.ISysAdminRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 管理员和角色关系表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysAdminRoleServiceImpl extends ServiceImpl<SysAdminRoleMapper, SysAdminRole> implements ISysAdminRoleService {
    @Override
    public List<Long> getAdminRoleByAdminId(Long adminId) {
        if (StrUtil.isNull(adminId)) {
            return null;
        }
        return baseMapper.getAdminRoleByAdminId(adminId);
    }

    @Override
    public List<SysAdminRole> findAdminRoleByAdminId(Long adminId) {
        if (StrUtil.isNull(adminId)) {
            return null;
        }
        QueryWrapper<SysAdminRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("admin_id", adminId);
        return list(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAdminRole(Long adminId, String roleIds) {
        QueryWrapper<SysAdminRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("admin_id", adminId);
        remove(queryWrapper);
        List<Long> roleList = StrUtil.getIdsByLong(roleIds);
        List<SysAdminRole> list = new ArrayList<>();
        SysAdminRole sysAdminRole;
        if (StrUtil.isNull(roleList)) {
            throw BaseException.base("添加管理员角色失败,角色ids为空");
        }
        for (Long id : roleList) {
            sysAdminRole = new SysAdminRole();
            sysAdminRole.setAdminId(adminId);
            sysAdminRole.setRoleId(id);
            sysAdminRole.setCreateTime(new Date());
            sysAdminRole.setStatus(Status.STATUS_ACTIVITY);
            list.add(sysAdminRole);
        }
        saveBatch(list, 100);
    }
}
