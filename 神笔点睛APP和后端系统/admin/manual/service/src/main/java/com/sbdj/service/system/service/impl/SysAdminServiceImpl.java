package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.AdminType;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.MD5Util;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.admin.query.AdminQuery;
import com.sbdj.service.system.admin.vo.AdminInfoVo;
import com.sbdj.service.system.admin.vo.AdminVo;
import com.sbdj.service.system.entity.SysAdmin;
import com.sbdj.service.system.mapper.SysAdminMapper;
import com.sbdj.service.system.service.ISysAdminService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 管理员表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysAdminServiceImpl extends ServiceImpl<SysAdminMapper, SysAdmin> implements ISysAdminService {

    @Override
    public SysAdmin findSysAdminByMobile(String mobile) {
        if (StrUtil.isNull(mobile)) {
            return null;
        }
        QueryWrapper<SysAdmin> wrapper = new QueryWrapper<>();
        wrapper.eq( "mobile", mobile);
        return baseMapper.selectOne(wrapper);
    }

    @Override
    public SysAdmin findSysAdminByMobileAndPwd(String mobile, String pwd) {
        if (StrUtil.isNull(mobile) || StrUtil.isNull(pwd)) {
            return null;
        }
        QueryWrapper<SysAdmin> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        wrapper.eq("pwd", pwd);
        return baseMapper.selectOne(wrapper);
    }

    @Override
    public IPage<AdminVo> findAdminPageList(AdminQuery query, IPage<AdminVo> iPage) {
        QueryWrapper<AdminQuery> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("sa.status", Status.STATUS_ACTIVITY,Status.STATUS_LOCK);
        queryWrapper.eq("so.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq((StrUtil.isNotNull(query.getType()) && !Number.INT_ZERO.equals(query.getType())), "sa.type", query.getType());
        queryWrapper.eq((StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId())), "sa.org_id", query.getOrgId());
        queryWrapper.and(StrUtil.isNotNull(query.getKeyword()),
                f -> f.like("sa.name", query.getKeyword())
                        .or().like("sa.mobile", query.getKeyword())
                        .or().like("sa.code", query.getKeyword()));
        queryWrapper.eq(StrUtil.isNotNull(query.getStatus()), "sa.status", query.getStatus());
        return baseMapper.findAdminPageList(iPage, queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAdmin(SysAdmin admin) {
        if (StrUtil.isNull(admin)) {
            throw BaseException.base("增加管理员失败,实体为空");
        }
        admin.setCreateTime(new Date());
        admin.setLastLoginTime(new Date());
        admin.setStatus(Status.STATUS_ACTIVITY);
        if(StrUtil.isNotNull(admin.getPwd())) {
            // 密码进行加密处理MD5
            String pwd = MD5Util.str2MD5(admin.getPwd()+admin.getWbKey());
            admin.setPwd(pwd);
        }
        save(admin);
    }

    @Override
    public List<SysAdmin> findAdminSelect(Long orgId) {
        QueryWrapper<SysAdmin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId), "org_id", orgId);
        queryWrapper.select("id","name","another_name");
        return list(queryWrapper);
    }

    @Override
    public List<SysAdmin> findAdminByTypeAndOrgId(Long orgId) {
        QueryWrapper<SysAdmin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type", AdminType.TYPE_SELLER);
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(orgId), "org_id", orgId);
        queryWrapper.select("id","name","org_id","status");
        return list(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateAdminHeadUrlById(Long id, String headUrl) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新管理员头像失败,id为空");
        }
        UpdateWrapper<SysAdmin> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("haed_url", headUrl);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Override
    public AdminInfoVo findAdminInfoVoByAdminId(Long adminId) {
        return baseMapper.findAdminInfoVoByAdminId(adminId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateStatus(Long id, String status, Long oprId) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新管理员状态失败,id为空");
        }
        UpdateWrapper<SysAdmin> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", status);
        updateWrapper.set("opr_id", oprId);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateAdmin(SysAdmin admin) {
        SysAdmin sa = getById(admin.getId());
        if(StrUtil.isNull(sa)) {
            throw BaseException.base("更新管理员失败,实体为空");
        }
        // 当前提交的密码不为空，则更新密码进行更新操作!
        if (StrUtil.isNotNull(admin.getPwd())) {
            // 密码进行加密处理MD5
            String pwd = MD5Util.str2MD5(admin.getPwd() + sa.getWbKey());
            sa.setPwd(pwd);
        }
        updateById(sa);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteAdmin(Long id, Long oprId) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除管理员失败,id为空");
        }
        UpdateWrapper<SysAdmin> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.set("opr_time", new Date());
        updateWrapper.set("opr_id", oprId);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteAdmins(String ids, Long oprId) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("批量逻辑删除管理员失败,ids为空");
        }
        UpdateWrapper<SysAdmin> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.set("opr_time", new Date());
        updateWrapper.set("opr_id", oprId);
        updateWrapper.in("id", list);
        update(updateWrapper);
    }
}
