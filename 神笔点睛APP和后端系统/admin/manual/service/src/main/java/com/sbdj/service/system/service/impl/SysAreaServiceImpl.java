package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysArea;
import com.sbdj.service.system.mapper.SysAreaMapper;
import com.sbdj.service.system.service.ISysAreaService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 省市区表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysAreaServiceImpl extends ServiceImpl<SysAreaMapper, SysArea> implements ISysAreaService {
    @Cacheable(cacheNames = "addressArea", key = "#parentId")
    @Override
    public List<SysArea> findAreaByParentId(Long parentId) {
        if (StrUtil.isNull(parentId)) {
            parentId = Number.LONG_NEGATIVE;
        }
        QueryWrapper<SysArea> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id" ,parentId);
        return list(queryWrapper);
    }
}
