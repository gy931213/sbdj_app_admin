package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysConfigDtl;
import com.sbdj.service.system.mapper.SysConfigDtlMapper;
import com.sbdj.service.system.service.ISysConfigDtlService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统配置详情表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysConfigDtlServiceImpl extends ServiceImpl<SysConfigDtlMapper, SysConfigDtl> implements ISysConfigDtlService {
    @Override
    public IPage<SysConfigDtl> findConfigDtlList(Long configId, IPage<SysConfigDtl> iPage) {
        QueryWrapper<SysConfigDtl> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("config_id", configId);
        queryWrapper.orderByAsc("sort");
        return page(iPage, queryWrapper);
    }

    @Cacheable(cacheNames = "ConfigDtlSetting", sync = true, key = "#configId")
    @Override
    public List<SysConfigDtl> findConfigDtl(Long configId) {
        QueryWrapper<SysConfigDtl>  queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("config_id", configId);
        queryWrapper.orderByAsc("sort");
        queryWrapper.select("id","name","sort");
        return list(queryWrapper);
    }

    @CacheEvict(cacheNames = "ConfigDtlSetting", allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveConfigDtl(SysConfigDtl configDtl) {
        configDtl.setCreateTime(new Date());
        configDtl.setStatus(Status.STATUS_ACTIVITY);
        save(configDtl);
    }

    @CacheEvict(cacheNames = "ConfigDtlSetting", allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateConfigDtl(SysConfigDtl configDtl) {
        if (StrUtil.isNull(configDtl) || StrUtil.isNull(configDtl.getId())) {
            throw BaseException.base("更新系统详细配置失败,id为空");
        }
        updateById(configDtl);
    }

    @CacheEvict(cacheNames = "ConfigDtlSetting", allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteConfigDtl(Long id) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除系统详细配置失败,id为空");
        }
        UpdateWrapper<SysConfigDtl> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @CacheEvict(cacheNames = "ConfigDtlSetting", allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteConfigDtls(String ids) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("逻辑删除系统详细配置失败,ids为空");
        }
        UpdateWrapper<SysConfigDtl> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.in("id", list);
        update(updateWrapper);
    }

    @Cacheable(cacheNames = "ConfigDtlSetting", sync = true, key = "#code")
    @Override
    public List<SysConfigDtl> findConfigDtl(String code) {
        return baseMapper.findConfigDtl(code);
    }
}
