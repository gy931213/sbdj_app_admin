package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysConfig;
import com.sbdj.service.system.mapper.SysConfigMapper;
import com.sbdj.service.system.service.ISysConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统配置表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {
    @Override
    public List<SysConfig> findSysConfigList() {
        QueryWrapper<SysConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.orderByAsc("id");
        return list(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveConfig(SysConfig config) {
        config.setCreateTime(new Date());
        config.setStatus(Status.STATUS_ACTIVITY);
        save(config);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateConfig(SysConfig config) {
        if (StrUtil.isNull(config.getId())) {
            throw BaseException.base("更新系统配置失败,id为空");
        }
        updateById(config);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteConfig(Long id) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除系统配置失败,id为空");
        }
        UpdateWrapper<SysConfig> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteConfigs(String ids) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("逻辑删除系统配置失败,ids为空");
        }
        UpdateWrapper<SysConfig> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.in("id", list);
        update(updateWrapper);
    }
}
