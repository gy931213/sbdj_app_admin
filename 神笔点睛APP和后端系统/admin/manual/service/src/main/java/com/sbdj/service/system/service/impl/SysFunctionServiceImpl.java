package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysFunction;
import com.sbdj.service.system.entity.SysRole;
import com.sbdj.service.system.mapper.SysFunctionMapper;
import com.sbdj.service.system.service.ISysAdminRoleService;
import com.sbdj.service.system.service.ISysFunctionService;
import com.sbdj.service.system.service.ISysRoleFunctionService;
import com.sbdj.service.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 功能表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysFunctionServiceImpl extends ServiceImpl<SysFunctionMapper, SysFunction> implements ISysFunctionService {
    @Autowired
    private ISysAdminRoleService iSysAdminRoleService;
    @Autowired
    private ISysRoleFunctionService iSysRoleFunctionService;
    @Autowired
    private ISysRoleService iSysRoleService;
    @Lazy
    @Autowired
    private ISysFunctionService iSysFunctionService;

    @Override
    public List<SysFunction> findFunctionByAdminId(Long adminId) {
        if (StrUtil.isNull(adminId)) {
            return null;
        }
        List<Long> roleIds = iSysAdminRoleService.getAdminRoleByAdminId(adminId);
        if (StrUtil.isNull(roleIds) || roleIds.size() < 1) {
            return null;
        }
        List<Long> funcIds = iSysRoleFunctionService.getRoleFunctionByRoleIds(StrUtil.getIds(roleIds));
        if (StrUtil.isNull(funcIds) || funcIds.size() < 1) {
            return null;
        }
        QueryWrapper<SysFunction> wrapper = new QueryWrapper<>();
        wrapper.eq("status", Status.STATUS_ACTIVITY);
        wrapper.in("id", funcIds);
        wrapper.orderByAsc("path", "sort");
        return list(wrapper);
    }

    @Override
    public List<SysFunction> findFunctionPageList() {
        QueryWrapper<SysFunction> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.orderByAsc("path", "sort");
        return list(queryWrapper);
    }

    @Override
    public List<SysFunction> findFunctionList(Long orgId) {
        if (StrUtil.isNull(orgId)) {
            return null;
        }
        return baseMapper.findFunctionList(orgId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveFunction(SysFunction function) {
        function.setCreateTime(new Date());
        function.setStatus(Status.STATUS_ACTIVITY);
        save(function);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateFunction(SysFunction function) {
        if (StrUtil.isNull(function) || StrUtil.isNull(function.getId())) {
            throw BaseException.base("功能菜单为空或ID为空");
        }
        updateById(function);
    }

    @Override
    public List<Long> getRoleFunctionByRoleId(Long roleId) {
        if (StrUtil.isNull(roleId)) {
            return null;
        }
        return baseMapper.getRoleFunctionByRoleId(roleId);
    }

    @Override
    public List<SysFunction> findFunctionByIds(List<Long> ids) {
        if (StrUtil.isNull(ids) || ids.size() < 1) {
            return null;
        }
        QueryWrapper<SysFunction> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.in("id", ids);
        return list(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateAuthorizeByOrgId(Long orgId) {
        List<SysFunction> functionList = iSysFunctionService.findFunctionList(orgId);
        // 获取该组织下的所有角色
        List<SysRole> roleList = iSysRoleService.findRoleSelect(orgId);
        List<Long> funcIds;
        for (SysRole role : roleList) {
            // 获取该角色下的所有权限id
            funcIds = iSysFunctionService.findFunctionListByRoleId(role.getId());
            // 更新权限
            // 删除该角色的所有权限
            iSysRoleFunctionService.deleteRoleFunctionByRoleId(role.getId());
            // 定义控制值
            boolean flag = false;
            // 权限匹配 组织没有的 角色则删除
            for (Long funcId : funcIds) {
                for (SysFunction function : functionList) {
                    if (function.getId().equals(funcId)) {
                        flag = false;
                        break;
                    } else {
                        flag = true;
                    }
                }
                if (flag) {
                    funcIds.remove(funcId);
                }
            }
            // 权限更新
            iSysRoleFunctionService.saveRoleFunction(role.getId(), funcIds);
        }
    }

    @Override
    public List<Long> findFunctionListByRoleId(Long roleId) {
        return baseMapper.findFunctionListByRoleId(roleId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteFunctionByIds(String ids) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNotNull(list) && list.size() < 1) {
            throw BaseException.base("批量删除权限失败,ids为空");
        }
        QueryWrapper<SysFunction> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id", list);
        removeByIds(list);
    }
}
