package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysMapTrajectory;
import com.sbdj.service.system.mapper.SysMapTrajectoryMapper;
import com.sbdj.service.system.service.ISysMapTrajectoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class SysMapTrajectoryServiceImpl extends ServiceImpl<SysMapTrajectoryMapper, SysMapTrajectory> implements ISysMapTrajectoryService {
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSysMapTrajectory(SysMapTrajectory sysMapTrajectory) {
        // 查询经纬度一致的数据
        QueryWrapper<SysMapTrajectory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("target_id", sysMapTrajectory.getTargetId());
        queryWrapper.eq("longitude", sysMapTrajectory.getLongitude());
        queryWrapper.eq("latitude", sysMapTrajectory.getLatitude());
        SysMapTrajectory smt = getOne(queryWrapper);

        if (StrUtil.isNotNull(smt)) {
            // 不为空则增量添加
            smt.setIp(sysMapTrajectory.getIp());
            smt.setCreateTime(new Date());
            save(smt);
        } else {
            // 为空则新增
            sysMapTrajectory.setLongLati(sysMapTrajectory.getLongitude()+","+sysMapTrajectory.getLatitude());
            sysMapTrajectory.setStatus(Status.STATUS_ACTIVITY);
            sysMapTrajectory.setCreateTime(new Date());
            save(sysMapTrajectory);
        }
    }

    @Override
    public Integer countMapTrajectoryByTargetId(Long targetId) {
        QueryWrapper<SysMapTrajectory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("target_id",targetId);
        return count(queryWrapper);
    }
}
