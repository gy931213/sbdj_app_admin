package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.service.system.entity.SysNumber;
import com.sbdj.service.system.mapper.SysNumberMapper;
import com.sbdj.service.system.service.ISysNumberService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 单号的生成策略表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysNumberServiceImpl extends ServiceImpl<SysNumberMapper, SysNumber> implements ISysNumberService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String findSysNumberJOBByNumberCoe(String code) {
       StringBuffer sb = new StringBuffer();
            // 根据code锁行
            baseMapper.sysNumberLock(code);
            // 获取到当前锁住的数据
            SysNumber number = findOne(code);
            if (number == null) {
                throw BaseException.base("没有单号生成规则配置");
            }
            // 前缀字符
            sb.append(number.getPrefix());

            // 单号的长度-前缀的字符长度 = 循环次数
            int index = (number.getLength() - (number.getPrefix().length() + String.valueOf(number.getAddValue()).length()));
            for (int i = 0; i < index; i++) {
                sb.append(number.getLastValue());
            }
            number.setLastValue("15");
            // 递增值
            sb.append(number.getAddValue());
            // 更新数据
            number.setLastNumber(sb.toString());
            updateSysNumberByParams(number);
        return sb.toString();

    }

    @Override
    public SysNumber findOne(String code) {
        return baseMapper.findOne(Status.STATUS_ACTIVITY,code);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSysNumberByParams(SysNumber maxNumber) {
        // 默认的最大值
        Integer defaultMax = 9999999;
        // 生成每一次累加一
        maxNumber.setAddValue(maxNumber.getAddValue() + 1);
        // 判断生成的最大值是否为空
        if (null != maxNumber.getMaxValue()) {
            defaultMax = maxNumber.getMaxValue();
        }
        // 判断这个递增值是大于指定的最大递增值
        if (maxNumber.getAddValue() >= defaultMax) {
            maxNumber.setAddValue(0);
        }
        baseMapper.updateSysNumber(maxNumber.getId(), maxNumber.getAddValue(), maxNumber.getLastNumber());
    }
}
