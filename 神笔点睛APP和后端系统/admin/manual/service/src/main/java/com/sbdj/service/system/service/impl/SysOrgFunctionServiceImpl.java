package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysOrgFunction;
import com.sbdj.service.system.mapper.SysOrgFunctionMapper;
import com.sbdj.service.system.service.ISysFunctionService;
import com.sbdj.service.system.service.ISysOrgFunctionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Struct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统组织权限 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysOrgFunctionServiceImpl extends ServiceImpl<SysOrgFunctionMapper, SysOrgFunction> implements ISysOrgFunctionService {
    @Autowired
    private ISysFunctionService iSysFunctionService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveOrgFunction(Long orgId, Long oprId, String funcIds) {
        if (StrUtil.isNull(funcIds) || StrUtil.isNull(oprId) || StrUtil.isNull(orgId)) {
            throw BaseException.base("功能id、机构id或操作人id为空,保存失败");
        }
        QueryWrapper<SysOrgFunction> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("org_id", orgId);
        remove(queryWrapper);
        List<Long> ids = StrUtil.getIdsByLong(funcIds);
        if (StrUtil.isNull(ids)) {
            throw BaseException.base("获取机构id集合为空,停止保存");
        }
        List<SysOrgFunction> list = new ArrayList<>();
        SysOrgFunction orgFunction;
        for (Long id : ids) {
            orgFunction = new SysOrgFunction();
            orgFunction.setOrgId(orgId);
            orgFunction.setFuncId(id);
            orgFunction.setOprId(oprId);
            orgFunction.setCreateTime(new Date());
            list.add(orgFunction);
        }
        // 批量保存 100条分割
        saveBatch(list, 100);
        // 更新该机构下的所有权限
        iSysFunctionService.updateAuthorizeByOrgId(orgId);
    }
}
