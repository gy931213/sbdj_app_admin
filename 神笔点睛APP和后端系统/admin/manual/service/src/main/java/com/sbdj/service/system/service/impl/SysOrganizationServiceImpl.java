package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysOrganization;
import com.sbdj.service.system.mapper.SysOrganizationMapper;
import com.sbdj.service.system.service.ISysOrganizationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 组织表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysOrganizationServiceImpl extends ServiceImpl<SysOrganizationMapper, SysOrganization> implements ISysOrganizationService {

    @Override
    public List<SysOrganization> findOrganizationList() {
        QueryWrapper<SysOrganization> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.orderByAsc("sort");
        return list(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveOrganization(SysOrganization organization) {
        organization.setCreateTime(new Date());
        organization.setStatus(Status.STATUS_ACTIVITY);
        save(organization);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateOrganization(SysOrganization organization) {
        if (StrUtil.isNull(organization) || StrUtil.isNull(organization.getId())) {
            throw BaseException.base("更新组织时,实体为空或id为空");
        }
        updateById(organization);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteOrganization(String ids, Long oprId) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list) || StrUtil.isNull(oprId)) {
            throw BaseException.base("删除组织时,id为空或oprId为空");
        }
        UpdateWrapper<SysOrganization> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.set("opr_time", new Date());
        updateWrapper.set("opr_id", oprId);
        updateWrapper.in("id", list);
        update(updateWrapper);
    }

    @Override
    public List<SysOrganization> findOrganizationSelect() {
        QueryWrapper<SysOrganization> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.orderByAsc("sort");
        return list(queryWrapper);
    }

    @Override
    public List<SysOrganization> findOrganizationSelect(Long orgId) {
        if (StrUtil.isNull(orgId)) {
            return null;
        }
        QueryWrapper<SysOrganization> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("id", orgId);
        queryWrapper.orderByAsc("sort");
        return list(queryWrapper);
    }
}
