package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.entity.SysRoleFunction;
import com.sbdj.service.system.mapper.SysRoleFunctionMapper;
import com.sbdj.service.system.service.ISysRoleFunctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 角色功能关系表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysRoleFunctionServiceImpl extends ServiceImpl<SysRoleFunctionMapper, SysRoleFunction> implements ISysRoleFunctionService {
    @Lazy
    @Autowired
    ISysRoleFunctionService iSysRoleFunctionService;

    @Override
    public List<Long> getRoleFunctionByRoleIds(String roleIds) {
        if (StrUtil.isNull(roleIds)) {
            return null;
        }
        return baseMapper.getRoleFunctionByRoleIds(roleIds);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteRoleFunctionByRoleId(Long roleId) {
        if (StrUtil.isNull(roleId)) {
            throw BaseException.base("roleId为空,无法删除该角色下的所有权限");
        }
        QueryWrapper<SysRoleFunction> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id", roleId);
        remove(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveRoleFunction(Long roleId, String funcIds) {
        List<Long> list = StrUtil.getIdsByLong(funcIds);
        iSysRoleFunctionService.saveRoleFunction(roleId, list);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveRoleFunction(Long roleId, List<Long> funcIds) {
        List<SysRoleFunction> list = new ArrayList<>();
        SysRoleFunction roleFunction;
        for (Long funcId : funcIds) {
            roleFunction = new SysRoleFunction();
            roleFunction.setRoleId(roleId);
            roleFunction.setFuncId(funcId);
            roleFunction.setCreateTime(new Date());
            roleFunction.setStatus(Status.STATUS_ACTIVITY);
            list.add(roleFunction);
        }
        saveBatch(list, 100);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteRoleFunctionByRoleIds(String roleIds) {
        if (StrUtil.isNull(roleIds)) {
            throw BaseException.base("批量删除角色权限错误,角色id为空");
        }
        baseMapper.deleteRoleFunctionByRoleIds(roleIds);
    }
}
