package com.sbdj.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.system.admin.vo.SysRoleVo;
import com.sbdj.service.system.entity.SysRole;
import com.sbdj.service.system.mapper.SysRoleMapper;
import com.sbdj.service.system.service.ISysRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 角色表
 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-08
 */
@Transactional(readOnly = true)
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {
    @Override
    public List<SysRole> findRoleSelect(Long orgId) {
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId), "org_id", orgId);
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        return list(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveRole(SysRole role) {
        role.setCreateTime(new Date());
        role.setOprTime(new Date());
        role.setStatus(Status.STATUS_ACTIVITY);
        save(role);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateRole(SysRole role) {
        if (StrUtil.isNull(role) || StrUtil.isNull(role.getId())) {
            throw BaseException.base("更新角色时,实体或id为空");
        }
        UpdateWrapper<SysRole> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set(StrUtil.isNotNull(role.getName()), "name", role.getName());
        updateWrapper.set(StrUtil.isNotNull(role.getCode()), "code", role.getCode());
        updateWrapper.set(StrUtil.isNotNull(role.getDescription()), "description", role.getDescription());
        updateWrapper.set(StrUtil.isNotNull(role.getOrgId()), "org_id", role.getOrgId());
        updateWrapper.set("create_time", new Date());
        updateWrapper.eq("id", role.getId());
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteRole(Long id) {
        UpdateWrapper<SysRole> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.set("opr_time", new Date());
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteRoles(String ids) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("id为空,停止逻辑删除角色");
        }
        UpdateWrapper<SysRole> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.set("opr_time", new Date());
        updateWrapper.in("id", list);
        update(updateWrapper);
    }

    @Override
    public IPage<SysRoleVo> findRolePage(String keyword, Long orgId, IPage<SysRoleVo> iPage) {
        return baseMapper.findRolePage(iPage, keyword, orgId);
    }
}
