package com.sbdj.service.system.type;

public final class AreaType {

    /**
     * 省 {@value}
     */
    public static final String province = "P";
    /**
     * 市 {@value}
     */
    public static final String city = "C";
    /**
     * 区/县
     */
    public static final String district = "D";
}
