package com.sbdj.service.system.type;

/**
 * @ClassName: SysNumberType
 * @Description: 系统单号生成策略代码
 * @author: 黄天良
 * @date: 2018年12月1日 下午4:52:57
 */
public final class SysNumberType {

    /**
     * 任务单号 {@value}
     */
    public static final String CODE_TASK = "CODE_TASK";

    /**
     * 子任务单号 {@value}
     */
    public static final String CODE_TASK_ONE = "CODE_TASK_ONE";

    /**
     * 业务员工号 {@value}
     */
    public static final String CODE_JOB = "CODE_JOB";

    /**
     * 申请货款单号 {@value}
     */
    public static final String CODE_APPLY_PAY = "CODE_APPLY_PAY";

}
