package com.sbdj.service.task.admin.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @ClassName: TaskCommonSentenceQuery
 * @Description:
 * @author: 杨凌云
 * @date: 2019/3/8 11:23
 */
@ApiModel(value = "机构查询类",description = "提供机构查询条件")
public class TaskCommonSentenceQuery {

    @ApiModelProperty(value = "所属机构")
    private Long orgId;
    
    @ApiModelProperty(value = "所属平台")
    private Long platformId;
    
    @ApiModelProperty(value = "内容")
    private String content;
    
    @ApiModelProperty(value = "状态,A/D")
    private String status;
    
    @ApiModelProperty(value = "类型,AN:不通过 AF:已失败")
    private String TcsType;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTcsType() {
        return TcsType;
    }

    public void setTcsType(String tcsType) {
        TcsType = tcsType;
    }
}
