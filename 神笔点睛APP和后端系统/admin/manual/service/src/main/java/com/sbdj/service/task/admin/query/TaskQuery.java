package com.sbdj.service.task.admin.query;


import com.sbdj.core.constant.Status;
import com.sbdj.core.util.StrUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @ClassName: TaskParams
 * @Description: 任务查询的参数类
 * @author: 黄天良
 * @date: 2018年12月1日 下午5:36:18
 */
@ApiModel(value = "任务查询条件类",description = "用于提供任务查询条件")
public class TaskQuery {
	

	@ApiModelProperty(value = "所属机构")
	private Long orgId;

	@ApiModelProperty(value = "任务状态")
	private String status;

	@ApiModelProperty(value = "任务单号")
	private String TaskNumber;

	@ApiModelProperty(value = "店铺名称")
	private String shopName;

	@ApiModelProperty(value = "所属平台")
	private Long platformId;

	@ApiModelProperty(value = "任务类型")
	private Long taskTypeId;

	@ApiModelProperty(value = "开始时间")
	private String beginDate;

	@ApiModelProperty(value = "结束时间")
	private String endDate;

	@ApiModelProperty(value = "是否开启历史数据查询")
	private String isHistory;

	@ApiModelProperty(value = "商品类目")
	private String category;

	public String getStatus() {
		return status;
	}

	public String getTaskNumber() {
		return TaskNumber;
	}

	public String getShopName() {
		return shopName;
	}

	public Long getPlatformId() {
		return platformId;
	}

	public Long getTaskTypeId() {
		return taskTypeId;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTaskNumber(String taskNumber) {
		TaskNumber = taskNumber;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}

	public void setTaskTypeId(Long taskTypeId) {
		this.taskTypeId = taskTypeId;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getIsHistory() {
		if(StrUtil.isNull(isHistory)) {
			isHistory = Status.STATUS_NO;
		}
		return isHistory;
	}

	public void setIsHistory(String isHistory) {
		this.isHistory = isHistory;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
