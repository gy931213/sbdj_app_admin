package com.sbdj.service.task.admin.query;


import com.sbdj.core.constant.Status;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @ClassName: TaskSonQuery
 * @Description: (这里用一句话描述这个类的作用)
 * @author: 黄天良
 * @date: 2018年12月4日 上午10:44:00
 */
@ApiModel(value = "子任务查询条件类",description = "用于提供子任务查询条件")
public class TaskSonQuery {

	
	@ApiModelProperty(value = "所属机构")
	private Long orgId;
	
	@ApiModelProperty(value = "是否超时")
	private String isTimeOut;
	
	@ApiModelProperty(value = "任务状态")
	private String status;
	
	@ApiModelProperty(value = "主任务编号")
	private String taskNumber;
	
	@ApiModelProperty(value = "子任务编号")
	private String taskSonNumber;
	
	@ApiModelProperty(value = "开始时间")
	private String beginTime;
	
	@ApiModelProperty(value = "结束时间")
	private String endTime;
	
	
	@ApiModelProperty(value = "店铺名称")
	private String shopName;
	
	@ApiModelProperty(value = "订单号")
	private String orderNum;
	
	@ApiModelProperty(value = "业务员名称")
	private String salesmanName;
	
	@ApiModelProperty(value = "号主Id")
	private String onlineid;
	
	@ApiModelProperty(value = "审核人名称")
	private String auditorName;
	
	@ApiModelProperty(value = "平台编号")
    private Long platformId;
	
	@ApiModelProperty(value = "任务类型编号")
    private Long taskTypeId;
    
	@ApiModelProperty(value = "商家对单{对单：YES，未对单：NO}")
	private String orderMatch;

	@ApiModelProperty(value = "货款类型，[{DF:垫付}，{SQ:申请}]")
    private String taskSonType;

	@ApiModelProperty(value = "是否开启历史数据查询")
	private String isHistory;
	
	@ApiModelProperty(value = "是否购买应用 YES:是， NO:否")
	private String isBuy;

	@ApiModelProperty(value = "业务员手机")
	private String salesmanPhone;

	@ApiModelProperty(value = "号主手机")
	private String salesmanNumberPhone;

	@ApiModelProperty(value = "商品类目")
	private String category;

	public String getIsTimeOut() {
		return isTimeOut;
	}

	public String getStatus() {
		return status;
	}

	public String getTaskNumber() {
		return taskNumber;
	}

	public String getTaskSonNumber() {
		return taskSonNumber;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setIsTimeOut(String isTimeOut) {
		this.isTimeOut = isTimeOut;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTaskNumber(String taskNumber) {
		this.taskNumber = taskNumber;
	}

	public void setTaskSonNumber(String taskSonNumber) {
		this.taskSonNumber = taskSonNumber;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getSalesmanName() {
		return salesmanName;
	}

	public void setSalesmanName(String salesmanName) {
		this.salesmanName = salesmanName;
	}

	public String getOnlineid() {
		return onlineid;
	}

	public void setOnlineid(String onlineid) {
		this.onlineid = onlineid;
	}

	public String getAuditorName() {
		return auditorName;
	}

	public void setAuditorName(String auditorName) {
		this.auditorName = auditorName;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

	public String getOrderMatch() {
		return orderMatch;
	}

	public void setOrderMatch(String orderMatch) {
		this.orderMatch = orderMatch;
	}
    
	public String getIsHistory() {
		if(null == isHistory || "" == isHistory) {
			isHistory = Status.STATUS_NO;
		}
		return isHistory;
	}
	
    public String getTaskSonType() {
        return taskSonType;
    }

    public void setTaskSonType(String taskSonType) {
        this.taskSonType = taskSonType;
    }

    public void setIsHistory(String isHistory) {
		this.isHistory = isHistory;
	}

	public String getIsBuy() {
		return isBuy;
	}

	public void setIsBuy(String isBuy) {
		this.isBuy = isBuy;
	}

	public String getSalesmanPhone() {
		return salesmanPhone;
	}

	public void setSalesmanPhone(String salesmanPhone) {
		this.salesmanPhone = salesmanPhone;
	}

	public String getSalesmanNumberPhone() {
		return salesmanNumberPhone;
	}

	public void setSalesmanNumberPhone(String salesmanNumberPhone) {
		this.salesmanNumberPhone = salesmanNumberPhone;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
