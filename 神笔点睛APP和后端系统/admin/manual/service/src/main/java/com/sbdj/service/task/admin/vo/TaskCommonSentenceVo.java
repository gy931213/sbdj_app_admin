package com.sbdj.service.task.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @ClassName: TaskCommonSentenceExt
 * @Description:
 * @author: 杨凌云
 * @date: 2019/3/8 10:19
 */
public class TaskCommonSentenceVo {
    private Long id;
    /**
     * 所属机构
     **/
    private Long orgId;
    /**
     * 所属平台
     **/
    private Long platformId;
    /**
     * 内容
     **/
    private String content;
    /**
     * 状态,A/D
     **/
    private String status;
    /**
     * 类型,AN:不通过 AF:已失败
     **/
    private String type;
    /**
     * 创建时间
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 机构名
     **/
    private String orgName;
    /**
     * 机构编码
     **/
    private String orgCode;
    /**
     * 平台名
     **/
    private String platformName;
    /**
     * 平台编码
     **/
    private String platformCode;
    /**
     * 任务图片类型ID
     **/
    private String tptsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getPlatformCode() {
        return platformCode;
    }

    public void setPlatformCode(String platformCode) {
        this.platformCode = platformCode;
    }

    public String getTptsId() {
        return tptsId;
    }

    public void setTptsId(String tptsId) {
        this.tptsId = tptsId;
    }
}
