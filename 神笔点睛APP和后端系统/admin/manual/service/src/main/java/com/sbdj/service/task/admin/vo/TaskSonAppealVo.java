package com.sbdj.service.task.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: TaskSonAppealExt
 * @Description: 子任务申诉表
 * @author: 杨凌云
 * @date: 2019/6/24 10:56
 */
@ApiModel(value = "子任务申诉Vo类",description = "用于接收查询到的子任务申诉数据")
public class TaskSonAppealVo {

    @ApiModelProperty(value = "主键自增")
    private Long id;

    @ApiModelProperty(value = "子任务号码")
    private Long taskSonId;

    @ApiModelProperty(value = "子任务编号")
    private String taskSonNumber;

    @ApiModelProperty(value = "所属平台的订单号")
    private String orderNum;

    @ApiModelProperty(value = "所属业务员id")
    private Long salesmanId;

    @ApiModelProperty(value = "所属业务员号主id")
    private Long salesmanNumberId;

    @ApiModelProperty(value = "平台账号")
    private String onlineid;

    @ApiModelProperty(value = "图片链接/使用逗号隔开(“,”)")
    private String images;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "审核时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date auditorTime;

    @ApiModelProperty(value = "审核人id/对应sys_admin表中的id")
    private Long auditorId;

    @ApiModelProperty(value = "审核人名称")
    private String auditorName;

    @ApiModelProperty(value = "原因")
    private String reasons;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "业务员名称")
    private String salesmanName;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "是否超时{超时：YES，未超时：NO}")
    private String isTimeOut;

    @ApiModelProperty(value = "号主名称")
    private String salesmanNumberName;

    @ApiModelProperty(value = "审核原因")
    private String account;

    @ApiModelProperty(value = "应付价格")
    private BigDecimal realPrice;

    @ApiModelProperty(value = "店铺名称")
    private String shopName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }

    public String getTaskSonNumber() {
        return taskSonNumber;
    }

    public void setTaskSonNumber(String taskSonNumber) {
        this.taskSonNumber = taskSonNumber;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getAuditorTime() {
        return auditorTime;
    }

    public void setAuditorTime(Date auditorTime) {
        this.auditorTime = auditorTime;
    }

    public Long getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(Long auditorId) {
        this.auditorId = auditorId;
    }

    public String getAuditorName() {
        return auditorName;
    }

    public void setAuditorName(String auditorName) {
        this.auditorName = auditorName;
    }

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getIsTimeOut() {
        return isTimeOut;
    }

    public void setIsTimeOut(String isTimeOut) {
        this.isTimeOut = isTimeOut;
    }

    public String getSalesmanNumberName() {
        return salesmanNumberName;
    }

    public void setSalesmanNumberName(String salesmanNumberName) {
        this.salesmanNumberName = salesmanNumberName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
