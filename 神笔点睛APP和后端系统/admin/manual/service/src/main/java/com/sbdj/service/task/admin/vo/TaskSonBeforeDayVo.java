package com.sbdj.service.task.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * <p>
 * 前一天/当天任务状态数据类
 * </p>
 *
 * @author Yly
 * @since 2019-11-27
 */
public class TaskSonBeforeDayVo {
    // 总量
    private Long sum;
    // 状态code
    private String status;
    // 状态名称
    private String statusName;
    // 创建时间
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;

    public Long getSum() {
        return sum;
    }
    public void setSum(Long sum) {
        this.sum = sum;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getStatusName() {
        return statusName;
    }
    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
