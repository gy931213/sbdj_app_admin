package com.sbdj.service.task.admin.vo;

/**
 * @ClassName: TaskSonImagesExt
 * @Description: 子任务图片集合扩展类
 * @author: 黄天良
 * @date: 2018年11月30日 下午4:59:20
 */
public class TaskSonImagesVo {
    /**
     * id
     **/
    private Long id;
    /**
     * 图片链接
     **/
    private String link;
    /**
     * 图片链接类型名称
     **/
    private String typeName;

    public Long getId() {
        return id;
    }

    public String getLink() {
        return link;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }


}
