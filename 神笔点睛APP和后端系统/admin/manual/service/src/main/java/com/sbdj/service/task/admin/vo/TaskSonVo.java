package com.sbdj.service.task.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: TaskSon
 * @Description: 子任务扩展类
 * @author: 黄天良
 * @date: 2018年11月30日 下午4:59:00
 */
@ApiModel(value = "子任务扩展类",description = "用于接收查询到的扩展的子任务数据")
public class TaskSonVo {
	
	@ApiModelProperty(value = "主键自增")
	private Long id;
	
	@ApiModelProperty(value = "子任务编号")
	private String taskSonNumber;
	
	@ApiModelProperty(value = "所属平台的订单号")
	private String orderNum;
	
	@ApiModelProperty(value = "所属平台id")
	private Long platformId;
	
	@ApiModelProperty(value = "所属平台Code")
	private String platformCode;
	
	@ApiModelProperty(value = "所属主任务")
	private Long taskId;
	
	@ApiModelProperty(value = "所属业务员id")
	private Long salesmanId;
	
	@ApiModelProperty(value = "业务员工号")
	private String jobNumber;
	
	@ApiModelProperty(value = "所属业务员号主id")
	private Long salesmanNumberId;
	
	@ApiModelProperty(value = "平台账号")
	private String onlineid;
	
	@ApiModelProperty(value = "所属关键字id")
	private Long keywordId;
	
	@ApiModelProperty(value = "关键字名称")
	private String keyword;
	
	@ApiModelProperty(value = "所属业务员号主手机唯一码")
	private String imei;
	
	@ApiModelProperty(value = "图片链接/使用逗号隔开(“,”)")
	private String images;
	
	@ApiModelProperty(value = "佣金")
	private BigDecimal brokerage;
	
	@ApiModelProperty(value = "在A状态过期时间创建时间累加30钟，在F的过期时间是点击操作的时间累加6个小时。")
	private Date deadlineTime;
	
	@ApiModelProperty(value = "分钟")
	private Integer minute;
	
	@ApiModelProperty(value = "小时")
	private Integer hour;
	
	@ApiModelProperty(value = "是否是当天（作用与隔日单）")
	private boolean isDay;
	
	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;
	
	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date updateTime;
	
	@ApiModelProperty(value = "是否超时{超时：YES，未超时：NO}")
	private String isTimeOut;
	
	@ApiModelProperty(value = "审核人id/对应sys_admin表中的id")
	private Long auditorId;
	
	@ApiModelProperty(value = "审核人名称")
	private String auditorName;
	
	@ApiModelProperty(value = "审核不通过/审核失败/原因")
	private String account;
	
	@ApiModelProperty(value = "状态{开始：A， 待提交：F，已审核：T，已传图：PG，被拒绝：AN，已失败：AF }")
	private String status;
	
	@ApiModelProperty(value = "商家对单{对单：YES，未对单：NO}")
	private String orderMatch;
	
	@ApiModelProperty(value = "反审次数")
	private Integer recheck;

	
	@ApiModelProperty(value = "主任务编号")
	private String taskNumber;
	
	@ApiModelProperty(value = "实付价格")
	private BigDecimal realPrice;
	
	@ApiModelProperty(value = "业务员名称")
	private String salesmanName;
	
	@ApiModelProperty(value = "业务员号主名称")
	private String salesmanNumberName;

	
	@ApiModelProperty(value = "所属店铺id")
	private Long sellerShopId;
	
	@ApiModelProperty(value = "所属商品id")
	private Long goodsId;
	
	@ApiModelProperty(value = "店铺名称")
	private String shopName;
	
	@ApiModelProperty(value = "商品标题")
	private String title;
	
	@ApiModelProperty(value = "商品链接")
	private String link;
	
	@ApiModelProperty(value = "商品主图")
	private String imgLink;
	
	@ApiModelProperty(value = "对单的Id")
	private Integer uniacid;
	
	
	@ApiModelProperty(value = "所属平台（名称）")
	private String platformName;
	
	
	@ApiModelProperty(value = "任务类型id")
	private Long taskTypeId;
	
	@ApiModelProperty(value = "任务类型（名称）")
	private String taskTypeName;
	
	@ApiModelProperty(value = "任务代码")
	private String taskCode;
	
	@ApiModelProperty(value = "任务类型编码")
	private String taskTypeCode;
	
	@ApiModelProperty(value = "所属机构")
	private Long orgId;
	

	
	@ApiModelProperty(value = "订单状态-第三方接口响应参数/0：待付款，1：待发货，2：待收货，3：已完成 ，-1：已关闭")
	private Integer orderStatus;
	
	@ApiModelProperty(value = "订单实付金额-第三方接口响应参数")
	private BigDecimal payment;
	
	@ApiModelProperty(value = "买家昵称-第三方接口响应参数")
	private String buyerNick;
	
	@ApiModelProperty(value = "任务类型，[{DF:垫付}，{SQ:申请}")
	private String taskSonType;
	
	
	@ApiModelProperty(value = "商品是否打标[{0：否},{1：是},{2：自动}]")
	private Integer isCard;
	
	@ApiModelProperty(value = "是否标记为已打标状态[{0：否},{1：是}]")
	private Integer isMark;
	
	
	@ApiModelProperty(value = "商家昵称")
	private String sellerNick;
	
	@ApiModelProperty(value = "是否购买应用 YES:有 NO:没有")
	private String isBuy;

	@ApiModelProperty(value = "是否平台发货 [{YES ：是},{NO ： 否}]  默认为 NO")
    private String isExpress;
    
	@ApiModelProperty(value = "状态名称")
    private String statusName;
    
	@ApiModelProperty(value = "订单状态名称")
    private String orderStatusName;
    
	@ApiModelProperty(value = "商品关键词")
    private String goodsKeyword;
	
	@ApiModelProperty(value = "是否首次任务 0:否 1:是 默认:0")
	private Integer firstMission = 0;
	
	@ApiModelProperty(value = "接取的任务数")
	private Integer taskTotal;
    
	@ApiModelProperty(value = "号主状态{A：启用，N：禁用}")
    private String pStatus;
    
	@ApiModelProperty(value = "会员状态{A：启用，N：禁用}")
    private String mStatus;

	@ApiModelProperty(value = "商品类目")
	private String category;

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTaskSonNumber() {
		return taskSonNumber;
	}

	public void setTaskSonNumber(String taskSonNumber) {
		this.taskSonNumber = taskSonNumber;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Long getPlatformId() {
		return platformId;
	}

	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Long getSalesmanId() {
		return salesmanId;
	}

	public void setSalesmanId(Long salesmanId) {
		this.salesmanId = salesmanId;
	}

	public Long getSalesmanNumberId() {
		return salesmanNumberId;
	}

	public void setSalesmanNumberId(Long salesmanNumberId) {
		this.salesmanNumberId = salesmanNumberId;
	}

	public String getOnlineid() {
		return onlineid;
	}

	public void setOnlineid(String onlineid) {
		this.onlineid = onlineid;
	}

	public Long getKeywordId() {
		return keywordId;
	}

	public void setKeywordId(Long keywordId) {
		this.keywordId = keywordId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public BigDecimal getBrokerage() {
		return brokerage;
	}

	public void setBrokerage(BigDecimal brokerage) {
		this.brokerage = brokerage;
	}

	public Date getDeadlineTime() {
		return deadlineTime;
	}

	public void setDeadlineTime(Date deadlineTime) {
		this.deadlineTime = deadlineTime;
	}

	public Integer getMinute() {
		return minute;
	}

	public void setMinute(Integer minute) {
		this.minute = minute;
	}

	public Integer getHour() {
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public boolean isDay() {
		return isDay;
	}

	public void setDay(boolean day) {
		isDay = day;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getIsTimeOut() {
		return isTimeOut;
	}

	public void setIsTimeOut(String isTimeOut) {
		this.isTimeOut = isTimeOut;
	}

	public Long getAuditorId() {
		return auditorId;
	}

	public void setAuditorId(Long auditorId) {
		this.auditorId = auditorId;
	}

	public String getAuditorName() {
		return auditorName;
	}

	public void setAuditorName(String auditorName) {
		this.auditorName = auditorName;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrderMatch() {
		return orderMatch;
	}

	public void setOrderMatch(String orderMatch) {
		this.orderMatch = orderMatch;
	}

	public Integer getRecheck() {
		return recheck;
	}

	public void setRecheck(Integer recheck) {
		this.recheck = recheck;
	}

	public String getTaskNumber() {
		return taskNumber;
	}

	public void setTaskNumber(String taskNumber) {
		this.taskNumber = taskNumber;
	}

	public BigDecimal getRealPrice() {
		return realPrice;
	}

	public void setRealPrice(BigDecimal realPrice) {
		this.realPrice = realPrice;
	}

	public String getSalesmanName() {
		return salesmanName;
	}

	public void setSalesmanName(String salesmanName) {
		this.salesmanName = salesmanName;
	}

	public String getSalesmanNumberName() {
		return salesmanNumberName;
	}

	public void setSalesmanNumberName(String salesmanNumberName) {
		this.salesmanNumberName = salesmanNumberName;
	}

	public Long getSellerShopId() {
		return sellerShopId;
	}

	public void setSellerShopId(Long sellerShopId) {
		this.sellerShopId = sellerShopId;
	}

	public Long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public Long getTaskTypeId() {
		return taskTypeId;
	}

	public void setTaskTypeId(Long taskTypeId) {
		this.taskTypeId = taskTypeId;
	}

	public String getTaskTypeName() {
		return taskTypeName;
	}

	public void setTaskTypeName(String taskTypeName) {
		this.taskTypeName = taskTypeName;
	}

	public String getTaskCode() {
		return taskCode;
	}

	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}

	public String getTaskTypeCode() {
		return taskTypeCode;
	}

	public void setTaskTypeCode(String taskTypeCode) {
		this.taskTypeCode = taskTypeCode;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public BigDecimal getPayment() {
		return payment;
	}

	public void setPayment(BigDecimal payment) {
		this.payment = payment;
	}

	public String getBuyerNick() {
		return buyerNick;
	}

	public void setBuyerNick(String buyerNick) {
		this.buyerNick = buyerNick;
	}

	public String getTaskSonType() {
		return taskSonType;
	}

	public void setTaskSonType(String taskSonType) {
		this.taskSonType = taskSonType;
	}

	public Integer getUniacid() {
		return uniacid;
	}

	public void setUniacid(Integer uniacid) {
		this.uniacid = uniacid;
	}

	public String getImgLink() {
		return imgLink;
	}

	public void setImgLink(String imgLink) {
		this.imgLink = imgLink;
	}

	public Integer getIsCard() {
		return isCard;
	}

	public void setIsCard(Integer isCard) {
		this.isCard = isCard;
	}

	public Integer getIsMark() {
		return isMark;
	}

	public void setIsMark(Integer isMark) {
		this.isMark = isMark;
	}

	public String getSellerNick() {
		return sellerNick;
	}

	public void setSellerNick(String sellerNick) {
		this.sellerNick = sellerNick;
	}

    public String getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(String isBuy) {
        this.isBuy = isBuy;
    }

    public String getIsExpress() {
        return isExpress;
    }

    public void setIsExpress(String isExpress) {
        this.isExpress = isExpress;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getOrderStatusName() {
        return orderStatusName;
    }

    public void setOrderStatusName(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

    public String getGoodsKeyword() {
        return goodsKeyword;
    }

    public void setGoodsKeyword(String goodsKeyword) {
        this.goodsKeyword = goodsKeyword;
    }

	public String getPlatformCode() {
		return platformCode;
	}

	public void setPlatformCode(String platformCode) {
		this.platformCode = platformCode;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getFirstMission() {
		return firstMission;
	}

	public void setFirstMission(Integer firstMission) {
		this.firstMission = firstMission;
	}

    public Integer getTaskTotal() {
        return taskTotal;
    }

    public void setTaskTotal(Integer taskTotal) {
        this.taskTotal = taskTotal;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
