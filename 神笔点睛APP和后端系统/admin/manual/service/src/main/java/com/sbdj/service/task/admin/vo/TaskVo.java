package com.sbdj.service.task.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: Task
 * @Description: 任务扩展类
 * @author: 黄天良
 * @date: 2018年11月30日 下午4:57:54
 */
@ApiModel(value = "任务扩展类",description = "用于接收查询到的扩展的任务数据")
public class TaskVo {
	@ApiModelProperty(value = "任务ID")
	private Long id;

	@ApiModelProperty(value = "所属机构id")
	private Long orgId;

	@ApiModelProperty(value = "任务编号，后台自动生成")
	private String taskNumber;
	
	@ApiModelProperty(value = "所属店铺id")
	private Long sellerShopId;
	
	@ApiModelProperty(value = "所属平台id")
	private Long platformId;
	
	@ApiModelProperty(value = "所属商品id")
	private Long goodsId;
	
	@ApiModelProperty(value = "商品搜索价格")
	private BigDecimal showPrice;
	
	@ApiModelProperty(value = "商品优惠价格")
	private BigDecimal prefPrice;
	
	@ApiModelProperty(value = "商品实付价格")
	private BigDecimal realPrice;
	
	@ApiModelProperty(value = "商品包裹/商品套餐")
	private String packages;
	
	@ApiModelProperty(value = "佣金")
	private BigDecimal brokerage;
	
	@ApiModelProperty(value = "服务费")
	private BigDecimal serverPrice;
	
	@ApiModelProperty(value = "任务总数")
	private Integer taskTotal = 0;
	
	@ApiModelProperty(value = "任务剩余数")
	private Integer tesidueNum = 0;
	
	@ApiModelProperty(value = "任务待审核数")
	private Integer notAuditedNum = 0;
	
	@ApiModelProperty(value = "任务已审核数")
	private Integer yesAuditedNum = 0;

	@ApiModelProperty(value = "任务待操作数")
	private Integer stayOprNum = 0;
	
	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;
	
	@ApiModelProperty(value = "发布时间")
	private Date releaseTime;
	
	@ApiModelProperty(value = "定时发布")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date timingTime;
	
	@ApiModelProperty(value = "商家要求")
	private String sellerAsk;

	@ApiModelProperty(value = "状态{R：运行状态，D：删除状态，C：草稿，N：终止, NC: 未完成草稿}")
	private String status;
	
	@ApiModelProperty(value = "置顶")
	private Integer top;

	
	@ApiModelProperty(value = "店铺名称")
	private String shopName;
	
	@ApiModelProperty(value = "店铺联系人")
	private String userName;
	
	@ApiModelProperty(value = "商品链接")
	private String link;
	
	@ApiModelProperty(value = "商品标题")
	private String title;
	
	@ApiModelProperty(value = "商品主图")
	private String imgLink;
	
	
	@ApiModelProperty(value = "平台名称")
	private String platformName;
	
	@ApiModelProperty(value = "任务类型")
	private Long taskTypeId;
	
	@ApiModelProperty(value = "任务类型名称")
	private String taskTypeName;
	
	@ApiModelProperty(value = "任务类型编码")
	private String taskTypeCode;

	@ApiModelProperty(value = "商品是否打标{0：否},{1：是}]（对应seller_shop_goods -> is_card）")
	private Integer isCard;

	@ApiModelProperty(value = "商品查找关键词（查文截图），根据商品表的keyword字符进行数据冗余")
	private String goodsKeywords;

	@ApiModelProperty(value = "商品类目")
	private String category;
	
	public Integer getStayOprNum() {
		return stayOprNum;
	}


	public void setStayOprNum(Integer stayOprNum) {
		this.stayOprNum = stayOprNum;
	}


	public Long getId() {
		return id;
	}


	public String getPackages() {
		return packages;
	}


	public void setPackages(String packages) {
		this.packages = packages;
	}


	public String getTaskTypeName() {
		return taskTypeName;
	}


	public void setTaskTypeName(String taskTypeName) {
		this.taskTypeName = taskTypeName;
	}


	public Long getGoodsId() {
		return goodsId;
	}

	public BigDecimal getShowPrice() {
		return showPrice;
	}

	public BigDecimal getPrefPrice() {
		return prefPrice;
	}

	public BigDecimal getRealPrice() {
		return realPrice;
	}

	public BigDecimal getBrokerage() {
		return brokerage;
	}

	public BigDecimal getServerPrice() {
		return serverPrice;
	}


	public Date getCreateTime() {
		return createTime;
	}

	public Date getReleaseTime() {
		return releaseTime;
	}

	public Date getTimingTime() {
		return timingTime;
	}

	public String getSellerAsk() {
		return sellerAsk;
	}

	public String getStatus() {
		return status;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getTaskNumber() {
		return taskNumber;
	}


	public void setTaskNumber(String taskNumber) {
		this.taskNumber = taskNumber;
	}


	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public void setShowPrice(BigDecimal showPrice) {
		this.showPrice = showPrice;
	}

	public void setPrefPrice(BigDecimal prefPrice) {
		this.prefPrice = prefPrice;
	}

	public void setRealPrice(BigDecimal realPrice) {
		this.realPrice = realPrice;
	}

	public void setBrokerage(BigDecimal brokerage) {
		this.brokerage = brokerage;
	}

	public void setServerPrice(BigDecimal serverPrice) {
		this.serverPrice = serverPrice;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setReleaseTime(Date releaseTime) {
		this.releaseTime = releaseTime;
	}

	public void setTimingTime(Date timingTime) {
		this.timingTime = timingTime;
	}

	public void setSellerAsk(String sellerAsk) {
		this.sellerAsk = sellerAsk;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public Integer getTaskTotal() {
		return taskTotal;
	}

	public void setTaskTotal(Integer taskTotal) {
		this.taskTotal = taskTotal;
	}

	public String getShopName() {
		return shopName;
	}

	public String getPlatformName() {
		return platformName;
	}


	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}


	public Integer getTesidueNum() {
		return tesidueNum;
	}

	public Integer getNotAuditedNum() {
		return notAuditedNum;
	}

	public Integer getYesAuditedNum() {
		return yesAuditedNum;
	}

	public void setTesidueNum(Integer tesidueNum) {
		this.tesidueNum = tesidueNum;
	}

	public void setNotAuditedNum(Integer notAuditedNum) {
		this.notAuditedNum = notAuditedNum;
	}

	public void setYesAuditedNum(Integer yesAuditedNum) {
		this.yesAuditedNum = yesAuditedNum;
	}

	public Long getTaskTypeId() {
		return taskTypeId;
	}

	public void setTaskTypeId(Long taskTypeId) {
		this.taskTypeId = taskTypeId;
	}

	public String getLink() {
		return link;
	}

	public String getTitle() {
		return title;
	}

	public String getImgLink() {
		return imgLink;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setImgLink(String imgLink) {
		this.imgLink = imgLink;
	}


	public String getTaskTypeCode() {
		return taskTypeCode;
	}


	public void setTaskTypeCode(String taskTypeCode) {
		this.taskTypeCode = taskTypeCode;
	}


	public Long getSellerShopId() {
		return sellerShopId;
	}


	public void setSellerShopId(Long sellerShopId) {
		this.sellerShopId = sellerShopId;
	}


	public Long getOrgId() {
		return orgId;
	}


	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}


	public Integer getTop() {
		return top;
	}


	public void setTop(Integer top) {
		this.top = top;
	}

	public Long getPlatformId() {
		return platformId;
	}

	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}


	public Integer getIsCard() {
		return isCard;
	}


	public void setIsCard(Integer isCard) {
		this.isCard = isCard;
	}

	public String getGoodsKeywords() {
		return goodsKeywords;
	}


	public void setGoodsKeywords(String goodsKeywords) {
		this.goodsKeywords = goodsKeywords;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
