package com.sbdj.service.task.app.queue;



import com.sbdj.core.util.RedisUtil;
import com.sbdj.core.util.StrUtil;

import java.text.MessageFormat;

/**
 * @ClassName: TaskQueue
 * @Description: 任务相关的队列常量
 * @author: 黄天良
 * @date: 2018年12月27日 上午10:42:40
 */
public class TaskQueue {

    private TaskQueue() {
    }

    /**
     * "TASK-{0}-{1}-{2}" >>> "TASK-{所属机构}-{所属任务类型}-{所属任务}"
     */
    public static final String TASK_QUEUE_KEY = "TASK-{0}-{1}-{2}";

    public static final String TASK_RESIDUE_TOTAL = "TRT_{0}_{1}_{2}";

    /**
     * 缓存任务可领取数key {@value}
     */
    public static final String TASK_CACHE_KEY = "TRT_{0}";

    public static String getTaskQueueKey(String key, String... value) {
        return MessageFormat.format(key, (Object) value);
    }

    public static String getTaskQueueKey(String key, Object... obj) {
        return MessageFormat.format(key, obj);
    }

    public static String getTaskQueueKey(String key, Long... value) {
        return MessageFormat.format(key, (Object) value);
    }

    public static String getTaskQueueKey(String key, int... value) {
        return MessageFormat.format(key, value);
    }

    /**
     * Created by Htl to 2019/05/17 <br/>
     * 根据参数生成缓存key <br/>
     *
     * @param orgId      机构id
     * @param taskId     任务id
     * @param taskTypeId 任务类型id
     */
    public static String getCacheKey(Long orgId, Long taskId, Long taskTypeId) {
        // 获取缓存key
        String cacheKey = getTaskQueueKey(TASK_RESIDUE_TOTAL, orgId, taskTypeId, taskId);
        return cacheKey;
    }

    /**
     * Created by Htl to 2019/05/17 <br/>
     *
     * @param str
     * @return
     */
    public static String getCacheKey(String str) {
        // 获取缓存key
        return getTaskQueueKey(TASK_CACHE_KEY, str);
    }

    /**
     * Created by Htl to 2019/05/17 <br/>
     *

     * @return
     */
    public static String getCacheKey(Object obj) {
        // 获取缓存key
        return getTaskQueueKey(TASK_CACHE_KEY, obj);
    }

    /**
     * @param key
     * @param value
     * @return void 返回类型
     * @Title: leftPush
     * @Description: 往队列头部插入一个元素
     */
    public static void leftPush(String key, Long value) {
        RedisUtil.leftPush(key, value);
    }


    /**
     * @param key
     * @param value
     * @return void 返回类型
     * @Title: leftPush
     * @Description: 往队列头部插入一个元素
     */
    public static void leftPush(String key, String value) {
        RedisUtil.leftPush(key, value);
    }

    /**
     * @param key
     * @param value
     * @return void 返回类型
     * @Title: rightPush
     * @Description:从尾部插入一个元素
     */
    public static void rightPush(String key, Long value) {
        RedisUtil.rightPush(key, value);
    }

    /**
     * @param key
     * @param value
     * @return void 返回类型
     * @Title: rightPush
     * @Description:从尾部插入一个元素
     */
    public static void rightPush(String key, String value) {
        RedisUtil.rightPush(key, value);
    }

    /**
     * @param key
     * @return void 返回类型
     * @Title: rightPop
     * @Description: 根据key数据弹出队列
     */
    public static void rightPop(String key) {
        RedisUtil.rightPop(key);
    }

    /**
     * Created by Htl to 2019/05/17 <br/>
     * 将发布的任务可领取的任务数量，缓存在redis中，减少db的压力（原子递增）<br/>
     *
     * @param orgId      机构id
     * @param taskId     任务id
     * @param taskTypeId 任务类型id
     * @return
     * @throws Exception
     */
    public static long incr(Long orgId, Long taskId, Long taskTypeId) throws Exception {
        if (StrUtil.isNull(orgId) || StrUtil.isNull(taskId) || StrUtil.isNull(taskTypeId)) {
            throw new Exception("缺少参数...!");
        }
        // 获取缓存key
        String cacheKey = getTaskQueueKey(TASK_RESIDUE_TOTAL, orgId, taskTypeId, taskId);
        if (StrUtil.isNull(cacheKey)) {
            throw new Exception("获取缓存key失败!");
        }
        return RedisUtil.incr(cacheKey, 1L);
    }

    /**
     * Created by Htl to 2019/05/17 <br/>
     * 将发布的任务可领取的任务数量，缓存在redis中，减少db的压力（原子递减）<br/>
     *
     * @param orgId      机构id
     * @param taskId     任务id
     * @param taskTypeId 任务类型id
     * @return
     * @throws Exception
     */
    public static long decr(Long orgId, Long taskId, Long taskTypeId) throws Exception {
        if (StrUtil.isNull(orgId) || StrUtil.isNull(taskId) || StrUtil.isNull(taskTypeId)) {
            throw new Exception("缺少参数...!");
        }
        // 获取缓存key
        String cacheKey = getTaskQueueKey(TASK_RESIDUE_TOTAL, orgId, taskTypeId, taskId);
        if (StrUtil.isNull(cacheKey)) {
            throw new Exception("获取缓存key失败!");
        }
        return RedisUtil.decr(cacheKey, 1L);
    }

    public static void main(String[] args) {
        String obj = getCacheKey("OR201905170000001108");
        System.out.println(obj);
    }

}
