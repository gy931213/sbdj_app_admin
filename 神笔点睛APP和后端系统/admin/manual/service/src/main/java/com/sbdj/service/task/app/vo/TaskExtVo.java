package com.sbdj.service.task.app.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: TaskExtVo
 * @Description: 任务扩展
 * @author: 杨凌云
 * @date: 2019/4/27 14:26
 */
@ApiModel
public class TaskExtVo {
    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "任务类型")
    private Long taskTypeId;
    @ApiModelProperty(value = "任务数量")
    private Long taskTotal = 0L;
    @ApiModelProperty(value = "所属店铺id")
    private Long sellerShopId;
    @ApiModelProperty(value = "店铺编码")
    private int shopCode;
    @ApiModelProperty(value = "所属商品id")
    private Long goodsId;
    @ApiModelProperty(value = "商品实付价格")
    private BigDecimal realPrice;
    @ApiModelProperty(value = "定时发布")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date timingTime;
    @ApiModelProperty(value = "商品主图")
    private String imgLink;
    @ApiModelProperty(value = "任务剩余数量")
    private int taskResidueTotal = 0;
    @ApiModelProperty(value = "是否回购 0：否，1：是")
    private int isBuy = 0;
    @ApiModelProperty(value = "是否置顶 0：否，1：是")
    private int top = 0;
    @ApiModelProperty(value = "剩余的时间戳")
    private int timestamp = 0;
    @ApiModelProperty(value = "商品类目")
    private String category;

    public Long getId() {
        return id;
    }

    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public Long getSellerShopId() {
        return sellerShopId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public Date getTimingTime() {
        return timingTime;
    }

    public String getImgLink() {
        return imgLink;
    }


    public int getIsBuy() {
        return isBuy;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSellerShopId(Long sellerShopId) {
        this.sellerShopId = sellerShopId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public void setTimingTime(Date timingTime) {
        this.timingTime = timingTime;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }


    public void setIsBuy(int isBuy) {
        this.isBuy = isBuy;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public Long getTaskTotal() {
        return taskTotal;
    }

    public int getTaskResidueTotal() {
        return taskResidueTotal;
    }

    public void setTaskTotal(Long taskTotal) {
        this.taskTotal = taskTotal;
    }

    public void setTaskResidueTotal(int taskResidueTotal) {
        this.taskResidueTotal = taskResidueTotal;
    }

    public int getShopCode() {
        return shopCode;
    }

    public void setShopCode(int shopCode) {
        this.shopCode = shopCode;
    }

    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
