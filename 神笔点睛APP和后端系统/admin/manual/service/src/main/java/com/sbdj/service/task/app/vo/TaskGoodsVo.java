package com.sbdj.service.task.app.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: TaskGoodsExt
 * @Description: (这里用一句话描述这个类的作用)
 * @author: 陈元祺
 * @date: 2018/12/13 14:38
 */
public class TaskGoodsVo {
	/**
	 * 创建时间
	 **/
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date createTime;

	/**
	 * 商品标题
	 **/
	private String title;

	/**
	 * 商品实付价格
	 **/
	private BigDecimal realPrice;

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BigDecimal getRealPrice() {
		return realPrice;
	}

	public void setRealPrice(BigDecimal realPrice) {
		this.realPrice = realPrice;
	}
}
