package com.sbdj.service.task.app.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName: TaskImgEchoVo
 * @Description: 子任务回显
 * @author: 杨凌云
 * @date: 2019/4/27 14:45
 */
@ApiModel
public class TaskImgEchoVo {
    @ApiModelProperty(value = "子任务id")
    private Long id;
    @ApiModelProperty(value = "实付价格")
    private BigDecimal realPrice;
    @ApiModelProperty(value = "手机唯一串号")
    private String imei;
    @ApiModelProperty(value = "店铺名称")
    private String shopName;
    @ApiModelProperty(value = "任务类型编码")
    private String taskTypeCode;
    @ApiModelProperty(value = "任务类型名称")
    private String taskTypeName;
    @ApiModelProperty(value = "状态")
    private String status;
    @ApiModelProperty(value = "图片")
    private String images;
    @ApiModelProperty(value = "判断隔日单第一和第二次传图的区别，false：第一次，true：第二次")
    private boolean flag = false;
    @ApiModelProperty(value = "要提交的图片")
    private List<TaskPictureTypeSettingVo> tptsv;
    // 所属机构id
    private Long orgId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTaskTypeCode() {
        return taskTypeCode;
    }

    public void setTaskTypeCode(String taskTypeCode) {
        this.taskTypeCode = taskTypeCode;
    }

    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public List<TaskPictureTypeSettingVo> getTptsv() {
        return tptsv;
    }

    public void setTptsv(List<TaskPictureTypeSettingVo> tptsv) {
        this.tptsv = tptsv;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getTaskTypeName() {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName) {
        this.taskTypeName = taskTypeName;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

}
