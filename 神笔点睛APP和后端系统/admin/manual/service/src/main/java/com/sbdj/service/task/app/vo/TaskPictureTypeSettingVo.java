package com.sbdj.service.task.app.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by Htl to 2019/06/06 <br/>
 * 任务图片类型设置表
 */
@ApiModel
public class TaskPictureTypeSettingVo implements Serializable {


    /**
     * Created by Htl to 2019/06/10 <br/>
     */
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "编号")
    private Long id;
    @ApiModelProperty(value = "类型名称")
    private String name;
    @ApiModelProperty(value = "类型别名")
    private String alias;
    @ApiModelProperty(value = "类型编码")
    private String code;
    @ApiModelProperty(value = "标志是否相同[{1：是},{0：否}]")
    private int flag;
    @ApiModelProperty(value = "是否是必填[{1：是},{0：否}]")
    private int fill;
    @ApiModelProperty(value = " 排序号")
    private int sort;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFill() {
        return fill;
    }

    public void setFill(int fill) {
        this.fill = fill;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

}
