package com.sbdj.service.task.app.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: TaskSonCommissionTotalApi
 * @Description: (这里用一句话描述这个类的作用)
 * @author: 陈元祺
 * @date: 2019/1/4 15:07
 */
public class TaskSonCommissionTotalApi {
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 佣金总额
     */
    private BigDecimal total;

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
