package com.sbdj.service.task.app.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: TaskSonDetailVo
 * @Description: 子任务详情
 * @author: 杨凌云
 * @date: 2019/4/27 14:38
 */
@ApiModel
public class TaskSonDetailVo {
    @ApiModelProperty(value = "子任务id")
    private Long id;
    @ApiModelProperty(value = "套餐")
    private String packages;
    @ApiModelProperty(value = "关键字名称")
    private String keyword;
    @ApiModelProperty(value = "实付价格")
    private BigDecimal realPrice;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @ApiModelProperty(value = "在A状态过期时间创建时间累加30钟，在F的过期时间是点击操作的时间累加6个小时。")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deadlineTime;
    @ApiModelProperty(value = "平台名称")
    private String platformName;
    @ApiModelProperty(value = "商品标题")
    private String title;
    @ApiModelProperty(value = "商品图片链接")
    private String imgLink;
    @ApiModelProperty(value = "搜索提示")
    private String searchTips;
    @ApiModelProperty(value = "搜索说明")
    private String searchExplain;
    @ApiModelProperty(value = "警告信息")
    private String warnMsg;
    @ApiModelProperty(value = "店铺名称")
    private String shopName;
    @ApiModelProperty(value = "商品优惠价格")
    private BigDecimal prefPrice;
    @ApiModelProperty(value = "商品搜索价格")
    private BigDecimal showPrice;
    @ApiModelProperty(value = "商家要求")
    private String sellerAsk;
    @ApiModelProperty(value = "商品是否打标[{0：否},{1：是},{2：自动}]")
    private Integer isCard;
    @ApiModelProperty(value = "是否标记为已打标状态[{0：否},{1：是}]")
    private Integer isMark;
    @ApiModelProperty(value = "优惠卷链接（图片链接）")
    private String couponLink;
    @ApiModelProperty(value = "是否有优惠卷[{0：否}，{1：是}]")
    private Integer isCoupon;
    @ApiModelProperty(value = "审核人名称")
    private String auditorName;
    @ApiModelProperty(value = "店铺负责人名称")
    private String principalName;
    @ApiModelProperty(value = "商品关键词")
    private String goodsKeyword;
    @ApiModelProperty(value = "货款类型[SQ:申请，DF:垫付]")
    private String paymentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getDeadlineTime() {
        return deadlineTime;
    }

    public void setDeadlineTime(Date deadlineTime) {
        this.deadlineTime = deadlineTime;
    }

    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getImgLink() {
        return imgLink;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }

    public String getSearchTips() {
        return searchTips;
    }

    public void setSearchTips(String searchTips) {
        this.searchTips = searchTips;
    }

    public String getSearchExplain() {
        return searchExplain;
    }

    public void setSearchExplain(String searchExplain) {
        this.searchExplain = searchExplain;
    }

    public String getWarnMsg() {
        return warnMsg;
    }

    public void setWarnMsg(String warnMsg) {
        this.warnMsg = warnMsg;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public BigDecimal getPrefPrice() {
        return prefPrice;
    }

    public void setPrefPrice(BigDecimal prefPrice) {
        this.prefPrice = prefPrice;
    }

    public BigDecimal getShowPrice() {
        return showPrice;
    }

    public void setShowPrice(BigDecimal showPrice) {
        this.showPrice = showPrice;
    }

    public String getSellerAsk() {
        return sellerAsk;
    }

    public void setSellerAsk(String sellerAsk) {
        this.sellerAsk = sellerAsk;
    }

    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }

    public Integer getIsMark() {
        return isMark;
    }

    public void setIsMark(Integer isMark) {
        this.isMark = isMark;
    }

    public Integer getIsCard() {
        return isCard;
    }

    public void setIsCard(Integer isCard) {
        this.isCard = isCard;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getCouponLink() {
        return couponLink;
    }

    public void setCouponLink(String couponLink) {
        this.couponLink = couponLink;
    }

    public Integer getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(Integer isCoupon) {
        this.isCoupon = isCoupon;
    }

    public String getAuditorName() {
        return auditorName;
    }

    public void setAuditorName(String auditorName) {
        this.auditorName = auditorName;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getGoodsKeyword() {
        return goodsKeyword;
    }

    public void setGoodsKeyword(String goodsKeyword) {
        this.goodsKeyword = goodsKeyword;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

}
