package com.sbdj.service.task.app.vo;

/**
 * @ClassName: TaskSonSalesmanApi
 * @Description: (这里用一句话描述这个类的作用)
 * @author: 陈元祺
 * @date: 2019/1/10 11:08
 */
public class TaskSonSalesmanApi {
    /**
     * 业务员名字
     **/
    private String salesmanName;
    /**
     * 业务员id
     **/
    private Long salesmanId;
    /**
     * 子任务id
     **/
    private Long id;
    /**
     * 子任务编号
     **/
    private String taskSonNumber;

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskSonNumber() {
        return taskSonNumber;
    }

    public void setTaskSonNumber(String taskSonNumber) {
        this.taskSonNumber = taskSonNumber;
    }
}
