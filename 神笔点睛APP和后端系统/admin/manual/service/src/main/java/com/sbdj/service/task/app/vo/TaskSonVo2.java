package com.sbdj.service.task.app.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**+++++++++++++++++++++++++++++++++++++++++++++++++++名字有冲突
 * @ClassName: TaskSonVo
 * @Description: 子任务
 * @author: 杨凌云
 * @date: 2019/4/26 15:05
 */
@ApiModel
public class TaskSonVo2 {
    @ApiModelProperty(value = "子任务编号")
    private String taskSonNumber;
    @ApiModelProperty(value = "佣金")
    private BigDecimal brokerage;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
    @ApiModelProperty(value = "平台账号")
    private String onlineid;
    @ApiModelProperty(value = "审核不通过/审核失败/原因")
    private String account;
    @ApiModelProperty(value = "子任务编号")
    private Long id;
    @ApiModelProperty(value = "所属平台id")
    private Long platformId;
    @ApiModelProperty(value = "所属业务员id")
    private Long salesmanId;
    @ApiModelProperty(value = "所属业务员号主id")
    private Long salesmanNumberId;
    @ApiModelProperty(value = "在A状态过期时间创建时间累加30钟，在F的过期时间是点击操作的时间累加6个小时")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deadlineTime;
    @ApiModelProperty(value = "剩余时间")
    private int timestamp;
    /**
     * 分钟
     **/
    private Integer minute;
    /**
     * 小时
     **/
    private Integer hour;
    /**
     * 是否是当天（作用与隔日单）
     **/
    private boolean isDay;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @ApiModelProperty(value = "状态{开始：A， 待提交：F，已审核：T，已传图：PG，被拒绝：AN，已失败：AF")
    private String status;
    @ApiModelProperty(value = "实付价格")
    private BigDecimal realPrice;
    @ApiModelProperty(value = "任务类型（名称）")
    private String taskTypeName;
    @ApiModelProperty(value = "任务代码")
    private String taskCode;
    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    public String getTaskSonNumber() {
        return taskSonNumber;
    }

    public void setTaskSonNumber(String taskSonNumber) {
        this.taskSonNumber = taskSonNumber;
    }

    public BigDecimal getBrokerage() {
        return brokerage;
    }

    public void setBrokerage(BigDecimal brokerage) {
        this.brokerage = brokerage;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOnlineid() {
        return onlineid;
    }

    public void setOnlineid(String onlineid) {
        this.onlineid = onlineid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }

    public Date getDeadlineTime() {
        return deadlineTime;
    }

    public void setDeadlineTime(Date deadlineTime) {
        this.deadlineTime = deadlineTime;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public boolean isDay() {
        return isDay;
    }

    public void setDay(boolean day) {
        isDay = day;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }

    public String getTaskTypeName() {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName) {
        this.taskTypeName = taskTypeName;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

}
