package com.sbdj.service.task.app.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by Htl to 2019/05/28 <br/>
 * 任务类型类
 */
@ApiModel
public class TaskTypeVo {
    @ApiModelProperty(value = "任务类型ID")
    private Long taskTypeId;
    @ApiModelProperty(value = "任务剩余数量")
    private Long taskTotal = 0L;
    @ApiModelProperty(value = "任务类型名称")
    private String TaskTypeName;

    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public Long getTaskTotal() {
        return taskTotal;
    }

    public void setTaskTotal(Long taskTotal) {
        this.taskTotal = taskTotal;
    }

    public String getTaskTypeName() {
        return TaskTypeName;
    }

    public void setTaskTypeName(String taskTypeName) {
        TaskTypeName = taskTypeName;
    }

}
