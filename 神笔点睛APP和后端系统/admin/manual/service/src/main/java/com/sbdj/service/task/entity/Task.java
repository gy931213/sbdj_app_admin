package com.sbdj.service.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 任务主表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="Task对象", description="任务主表")
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构id")
    private Long orgId;

    @ApiModelProperty(value = "任务编号，后台自动生成")
    private String taskNumber;

    @ApiModelProperty(value = "所属平台")
    private Long platformId;

    @ApiModelProperty(value = "所属店铺id")
    private Long sellerShopId;

    @ApiModelProperty(value = "所属商品id")
    private Long goodsId;

    @ApiModelProperty(value = "商品搜索价格")
    private BigDecimal showPrice;

    @ApiModelProperty(value = "商品优惠价格")
    private BigDecimal prefPrice;

    @ApiModelProperty(value = "商品实付价格")
    private BigDecimal realPrice;

    @ApiModelProperty(value = "商品包裹/商品套餐")
    private String packages;

    @ApiModelProperty(value = "佣金")
    private BigDecimal brokerage;

    @ApiModelProperty(value = "服务费")
    private BigDecimal serverPrice;

    @ApiModelProperty(value = "任务类型")
    private Long taskTypeId;

    @ApiModelProperty(value = "任务总数")
    private Integer taskTotal;

    @ApiModelProperty(value = "任务剩余数")
    private Integer taskResidueTotal;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "发布时间")
    private Date releaseTime;

    @ApiModelProperty(value = "定时发布")
    private Date timingTime;

    @ApiModelProperty(value = "商家要求")
    private String sellerAsk;

    @ApiModelProperty(value = "操作人")
    private Long oprId;

    @ApiModelProperty(value = "操作时间")
    private Date oprTime;

    @ApiModelProperty(value = "任务置顶{0：否，1：是}")
    private Integer top = 0;

    @ApiModelProperty(value = "状态{R：运行状态，D：删除状态，C：草稿，N：终止}")
    private String status;

    @ApiModelProperty(value = "商品是否打标{0：否},{1：是}]（对应seller_shop_goods -> is_card）")
    private Integer isCard;

    @ApiModelProperty(value = "商品关键词记录 来自对应商品以及相关修改")
    private String goodsKeywords;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }
    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }
    public Long getSellerShopId() {
        return sellerShopId;
    }

    public void setSellerShopId(Long sellerShopId) {
        this.sellerShopId = sellerShopId;
    }
    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }
    public BigDecimal getShowPrice() {
        return showPrice;
    }

    public void setShowPrice(BigDecimal showPrice) {
        this.showPrice = showPrice;
    }
    public BigDecimal getPrefPrice() {
        return prefPrice;
    }

    public void setPrefPrice(BigDecimal prefPrice) {
        this.prefPrice = prefPrice;
    }
    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }
    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }
    public BigDecimal getBrokerage() {
        return brokerage;
    }

    public void setBrokerage(BigDecimal brokerage) {
        this.brokerage = brokerage;
    }
    public BigDecimal getServerPrice() {
        return serverPrice;
    }

    public void setServerPrice(BigDecimal serverPrice) {
        this.serverPrice = serverPrice;
    }
    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }
    public Integer getTaskTotal() {
        return taskTotal;
    }

    public void setTaskTotal(Integer taskTotal) {
        this.taskTotal = taskTotal;
    }
    public Integer getTaskResidueTotal() {
        return taskResidueTotal;
    }

    public void setTaskResidueTotal(Integer taskResidueTotal) {
        this.taskResidueTotal = taskResidueTotal;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }
    public Date getTimingTime() {
        return timingTime;
    }

    public void setTimingTime(Date timingTime) {
        this.timingTime = timingTime;
    }
    public String getSellerAsk() {
        return sellerAsk;
    }

    public void setSellerAsk(String sellerAsk) {
        this.sellerAsk = sellerAsk;
    }
    public Long getOprId() {
        return oprId;
    }

    public void setOprId(Long oprId) {
        this.oprId = oprId;
    }
    public Date getOprTime() {
        return oprTime;
    }

    public void setOprTime(Date oprTime) {
        this.oprTime = oprTime;
    }
    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Integer getIsCard() {
        return isCard;
    }

    public void setIsCard(Integer isCard) {
        this.isCard = isCard;
    }
    public String getGoodsKeywords() {
        return goodsKeywords;
    }

    public void setGoodsKeywords(String goodsKeywords) {
        this.goodsKeywords = goodsKeywords;
    }

    @Override
    public String toString() {
        return "Task{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", taskNumber=" + taskNumber +
            ", platformId=" + platformId +
            ", sellerShopId=" + sellerShopId +
            ", goodsId=" + goodsId +
            ", showPrice=" + showPrice +
            ", prefPrice=" + prefPrice +
            ", realPrice=" + realPrice +
            ", packages=" + packages +
            ", brokerage=" + brokerage +
            ", serverPrice=" + serverPrice +
            ", taskTypeId=" + taskTypeId +
            ", taskTotal=" + taskTotal +
            ", taskResidueTotal=" + taskResidueTotal +
            ", createTime=" + createTime +
            ", releaseTime=" + releaseTime +
            ", timingTime=" + timingTime +
            ", sellerAsk=" + sellerAsk +
            ", oprId=" + oprId +
            ", oprTime=" + oprTime +
            ", top=" + top +
            ", status=" + status +
            ", isCard=" + isCard +
            ", goodsKeywords=" + goodsKeywords +
        "}";
    }
}
