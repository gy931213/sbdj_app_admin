package com.sbdj.service.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 任务审核常用的语句
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="TaskCommonSentence对象", description="任务审核常用的语句")
public class TaskCommonSentence implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "所属平台")
    private Long platformId;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "状态,A/D")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "类型 AN:不通过 AF:失败")
    private String type;

    @ApiModelProperty(value = "任务图片类型id，对应task_picture_type_setting，多个以逗号分隔")
    private String tptsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getTptsId() {
        return tptsId;
    }

    public void setTptsId(String tptsId) {
        this.tptsId = tptsId;
    }

    @Override
    public String toString() {
        return "TaskCommonSentence{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", platformId=" + platformId +
            ", content=" + content +
            ", status=" + status +
            ", createTime=" + createTime +
            ", type=" + type +
            ", tptsId=" + tptsId +
        "}";
    }
}
