package com.sbdj.service.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 任务关键词表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="TaskKeyword对象", description="任务关键词表")
public class TaskKeyword implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属任务{一个任务含多个关键词}")
    private Long taskId;

    @ApiModelProperty(value = "关键词名称")
    private String keyword;

    @ApiModelProperty(value = "数量")
    private Integer number;

    @ApiModelProperty(value = "关键词类型[{图片链接：U},{字符串：S}]")
    private String keyType;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "状态[{A：活动状态},{D：删除状态}]")
    private String status;

    @ApiModelProperty(value = "商品是否打标[{0：否},{1：是},{2：自动}]")
    private Integer isCard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
    public String getKeyType() {
        return keyType;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Integer getIsCard() {
        return isCard;
    }

    public void setIsCard(Integer isCard) {
        this.isCard = isCard;
    }

    @Override
    public String toString() {
        return "TaskKeyword{" +
            "id=" + id +
            ", taskId=" + taskId +
            ", keyword=" + keyword +
            ", number=" + number +
            ", keyType=" + keyType +
            ", createTime=" + createTime +
            ", status=" + status +
            ", isCard=" + isCard +
        "}";
    }
}
