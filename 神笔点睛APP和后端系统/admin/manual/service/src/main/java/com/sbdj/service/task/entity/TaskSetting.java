package com.sbdj.service.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * TODO
 * </p>
 *
 * @author Gy
 * @since 2019-12-16
 */
@ApiModel(value="TaskSetting对象", description="任务分配配置表")
public class TaskSetting implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "所属业务员等级")
    private Long salesmanLevelId;

    @ApiModelProperty(value = "一个业务员同一天只能分配号主的个数")
    private Integer allotTotal;

    @ApiModelProperty(value = "一个业务员同一天邀请下线个数")
    private Integer inviteTotal;

    @ApiModelProperty(value = "号主同一天能接任务数")
    private Integer taskTotal;

    @ApiModelProperty(value = "号主近n天任务数不能超过")
    private Integer notBigDays;

    @ApiModelProperty(value = "同店铺大于多少天佣金减半")
    private Integer sameShopBigDays;

    @ApiModelProperty(value = "设置n天数不可接任务")
    private Integer sameBigDays;

    @ApiModelProperty(value = "同店铺小于n天不可接任务")
    private Integer sameShopSmallDays;

    @ApiModelProperty(value = "上级间隔日期")
    private Integer onlineIntervalDay;

    @ApiModelProperty(value = "同级间隔日期")
    private Integer onlineOfflineIntervalDay;

    @ApiModelProperty(value = "下级间隔日期")
    private Integer offlineIntervalDay;

    @ApiModelProperty(value = "类目间隔日期")
    private Integer categoryIntervalDay;

    @ApiModelProperty(value = "拼多多间隔日期")
    private Integer pddIntervalDay;

    @ApiModelProperty(value = "业务排除开关")
    private Integer memberFlag;

    @ApiModelProperty(value = "下级排除开关")
    private Integer offlineFlag;

    @ApiModelProperty(value = "上级排除开关")
    private Integer onlineFlag;

    @ApiModelProperty(value = "同级排除开关")
    private Integer onlineOfflineFlag;

    @ApiModelProperty(value = "类目排除开关")
    private Integer categoryFlag;

    @ApiModelProperty(value = "自动接单开关")
    private Integer autoGetOrderFlag;

    @ApiModelProperty(value = "显示可接单开关")
    private Integer showOrderFlag;

    @ApiModelProperty(value = "上传订单号开关")
    private Integer uploadOrderNumFlag;

    @ApiModelProperty(value = "每一个等级的最高额度值")
    private BigDecimal credit;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * (机构名称) 不持久化
     **/
    @TableField(exist = false)
    private String orgName;
    /**
     * (等级名称) 不持久化
     **/
    @TableField(exist = false)
    private String salesmanLevelName;

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getSalesmanLevelName() {
        return salesmanLevelName;
    }

    public void setSalesmanLevelName(String salesmanLevelName) {
        this.salesmanLevelName = salesmanLevelName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getSalesmanLevelId() {
        return salesmanLevelId;
    }

    public void setSalesmanLevelId(Long salesmanLevelId) {
        this.salesmanLevelId = salesmanLevelId;
    }
    public Integer getAllotTotal() {
        return allotTotal;
    }

    public void setAllotTotal(Integer allotTotal) {
        this.allotTotal = allotTotal;
    }
    public Integer getInviteTotal() {
        return inviteTotal;
    }

    public void setInviteTotal(Integer inviteTotal) {
        this.inviteTotal = inviteTotal;
    }
    public Integer getTaskTotal() {
        return taskTotal;
    }

    public void setTaskTotal(Integer taskTotal) {
        this.taskTotal = taskTotal;
    }
    public Integer getNotBigDays() {
        return notBigDays;
    }

    public void setNotBigDays(Integer notBigDays) {
        this.notBigDays = notBigDays;
    }
    public Integer getSameShopBigDays() {
        return sameShopBigDays;
    }

    public void setSameShopBigDays(Integer sameShopBigDays) {
        this.sameShopBigDays = sameShopBigDays;
    }
    public Integer getSameBigDays() {
        return sameBigDays;
    }

    public void setSameBigDays(Integer sameBigDays) {
        this.sameBigDays = sameBigDays;
    }
    public Integer getSameShopSmallDays() {
        return sameShopSmallDays;
    }

    public void setSameShopSmallDays(Integer sameShopSmallDays) {
        this.sameShopSmallDays = sameShopSmallDays;
    }
    public Integer getOnlineIntervalDay() {
        return onlineIntervalDay;
    }

    public void setOnlineIntervalDay(Integer onlineIntervalDay) {
        this.onlineIntervalDay = onlineIntervalDay;
    }
    public Integer getOnlineOfflineIntervalDay() {
        return onlineOfflineIntervalDay;
    }

    public void setOnlineOfflineIntervalDay(Integer onlineOfflineIntervalDay) {
        this.onlineOfflineIntervalDay = onlineOfflineIntervalDay;
    }
    public Integer getOfflineIntervalDay() {
        return offlineIntervalDay;
    }

    public void setOfflineIntervalDay(Integer offlineIntervalDay) {
        this.offlineIntervalDay = offlineIntervalDay;
    }
    public Integer getMemberFlag() {
        return memberFlag;
    }

    public void setMemberFlag(Integer memberFlag) {
        this.memberFlag = memberFlag;
    }
    public Integer getOfflineFlag() {
        return offlineFlag;
    }

    public void setOfflineFlag(Integer offlineFlag) {
        this.offlineFlag = offlineFlag;
    }
    public Integer getOnlineFlag() {
        return onlineFlag;
    }

    public void setOnlineFlag(Integer onlineFlag) {
        this.onlineFlag = onlineFlag;
    }
    public Integer getOnlineOfflineFlag() {
        return onlineOfflineFlag;
    }

    public void setOnlineOfflineFlag(Integer onlineOfflineFlag) {
        this.onlineOfflineFlag = onlineOfflineFlag;
    }
    public Integer getAutoGetOrderFlag() {
        return autoGetOrderFlag;
    }

    public void setAutoGetOrderFlag(Integer autoGetOrderFlag) {
        this.autoGetOrderFlag = autoGetOrderFlag;
    }
    public Integer getShowOrderFlag() {
        return showOrderFlag;
    }

    public void setShowOrderFlag(Integer showOrderFlag) {
        this.showOrderFlag = showOrderFlag;
    }
    public Integer getUploadOrderNumFlag() {
        return uploadOrderNumFlag;
    }

    public void setUploadOrderNumFlag(Integer uploadOrderNumFlag) {
        this.uploadOrderNumFlag = uploadOrderNumFlag;
    }
    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCategoryFlag() {
        return categoryFlag;
    }

    public void setCategoryFlag(Integer categoryFlag) {
        this.categoryFlag = categoryFlag;
    }

    public Integer getCategoryIntervalDay() {
        return categoryIntervalDay;
    }

    public void setCategoryIntervalDay(Integer categoryIntervalDay) {
        this.categoryIntervalDay = categoryIntervalDay;
    }

    public Integer getPddIntervalDay() {
        return pddIntervalDay;
    }

    public void setPddIntervalDay(Integer pddIntervalDay) {
        this.pddIntervalDay = pddIntervalDay;
    }

    @Override
    public String toString() {
        return "TaskSetting{" +
                "id=" + id +
                ", orgId=" + orgId +
                ", salesmanLevelId=" + salesmanLevelId +
                ", allotTotal=" + allotTotal +
                ", inviteTotal=" + inviteTotal +
                ", taskTotal=" + taskTotal +
                ", notBigDays=" + notBigDays +
                ", sameShopBigDays=" + sameShopBigDays +
                ", sameBigDays=" + sameBigDays +
                ", sameShopSmallDays=" + sameShopSmallDays +
                ", onlineIntervalDay=" + onlineIntervalDay +
                ", onlineOfflineIntervalDay=" + onlineOfflineIntervalDay +
                ", offlineIntervalDay=" + offlineIntervalDay +
                ", categoryIntervalDay=" + categoryIntervalDay +
                ", pddIntervalDay=" + pddIntervalDay +
                ", memberFlag=" + memberFlag +
                ", offlineFlag=" + offlineFlag +
                ", onlineFlag=" + onlineFlag +
                ", onlineOfflineFlag=" + onlineOfflineFlag +
                ", categoryFlag=" + categoryFlag +
                ", autoGetOrderFlag=" + autoGetOrderFlag +
                ", showOrderFlag=" + showOrderFlag +
                ", uploadOrderNumFlag=" + uploadOrderNumFlag +
                ", credit=" + credit +
                ", status='" + status + '\'' +
                ", createTime=" + createTime +
                ", orgName='" + orgName + '\'' +
                ", salesmanLevelName='" + salesmanLevelName + '\'' +
                '}';
    }
}
