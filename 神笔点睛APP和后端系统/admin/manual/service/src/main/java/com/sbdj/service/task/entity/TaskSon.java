package com.sbdj.service.task.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 任务子表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="TaskSon对象", description="任务子表")
public class TaskSon implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "子任务编号")
    private String taskSonNumber;

    @ApiModelProperty(value = "所属平台的订单号")
    private String orderNum;

    @ApiModelProperty(value = "实付价格")
    private BigDecimal realPrice;

    @ApiModelProperty(value = "佣金")
    private BigDecimal brokerage;

    @ApiModelProperty(value = "所属主任务")
    private Long taskId;

    @ApiModelProperty(value = "任务类型")
    private Long taskTypeId;

    @ApiModelProperty(value = "所属平台")
    private Long platformId;

    @ApiModelProperty(value = "所属业务员id")
    private Long salesmanId;

    @ApiModelProperty(value = "所属业务员号主id")
    private Long salesmanNumberId;

    @ApiModelProperty(value = "所属业务员号主手机唯一码")
    private String imei;

    @ApiModelProperty(value = "所属店铺id")
    private Long sellerShopId;

    @ApiModelProperty(value = "所属商品id")
    private Long goodsId;

    @ApiModelProperty(value = "商品包裹/商品套餐")
    private String packages;

    @ApiModelProperty(value = "所属关键字id")
    private Long keywordId;

    @ApiModelProperty(value = "关键字名称")
    private String keyword;

    @ApiModelProperty(value = "图片链接/使用逗号隔开(“,”)")
    private String images;

    @ApiModelProperty(value = "在A状态过期时间创建时间累加30钟，在F的过期时间是点击操作的时间累加6个小时。")
    private Date deadlineTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "是否超时{超时：YES，未超时：NO}")
    private String isTimeOut;

    @ApiModelProperty(value = "审核人id/对应sys_admin表中的id")
    private Long auditorId;

    @ApiModelProperty(value = "审核不通过/审核失败/原因")
    private String account;

    @ApiModelProperty(value = "状态{开始：A， 待提交：F，已审核：T，已传图：AU，被拒绝：AN，已失败：AF  }")
    private String status;

    @ApiModelProperty(value = "商家对单{对单：YES，未对单：NO}")
    private String orderMatch;

    @ApiModelProperty(value = "反审次数")
    private Integer recheck;

    @ApiModelProperty(value = "订单状态-第三方接口响应参数/0：待付款，1：待发货，2：待收货，3：已完成 ，-1：已关闭 /默认值为-3")
    private Integer orderStatus;

    @ApiModelProperty(value = "订单实付金额-第三方接口响应参数")
    private BigDecimal payment;

    @ApiModelProperty(value = "买家昵称-第三方接口响应参数")
    private String buyerNick;

    @ApiModelProperty(value = " 商品是否打标[{0：否},{1：是},{2：自动}]")
    private Integer isCard;

    @ApiModelProperty(value = " 是否标记为已打标状态[{0：否},{1：是}]")
    private Integer isMark;

    @ApiModelProperty(value = "货款类型 DF:垫付 SQ:申请")
    private String paymentType;

    @ApiModelProperty(value = "商品所属关键词，商品的关键词，随机分配词")
    private String goodsKeyword;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public String getTaskSonNumber() {
        return taskSonNumber;
    }

    public void setTaskSonNumber(String taskSonNumber) {
        this.taskSonNumber = taskSonNumber;
    }
    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }
    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }
    public BigDecimal getBrokerage() {
        return brokerage;
    }

    public void setBrokerage(BigDecimal brokerage) {
        this.brokerage = brokerage;
    }
    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }
    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }
    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
    public Long getSellerShopId() {
        return sellerShopId;
    }

    public void setSellerShopId(Long sellerShopId) {
        this.sellerShopId = sellerShopId;
    }
    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }
    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }
    public Long getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(Long keywordId) {
        this.keywordId = keywordId;
    }
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
    public Date getDeadlineTime() {
        return deadlineTime;
    }

    public void setDeadlineTime(Date deadlineTime) {
        this.deadlineTime = deadlineTime;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getIsTimeOut() {
        return isTimeOut;
    }

    public void setIsTimeOut(String isTimeOut) {
        this.isTimeOut = isTimeOut;
    }
    public Long getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(Long auditorId) {
        this.auditorId = auditorId;
    }
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getOrderMatch() {
        return orderMatch;
    }

    public void setOrderMatch(String orderMatch) {
        this.orderMatch = orderMatch;
    }
    public Integer getRecheck() {
        return recheck;
    }

    public void setRecheck(Integer recheck) {
        this.recheck = recheck;
    }
    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }
    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }
    public String getBuyerNick() {
        return buyerNick;
    }

    public void setBuyerNick(String buyerNick) {
        this.buyerNick = buyerNick;
    }
    public Integer getIsCard() {
        return isCard;
    }

    public void setIsCard(Integer isCard) {
        this.isCard = isCard;
    }
    public Integer getIsMark() {
        return isMark;
    }

    public void setIsMark(Integer isMark) {
        this.isMark = isMark;
    }
    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    public String getGoodsKeyword() {
        return goodsKeyword;
    }

    public void setGoodsKeyword(String goodsKeyword) {
        this.goodsKeyword = goodsKeyword;
    }

    @Override
    public String toString() {
        return "TaskSon{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", taskSonNumber=" + taskSonNumber +
            ", orderNum=" + orderNum +
            ", realPrice=" + realPrice +
            ", brokerage=" + brokerage +
            ", taskId=" + taskId +
            ", taskTypeId=" + taskTypeId +
            ", platformId=" + platformId +
            ", salesmanId=" + salesmanId +
            ", salesmanNumberId=" + salesmanNumberId +
            ", imei=" + imei +
            ", sellerShopId=" + sellerShopId +
            ", goodsId=" + goodsId +
            ", packages=" + packages +
            ", keywordId=" + keywordId +
            ", keyword=" + keyword +
            ", images=" + images +
            ", deadlineTime=" + deadlineTime +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", isTimeOut=" + isTimeOut +
            ", auditorId=" + auditorId +
            ", account=" + account +
            ", status=" + status +
            ", orderMatch=" + orderMatch +
            ", recheck=" + recheck +
            ", orderStatus=" + orderStatus +
            ", payment=" + payment +
            ", buyerNick=" + buyerNick +
            ", isCard=" + isCard +
            ", isMark=" + isMark +
            ", paymentType=" + paymentType +
            ", goodsKeyword=" + goodsKeyword +
        "}";
    }
}
