package com.sbdj.service.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 子任务所属图片集合表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="TaskSonImages对象", description="子任务所属图片集合表")
public class TaskSonImages implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属子任务")
    private Long taskSonId;

    @ApiModelProperty(value = "图片链接")
    private String link;

    @ApiModelProperty(value = "图片类型\\\\\\\\对应到，sys_config，sys_config_dtl 表中，sfd_id,对应到详情表")
    private Long sfdId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "状态{A：活动状态，D：删除状态}")
    private String status;

    @ApiModelProperty(value = "图片类型 task_picture_type_setting")
    private Long tptsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    public Long getSfdId() {
        return sfdId;
    }

    public void setSfdId(Long sfdId) {
        this.sfdId = sfdId;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Long getTptsId() {
        return tptsId;
    }

    public void setTptsId(Long tptsId) {
        this.tptsId = tptsId;
    }

    @Override
    public String toString() {
        return "TaskSonImages{" +
            "id=" + id +
            ", taskSonId=" + taskSonId +
            ", link=" + link +
            ", sfdId=" + sfdId +
            ", createTime=" + createTime +
            ", status=" + status +
            ", tptsId=" + tptsId +
        "}";
    }
}
