package com.sbdj.service.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 子任务未通过


 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="TaskSonNotPass对象", description="子任务未通过")
public class TaskSonNotPass implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "所属子任务")
    private Long taskSonId;

    @ApiModelProperty(value = "不通过要上传的图片id，多个图片使用逗号隔开（1,2,3.....n）")
    private String images;

    @ApiModelProperty(value = "不通的原因")
    private String account;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "状态[{活动状态：A},{删除状态：D}]")
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }
    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TaskSonNotPass{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", taskSonId=" + taskSonId +
            ", images=" + images +
            ", account=" + account +
            ", createTime=" + createTime +
            ", status=" + status +
        "}";
    }
}
