package com.sbdj.service.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 子任务作废表
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@ApiModel(value="TaskSonNullify对象", description="子任务作废表")
public class TaskSonNullify implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属机构")
    private Long orgId;

    @ApiModelProperty(value = "子任务id")
    private Long taskSonId;

    @ApiModelProperty(value = "作废业务员")
    private Long salesmanId;

    @ApiModelProperty(value = "业务员号主")
    private Long salesmanNumberId;

    @ApiModelProperty(value = "作废原因")
    private String reasons;

    @ApiModelProperty(value = "作废图片（退款图....）")
    private String images;

    @ApiModelProperty(value = "作废时间")
    private Date createTime;

    @ApiModelProperty(value = "作废状态 (待审核:AU 已审核:T 不通过:AN 已删除:D)")
    private String status;

    @ApiModelProperty(value = "审核不通过/审核失败/原因")
    private String account;

    @ApiModelProperty(value = "审核人")
    private Long auditorId;

    @ApiModelProperty(value = "审核时间")
    private Date auditorTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getTaskSonId() {
        return taskSonId;
    }

    public void setTaskSonId(Long taskSonId) {
        this.taskSonId = taskSonId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Long getSalesmanNumberId() {
        return salesmanNumberId;
    }

    public void setSalesmanNumberId(Long salesmanNumberId) {
        this.salesmanNumberId = salesmanNumberId;
    }
    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }
    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
    public Long getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(Long auditorId) {
        this.auditorId = auditorId;
    }
    public Date getAuditorTime() {
        return auditorTime;
    }

    public void setAuditorTime(Date auditorTime) {
        this.auditorTime = auditorTime;
    }

    @Override
    public String toString() {
        return "TaskSonNullify{" +
            "id=" + id +
            ", orgId=" + orgId +
            ", taskSonId=" + taskSonId +
            ", salesmanId=" + salesmanId +
            ", salesmanNumberId=" + salesmanNumberId +
            ", reasons=" + reasons +
            ", images=" + images +
            ", createTime=" + createTime +
            ", status=" + status +
            ", account=" + account +
            ", auditorId=" + auditorId +
            ", auditorTime=" + auditorTime +
        "}";
    }
}
