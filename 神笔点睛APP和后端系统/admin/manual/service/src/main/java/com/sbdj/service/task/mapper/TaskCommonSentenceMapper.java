package com.sbdj.service.task.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.task.admin.query.TaskCommonSentenceQuery;
import com.sbdj.service.task.admin.vo.TaskCommonSentenceVo;
import com.sbdj.service.task.entity.TaskCommonSentence;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 任务审核常用的语句 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface TaskCommonSentenceMapper extends BaseMapper<TaskCommonSentence> {

    IPage<TaskCommonSentenceVo> findTaskCommonSentenceListByOrgIdAndPlatformIdPageList(IPage<TaskCommonSentenceVo> page, @Param(Constants.WRAPPER)QueryWrapper<TaskCommonSentenceQuery> queryWrapper);
}
