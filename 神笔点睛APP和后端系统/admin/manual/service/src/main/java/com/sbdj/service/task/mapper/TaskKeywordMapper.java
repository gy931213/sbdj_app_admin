package com.sbdj.service.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sbdj.service.task.entity.TaskKeyword;

/**
 * <p>
 * 任务关键词表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface TaskKeywordMapper extends BaseMapper<TaskKeyword> {

    /**
     * 增减搜索词的数量的接口
     *
     * @param id 要增减的id
     * @return
     */
    void reduceTaskKeywordNumberPlus(Long id);
    /**
     * 减少搜索词的数量的接口
     *
     * @param id 任务搜索词id
     * @return void 返回类型
     */
    void reduceTaskKeywordNumberMinus(Long id);
}
