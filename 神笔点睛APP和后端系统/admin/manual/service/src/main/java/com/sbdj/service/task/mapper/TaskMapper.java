package com.sbdj.service.task.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.task.admin.query.TaskQuery;
import com.sbdj.service.task.admin.vo.TaskVo;
import com.sbdj.service.task.app.vo.TaskExtVo;
import com.sbdj.service.task.entity.Task;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 任务主表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface TaskMapper extends BaseMapper<Task> {



    /**
     * 根据状态和任务id获取任务对应的子任务的数据的接口
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<TaskVo> findTaskPageList(IPage<TaskVo> page, @Param(Constants.WRAPPER) QueryWrapper<TaskQuery> queryWrapper);

    /**
     * 根据id增加任务剩余数量的接口
     *
     * @param id 要增减的编号
     */
    void updateTaskResidueTaskResidueTotalPlus(@Param("id") Long id);
    /**
     * 根据id减少任务剩余数量的接口
     *
     * @param id 要减少的编号
     */
    void updateTaskResidueTaskResidueTotalMinus(@Param("id") Long id);
    /**
     * @param taskId  任务id
     * @param shopIds 店铺id
     * @param orgId   所属机构id
     * @param top     是否置顶
     * @return Task 返回类型
     * @Title: findTaskOneBySellerShopId
     * @Description: 根据参数获取任务数据的接口
     */
    Task findTaskOneBySellerShopId(@Param("taskId") Long taskId, @Param("shopIds") String shopIds, @Param("orgId") Long orgId, @Param("top") int top);

    /**
     * 未完成任务接口
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<TaskVo> findTaskUndonePageList(IPage<TaskVo> page, @Param(Constants.WRAPPER) QueryWrapper<TaskQuery> queryWrapper);

    /**
     * 查询模板列表
     * @param page
     * @return
     */
    IPage<TaskVo> findTaskTemplatePageList( IPage<TaskVo> page,@Param(Constants.WRAPPER) QueryWrapper<TaskVo> queryWrapper);

    /**
     * 通过所属机构获取实时任务进度数据
     * @author Yly
     * @date 2019-11-27 14:33
     * @param orgId 机构id
     * @param taskTypeId 任务类型id
     * @return java.util.List<com.sbdj.service.task.entity.Task>
     */
    List<Task> taskSpeedList(@Param("orgId") Long orgId,@Param("taskTypeId") Long taskTypeId);

    /**
     * 获取当前号主能进行分配的任务数据
     * @author Gy
     * @date 2019-11-29 14:08
     * @param page
     * @param queryWrapper
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.task.app.vo.TaskExtVo>
     */
    IPage<TaskExtVo> finaTaskPageList(IPage<TaskExtVo> page,@Param(Constants.WRAPPER) QueryWrapper<Task> queryWrapper);
    /**
     * 根据号主id和规定的天数获取可领取的任务条数
     * @author Gy
     * @date 2019-11-29 15:00
     * @param queryWrapper
     * @return java.util.List<com.sbdj.service.task.app.vo.TaskExtVo>
     */
    List<TaskExtVo> findTaskTypeTaskTotalBySalNumberIdAdnDays(@Param(Constants.WRAPPER) QueryWrapper<Task> queryWrapper);
    /**
     * 获取任务列表数据的接口
     * @author Gy
     * @date 2019-11-29 15:30
     * @param queryWrapper
     * @return
     */
    List<TaskVo> findTaskExtOne(@Param(Constants.WRAPPER) QueryWrapper<Task> queryWrapper);

    /**
     * 根据号主id与规定的天数获取可领取的任务
     * @author Yly
     * @date 2019-12-10 17:14
     * @param queryWrapper 查询条件
     * @return java.util.List<com.sbdj.service.task.app.vo.TaskExtVo>
     */
    List<TaskExtVo> findTaskTypeTaskBySalNumberIdAdnDays(@Param(Constants.WRAPPER) QueryWrapper<Task> queryWrapper);
}
