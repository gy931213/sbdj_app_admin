package com.sbdj.service.task.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sbdj.service.task.app.vo.TaskPictureTypeSettingVo;
import com.sbdj.service.task.entity.TaskPictureTypeSetting;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface TaskPictureTypeSettingMapper extends BaseMapper<TaskPictureTypeSetting> {

    IPage<TaskPictureTypeSetting> findTaskPictureTypeSettingPageList(IPage<TaskPictureTypeSetting> page, @Param(Constants.WRAPPER)QueryWrapper queryWrapper);

    List<TaskPictureTypeSettingVo> findTaskPictureTypeSettingVoByPictureType( @Param(Constants.WRAPPER) QueryWrapper<TaskPictureTypeSetting> queryWrapper);
    /**
     * 根据ID查看图片类型
     * @author Gy
     * @date 2019-11-29 20:18
     * @param queryWrapper
     * @return void
     */
    List<TaskPictureTypeSettingVo> findTaskPictureTypeSettingVoByIds(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);
}
