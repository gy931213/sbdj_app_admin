package com.sbdj.service.task.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sbdj.service.task.entity.TaskSetting;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 任务分配配置表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface TaskSettingMapper extends BaseMapper<TaskSetting> {

    IPage<TaskSetting> findTaskSettingPageList(IPage<TaskSetting> page, @Param(Constants.WRAPPER) QueryWrapper<TaskSetting> queryWrapper);
}
