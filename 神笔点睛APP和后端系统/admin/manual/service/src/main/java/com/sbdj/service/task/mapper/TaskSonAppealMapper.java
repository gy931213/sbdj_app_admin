package com.sbdj.service.task.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonAppealVo;
import com.sbdj.service.task.entity.TaskSonAppeal;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 子任务申诉表 Mapper 接口
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
public interface TaskSonAppealMapper extends BaseMapper<TaskSonAppeal> {
    /**
     * 查询申述列表接口
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<TaskSonAppealVo> findTaskSonAppealByPage(IPage<TaskSonAppealVo> page, @Param(Constants.WRAPPER)QueryWrapper<TaskSonQuery> queryWrapper);
}
