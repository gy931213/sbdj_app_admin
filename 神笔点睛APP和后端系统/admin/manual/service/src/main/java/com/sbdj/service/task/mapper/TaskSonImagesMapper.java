package com.sbdj.service.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sbdj.service.task.admin.vo.TaskSonImagesVo;
import com.sbdj.service.task.entity.TaskSonImages;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 子任务所属图片集合表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface TaskSonImagesMapper extends BaseMapper<TaskSonImages> {

    List<TaskSonImagesVo> findTaskSonImagesList(@Param("taskSonId") Long taskSonId);
}
