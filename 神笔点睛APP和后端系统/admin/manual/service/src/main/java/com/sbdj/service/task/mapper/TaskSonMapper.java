package com.sbdj.service.task.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sbdj.service.finance.scheduler.vo.StatementPercentageVo;
import com.sbdj.service.finance.scheduler.vo.StatementTaskSonVo;
import com.sbdj.service.seller.entity.SellerShop;
import com.sbdj.service.task.admin.query.TaskQuery;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonBeforeDayVo;
import com.sbdj.service.task.admin.vo.TaskSonVo;
import com.sbdj.service.task.app.vo.*;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.scheduler.vo.TaskSonSalesmanVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 任务子表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface TaskSonMapper extends BaseMapper<TaskSon> {
    /**
     * 查询子任务列表相关接口
     * @param page 分页数据
     * @param queryWrapper 查询条件
     * @return
     */
    IPage<TaskSonVo> findTaskSonPageList(IPage<TaskSonVo> page, @Param(Constants.WRAPPER) QueryWrapper<TaskSonQuery> queryWrapper);

    /**
     * 获取昨日的未提交的隔日单
     * @author Yly
     * @date 2019-11-27 11:35
     * @return java.util.List<com.sbdj.service.task.entity.TaskSon>
     */
    List<TaskSon> searchGRDNotSubmitYesterday();

    /**
     * 根据所属机构id获取前一天
     * @author Yly
     * @date 2019-11-27 14:51
     * @param orgId 所属机构
     * @param isBeforeDay 是否为前天
     * @return java.util.List<com.sbdj.service.task.admin.vo.TaskSonBeforeDayVo>
     */
    List<TaskSonBeforeDayVo> findTaskSonIsBeforeDayData(@Param("orgId") Long orgId,@Param("isBeforeDay") boolean isBeforeDay);

    /**
     * 获取已超时的子任务
     * @author Yly
     * @date 2019-11-28 14:34
     * @param query 查询条件
     * @return java.util.List<com.sbdj.service.task.admin.vo.TaskSonVo>
     */
    List<TaskSonVo> findTaskSonIsTimeOut(@Param(Constants.WRAPPER) Wrapper<TaskSonVo> query);

    /**
     * 查询业务员day天是否有做单
     * @author Yly
     * @date 2019-11-28 18:17
     * @param day
     * @return java.util.List<com.sbdj.service.task.scheduler.vo.TaskSonSalesmanVo>
     */
    List<TaskSonSalesmanVo> checkSalesmanIsInactive(@Param("day") Integer day);

    /**
     * 获取昨天每人的佣金总额
     * @author Yly
     * @date 2019-11-28 19:07
     * @param
     * @return java.util.List<com.sbdj.service.finance.scheduler.vo.StatementTaskSonVo>
     */
    List<StatementTaskSonVo> findSalesmanBrokerage();

    /**
     * 查询所有业务员的下级业务员的子任务完成数量
     * @author Yly
     * @date 2019-11-28 19:25
     * @return java.util.List<StatementPercentageApi>
     */
    List<StatementPercentageVo> findSalesmanPercentage();

    List<StatementPercentageVo> findSalesmanPercentageByDateAndJobNumber(@Param("date") String date, @Param("jobNumber") String jobNumber);

    TaskSonVo getTaskSonById(@Param("id") Long id);

    /**
     * 根据业务员ID查询违规任务数据
     * @author Gy
     * @date 2019-11-29 10:40
     * @param page
     * @param salesmanId 业务员ID也就是登陆人ID
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.task.admin.vo.TaskSonVo>
     */
    IPage<TaskSonVo> findTaskSonPageListBySalesmanId(IPage<TaskSonVo> page,@Param("salesmanId") Long salesmanId);

    /**
     * 获取当前号主做过的店铺任务
     * @author Gy
     * @date 2019-11-29 11:17
     * @param salesmanNumberId 号主ID
     * @param days  天数
     * @return java.util.List<com.sbdj.service.seller.entity.SellerShop>
     */
    List<SellerShop> getSellerShopId(@Param("salesmanNumberId") Long salesmanNumberId, @Param("days") int days);

    /**
     * 根据号主id获取当天号主已领取任务数量
     * @author Gy
     * @date 2019-11-29 13:46
     * @param salNumberId
     * @param platformId
     * @return int
     */
    int findDayTaskCountBySalNumberId(@Param("salNumberId") Long salNumberId,@Param("platformId") Long platformId);

    int findNumberTaskCountByDay(@Param("salNumberId") Long salNumberId,@Param("platformId") Long platformId,@Param("day") Integer day);
    /**
     * 获取子任务数据
     * @author Gy
     * @date 2019-11-29 18:26
     * @param page
     * @param queryWrapper
     * @return void
     */
    IPage<TaskSonVo2> findTaskSonVoList(IPage<TaskSonVo2> page, @Param(Constants.WRAPPER) QueryWrapper<TaskSonVo2> queryWrapper);
    /**
     * 获取数据列表数据的接口
     * @author Gy
     * @date 2019-11-29 19:13
     * @param queryWrapper
     * @return void
     */
    List<TaskSonVo> findTaskSonList(@Param(Constants.WRAPPER) QueryWrapper<TaskSon> queryWrapper);
    /**
     * 更新任务截止时间
     * @author Gy
     * @date 2019-11-29 19:27
     * @param taskSonId
     * @param startStatus
     * @param endStatus
     * @param hour
     * @return void
     */
    void updateTaskSonDeadlineTime(@Param("taskSonId") Long taskSonId,@Param("startStatus") String startStatus,@Param("endStatus") String endStatus,@Param("hour") int hour);

    /**
     * TODO
     * @author Gy
     * @date 2019-11-29 19:40
     * @param queryWrapper
     * @return void
     */
    List<TaskImgEchoVo> findDataById(@Param(Constants.WRAPPER) QueryWrapper<TaskImgEchoVo> queryWrapper);
    /**
     * TODO
     * @author Gy
     * @date 2019-11-29 20:01
     * @param queryWrapper
     * @return void
     */
    List<TaskImgEchoVo> findNotPassDataById(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);
    /**
     * 查看任务详细信息
     * @author Gy
     * @date 2019-11-29 20:30
     * @param queryWrapper
     * @return com.sbdj.service.task.app.vo.TaskSonDetailVo
     */
    TaskSonDetailVo findTaskSonDetail(@Param(Constants.WRAPPER) QueryWrapper<TaskSonDetailVo> queryWrapper);

    TaskSonVo findTaskSonExt(@Param("id") Long id);

    /**
     * 查询任务超时接口
     * @param page
     * @param queryWrapper
     */
    IPage<TaskSonVo> findTaskSonOvertimeList(IPage<TaskSonVo> page,@Param(Constants.WRAPPER) QueryWrapper<TaskSonVo> queryWrapper);

    /**
     * 查询佣金总额
     * @author Gy
     * @date 2019-11-30 09:45
     * @param page
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.sbdj.service.task.app.vo.TaskSonCommissionTotalApi>
     */
    IPage<TaskSonCommissionTotalApi> findCommission(IPage<TaskSonCommissionTotalApi> page,@Param("salesmanId") Long salesmanId);

    /**
     *
     * @param taskSonId
     * @param status
     * @param platformId
     * @return
     */
    SellerShop selectStoreData(@Param("taskSonId") Long taskSonId, @Param("status") String status,@Param("platformId") Long platformId);
    /**
     * 店铺同编码的任务数据
     * @author Gy
     * @date 2019-11-30 14:32
     * @param shopCode
     * @param status
     * @param salesmanId
     * @param salNumberId
     * @return java.util.List<com.sbdj.service.task.entity.TaskSon>
     */
    List<TaskSon> sameTaskSon(@Param("shopCode") Integer shopCode,@Param("status") String status,@Param("salesmanId") Long salesmanId,@Param("salNumberId") Long salNumberId);
    /**
     * 查询号主所接任务的对应商品及创建时间
     * @author Gy
     * @date 2019-11-30 14:50
     * @param id
     * @param status
     * @return java.util.List<com.sbdj.service.task.app.vo.TaskGoodsVo>
     */
    List<TaskGoodsVo> findGoodAndTime(@Param("id") long id,@Param("status") String status);


    List<TaskSon> findTaskSonRemoveTheDayTrue(@Param("salesmanId") Long salesmanId,@Param("salesmanNumberId") Long salesmanNumberId,@Param("status") String status);

    List<TaskSon> findTaskSonRemoveTheDayFalse(@Param("salesmanId") Long salesmanId,@Param("salesmanNumberId") Long salesmanNumberId,@Param("statusNullify") String statusNullify);

    List<String> findCategoryByTaskSon(@Param("orgId") Long orgId,@Param("salesmanNumberId") Long salesmanNumberId,@Param("days") Integer days,@Param("platformId") Long platformId);
}
