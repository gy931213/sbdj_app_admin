package com.sbdj.service.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sbdj.service.task.entity.TaskSonNotPass;

/**
 * <p>
 * 子任务未通过

 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface TaskSonNotPassMapper extends BaseMapper<TaskSonNotPass> {

}
