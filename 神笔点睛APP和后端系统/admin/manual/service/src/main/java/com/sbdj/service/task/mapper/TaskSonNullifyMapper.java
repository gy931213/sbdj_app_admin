package com.sbdj.service.task.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonNullifyVo;
import com.sbdj.service.task.entity.TaskSonNullify;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 子任务作废表 Mapper 接口
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface TaskSonNullifyMapper extends BaseMapper<TaskSonNullify> {
    /**
     *查询作废任务信息
     * @param page 分页信息
     * @param queryWrapper 条件构造器
     * @return 作废任务信息集合
     */
    IPage<TaskSonNullifyVo> findTaskSonNullifyByPage(IPage<TaskSonNullifyVo> page, @Param(Constants.WRAPPER) QueryWrapper<TaskSonQuery> queryWrapper);

    /**
     * 查询指定作废子任务信息
     * @param id  作废子任务ID
     * @return
     */
    TaskSonNullifyVo findTaskSonPaymentType(Long id);
    /**
     * 获取作废分页数据
     * @author Gy
     * @date 2019-11-29 17:41
     * @param page
     * @param queryWrapper
     * @return void
     */
    IPage<TaskSonNullifyVo> findTaskSonNullifyPage(IPage<TaskSonNullifyVo> page,@Param(Constants.WRAPPER) QueryWrapper<TaskSonNullifyVo> queryWrapper);
}
