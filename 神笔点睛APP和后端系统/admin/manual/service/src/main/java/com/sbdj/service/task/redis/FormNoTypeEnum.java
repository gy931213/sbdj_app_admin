package com.sbdj.service.task.redis;

/**
 * Created by Htl to 2019/05/17 <br/>
 * 单号生成类型枚举<br/>
 * 注：随机号位于流水号之后,流水号使用redis计数据，每天都是一个新的key,长度不足时则自动补0<br/>
 * 生成规则 =固定前缀+当天日期串+流水号(redis自增，不足长度则补0)+随机数<br/>
 */
public enum FormNoTypeEnum {

    /**
     * 任务单号：OR201905170010187701<br/>
     * 固定前缀：OR <br/>
     * 时间格式：yyyyMMdd <br/>
     * 流水号长度：7(当单日单据较多时可根据业务适当增加流水号长度)<br/>
     * 随机数长度：3 <br/>
     * 总长度：20 <br/>
     */
    OR_ORDER("OR", FormNoConstants.SERIAL_YYYYMMDD_PREFIX, 7, 3, 20),

    /**
     * 子任务单号：OS201905170010187701<br/>
     * 固定前缀：OS <br/>
     * 时间格式：yyyyMMdd <br/>
     * 流水号长度：7 <br/>
     * 随机数长度：3 <br/>
     * 总长度：20 <br/>
     */
    OS_ORDER("OS", FormNoConstants.SERIAL_YYYYMMDD_PREFIX, 7, 3, 20),

    /**
     * 货款单号：ZF201905170010187701<br/>
     * 固定前缀：ZF <br/>
     * 时间格式：yyyyMMdd <br/>
     * 流水号长度：7 <br/>
     * 随机数长度：3 <br/>
     * 总长度：20 <br/>
     */
    ZF_ORDER("ZF", FormNoConstants.SERIAL_YYYYMMDD_PREFIX, 7, 3, 20),

    /**
     * 业务员工号：
     * 固定前缀：JOB <br/>
     * 时间格式："" <br/>
     * 流水号长度：10 <br/>
     * 随机数长度：1 <br/>
     * 总长度：10 <br/>
     */
    JOB_ORDER("JOB", "", 5, 2, 15),

    ;

    /**
     * 单号前缀 为空时填""
     */
    private String prefix;

    /**
     * 时间格式表达式 例如：yyyyMMdd
     */
    private String datePattern;

    /**
     * 流水号长度
     */
    private Integer serialLength;
    /**
     * 随机数长度
     */
    private Integer randomLength;

    /**
     * 总长度
     */
    private Integer totalLength;

    FormNoTypeEnum(String prefix, String datePattern, Integer serialLength, Integer randomLength, Integer totalLength) {
        this.prefix = prefix;
        this.datePattern = datePattern;
        this.serialLength = serialLength;
        this.randomLength = randomLength;
        this.totalLength = totalLength;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

    public Integer getSerialLength() {
        return serialLength;
    }

    public void setSerialLength(Integer serialLength) {
        this.serialLength = serialLength;
    }

    public Integer getRandomLength() {
        return randomLength;
    }

    public void setRandomLength(Integer randomLength) {
        this.randomLength = randomLength;
    }

    public Integer getTotalLength() {
        return totalLength;
    }

    public void setTotalLength(Integer totalLength) {
        this.totalLength = totalLength;
    }

}
