package com.sbdj.service.task.redis.generate.impl;


import com.sbdj.core.util.RedisUtil;
import com.sbdj.service.task.redis.FormNoConstants;
import com.sbdj.service.task.redis.FormNoSerialUtil;
import com.sbdj.service.task.redis.FormNoTypeEnum;
import com.sbdj.service.task.redis.generate.intf.IFormNoGenerateService;
import org.springframework.stereotype.Service;

/**
 * Created by Htl to 2019/05/17 <br/>
 * 单号生成接口实现
 */
@Service
public class FormNoGenerateServiceImpl implements IFormNoGenerateService {

    @Override
    public String generateFormNo(FormNoTypeEnum formNoTypeEnum) {
        //获得单号前缀
        //格式 固定前缀 +时间前缀 示例 ：OR20190101
        String formNoPrefix = FormNoSerialUtil.getFormNoPrefix(formNoTypeEnum);
        //获得缓存key
        String cacheKey = FormNoSerialUtil.getCacheKey(formNoPrefix);
        //获得当日自增数

        Long incrementalSerial = RedisUtil.incr(cacheKey, 1L);// redisCache.incr(cacheKey);
        //设置失效时间 1天
        //redisCache.expire(cacheKey, FormNoConstants.DEFAULT_CACHE_DAYS, TimeUnit.DAYS);
        RedisUtil.expire(cacheKey, FormNoConstants.DEFAULT_CACHE_DAYS);
        //组合单号并补全流水号
        String serialWithPrefix = FormNoSerialUtil.completionSerial(formNoPrefix, incrementalSerial, formNoTypeEnum);
        //补全随机数
        return FormNoSerialUtil.completionRandom(serialWithPrefix, formNoTypeEnum);
    }

}
