package com.sbdj.service.task.redis.generate.intf;


import com.sbdj.service.task.redis.FormNoTypeEnum;

/**
 * Created by Htl to 2019/05/17 <br/>
 * 单号生成接口
 */
public interface IFormNoGenerateService {

    /**
     * 根据单据编号类型 生成单据编号
     *
     * @param formNoTypeEnum 单据编号类型
     */
    String generateFormNo(FormNoTypeEnum formNoTypeEnum);

}
