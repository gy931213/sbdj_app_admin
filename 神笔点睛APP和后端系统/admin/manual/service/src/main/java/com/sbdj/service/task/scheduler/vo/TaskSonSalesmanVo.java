package com.sbdj.service.task.scheduler.vo;

/**
 * <p>
 * 业务员所做任务
 * </p>
 *
 * @author Yly
 * @since 2019-11-28
 */
public class TaskSonSalesmanVo {
    /** 业务员名字 **/
    private String salesmanName;
    /** 业务员id **/
    private Long salesmanId;
    /** 子任务id **/
    private Long id;
    /** 子任务编号 **/
    private String taskSonNumber;

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskSonNumber() {
        return taskSonNumber;
    }

    public void setTaskSonNumber(String taskSonNumber) {
        this.taskSonNumber = taskSonNumber;
    }
}
