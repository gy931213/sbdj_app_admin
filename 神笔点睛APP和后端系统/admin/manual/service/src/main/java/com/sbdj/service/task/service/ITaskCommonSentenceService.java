package com.sbdj.service.task.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.task.admin.query.TaskCommonSentenceQuery;
import com.sbdj.service.task.admin.vo.TaskCommonSentenceVo;
import com.sbdj.service.task.entity.TaskCommonSentence;

import java.util.List;

/**
 * <p>
 * 任务审核常用的语句 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ITaskCommonSentenceService extends IService<TaskCommonSentence> {

    void saveaskCommonSentence(TaskCommonSentence taskCommonSentence);

    void updateTaskCommonSentence(TaskCommonSentence taskCommonSentence);

    void updateTaskCommonSentenceStatus(Long id, String status);

    void deleteTaskCommonSentence(Long id);

    void logicDeleteTaskCommonSentence(Long id);

    TaskCommonSentence findTaskCommonSentenceOne(Long id);

    /**
     * @return java.util.List<com.shenbi.biz.task.domain.TaskCommonSentence> 返回类型
     * @Title: findTaskCommonSentenceList
     * @Description 查询所有的数据
     * @Param []
     * @Author 杨凌云
     * @Date 2019/3/8 10:02
     **/
    List<TaskCommonSentence> findTaskCommonSentenceList();

    /**
     * @return java.util.List<com.shenbi.biz.task.domain.TaskCommonSentence> 返回类型
     * @Title: findTaskCommonSentenceListByOrgIdAndPlatformId
     * @Description 根据机构编号与平台编号查询数据
     * @Param [orgId 机构编号, platformId 平台编号]
     * @Author 杨凌云
     * @Date 2019/3/8 10:01
     **/
    List<TaskCommonSentence> findTaskCommonSentenceListByOrgIdAndPlatformId(Long orgId, Long platformId);

    /**
     * @return java.util.List<com.shenbi.biz.task.domain.TaskCommonSentence> 返回类型
     * @Title: findTaskCommonSentenceListByOrgIdAndPlatformIdAndType
     * @Description 通过机构号、平台编号与类型查询数据
     * @Param [orgId 机构编号, platformId 平台编号, type 类型]
     * @Author 杨凌云
     * @Date 2019/6/8 14:07
     **/
    List<TaskCommonSentence> findTaskCommonSentenceListByOrgIdAndPlatformIdAndType(Long orgId, Long platformId, String type);

    /**
     * @return org.springframework.data.domain.Page<com.shenbi.biz.task.extend.TaskCommonSentenceExt> 返回类型
     * @Title: findTaskCommonSentenceListByOrgIdAndPlatformIdPageList
     * @Description 根据机构编号与平台编号分页查询数据
     * @Param [query 查询参数, pageable 分页组件]
     * @Author 杨凌云
     * @Date 2019/3/8 10:22
     **/
    IPage<TaskCommonSentenceVo> findTaskCommonSentenceListByOrgIdAndPlatformIdPageList(TaskCommonSentenceQuery query, IPage<TaskCommonSentenceVo> page);
}
