package com.sbdj.service.task.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.task.entity.TaskKeyword;

import java.util.List;

/**
 * <p>
 * 任务关键词表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ITaskKeywordService extends IService<TaskKeyword> {
    /**
     * 根据任务id获取任务关键字数量的剩余的数量的接口
     * @param taskId
     * @return
     */
    int getKeywordNumberSurplus(Long taskId);

    /**
     *根据任务编号以及状态值来获取词条
     * @param taskId
     * @param status
     * @return
     */
    List<TaskKeyword> findAllByTaskIdAndStatus(Long taskId, String status);
    /**
     * 根据搜索词id，增减或者减少搜索词数量的接口（加入行锁）
     *
     * @param id   搜索词id
     * @param bool 【true：加1，false：减1】
     * @throws Exception
     */
    void updateTaskKeywordNumberLock(Long id, boolean bool);

    /**
     * @param taskKeyword 要保存的数据对象
     * @return void 返回类型
     * @Title: saveTaskKeyword
     * @Description:保存数据的接口
     */
    void saveTaskKeyword(TaskKeyword taskKeyword);
    /**
     * @param taskId 任务id
     * @Description： 根据任务id物理删除数据的接口
     */
    void deleteByTaskId(Long taskId);

    /**
     * 保存关键字
     * @param taskKeyword
     */
    void saveData(TaskKeyword taskKeyword);

    /**
     * 通过任务编号，状态与天数获取任务关键词
     * @author Yly
     * @date 2019-11-27 11:52
     * @param taskId 任务id
     * @param status 任务状态
     * @return java.util.List<com.sbdj.service.task.entity.TaskKeyword>
     */
    List<TaskKeyword> findTaskKeywordByTaskIdAndStatus(Long taskId, String status);

    /**
     * 根据任务id删除任务对应的搜索词
     * @author Yly
     * @date 2019-11-27 13:32
     * @param taskId 任务id
     * @return void
     */
    void logicDeleteTaskKeywordByTaskId(Long taskId);

    /**
     * @param taskId 任务id
     * @return List<TaskKeyword> 返回类型
     * @Title: findAvailableTaskKeywordByTaskId
     * @Description: 根据任务id获取可领取的搜索词数据的接口【api】
     */
     TaskKeyword findAvailableTaskKeywordByTaskId(Long taskId);
}
