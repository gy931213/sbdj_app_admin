package com.sbdj.service.task.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.task.app.vo.TaskPictureTypeSettingVo;
import com.sbdj.service.task.entity.TaskPictureTypeSetting;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
public interface ITaskPictureTypeSettingService extends IService<TaskPictureTypeSetting> {
    /**
     * @return void 返回类型
     * @Title: saveTaskPictureTypeSetting
     * @Description 增加数据
     * @Param [taskPictureTypeSetting]
     * @Author 杨凌云
     * @Date 2019/6/8 14:52
     **/
    void saveTaskPictureTypeSetting(TaskPictureTypeSetting taskPictureTypeSetting);
    /**
     * @return void 返回类型
     * @Title: deleteTaskPictureTypeSetting
     * @Description 逻辑删除
     * @Param [id, status]
     * @Author 杨凌云
     * @Date 2019/6/8 16:39
     **/
    void deleteTaskPictureTypeSetting(Long id, String status);
    /**
     * @return void 返回类型
     * @Title: disableTaskPictureTypeSetting
     * @Description 禁用
     * @Param [id, status]
     * @Author 杨凌云
     * @Date 2019/6/8 16:45
     **/
    void disableTaskPictureTypeSetting(Long id, String status);
    /**
     * @return void 返回类型
     * @Title: updateTaskPictureTypeSetting
     * @Description 更新数据
     * @Param [taskPictureTypeSetting]
     * @Author 杨凌云
     * @Date 2019/6/8 14:52
     **/
    void updateTaskPictureTypeSetting(TaskPictureTypeSetting taskPictureTypeSetting);
    /**
     * @return org.springframework.data.domain.Page<com.shenbi.biz.task.domain.TaskPictureTypeSetting> 返回类型
     * @Title: findTaskPictureTypeSettingPageList
     * @Description 通过状态查询
     * @Param [orgId, status, pageable]
     * @Author 杨凌云
     * @Date 2019/6/8 15:59
     **/
    IPage<TaskPictureTypeSetting> findTaskPictureTypeSettingPageList(Long orgId, String status, IPage<TaskPictureTypeSetting> page);
    /**
     * @return java.util.List<com.shenbi.biz.task.domain.TaskPictureTypeSetting> 返回类型
     * @Title: findTaskPictureTypeSettingSelect
     * @Description 根据状态获取数据
     * @Param [orgId]
     * @Author 杨凌云
     * @Date 2019/7/1 17:03
     **/
    List<TaskPictureTypeSetting> findTaskPictureTypeSettingSelect(Long orgId);

    /**
     * Created by Htl to 2019/06/06 <br/>
     * 根据图片类型获取数据（适用于APP）
     *
     * @param pictureType 图片类型[{XFD：现付单},{GRD：隔日单}]
     * @param orgId       所属机构id
     * @return
     */
    List<TaskPictureTypeSettingVo> findTaskPictureTypeSettingVoByPictureType(String pictureType, Long orgId);

    /**
     * Created by Htl to 2019/06/06 <br/>
     * 根据图片类型获取数据（适用于APP）
     *
     * @param pictureType 图片类型[{XFD：现付单},{GRD：隔日单}]
     * @return
     */
    public List<TaskPictureTypeSettingVo> findTaskPictureTypeSettingVoByPictureType(String pictureType);

    /**
     * @return java.util.List<com.shenbi.biz.task.domain.vo.TaskPictureTypeSettingVo> 返回类型
     * @Title: findTaskPictureTypeSettingVoByIds
     * @Description 根据编号获取数据
     * @Param [ids]
     * @Author 杨凌云
     * @Date 2019/7/1 17:02
     **/
    List<TaskPictureTypeSettingVo> findTaskPictureTypeSettingVoByIds(String ids);
}
