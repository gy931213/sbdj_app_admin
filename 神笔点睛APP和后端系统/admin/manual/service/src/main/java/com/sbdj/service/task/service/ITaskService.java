package com.sbdj.service.task.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.task.admin.query.TaskQuery;
import com.sbdj.service.task.admin.vo.TaskVo;
import com.sbdj.service.task.app.vo.TaskExtVo;
import com.sbdj.service.task.entity.Task;

import java.util.List;

/**
 * <p>
 * 任务主表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ITaskService extends IService<Task> {

    /**
     * 任务分页数据的接口 获取任务列表数据
     * @param query  任务查询的参数对象
     * @return
     */
    IPage<TaskVo> findTaskPageList(TaskQuery query, IPage<TaskVo> iPage);

    /**
     *获取任务数据的接口
     * @param taskId 任务ID
     * @return
     */

    Task findTaskOne(Long taskId);

    /**
     * 终止任务
     * @param taskId 任务ID
     * @param oprId 操作人ID
     * @param status 状态
     */
    void updateStatusStop(Long taskId, Long oprId, String status);

    /**
     * 运行任务
     * @param taskId
     * @param identify
     * @param status
     */
    void updateStatusRun(Long taskId, Long identify, String status);

    /**
     *运行任务，重新发布不改变定时法布时间
     * @param taskId
     * @param oprId
     * @param status
     */
    void updateTaskStatusRunTimingTime(Long taskId, Long oprId, String status);

    /**
     * 删除数据的接口
     * @param id 要删除的ID
     * @param oprId 操作人Id
     */
    void logiceDeleteTask(Long id, Long oprId);

    /**
     * 任务置顶
     * @param taskId 任务ID
     * @param oprId 操作人ID
     */
    void setTaskTopByTaskId(Long taskId, Long oprId);

    /**
     * @Title: findTaskOneBySellerShopId
     * @Description: 根据参数获取任务数据的接口
     * @param taskId 任务id
     * @param shopIds 店铺id
     * @param orgId  所属机构id
     * @param top    是否置顶
     * @return
     * @return Task 返回类型
     */
    Task findTaskOneBySellerShopId(Long taskId, String shopIds, Long orgId, int top);

    /**
     * 根据任务id增或减任务剩余数据的接口
     *
     * @param taskId 任务id
     * @param bool   【true：增1，false：减1】
     * @throws Exception
     */
    void updateTaskResidueTotalLock(Long taskId, boolean bool);
    /**
     * Created by Yly to 2019/9/11 15:42
     * 更新任务总数据
     *
     * @param taskId    任务编号
     * @param taskTotal 任务总数量
     * @return void
     **/
    void updateTaskTotal(Long taskId, int taskTotal);

    /**
     * @param task 任务对象
     * @return void 返回类型
     * @Title: saveTask
     * @Description: 保存数据的接口
     */
    void saveTask(Task task);

    /**
     * @param taskId 任务id
     * @param oprId  操作人id
     * @return void 返回类型
     * @Title: cancelTaskTopByTaskId
     * @Description: 取消任务置顶的接口
     */
    void cancelTaskTopByTaskId(Long taskId, Long oprId);

    /**
     * 查询未完成任务
     * @param query
     * @return
     */
    IPage<TaskVo> findTaskUndonePageList(TaskQuery query, IPage<TaskVo> page);

    /**
     * 更新任务和任务搜索词接口
     * @param task
     * @param taskKeyword
     * @param time
     */
    void updateTaskUndone(Task task, String taskKeyword, String time);

    /**
     * 查找任务模板列表接口
     * @param query
     * @return
     */
    IPage<TaskVo> findTaskTemplatePageList(TaskQuery query, IPage<TaskVo> page);

    /**
     * 保存模板
     * @param task
     * @param taskKeyword
     */
    void saveTaskTemplate(Task task, String taskKeyword);

    /**
     * 修改模板数据
     * @param task
     * @param taskKeyword
     * @param time
     */
    void updateTaskTemplate(Task task, String taskKeyword, String time);

    /**
     * @return com.shenbi.biz.task.domain.Task 返回类型
     * @Title: findTaskBySellerShopId
     * @Description 根据店铺ID获取对应的任务数据
     * @Param [sellerShopId]
     * @Author 杨凌云
     * @Date 2019/2/17 16:48
     **/
    Task findTaskBySellerShopId(Long sellerShopId, Long goodsId);

    /**
     * 获取昨天未完成的任务
     * @author Yly
     * @date 2019-11-27 11:47
     * @return void
     */
    void updateUndoneTaskByDays();

    /**
     * 终止昨天的任务
     * @author Yly
     * @date 2019-11-27 13:20
     * @return void
     */
    void schedulerStopTask();

    /**
     * Created by Htl to 2019/06/06 <br/>
     * 添加任务数据的接口
     *
     * @param task        任务数据
     * @param taskKeyword 搜索词数据
     */
    void saveTaskAndKeyword(Task task, String taskKeyword);
    /**
     * @param taskId 任务id
     * @return TaskExt 返回类型
     * @Title: findTaskExtOne
     * @Description: 获取任务数据的接口
     */
    TaskVo findTaskExtOne(Long taskId);

    /**
     * @param taskIds
     * @return List<TaskExt> 返回类型
     * @Title: findTaskExtOne
     * @Description: 获取任务列表数据的接口
     */
    List<TaskVo> findTaskExtOne(String taskIds);

    /**
     * 通过所属机构获取实时任务进度数据
     * @author Yly
     * @date 2019-11-27 14:24
     * @param orgId 机构id
     * @return java.util.List<com.sbdj.service.task.entity.Task>
     */
    List<Task> taskSpeedList(Long orgId);

    /**
     * Created by Htl to 2019/06/06 <br/>
     * 更新任务数据的接口
     *
     * @param task        任务数据
     * @param taskKeyword 搜索词数据
     * @param time        定时时间
     */
    void upadteTaskAndKeyword(Task task, String taskKeyword, String time);

    /**
     * @param salNumberId 号主id
     * @param days        天数
     * @return List<TaskExtVo> 返回类型
     * @Title: findTaskTypeTaskTotalBySalNumberIdAdnDays
     * @Description: 根据号主id和规定的天数获取可领取的任务条数的接口【api】
     */
    List<TaskExtVo> findTaskTypeTaskTotalBySalNumberIdAdnDays(Long orgId, Long salNumberId, Long platformId, int days);

    /**
     * @param orgId       机构id
     * @param taskType    任务类型
     * @param salNumberId 号主id
     * @param platformId  所属平台
     * @param days        天数
     * @return Page<TaskExtVo> 返回类型
     * @Title: finaTaskPageList
     * @Description: 获取当前号主能进行分配的任务数据的接口 【api】
     */
    IPage<TaskExtVo> finaTaskPageList(Long orgId, Long taskType, Long salNumberId, Long platformId, int days, IPage<TaskExtVo> page);

    /**
     * @param taskIds
     * @param salesmanId
     * @param salNumberId
     * @param orgId
     * @return String 返回类型
     * @Title: taskReceiveNew
     */
    void taskReceiveNew(String taskIds, Long salesmanId, Long salNumberId, Long orgId);

    /**
     * 保存任务
     * @author Yly
     * @date 2019-12-02 14:28
     * @param taskId 任务id
     * @param number 号主
     * @return void
     */
    void saveTaskSonOne(Long taskId, SalesmanNumber number);

    /**
     * 批量保存任务
     * @author Yly
     * @date 2019-12-02 14:29
     * @param taskIds 任务ids
     * @param number 号主
     * @return void
     */
    void saveTaskSonMuch(String taskIds, SalesmanNumber number);
}
