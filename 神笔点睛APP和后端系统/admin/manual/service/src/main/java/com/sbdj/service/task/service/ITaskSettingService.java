package com.sbdj.service.task.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.task.entity.TaskSetting;

import java.math.BigDecimal;

/**
 * <p>
 * 任务分配配置表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ITaskSettingService extends IService<TaskSetting> {
    /**
     * @return Page<TaskSetting> 返回类型
     * @Title: findTaskSettingPageList
     * @Description: 获取分页数据的接口
     */
    IPage<TaskSetting> findTaskSettingPageList(Long orgId, IPage<TaskSetting> page);

    /**
     * @param orgId      机构id
     * @param salLevelId 等级id
     * @return TaskSetting 返回类型
     * @Title: findTaskSettingByOrgId
     * @Description: 根据机构id获取数据的接口
     */
    TaskSetting findTaskSettingOne(Long orgId, Long salLevelId);
    /**
     * @param setting
     * @return void 返回类型
     * @Title: saveTaskSetting
     * @Description: 保存数据的接口
     */

    /**
     * @param id
     * @return TaskSetting 返回类型
     * @Title: findTaskSettingOne
     * @Description: 根据id获取数据的接口
     */
    TaskSetting findTaskSettingOne(Long id);

    /**
     * 保存配置
     * @param setting
     */
    void saveTaskSetting(TaskSetting setting);

    /**
     * @param orgId      机构id
     * @param salLevelId 等级id
     * @return TaskSetting 返回类型
     * @Title: findTaskSettingByOrgId
     * @Description: 根据机构id获取数据的接口
     */
    TaskSetting findTaskSettingByOrgIdAndSalLevelId(Long orgId, Long salLevelId);
    /**
     * @param setting
     * @return void 返回类型
     * @Title: updateTaskSetting
     * @Description: 更新数据的接口
     */
    void updateTaskSetting(TaskSetting setting);

    /**
     * @param id
     * @return void 返回类型
     * @Title: logicDeleteTaskSetting
     * @Description: 删除数据的接口
     */
    void logicDeleteTaskSetting(Long id);
    /**
     * @return com.shenbi.biz.task.domain.TaskSetting 返回类型
     * @Title: findTaskSettingByCredit
     * @Description 根据机构ID和可用信用余额获取数据
     * @Param [orgId 机构编号, credit 可用信用额度]
     * @Author 杨凌云
     * @Date 2019/3/6 11:12
     **/
    TaskSetting findTaskSettingByOrgIdAndCredit(Long orgId, BigDecimal credit);
    /**
     * @return com.shenbi.biz.task.domain.TaskSetting 返回类型
     * @Title: findTaskSettingMinByOrgId
     * @Description 根据机构ID获取最低信用额度数据
     * @Param [orgId 机构编号]
     * @Author 杨凌云
     * @Date 2019/3/6 11:28
     **/
    TaskSetting findTaskSettingMinByOrgId(Long orgId);
}
