package com.sbdj.service.task.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonAppealVo;
import com.sbdj.service.task.entity.TaskSonAppeal;

/**
 * <p>
 * 子任务申诉表 服务类
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
public interface ITaskSonAppealService extends IService<TaskSonAppeal> {

    /**
     * 查询申述列表数据
     * @param query 查询条件
     * @return
     */
    IPage<TaskSonAppealVo> findTaskSonAppealByPage(TaskSonQuery query, IPage<TaskSonAppealVo> page);
    /**
     * @return void 返回类型
     * @Title: saveTaskSonAppeal
     * @Description 添加数据
     * @Param [taskSonAppeal]
     * @Author 杨凌云
     * @Date 2019/6/23 10:17
     **/
    void saveTaskSonAppeal(TaskSonAppeal taskSonAppeal);
    /**
     * @return void 返回类型
     * @Title: saveTaskSonAppeal
     * @Description 添加数据
     **/
    void saveTaskSonAppeal(Long taskSonId, String reasons, String images);

    /**
     * 修改申述数据
     * @param taskSonAppeal
     */
    void updateTaskSonAppeal(TaskSonAppeal taskSonAppeal);
    /**
     * @return void 返回类型
     * @Title: logicDeleteTaskSonAppealById
     * @Description 通过编号逻辑删除数据
     * @Param [id, oprId]
     * @Author 杨凌云
     * @Date 2019/6/23 10:18
     **/
    void logicDeleteTaskSonAppealById(Long id, Long oprId);

    /**
     *审核
     * @param id
     * @param taskSonId
     * @param beginStatus
     * @param endStatus
     * @param account
     * @param oprId
     */
    void reviewTaskSonAppeal(Long id, Long taskSonId, String beginStatus, String endStatus, String account, Long oprId);
    /**
     * @return void 返回类型
     * @Title: reviewTaskSonAppeal
     * @Description 审核
     * @Param [id, beginStatus, endStatus, oprId, account]
     * @Author 杨凌云
     * @Date 2019/6/23 10:56
     **/
    void reviewTaskSonAppeal(Long id, String beginStatus, String endStatus, String account, Long oprId);
}
