package com.sbdj.service.task.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.task.admin.vo.TaskSonImagesVo;
import com.sbdj.service.task.entity.TaskSonImages;

import java.util.List;

/**
 * <p>
 * 子任务所属图片集合表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ITaskSonImagesService extends IService<TaskSonImages> {
    /**
     * @param taskSonId 子任务id
     * @return void 返回类型
     * @Title: logicDeleteTasksonImgsById
     * @Description: 根据子任务id删除任务对应的图片数据的接口
     */
    public void logicDeleteTasksonImgsByTaskSonId(Long taskSonId);

    /**
     * Created by Htl to 2019/07/08 <br/>
     * 逻辑删除子任务图片信息数据的接口
     *
     * @param taskSonId 子任务id
     * @param tptsId    图片类型id
     */
    public void logicDeleteTasksonImgsByTaskSonId(Long taskSonId, Long tptsId);

    /**
     * @param taskSonId 子任务id
     * @return List<TaskSonImages> 返回类型
     * @Title: findTaskSonImagesList
     * @Description: 根据子任务id获取子任务对应的图片集合数据的接口
     */
    public List<TaskSonImagesVo> findTaskSonImagesList(Long taskSonId);

    /**
     * @param sonImages 要保存的数据
     * @return void 返回类型
     * @Title: saveTaskSonImages
     * @Description: 保存数据的接口
     */
    public void saveTaskSonImages(TaskSonImages sonImages);

    /**
     * Created by Htl to 2019/07/16 <br/>
     * 根据子任务id获取图片资源信息的接口
     *
     * @param taskSonId 子任务id
     * @return
     */
    List<TaskSonImages> findTaskSonImagesByTaskSonId(Long taskSonId);
}
