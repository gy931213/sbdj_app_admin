package com.sbdj.service.task.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.task.entity.TaskSonNotPass;

/**
 * <p>
 * 子任务未通过

 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ITaskSonNotPassService extends IService<TaskSonNotPass> {

    /**
     * 逻辑删除数据的接口
     *
     * @param taskSonId 子任务id
     */
    void logicDeleteTaskSonNotPass(Long taskSonId);

    /**
     * 保存数据
     * @param taskSonNotPass
     */
    void saveTaskSonNotPass(TaskSonNotPass taskSonNotPass);
}
