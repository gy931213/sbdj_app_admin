package com.sbdj.service.task.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonNullifyVo;
import com.sbdj.service.task.entity.TaskSonNullify;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * <p>
 * 子任务作废表 服务类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
public interface ITaskSonNullifyService extends IService<TaskSonNullify> {
    /**
     * Created by Htl to 2019/07/04 <br/>
     * 保存作废的数据的接口
     *
     * @param taskSonId 任务详情的id
     * @param reasons   作废原因
     * @param images    作废图片
     */
    void saveTaskSonNullify(Long taskSonId, String reasons, String images);

    /**
     * 查询作废列表接口
     * @param query
     * @return
     */
    IPage<TaskSonNullifyVo> findTaskSonNullifyByPage(TaskSonQuery query, IPage<TaskSonNullifyVo> page);
    /**
     * @return void 返回类型
     * @Title: reviewTaskSonNullify
     * @Description 审核
     * @Param [id, beginStatus, endStatus, oprId]
     * @Author 杨凌云
     * @Date 2019/6/23 10:57
     **/
    void reviewTaskSonNullify(Long id, String beginStatus, String endStatus, String account, Long oprId);

    /**
     *查找指定作废子任务信息
     * @param tsnId 作废子任务ID
     * @return
     */
    TaskSonNullifyVo findTaskSonPaymentType(Long tsnId);
    /**
     * @return void 返回类型
     * @Title: reviewTaskSonNullify
     * @Description 审核
     * @Param [id, beginStatus, endStatus, oprId]
     * @Author 杨凌云
     * @Date 2019/6/23 10:57
     **/
    void reviewTaskSonNullifyNotRecovering(Long id, String beginStatus, String endStatus, String account, Long oprId);

    /**
     * Created by Htl to 2019/07/23 <br/>
     * API获取分页数据
     *
     * @param salesmanId 当前登录人id（uid）
     * @param status     状态
     * @param page   分页参数
     * @return
     */
    IPage<TaskSonNullifyVo> findTaskSonNullifyPage(Long salesmanId, String status, IPage<TaskSonNullifyVo> page);
    /**
     * 修改作废子任务
     * @author Gy
     * @date 2019-11-29 17:58
     * @param taskSonNullify
     * @return void
     */
    void updateTaskSonNullify(TaskSonNullify taskSonNullify);

    /**
     * @return void 返回类型
     * @Title: logicDeleteTaskSonNullifyById
     * @Description 通过编号逻辑删除
     * @Param [id, oprId]
     * @Author 杨凌云
     * @Date 2019/6/23 10:19
     **/
    void logicDeleteTaskSonNullifyById(Long id, Long oprId);
}
