package com.sbdj.service.task.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sbdj.core.exception.BaseException;
import com.sbdj.service.finance.scheduler.vo.StatementPercentageVo;
import com.sbdj.service.finance.scheduler.vo.StatementTaskSonVo;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonBeforeDayVo;
import com.sbdj.service.task.admin.vo.TaskSonVo;
import com.sbdj.service.task.app.vo.*;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.scheduler.vo.TaskSonSalesmanVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 任务子表 服务类
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
public interface ITaskSonService extends IService<TaskSon> {
    /**
     * 根据状态和任务id获取任务对应的子任务的数据的接口
     * @param taskId
     * @param status
     * @return
     */
    int getTaskSonNumByStatusAndTaskId(Long taskId, String status);


    /**
     * @Title: findTaskSonNumPageList
     * @Description: 获取子任务分页数据的接口
     * @param sonQuery 查询参数
     * @param
     * @return
     * @return Page<TaskExt> 返回类型
     */
    IPage<TaskSonVo> findTaskSonNumPageList(TaskSonQuery sonQuery, IPage<TaskSonVo> page);

    /**
     * 根据子任务Id审核子任务
     * @param ids
     * @param auditorId
     */
    void forceAuditorTaskSonByTaskSonIds(String ids, Long auditorId);
    /**
     * @return void 返回类型
     * @Title: saveDifferPrice
     * @Description 添加差价
     * @Param [realPrice, payment, taskSon, status]
     * @Date 2019/5/30 10:07
     **/
    void saveDifferPrice(BigDecimal realPrice, BigDecimal payment, TaskSon taskSon, String status);
    /**
     * Created by Yly to 2019/9/13 9:47
     * 作废子任务
     *
     * @param
     * @return void
     **/
    void invalidTaskSon(String taskSonIds);
    /**
     * @param id 要获取的id
     * @return TaskSon 返回类型
     * @Title: findTaskSonOne
     * @Description: 获取一条数据的接口
     */
    TaskSon findTaskSonOne(Long id);

    /**
     * Created by Htl on 2019/7/4 <br/>
     * 作废子任务的接口
     *
     * @Param taskSonId 子任务id
     */
    void taskSonNullify(Long taskSonId);

    /**
     * 获取昨日的未提交的隔日单
     * @author Yly
     * @date 2019-11-27 11:35
     * @return java.util.List<com.sbdj.service.task.entity.TaskSon>
     */
    List<TaskSon> searchGRDNotSubmitYesterday();

    /**
     * 更新子任务
     * @author Yly
     * @date 2019-11-27 11:44
     * @param taskSon 子任务
     * @return void
     */
    void updateTaskSon(TaskSon taskSon);

    /**
     * 根据所属机构id获取前一天/当天任务审核状态
     * @author Yly
     * @date 2019-11-27 14:49
     * @param orgId 所属机构
     * @param isBeforeDay 是否为前天
     * @return java.util.List<com.sbdj.service.task.admin.vo.TaskSonBeforeDayVo>
     */
    List<TaskSonBeforeDayVo> findTaskSonIsBeforeDayData(Long orgId, boolean isBeforeDay);

    /**
     * 更新任务是否超时30分钟
     * @author Yly
     * @date 2019-11-28 13:43
     * @return void
     */
    void updateTaskSonIsTimeOut30Minute();

    /**
     * 更新任务是否超时6小时
     * @author Yly
     * @date 2019-11-28 13:57
     * @return void
     */
    void updateTaskSonIsTimeOut6Hour();

    /**
     * 获取已超时的任务数据返回到任务中
     * @author Yly
     * @date 2019-11-28 14:27
     * @return void
     */
    void findTaskSonIsTimeOut();

    /**
     * 查询业务员day天是否有做单
     * @author Yly
     * @date 2019-11-28 18:17
     * @param day
     * @return java.util.List<com.sbdj.service.task.scheduler.vo.TaskSonSalesmanVo>
     */
    List<TaskSonSalesmanVo> checkSalesmanIsInactive(Integer day);

    /**
     * 获取昨天每人的佣金总额
     * @author Yly
     * @date 2019-11-28 19:07
     * @param
     * @return java.util.List<com.sbdj.service.finance.scheduler.vo.StatementTaskSonVo>
     */
    List<StatementTaskSonVo> findSalesmanBrokerage();

    /**
     * 查询所有业务员的下级业务员的子任务完成数量
     * @author Yly
     * @date 2019-11-28 19:25
     * @return java.util.List<StatementPercentageApi>
     */
    List<StatementPercentageVo> findSalesmanPercentage();

    List<StatementPercentageVo> findSalesmanPercentageByDateAndJobNumber(String date, String jobNumber);

    /**
     * @return com.shenbi.biz.task.extend.TaskSonExt 返回类型
     * @Title: getTaskSonById
     * @Description 根据子任务编号获取相关信息(差价截图)
     * @Param [id]
     * @Author 杨凌云
     * @Date 2019/6/11 13:48
     **/
    TaskSonVo getTaskSonById(Long id);

    /**
     * @Title: completedTaskCountBySalManId
     * @Description: (获取业务员已完成任务的数据【api】)
     * @param salManId 业务员id
     * @return  int 返回类型
     */
    long completedTaskCountBySalManId(long salManId);

    /**
     * @param ids       编号
     * @param salId     所属业务员
     * @param status    状态
     * @param isTimeOut 是否超时
     * @return List<TaskSon> 返回类型
     * @Title: findTaskSonList
     * @Description: 获取数据列表数据的接口
     */
    public List<TaskSonVo> findTaskSonList(String ids, Long salId, String status, String isTimeOut);

    /**
     * @param
     * @return Page<TaskSonVo>
     * @Title: findTaskSonPageList
     * @Description: (api : 查询违规任务接口)
     */
    IPage<TaskSonVo> findTaskSonPageList(IPage<TaskSonVo> page, Long salesmanId);

    /**
     * @Title: findGoodAndTime
     * @Description: (api:查询号主所接任务的对应商品及创建时间)
     * @param  id
     * @return  返回类型:List<TaskGoodsExt>
     */
    List<TaskGoodsVo> findGoodAndTime(long id);

    /**
     * @Title: completedTaskCountBySalNumberId
     * @Description: 获取号主已完成任务的总数据【api】
     * @param salNumberId 号主id
     * @return
     * @return int 返回类型
     */
    public int completedTaskCountBySalNumberId(Long salNumberId);

    /**
     * 根据业务员id和号主id获取任务数据，去除当天的数据
     * @param salesmanId 业务员id
     * @param salesmanNumberId 号主id
     * @param isday 是否去除当天[true:是，false：否]
     * @return
     */
    public List<TaskSon> findTaskSonRemoveTheDay(Long salesmanId,Long salesmanNumberId,Boolean isday);
    /**
     * 根据参数获取数据的接口
     * @param salesmanId 业务员id
     * @param salesmanNumberId 号主id
     * @param sellerShopId 店铺id
     * @param goodsId 商品id
     * @return
     */
    public TaskSon findTaskSon(Long salesmanId,Long salesmanNumberId,Long sellerShopId,Long goodsId);

    /**
     * Created by Htl to 2019/05/07 <br/>
     * 根据参数验证否是领取相同的任务数据
     * @param taskSonId 子任务id
     * @param platformId 平台id
     * @param salesmanId 业务员id
     * @param salNumberId 号主id
     * @return
     */
    boolean verifyIsSameTaskSon(Long taskSonId,Long platformId,Long salesmanId,Long salNumberId);

    /**
     * @Title: getUnavailableShopIds
     * @Description: 根据参数获取当前号主不可接的店铺任务数据的接口
     * @param orgId 机构id
     * @param salNumberId 号主id
     * @param days 限制的指定条数内
     * @return
     * @return String 返回类型
     */
    String getUnavailableShopIds(Long orgId, Long salNumberId, int days, Long platformId);

    /**
     * 根据参数获取当前号主不可接的类目下商品任务数据的接口
     * @author Yly
     * @date 2019-12-09 15:06
     * @param orgId 机构id
     * @param salNumberId 号主id
     * @param days 天数
     * @param platformId 平台id
     * @return java.lang.String
     */
    String getUnavailableGoodsIds(Long orgId, Long salNumberId, int days, Long platformId);

    /**
     * @param salesmanId 业务员编号
     * @return int 返回类型
     * @Title: findNotSubmitTaskSonBySalesmanId
     * @Description: 根据业务员编号查询未提交的任务数据的接口
     */
    public int findNotSubmitTaskSonBySalesmanId(Long salesmanId);

    /**
     * Created by Yly to 2019/9/8 10:57
     * 获取业务员编号获取还未审核完的任务数量
     *
     * @param salesmanId 业务员编号
     * @return java.lang.Integer
     **/
    Integer searchTaskSonNotReviewTotalBySalesmanId(Long salesmanId);

    /**
     * @param salesmanId  业务员 id
     * @param salNumberId 号主 id
     * @param platformId  平台 id
     * @return int 返回类型
     * @Title: findSalAllotNumberCount
     * @Description: 根据业务员获取当天分配的号主的个数的接口【api】
     */
    public int findSalAllotNumberCount(Long salesmanId, Long salNumberId, Long platformId);

    /**
     * @param salNumberId 号主id
     * @param platformId  所属平台
     * @return int 返回类型
     * @Title: findDayTaskCountBySalNumberId
     * @Description: 根据号主id获取当天号主已领取任务数量【api】
     */
    public int findDayTaskCountBySalNumberId(Long salNumberId, Long platformId);

    /**
     * @param salNumberId 号主id
     * @param platformId  平台id
     * @title: findNumberTaskCountByDay
     * @description: 根据号主ID获取号主指定天数做任务的数量的接口
     * @author: Yly
     * @return: int
     * @date: 2019-08-02 09:16
     **/
    int findNumberTaskCountByDay(Long salNumberId, Long platformId, Integer day);

    /**
     * Created by Htl to 2019/06/18 <br/>
     * 验证该号主是否接同店铺同商品的任务的接口
     *
     * @param salesmanNumberId 号主id
     * @param shopId           号主id
     * @return
     */
    boolean verifyTaskSon(Long salesmanNumberId, Long shopId);

    /**
     * @param taskId 主任务id
     * @return int 返回类型
     * @Title: taskSonCountByTaskId
     * @Description: 根据任务id判断已领的任务数量（防止任务超领的现象）
     */
    int taskSonCountByTaskId(Long taskId);

    /**
     * @param taskId      任务id
     * @param salesmanId  业务员id
     * @param salNumberId 号主id
     * @return int 返回类型
     * @Title: taskSonCountByParams
     * @Description: 判断当前业务员的号主是否已经领取该任务的接口
     */
    int taskSonCountByParams(Long taskId, Long salesmanId, Long salNumberId);

    /**
     * Created by Htl to 2019/06/18 <br/>
     * 判断是否是复购的号主
     *
     * @param salesmanNumberId 号主id
     * @param shopId           店铺id
     * @param day              天数
     * @return
     */
    boolean repeatPurchaseTaskSon(Long salesmanNumberId, Long shopId, int day);

    /**
     * @param taskSon 任务对象
     * @return void 返回类型
     * @Title: saveTaskSon
     * @Description: 保存数据的接口
     */
    public void saveTaskSon(TaskSon taskSon);

    /**
     * 更新任务数据的接口
     * @param taskSonId 任务id
     * @param salesmanId 业务员id
     * @param startSalNumberId 更改前号主id
     * @param endSalNumberId  更改后号主id
     * @param imei 手机串号
     */
    public void taskSonReplaceSalesmanNumber(Long taskSonId,Long salesmanId,Long startSalNumberId,Long endSalNumberId,String imei);

    /**
     * @param numberId     号主id
     * @param sellerShopId 店铺id
     * @param goodsId      商品id
     * @param day          天数
     * @return TaskSon 返回类型
     * @Title: findSameTaskSon
     * @Description: (查询符合佣金减半条件的子任务)
     */
    TaskSon findSameTaskSon(Long numberId, Long sellerShopId, Long goodsId, int day);

    /**
     * 修改子任务状态
     * @author Gy
     * @date 2019-11-29 17:19
     * @param taskSonId
     * @return void
     */
    void taskSonAppeal(Long taskSonId);
    /**
     * 获取任务详情列表数据的接口
     *
     * @param salesmanId 业务员id
     * @param status     状态
     * @param page   分页参数
     * @return
     */
    IPage<TaskSonVo2> findTaskSonVoList(Long salesmanId, String status, IPage<TaskSonVo2> page);

    /**
     * 生成任务货款的接口（单个，批量）
     *
     * @param salesmanId 业务员id（当前登录id）
     * @param taskSonIds 需要生成的任务id
     * @param applyType  类型：[{SQ ： 申请},{DF ：垫付}]
     */
    void batchGenerateTaskSonPayGoods(Long salesmanId, String taskSonIds, String applyType) throws BaseException;

    /**
     * Created by Htl to 2019/06/11 <br/>
     * 根据订单号判断是否存在，全平台的单号唯一
     *
     * @param orderNum 订单号
     * @return
     */
    boolean isVerifyTaskSonByOrderNum(String orderNum);

    /**
     * Created by Htl to 2019/07/08 <br/>
     * 保存子任务数据并且保存子任务上传的图片接口
     *
     * @param taskSon 子任务数据
     * @param images  图片数据
     */
    void saveTaskSonAndTaskSonImages(TaskSon taskSon, String images);

    /**
     * @param ids
     * @return 返回类型
     * @Title: findDataById
     * @Description: (这里用一句话描述这个方法的作用)
     */
    List<TaskImgEchoVo> findDataById(String ids);
    /**
     * TODO
     * @author Gy
     * @date 2019-11-29 20:00
     * @param ids
     * @return java.util.List<com.sbdj.service.task.app.vo.TaskImgEchoVo>
     */
    List<TaskImgEchoVo> findNotPassDataById(String ids);

    /**
     * Created by Htl to 2019/05/06 <br/>
     * 获取任务详情数据的接口（适用APP中的任务详情数据展示）
     *
     * @param taskSonId  子任务id
     * @param platformId 平台id
     * @return
     */
    TaskSonDetailVo findTaskSonDetail(Long taskSonId, Long platformId);

    /**
     * Created by Htl to 2019/07/24 <br/>
     * 根据id获取数据的接口（适用于商品打标数据）
     *
     * @param id 要获取的id
     * @return
     */
    TaskSonVo findTaskSonExt(Long id);

    /**
     * Created by Htl to 2019/05/06 <br/>
     * 根据子任务id更新打标标识的状态 【0 - 1】
     *
     * @param taskSonId
     */
    void updateTaskSonisMark(Long taskSonId);
    /**
     * Created by Htl to 2019/07/23 <br/>
     * 查看子任务超时任务数据的接口
     *
     * @param salesmanId 当前登录人id（uid）
     * @param page   分页参数
     * @return
     */
    IPage<TaskSonVo> findTaskSonOvertimeList(Long salesmanId, IPage<TaskSonVo> page);
    /**
     * @param salesmanId
     * @return Page<TaskSon> 返回类型
     * @Title: findCommission
     * @Description: (查询佣金总额)
     */
    IPage<TaskSonCommissionTotalApi> findCommission(Long salesmanId, IPage<TaskSonCommissionTotalApi> page);

    /**
     * @param time 时间
     * @return List<TaskSon> 返回类型
     * @Title:findCommission
     * @Description: (查询佣金列表)
     */
    List<TaskSon> findCommission(Long salesmanId, String time);

    /**
     * 审核子任务
     * @author Yly
     * @date 2019-12-02 11:40
     * @param id 子任务id
     * @param auditorId 审核人id
     * @param status 审核状态
     * @param account 原因
     * @param taskSon 子任务
     * @return void
     */
    void auditorTaskSonByTaskSonId(Long id,Long auditorId,String status,String account,TaskSon taskSon);

    /**
     * 重审子任务
     * @author Yly
     * @date 2019-12-02 11:50
     * @param id 子任务id
     * @param auditorId 审核人id
     * @param startStatus 状态前
     * @param endStatus 状态后
     * @param account 原因
     * @param taskSon 子任务
     * @return void
     */
    void auditorTaskSonAgainByTaskSonId(Long id, Long auditorId, String startStatus , String endStatus, String account , TaskSon taskSon);

    /**
     * 批量审核子任务
     * @author Yly
     * @date 2019-12-02 13:21
     * @param ids 子任务ids
     * @param auditorId 审核人
     * @return void
     */
    void auditorTaskSonByTaskSonIds(String ids,Long auditorId);

    /**
     * 更新子任务超时状态
     * @author Yly
     * @date 2019-12-02 13:27
     * @param id 子任务id
     * @param auditorId 审核人
     * @param isTimeOut 是否超时
     * @return void
     */
    void updateTaskSonIsTimeOut(Long id,Long auditorId,String isTimeOut);

    /**
     * 更新子任务的实付款
     * @author Yly
     * @date 2019-12-02 13:30
     * @param id 子任务id
     * @param payment 实付款
     * @return void
     */
    void updateTaskSonPaymentById(Long id,BigDecimal payment);

    /**
     * 通过订单号获取子任务数据
     * @author Yly
     * @date 2019-12-02 13:39
     * @param orderNum 订单号
     * @return com.sbdj.service.task.entity.TaskSon
     */
    TaskSon findTaskSonByOrderNum(String orderNum);

    /**
     * 更新子任务的订单号
     * @author Yly
     * @date 2019-12-02 13:41
     * @param taskSonId 子任务id
     * @param platformId 平台id
     * @param orderNum 订单号
     * @return void
     */
    void updateOrderNumByTaskSonId(Long taskSonId,Long platformId,String orderNum);
}
