package com.sbdj.service.task.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.task.admin.query.TaskCommonSentenceQuery;
import com.sbdj.service.task.admin.vo.TaskCommonSentenceVo;
import com.sbdj.service.task.entity.TaskCommonSentence;
import com.sbdj.service.task.mapper.TaskCommonSentenceMapper;
import com.sbdj.service.task.service.ITaskCommonSentenceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 任务审核常用的语句 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class TaskCommonSentenceServiceImpl extends ServiceImpl<TaskCommonSentenceMapper, TaskCommonSentence> implements ITaskCommonSentenceService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveaskCommonSentence(TaskCommonSentence taskCommonSentence) {
        taskCommonSentence.setCreateTime(new Date());
        taskCommonSentence.setStatus(Status.STATUS_ACTIVITY);

        baseMapper.insert(taskCommonSentence);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskCommonSentence(TaskCommonSentence taskCommonSentence) {
        updateById(taskCommonSentence);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskCommonSentenceStatus(Long id, String status) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新任务审核语句失败,id为空");
        }
        UpdateWrapper<TaskCommonSentence> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", status);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteTaskCommonSentence(Long id) {
        baseMapper.deleteById(id);
    }

    @Override
    public void logicDeleteTaskCommonSentence(Long id) {
        UpdateWrapper<TaskCommonSentence> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Override
    public TaskCommonSentence findTaskCommonSentenceOne(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public List<TaskCommonSentence> findTaskCommonSentenceList() {
        QueryWrapper<TaskCommonSentence> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",Status.STATUS_ACTIVITY);
        queryWrapper.orderByDesc("create_time");
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<TaskCommonSentence> findTaskCommonSentenceListByOrgIdAndPlatformId(Long orgId, Long platformId) {

        QueryWrapper<TaskCommonSentence> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(platformId),"platform_id",platformId);
        queryWrapper.eq(null != orgId && !Number.LONG_ZERO.equals(orgId),"org_id",orgId);
        queryWrapper.orderByDesc("create_time");

        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<TaskCommonSentence> findTaskCommonSentenceListByOrgIdAndPlatformIdAndType(Long orgId, Long platformId, String type) {

        QueryWrapper<TaskCommonSentence> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(platformId),"platform_id",platformId);
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId),"org_id",orgId);
        queryWrapper.eq(StrUtil.isNotNull(type),"type",type).or().isNull("type");
        queryWrapper.orderByDesc("create_time");

        return  baseMapper.selectList(queryWrapper);
    }

    @Override
    public IPage<TaskCommonSentenceVo> findTaskCommonSentenceListByOrgIdAndPlatformIdPageList(TaskCommonSentenceQuery query, IPage<TaskCommonSentenceVo> page) {

        QueryWrapper<TaskCommonSentenceQuery> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(query.getStatus()),"tcs.`status`",query.getStatus());
        queryWrapper.eq(StrUtil.isNotNull(query.getTcsType()),"tcs.`type`",query.getTcsType());
        queryWrapper.eq(StrUtil.isNotNull(query.getPlatformId()) && !Number.LONG_ZERO.equals(query.getPlatformId()),"tcs.platform_id",query.getPlatformId());
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId()),"tcs.org_id",query.getOrgId());
        queryWrapper.like(StrUtil.isNotNull(query.getContent()),"tcs.content",query.getContent());
        queryWrapper.orderByDesc("tcs.create_time");

        return baseMapper.findTaskCommonSentenceListByOrgIdAndPlatformIdPageList(page,queryWrapper);
    }
}
