package com.sbdj.service.task.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.task.entity.TaskKeyword;
import com.sbdj.service.task.mapper.TaskKeywordMapper;
import com.sbdj.service.task.service.ITaskKeywordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 任务关键词表 服务实现类
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class TaskKeywordServiceImpl extends ServiceImpl<TaskKeywordMapper, TaskKeyword> implements ITaskKeywordService {

    @Override
    public int getKeywordNumberSurplus(Long taskId) {
        QueryWrapper<TaskKeyword> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(taskId),"task_id",taskId);
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        List<TaskKeyword> taskKeywords= baseMapper.selectList(queryWrapper);
        //任务个数总和
        int sum = 0;
        for (TaskKeyword taskKeyword : taskKeywords){
            sum+=taskKeyword.getNumber();
        }
        return  sum;
    }


    @Override
    public List<TaskKeyword> findAllByTaskIdAndStatus(Long taskId, String status) {
        if (StrUtil.isNotNull(status)) {
            QueryWrapper<TaskKeyword> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(StrUtil.isNotNull(taskId),"task_id",taskId);
            queryWrapper.eq(StrUtil.isNotNull(status),"status", status);
            return  baseMapper.selectList(queryWrapper);
        } else {
            QueryWrapper<TaskKeyword> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(StrUtil.isNotNull(taskId),"task_id",taskId);
            queryWrapper.eq("status", Status.STATUS_ACTIVITY);
            return  baseMapper.selectList(queryWrapper);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskKeywordNumberLock(Long id, boolean bool) {
        // 锁行
        //TaskKeyword taskKey = iTaskKeywordDao.taskKeyworForUpdateLock(id);
        // bool 【true：加1，false：减1】
        if (bool) {
            baseMapper.reduceTaskKeywordNumberPlus(id);
        } else {
            baseMapper.reduceTaskKeywordNumberMinus(id);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskKeyword(TaskKeyword taskKeyword) {
        taskKeyword.setCreateTime(new Date());
        taskKeyword.setStatus(Status.STATUS_ACTIVITY);
        /*QueryWrapper<TaskKeyword> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(taskKeyword.getTaskId()),"task_id",taskKeyword.getTaskId());
        List<TaskKeyword> keywordList =baseMapper.selectList(queryWrapper);
        if(keywordList.size()==0){
            baseMapper.insert(taskKeyword);
        }else {
            //不同关键字才能存
            for (TaskKeyword  tk: keywordList) {
                if (!tk.getKeyword() .equals(taskKeyword.getKeyword()) ){
                    baseMapper.insert(taskKeyword);
                }
            }
        }*/
        save(taskKeyword);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteByTaskId(Long taskId) {
        QueryWrapper<TaskKeyword> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id", taskId);
        remove(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveData(TaskKeyword taskKeyword) {
        baseMapper.insert(taskKeyword);
    }

    @Override
    public List<TaskKeyword> findTaskKeywordByTaskIdAndStatus(Long taskId, String status) {
        QueryWrapper<TaskKeyword> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id", taskId);
        queryWrapper.eq(StrUtil.isNotNull(status), "status", status);
        return list(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteTaskKeywordByTaskId(Long taskId) {
        UpdateWrapper<TaskKeyword> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_DELETE);
        updateWrapper.eq("task_id", taskId);
        updateWrapper.eq("status", Status.STATUS_ACTIVITY);
        update(updateWrapper);
    }

    @Override
    public TaskKeyword findAvailableTaskKeywordByTaskId(Long taskId) {
        QueryWrapper<TaskKeyword> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(taskId),"task_id",taskId);
        queryWrapper.gt("number",0);
        queryWrapper.eq("status",Status.STATUS_ACTIVITY);
        queryWrapper.last("order by rand() limit 1");
        return baseMapper.selectOne(queryWrapper);
    }
}
