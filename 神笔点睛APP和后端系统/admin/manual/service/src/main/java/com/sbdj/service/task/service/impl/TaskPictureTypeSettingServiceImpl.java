package com.sbdj.service.task.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.task.app.vo.TaskPictureTypeSettingVo;
import com.sbdj.service.task.entity.TaskPictureTypeSetting;
import com.sbdj.service.task.mapper.TaskPictureTypeSettingMapper;
import com.sbdj.service.task.service.ITaskPictureTypeSettingService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class TaskPictureTypeSettingServiceImpl extends ServiceImpl<TaskPictureTypeSettingMapper, TaskPictureTypeSetting> implements ITaskPictureTypeSettingService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskPictureTypeSetting(TaskPictureTypeSetting taskPictureTypeSetting) {
        taskPictureTypeSetting.setCreateTime(new Date());
        baseMapper.insert(taskPictureTypeSetting);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteTaskPictureTypeSetting(Long id, String status) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除任务图片设置失败,id为空");
        }
        UpdateWrapper<TaskPictureTypeSetting> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", status);
        updateWrapper.eq("id",id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void disableTaskPictureTypeSetting(Long id, String status) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("禁用任务图片设置失败,id为空");
        }
        UpdateWrapper<TaskPictureTypeSetting> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", status);
        updateWrapper.eq("id",id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskPictureTypeSetting(TaskPictureTypeSetting taskPictureTypeSetting) {
        baseMapper.updateById(taskPictureTypeSetting);
    }

    @Override
    public IPage<TaskPictureTypeSetting> findTaskPictureTypeSettingPageList(Long orgId, String status,IPage<TaskPictureTypeSetting> page) {
        QueryWrapper<TaskPictureTypeSetting> queryWrapper = new QueryWrapper<>();
        //状态
        queryWrapper.eq(StrUtil.isNotNull(status),"status",status);
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId), "org_id", orgId);
        queryWrapper.ne("status", Status.STATUS_DELETE);
        return page(page, queryWrapper);
    }

    @Override
    public List<TaskPictureTypeSetting> findTaskPictureTypeSettingSelect(Long orgId) {
        QueryWrapper<TaskPictureTypeSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId),"org_id",orgId);
        queryWrapper.eq("status",Status.STATUS_ENABLE);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<TaskPictureTypeSettingVo> findTaskPictureTypeSettingVoByPictureType(String pictureType, Long orgId) {
        QueryWrapper<TaskPictureTypeSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(pictureType),"picture_type",pictureType);
        queryWrapper.eq("status",Status.STATUS_ENABLE);
        queryWrapper.eq(StrUtil.isNotNull(orgId),"org_id",orgId);
        queryWrapper.orderByAsc("sort");

        return  baseMapper.findTaskPictureTypeSettingVoByPictureType(queryWrapper);
    }

    @Override
    public List<TaskPictureTypeSettingVo> findTaskPictureTypeSettingVoByPictureType(String pictureType) {
        QueryWrapper<TaskPictureTypeSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(pictureType),"picture_type",pictureType);
        queryWrapper.eq("status",Status.STATUS_ENABLE);
        queryWrapper.orderByAsc("sort");

        return baseMapper.findTaskPictureTypeSettingVoByPictureType(queryWrapper);
    }

    @Override
    public List<TaskPictureTypeSettingVo> findTaskPictureTypeSettingVoByIds(String ids) {
        List<String> list = StrUtil.getIdsByString(ids);
        if (StrUtil.isNull(list)) {
            return null;
        }
        QueryWrapper<TaskPictureTypeSettingVo> queryWrapper = new QueryWrapper<>();
        //queryWrapper.in(StrUtil.isNotNull(ids),"tpts.id",ids);
        queryWrapper.eq("tpts.status",Status.STATUS_ENABLE);
        queryWrapper.in("tpts.id",  list);
        queryWrapper.orderByAsc("tpts.sort");

        return  baseMapper.findTaskPictureTypeSettingVoByIds(queryWrapper);
    }
}
