package com.sbdj.service.task.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.base.TaskRedis;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.*;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.CopyObjectUtil;
import com.sbdj.core.util.DateUtil;
import com.sbdj.core.util.RedisUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.entity.ConfigPlatform;
import com.sbdj.service.configure.service.IConfigPlatformService;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.member.service.ISalesmanNumberService;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.seller.entity.SellerShop;
import com.sbdj.service.seller.service.ISellerShopService;
import com.sbdj.service.system.entity.SysConfigDtl;
import com.sbdj.service.system.service.ISysConfigDtlService;
import com.sbdj.service.task.admin.query.TaskQuery;
import com.sbdj.service.task.admin.vo.TaskVo;
import com.sbdj.service.task.app.vo.TaskExtVo;
import com.sbdj.service.task.entity.Task;
import com.sbdj.service.task.entity.TaskKeyword;

import com.sbdj.service.task.entity.TaskSetting;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.mapper.TaskMapper;
import com.sbdj.service.task.redis.FormNoTypeEnum;
import com.sbdj.service.task.redis.generate.intf.IFormNoGenerateService;
import com.sbdj.service.task.service.ITaskKeywordService;
import com.sbdj.service.task.service.ITaskService;
import com.sbdj.service.task.service.ITaskSettingService;
import com.sbdj.service.task.service.ITaskSonService;
import com.sbdj.service.task.type.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * <p>
 * 任务主表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements ITaskService {
    private Logger logger = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Autowired
    private ITaskKeywordService iTaskKeywordService;
    @Autowired
    private ITaskSonService iTaskSonService;
    @Autowired
    private ISellerShopService iSellerShopService;
    @Autowired
    private IFormNoGenerateService iFormNoGenerateService;
    @Autowired
    private ISysConfigDtlService iSysConfigDtlService;
    @Autowired
    @Lazy
    private ITaskService iTaskService;
    @Autowired
    private ISalesmanNumberService iSalesmanNumberService;
    @Autowired
    private ISalesmanService iSalesmanService;
    @Autowired
    private ITaskSettingService iTaskSettingService;
    @Autowired
    private IConfigPlatformService iConfigPlatformService;

    @Override
    public IPage<TaskVo> findTaskPageList(TaskQuery query, IPage<TaskVo> page) {
        QueryWrapper<TaskQuery> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("t.status", Status.STATUS_RELEASED,Status.STATUS_STOP,Status.STATUS_DRAFT);
        //所属机构
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId()),"t.org_id",query.getOrgId());
        //店铺名称
        queryWrapper.like(StrUtil.isNotNull(query.getShopName()),"ss.shop_name",query.getShopName());
        //任务编号
        queryWrapper.like(StrUtil.isNotNull(query.getTaskNumber()),"t.task_number",query.getTaskNumber());
        //任务状态
        queryWrapper.eq(StrUtil.isNotNull(query.getStatus()),"t.status",query.getStatus());
        //平台类型
        queryWrapper.eq(StrUtil.isNotNull(query.getPlatformId())&& !Number.LONG_ZERO.equals(query.getPlatformId()),"cp.id",query.getPlatformId());
        //任务类型
        queryWrapper.eq(StrUtil.isNotNull(query.getTaskTypeId()) && !Number.LONG_ZERO.equals(query.getTaskTypeId()),"scd.id",query.getTaskTypeId());
        //历史数据  查询5天前的数据
        Date date = DateUtil.genDayDate(-5, new Date());
        queryWrapper.ge(StrUtil.isNotNull(query.getIsHistory())&&Status.STATUS_NO.equalsIgnoreCase(query.getIsHistory()),"t.create_time",date);
        //发布时间
        //开始时间
        queryWrapper.ge(StrUtil.isNotNull(query.getBeginDate()),"t.timing_time",query.getBeginDate());
        //结束时间
        queryWrapper.le(StrUtil.isNotNull(query.getEndDate()),"t.timing_time",query.getEndDate());
        // 商品类目
        queryWrapper.like(StrUtil.isNotNull(query.getCategory()), "ssg.category", query.getCategory());
        //按时间倒序输出
        queryWrapper.orderByDesc("t.timing_time");
        //获取任务数据
        IPage<TaskVo> pageList=baseMapper.findTaskPageList(page,queryWrapper);

        for (TaskVo taskVo : pageList.getRecords()) {
            // ~获取任务剩余数
            taskVo.setTesidueNum(iTaskKeywordService.getKeywordNumberSurplus(taskVo.getId()));
            // ~获取待操作数
            taskVo.setStayOprNum(iTaskSonService.getTaskSonNumByStatusAndTaskId(taskVo.getId(),"'"+ Status.STATUS_ACTIVITY+"'"));
            // ~获取任务待审核数
            taskVo.setNotAuditedNum(iTaskSonService.getTaskSonNumByStatusAndTaskId(taskVo.getId(),"'"+ Status.STATUS_ALREADY_UPLOAD+"','"+ Status.STATUS_NOT_AUDITED+"'"));
            // ~获取任务已审核数
            String status = "'"+ Status.STATUS_YES_AUDITED+"','"+ Status.STATUS_AUDITED_FAIL+"','"+ Status.STATUS_NOT_PASS+"'";
            taskVo.setYesAuditedNum(iTaskSonService.getTaskSonNumByStatusAndTaskId(taskVo.getId(),status));
        }
        return pageList;
    }


    @Override
    public Task findTaskOne(Long taskId) {
        return getById(taskId);
        /*QueryWrapper<Task> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(taskId),"id",taskId);
        return baseMapper.selectOne(queryWrapper);*/
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateStatusStop(Long taskId, Long oprId, String status) {
        if (StrUtil.isNull(taskId)) {
            throw BaseException.base("停止任务失败,任务id为空");
        }
        UpdateWrapper<Task> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set(StrUtil.isNotNull(oprId), "opr_id", oprId);
        updateWrapper.set(StrUtil.isNotNull(status), "status", status);
        updateWrapper.eq("id",taskId);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateStatusRun(Long taskId, Long identify, String status) {
        if (StrUtil.isNull(taskId)) {
            throw BaseException.base("运行任务失败,任务id为空");
        }
        UpdateWrapper<Task> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set(StrUtil.isNotNull(identify), "opr_id", identify);
        updateWrapper.set(StrUtil.isNotNull(status), "status", status);
        updateWrapper.set("opr_time", new Date());
        updateWrapper.set("timing_time", new Date());
        updateWrapper.eq("id",taskId);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskStatusRunTimingTime(Long taskId, Long oprId, String status) {
        if (StrUtil.isNull(taskId)) {
            throw BaseException.base("更新任务状态失败,任务id为空");
        }
        UpdateWrapper<Task> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("opr_time", new Date());
        updateWrapper.set(StrUtil.isNotNull(oprId), "opr_id", oprId);
        updateWrapper.set(StrUtil.isNotNull(status), "status", status);
        updateWrapper.eq("id",taskId);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logiceDeleteTask(Long id, Long oprId) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("逻辑删除任务失败,任务id为空");
        }
        UpdateWrapper<Task> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("opr_time", new Date());
        updateWrapper.set(StrUtil.isNotNull(oprId), "opr_id", oprId);
        updateWrapper.set( "status", Status.STATUS_DELETE);
        updateWrapper.eq("id",id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void setTaskTopByTaskId(Long taskId, Long oprId) {
        Task t = findTaskOne(taskId);
        if(null == t) {
            throw BaseException.base("任务数据为空");
        }

        // 处理同店铺当天不能置顶多个
        // 根据参数获取
        Task task = findTaskOneBySellerShopId(t.getId(),t.getSellerShopId().toString(), t.getOrgId(), TopType.TOP_YES);
        if(null != task) {
            throw  BaseException.base("相同店铺只能置顶一个！");
        }
        // 处理店铺同简码当天不能置顶多个
        // 获取当前任务的店铺数据
        SellerShop ss = iSellerShopService.getById(t.getSellerShopId());
        if(null == ss) {
            throw BaseException.base("店铺数据为空");
        }

        // 根据店铺id，和店铺简码，所属机构获取店铺信息数据！
        String shopIds = iSellerShopService.findSellerShopByShopIdShopCode(ss.getId(), ss.getShopCode(), ss.getOrgId());
        if(StrUtil.isNotNull(shopIds)) {
            Task task1 = findTaskOneBySellerShopId(t.getId(),shopIds, ss.getOrgId(), TopType.TOP_YES);
            if(null!=task1) {
                throw BaseException.base("相同简码的店铺只能置顶一个！");
            }
        }

        // 任务置顶
        UpdateWrapper<Task> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("opr_time", new Date());
        updateWrapper.set("opr_id", oprId);
        updateWrapper.set("top", TopType.TOP_YES);
        updateWrapper.eq(StrUtil.isNotNull(taskId),"id",taskId);
        update(updateWrapper);
    }


    @Override
    public Task findTaskOneBySellerShopId(Long taskId, String shopIds, Long orgId, int top){
        return  baseMapper.findTaskOneBySellerShopId(taskId,shopIds,orgId,top);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskResidueTotalLock(Long taskId, boolean bool) {
        // 锁行
        //Task task =  iTaskDao.taskForUpdateLock(taskId);
        // bool 【true：增1，false：减1】
        if (bool) {
            baseMapper.updateTaskResidueTaskResidueTotalPlus(taskId);
        } else {
            baseMapper.updateTaskResidueTaskResidueTotalMinus(taskId);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskTotal(Long taskId, int taskTotal) {
        Task task= new Task();
        task.setTaskTotal(taskTotal);
        UpdateWrapper<Task> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",taskId);
        baseMapper.update(task,updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTask(Task task) {
        task.setTaskResidueTotal(task.getTaskTotal());
        task.setTaskNumber(iFormNoGenerateService.generateFormNo(FormNoTypeEnum.OR_ORDER));
        task.setCreateTime(new Date());
        task.setStatus(Status.STATUS_DRAFT);
        baseMapper.insert(task);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void cancelTaskTopByTaskId(Long taskId, Long oprId) {
        Task task = new Task();
        task.setOprTime(new Date());
        task.setOprId(oprId);
        task.setTop(TopType.TOP_NO);
        UpdateWrapper<Task> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",taskId);
        baseMapper.update(task,updateWrapper);
    }

    @Override
    public IPage<TaskVo> findTaskUndonePageList(TaskQuery query,IPage<TaskVo> page) {

        QueryWrapper<TaskQuery> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("t.status", Status.STATUS_UNDONE);
        // 判断是否开启历史查询
        Date date = DateUtil.genDayDate(-5, new Date());
        queryWrapper.ge(StrUtil.isNotNull(query.getIsHistory()) && Status.STATUS_NO.equalsIgnoreCase(query.getIsHistory()),"t.create_time",date);
        // 所属机构
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !query.getOrgId().equals(Number.LONG_ZERO),"t.org_id",query.getOrgId());
        // 状态
        queryWrapper.eq(StrUtil.isNotNull(query.getStatus()),"t.status",query.getStatus());
        // 所属平台
        queryWrapper.eq(StrUtil.isNotNull(query.getPlatformId()) && !Number.LONG_ZERO.equals(query.getPlatformId()),"cp.id",query.getPlatformId());
        // 订单类型
        queryWrapper.eq(StrUtil.isNotNull(query.getTaskTypeId()) && !Number.LONG_ZERO.equals(query.getTaskTypeId()),"t.task_type_id",query.getTaskTypeId());
        // 所属店铺
        queryWrapper.like(StrUtil.isNotNull(query.getShopName()),"ss.shop_name",query.getShopName());
        // 任务订单
        queryWrapper.eq(StrUtil.isNotNull(query.getTaskNumber()),"t.task_number",query.getTaskNumber());
        // 开始时间
        queryWrapper.ge(StrUtil.isNotNull(query.getBeginDate()),"t.create_time",query.getBeginDate());
        // 结束时间
        queryWrapper.le(StrUtil.isNotNull(query.getEndDate()),"t.create_time",query.getEndDate());
        // 商品类目
        queryWrapper.like(StrUtil.isNotNull(query.getCategory()), "ssg.category", query.getCategory());

        return baseMapper.findTaskUndonePageList(page,queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskUndone(Task task, String taskKeyword, String time) {
        Task tobj = findTaskOne(task.getId());
        // 判断对象为空处理
        if (StrUtil.isNull(tobj)) {
            throw BaseException.base("更新失败");
        }
        try {
            CopyObjectUtil.copyProperties(task, tobj);
            // 判断定时发布时间为空，定时发布时间为空，则使用当前时间发布
            // if 定时发布时间不为空
            if (StrUtil.isNotNull(time)) {
                tobj.setTimingTime(DateUtil.stringToDate(time));
            } else {
                tobj.setTimingTime(new Date());
            }
            task.setTaskNumber(iFormNoGenerateService.generateFormNo(FormNoTypeEnum.OR_ORDER));
            tobj.setTaskResidueTotal(tobj.getTaskTotal());
            updateById(tobj);

            // 先删除任务对应的搜索词数据，該操作是物理删除数据
            iTaskKeywordService.deleteByTaskId(task.getId());

           /* Gson gson = new GsonBuilder().create();
            List<TaskKeyword> list2 =  gson.fromJson(taskKeyword,TaskKeyword.class);*/
            // JSON to JSONArray
            List<TaskKeyword> list = JSONArray.parseArray(taskKeyword, TaskKeyword.class);
            // 遍历保存搜索数据
            for (TaskKeyword taskKey : list) {
                taskKey.setTaskId(tobj.getId());
                taskKey.setKeyType(KeywordType.KEYWORD_S);
                iTaskKeywordService.saveTaskKeyword(taskKey);
            }

            if (tobj.getStatus().equals(Status.STATUS_RELEASED)) {
                // Redis_缓存任务可领数量。
                if (!TaskRedis.setTaskTotalCacheKey(tobj.getId(), tobj.getTaskResidueTotal())) {
                    logger.error("===>【任务更新，并且缓存“任务可领数量”失败！要缓存的ID：{}，任务编号为：{}】", tobj.getId(), tobj.getTaskNumber());
                }
            }
        } catch (Exception e) {
            logger.error("===>【任务更新失败】===失败原因：{}", e.getMessage());
            throw BaseException.base("更新失败");
        }
    }

    @Override
    public IPage<TaskVo> findTaskTemplatePageList(TaskQuery query, IPage<TaskVo> page) {
        QueryWrapper<TaskVo> queryWrapper = new QueryWrapper<>();
        // 判断是否开启历史查询
        Date date = DateUtil.genDayDate(-5, new Date());
        queryWrapper.ge(StrUtil.isNotNull(query.getIsHistory()) && Status.STATUS_NO.equalsIgnoreCase(query.getIsHistory()),"t.create_time",date);
        // 所属机构
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId()),"t.org_id",query.getOrgId());
        // 状态
        queryWrapper.eq(StrUtil.isNotNull(query.getStatus()),"t.status",query.getStatus());
        // 所属平台
        queryWrapper.eq(StrUtil.isNotNull(query.getPlatformId()) && !Number.LONG_ZERO.equals(query.getPlatformId()),"cp.id",query.getPlatformId());
        // 订单类型
        queryWrapper.eq(StrUtil.isNotNull(query.getTaskTypeId()) && !Number.LONG_ZERO.equals(query.getTaskTypeId()),"t.task_type_id",query.getTaskTypeId());
        // 所属店铺
        queryWrapper.like(StrUtil.isNotNull(query.getShopName()),"ss.shop_name",query.getShopName());
        // 任务订单
        queryWrapper.eq(StrUtil.isNotNull(query.getTaskNumber()),"t.task_number",query.getTaskNumber());
        // 开始时间
        queryWrapper.ge(StrUtil.isNotNull(query.getBeginDate()),"t.create_time",query.getBeginDate());
        // 结束时间
        queryWrapper.le(StrUtil.isNotNull(query.getEndDate()),"t.create_time",query.getEndDate());
        // 商品类目
        queryWrapper.like(StrUtil.isNotNull(query.getCategory()), "ssg.category", query.getCategory());
        queryWrapper.in("t.status",Status.STATUS_TEMPLATE);

        return  baseMapper.findTaskTemplatePageList(page,queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskTemplate(Task task, String taskKeyword) {
        // 为了防止id存在值，清除id值.
        task.setId(null);
        task.setTaskResidueTotal(task.getTaskTotal());
        task.setTaskNumber(iFormNoGenerateService.generateFormNo(FormNoTypeEnum.OR_ORDER));
        task.setCreateTime(new Date());
        task.setStatus(Status.STATUS_TEMPLATE);
        try {
            // 保存住任务数据
            baseMapper.insert(task);
            // 判断关键词是否为空
            if (StrUtil.isNotNull(task.getId())) {
                List<TaskKeyword> list = JSONArray.parseArray(taskKeyword, TaskKeyword.class);
                for (TaskKeyword taskKey : list) {
                    // 保存任务搜索词
                    taskKey.setTaskId(task.getId());
                    taskKey.setKeyType(KeywordType.KEYWORD_S);
                    taskKey.setCreateTime(new Date());
                    taskKey.setStatus(Status.STATUS_TEMPLATE);
                    iTaskKeywordService.saveData(taskKey);
                }
            }
        } catch (Exception e) {
            logger.error("===>【新增任务失败】===失败原因：{}", e.getMessage());
            throw BaseException.base("新增失败");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskTemplate(Task task, String taskKeyword, String time) {
        Task tobj = findTaskOne(task.getId());
        // 判断对象为空处理
        if (StrUtil.isNull(tobj)) {
            throw BaseException.base("更新失败");
        }
        try {
            CopyObjectUtil.copyProperties(task, tobj);
            // 判断定时发布时间为空，定时发布时间为空，则使用当前时间发布
            // if 定时发布时间不为空
            if (StrUtil.isNotNull(time)) {
                tobj.setTimingTime(DateUtil.stringToDate(time));
            } else {
                tobj.setTimingTime(new Date());
            }
            tobj.setStatus(Status.STATUS_TEMPLATE);
            baseMapper.updateById(tobj);

            // 先删除任务对应的搜索词数据，該操作是物理删除数据
            iTaskKeywordService.deleteByTaskId(task.getId());
            // JSON to JSONArray

            List<TaskKeyword> list = JSONArray.parseArray(taskKeyword, TaskKeyword.class);
            // 遍历保存搜索数据
            for (TaskKeyword taskKey : list) {
                taskKey.setTaskId(task.getId());
                taskKey.setKeyType(KeywordType.KEYWORD_S);
                taskKey.setStatus(Status.STATUS_TEMPLATE);
                taskKey.setCreateTime(new Date());
                iTaskKeywordService.saveData(taskKey);
            }
        } catch (Exception e) {
            logger.error("===>【任务更新失败】===失败原因：{}", e.getMessage());
            throw BaseException.base("更新失败");
        }
    }

    @Override
    public Task findTaskBySellerShopId(Long sellerShopId, Long goodsId) {
        QueryWrapper<Task> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(sellerShopId),"seller_shop_id",sellerShopId);
        queryWrapper.eq(StrUtil.isNotNull(goodsId),"goods_id",goodsId);
        queryWrapper.orderByDesc("create_time");
        queryWrapper.last("limit 1");
        return baseMapper.selectOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateUndoneTaskByDays() {
        QueryWrapper<Task> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_RELEASED);
        queryWrapper.eq("DATEDIFF(NOW(),timing_time)", 1);
        queryWrapper.gt("task_residue_total", 0);
        List<Task> list = list(queryWrapper);
        Task task;
        for (Task t : list) {
            task = new Task();
            CopyObjectUtil.copyProperties(t, task);
            task.setStatus(Status.STATUS_UNDONE);
            task.setCreateTime(new Date());
            task.setTimingTime(null);
            task.setTaskTotal(t.getTaskResidueTotal());
            task.setTaskNumber(iFormNoGenerateService.generateFormNo(FormNoTypeEnum.OR_ORDER));
            task.setId(null);
            save(task);

            List<TaskKeyword> tkList = iTaskKeywordService.findTaskKeywordByTaskIdAndStatus(t.getId(), null);
            for (TaskKeyword keyword : tkList) {
                keyword.setId(null);
                keyword.setTaskId(task.getId());
                iTaskKeywordService.saveTaskKeyword(keyword);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void schedulerStopTask() {
        QueryWrapper<Task> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", Status.STATUS_RELEASED);
        queryWrapper.and(f->f.lt("timing_time", new Date())
                .ne("date_format(timing_time,'%Y-%m-%d')", "date_format(now(),'%Y-%m-%d')"));
        List<Task> list = list(queryWrapper);
        List<Integer> ids = new ArrayList<>();
        List<String> keys;
        if (StrUtil.isNull(list)) {
            logger.info("===>没有要终止的任务！");
            return;
        }
        keys = new ArrayList<>();
        for (Task task : list) {
            ids.add(task.getId().intValue());
            String key = TaskRedis.getTaskTotalCacheKey(task.getId());
            keys.add(key);
            // 更新关键词
            iTaskKeywordService.logicDeleteTaskKeywordByTaskId(task.getId());
        }

        if(ids.size() > 0 && keys.size() > 0) {
            // 批量删除Redis中缓存数据
            RedisUtil.del(keys);
            UpdateWrapper<Task> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("status", Status.STATUS_STOP);
            updateWrapper.in("id", ids);
            // 批量更新任务状态将运行状态更新为终止状态
            update(updateWrapper);
        }
        logger.info("===>终止【{}】 个任务！",list.size());
    }

    @Override
    public List<Task> taskSpeedList(Long orgId) {
        List<Task> list = new ArrayList<>();
        List<Task> tmp;
        //根据任务类型配置信息
        List<SysConfigDtl> scd = iSysConfigDtlService.findConfigDtl(SysConfigType.TASK_TYPE);
        for (SysConfigDtl cfd : scd) {
            if (cfd.getName().equals("隔日单")) {
                tmp = baseMapper.taskSpeedList(orgId, cfd.getId());
                if (StrUtil.isNotNull(tmp) && tmp.size() > 0) {
                    // 移除所有null数据
                    tmp.removeAll(Collections.singleton(null));
                    list.addAll(tmp);
                }
            } else if (cfd.getName().equals("现付单")) {
                tmp = baseMapper.taskSpeedList(orgId, cfd.getId());
                if (StrUtil.isNotNull(tmp) && tmp.size() > 0) {
                    // 移除所有null数据
                    tmp.removeAll(Collections.singleton(null));
                    list.addAll(tmp);
                }
            }
        }
        return list;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void upadteTaskAndKeyword(Task task, String taskKeyword, String time) {
        Task tobj = findTaskOne(task.getId());
        // 判断对象为空处理
        if (StrUtil.isNull(tobj)) {
            throw BaseException.base("更新失败");
        }
        try {
            // 判断定时发布时间为空，定时发布时间为空，则使用当前时间发布
            // if 定时发布时间不为空
            if (StrUtil.isNotNull(time)) {
                tobj.setTimingTime(DateUtil.stringToDate(time));
            } else {
                tobj.setTimingTime(new Date());
            }
            tobj.setTaskTypeId(task.getTaskTypeId());
            tobj.setBrokerage(task.getBrokerage());
            tobj.setPrefPrice(task.getPrefPrice());
            tobj.setRealPrice(task.getRealPrice());
            tobj.setShowPrice(task.getShowPrice());
            tobj.setTaskTotal(task.getTaskTotal());
            tobj.setTaskResidueTotal(task.getTaskTotal());
            tobj.setSellerAsk(task.getSellerAsk());
            tobj.setServerPrice(task.getServerPrice());
            tobj.setStatus(task.getStatus());
            tobj.setGoodsKeywords(task.getGoodsKeywords());
            baseMapper.updateById(tobj);

            // 先删除任务对应的搜索词数据，該操作是物理删除数据
            iTaskKeywordService.deleteByTaskId(task.getId());
            // JSON to JSONArray
            List<TaskKeyword> list = JSONArray.parseArray(taskKeyword, TaskKeyword.class);
            // 遍历保存搜索数据
            for (TaskKeyword taskKey : list) {
                taskKey.setTaskId(task.getId());
                taskKey.setKeyType(KeywordType.KEYWORD_S);
                iTaskKeywordService.saveTaskKeyword(taskKey);
            }
        } catch (Exception e) {
            logger.error("===>【任务更新失败】===失败原因：{}", e.getMessage());
            throw BaseException.base("更新失败");
        }

        // 判断运行状态，如果是“R”状态，
        // 将任务可领数量放置到Redis缓存中。
        if (tobj.getStatus().equalsIgnoreCase(Status.STATUS_RELEASED)) {
            // Redis_缓存任务可领数量。
            if (!TaskRedis.setTaskTotalCacheKey(tobj.getId(), tobj.getTaskResidueTotal())) {
                logger.error("===>【任务更新，并且缓存“任务可领数量”失败！要缓存的ID：{}，任务编号为：{}】", tobj.getId(), tobj.getTaskNumber());
            }

            // Redis_缓存任务数据。
            //if (!RedisKit.set(TaskRedis.getTaskDataCachKey(tobj.getId()), tobj)) {
            //	logger.error("===>【任务更新，并且缓存“任务数据”失败！要缓存的ID:{}，任务编号为：{}】", tobj.getId(), tobj.getTaskNumber());
            //}
        }
    }

    @Override
    public List<TaskExtVo> findTaskTypeTaskTotalBySalNumberIdAdnDays(Long orgId, Long salNumberId, Long platformId, int days) {

        // \(^o^)/~  过滤当前号主做过的同店铺，同店铺编码的店铺数据信息 以及业务员的上线
        String shopIds = iTaskSonService.getUnavailableShopIds(orgId, salNumberId, days, platformId);
        // ~任务数据!
        QueryWrapper<Task> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("t.status",Status.STATUS_RELEASED);
        queryWrapper.apply("(t.timing_time < now() && date_format(t.timing_time ,'%Y-%m-%d')= date_format(now(),'%Y-%m-%d'))");
        queryWrapper.eq(StrUtil.isNotNull(orgId),"t.org_id",orgId);
        queryWrapper.eq(StrUtil.isNotNull(platformId),"t.platform_id",platformId);
        queryWrapper.gt("t.task_residue_total",0);
        // 过滤掉已做过的任务信息
        queryWrapper.notIn(StrUtil.isNotNull(shopIds),"t.seller_shop_id",StrUtil.getIdsByString(shopIds));
        // 过滤掉已做过的类目
        Salesman s = iSalesmanService.findSalesmanBySalesmanNumberId(salNumberId);
        // 拼多多逻辑
        boolean pddFlag = false;
        ConfigPlatform platform = iConfigPlatformService.findPlatformOneById(platformId);
        if (StrUtil.isNotNull(platform)) {
            if ((PlatformType.P_TYPE_PDD).equalsIgnoreCase(platform.getCode())) {
                pddFlag = true;
            }
        }
        if (StrUtil.isNotNull(s)) {
            // 获取任务配置
            TaskSetting setting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(orgId, s.getSalesmanLevelId());
            // 检测是否开启类目错开
            if (StrUtil.isNotNull(setting) && setting.getCategoryFlag() > 0) {
                // 获取类目错开ids
                String goodsIds = iTaskSonService.getUnavailableGoodsIds(orgId, salNumberId, setting.getCategoryIntervalDay(), platformId);
                queryWrapper.notIn(StrUtil.isNotNull(goodsIds),"t.goods_id", StrUtil.getIdsByString(goodsIds));
                // 数据处理
                List<TaskExtVo> list = baseMapper.findTaskTypeTaskBySalNumberIdAdnDays(queryWrapper);
                if (list.size() > 0) {
                    Set<String> category = new HashSet<>();
                    Set<Long> taskTypeId = new HashSet<>();
                    List<Long> taskIds = new ArrayList<>();
                    list.stream().filter(f -> StrUtil.isNotNull(f.getCategory())).forEach(f->category.add(f.getCategory()));
                    if (category.size() > 0) {
                        list.forEach(f->taskTypeId.add(f.getTaskTypeId()));
                        Map<Long, Integer> maxMap = new HashMap<>();
                        for (Long type : taskTypeId) {
                            maxMap.put(type, 0);
                        }
                        int max;
                        Long taskIdKey;
                        boolean topFlag;
                        // 类目
                        for (String s1 : category) {
                            // 任务类型{隔日单、现付单}
                            for (Long type : taskTypeId) {
                                taskIdKey = -1L;
                                // 初始化为0
                                maxMap.put(type, 0);
                                // 数据
                                topFlag = false;
                                // 检测是否存在置顶的任务
                                for (TaskExtVo tev : list) {
                                    if (tev.getTaskTypeId().equals(type) && tev.getCategory().equalsIgnoreCase(s1)) {
                                        if (tev.getTop() > 0) {
                                            topFlag = true;
                                            break;
                                        }
                                    }
                                }
                                for (TaskExtVo tev : list) {
                                    if (tev.getTaskTypeId().equals(type) && tev.getCategory().equalsIgnoreCase(s1)) {
                                        if (tev.getTop() > 0) {
                                            max = maxMap.get(tev.getTaskTypeId());
                                            if (max < tev.getTaskResidueTotal()) {
                                                max = tev.getTaskResidueTotal();
                                                taskIdKey = tev.getId();
                                                maxMap.put(tev.getTaskTypeId(), max);
                                            }
                                        }
                                        if (topFlag) {
                                            continue;
                                        }
                                        // 比较大小
                                        max = maxMap.get(tev.getTaskTypeId());
                                        if (max < tev.getTaskResidueTotal()) {
                                            max = tev.getTaskResidueTotal();
                                            taskIdKey = tev.getId();
                                            maxMap.put(tev.getTaskTypeId(), max);
                                        }
                                    }
                                }
                                taskIds.add(taskIdKey);
                            }
                        }
                        // 只获取指定的任务
                        queryWrapper.in("t.id", taskIds);
                    }
                }
            }
        }
        //queryWrapper.groupBy("t.task_type_id");
        // 获取数据
        List<TaskExtVo> listOrigin = baseMapper.findTaskTypeTaskTotalBySalNumberIdAdnDays(queryWrapper);
        Set<Long> taskTypeIds = new HashSet<>();
        listOrigin.forEach(f->taskTypeIds.add(f.getTaskTypeId()));
        List<TaskExtVo> list = new ArrayList<>();
        Map<Long, Long> map = new HashMap<>();
        TaskExtVo tev;
        for (Long taskTypeId : taskTypeIds) {
            tev = new TaskExtVo();
            tev.setTaskTypeId(taskTypeId);
            tev.setTaskTotal(Number.LONG_ZERO);
            list.add(tev);
            map.put(taskTypeId, Number.LONG_ZERO);
        }
        // 依据任务类型赋值
        for (TaskExtVo taskExtVo : listOrigin) {
            map.put(taskExtVo.getTaskTypeId(), map.get(taskExtVo.getTaskTypeId())+1);
        }
        for (TaskExtVo taskExtVo : list) {
            if (pddFlag) {
                if (map.get(taskExtVo.getTaskTypeId()) < 1) {
                    taskExtVo.setTaskTotal(Number.LONG_ZERO);
                } else {
                    taskExtVo.setTaskTotal(Number.LONG_ONE);
                }
                continue;
            }
            taskExtVo.setTaskTotal(map.get(taskExtVo.getTaskTypeId()));
        }
        return list;
    }

    @Override
    public IPage<TaskExtVo> finaTaskPageList(Long orgId, Long taskType, Long salNumberId, Long platformId, int days, IPage<TaskExtVo> page) {
        // \(^o^)/~  过滤当前号主做过的同店铺，同店铺编码的店铺数据信息
        String shopIds = iTaskSonService.getUnavailableShopIds(orgId, salNumberId, days, platformId);
        // ~任务数据!
        QueryWrapper<Task> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(taskType),"t.task_type_id",taskType);
        queryWrapper.eq("t.status",Status.STATUS_RELEASED);
        queryWrapper.apply("(t.timing_time < now() && date_format(t.timing_time ,'%Y-%m-%d')= date_format(now(),'%Y-%m-%d'))");
        queryWrapper.eq(StrUtil.isNotNull(orgId),"t.org_id",orgId);
        queryWrapper.eq(StrUtil.isNotNull(platformId),"t.platform_id",platformId);
        queryWrapper.gt("t.task_residue_total",0);
        // 过滤掉已做过的任务信息
        queryWrapper.notIn(StrUtil.isNotNull(shopIds),"t.seller_shop_id",StrUtil.getIdsByString(shopIds));
        // 过滤掉已做过的类目
        Salesman s = iSalesmanService.findSalesmanBySalesmanNumberId(salNumberId);
        // 拼多多逻辑
        boolean pddFlag = false;
        ConfigPlatform platform = iConfigPlatformService.findPlatformOneById(platformId);
        if (StrUtil.isNotNull(platform)) {
            if ((PlatformType.P_TYPE_PDD).equalsIgnoreCase(platform.getCode())) {
                pddFlag = true;
            }
        }
        if (StrUtil.isNotNull(s)) {

            TaskSetting setting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(orgId, s.getSalesmanLevelId());
            if (StrUtil.isNotNull(setting) && setting.getCategoryFlag() > 0) {
                String goodsIds = iTaskSonService.getUnavailableGoodsIds(orgId, salNumberId, setting.getCategoryIntervalDay(), platformId);
                queryWrapper.notIn(StrUtil.isNotNull(goodsIds),"t.goods_id", StrUtil.getIdsByString(goodsIds));
                // 数据处理
                List<TaskExtVo> list = baseMapper.findTaskTypeTaskBySalNumberIdAdnDays(queryWrapper);
                if (list.size() > 0) {
                    Set<String> category = new HashSet<>();
                    Set<Long> taskTypeId = new HashSet<>();
                    List<Long> taskIds = new ArrayList<>();
                    list.stream().filter(f -> StrUtil.isNotNull(f.getCategory())).forEach(f->category.add(f.getCategory()));
                    if (category.size() > 0) {
                        list.forEach(f->taskTypeId.add(f.getTaskTypeId()));
                        Map<Long, Integer> maxMap = new HashMap<>();
                        for (Long type : taskTypeId) {
                            maxMap.put(type, 0);
                        }
                        int max;
                        Long taskIdKey;
                        boolean topFlag;
                        // 类目
                        for (String s1 : category) {
                            // 任务类型{隔日单、现付单}
                            for (Long type : taskTypeId) {
                                taskIdKey = -1L;
                                // 初始化为0
                                maxMap.put(type, 0);
                                // 数据
                                topFlag = false;
                                // 检测是否存在置顶的任务
                                for (TaskExtVo tev : list) {
                                    if (tev.getTaskTypeId().equals(type) && tev.getCategory().equalsIgnoreCase(s1)) {
                                        if (tev.getTop() > 0) {
                                            topFlag = true;
                                            break;
                                        }
                                    }
                                }
                                for (TaskExtVo tev : list) {
                                    if (tev.getTaskTypeId().equals(type) && tev.getCategory().equalsIgnoreCase(s1)) {
                                        if (tev.getTop() > 0) {
                                            max = maxMap.get(tev.getTaskTypeId());
                                            if (max < tev.getTaskResidueTotal()) {
                                                max = tev.getTaskResidueTotal();
                                                taskIdKey = tev.getId();
                                                maxMap.put(tev.getTaskTypeId(), max);
                                            }
                                        }
                                        if (topFlag) {
                                            continue;
                                        }
                                        // 比较大小
                                        max = maxMap.get(tev.getTaskTypeId());
                                        if (max < tev.getTaskResidueTotal()) {
                                            max = tev.getTaskResidueTotal();
                                            taskIdKey = tev.getId();
                                            maxMap.put(tev.getTaskTypeId(), max);
                                        }
                                    }
                                }
                                taskIds.add(taskIdKey);
                            }
                        }
                        // 只获取指定的任务
                        queryWrapper.in("t.id", taskIds);
                    }
                }
            }
        }
        //sql.append(" order by t.top desc,rand() limit 10");
        queryWrapper.orderByDesc("t.top","t.task_residue_total");
        queryWrapper.last(",rand()");
        //queryWrapper.apply("order by t.top,t.task_residue_total desc,rand() limit 10");
        if (pddFlag) {
            page.setSize(Number.INT_ONE);
        }

        IPage<TaskExtVo> ipage = baseMapper.finaTaskPageList(page,queryWrapper);
        if (ipage.getSize() > 0) {
            for (TaskExtVo task : ipage.getRecords()) {
                // \(^o^)/~ 判断是否回购,0：否，1：是
                if (iTaskSonService.verifyTaskSon(salNumberId, task.getSellerShopId())) {
                    task.setIsBuy(Number.INT_ONE);
                }
            }
        }
        return page;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void taskReceiveNew(String taskIds, Long salesmanId, Long salNumberId, Long orgId) {
        // if 判断任务ids是否为空!
        if (StrUtil.isNull(taskIds)) {
            logger.error("===>领取任务：任务ids为空");
            throw  BaseException.base("领取任务失败！");
        }

        // 获取号主信息
        SalesmanNumber number = iSalesmanNumberService.getById(salNumberId);
		/*// 判断号主是否为空
		if(StrUtil.isNull(number) || StrUtil.isNull(number.getImei())) {
			logger.error("===>领取任务：号主信息数据为空，或者是号主的手机串号（IMEI）为空，号主ID：[{}]",salNumberId);
			throw new BaseException(BaseErrorCode.ERROR_PARAM_ERROR.getCode(),"该号主信息缺失，分配失败！");
		}*/
        if (StrUtil.isNull(number)) {
            logger.error("===>领取任务：号主信息数据为空 号主ID：[{}]", salNumberId);
            throw  BaseException.base( "该号主信息缺失，分配失败！");
        }

        // if是多个任务，else是单个任务
        if (StrUtil.indexOf(taskIds, Chars.CHAR_COMMA)) {
            iTaskService.saveTaskSonMuch(taskIds, number);
        } else {
            iTaskService.saveTaskSonOne(Long.valueOf(taskIds), number);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskSonOne(Long taskId, SalesmanNumber number) {
        // 获取主任务数据！
        TaskVo task = findTaskExtOne(taskId);
        if (StrUtil.isNull(task)) {
            // 扔回相应的队列中
            logger.error("===>生成[单个]子 任务失败：获取【主任务】数据为空，主任务ID：[{}]", taskId);
            throw BaseException.base("生成[单个]子任务失败！");
        }
        // 任务搜索词
        TaskKeyword keyword = iTaskKeywordService.findAvailableTaskKeywordByTaskId(taskId);
        if (StrUtil.isNull(keyword)) {
            logger.error("===>生成[单个]子任务失败：获取主任务【搜索词】数据为空，主任务ID：[{}]", taskId);
            throw BaseException.base("生成[单个]子任务失败！");
        }
        // 获取业务员信息
        Salesman salesman = iSalesmanService.getById(number.getSalesmanId());
        if (StrUtil.isNull(salesman)) {
            // 扔回相应的队列中
            TaskRedis.increment(task.getId());
            logger.error("===>生成[单个]子任务失败：获取当前领取【任务业务员】数据为空，主任务ID：[{}]", taskId);
            throw BaseException.base("生成[单个]子任务失败！");
        }
        // 获取任务配置
        TaskSetting taskSetting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(task.getOrgId(), salesman.getSalesmanLevelId());
        if (StrUtil.isNull(taskSetting)) {
            // 扔回相应的队列中
            TaskRedis.increment(task.getId());
            logger.error("===>生成[单个]子任务失败：获取当前【任务分配配置】数据为空，主任务ID：[{}]", taskId);
            throw BaseException.base("生成[单个]子任务失败！");
        }

        // if 判断任务是否终止了！
        if (Status.STATUS_STOP.equals(task.getStatus())) {
            // 删除对应的任务key
            TaskRedis.remove(task.getId());
            logger.error("===>生成[单个]子任务失败：当前的任务状态为【已终止】,任务ID：[{}]", taskId);
            throw BaseException.base("生成[单个]子任务失败！");
        }

        // if 判断该任务是否超领现象!
        int sum = iTaskSonService.taskSonCountByTaskId(taskId);
        if (task.getTaskTotal() < (sum + 1)) {
            // 删除对应的任务key
            TaskRedis.remove(task.getId());
            logger.error("===>生成[单个]子任务失败：当前的任务ID：[{}]，任务发布总数量为：[{}]，任务已领取数量为：[{}]，该任务出现超领的现象！", taskId, task.getTaskTotal(), sum);
            throw BaseException.base("生成[单个]子任务失败！");
        }

        // 判断业务员信用度!
        // -1 小于   0 等于    1 大于
        /*
		if(salesman.getSurplusCredit().compareTo(task.getRealPrice()) < 0) {
			// 扔回相应的队列中
			TaskRedis.increment(task.getId());
			logger.error("===>生成[单个]子任务失败：当前的任务ID：[{}]，业务员的信用额度已到达上限，业务员ID：[{}]！",task.getId(),salesman.getId());
			throw new BaseException(SalesmanErrorCode.SALESMAN_NOT_CREDIT_COUNT.getCode(),"分配失败，额度已上限");
		}
		*/

        // 判断当前业务员的号主是否领取相同的任务数据！
        if (iTaskSonService.taskSonCountByParams(taskId, salesman.getId(), number.getId()) > 0) {
            // 扔回相应的队列中
            TaskRedis.increment(task.getId());
            logger.error("===>生成[单个]子任务失败：当前的任务ID：[{}]，业务员对应的号主：[{}] 领取重复任务，业务员ID：[{}]", task.getId(), number.getOnlineid(), salesman.getId());
            throw  BaseException.base( "请勿重复领取");
        }

        // 判断任务数据和任务搜索词数据不为空，则先减少剩余数量！
        // 更新任务剩余任务数量!
        updateTaskResidueTotalLock(task.getId(), Boolean.FALSE);
        // 更新任务搜索词可领数量!
        iTaskKeywordService.updateTaskKeywordNumberLock(keyword.getId(), Boolean.FALSE);

        // ~保存子任务数据!
        TaskSon taskSon = new TaskSon();
        taskSon.setTaskSonNumber(iFormNoGenerateService.generateFormNo(FormNoTypeEnum.OS_ORDER));
        taskSon.setRealPrice(task.getRealPrice());
        taskSon.setOrgId(task.getOrgId());
        taskSon.setCreateTime(new Date());
        taskSon.setGoodsId(task.getGoodsId());
        taskSon.setPackages(task.getPackages());
        taskSon.setSellerShopId(task.getSellerShopId());
        taskSon.setIsTimeOut(IsTimeOutType.TIME_OUT_TYPE_NO);
        taskSon.setTaskId(task.getId());
        taskSon.setSalesmanId(salesman.getId());
        taskSon.setSalesmanNumberId(number.getId());
        taskSon.setImei(number.getImei());
        taskSon.setKeyword(keyword.getKeyword());
        taskSon.setKeywordId(keyword.getId());
        taskSon.setPayment(BigDecimal.ZERO);
        taskSon.setPlatformId(task.getPlatformId());
        taskSon.setTaskTypeId(task.getTaskTypeId());
        taskSon.setOrderMatch(OrderMatchType.OREDER_MATCH_NO);
        taskSon.setOrderStatus(-3);

        // if 判断该任务是否需要商品标注为快搜模式 （标记为：已打标）
        if (task.getIsCard().equals(IsCardType.CARD_NOT)) {
            taskSon.setIsCard(IsCardType.CARD_NOT);
            taskSon.setIsMark(IsMarkType.MARK_TYPE_YES);
        } else {
            // 否 0 （标记为：已打标）
            if (keyword.getIsCard().equals(IsCardType.CARD_NOT)) {
                taskSon.setIsCard(IsCardType.CARD_NOT);
                taskSon.setIsMark(IsMarkType.MARK_TYPE_YES);
                // 是 1 （标记为：未打标）
            } else if (keyword.getIsCard().equals(IsCardType.CARD_YES)) {
                taskSon.setIsCard(IsCardType.CARD_YES);
                taskSon.setIsMark(IsMarkType.MARK_TYPE_NO);
                // 自动 2 （标记为：未打标）
            } else {
                taskSon.setIsCard(IsCardType.CARD_AUTO);
                taskSon.setIsMark(IsMarkType.MARK_TYPE_NO);
            }
        }

        // 判断是否为空，不为空则佣金减半，反之则不减！[处理是否是复购的商品]
        BigDecimal brokerage = task.getBrokerage();
        if (iTaskSonService.repeatPurchaseTaskSon(number.getId(), task.getSellerShopId(), taskSetting.getSameShopBigDays())) {
            brokerage = task.getBrokerage().divide(new BigDecimal(2), 2, RoundingMode.HALF_UP);
        }
        taskSon.setBrokerage(brokerage);

        // 判断任务的类型。
        if (task.getTaskTypeCode().equals(TaskTypeCode.TASK_TYPE_XFD)) {
            // 现付单： 30分钟过期
            taskSon.setDeadlineTime(DateUtil.plusMinute(30));
            // 现付单：A -开始
            taskSon.setStatus(Status.STATUS_ACTIVITY);
        } else if (task.getTaskTypeCode().equals(TaskTypeCode.TASK_TYPE_GRD)) {
            // 隔日单： 第一个次6个小时过期
            taskSon.setDeadlineTime(DateUtil.dateAddHour(6));
            // 隔日单: F -待提交
            taskSon.setStatus(Status.STATUS_NOT_AUDITED);
            // 设置为隔日单
            taskSon.setImages(TaskTypeCode.TASK_TYPE_GRD);
        }

        // 判断商品中是否有配置查文截图文字.
        // 判断该字段不为空
        if (!isBlank(task.getGoodsKeywords())) {
            taskSon.setGoodsKeyword(randomGoodsKeyword(task.getGoodsKeywords()));
        }

        // 保存子任务！
        iTaskSonService.save(taskSon);
        // 更新业务员的信用额度,减额度
        //iSalesmanService.updateSalesmanCreditById(salesman.getId(),task.getRealPrice(),Boolean.TRUE);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskSonMuch(String taskIds, SalesmanNumber number) {
        List<TaskVo> list = findTaskExtOne(taskIds);
        if (list.isEmpty()) {
            logger.error("===>生成[多个]子 任务失败：获取【主任务】数据为空，主任务IDS：[{}]", taskIds);
            throw  BaseException.base("获取失败");
        }
        // 统计有多少个任务是额度上限！
        //int count = 0 ;

        // 遍历数据
        for (TaskVo task : list) {
            // 任务搜索词
            TaskKeyword keyword = iTaskKeywordService.findAvailableTaskKeywordByTaskId(task.getId());
            if (StrUtil.isNull(keyword)) {
                logger.error("===>生成[多个]子任务失败：获取主任务【搜索词】数据为空，主任务ID：[{}]", task.getId());
                continue;
            }
            // 获取业务员信息
            Salesman salesman = iSalesmanService.getById(number.getSalesmanId());
            if (StrUtil.isNull(salesman)) {
                // 扔回相应的队列中
                TaskRedis.increment(task.getId());
                logger.error("===>生成[多个]子任务失败：获取当前领取【任务业务员】数据为空，主任务ID：[{}]", task.getId());
                continue;
            }
            // 获取任务配置
            TaskSetting taskSetting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(task.getOrgId(), salesman.getSalesmanLevelId());
            if (StrUtil.isNull(taskSetting)) {
                // 扔回相应的队列中
                TaskRedis.increment(task.getId());
                logger.error("===>生成[多个]子任务失败：获取当前【任务分配配置】数据为空，主任务ID：[{}]", task.getId());
                continue;
            }

            // if 判断任务是否终止了！
            if (Status.STATUS_STOP.equals(task.getStatus())) {
                // 删除对应的任务key
                TaskRedis.remove(task.getId());
                logger.error("===>生成[多个]子任务失败：当前的任务状态为【已终止】,任务ID：[{}]", task.getId());
                continue;
            }

            // if 判断该任务是否超领现象!
            int sum = iTaskSonService.taskSonCountByTaskId(task.getId());
            if (task.getTaskTotal() < (sum + 1)) {
                // 删除对应的任务key
                TaskRedis.remove(task.getId());
                logger.error("===>生成[多个]子任务失败：当前的任务ID：[{}]，任务发布总数量为：[{}]，任务已领取数量为：[{}]，该任务出现超领的现象！", task.getId(), task.getTaskTotal(), sum);
                continue;
            }

            // 判断当前业务员的号主是否领取相同的任务数据！
            if (iTaskSonService.taskSonCountByParams(task.getId(), salesman.getId(), number.getId()) > 0) {
                // 扔回相应的队列中
                TaskRedis.increment(task.getId());
                logger.error("===>生成[单个]子任务失败：当前的任务ID：[{}]，业务员对应的号主：[{}] 领取重复任务，业务员ID：[{}]", task.getId(), number.getOnlineid(), salesman.getId());
                continue;
            }

            // 判断业务员信用度!
            // -1 小于   0 等于    1 大于
            /*
			if(salesman.getSurplusCredit().compareTo(task.getRealPrice()) < 0) {
				// 扔回相应的队列中
				TaskRedis.increment(task.getId());
				logger.error("===>生成[多个]子任务失败：当前的任务ID：[{}]，业务员的信用额度已到达上限，业务员ID：[{}]！",task.getId(),salesman.getId());
				count ++;
				continue;
			}
			*/

            // 更新任务剩余任务数量!
            updateTaskResidueTotalLock(task.getId(), Boolean.FALSE);
            // 更新任务搜索词可领数量!
            iTaskKeywordService.updateTaskKeywordNumberLock(keyword.getId(), Boolean.FALSE);

            // ~保存子任务数据!
            TaskSon taskSon = new TaskSon();
            taskSon.setTaskSonNumber(iFormNoGenerateService.generateFormNo(FormNoTypeEnum.OS_ORDER));
            taskSon.setRealPrice(task.getRealPrice());
            taskSon.setCreateTime(new Date());
            taskSon.setOrgId(task.getOrgId());
            taskSon.setGoodsId(task.getGoodsId());
            taskSon.setPackages(task.getPackages());
            taskSon.setSellerShopId(task.getSellerShopId());
            taskSon.setIsTimeOut(IsTimeOutType.TIME_OUT_TYPE_NO);
            taskSon.setTaskId(task.getId());
            taskSon.setSalesmanId(salesman.getId());
            taskSon.setSalesmanNumberId(number.getId());
            taskSon.setImei(number.getImei());
            taskSon.setKeyword(keyword.getKeyword());
            taskSon.setKeywordId(keyword.getId());
            taskSon.setPayment(BigDecimal.ZERO);
            taskSon.setPlatformId(task.getPlatformId());
            taskSon.setTaskTypeId(task.getTaskTypeId());
            taskSon.setOrderMatch(OrderMatchType.OREDER_MATCH_NO);
            taskSon.setOrderStatus(-3);

            // if 判断该任务是否需要商品标注为快搜模式 （标记为：已打标）
            if (task.getIsCard().equals(IsCardType.CARD_NOT)) {
                taskSon.setIsCard(IsCardType.CARD_NOT);
                taskSon.setIsMark(IsMarkType.MARK_TYPE_YES);
            } else {
                // 否 0 （标记为：已打标）
                if (keyword.getIsCard().equals(IsCardType.CARD_NOT)) {
                    taskSon.setIsCard(IsCardType.CARD_NOT);
                    taskSon.setIsMark(IsMarkType.MARK_TYPE_YES);
                    // 是 1 （标记为：未打标）
                } else if (keyword.getIsCard().equals(IsCardType.CARD_YES)) {
                    taskSon.setIsCard(IsCardType.CARD_YES);
                    taskSon.setIsMark(IsMarkType.MARK_TYPE_NO);
                    // 自动 2 （标记为：未打标）
                } else {
                    taskSon.setIsCard(IsCardType.CARD_AUTO);
                    taskSon.setIsMark(IsMarkType.MARK_TYPE_NO);
                }
            }

            // 判断是否为空，不为空则佣金减半，反之则不减！[处理是否是复购的商品]
            BigDecimal brokerage = task.getBrokerage();
            // iTaskSonService.findSameTaskSon(number.getId(), task.getSellerShopId(), task.getGoodsId(), taskSetting.getSameShopBigDays());
            if (iTaskSonService.repeatPurchaseTaskSon(number.getId(), task.getSellerShopId(), taskSetting.getSameShopBigDays())) {
                brokerage = task.getBrokerage().divide(new BigDecimal(2), 2, RoundingMode.HALF_UP);
            }
            taskSon.setBrokerage(brokerage);

            // 判断任务的类型。
            if (task.getTaskTypeCode().equals(TaskTypeCode.TASK_TYPE_XFD)) {
                // 现付单： 30分钟过期
                taskSon.setDeadlineTime(DateUtil.plusMinute(30));
                // 现付单：A -开始
                taskSon.setStatus(Status.STATUS_ACTIVITY);
            } else if (task.getTaskTypeCode().equals(TaskTypeCode.TASK_TYPE_GRD)) {
                // 隔日单： 第一个次6个小时过期
                taskSon.setDeadlineTime(DateUtil.dateAddHour(6));
                // 隔日单: F -待提交
                taskSon.setStatus(Status.STATUS_NOT_AUDITED);
                // 设置为隔日单
                taskSon.setImages(TaskTypeCode.TASK_TYPE_GRD);
            }

            // 判断商品中是否有配置查文截图文字.
            // 判断该字段不为空
            if (!isBlank(task.getGoodsKeywords())) {
                taskSon.setGoodsKeyword(randomGoodsKeyword(task.getGoodsKeywords()));
            }

            // 保存子任务！
            iTaskSonService.save(taskSon);

            // 更新业务员的信用额度,减额度
            //iSalesmanService.updateSalesmanCreditById(salesman.getId(),task.getRealPrice(),Boolean.TRUE);
        }

		/*if(count > 0) {
			throw new BaseException(count+" 个任务分配失败，额度已上限！");
		}*/
    }

    /**
     * Created by Htl to 2019/07/23 <br/>
     * 根据商品关键词，有多个根据随机一个商品关键即可
     *
     * @param goodsKeywords 商品关键词
     * @return
     */
    private static String randomGoodsKeyword(String goodsKeywords) {
        if (isBlank(goodsKeywords)) {
            return "无";
        }
        // 判断是否包含多个商品关键词
        // 商品关键词是根据换行符进行分割
        // 处理单个关键词
        if (!StrUtil.indexOf(goodsKeywords, "\n")) {
            return goodsKeywords;
        }

        // 处理多个关键词
        String[] keys = goodsKeywords.split("\n");
        int index = (keys.length - 1);
        // 根据关键词的个数创建随机数
        int num = StrUtil.getRandom(index);
        String key = keys[num];
        // 防止第一次获取为空
        if (isBlank(key)) {
            key = keys[StrUtil.getRandom(index)];
        }
        return key;
    }

    @Override
    public TaskVo findTaskExtOne(Long taskId) {

        QueryWrapper<Task> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sc.code",SysConfigType.TASK_TYPE);
        queryWrapper.eq("scd.status",Status.STATUS_ACTIVITY);
        queryWrapper.eq("sc.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(taskId)," t.id",taskId);

        return baseMapper.findTaskExtOne(queryWrapper).get(0);
    }

    @Override
    public List<TaskVo> findTaskExtOne(String taskIds) {

        QueryWrapper<Task> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sc.code",SysConfigType.TASK_TYPE);
        queryWrapper.eq("scd.status",Status.STATUS_ACTIVITY);
        queryWrapper.eq("sc.status",Status.STATUS_ACTIVITY);
        queryWrapper.inSql(StrUtil.isNotNull(taskIds),"t.id",taskIds);

        return  baseMapper.findTaskExtOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskAndKeyword(Task task, String taskKeyword) {
        // 为了防止id存在值，清除id值.
        task.setId(null);
        try {
            // 保存住任务数据
            //iTaskDao.save(task);
            iTaskService.saveTask(task);
            // 判断关键词是否为空
            if (StrUtil.isNotNull(task.getId())) {
                List<TaskKeyword> list = JSONArray.parseArray(taskKeyword, TaskKeyword.class);
                for (TaskKeyword taskKey : list) {
                    // 保存任务搜索词
                    taskKey.setTaskId(task.getId());
                    taskKey.setKeyType(KeywordType.KEYWORD_S);
                    iTaskKeywordService.saveTaskKeyword(taskKey);
                }
            }
        } catch (Exception e) {
            logger.error("===>【新增任务失败】===失败原因：{}", e.getMessage());
            throw BaseException.base("新增失败");
        }
    }

    /**
     * 字符串为 null 或者内部字符全部为 ' ' '\t' '\n' '\r' 这四类字符时返回 true <br/>
     *
     * @param str 字符串
     * @return
     */
    private static boolean isBlank(String str) {
        if (str == null) {
            return true;
        }
        int len = str.length();
        if (len == 0) {
            return true;
        }
        for (int i = 0; i < len; i++) {
            switch (str.charAt(i)) {
                case ' ':
                case '\t':
                case '\n':
                case '\r':
                    break;
                default:
                    return false;
            }
        }
        return true;
    }
}
