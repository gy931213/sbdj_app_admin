package com.sbdj.service.task.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.task.entity.TaskSetting;
import com.sbdj.service.task.mapper.TaskSettingMapper;
import com.sbdj.service.task.service.ITaskSettingService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 任务分配配置表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class TaskSettingServiceImpl extends ServiceImpl<TaskSettingMapper, TaskSetting> implements ITaskSettingService {

    @Override
    public IPage<TaskSetting> findTaskSettingPageList(Long orgId, IPage<TaskSetting> page) {
        QueryWrapper<TaskSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ts.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("so.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("sl.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId),"ts.org_id",orgId);
        queryWrapper.orderByDesc("sl.sort,ts.create_time");
        return baseMapper.findTaskSettingPageList(page, queryWrapper);
    }

    @Override
    public TaskSetting findTaskSettingOne(Long orgId, Long salLevelId) {
        QueryWrapper<TaskSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId),"org_id",orgId);
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(salLevelId),"salesman_level_id",salLevelId);
        queryWrapper.orderByDesc("create_time");
        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public TaskSetting findTaskSettingOne(Long id) {
        return baseMapper.selectById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskSetting(TaskSetting setting) {
        setting.setStatus(Status.STATUS_ACTIVITY);
        setting.setCreateTime(new Date());
        baseMapper.insert(setting);
    }

    @Cacheable(cacheNames="TaskSetting",sync=true,key="#orgId +'-'+#salLevelId")
    @Override
    public TaskSetting findTaskSettingByOrgIdAndSalLevelId(Long orgId, Long salLevelId) {
        QueryWrapper<TaskSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId),"org_id",orgId);
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq(StrUtil.isNotNull(salLevelId),"salesman_level_id",salLevelId);
        queryWrapper.orderByDesc("create_time");
        queryWrapper.last("limit 1");
        return baseMapper.selectOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames="TaskSetting", allEntries=true)
    @Override
    public void updateTaskSetting(TaskSetting setting) {
        baseMapper.updateById(setting);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteTaskSetting(Long id) {
        TaskSetting taskSetting = new TaskSetting();
        taskSetting.setStatus(Status.STATUS_DELETE);
        UpdateWrapper<TaskSetting> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",id);
        baseMapper.update(taskSetting,updateWrapper);
    }

    @Override
    public TaskSetting findTaskSettingByOrgIdAndCredit(Long orgId, BigDecimal credit) {
        QueryWrapper<TaskSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(orgId) && !Number.LONG_ZERO.equals(orgId),"org_id",orgId);
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.ge(StrUtil.isNotNull(credit),"credit",credit);
        queryWrapper.orderByAsc("credit");
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Cacheable(cacheNames="TaskSetting",sync=true,key="'taskSettingMin-'+#orgId")
    @Override
    public TaskSetting findTaskSettingMinByOrgId(Long orgId) {
        QueryWrapper<TaskSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(orgId),"org_id",orgId);
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.orderByAsc("credit");
        return baseMapper.selectOne(queryWrapper);
    }
}
