package com.sbdj.service.task.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonAppealVo;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.entity.TaskSonAppeal;
import com.sbdj.service.task.mapper.TaskSonAppealMapper;
import com.sbdj.service.task.mapper.TaskSonMapper;
import com.sbdj.service.task.service.ITaskSonAppealService;
import com.sbdj.service.task.service.ITaskSonService;
import com.sbdj.service.task.status.AppealStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 * 子任务申诉表 服务实现类
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class TaskSonAppealServiceImpl extends ServiceImpl<TaskSonAppealMapper, TaskSonAppeal> implements ITaskSonAppealService {
    @Autowired
    private ITaskSonService iTaskSonService;

    @Override
    public IPage<TaskSonAppealVo> findTaskSonAppealByPage(TaskSonQuery query, IPage<TaskSonAppealVo> page) {
        QueryWrapper<TaskSonQuery> queryWrapper = new QueryWrapper<>();
        //所属机构
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId()),"tsa.org_id",query.getOrgId());
        // ~店铺名称
        queryWrapper.eq(StrUtil.isNotNull(query.getShopName()),"ss.shop_name",query.getShopName());
        // ~子任务单号
        queryWrapper.eq(StrUtil.isNotNull(query.getTaskSonNumber()),"ts.task_son_number",query.getTaskSonNumber());
        // ~状态
        queryWrapper.eq(StrUtil.isNotNull(query.getStatus()),"tsa.status",query.getStatus());
        // 开始时间
        queryWrapper.ge(StrUtil.isNotNull(query.getBeginTime()),"ts.create_time",query.getBeginTime());
        // 结束时间
        queryWrapper.le(StrUtil.isNotNull(query.getEndTime()),"ts.create_time",query.getEndTime());
        // 业务员名称
        queryWrapper.eq(StrUtil.isNotNull(query.getSalesmanName()),"s.name",query.getSalesmanName());
        // 号主Id
        queryWrapper.eq(StrUtil.isNotNull(query.getOnlineid()),"sn.onlineid",query.getOnlineid());
        // 审核人名称
        queryWrapper.eq(StrUtil.isNotNull(query.getAuditorName()),"sa.name",query.getAuditorName());
        //按创建时间倒序排列
        queryWrapper.orderByDesc("ts.create_time");

        return baseMapper.findTaskSonAppealByPage(page,queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskSonAppeal(TaskSonAppeal taskSonAppeal) {
        // 设置申诉时间
        taskSonAppeal.setCreateTime(new Date());
        // 设置状态 默认待审核
        taskSonAppeal.setStatus(Status.STATUS_ALREADY_UPLOAD);
        baseMapper.insert(taskSonAppeal);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskSonAppeal(Long taskSonId, String reasons, String images) {
        TaskSon ts = iTaskSonService.findTaskSonOne(taskSonId);
        if (StrUtil.isNotNull(ts)) {
            TaskSonAppeal tsa = new TaskSonAppeal();
            tsa.setOrgId(ts.getOrgId());
            tsa.setCreateTime(new Date());
            tsa.setImages(images);
            tsa.setReasons(reasons);
            tsa.setTaskSonId(taskSonId);
            tsa.setSalesmanId(ts.getSalesmanId());
            tsa.setSalesmanNumberId(ts.getSalesmanNumberId());
            tsa.setStatus(AppealStatus.STATUS_WAIT.getCode());
            baseMapper.insert(tsa);

            // 修改子任务状态, AF--AP
            // 申诉状态只能在待提交状态下提交
            iTaskSonService.taskSonAppeal(taskSonId);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskSonAppeal(TaskSonAppeal taskSonAppeal) {
        baseMapper.updateById(taskSonAppeal);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteTaskSonAppealById(Long id, Long oprId) {
        TaskSonAppeal taskSonAppeal = new TaskSonAppeal();
        taskSonAppeal.setStatus(Status.STATUS_DELETE);
        taskSonAppeal.setAuditorId(oprId);
        taskSonAppeal.setAuditorTime(new Date());
        UpdateWrapper<TaskSonAppeal> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",id);
        updateWrapper.eq("status",Status.STATUS_ALREADY_UPLOAD);
        baseMapper.update(taskSonAppeal,updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void reviewTaskSonAppeal(Long id, Long taskSonId, String beginStatus, String endStatus, String account, Long oprId) {

        // 由AP--T
        TaskSon taskSon= new TaskSon();
        taskSon.setStatus(Status.STATUS_YES_AUDITED);
        UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",taskSonId);
        updateWrapper.eq("status",Status.STATUS_APPEAL);
        iTaskSonService.update(taskSon,updateWrapper);
        //iTaskSonAppealDao.updateTaskSonAppealById(id, beginStatus, endStatus, oprId);
        TaskSonAppeal taskSonAppeal = new TaskSonAppeal();
        taskSonAppeal.setStatus(endStatus);
        taskSonAppeal.setAuditorId(oprId);
        taskSonAppeal.setAccount(account);
        taskSonAppeal.setAuditorTime(new Date());
        UpdateWrapper<TaskSonAppeal> updateWrapper2 = new UpdateWrapper<>();
        updateWrapper2.eq(StrUtil.isNotNull(id),"id",id);
        updateWrapper2.eq(StrUtil.isNotNull(beginStatus),"status",beginStatus);
        baseMapper.update(taskSonAppeal,updateWrapper2);
        /*
        TaskSon taskSon = iTaskSonService.findTaskSonOne(taskSonId);
        // 生成奖罚
        PrizePenalty prizePenalty = new PrizePenalty();
        prizePenalty.setCreateTime(new Date());
        prizePenalty.setOrgId(taskSon.getOrgId());
        prizePenalty.setSalesmanId(taskSon.getSalesmanId());
        prizePenalty.setSalesmanNumberId(taskSon.getSalesmanNumberId());
        prizePenalty.setTaskId(taskSon.getTaskId());
        prizePenalty.setTaskSonId(taskSon.getId());
        prizePenalty.setReason(SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getMessage());
        prizePenalty.setAuditorId(oprId);
        prizePenalty.setAuditorTime(new Date());
        prizePenalty.setMoney(taskSon.getBrokerage());
        // 差价为负则为奖励,差价为正则为惩罚
        prizePenalty.setType(Status.PRICE_REWORD);
        prizePenalty.setStatus(Status.STATUS_ENABLE);

        // 获取业务员信息
        Salesman salesman = iSalesmanService.findSalesmanById(taskSon.getSalesmanId());

        // 生成账单
        SalesmanBill salesmanBill = new SalesmanBill();
        salesmanBill.setOrgId(taskSon.getOrgId());
        salesmanBill.setSalesmanId(taskSon.getSalesmanId());
        salesmanBill.setBillType(SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getCodeType());
        salesmanBill.setTargetId(taskSon.getId());
        salesmanBill.setLastMoney(salesman.getBalance());
        salesmanBill.setMoney(taskSon.getBrokerage().negate());
        // 差价为负则为增加,差价为正则为减少
        salesmanBill.setFinalMoney(salesman.getBalance().add(taskSon.getBrokerage()));
        salesmanBill.setAccount(SalesmanBillTypeMsg.BILL_TYPE_BROKERAGE.getMessage());
        salesmanBill.setBillDate(new Date());
        salesmanBill.setCreateTime(new Date());


        // 更新业务员余额 差价为负则为增加,差价为正则为减少
        iSalesmanService.updatesalesmanBrokerage(prizePenalty.getSalesmanId(), salesman.getBalance().add(taskSon.getBrokerage()));
        // 保存奖罚
        iPrizePenaltyService.saveiPrizePenal(prizePenalty);
        // 保存账单
        iSalesmanBillService.saveSalesmanBill(salesmanBill);
        */
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void reviewTaskSonAppeal(Long id, String beginStatus, String endStatus, String account, Long oprId) {
        TaskSonAppeal taskSonAppeal = new TaskSonAppeal();
        taskSonAppeal.setStatus(endStatus);
        taskSonAppeal.setAuditorId(oprId);
        taskSonAppeal.setAccount(account);
        taskSonAppeal.setAuditorTime(new Date());
        UpdateWrapper<TaskSonAppeal> updateWrapper2 = new UpdateWrapper<>();
        updateWrapper2.eq(StrUtil.isNotNull(id),"id",id);
        updateWrapper2.eq(StrUtil.isNotNull(beginStatus),"status",beginStatus);
        baseMapper.update(taskSonAppeal,updateWrapper2);
    }
}
