package com.sbdj.service.task.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.constant.Status;
import com.sbdj.service.task.admin.vo.TaskSonImagesVo;
import com.sbdj.service.task.entity.TaskSonImages;
import com.sbdj.service.task.mapper.TaskSonImagesMapper;
import com.sbdj.service.task.service.ITaskSonImagesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 子任务所属图片集合表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class TaskSonImagesServiceImpl extends ServiceImpl<TaskSonImagesMapper, TaskSonImages> implements ITaskSonImagesService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteTasksonImgsByTaskSonId(Long taskSonId) {
        TaskSonImages taskSonImages = new TaskSonImages();
        taskSonImages.setStatus(Status.STATUS_DELETE);
        UpdateWrapper<TaskSonImages> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("task_son_id",taskSonId);
        updateWrapper.eq("status",Status.STATUS_ACTIVITY);
        baseMapper.update(taskSonImages,updateWrapper);
    }

    @Override
    public List<TaskSonImagesVo> findTaskSonImagesList(Long taskSonId) {
        return baseMapper.findTaskSonImagesList(taskSonId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskSonImages(TaskSonImages sonImages) {
        sonImages.setCreateTime(new Date());
        sonImages.setStatus(Status.STATUS_ACTIVITY);
        baseMapper.insert(sonImages);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteTasksonImgsByTaskSonId(Long taskSonId, Long tptsId) {
        TaskSonImages taskSonImages = new TaskSonImages();
        taskSonImages.setStatus(Status.STATUS_DELETE);
        UpdateWrapper<TaskSonImages> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("task_son_id",taskSonId);
        updateWrapper.eq("tpts_id",tptsId);
        updateWrapper.eq("status",Status.STATUS_ACTIVITY);
        baseMapper.update(taskSonImages,updateWrapper);
    }

    @Override
    public List<TaskSonImages> findTaskSonImagesByTaskSonId(Long taskSonId) {
        QueryWrapper<TaskSonImages> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_son_id",taskSonId);
        queryWrapper.eq("status",Status.STATUS_ACTIVITY);
        return baseMapper.selectList(queryWrapper);
    }

}
