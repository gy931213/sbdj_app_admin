package com.sbdj.service.task.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.service.task.entity.TaskSonNotPass;
import com.sbdj.service.task.mapper.TaskSonNotPassMapper;
import com.sbdj.service.task.service.ITaskSonNotPassService;
import com.sbdj.service.task.status.NotPassStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 子任务未通过

 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class TaskSonNotPassServiceImpl extends ServiceImpl<TaskSonNotPassMapper, TaskSonNotPass> implements ITaskSonNotPassService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteTaskSonNotPass(Long taskSonId) {
        TaskSonNotPass taskSonNotPass = new TaskSonNotPass();
        taskSonNotPass.setStatus(NotPassStatus.STATUS_DELETE.getCode());
        UpdateWrapper<TaskSonNotPass> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("task_son_id",taskSonId);
        updateWrapper.eq("status",NotPassStatus.STATUS_ACTIVITY.getCode());
        baseMapper.update(taskSonNotPass,updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskSonNotPass(TaskSonNotPass taskSonNotPass) {
        taskSonNotPass.setCreateTime(new Date());
        taskSonNotPass.setStatus(NotPassStatus.STATUS_ACTIVITY.getCode());
        baseMapper.insert(taskSonNotPass);
    }
}
