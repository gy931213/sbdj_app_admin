package com.sbdj.service.task.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.base.TaskRedis;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.Status;
import com.sbdj.core.constant.TaskTypeCode;
import com.sbdj.core.util.CopyObjectUtil;
import com.sbdj.core.util.DateUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.member.type.PayGoodsType;
import com.sbdj.service.system.entity.SysConfigDtl;
import com.sbdj.service.system.service.ISysConfigDtlService;
import com.sbdj.service.system.type.SysConfigType;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonNullifyVo;
import com.sbdj.service.task.entity.Task;
import com.sbdj.service.task.entity.TaskKeyword;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.entity.TaskSonNullify;
import com.sbdj.service.task.mapper.TaskSonNullifyMapper;
import com.sbdj.service.task.service.ITaskKeywordService;
import com.sbdj.service.task.service.ITaskService;
import com.sbdj.service.task.service.ITaskSonNullifyService;
import com.sbdj.service.task.service.ITaskSonService;
import com.sbdj.service.task.status.NullifyStatus;
import com.sbdj.service.task.type.KeywordType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 子任务作废表 服务实现类
 * </p>
 *
 * @author Gy
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class TaskSonNullifyServiceImpl extends ServiceImpl<TaskSonNullifyMapper, TaskSonNullify> implements ITaskSonNullifyService {
    private Logger logger = LoggerFactory.getLogger(TaskSonNullifyServiceImpl.class);

    @Autowired
    private ITaskSonService iTaskSonService;
    @Autowired
    private ISysConfigDtlService iConfigDtlService;
    @Autowired
    private ITaskService iTaskService;
    @Autowired
    private ITaskKeywordService iTaskKeywordService;
    @Autowired
    private ISalesmanService iSalesmanService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskSonNullify(Long taskSonId, String reasons, String images) {
        TaskSon ts = iTaskSonService.getById(taskSonId);
        if (StrUtil.isNotNull(ts)) {
            // 保存子任务作废的信息
            TaskSonNullify tsn = new TaskSonNullify();
            tsn.setOrgId(ts.getOrgId());
            tsn.setCreateTime(new Date());
            tsn.setImages("disable");
            tsn.setReasons(reasons);
            tsn.setTaskSonId(taskSonId);
            tsn.setSalesmanId(ts.getSalesmanId());
            tsn.setSalesmanNumberId(ts.getSalesmanNumberId());
            tsn.setStatus(NullifyStatus.STATUS_WAIT.getCode());
            baseMapper.insert(tsn);
            //根据任务类型配置信息
            List<SysConfigDtl> cfds = iConfigDtlService.findConfigDtl(SysConfigType.TASK_TYPE);
            Long grd = 0L;
            Long xfd = 0L;

            for (SysConfigDtl cfd : cfds) {

                if (cfd.getCode().equalsIgnoreCase(TaskTypeCode.TASK_TYPE_GRD)) {
                    grd = cfd.getId();
                } else if (cfd.getCode().equalsIgnoreCase(TaskTypeCode.TASK_TYPE_XFD)) {
                    xfd = cfd.getId();
                }
            }

            if (!ts.getTaskTypeId().equals(grd)) {
                // 重新回归任务池
                // 更新主任务剩余数量,增加
                iTaskService.updateTaskResidueTotalLock(ts.getTaskId(), Boolean.TRUE);
                // 更新任务搜索词剩余数量，增加
                iTaskKeywordService.updateTaskKeywordNumberLock(ts.getKeywordId(), Boolean.TRUE);
                // 更新业务员信用额度,增加额度
                // iSalesmanService.updateSalesmanCreditById(ts.getSalesmanId(), ts.getRealPrice(), false);
                // 将任务剩余数量扔回到响应的队列中!
                // 返回任务可领取数量，返回到任务池中即可。
                TaskRedis.increment(ts.getTaskId());
                // 重新回归任务池
            } else {
                // 隔日单判断
                Task task = iTaskService.findTaskOne(ts.getTaskId());
                Date date = new Date();
                // 任务剩余数
                //task.setTaskResidueTotal(task.getTaskResidueTotal());
                // 日期已经过完一天则重新发布现付单,否则重新回归任务池
                int hour = DateUtil.hourBetween(ts.getCreateTime(), date);
                if (hour > 6) {
                    // 任务总数
                    // 更新任务数据
                    iTaskService.updateTaskResidueTotalLock(ts.getTaskId(), Boolean.FALSE);
                    task.setTaskTotal(task.getTaskTotal() - 1);
                    iTaskService.updateTaskTotal(task.getId(), task.getTaskTotal());
                    // 重新发布一个现付单
                    // 现付单
                    Task xfdTask = new Task();
                    CopyObjectUtil.copyProperties(task, xfdTask);

                    xfdTask.setId(null);
                    xfdTask.setTaskTotal(1);
                    xfdTask.setTaskResidueTotal(1);
                    xfdTask.setTaskTypeId(xfd);
                    xfdTask.setCreateTime(date);
                    xfdTask.setTimingTime(date);

                    iTaskService.saveTask(xfdTask);
                    iTaskService.updateStatusRun(xfdTask.getId(), 1L, Status.STATUS_RELEASED);

                    TaskKeyword taskKeyword = new TaskKeyword();
                    taskKeyword.setTaskId(xfdTask.getId());
                    taskKeyword.setIsCard(0);
                    taskKeyword.setKeyType(KeywordType.KEYWORD_S);
                    taskKeyword.setKeyword(ts.getKeyword());
                    taskKeyword.setNumber(xfdTask.getTaskResidueTotal());
                    iTaskKeywordService.saveTaskKeyword(taskKeyword);

                    if (!TaskRedis.setTaskTotalCacheKey(xfdTask.getId(), xfdTask.getTaskResidueTotal())) {
                        logger.error("===>【任务更新，并且缓存“任务可领数量”失败！要缓存的ID：{}，任务编号为：{}】", task.getId(), task.getTaskNumber());
                    }
                    // 重新回归任务池
                } else {
                    // 重新回归任务池
                    // 更新主任务剩余数量,增加
                    iTaskService.updateTaskResidueTotalLock(ts.getTaskId(), Boolean.TRUE);
                    // 更新任务搜索词剩余数量，增加
                    iTaskKeywordService.updateTaskKeywordNumberLock(ts.getKeywordId(), Boolean.TRUE);
                    // 更新业务员信用额度,增加额度
                    // iSalesmanService.updateSalesmanCreditById(ts.getSalesmanId(), ts.getRealPrice(), false);
                    // 将任务剩余数量扔回到响应的队列中!
                    // 返回任务可领取数量，返回到任务池中即可。
                    TaskRedis.increment(ts.getTaskId());
                    // 重新回归任务池
                }
            }

            // 修改子任务状态，F -- NF
            // 作废操作只能在待提交状态下提交
            iTaskSonService.taskSonNullify(taskSonId);
        }
    }

    @Override
    public IPage<TaskSonNullifyVo> findTaskSonNullifyByPage(TaskSonQuery query, IPage<TaskSonNullifyVo> page) {
        QueryWrapper<TaskSonQuery> queryWrapper = new QueryWrapper<>();
        //所属机构
        queryWrapper.eq(StrUtil.isNotNull(query.getOrgId()) && !Number.LONG_ZERO.equals(query.getOrgId()),"tsn.org_id",query.getOrgId());
        // ~店铺名称
        queryWrapper.eq(StrUtil.isNotNull(query.getShopName()),"ss.shop_name",query.getShopName());
        // ~子任务单号
        queryWrapper.eq(StrUtil.isNotNull(query.getTaskSonNumber()),"ts.task_son_number",query.getTaskSonNumber());
        // ~状态
        queryWrapper.eq(StrUtil.isNotNull(query.getStatus()),"tsn.status",query.getStatus());
        // 开始时间
        queryWrapper.ge(StrUtil.isNotNull(query.getBeginTime()),"ts.create_time",query.getBeginTime());
        // 结束时间
        queryWrapper.le(StrUtil.isNotNull(query.getEndTime()),"ts.create_time",query.getEndTime());
        // 业务员名称
        queryWrapper.eq(StrUtil.isNotNull(query.getSalesmanName()),"s.name",query.getSalesmanName());
        // 号主Id
        queryWrapper.eq(StrUtil.isNotNull(query.getOnlineid()),"sn.onlineid",query.getOnlineid());
        // 审核人名称
        queryWrapper.eq(StrUtil.isNotNull(query.getAuditorName()),"sa.name",query.getAuditorName());
        //按创建时间倒序排序
        queryWrapper.orderByDesc("ts.create_time");

        IPage<TaskSonNullifyVo> list = baseMapper.findTaskSonNullifyByPage(page,queryWrapper);
        for (TaskSonNullifyVo tsn : list.getRecords()) {
            if (StrUtil.isNotNull(tsn.getPaymentType()) && PayGoodsType.PAY_GOODS_TYPE_DF.equalsIgnoreCase(tsn.getPaymentType())) {
                tsn.setPaymentTypeName("垫付");
            } else if (StrUtil.isNotNull(tsn.getPaymentType()) && PayGoodsType.PAY_GOODS_TYPE_SQ.equalsIgnoreCase(tsn.getPaymentType())) {
                tsn.setPaymentTypeName("申请");
            }
        }
        return list;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void reviewTaskSonNullify(Long id, String beginStatus, String endStatus, String account, Long oprId) {
        TaskSonNullify taskSonNullify = new TaskSonNullify();
        taskSonNullify.setStatus(endStatus);
        taskSonNullify.setAuditorId(oprId);
        taskSonNullify.setAccount(account);
        taskSonNullify.setAuditorTime(new Date());
        UpdateWrapper<TaskSonNullify> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",id);
        updateWrapper.eq(StrUtil.isNotNull(beginStatus),"status",beginStatus);
        baseMapper.update(taskSonNullify,updateWrapper);
        // 如果是申请并且审核通过 则恢复额度
        if (endStatus.equalsIgnoreCase(Status.STATUS_YES_AUDITED)) {
            TaskSonNullifyVo tsnExt = findTaskSonPaymentType(id);
            if (PayGoodsType.PAY_GOODS_TYPE_SQ.equalsIgnoreCase(tsnExt.getPaymentType())) {
                // [{业务员},{额度值},{增减信用额度}]
                iSalesmanService.updateSalesmanCreditById(tsnExt.getSalesmanId(), tsnExt.getRealPrice(), Boolean.FALSE);
            }
        }
    }

    @Override
    public TaskSonNullifyVo findTaskSonPaymentType(Long id) {
        return baseMapper.findTaskSonPaymentType(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void reviewTaskSonNullifyNotRecovering(Long id, String beginStatus, String endStatus, String account, Long oprId) {
        TaskSonNullify taskSonNullify= new TaskSonNullify();
        taskSonNullify.setStatus(endStatus);
        taskSonNullify.setAuditorId(oprId);
        taskSonNullify.setAccount(account);
        taskSonNullify.setAuditorTime(new Date());
        UpdateWrapper<TaskSonNullify> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",id);
        updateWrapper.eq(StrUtil.isNotNull(beginStatus),"status",beginStatus);
        baseMapper.update(taskSonNullify,updateWrapper);
    }

    @Override
    public IPage<TaskSonNullifyVo> findTaskSonNullifyPage(Long salesmanId, String status, IPage<TaskSonNullifyVo> page) {
        QueryWrapper<TaskSonNullifyVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(salesmanId),"tsn.salesman_id",salesmanId);
        queryWrapper.eq("tsn.`status`",status);
        baseMapper.findTaskSonNullifyPage(page,queryWrapper);
        return page;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskSonNullify(TaskSonNullify taskSonNullify) {
        taskSonNullify.setAuditorTime(new Date());
        baseMapper.updateById(taskSonNullify);

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void logicDeleteTaskSonNullifyById(Long id, Long oprId) {
        TaskSonNullify taskSonNullify = new TaskSonNullify();
        taskSonNullify.setStatus(Status.STATUS_DELETE);
        taskSonNullify.setAuditorId(oprId);
        taskSonNullify.setAuditorTime(new Date());
        UpdateWrapper<TaskSonNullify> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",id);
        updateWrapper.eq("status",Status.STATUS_ALREADY_UPLOAD);
        baseMapper.update(taskSonNullify,updateWrapper);
    }
}
