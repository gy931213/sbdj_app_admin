package com.sbdj.service.task.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sbdj.core.base.TaskRedis;
import com.sbdj.core.constant.Number;
import com.sbdj.core.constant.*;
import com.sbdj.core.exception.BaseException;
import com.sbdj.core.util.DateUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.configure.entity.ConfigPlatform;
import com.sbdj.service.configure.service.IConfigPlatformService;
import com.sbdj.service.finance.entity.FinanceDifferPrice;
import com.sbdj.service.finance.entity.FinancePayGoods;
import com.sbdj.service.finance.entity.FinanceStatement;
import com.sbdj.service.finance.scheduler.vo.StatementPercentageVo;
import com.sbdj.service.finance.scheduler.vo.StatementTaskSonVo;
import com.sbdj.service.finance.service.IFinanceDifferPriceService;
import com.sbdj.service.finance.service.IFinancePayGoodsService;
import com.sbdj.service.finance.service.IFinanceStatementService;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanNumber;
import com.sbdj.service.member.service.ISalesmanNumberService;
import com.sbdj.service.member.service.ISalesmanService;
import com.sbdj.service.seller.entity.SellerShop;
import com.sbdj.service.seller.entity.SellerShopGoods;
import com.sbdj.service.seller.service.ISellerShopGoodsService;
import com.sbdj.service.seller.service.ISellerShopService;
import com.sbdj.service.system.type.SysConfigType;
import com.sbdj.service.task.admin.query.TaskSonQuery;
import com.sbdj.service.task.admin.vo.TaskSonBeforeDayVo;
import com.sbdj.service.task.admin.vo.TaskSonVo;
import com.sbdj.service.task.app.vo.*;
import com.sbdj.service.task.entity.TaskSetting;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.entity.TaskSonImages;
import com.sbdj.service.task.mapper.TaskSonMapper;
import com.sbdj.service.task.redis.FormNoTypeEnum;
import com.sbdj.service.task.redis.generate.intf.IFormNoGenerateService;
import com.sbdj.service.task.scheduler.vo.TaskSonSalesmanVo;
import com.sbdj.service.task.service.*;
import com.sbdj.service.task.type.IsMarkType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 任务子表 服务实现类
 * </p>
 *
 * @author Yly
 * @since 2019-11-22
 */
@Transactional(readOnly = true)
@Service
public class TaskSonServiceImpl extends ServiceImpl<TaskSonMapper, TaskSon> implements ITaskSonService {

    private Logger logger = LoggerFactory.getLogger(TaskSonServiceImpl.class);


    @Autowired
    private IFinanceDifferPriceService iDifferPriceService;
    @Autowired
    private ITaskSonNullifyService iTaskSonNullifyService;
    @Autowired
    private ISalesmanService iSalesmanService;
    @Autowired
    private ITaskKeywordService iTaskKeywordService;
    @Autowired
    private ITaskService iTaskService;
    @Autowired
    private ITaskSonService iTaskSonService;
    @Autowired
    private ITaskSettingService iTaskSettingService;
    @Autowired
    private ISalesmanNumberService iSalesmanNumberService;
    @Autowired
    private ISellerShopService iSellerShopService;
    @Autowired
    private IFinancePayGoodsService iPayGoodsService;
    @Autowired
    private IFormNoGenerateService iFormNoGenerateService;
    @Autowired
    private ITaskSonImagesService iTaskSonImagesService;
    @Autowired
    private IFinanceStatementService iFinanceStatementService;
    @Autowired
    private ISellerShopGoodsService iSellerShopGoodsService;
    @Autowired
    private IConfigPlatformService iConfigPlatformService;

    @Override
    public int getTaskSonNumByStatusAndTaskId(Long taskId,String status) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(taskId),"task_id",taskId);
        queryWrapper.in(StrUtil.isNotNull(status),"status",status);
        return baseMapper.selectCount(queryWrapper);
    }


    @Override
    public IPage<TaskSonVo> findTaskSonNumPageList(TaskSonQuery sonQuery, IPage<TaskSonVo> page) {
        QueryWrapper<TaskSonQuery> queryWrapper = new QueryWrapper<>();

        //历史数据  查询5天前的数据  有问题
        Date date = DateUtil.genDayDate(-5, new Date());
        queryWrapper.ge(StrUtil.isNotNull(sonQuery.getIsHistory()) && Status.STATUS_NO.equalsIgnoreCase(sonQuery.getIsHistory()),"t.create_time",date);
        //状态
        queryWrapper.eq("ssg.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("ss.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("cp.status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("scd.status", Status.STATUS_ACTIVITY);
        //所属机构
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getOrgId()) && !Number.LONG_ZERO.equals(sonQuery.getOrgId()),"t.org_id",sonQuery.getOrgId());
        // ~任务单号
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getTaskNumber()),"t.task_number", sonQuery.getTaskNumber());
        // ~子任务单号
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getTaskSonNumber()),"ts.task_son_number",sonQuery.getTaskSonNumber());
        // ~是否超时
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getIsTimeOut()),"ts.is_time_out",sonQuery.getIsTimeOut());
        // 作废名单
        boolean flag = true;
        // ~状态
        if (StrUtil.isNotNull(sonQuery.getStatus())) {
            queryWrapper.eq("ts.status",sonQuery.getStatus());
            if (sonQuery.getStatus().equals(Status.STATUS_NULLIFY)) {
                flag = false;
            }
        }
        queryWrapper.notIn(flag,"ts.`status`", Status.STATUS_NULLIFY);
        //开始时间
        queryWrapper.ge(StrUtil.isNotNull(sonQuery.getBeginTime()),"ts.create_time",sonQuery.getBeginTime());
        //结束时间
        queryWrapper.le(StrUtil.isNotNull(sonQuery.getEndTime()),"ts.create_time",sonQuery.getEndTime());
        // 店铺名称
        queryWrapper.like(StrUtil.isNotNull(sonQuery.getShopName()),"ss.shop_name",sonQuery.getShopName());
        // 订单号
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getOrderNum()),"ts.order_num",sonQuery.getOrderNum());
        // 业务员名称
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getSalesmanName()),"s.name",sonQuery.getSalesmanName());
        // 号主Id
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getOnlineid()),"sn.onlineid",sonQuery.getOnlineid());
        // 审核人名称
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getAuditorName()),"sa.name",sonQuery.getAuditorName());
        // 平台编号
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getPlatformId()) && !Number.LONG_ZERO.equals(sonQuery.getPlatformId()),"cp.id",sonQuery.getPlatformId());
        // 任务类型编号
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getTaskTypeId()) && !Number.LONG_ZERO.equals(sonQuery.getTaskTypeId()),"ts.task_type_id",sonQuery.getTaskTypeId());
        // 是否对单
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getOrderMatch()),"ts.order_match",sonQuery.getOrderMatch());
        // 任务类型
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getTaskSonType()),"fpg.type",sonQuery.getTaskSonType());
        // 货款类型
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getTaskSonType()),"fpg.type",sonQuery.getTaskSonType());
        // 是否购买应用 [{YES:是},{NO:否}]
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getIsBuy()),"ss.is_buy",sonQuery.getIsBuy());
        // 业务员手机
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getSalesmanPhone()), "s.mobile", sonQuery.getSalesmanPhone());
        // 号主手机
        queryWrapper.eq(StrUtil.isNotNull(sonQuery.getSalesmanNumberPhone()), "sn.mobile", sonQuery.getSalesmanNumberPhone());
        // 商品类目
        queryWrapper.like(StrUtil.isNotNull(sonQuery.getCategory()), "ssg.category", sonQuery.getCategory());

        //按时间倒序输出
        queryWrapper.orderByDesc("ts.create_time");
        //获取任务数据
        IPage<TaskSonVo> pageList =  baseMapper.findTaskSonPageList(page,queryWrapper);

        // 数据封装
        for (TaskSonVo TaskSonVo : pageList.getRecords()) {
            // 订单状态
            switch (TaskSonVo.getOrderStatus()) {
                case 0:
                    TaskSonVo.setOrderStatusName("待付款");
                    break;
                case 1:
                    TaskSonVo.setOrderStatusName("待发货");
                    break;
                case 2:
                    TaskSonVo.setOrderStatusName("待收货");
                    break;
                case 3:
                    TaskSonVo.setOrderStatusName("已完成");
                    break;
                case -1:
                    TaskSonVo.setOrderStatusName("已关闭");
                    break;
                case -3:
                    TaskSonVo.setOrderStatusName("默认");
                    break;
            }
            // 状态
            switch (TaskSonVo.getStatus()) {
                case Status.STATUS_ACTIVITY:
                    TaskSonVo.setStatusName("待操作");
                    break;
                case Status.STATUS_NOT_AUDITED:
                    TaskSonVo.setStatusName("待提交");
                    break;
                case Status.STATUS_YES_AUDITED:
                    TaskSonVo.setStatusName("已审核");
                    break;
                case Status.STATUS_NOT_PASS:
                    TaskSonVo.setStatusName("不通过");
                    break;
                case Status.STATUS_AUDITED_FAIL:
                    TaskSonVo.setStatusName("已失败");
                    break;
                case Status.STATUS_ALREADY_UPLOAD:
                    TaskSonVo.setStatusName("已传图");
                    break;
                case Status.STATUS_NULLIFY:
                    TaskSonVo.setStatusName("已作废");
                    break;
                case Status.STATUS_APPEAL:
                    TaskSonVo.setStatusName("申诉中");
                    break;
            }

        }

        return pageList;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void forceAuditorTaskSonByTaskSonIds(String ids, Long auditorId) {
        List<Long> list = StrUtil.getIdsByLong(ids);
        if (StrUtil.isNotNull(list)) {
            TaskSon taskSon;
            // 批量标记
            for (Long taskSonId : list) {
                // 获取子任务数据
                taskSon = baseMapper.selectById(taskSonId);
                // 匹配应付款与实付款
                saveDifferPrice(taskSon.getRealPrice(), taskSon.getPayment(), taskSon, Status.STATUS_YES_AUDITED);
                //修改子任务状态为已审核
                TaskSon tk = new TaskSon();
                tk.setStatus(Status.STATUS_YES_AUDITED);
                tk.setAuditorId(auditorId);
                tk.setUpdateTime(new Date());
                UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
                updateWrapper.eq(StrUtil.isNotNull(taskSonId),"id",taskSonId);
                baseMapper.update(tk,updateWrapper);
                // 非超时状态
                /*if (taskSon.getIsTimeOut().equalsIgnoreCase(Status.NOT_OK)) {
                    // 生成佣金流水数据
                    FinanceStatement statement = new FinanceStatement();
                    statement.setSalesmanId(taskSon.getSalesmanId());
                    statement.setTaskSonId(taskSon.getId());
                    statement.setMoney(taskSon.getBrokerage());
                    statement.setTaskSonStatus(Status.STATUS_YES_AUDITED);
                    statement.setStatus(Number.INT_ONE);
                    statement.setCreateTime(new Date());
                    iFinanceStatementService.save(statement);
                }*/
                // 标记该业务员已经完成首次任务
                iSalesmanService.updateSalesmanFirstMission(taskSon.getSalesmanId());
                iSalesmanService.updateSalesmanTaskTotal(taskSon.getSalesmanId(), 1);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveDifferPrice(BigDecimal realPrice, BigDecimal payment, TaskSon taskSon, String status) {
        // 不是已审核的任务 不操作
        if (!Status.STATUS_YES_AUDITED.equals(status)) {
            return;
        }
        FinanceDifferPrice dp = iDifferPriceService.findDifferPriceByTaskSonId(taskSon.getId());

        if (StrUtil.isNotNull(dp) && Status.STATUS_YES.equals(dp.getStatus())) {
            return;
        }

        FinanceDifferPrice differPrice;

        // dp等于空时，并且应付和实付【存在】差价。
        boolean isDifferPrice = payment.compareTo(BigDecimal.ZERO) > 0 && realPrice.compareTo(payment) != 0;
        if (StrUtil.isNull(dp) && isDifferPrice) {
            differPrice = new FinanceDifferPrice();
            differPrice.setOrgId(taskSon.getOrgId());
            differPrice.setTaskId(taskSon.getTaskId());
            differPrice.setTaskSonId(taskSon.getId());
            differPrice.setShopId(taskSon.getSellerShopId());
            differPrice.setShopGoodsId(taskSon.getGoodsId());
            differPrice.setRealPrice(taskSon.getRealPrice());
            differPrice.setPayment(taskSon.getPayment());
            differPrice.setDifferPrice(realPrice.subtract(payment));
            differPrice.setSalesmanId(taskSon.getSalesmanId());
            differPrice.setSalesmanNumberId(taskSon.getSalesmanNumberId());
            differPrice.setCreateTime(taskSon.getCreateTime());
            differPrice.setTaskTypeId(taskSon.getTaskTypeId());

            differPrice.setStatus(Status.STATUS_NO);
            iDifferPriceService.save(differPrice);
        } else if (StrUtil.isNotNull(dp)) {
            // dp不等于空时，并且应付和实付【存在】差价。
            if (isDifferPrice) {
                differPrice = new FinanceDifferPrice();
                differPrice.setId(dp.getId());
                differPrice.setRealPrice(taskSon.getRealPrice());
                differPrice.setPayment(taskSon.getPayment());
                differPrice.setDifferPrice(realPrice.subtract(payment));
                differPrice.setStatus(Status.NOT_OK);

                try {
                    UpdateWrapper<FinanceDifferPrice> updateWrapper = new UpdateWrapper<>();
                    updateWrapper.eq("id",differPrice.getId());
                    iDifferPriceService.update(differPrice,updateWrapper);
                } catch (Exception e) {
                    logger.error("===>应付和实付【存在】差价，更新失败 id： {}", dp.getId());
                }
            } else {
                // dp不等于空时，并且应付和实付【不存在】差价。
                differPrice = new FinanceDifferPrice();
                differPrice.setId(dp.getId());
                differPrice.setRealPrice(taskSon.getRealPrice());
                differPrice.setPayment(taskSon.getPayment());
                differPrice.setDifferPrice(realPrice.subtract(payment));
                differPrice.setStatus(Status.STATUS_DELETE);
                try {
                    UpdateWrapper<FinanceDifferPrice> updateWrapper = new UpdateWrapper<>();
                    updateWrapper.eq("id",differPrice.getId());
                    iDifferPriceService.update(differPrice,updateWrapper);
                } catch (Exception e) {
                    logger.error("===>应付和实付【不存在】差价，更新失败 id： {}", dp.getId());
                }
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void invalidTaskSon(String taskSonIds) {
        List<Long> list = StrUtil.getIdsByLong(taskSonIds);
        if (StrUtil.isNull(list)) {
            throw BaseException.base("需要作废任务ids为空");
        }
        for (Long id : list) {
            UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("id",id);
            updateWrapper.set("status", Status.STATUS_NULLIFY);
            update(updateWrapper);
            iTaskSonNullifyService.saveTaskSonNullify(id, "作废", null);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TaskSon findTaskSonOne(Long id) {
        return baseMapper.selectById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void taskSonNullify(Long taskSonId) {
        TaskSon taskSon = new TaskSon();
        taskSon.setStatus(Status.STATUS_NULLIFY);
        UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",taskSonId);
        updateWrapper.in("status", Status.STATUS_NOT_AUDITED, Status.STATUS_NOT_PASS);
        baseMapper.update(taskSon,updateWrapper);
    }

    @Override
    public List<TaskSon> searchGRDNotSubmitYesterday() {
        return baseMapper.searchGRDNotSubmitYesterday();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskSon(TaskSon taskSon) {
        updateById(taskSon);
    }

    @Override
    public List<TaskSonBeforeDayVo> findTaskSonIsBeforeDayData(Long orgId, boolean isBeforeDay) {
        return baseMapper.findTaskSonIsBeforeDayData(orgId, isBeforeDay);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskSonIsTimeOut30Minute() {
        logger.info(">>>>>>>>【任务调度（30分钟过期，更新为已超时）】，接口：【updateTaskSonIsTimeOut30Minute】，执行查询任务的过期时间小于当前时间的任务数据>>> start");
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.lt("deadline_time", new Date());
        queryWrapper.eq("status", Status.STATUS_ACTIVITY);
        queryWrapper.eq("is_time_out", IsTimeOutType.TIME_OUT_TYPE_NO);
        queryWrapper.apply("DATE_FORMAT(deadline_time, '%Y-%m-%d') = CURDATE()");
        queryWrapper.select("id");
        List<TaskSon> list = list(queryWrapper);
        List<Long> ids = new ArrayList<>();
        list.forEach(f->ids.add(f.getId()));
        if (ids.size() > 0) {
            UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("is_time_out", IsTimeOutType.TIME_OUT_TYPE_YES);
            updateWrapper.eq("status", Status.STATUS_ACTIVITY);
            updateWrapper.in("id", ids);
            update(updateWrapper);
        }
        logger.info(">>>>>>>>【任务调度（30分钟过期，更新为已超时）】，接口：【updateTaskSonIsTimeOut30Minute】，执行查询任务的过期时间小于当前时间的任务数据>>> end");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskSonIsTimeOut6Hour() {
        logger.info(">>>>>>>>【任务调度（6小时过期，更新为已超时）】，接口：【updateTaskSonIsTimeOut6Hour】执行查询任务的过期时间小于当前时间的任务数据>>> start");
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.lt("deadline_time", new Date());
        queryWrapper.eq("is_time_out", IsTimeOutType.TIME_OUT_TYPE_NO);
        queryWrapper.apply("DATE_FORMAT(deadline_time, '%Y-%m-%d') = CURDATE()");
        queryWrapper.and(f->f.eq("status", Status.STATUS_NOT_AUDITED)
                .or(g->g.eq("status", Status.STATUS_NOT_AUDITED).eq("images", TaskTypeCode.TASK_TYPE_GRD)));
        queryWrapper.select("id");
        List<TaskSon> list = list(queryWrapper);
        List<Long> ids = new ArrayList<>();
        list.forEach(f->ids.add(f.getId()));
        if (ids.size() > 0) {
            UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("is_time_out", IsTimeOutType.TIME_OUT_TYPE_YES);
            updateWrapper.eq("status", Status.STATUS_NOT_AUDITED);
            updateWrapper.in("id", ids);
            update(updateWrapper);
        }
        logger.info(">>>>>>>>【任务调度（6小时过期，更新为已超时）】，接口：【updateTaskSonIsTimeOut6Hour】执行查询任务的过期时间小于当前时间的任务数据>>> end");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void findTaskSonIsTimeOut() {
        // 为了防止数据统计出错，则该接口实现加入了代码块同步锁
        synchronized (this) {
            logger.info("==>>>>>>>>【任务调度 （将已超时的任务返回到任务池中）】，获取已超时的任务，将已超时的任务返回到任务池中，接口：【findTaskSonIsTimeOut】>>> start");
            QueryWrapper<TaskSonVo> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("ts.is_time_out", IsTimeOutType.TIME_OUT_TYPE_YES);
            queryWrapper.and(f->f.eq("ts.status", Status.STATUS_ACTIVITY)
                    .or(g->g.eq("ts.status", Status.STATUS_NOT_AUDITED).eq("ts.images", TaskTypeCode.TASK_TYPE_GRD)));
            List<TaskSonVo> list = baseMapper.findTaskSonIsTimeOut(queryWrapper);

            // 遍历的更新
            for (TaskSonVo tsv : list) {
                // 更新主任务剩余数量,增加
                iTaskService.updateTaskResidueTotalLock(tsv.getTaskId(),Boolean.TRUE);

                // 更新任务搜索词剩余数量，增加
                iTaskKeywordService.updateTaskKeywordNumberLock(tsv.getKeywordId(),Boolean.TRUE);

                // 更新业务员信用额度,增加额度
                //iSalesmanService.updateSalesmanCreditById(tsv.getSalesmanId(), tsv.getRealPrice(), false);

                // 返回任务可领取数量，返回到任务池中即可。
                TaskRedis.increment(tsv.getTaskId());

                // \(^o^)/~ 物理删除对应的子任务数据
                removeById(tsv.getId());
                logger.info("==>>>>>>>>【要返回到任务池的任务数：{}】，所属机构：{}，所属主任务：{}，所属任务：{}，所属搜索词：{}，所属业务员：{}", list.size(), tsv.getOrgId(),tsv.getTaskId(), tsv.getId(), tsv.getKeywordId(), tsv.getSalesmanId());
            }
            logger.info("==>>>>>>>>【任务调度 （将已超时的任务返回到任务池中）】，获取已超时的任务，将已超时的任务返回到任务池中，接口：【findTaskSonIsTimeOut】>>> end");
        }
    }

    @Override
    public List<TaskSonSalesmanVo> checkSalesmanIsInactive(Integer day) {
        return baseMapper.checkSalesmanIsInactive(day);
    }

    @Override
    public List<StatementTaskSonVo> findSalesmanBrokerage() {
        return baseMapper.findSalesmanBrokerage();
    }

    @Override
    public List<StatementPercentageVo> findSalesmanPercentage() {
        return baseMapper.findSalesmanPercentage();
    }

    @Override
    public List<StatementPercentageVo> findSalesmanPercentageByDateAndJobNumber(String date, String jobNumber) {
        return baseMapper.findSalesmanPercentageByDateAndJobNumber(date, jobNumber);
    }

    @Override
    public TaskSonVo getTaskSonById(Long id) {
        // 子任务编号
        if (StrUtil.isNull(id)) {
            throw BaseException.base("id值不能为空！");
        }
        return baseMapper.getTaskSonById(id);
    }

    @Override
    public IPage<TaskSonVo> findTaskSonPageList(IPage<TaskSonVo> page, Long salesmanId) {
        baseMapper.findTaskSonPageListBySalesmanId(page,salesmanId);
        return page;
    }

    @Override
    public List<TaskGoodsVo> findGoodAndTime(long id) {
        return  baseMapper.findGoodAndTime(id,Status.STATUS_ACTIVITY);
    }

    @Override
    public int completedTaskCountBySalNumberId(Long salNumberId) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("salesman_number_id",salNumberId);
        queryWrapper.in("status",Status.STATUS_YES_AUDITED);
        return baseMapper.selectCount(queryWrapper);
    }

    @Override
    public List<TaskSon> findTaskSonRemoveTheDay(Long salesmanId, Long salesmanNumberId, Boolean isday) {
        if(isday){
         return baseMapper.findTaskSonRemoveTheDayTrue(salesmanId,salesmanNumberId,Status.STATUS_NULLIFY);
        }else{
           return baseMapper.findTaskSonRemoveTheDayFalse(salesmanId,salesmanNumberId,Status.STATUS_NULLIFY);
        }
    }

    @Override
    public TaskSon findTaskSon(Long salesmanId, Long salesmanNumberId, Long sellerShopId, Long goodsId) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(salesmanId),"salesman_id",salesmanId);
        queryWrapper.notIn("status",Status.STATUS_NULLIFY);
        queryWrapper.eq(StrUtil.isNotNull(salesmanNumberId),"salesman_number_id",salesmanNumberId);
        queryWrapper.eq(StrUtil.isNotNull(sellerShopId),"seller_shop_id",sellerShopId);
        queryWrapper.eq(StrUtil.isNotNull(goodsId),"goods_id",goodsId);
        queryWrapper.last("limit 1");

        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public boolean verifyIsSameTaskSon(Long taskSonId, Long platformId, Long salesmanId, Long salNumberId) {
        // 获取当前任务的所属店铺的编码
        // 店铺数据
        SellerShop ss= baseMapper.selectStoreData(taskSonId,Status.STATUS_NULLIFY,platformId);
        if(null == ss) {
            return false;
        }
        // 置空
        // 获取店铺同编码的任务数据
        // 店铺同编码的任务数据
        List<TaskSon> list = baseMapper.sameTaskSon(ss.getShopCode(),Status.STATUS_NULLIFY,salesmanId,salNumberId);
        // 判断非空
        return !list.isEmpty();
    }

    @Override
    public String getUnavailableShopIds(Long orgId, Long salNumberId, int days, Long platformId) {
        // 获取业务员信息
        Salesman s = iSalesmanService.findSalesmanBySalesmanNumberId(salNumberId);
        if (StrUtil.isNull(s)) {
            return null;
        }
        // 获取配置信息
        // 任务配置
        TaskSetting setting = iTaskSettingService.findTaskSettingByOrgIdAndSalLevelId(s.getOrgId(), s.getSalesmanLevelId());

        // 获取平台信息
        ConfigPlatform platform = iConfigPlatformService.findPlatformOneById(platformId);

        // 拼多多逻辑
        boolean pddFlag = true;
        if (StrUtil.isNotNull(platform)) {
            if ((PlatformType.P_TYPE_PDD).equalsIgnoreCase(platform.getCode())) {
                pddFlag = false;
            }
        }

        // \(^o^)/~ 获取当前号主所做过的店铺任务
        // 定义临时数据
        StringBuilder tmp = new StringBuilder();
        // 定义店铺编码集合
        Set<Integer> shopCodes = new HashSet<>();
        // 定义店铺ID集合
        Set<Long> shopIds = new HashSet<>();
        // 定义店铺ID集合
        List<SellerShop> list;
        // 下线之间
        List<Salesman> offlineSalesman;
        // 索引
        int index;

        QueryWrapper<SalesmanNumber> qw1;
        List<SalesmanNumber> snList;

        // 当前业务的指定平台的号主所接的任务
        if (setting.getMemberFlag() > 0 || pddFlag) {
            // 当前业务该平台下的号主
            qw1 = new QueryWrapper<>();
            qw1.eq("platform_id", platformId);
            qw1.eq("salesman_id", s.getId());
            snList = iSalesmanNumberService.list(qw1);
            for (SalesmanNumber sn : snList) {
                // 获取当前号主做过的店铺任务
                list = getSellerShopId(sn.getId(), days);
                // 赋值
                list.forEach(f->shopIds.add(f.getId()));
            }
        }

        // 下线之间逻辑错开机制
        if (setting.getOfflineFlag() > 0 && pddFlag) {
            // 获取下线业务员做过的店铺任务
            // 获取下线之间的做过的店铺任务
            offlineSalesman = iSalesmanService.findSalesmanOffline(s.getId());
            getOfflineSalesmanTask(salNumberId, setting.getOfflineIntervalDay(), platformId, shopIds, offlineSalesman);
        }

        // 获取上线业务员信息
        if (pddFlag) {
            if (setting.getOnlineFlag() > 0 || setting.getOnlineOfflineFlag() > 0) {
                Salesman salesmanOnline = iSalesmanService.findSalesmanOnlineById(salNumberId);
                if (StrUtil.isNotNull(salesmanOnline) && !salesmanOnline.getMobile().equals("1") && !salesmanOnline.getId().equals(salNumberId)) {
                    // 当前业务该平台下的号主
                    qw1 = new QueryWrapper<>();
                    qw1.eq("platform_id", platformId);
                    qw1.eq("salesman_id", salesmanOnline.getId());
                    snList = iSalesmanNumberService.list(qw1);

                    for (SalesmanNumber sn : snList) {
                        if (StrUtil.isNotNull(sn) && setting.getOnlineFlag() > 0) {
                            // 获取上线号主做过的店铺任务
                            list = getSellerShopId(sn.getId(), setting.getOnlineIntervalDay());
                            // 赋值
                            list.forEach(f->shopIds.add(f.getId()));
                        }
                    }

                    // 上线的下线(同级)之间错开
                    if (setting.getOnlineOfflineFlag() > 0) {
                        // 获取下线之间的做过的店铺任务
                        offlineSalesman = iSalesmanService.findSalesmanOffline(salesmanOnline.getId());
                        getOfflineSalesmanTask(salNumberId, setting.getOnlineOfflineIntervalDay(), platformId, shopIds, offlineSalesman);
                    }
                }
            }
        }

        // 上下线逻辑错开机制
        if (shopIds.size() <= 0) {
            return null;
        }

        // 数据封装
        shopIds.forEach(f->tmp.append(f).append(","));
        index = tmp.lastIndexOf(",");
        if (index != -1) {
            tmp.deleteCharAt(index);
        }

        // \(^o^)/~ 根据当前做过的店铺，获取对应的店铺编码数据
        QueryWrapper<SellerShop> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id",StrUtil.getIdsByString(tmp.toString()));
        queryWrapper.eq("org_id",orgId);
        queryWrapper.eq("platform_id",platformId);
        list = iSellerShopService.list(queryWrapper);

        // 数据封装
        list.forEach(f->shopCodes.add(f.getShopCode()));
        tmp.setLength(0);
        shopCodes.forEach(f->tmp.append(f).append(","));
        index = tmp.lastIndexOf(",");
        if (index != -1) {
            tmp.deleteCharAt(index);
        }

        if (tmp.length() < 1) {
            return null;
        }

        // \(^o^)/~ 根据当前的店铺简码获取店铺数据
        queryWrapper = new QueryWrapper<>();
        queryWrapper.in("shop_code",StrUtil.getIdsByString(tmp.toString()));
        queryWrapper.eq("org_id",orgId);
        queryWrapper.eq("platform_id",platformId);
        list = iSellerShopService.list(queryWrapper);

        // 数据封装
        tmp.setLength(0);
        list.forEach(f->tmp.append(f.getId()).append(","));
        index = tmp.lastIndexOf(",");
        if (index != -1) {
            tmp.deleteCharAt(index);
        }

        return tmp.toString();
    }

    // 获取同类目信息
    @Override
    public String getUnavailableGoodsIds(Long orgId, Long salNumberId, int days, Long platformId) {
        List<String> list = baseMapper.findCategoryByTaskSon(orgId, salNumberId, days, platformId);
        if (list.size() < 1) {
            return null;
        }
        QueryWrapper<SellerShopGoods> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("category", list);
        queryWrapper.select("id");
        List<SellerShopGoods> data = iSellerShopGoodsService.list(queryWrapper);
        if (data.size() < 1) {
            return null;
        }
        StringBuilder ids = new StringBuilder();
        data.forEach(f->ids.append(f.getId()).append(Chars.CHAR_COMMA));
        ids.deleteCharAt(ids.lastIndexOf(Chars.CHAR_COMMA));
        return ids.toString();
    }

    public List<SellerShop> getSellerShopId(Long salesmanNumberId, int days) {
        return  baseMapper.getSellerShopId(salesmanNumberId,days);
    }

    public void getOfflineSalesmanTask(Long salNumberId, int days, Long platformId, Set<Long> shopIds, List<Salesman> offlineSalesman) {
        List<SalesmanNumber> snList;
        List<SellerShop> list;
        if (offlineSalesman.size() > 1) {
            for (Salesman salesman : offlineSalesman) {
                // 当前业务该平台下的号主
                QueryWrapper<SalesmanNumber> qw1 = new QueryWrapper<>();
                qw1.eq("platform_id", platformId);
                qw1.eq("salesman_id", salesman.getId());
                snList = iSalesmanNumberService.list(qw1);
                for (SalesmanNumber sn : snList) {
                    if (StrUtil.isNull(sn) || sn.getId().equals(salNumberId)) {
                        continue;
                    }
                    list = getSellerShopId(sn.getId(), days);
                    // 赋值
                    list.forEach(f -> shopIds.add(f.getId()));
                }
            }
        }
    }

    @Override
    public int findNotSubmitTaskSonBySalesmanId(Long salesmanId) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("status",Status.STATUS_NOT_AUDITED,Status.STATUS_ACTIVITY,Status.STATUS_NOT_PASS);
        queryWrapper.eq(StrUtil.isNotNull(salesmanId),"salesman_id",salesmanId);
        queryWrapper.apply("datediff(now(), create_time) > 0");
        return baseMapper.selectCount(queryWrapper);
    }

    @Override
    public Integer searchTaskSonNotReviewTotalBySalesmanId(Long salesmanId) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(salesmanId),"salesman_id",salesmanId);
        queryWrapper.eq("status",Status.STATUS_ALREADY_UPLOAD);
        queryWrapper.apply("datediff(now(), create_time) > 0");
        return baseMapper.selectCount(queryWrapper);
    }

    @Override
    public int findSalAllotNumberCount(Long salesmanId, Long salNumberId, Long platformId) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(salesmanId),"salesman_id",salesmanId);
        queryWrapper.apply("date_format(create_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d')");
        queryWrapper.notIn(StrUtil.isNotNull(salNumberId),"salesman_number_id",salNumberId);
        queryWrapper.notIn("status",Status.STATUS_NULLIFY);
        queryWrapper.eq(StrUtil.isNotNull(platformId),"platform_id",platformId);
        queryWrapper.orderByAsc("salesman_number_id");

        List<TaskSon> list = baseMapper.selectList(queryWrapper);
        if (list.isEmpty()) {
            return 0;
        }
        return list.size();
    }

    @Override
    public int findDayTaskCountBySalNumberId(Long salNumberId, Long platformId) {
        return  baseMapper.findDayTaskCountBySalNumberId(salNumberId,platformId);
    }

    @Override
    public int findNumberTaskCountByDay(Long salNumberId, Long platformId, Integer day) {
        return baseMapper.findNumberTaskCountByDay(salNumberId,platformId,day);
    }

    @Override
    public boolean verifyTaskSon(Long salesmanNumberId, Long shopId) {
        QueryWrapper<TaskSon> quyeryWrapper = new QueryWrapper<>();
        quyeryWrapper.eq(StrUtil.isNotNull(shopId),"seller_shop_id",shopId);
        quyeryWrapper.eq(StrUtil.isNotNull(salesmanNumberId),"salesman_number_id",salesmanNumberId);
        quyeryWrapper.last("limit 1");
        Integer count = baseMapper.selectCount(quyeryWrapper);
        return StrUtil.isNotNull(count) && count > 0;
    }

    @Override
    public int taskSonCountByTaskId(Long taskId) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(taskId),"task_id",taskId);
        queryWrapper.notIn("status",Status.STATUS_NULLIFY);

        return baseMapper.selectCount(queryWrapper);
    }

    @Override
    public int taskSonCountByParams(Long taskId, Long salesmanId, Long salNumberId) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(taskId),"task_id",taskId);
        queryWrapper.eq(StrUtil.isNotNull(salesmanId),"salesman_id",salesmanId);
        queryWrapper.eq(StrUtil.isNotNull(salNumberId),"salesman_number_id",salNumberId);
        queryWrapper.notIn("status",Status.STATUS_NULLIFY);

        return baseMapper.selectCount(queryWrapper);
    }

    @Override
    public boolean repeatPurchaseTaskSon(Long salesmanNumberId, Long shopId, int day) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.apply(" DATE_FORMAT(create_time ,'%Y-%m-%d') <= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL " + day + " DAY),'%Y-%m-%d')");
        queryWrapper.eq(StrUtil.isNotNull(salesmanNumberId),"salesman_number_id",salesmanNumberId);
        queryWrapper.eq(StrUtil.isNotNull(shopId),"seller_shop_id",shopId);
        queryWrapper.last("limit 1");
        TaskSon ts = baseMapper.selectOne(queryWrapper);
        return StrUtil.isNotNull(ts);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskSon(TaskSon taskSon) {
        baseMapper.insert(taskSon);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void taskSonReplaceSalesmanNumber(Long taskSonId, Long salesmanId, Long startSalNumberId, Long endSalNumberId, String imei) {
        TaskSon taskSon = new TaskSon();
        taskSon.setSalesmanNumberId(endSalNumberId);
        taskSon.setImei(imei);
        UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",taskSonId);
        updateWrapper.eq("salesman_id",salesmanId);
        updateWrapper.eq("salesman_number_id",startSalNumberId);
        baseMapper.update(taskSon,updateWrapper);
    }

    @Override
    public TaskSon findSameTaskSon(Long numberId, Long sellerShopId, Long goodsId, int day) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.apply("DATE_FORMAT(create_time ,'%Y-%m-%d') <= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL " + day + " DAY),'%Y-%m-%d')");
        queryWrapper.eq(StrUtil.isNotNull(numberId),"salesman_number_id",numberId);
        queryWrapper.eq(StrUtil.isNotNull(sellerShopId),"seller_shop_id",sellerShopId);
        queryWrapper.eq(StrUtil.isNotNull(goodsId),"goods_id",goodsId);
        queryWrapper.last("limit 1");

        return baseMapper.selectOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void taskSonAppeal(Long taskSonId) {
        TaskSon taskSon = new TaskSon();
        taskSon.setStatus(Status.STATUS_APPEAL);
        UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",taskSonId);
        updateWrapper.eq("status",Status.STATUS_AUDITED_FAIL);
        baseMapper.update(taskSon,updateWrapper);
    }

    @Override
    public IPage<TaskSonVo2> findTaskSonVoList(Long salesmanId, String status, IPage<TaskSonVo2> page) {

        QueryWrapper<TaskSonVo2> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(salesmanId),"ts.salesman_id",salesmanId);
        queryWrapper.eq(StrUtil.isNotNull(status),"ts.status",status);
        queryWrapper.eq("scd.status",Status.STATUS_ACTIVITY);
        queryWrapper.eq("sc.status",Status.STATUS_ACTIVITY);
        queryWrapper.eq("sc.code",SysConfigType.TASK_TYPE);
        queryWrapper.orderByDesc("ts.create_time");

        IPage<TaskSonVo2>  iPage = baseMapper.findTaskSonVoList(page,queryWrapper);
        for (TaskSonVo2 taskSonExt : iPage.getRecords()) {
            taskSonExt.setTimestamp(DateUtil.timestamp(new Date(), taskSonExt.getDeadlineTime()));

            // ~判断隔日单是否可以点操作!
            if (taskSonExt.getTaskCode().equalsIgnoreCase(TaskTypeCode.TASK_TYPE_GRD) && taskSonExt.getStatus().equalsIgnoreCase(Status.STATUS_ACTIVITY)) {
                taskSonExt.setDay(!DateUtil.isSameDate(new Date(), taskSonExt.getDeadlineTime()));
            }
        }
        return page;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchGenerateTaskSonPayGoods(Long salesmanId, String taskSonIds, String applyType) throws BaseException {

        List<TaskSonVo> list = iTaskSonService.findTaskSonList(taskSonIds, salesmanId, Status.STATUS_ACTIVITY, IsTimeOutType.TIME_OUT_TYPE_NO);
        // 判断任务数据不存在
        if (list.isEmpty()) {
            throw BaseException.base("操作失败！");
        }
        // 保存超时的任务id
        List<Long> timeOutId = new ArrayList<>();
        // 保存要更新状态的任务id
        List<Long> updateStatusId = new ArrayList<>();
        // 遍历任务数据
        for (TaskSonVo taskSon : list) {
            // 忽略掉【隔日单】
            /*if (!taskSon.getTaskTypeCode().equalsIgnoreCase(TaskTypeCode.TASK_TYPE_GRD)) {
                // 【现付单】判断超时状态
            	// 超时只针对【现付单】
                if (!DateUtil.compareTwoTimeSizeb(taskSon.getDeadlineTime(), new Date())) {
                    // 保存要进行更新为超时的任务id
                    timeOutId.add(taskSon.getId());
                    continue;
                }
            }*/

            // 判断货款已经生成，直接忽略掉
            if (iPayGoodsService.verifyPayGoods(taskSon.getId(), taskSon.getSalesmanId(), taskSon.getSalesmanNumberId())) {
                continue;
            }

            // 生成任务货款
            FinancePayGoods payGoods = new FinancePayGoods();
            payGoods.setApplyNumber(iFormNoGenerateService.generateFormNo(FormNoTypeEnum.ZF_ORDER));
            payGoods.setSalesmanNumberId(taskSon.getSalesmanNumberId());
            payGoods.setOrgId(taskSon.getOrgId());
            payGoods.setSalesmanId(salesmanId);
            payGoods.setStatus(Status.STATUS_NOT_PAY);
            payGoods.setApplyMoney(taskSon.getRealPrice());
            payGoods.setExport(Number.INT_ZERO);
            payGoods.setTaskSonId(taskSon.getId());
            payGoods.setCreateTime(new Date());
            payGoods.setType(applyType);
            iPayGoodsService.savePayGoods(payGoods);
            logger.info("===>生成任务货款成功： 所属机构：{} ，货款单号：{}，货款金额：{}，货款类型：{}", taskSon.getOrgId(), payGoods.getApplyNumber(), payGoods.getApplyMoney(), applyType);
            // 更新子任务货款类型
            TaskSon taskSon1 = new TaskSon();
            taskSon1.setPaymentType(applyType);
            UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("id",taskSon.getId());
            baseMapper.update(taskSon1,updateWrapper);
            // 保存已生成货款任务id
            updateStatusId.add(taskSon.getId());
            // 更新成功生成货款,更新任务状态: 从开始A>>>>>待提交F，任务的过期时间更新当前时间的累加6个小时
            //updateeTaskSonStatusAndDeadlineTime(taskSon.getId(), Status.STATUS_NOT_AUDITED, 6);

            /*// 判断当前的任务是否是申请货款还是自动垫付货款!
            // 判断如果是自动垫付货款的话，就要恢复对应的业务员的信用额度值!
            if (PayGoodsType.PAY_GOODS_TYPE_DF.equalsIgnoreCase(applyType)) {
                // [{业务员},{额度值},{增减信用额度}]
                iSalesmanService.updateSalesmanCreditById(taskSon.getSalesmanId(), taskSon.getRealPrice(), Boolean.FALSE);
            }*/
        }

        // 判断有超时的任务，并且更改为超时状态
        /*if(timeOutId.size() > 0){
            UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("is_time_out", IsTimeOutType.TIME_OUT_TYPE_YES);
            updateWrapper.in("id", timeOutId);
            updateWrapper.eq("is_time_out", IsTimeOutType.TIME_OUT_TYPE_NO);
            update(updateWrapper);
        }*/

        // 货款生成，将任务状态从开始状态【A】=== 结束状态【F】，并且过期时间累加6小时。
        if (updateStatusId.size() > 0) {
            for (Long id: updateStatusId) {
                baseMapper.updateTaskSonDeadlineTime(id, Status.STATUS_ACTIVITY, Status.STATUS_NOT_AUDITED, 6);
            }
        }

        // 有超时的数据异常信息抛出
       /* if(timeOutId.size() > 0){
            throw BaseException.base("有"+timeOutId.size()+"个任务超时");
        }*/
    }

    @Override
    public boolean isVerifyTaskSonByOrderNum(String orderNum) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(orderNum),"order_num",orderNum);
        queryWrapper.notIn("status",Status.STATUS_NULLIFY);
        queryWrapper.last("limit 1");
        Integer count = baseMapper.selectCount(queryWrapper);
        return count != 0;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTaskSonAndTaskSonImages(TaskSon taskSon, String images) {
        // 保存子任务数据
        baseMapper.updateById(taskSon);
        // JSONObject to List
        if (StrUtil.isNotNull(images)) {
            List<TaskSonImages> imgs = JSONArray.parseArray(images, TaskSonImages.class);
            // 遍历集合图片数据
            for (TaskSonImages img : imgs) {
                // 逻辑删除子任务对应的图片数据
                iTaskSonImagesService.logicDeleteTasksonImgsByTaskSonId(taskSon.getId(), img.getTptsId());
                //
                //iTaskSonImagesService.logicDeleteTasksonImgsByTaskSonId(taskSonId);
                // 保存任务图片信息数据
                iTaskSonImagesService.saveTaskSonImages(img);
            }
        }
    }

    @Override
    public List<TaskImgEchoVo> findDataById(String ids) {
        QueryWrapper<TaskImgEchoVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.inSql(StrUtil.isNotNull(ids),"ts.id",ids);
        queryWrapper.in("ts.status",Status.STATUS_NOT_AUDITED,Status.STATUS_NOT_PASS);
        queryWrapper.eq("ss.`status`", Status.STATUS_ACTIVITY );

        return baseMapper.findDataById(queryWrapper);
    }

    @Override
    public List<TaskImgEchoVo> findNotPassDataById(String ids) {
        QueryWrapper<TaskImgEchoVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.inSql(StrUtil.isNotNull(ids),"ts.id",ids);
        queryWrapper.in("ts.status",Status.STATUS_NOT_AUDITED,Status.STATUS_NOT_PASS);
        queryWrapper.eq("ss.`status`", Status.STATUS_ACTIVITY);
        queryWrapper.orderByDesc("tsnp.create_time");

        return baseMapper.findNotPassDataById(queryWrapper);
    }

    @Override
    public TaskSonDetailVo findTaskSonDetail(Long taskSonId, Long platformId) {

        QueryWrapper<TaskSonDetailVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cw.status",Status.STATUS_ACTIVITY );
        queryWrapper.apply("ta.platform_id = cw.platform_id");
        queryWrapper.eq(StrUtil.isNotNull(taskSonId),"ts.id",taskSonId);
        queryWrapper.eq(StrUtil.isNotNull(platformId),"ta.platform_id",platformId);

        return  baseMapper.findTaskSonDetail(queryWrapper);
    }

    @Override
    public TaskSonVo findTaskSonExt(Long id) {
        return  baseMapper.findTaskSonExt(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskSonisMark(Long taskSonId) {
        TaskSon taskSon = new TaskSon();
        taskSon.setIsMark(IsMarkType.MARK_TYPE_YES);
        UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",taskSonId);
        updateWrapper.eq("is_mark",IsMarkType.MARK_TYPE_NO);
        baseMapper.update(taskSon,updateWrapper);
    }

    @Override
    public IPage<TaskSonVo> findTaskSonOvertimeList(Long salesmanId, IPage<TaskSonVo> page) {
        QueryWrapper<TaskSonVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotNull(salesmanId),"ts.salesman_id",salesmanId);
        queryWrapper.eq("ts.is_time_out",IsTimeOutType.TIME_OUT_TYPE_YES);
        queryWrapper.apply("ts.create_time >= DATE_SUB(NOW(),INTERVAL 2 DAY)");
        queryWrapper.orderByDesc("ts.create_time");

        return baseMapper.findTaskSonOvertimeList(page,queryWrapper);
    }

    @Override
    public IPage<TaskSonCommissionTotalApi> findCommission(Long salesmanId, IPage<TaskSonCommissionTotalApi> page) {
        return baseMapper.findCommission(page,salesmanId);
    }

    @Override
    public List<TaskSon> findCommission(Long salesmanId, String time) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("task_son_number"," update_time", "brokerage");
        queryWrapper.eq(StrUtil.isNotNull(salesmanId)," salesman_id",salesmanId);
        queryWrapper.in("status",Status.STATUS_YES_AUDITED);
        queryWrapper.apply("DATE_FORMAT(update_time ,'%Y-%m-%d') = '" + time + "' AND is_time_out = 'NO'");
        queryWrapper.orderByAsc("update_time");

        return  baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<TaskSonVo> findTaskSonList(String ids, Long salId, String status, String isTimeOut) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.inSql(StrUtil.isNotNull(ids)," ts.id",ids);
        queryWrapper.eq("sc.code",SysConfigType.TASK_TYPE);
        queryWrapper.eq(StrUtil.isNotNull(salId),"ts.salesman_id",salId);
        queryWrapper.eq(StrUtil.isNotNull(status),"ts.status",status);
        queryWrapper.eq(StrUtil.isNotNull(isTimeOut),"ts.is_time_out",isTimeOut);

        return  baseMapper.findTaskSonList(queryWrapper);
    }

    @Override
    public long completedTaskCountBySalManId(long salManId) {
        // 差下級業務員的已完成任務單數
        // 已完成按，審核通過，審核失敗，的狀態進行統計。
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("salesman_id",salManId);
        queryWrapper.in("status",Status.STATUS_YES_AUDITED,Status.STATUS_AUDITED_FAIL);
        return baseMapper.selectCount(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void auditorTaskSonByTaskSonId(Long id, Long auditorId, String status, String account, TaskSon taskSon) {
        UpdateWrapper<TaskSon> updateWrapper;
        // 第一次审核
        if (null != status && (status.equalsIgnoreCase(Status.STATUS_NOT_PASS) || status.equalsIgnoreCase(Status.STATUS_AUDITED_FAIL))) {
            updateWrapper = new UpdateWrapper<>();
            TaskSon uTs = new TaskSon();
            uTs.setUpdateTime(new Date());
            uTs.setStatus(status);
            uTs.setAccount(account);
            uTs.setAuditorId(auditorId);
            updateWrapper.eq("id", id);
            updateWrapper.eq("status", Status.STATUS_ALREADY_UPLOAD);
            // \(^o^)/~ 任务状态的更新 ，已传图  >>>>  审核不通过，或者，审核失败
            update(uTs, updateWrapper);

            // 判断只有在审核不通过（AN）的情况下才清空订单号。
            if(status.equalsIgnoreCase(Status.STATUS_NOT_PASS)) {
                updateWrapper = new UpdateWrapper<>();
                // 清除订单号
                updateWrapper.set("order_num", null);
                updateWrapper.eq("id", id);
                update(updateWrapper);
            }
        } else {
            updateWrapper = new UpdateWrapper<>();
            TaskSon uTs = new TaskSon();
            uTs.setUpdateTime(new Date());
            uTs.setStatus(Status.STATUS_YES_AUDITED);
            uTs.setAuditorId(auditorId);
            updateWrapper.eq("id", id);
            updateWrapper.eq("status", Status.STATUS_ALREADY_UPLOAD);
            // \(^o^)/~ 任务状态的更新 ，已传图  >>>> 审核通过，任务审核通过情况下，验证是否是超时任务，
            // \(^o^)/~ 如是超时任务则不生成佣金流水，否则生成佣金流水
            update(uTs, updateWrapper);
            // 非超时状态
            /*if (taskSon.getIsTimeOut().equalsIgnoreCase(Status.NOT_OK)) {
                // 生成佣金流水数据
                FinanceStatement statement = new FinanceStatement();
                statement.setSalesmanId(taskSon.getSalesmanId());
                statement.setTaskSonId(taskSon.getId());
                statement.setMoney(taskSon.getBrokerage());
                statement.setTaskSonStatus(status);
                statement.setStatus(Number.INT_ONE);
                statement.setCreateTime(new Date());
                iFinanceStatementService.save(statement);
            }*/
            // 标记该业务员已经完成首次任务
            iSalesmanService.updateSalesmanFirstMission(taskSon.getSalesmanId());
            // 子任务改为 已审核
            //taskSon.setStatus(Status.STATUS_YES_AUDITED);
            // 匹配应付款与实付款
            iTaskSonService.saveDifferPrice(taskSon.getRealPrice(), taskSon.getPayment(), taskSon, Status.STATUS_YES_AUDITED);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void auditorTaskSonAgainByTaskSonId(Long id, Long auditorId, String startStatus, String endStatus, String account, TaskSon taskSon) {
        UpdateWrapper<TaskSon> updateWrapper;
        // 重审
        if (null != endStatus && (endStatus.equalsIgnoreCase(Status.STATUS_AUDITED_FAIL) || endStatus.equalsIgnoreCase(Status.STATUS_NOT_PASS))
                && startStatus.equalsIgnoreCase(Status.STATUS_YES_AUDITED)) {

            //该子任务单第一次审核状态为T，重审的状态只能为AF,AN
            if (endStatus.equalsIgnoreCase(Status.STATUS_NOT_PASS)) {
                updateWrapper = new UpdateWrapper<>();
                updateWrapper.set("update_time", new Date());
                updateWrapper.set("status", endStatus);
                updateWrapper.set("account", account);
                updateWrapper.set("auditor_id", auditorId);
                updateWrapper.set("order_num", null);
                updateWrapper.eq("id", id);
                updateWrapper.eq("status", startStatus);
                //当重审状态为AN时，不更新子任务重审的次数
                // 清除订单号
                update(updateWrapper);
            } else {
                updateWrapper = new UpdateWrapper<>();
                updateWrapper.set("update_time", new Date());
                updateWrapper.set("status", endStatus);
                updateWrapper.set("account", account);
                updateWrapper.set("auditor_id", auditorId);
                updateWrapper.eq("id", id);
                updateWrapper.eq("status", startStatus);
                update(updateWrapper);
            }
            // 非超时
            /*if (taskSon.getIsTimeOut().equalsIgnoreCase(Status.NOT_OK)) {
                // 生成【减】佣金流水，当前任务为审核通过状态，重审为不通过或者失败的状态下时
                // 则会生成减佣金流水数据!
                FinanceStatement statement = new FinanceStatement();
                statement.setSalesmanId(taskSon.getSalesmanId());
                statement.setTaskSonId(taskSon.getId());
                statement.setMoney(taskSon.getBrokerage().negate());
                statement.setTaskSonStatus(endStatus);
                statement.setStatus(Number.INT_ZERO);
                statement.setCreateTime(new Date());
                iFinanceStatementService.save(statement);
            }*/

        } else if (null != endStatus && (endStatus.equalsIgnoreCase(Status.STATUS_YES_AUDITED) || endStatus.equalsIgnoreCase(Status.STATUS_NOT_PASS))
                && startStatus.equalsIgnoreCase(Status.STATUS_AUDITED_FAIL)) {
            // 该子任务单第一次审核状态为AF，重审的状态只能为T,AN
            if (endStatus.equalsIgnoreCase(Status.STATUS_NOT_PASS)) {
                updateWrapper = new UpdateWrapper<>();
                updateWrapper.set("update_time", new Date());
                updateWrapper.set("status", endStatus);
                updateWrapper.set("account", account);
                updateWrapper.set("auditor_id", auditorId);
                updateWrapper.set("order_num", null);
                updateWrapper.eq("id", id);
                updateWrapper.eq("status", startStatus);
                // 当重审状态为AN时，不更新子任务重审的次数
                // 清除订单号
                update(updateWrapper);
            } else {
                updateWrapper = new UpdateWrapper<>();
                updateWrapper.set("update_time", new Date());
                updateWrapper.set("status", endStatus);
                updateWrapper.set("account", account);
                updateWrapper.set("auditor_id", auditorId);
                updateWrapper.eq("id", id);
                updateWrapper.eq("status", startStatus);
                update(updateWrapper);
                // 非超时
                /*if (taskSon.getIsTimeOut().equalsIgnoreCase(Status.NOT_OK)) {
                    // 生成【加】佣金流水，如当前任务状态为，失败状态更新为审核通过状态下时
                    // 则会生成加佣金流水数据!
                    FinanceStatement statement = new FinanceStatement();
                    statement.setSalesmanId(taskSon.getSalesmanId());
                    statement.setTaskSonId(taskSon.getId());
                    statement.setMoney(taskSon.getBrokerage());
                    statement.setTaskSonStatus(endStatus);
                    statement.setStatus(Number.INT_ONE);
                    statement.setCreateTime(new Date());
                    iFinanceStatementService.save(statement);
                }*/

                // 子任务改为 已审核
                //taskSon.setStatus(Status.STATUS_YES_AUDITED);
                // 匹配应付款与实付款
                saveDifferPrice(taskSon.getRealPrice(), taskSon.getPayment(), taskSon, Status.STATUS_YES_AUDITED);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void auditorTaskSonByTaskSonIds(String ids, Long auditorId) {
        List<Long> list = StrUtil.getIdsByLong(ids);
        if (StrUtil.isNull(list)) {
            return;
        }
        TaskSon taskSon;
        UpdateWrapper<TaskSon> updateWrapper;
        for (Long id : list) {
            // 获取子任务数据
            taskSon = getById(id);
            // 匹配应付款与实付款
            saveDifferPrice(taskSon.getRealPrice(), taskSon.getPayment(), taskSon, Status.STATUS_YES_AUDITED);
            updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("update_time", new Date());
            updateWrapper.set("status", Status.STATUS_YES_AUDITED);
            updateWrapper.set("auditor_id", auditorId);
            updateWrapper.eq("id", id);
            updateWrapper.eq("status", Status.STATUS_ALREADY_UPLOAD);
            update(updateWrapper);
            // 标记该业务员已经完成首次任务
            iSalesmanService.updateSalesmanFirstMission(taskSon.getSalesmanId());
            iSalesmanService.updateSalesmanTaskTotal(taskSon.getSalesmanId(), 1);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskSonIsTimeOut(Long id, Long auditorId, String isTimeOut) {
        // 获取子任务数据
        TaskSon ts = getById(id);
        // 将超时任务更新为未超时，则生成佣金流水！
        if(isTimeOut.equals(IsTimeOutType.TIME_OUT_TYPE_NO) && ts.getStatus().equals(Status.STATUS_YES_AUDITED)) {
            // 生成佣金流水
            FinanceStatement statement = new FinanceStatement();
            statement.setSalesmanId(ts.getSalesmanId());
            statement.setTaskSonId(ts.getId());
            statement.setMoney(ts.getBrokerage());
            statement.setTaskSonStatus(ts.getStatus());
            statement.setStatus(Number.INT_ONE);
            statement.setCreateTime(new Date());
            iFinanceStatementService.save(statement);
        }
        UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("is_time_out", isTimeOut);
        updateWrapper.set("auditor_id", auditorId);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTaskSonPaymentById(Long id, BigDecimal payment) {
        if (StrUtil.isNull(id)) {
            throw BaseException.base("更新子任务实付款失败,id为空");
        }
        UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("payment", payment);
        updateWrapper.eq("id", id);
        update(updateWrapper);
    }

    @Override
    public TaskSon findTaskSonByOrderNum(String orderNum) {
        QueryWrapper<TaskSon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_num", orderNum);
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateOrderNumByTaskSonId(Long taskSonId, Long platformId, String orderNum) {
        UpdateWrapper<TaskSon> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("order_num", orderNum);
        updateWrapper.eq("id", taskSonId);
        updateWrapper.eq("platform_id", platformId);
        update(updateWrapper);
    }
}
