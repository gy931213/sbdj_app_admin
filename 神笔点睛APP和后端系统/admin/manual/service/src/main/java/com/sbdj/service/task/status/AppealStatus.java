package com.sbdj.service.task.status;

/**
 * 子任务申诉状态枚举类
 */
public enum AppealStatus {
    STATUS_WAIT("W", "待审核"),
    STATUS_AUDITED("T", "已审核"),
    STATUS_NOT_PASS("N", "不通过"),
    STATUS_DELETE("D", "已删除");

    private AppealStatus(String code, String name) {
        this.code = code;
        this.name = name;
    }

    private String code;
    private String name;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
