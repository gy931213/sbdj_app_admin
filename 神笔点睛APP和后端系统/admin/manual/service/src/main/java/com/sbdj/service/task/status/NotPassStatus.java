package com.sbdj.service.task.status;

public enum NotPassStatus {
    STATUS_ACTIVITY("A", "已活动"),
    STATUS_DELETE("D", "已删除");

    private NotPassStatus() {
    }

    NotPassStatus(String code, String name) {
        this.code = code;
        this.name = name;
    }

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
