package com.sbdj.service.task.type;

/**
 *
 */
public final class IsCardType {

    /**
     * 否 0
     */
    public final static Integer CARD_NOT = 0;

    /**
     * 是 1
     */
    public final static Integer CARD_YES = 1;

    /**
     * 自动 2
     */
    public final static Integer CARD_AUTO = 2;
}
