package com.sbdj.service.task.type;

/**
 * Created by Htl to 2019/05/06
 * 打标类型类
 */
public final class IsMarkType {

    /**
     * 已打标 1
     */
    public static final Integer MARK_TYPE_YES = 1;

    /**
     * 未打标 0
     */
    public static final Integer MARK_TYPE_NO = 0;

}
