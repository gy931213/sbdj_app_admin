package com.sbdj.service.task.type;

/**   
 * @ClassName:  KeywordType   
 * @Description: 关键字类型   
 * @author: 黄天良  
 * @date: 2018年11月30日 下午5:42:12   
 */
public final class KeywordType {

	/**
	 * 图片链接 {@value}
	 */
	public static final String KEYWORD_U = "U";
	
	/**
	 * 字符串 {@value}
	 */
	public static final String KEYWORD_S = "S";
}
