package com.sbdj.service.task.type;

/**
 * 是否商家对的
 *
 * @author Administrator
 */
public class OrderMatchType {

    /**
     * 是
     */
    public static final String OREDER_MATCH_YES = "YES";

    /**
     * 否
     */
    public static final String OREDER_MATCH_NO = "NO";

}
