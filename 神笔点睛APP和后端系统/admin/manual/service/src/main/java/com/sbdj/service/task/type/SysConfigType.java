package com.sbdj.service.task.type;

/**   
 * @ClassName:  SysConfigType   
 * @Description: 系统配置code   
 * @author: 黄天良  
 * @date: 2018年12月1日 下午5:42:32   
 */
public final class SysConfigType {

	/**
	 * 任务类型 {@value}
	 */
	public static final String TASK_TYPE = "TASK_TYPE";
	
	/**
	 * 图片类型
	 */
	public static final String CODE_IMG = "TPLX";
	
	/**
	 * 通讯录条数
	 */
	public static final String CODE_TXLTS = "TXLTS";
	
	/**
	 * 证件类型
	 */
	public static final String CODE_ZJLX = "CODE_ZJLX";
	
}
