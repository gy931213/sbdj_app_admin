package com.sbdj.service.task.type;

public interface TaskPictureType {

    public static final String TYPE_XFD = "XFD";

    public static final String TYPE_GRD = "GRD";

}
