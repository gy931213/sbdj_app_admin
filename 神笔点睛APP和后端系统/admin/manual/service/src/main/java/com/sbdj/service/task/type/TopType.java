package com.sbdj.service.task.type;

/**
 * 是否置顶
 * @author 黄天良
 */
public final class TopType {

	/**
	 * 是 {@value}
	 */
	public static final int TOP_YES = 1;
	/**
	 * 否 {@value}
	 */
	public static final int TOP_NO = 0;
}
