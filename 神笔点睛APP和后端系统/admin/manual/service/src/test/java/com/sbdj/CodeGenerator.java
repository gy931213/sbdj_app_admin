package com.sbdj;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.*;

/**
 * Created by Yly to 2019/11/8 10:16
 * 代码生成器
 */
public class CodeGenerator {
    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotEmpty(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    public static void main(String[] args) {
        // 需要生成的表
        String[] tables = ("account_info,audit_accounts,bank_stream,complain,config_bank,config_card_position_setting,config_check_number," +
                "config_formula,config_mark_goods_setting,config_marking,config_mirror,config_platform,config_qiniu,config_quota_setting," +
                "config_stay_net_time,config_warn_message,finance_differ_price,finance_draw_money_record,finance_pay_goods,finance_percentage," +
                "finance_percentage_tmp,finance_prize_penalty,finance_salesman_bill,finance_seller_bill,finance_statement,finance_statement_tmp," +
                "info_help,info_notice,salesman,salesman_bank_card,salesman_credit_record,salesman_images,salesman_level,salesman_mobile_code," +
                "salesman_number,salesman_number_bscore_level,salesman_number_cash_return_record,salesman_number_phones_tmp,salesman_number_setting," +
                "salesman_number_tmp,salesman_number_wechat_tying,salesman_percentage_leaderboard,salesman_phones,salesman_phones_tmp,salesman_ranking," +
                "salesman_royalty,salesman_statement_leaderboard,seller,seller_express_info,seller_shop,seller_shop_goods,seller_shop_record,sys_admin,sys_admin_bank," +
                "sys_admin_role,sys_area,sys_black_library,sys_cloud_library,sys_config,sys_config_dtl,sys_function,sys_map_trajectory,sys_number,sys_org_function," +
                "sys_org_role,sys_organization,sys_role,sys_role_function,sys_admin_info,task,task_common_sentence,task_keyword,task_picture_type_setting,task_setting,task_son," +
                "task_son_appeal,task_son_images,task_son_not_pass,task_son_nullify,wechat_cash_return,wechat_config,wechat_member_info,wechat_openid_info," +
                "wechat_red_package").split(",");
        String[] taskTable = "task,task_son,task_son_nullify,task_son_not_pass,task_setting,task_common_sentence,task_keyword,task_son_appeal,task_son_images,task_picture_type_setting".split(",");
        String taskPackageName = "task";
        String[] systemTable = "sys_admin,sys_admin_role,sys_area,sys_config,sys_config_dtl,sys_function,sys_number,sys_org_function,sys_organization,sys_role,sys_role_function,sys_admin_bank,sys_admin_info,sys_map_trajectory".split(",");
        String systemPackageName = "system";
        String[] sellerTable = "seller_shop,seller_shop_goods".split(",");
        String sellerPackageName = "seller";
        String[] memberTable = "salesman,salesman_number,salesman_level,salesman_mobile_code,salesman_images,salesman_number_tmp,salesman_phones,salesman_phones_tmp,salesman_royalty,salesman_percentage_leaderboard,salesman_statement_leaderboard,salesman_number_phones_tmp".split(",");
        String memberPackageName = "member";
        String[] configureTable = "bank_stream,config_bank,config_formula,config_platform,config_warn_message".split(",");
        String configurePackageName = "configure";
        String[] financeTable = "finance_differ_price,finance_pay_goods,finance_draw_money_record,finance_percentage,finance_statement,finance_salesman_bill,finance_prize_penalty".split(",");
        String financePackageName = "finance";
        String[] informationTable = "info_help,info_notice".split(",");
        String informationPackageName = "information";

        String packageName = financePackageName;
        String[] table = "salesman_credit_record".split(",");
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor("Yly");
        gc.setOpen(false);
        gc.setDateType(DateType.ONLY_DATE);
        // 实体属性 Swagger2 注解
        gc.setSwagger2(true);
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/sbdj?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=GMT%2B8");
        // dsc.setSchemaName("public");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("root");
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        //pc.setModuleName(scanner("模块名"));
        pc.setParent("com.sbdj.service");
        //pc.setController(packageName+".controller");
        pc.setEntity(packageName+".entity");
        pc.setMapper(packageName+".mapper");
        pc.setService(packageName+".service");
        pc.setServiceImpl(packageName+".service.impl");

        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
        // String templatePath = "/templates/mapper.xml.vm";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath + "/src/main/resources/mapper" //+ pc.getModuleName()
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        /*
        cfg.setFileCreate(new IFileCreate() {
            @Override
            public boolean isCreate(ConfigBuilder configBuilder, FileType fileType, String filePath) {
                // 判断自定义文件夹是否需要创建
                checkDir("调用默认方法创建的目录");
                return false;
            }
        });
        */
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();

        // 配置自定义输出模板
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
        // templateConfig.setEntity("templates/entity2.java");
        // templateConfig.setService();
        // templateConfig.setServiceImpl();
        // templateConfig.setController();

        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        // 公共实体
        //strategy.setSuperEntityClass("com.sbdj.core.common.BaseEntity");
        strategy.setEntityLombokModel(false);
        strategy.setRestControllerStyle(true);
        // 公共父类
        //strategy.setSuperControllerClass("com.baomidou.core.base.BaseController");
        // 写于父类中的公共字段
        //strategy.setSuperEntityColumns("id");
        strategy.setInclude(table);
        //strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
        // 驼峰转连字符
        strategy.setControllerMappingHyphenStyle(true);
        strategy.setTablePrefix(pc.getModuleName() + "_");
        // 跳过视图
        strategy.setSkipView(true);
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }
}
