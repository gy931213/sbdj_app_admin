package com.sbdj;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbdj.core.constant.Status;
import com.sbdj.core.util.DateUtil;
import com.sbdj.core.util.GPSUtil;
import com.sbdj.core.util.StrUtil;
import com.sbdj.service.member.admin.query.RegisterQuery;
import com.sbdj.service.member.entity.Salesman;
import com.sbdj.service.member.entity.SalesmanNumberPhonesTmp;
import com.sbdj.service.member.entity.SalesmanNumberTmp;
import com.sbdj.service.member.service.*;
import com.sbdj.service.system.entity.SysAdminRole;
import com.sbdj.service.system.entity.SysFunction;
import com.sbdj.service.system.entity.SysMapTrajectory;
import com.sbdj.service.system.entity.SysRoleFunction;
import com.sbdj.service.system.service.*;
import com.sbdj.service.task.entity.TaskSon;
import com.sbdj.service.task.service.ITaskService;
import com.sbdj.service.task.service.ITaskSonAppealService;
import com.sbdj.service.task.service.ITaskSonService;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by Yly to 2019/11/8 10:01
 * 主要测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MainTest {
    //@Autowired
    private ISysConfigDtlService iSysConfigDtlService;

    private ISalesmanMobileCodeService iSalesmanMobileCodeService;

    //@Autowired
    private ISysFunctionService iSysFunctionService;
    //@Autowired
    private ISysRoleFunctionService iSysRoleFunctionService;
    //@Autowired
    private ISysAdminRoleService iSysAdminRoleService;

    //@Test
    public void saveRoleFunction() {
        List<SysFunction> list = iSysFunctionService.findFunctionPageList();
        SysRoleFunction rf;
        for (SysFunction function : list) {
            rf = new SysRoleFunction();
            rf.setRoleId(1L);
            rf.setFuncId(function.getId());
            rf.setCreateTime(new Date());
            rf.setStatus(Status.STATUS_ACTIVITY);
            iSysRoleFunctionService.save(rf);
        }
        System.out.println("success");
    }

    //@Test
    public void saveAdminRole() {
        SysAdminRole adminRole = new SysAdminRole();
        adminRole.setAdminId(1L);
        adminRole.setRoleId(1L);
        adminRole.setCreateTime(new Date());
        adminRole.setStatus(Status.STATUS_ACTIVITY);
        iSysAdminRoleService.save(adminRole);
        System.out.println("success");
    }

    //@Test
    public void testConfigDtl() {
        RegisterQuery registerParam = new RegisterQuery();
        registerParam.setMobile("19999999999");
        registerParam.setName("ziji");
        registerParam.setOnlineMobile("17984351645");
        registerParam.setSalesmanId(14881L);
        if (iSalesmanService.findSalesmanByMobile(registerParam.getMobile()) != null) {
            System.out.println("该号码已注册！");
        }
        // 2019年9月2日 18:27:30 改动的地方
		/*SalesmanMobileCode salesmanMobileCode = null;
		Date expireTime = null;
		try {
			salesmanMobileCode = iSalesmanMobileCodeService.findSalesmanMobileCode(registerParam.getMobile());
			expireTime = salesmanMobileCode.getExpireTime();
		} catch (NullPointerException e) {
			return RespResult.error("请输入正确手机号！");
		}
		boolean FiveMinutes = new Date(System.currentTimeMillis()).getTime() > expireTime.getTime();
		if (!registerParam.getCode().equals("") || registerParam.getCode() != null) {
			if (FiveMinutes) {
				return RespResult.error("验证码过期！");
			}
			if (registerParam.getCode().equals(salesmanMobileCode.getCode()) && !FiveMinutes) {
				// iSalesmanMobileCodeService.register(registerParam,
				// RequestKit_api.identify_app(request));
				iSalesmanMobileCodeService.register(registerParam, registerParam.getSalesmanId());
			} else {
				return RespResult.error("验证码有误！");
			}
		} else {
			return RespResult.error("请输入验证码！");
		}*/
        iSalesmanMobileCodeService.register(registerParam, registerParam.getSalesmanId());
        System.out.println("成功");
        /*List<SysConfigDtl> cfds = iSysConfigDtlService.findConfigDtl(3L);
        Long grd = 0L;
        Long xfd = 0L;

        for (SysConfigDtl cfd : cfds) {
            System.out.println(cfd.getName());
        }

        for (SysConfigDtl cfd : cfds) {
            if (cfd.getName().equals("隔日单")) {
                grd = cfd.getId();
            } else if (cfd.getName().equals("现付单")) {
                xfd = cfd.getId();
            }
        }
        System.out.println("grd:"+grd+" xfd:"+xfd);*/
    }

    //@Autowired
    private ISalesmanService iSalesmanService;

    //@Test
    public void testEnableSalesman() {
        System.out.println("启用解封的业务员");
        iSalesmanService.autoEnableSalesmanByDisableTime();
        System.out.println("结束解封的业务员");
    }

    //@Test
    /*public void testUpdateSalesman() {
        iSalesmanService.updateSalesmanLastLoginTimeAndLoginTotal(14876L);
    }*/

    //@Test
    public void testUpdateStatus() {
        String id = "14876";
        UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_UNREVIEWED);
        //updateWrapper.set("id", 14876L);
        updateWrapper.eq("id", id);
        boolean flag = iSalesmanService.update(updateWrapper);
        System.out.println("状态: " + flag);
    }

    //@Test
    public void testSearchSalesman() {
        //List<String> list = new ArrayList<>();
        //list.add("14875");
        //list.add("14876");
        String[] ids = {"14875","14876"};
        UpdateWrapper<Salesman> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", Status.STATUS_UNREVIEWED);
        //updateWrapper.in("id", ids);
        iSalesmanService.update(updateWrapper);
    }

    //@Test
    public void testComputeTaskBrokerageToSalesmanBalance() {
        iSalesmanService.computeTaskBrokerageToSalesmanBalance();
    }

    //@Test
    public void testComputeTaskPercentageToSalesmanBalance() {
        iSalesmanService.computeTaskPercentageToSalesmanBalance();
    }

    //@Autowired
    private ITaskSonService iTaskSonService;

    //@Test
    public void testDoTaskSon() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>开始处理隔日单<<<<<<<<<<<<<<<<<<<<<<<<<");
        // 获取昨天未提交的隔日单
        List<TaskSon> list = iTaskSonService.searchGRDNotSubmitYesterday();
        if (list.size() > 0) {
            System.out.println("昨日总共有"+list.size()+"个隔日单没有提交");
            for (TaskSon taskSon : list) {
                // 隔日单第一次传图，将过期时间更新为第二天的中午12点13分14秒
                taskSon.setDeadlineTime(DateUtil.genDayDate(0, 12, 13, 14));
                taskSon.setStatus(Status.STATUS_ACTIVITY);
                taskSon.setImages("[]");
                iTaskSonService.updateTaskSon(taskSon);
            }
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>结束处理隔日单<<<<<<<<<<<<<<<<<<<<<<<<<");
    }

    //@Test
    public void testTaskSonIsTimeOut30Minute() {
        iTaskSonService.updateTaskSonIsTimeOut30Minute();
    }

    //@Test
    public void testTaskSonIsTimeOut6Hour() {
        iTaskSonService.updateTaskSonIsTimeOut6Hour();
    }

    //@Test
    public void testBackTaskSon() {
        iTaskSonService.findTaskSonIsTimeOut();
    }

    //@Autowired
    private ITaskService iTaskService;

    //@Test
    public void testUndoneTask() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>> 开始获取未完成任务 <<<<<<<<<<<<<<<<<<<<<<<<");
        iTaskService.updateUndoneTaskByDays();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>> 结束获取未完成任务 <<<<<<<<<<<<<<<<<<<<<<<<");
    }

    //@Test
    public void stopTask() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>> 开始终止任务 <<<<<<<<<<<<<<<<<<<<<<<<");
        iTaskService.schedulerStopTask();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>> 结束终止任务 <<<<<<<<<<<<<<<<<<<<<<<<");
    }

    //@Autowired
    private ISalesmanPercentageLeaderboardService iSalesmanPercentageLeaderboardService;
    //@Autowired
    private ISalesmanStatementLeaderboardService iSalesmanStatementLeaderboardService;

    //@Test
    public void testSalesmanPercentageLeaderboard() {
        iSalesmanPercentageLeaderboardService.saveSalesmanPercentageLeaderboard(2L);
    }

    //@Test
    public void testSalesmanStatementLeaderboard() {
        iSalesmanStatementLeaderboardService.saveSalesmanStatementLeaderboard(2L);
    }

    //@Autowired
    private ITaskSonAppealService iTaskSonAppealService;

    //@Test
    public void testSaveTaskSonAppeal() {
        iTaskSonAppealService.saveTaskSonAppeal(5L, "test", "http://img.sbdj.wang/1.jpg");
    }

    //@Autowired
    private ISalesmanNumberTmpService iSalesmanNumberTmpService;
    //@Autowired
    private ISysMapTrajectoryService iSysMapTrajectoryService;
    //@Autowired
    private ISalesmanNumberPhonesTmpService iSalesmanNumberPhonesTmpService;

    //@Test
    public void testGetDistance() {
        // 手机号码
        String phone = "15073246701";
        // 获取距离相差1KM以内的信息
        double distance = 1.00;
        // 开始时间
        long startTime = System.currentTimeMillis();
        // 获取临时号主信息
        SalesmanNumberTmp snt = iSalesmanNumberTmpService.findSalesmanNumberByPhone(phone);
        // 获取该临时号主的最近一次定位
        SysMapTrajectory smt = iSysMapTrajectoryService.getOne(new QueryWrapper<SysMapTrajectory>()
                .eq("target_id", snt.getId()).orderByDesc("create_time").last("limit 1"));
        // 获取所有的定位信息
        QueryWrapper<SysMapTrajectory> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("target_id","longitude","latitude","ip")
                .notExists("select 1 from sys_map_trajectory smt where target_id=smt.target_id and smt.create_time < create_time");
        List<SysMapTrajectory> list = iSysMapTrajectoryService.list(queryWrapper);
        Map<Long, Map<String, Object>> data = new HashMap<>();
        Map<String, Object> info;
        double distanceTmp;
        // 临时号主信息
        SalesmanNumberTmp sntTmp;
        // 获取符合要求的数据
        for (SysMapTrajectory tmp : list) {
            info = new HashMap<>();
            distanceTmp = GPSUtil.getDistance(Double.parseDouble(smt.getLongitude()), Double.parseDouble(smt.getLatitude()),
                    Double.parseDouble(tmp.getLongitude()), Double.parseDouble(tmp.getLatitude()));
            if (distanceTmp > distance) {
                continue;
            }
            sntTmp = iSalesmanNumberTmpService.getById(tmp.getTargetId());
            info.put("distance", distanceTmp);
            info.put("position", tmp.getLongitude() + "," + tmp.getLatitude());
            info.put("targetId", tmp.getTargetId());
            info.put("ip", tmp.getIp());
            info.put("name", sntTmp.getName());
            info.put("phone", sntTmp.getMobile());
            data.put(tmp.getTargetId(), info);
        }
        System.out.println(data.size());
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(data));
        StringBuilder ids = new StringBuilder();
        data.keySet().forEach(f -> ids.append(f).append(","));
        ids.deleteCharAt(ids.lastIndexOf(","));
        System.out.println(ids);
        // 结束时间
        long endTime = System.currentTimeMillis();
        System.out.println("位置检测 总共耗时: " + (endTime - startTime) + "ms");
    }

    //@Test
    public void testGetSameContacts() {
        String phone = "18509021510";
        // 开始时间
        long startTime = System.currentTimeMillis();
        // 正则 只提取数字
        Pattern pattern = Pattern.compile("[^0-9]");
        // 初始化Gson
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        // 获取被查询手机的信息
        SalesmanNumberTmp mySnt = iSalesmanNumberTmpService.findSalesmanNumberByPhone(phone);
        // 获取被查询手机的通讯录
        List<SalesmanNumberPhonesTmp> mySnpt = iSalesmanNumberPhonesTmpService.findSalesmanNumberPhonesTmpList(mySnt.getId());
        // 定义被查询手机的通讯录里面已注册查小号的手机
        Set<String> phones = new HashSet<>();
        // 临时数据-号主
        SalesmanNumberTmp sntTmp;
        // 获取手机信息
        String phoneTmp;
        Map<String,String> tmpMap = new HashMap<>();
        for (SalesmanNumberPhonesTmp tmp : mySnpt) {
            // 获取有效手机号码
            phoneTmp = pattern.matcher(tmp.getPhone()).replaceAll("");
            // 排除自己的手机
            if (phone.equalsIgnoreCase(phoneTmp)) {
                continue;
            }
            tmpMap.put(phoneTmp, tmp.getName());
        }
        System.out.println("这个 "+mySnt.getName()+" 通讯录实际数量: " + mySnpt.size());
        // 加入自己
        tmpMap.put(phone, mySnt.getName());
        // 通讯录条数
        int myTotal = tmpMap.size();
        // 迭代查询数据
        for (SalesmanNumberPhonesTmp tmp : mySnpt) {
            // 获取有效手机号码
            phoneTmp = pattern.matcher(tmp.getPhone()).replaceAll("");
            // 排除自己的手机
            if (phone.equalsIgnoreCase(phoneTmp)) {
                continue;
            }
            // 在系统查询记录
            sntTmp = iSalesmanNumberTmpService.findSalesmanNumberByPhone(phoneTmp);
            if (StrUtil.isNotNull(sntTmp)) {
                phones.add(sntTmp.getMobile());
            }
        }
        // 匹配通讯录的数据
        List<SalesmanNumberPhonesTmp> otherSnpt;
        // 数据
        // 定义通讯录匹配模型
        Map<String, Object> contactModel;
        // 使用集合存取数据
        List<Map<String, Object>> dataList = new ArrayList<>();
        // 其他数据条数
        int otherTotal;
        // 我的联系人
        String myContact;
        // 其他联系人
        String otherContact;
        // 保留两位小数
        DecimalFormat df = new DecimalFormat("#0.00");
        // 计数器
        int count;
        // 百分比
        double percentage;
        for (String str : phones) {
            // 获取这个手机所对应的记录
            sntTmp = iSalesmanNumberTmpService.findSalesmanNumberByPhone(str);
            // 获取该手机的通讯录
            otherSnpt = iSalesmanNumberPhonesTmpService.findSalesmanNumberPhonesTmpList(sntTmp.getId());
            if (StrUtil.isNotNull(otherSnpt)) {
                Map<String,String> tmpsMap = new HashMap<>();
                for (SalesmanNumberPhonesTmp tmps : otherSnpt) {
                    otherContact = pattern.matcher(tmps.getPhone()).replaceAll("");
                    tmpsMap.put(otherContact, tmps.getName());
                }
                System.out.println("这个 " +sntTmp.getName()+ " 通讯录实际数量: " + tmpsMap.size());
                tmpsMap.put(sntTmp.getMobile(), sntTmp.getName());
                otherTotal = tmpsMap.size();
                // 排除没有通讯录记录的数据
                if (otherTotal < 1) {
                    continue;
                }
                contactModel = new HashMap<>();
                contactModel.put("myPhone", phone);
                contactModel.put("myName", mySnt.getName());
                contactModel.put("otherPhone", str);
                contactModel.put("otherName", sntTmp.getName());
                contactModel.put("myTotal", myTotal);
                contactModel.put("otherTotal", otherTotal);
                count = 0;
                // 数据匹配
                for (String other : tmpsMap.keySet()) {
                    for (String my : tmpMap.keySet()) {
                        if (other.equalsIgnoreCase(my)) {
                            count++;
                        }
                    }
                }
                // 只需要重复的
                if (count < 1) {
                    continue;
                }
                contactModel.put("sameData", count);
                percentage = (count * 1.0) / (myTotal * 1.0);
                contactModel.put("percentage", df.format(percentage));
                dataList.add(contactModel);
            }
        }
        // JSON序列化
        System.out.println(gson.toJson(dataList));
        // 结束时间
        long endTime = System.currentTimeMillis();
        System.out.println("通讯录检测 总共耗时: " + (endTime - startTime) + "ms");
    }

    //@Test
    public void testGetSameContactsNew() {
        String phone = "18664805711";
        // 开始时间
        long startTime = System.currentTimeMillis();
        // 正则 只提取数字
        Pattern pattern = Pattern.compile("[^0-9]");
        // 初始化Gson
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        // 获取被查询手机的信息
        SalesmanNumberTmp mySnt = iSalesmanNumberTmpService.findSalesmanNumberByPhone(phone);
        // 获取被查询手机的通讯录
        List<SalesmanNumberPhonesTmp> mySnpt = iSalesmanNumberPhonesTmpService.findSalesmanNumberPhonesTmpList(mySnt.getId());

        Map<String, String> myContacts = new HashMap<>();
        String phoneTmp;
        for (SalesmanNumberPhonesTmp spt : mySnpt) {
            if (StrUtil.isNull(spt.getPhone())) {
                continue;
            }
            phoneTmp = pattern.matcher(spt.getPhone()).replaceAll("");
            if (phone.equalsIgnoreCase(phoneTmp)) {
                continue;
            }
            myContacts.put(phoneTmp, spt.getName());
        }
        myContacts.put(phone, mySnt.getName());
        // 统计数量
        int myContactsSize = myContacts.size();

        Map<Long, Map<String, String>> ext1 = new HashMap<>();
        Map<String, String> ext2;
        List<SalesmanNumberPhonesTmp> list = iSalesmanNumberPhonesTmpService.list(new QueryWrapper<SalesmanNumberPhonesTmp>().select("phone", "name", "target_id"));

        // 排除重复数据
        for (SalesmanNumberPhonesTmp snpt : list) {
            if (StrUtil.isNull(snpt.getPhone())) {
                continue;
            }
            if (ext1.containsKey(snpt.getTargetId())) {
                ext2 = ext1.get(snpt.getTargetId());
            } else {
                ext2 = new HashMap<>();
            }
            phoneTmp = pattern.matcher(snpt.getPhone()).replaceAll("");
            ext2.put(phoneTmp, snpt.getName());
            ext1.put(snpt.getTargetId(), ext2);
        }

        // 重复计数
        int count;
        // 重复率
        double percentage;
        // 临时号主
        SalesmanNumberTmp snt;
        // 保存数据
        Map<String, Object> map;
        // 保留两位小数
        DecimalFormat df = new DecimalFormat("#0.00");
        List<Map<String, Object>> data = new ArrayList<>();
        // 匹配通讯录
        for (Map.Entry<Long, Map<String, String>> entry : ext1.entrySet()) {
            count = 0;
            for (String tmp : myContacts.keySet()) {
                for (String key : entry.getValue().keySet()) {
                    if (tmp.equalsIgnoreCase(key)) {
                        count++;
                    }
                }
            }
            if (count > 0) {
                percentage = (double) count / (double) myContactsSize;
                if (percentage < 0.01) {
                    continue;
                }
                snt = iSalesmanNumberTmpService.getById(entry.getKey());
                map = new HashMap<>();
                map.put("name", snt.getName());
                map.put("phone", snt.getMobile());
                map.put("percentage", df.format(percentage));
                map.put("myContactsTotal", myContactsSize);
                map.put("otherContactsTotal", entry.getValue().size());
                map.put("sameSize", count);
                data.add(map);
            }
        }
        System.out.println(data.size());
        System.out.println(gson.toJson(data));

        // 结束时间
        long endTime = System.currentTimeMillis();
        System.out.println("通讯录检测 总共耗时: " + (endTime - startTime) + "ms");
    }
}
