package com.sbdj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by Yly to 2019/11/8 9:59
 * 业务层测试
 */
@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = "com.sbdj.service.*.mapper")
public class ServiceTest {
    public static void main(String[] args) {
        SpringApplication.run(ServiceTest.class);
    }
}
