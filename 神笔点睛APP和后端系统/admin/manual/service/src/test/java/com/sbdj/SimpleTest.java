package com.sbdj;

import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.AppMessage;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.exceptions.RequestException;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.style.Style0;
import com.sbdj.core.exception.BaseException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * <p>
 * 简单测试
 * </p>
 *
 * @author Yly
 * @since 2019/11/22 14:12
 */
public class SimpleTest {
    public void testException() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("1.基本异常 2.SQL异常 3.ORM异常");
        System.out.print("请输入数字: ");
        int num = scanner.nextInt();
        switch (num) {
            case 1:
                throw BaseException.base(BaseException.BASE.BASE);
            case 2:
                throw BaseException.base(BaseException.BASE.SQL);
            case 3:
                throw BaseException.base(BaseException.BASE.ORM);
            default:
                System.out.println("输入异常");
                break;
        }
        scanner.close();
    }

    // STEP1：获取应用基本信息
    private static String appId = "ksW6U7BUgu8gT3MWheGVZ3";
    private static String appKey = "dXyk4vysPp9X9K6ILSiEu6";
    private static String masterSecret = "KVNQK9Ssbw7kXZNnjfaMx8";
    private static String url = "http://sdk.open.api.igexin.com/apiex.htm";

    //@Test
    public void testAppPush() {
        IGtPush push = new IGtPush(url, appKey, masterSecret);

        Style0 style = new Style0();
        // STEP2：设置推送标题、推送内容
        style.setTitle("消息通知");
        style.setText("【查小号】你有一条消息待处理");
        //style.setLogo("push.png");  // 设置推送图标
        // STEP3：设置响铃、震动等推送效果
        style.setRing(true);  // 设置响铃
        style.setVibrate(true);  // 设置震动


        // STEP4：选择通知模板
        NotificationTemplate template = new NotificationTemplate();
        template.setAppId(appId);
        template.setAppkey(appKey);
        template.setStyle(style);


        // STEP5：定义"AppMessage"类型消息对象,设置推送消息有效期等推送参数
        List<String> appIds = new ArrayList<>();
        appIds.add(appId);
        AppMessage message = new AppMessage();
        message.setData(template);
        message.setAppIdList(appIds);
        message.setOffline(true);
        message.setOfflineExpireTime(1000 * 600);  // 时间单位为毫秒

        // STEP6：执行推送
        IPushResult ret = push.pushMessageToApp(message);
        System.out.println(ret.getResponse().toString());
    }

    public static NotificationTemplate getNotificationTemplate() {
        NotificationTemplate template = new NotificationTemplate();
        // 设置APPID与APPKEY
        template.setAppId(appId);
        template.setAppkey(appKey);

        Style0 style = new Style0();
        // 设置通知栏标题与内容
        style.setTitle("消息通知");
        style.setText("【查小号】你有一条消息待处理");
        // 配置通知栏图标
        //style.setLogo("icon.png");
        // 配置通知栏网络图标
        style.setLogoUrl("");
        // 设置通知是否响铃，震动，或者可清除
        style.setRing(true);
        style.setVibrate(true);
        style.setClearable(true);
        //style.setChannel("通知渠道id");
        //style.setChannelName("通知渠道名称");
        //style.setChannelLevel(3); //设置通知渠道重要性
        template.setStyle(style);

        //template.setTransmissionType(1);  // 透传消息接受方式设置，1：立即启动APP，2：客户端收到消息后需要自行处理
        //template.setTransmissionContent("请输入您要透传的内容");
        return template;
    }

    //@Test
    public void testPushToSingle() {
        // STEP1：获取应用基本信息
        String appId = "3f3hCfumB77TBzCsqQxpF2";
        String appKey = "Sg663BAPSt5B5s7TyZ1b15";
        String masterSecret = "pTRgM8wN5o8GFbyjhvXmE8";
        String url = "http://sdk.open.api.igexin.com/apiex.htm";
        String cid = "3f5113f7de9e88a335d8b8977f2a2501";
        //String alias = "heros";
        IGtPush push = new IGtPush(url, appKey, masterSecret);
        NotificationTemplate template = getNotificationTemplate();
        SingleMessage message = new SingleMessage();
        message.setOffline(true);
        // 离线有效时间，单位为毫秒
        message.setOfflineExpireTime(24 * 3600 * 1000);
        message.setData(template);
        // 可选，1为wifi，0为不限制网络环境。根据手机处于的网络情况，决定是否下发
        message.setPushNetWorkType(0);
        Target target = new Target();
        target.setAppId(appId);
        target.setClientId(cid);
        //target.setAlias(alias);
        IPushResult ret = null;
        try {
            ret = push.pushMessageToSingle(message, target);
        } catch (RequestException e) {
            e.printStackTrace();
            ret = push.pushMessageToSingle(message, target, e.getRequestId());
        }
        if (ret != null) {
            System.out.println(ret.getResponse().toString());
        } else {
            System.out.println("服务器响应异常");
        }
    }
}
