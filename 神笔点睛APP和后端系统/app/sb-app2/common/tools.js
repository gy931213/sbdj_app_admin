import {webUrl} from '@/common/config.js';
export { 
	Showtoast,		//消息提示
	ShowtoastJuep, //消息提示后跳转页面
	Request,		//数据请求
	Setstorage,		//异步缓存数据
	Getstorage,		//异步获取数据
	SwitchTab,		//跳转到 tabBar 页面
	NavigateTo,		//跳转到 非tabBar 页面
	ShowModal,		//显示模态弹窗，类似于标准 html 的消息框
	RedirectTo,		//关闭当前页面栈前往下一页面
}

function Showtoast(titles){//消息提示
	uni.showToast({
		title:titles,
		icon:"none",
		duration:1000,
	})
}

function ShowtoastJuep(titles,callback){//消息提示后跳转页面
	uni.showToast({
		title:titles,
		icon:"none",
		duration:1000,
		success:function(res){
			if(typeof(callback) == "function"){//判断参数是不是一个方法
				callback(res)
			}
		},
	})
}

function ShowModal(titles,contents,showCancels,callback){	//显示模态弹窗，类似于标准 html 的消息框：alert、confirm。
	uni.showModal({
		title:titles,
		content:contents,
		showCancel:showCancels,
		success: (res) => {
			if(typeof(callback) == "function"){//判断参数是不是一个方法
				callback(res)
			}
		}
	})
}
function Request(urls,methods,datas,result){	//向后台请求数据
	let tokens = '';  
	let identify = '';  
	try {
		const value = uni.getStorageSync('useData');
		if (value) {
			tokens =value.token;
			identify = value.salesmanId;
		}
	} catch (e) { 
		// error 
	}

	uni.request({
		header:{
			'content-type':'application/x-www-form-urlencoded',
			'Authorization':tokens,
			'client':'xq.app', 
			"identify":identify
			}, 
		url: webUrl+urls,
		method: methods,
		data: datas,
		success: res => {
			uni.hideLoading();	//取消下拉刷新
			if(res.data.code == 1000){//判断登录是否过期
				uni.redirectTo({
					url:"/pages/login/login" 
				})
				uni.removeStorage({//清理用户数据
					key:"useData",
				});
			}
			result(res);
		},
		fail: () => {
			setTimeout(function(){
				uni.hideLoading();
				uni.showtoast({title:"服务器繁忙！"})
			},1000)
		}
	});
}

function Setstorage(key1,value,callback){//异步缓存数据
	uni.setStorage({
		key:key1,
		data:value,
		success:function(res){
			if(typeof(callback) === "function"){//判断参数是不是一个方法
				callback(res)
			}
		},
	})
}
function Getstorage(key1,callback){//同步获取数据
	uni.getStorage({
		key:key1,
		success:function(res){
			if(typeof(callback) == "function"){//判断参数是不是一个方法
				callback(res)
			}
		},
		fail:function(callback){
			//if(key1=="ID"){
				Showtoast("未登录！请登录")
				uni.redirectTo({
					url:"/pages/login/login"
				})
			//}
		}
	}) 
}


function SwitchTab(usrls){	//跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面
	uni.switchTab({
		url:usrls
	})
}

function NavigateTo(urls,status){//前往下一页面
	if(status === "P" ){
		uni.showToast({
			title:'权限不足',
			icon:"none",
			duration:1000,
		})
	} else{
		uni.navigateTo({
			url:urls,
			animationDuration:200,
			animationType:"pop-in",
		})
	} 
}
function RedirectTo(urls,callback){//关闭当前页面栈前往下一页面
	uni.redirectTo({
		url:urls,
		animationDuration:800,
		animationType:"pop-in",
		success:function(res){
			if(typeof(callback) == "function"){//判断参数是不是一个方法
				callback(res)
			}
		},
	})
}