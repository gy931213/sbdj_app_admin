
import {webUrl} from '@/common/config.js'
import {Showtoast,Getstorage} from '@/common/tools.js'
export default{uploadimage}
function uploadimage(callback){
	uni.chooseImage({ //在choseImag	e里面this不是指向vue
		count:1,
		success:function(res){
			Getstorage(//获取缓存的用户信息 
				"useData",
				function(res1){
					uni.uploadFile({
						url:webUrl+'/base/upload',
						filePath:res.tempFilePaths[0],
						name:"key",
						header:{ 
							'content-type':'application/x-www-form-urlencoded',
							'Authorization':res1.data.token,
							'client':'xq.app',
							'identify':res1.data.salesmanId
						},
						success:function(res1,statusCode){
							console.log(res1)
							var data = JSON.parse(res1.data) 
							if(data.code == 1){
								Showtoast(data.message)
							}
							callback(data.message)
						},
						fail(err) {
							console.log(err)
						}
					})
				}
			)
		}
	});
}