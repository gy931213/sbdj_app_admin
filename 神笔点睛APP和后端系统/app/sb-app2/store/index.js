import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
	state:{
		taskindex:0,//任务的nav
		ownerIndex:true,//号主的nav常用和隐藏
		taskListcar:[], //订单数组
		uid:'',//业务员id
		name:'',//业务员名称
		NoticeInfoVid:'',//公告通知 视频链接
		NoticeInfo:'',//公告通知内容
		NoticeInfoImg:'',//公告通知 图片链接
		navList:[],//分配类型
		token:'',//token值
		balance:'',//余额
		useData:'',//用户数据
		status:'',//用户权限 P:禁用  E:启用
	},
	mutations:{
		changeItem:function(state,value){    //任务nav 索引
			state.taskindex = value
		},
		getToken:function(state,value){//获取token值
			state.token = value
		},
		ownerIndexs:function(state,Booleans){//号主nav 索引
			state.ownerIndex = Booleans
		},
		changetaskIndex:function(state,index){//任务栏nav的样式
			state.taskindex = index
		},
		ChangeTasklistcar:function(state,obj){//号主页=》分配任务栏 
			state.taskListcar = obj
		},
		clearList:function(state,index){//清空订单数组
			state.taskListcar=[]
		},
		changeNoticeInfo:function(state,content){//公告通知内容
			state.NoticeInfo = content;
		},
		changeNoticeInfoVid:function(state,Link){//公告通知内容的视频
			state.NoticeInfoVid = Link;
		},
		changeNoticeInfoImg:function(state,Img){//公告 图片
			state.NoticeInfoImg = Img;
		},
		gainUid:function(state){    //把缓存中信息存入VUEX
		    var that = this;
			try {
				const value = uni.getStorageSync('useData');
				if (value) {
					state.useData = value;
					state.uid = value.salesmanId; 
					state.name = value.name;
					state.token = value.token;
					state.balance = value.balance;
					state.status = value.status;
					uni.stopPullDownRefresh();//禁止下来刷新
				}
			} catch (e) {
				// error
			}
		},
		getNavList:function(state,navList){
			state.navList = navList;
		},
	} 
})	
export default store